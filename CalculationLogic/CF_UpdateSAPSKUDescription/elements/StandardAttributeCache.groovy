List keys = out.SAPBaseSKUConfigCache?.keySet() as List
String attribute
for (key in keys) {
    attribute = key?.minus("Std")?.toUpperCase() + "_TABLE"
    api.local[key] = libs.stdProductLib.RulesGenHelper.cacheCommonAttributes(out.Configuration[attribute], true)
    libs.stdProductLib.RulesGenHelper.cacheAttribute(out.Configuration[attribute])
    libs.stdProductLib.RulesGenHelper.cacheMetaData(out.Configuration[attribute])
}
return