List payload = []
for (skuDetail in api.local.SKUDetails) {
    if (skuDetail)
        payload << Lib.populatePayload(skuDetail.version, skuDetail.typedId, skuDetail.shortdescription, skuDetail.longdescription)
}
if (payload) {
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(payload), true)
}
return