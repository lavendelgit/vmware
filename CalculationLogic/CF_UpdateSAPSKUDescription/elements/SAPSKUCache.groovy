List filter = [
        Filter.equal("name", "SAPBASESKU"),
]
recordStream = api.stream("PX10", null, *filter)
api.local.sapSKUCache = recordStream?.collect { row -> row }
recordStream?.close()
return