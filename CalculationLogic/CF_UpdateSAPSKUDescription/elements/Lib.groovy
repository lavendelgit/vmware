Map populatePayload(long version, String typedId, String shortDesc, Map longDesc) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"   : typedId,
                    "attribute1": shortDesc ?: null,
                    "attribute2": longDesc.LongDesc,
                    "attribute3": longDesc.LongDesc1,
                    "attribute4": longDesc.LongDesc2,
                    "attribute5": longDesc.LongDesc3,
            ],
            "oldValues"    : [
                    "version": version,
                    "typedId": typedId,
            ]
    ]
}

Map generateSKUDetails(String sapSKU, String shortDescription, Map longDescription) {
    Map skuDetails
    api.local.sapSKUCache?.find {
        if (it.sku == sapSKU) {
            skuDetails = [
                    "version"         : it.version,
                    "typedId"         : it.typedId,
                    "shortdescription": shortDescription,
                    "longdescription" : longDescription
            ]
        }
    }
    return skuDetails
}