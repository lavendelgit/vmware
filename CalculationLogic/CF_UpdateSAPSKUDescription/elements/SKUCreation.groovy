api.local.SKUDetails = []
String shortDesc = ""
Map longDescription = [:]
List localSAPSKU = []
Map pXParams
int descLength
String attributeKey
int ruleIndex
List attributeKeys = out.SAPBaseSKUConfigCache?.keySet() as List
List attributeIndex = out.SAPBaseSKUConfigCache?.values() as List
List sapSKUs = api.local.sapSKUCache?.collect { it.sku }
for (combination in api.local.indexedCombination) {
    pXParams = [:]
    attributeIndex?.each { index ->
        attributeKey = attributeKeys[index - Constants.STANDARD_INDEX]
        ruleIndex = out.RuleKeyMapping[attributeKey?.minus(Constants.STD)]
        pXParams[attributeKey] = api.local[attributeKey]?.getAt(combination[ruleIndex])?.attribute1
        pXParams[attributeKey + "Key"] = combination[ruleIndex]
    }
    sapSKU = libs.stdProductLib.SkuGenHelper.generateSAPBaseSKU(out.SAPBaseSKUConfigCache, pXParams)
    if (sapSKUs?.contains(sapSKU) && !localSAPSKU.contains(sapSKU)) {
        pXParams.ShortDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.ShortDescConfig, pXParams)
        longDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.LongDescConfig, pXParams)
        if (longDesc) {
            descLength = longDesc.length()
            if (descLength < Constants.LONGDESC_END_INDEX && descLength > Constants.LONGDESC_ATTRIBUTETHREE_START) {
                longDescription = [
                        LongDesc : longDesc.substring(Constants.LONGDESC_START_INDEX, Constants.LONGDESC_ATTRIBUTEONE_START),
                        LongDesc1: longDesc.substring(Constants.LONGDESC_ATTRIBUTEONE_START, Constants.LONGDESC_ATTRIBUTETWO_START),
                        LongDesc2: longDesc.substring(Constants.LONGDESC_ATTRIBUTETWO_START, Constants.LONGDESC_ATTRIBUTETHREE_START),
                        LongDesc3: longDesc.substring(Constants.LONGDESC_ATTRIBUTETHREE_START, descLength)]
            } else {
                if (descLength < Constants.LONGDESC_ATTRIBUTETHREE_END && descLength > Constants.LONGDESC_ATTRIBUTETWO_START) {
                    longDescription = [
                            LongDesc : longDesc.substring(Constants.LONGDESC_START_INDEX, Constants.LONGDESC_ATTRIBUTEONE_START),
                            LongDesc1: longDesc.substring(Constants.LONGDESC_ATTRIBUTEONE_START, Constants.LONGDESC_ATTRIBUTETWO_START),
                            LongDesc2: longDesc.substring(LONGDESC_ATTRIBUTETWO_START, descLength),
                            LongDesc3: ""]
                } else {
                    if (descLength < Constants.LONGDESC_ATTRIBUTETWO_END && descLength > Constants.LONGDESC_ATTRIBUTEONE_START) {
                        longDescription = [
                                LongDesc : longDesc.substring(Constants.LONGDESC_START_INDEX, Constants.LONGDESC_ATTRIBUTEONE_START),
                                LongDesc1: longDesc.substring(Constants.LONGDESC_ATTRIBUTEONE_START, descLength),
                                LongDesc2: "",
                                LongDesc3: ""]
                    } else {
                        if (descLength < Constants.LONGDESC_ATTRIBUTEONE_END) {
                            longDescription = [
                                    LongDesc : longDesc,
                                    LongDesc1: "",
                                    LongDesc2: "",
                                    LongDesc3: ""]
                        }
                    }
                }
            }
        }
        localSAPSKU << sapSKU
        api.local.SKUDetails << Lib.generateSKUDetails(sapSKU, pXParams.ShortDesc, longDescription)
    }
}
return