List combination = []
List keys = out.SAPBaseSKUConfigCache?.keySet() as List
for (key in keys) {
    combination << api.local[key]?.keySet()
}
api.local.combinations = combination?.combinations()
return