api.local.indexedCombination = []
String value
List keys = out.SAPBaseSKUConfigCache?.keySet() as List
keys?.contains(Constants.TERM) ? keys << "TermUoM" : keys
List configurationIndex = out.RuleKeyMapping?.findAll { keys.contains(it.key) }?.values() as List
int combinationSize = out.RuleKeyMapping?.size()
List indexCombination = []
for (combination in api.local.combinations) {
    indexCombination[combinationSize] = []
    for (index in configurationIndex) {
        if (index == Constants.TERM_UOM_INDEX) {
            value = Constants.TERMUOM_NULL_PLACEHOLDER
            indexCombination.add(index, value)
        } else {
            attribute = out.RuleKeyMapping?.find { it.value == index }?.key
            value = combination[out.SAPBaseSKUConfigCache[attribute] - Constants.STANDARD_INDEX]
            indexCombination.add(index, value)
        }
    }
    api.local.indexedCombination << indexCombination
    indexCombination = []
}
return configurationIndex
