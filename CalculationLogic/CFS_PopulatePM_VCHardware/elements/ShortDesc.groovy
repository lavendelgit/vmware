def currObj = out.CurrentObject
def shortDesc
shortDesc = (currObj) ? currObj.attribute1 : null
if (shortDesc) return shortDesc
