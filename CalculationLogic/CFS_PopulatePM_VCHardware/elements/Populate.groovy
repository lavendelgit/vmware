def sku = out.SKU
def shortDesc = out.ShortDesc
def itemType = out.ItemType

if (sku) Library.populateProductMaster(sku, shortDesc, itemType)

