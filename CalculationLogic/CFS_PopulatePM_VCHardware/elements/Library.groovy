def populateProductMaster(sku, shortDesc, itemType) {

    def record = [
            sku       : sku,
            label     : shortDesc,
            attribute8: itemType

    ]
    if (record) api.addOrUpdate("P", record)
}