api.local.supportTypeAdjustmentCache = [:]
api.local.supportTypeAdjustmentCache = Lib.cacheAdjustments("attribute17")
Map populatedTermCache = [:]
Map adjustmentCache = [:]
if (api.local.supportTypeAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.supportTypeAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.supportTypeAdjustmentCache?.size() > 0) {
        api.local.supportTypeAdjustmentCache = api.local.supportTypeAdjustmentCache + populatedTermCache
    }
}
return