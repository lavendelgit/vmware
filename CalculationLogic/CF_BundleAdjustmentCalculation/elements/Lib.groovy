BigDecimal getUpliftOrFxRate(Map rulesCache, String product, String offer, String base, List adjs) {
    List keys = libs.vmwareUtil.util.frameKeyLists(product, offer, base, adjs)
    String key
    while (keys && keys.size() > 0) {
        key = keys.getAt(0)
        adjustment = rulesCache?.getAt(key)?.Adjustment
        if (adjustment) {
            return adjustment
        }
        keys.removeAt(0)
    }
}

Map getAdjustmentsWithMatchingFlag(Map adjustmentCache, String product, String offer, String base, List adjs) {
    List keys = libs.vmwareUtil.util.frameKeyLists(product, offer, base, adjs)
    BigDecimal adjustment
    String key
    Map adjustmentDetail = [:]
    String matchFlag = "Partial Match- Attribute"
    while (keys && keys.size() > 0) {
        key = keys.getAt(0)
        adjustment = adjustmentCache?.getAt(key)?.Adjustment
        if (adjustment) {
            adjustmentDetail.adjustment = adjustment
            if (key.contains("*")) {
                adjustmentDetail.matchFlag = "Partial Match- Hierarchy"
            } else {
                adjustmentDetail.matchFlag = "Match"
            }
            return adjustmentDetail
        }
        keys.removeAt(0)
    }
}


Map getAdjustmentsFromCacheForBundle(Map adjustmentsCache, Map standardAttributes, Map baseAttributes) {
    Map keyLists = [:]
    List baseKeyLists = []
    List skuKeyLists = []
    Map keys = [:]
    Map baseKeys = [:]
    adjustmentsCache?.each {
        adj ->
            keys = [:]
            baseKeys = [:]
            adj.value.Keys?.each {
                key ->
                    if (standardAttributes[api.local.standardProductMetaData[key]] || Constants.STANDARD_PRODUCT_MAPPING[key]) {
                        exceptionKey = api.local.standardProductMetaData[key] ?: api.local.standardProductMetaData[Constants.STANDARD_PRODUCT_MAPPING[key]]
                        keys[exceptionKey] = standardAttributes[exceptionKey]
                    }
                    if (baseAttributes[api.local.basePriceMetaData[key]] || Constants.STANDARD_PRODUCT_MAPPING[key]) {
                        exceptionKey = api.local.standardProductMetaData[key] ?: api.local.standardProductMetaData[Constants.STANDARD_PRODUCT_MAPPING[key]]
                        baseKeys[exceptionKey] = baseAttributes[exceptionKey]
                    }
            }
            baseKeyLists << baseKeys
            skuKeyLists << keys
    }
    skuKeyLists = skuKeyLists?.minus(["Product", "Offer", "Base"])
    baseKeyLists = baseKeyLists?.minus(["Product", "Offer", "Base"])
    keyLists.StdKeys = skuKeyLists
    keyLists.BaseKeys = baseKeyLists
    return keyLists
}

Map populateTermAltUoMAdjustments(Map adjustmentCache) {
    String term
    String termUoM
    Map populatedTermCache = [:]
    Map destTerm = [:]
    adjustmentCache?.findAll { it.value.Term != null && it.value["Term UoM"] != null }?.each {
        term = it.value.Term
        termUoM = it.value["Term UoM"]
        destTerm = api.local.termUoMConversionCache[term + "_" + termUoM]
        if (destTerm) {
            it.value.Term = destTerm.DestTerm
            it.value["Term UoM"] = destTerm.DestUoM
            key = frameTermKeys(it.value)
            populatedTermCache[key] = it.value
        }
    }
    return populatedTermCache
}

Map cacheAdjustments(String factorAttribute) {
    Map adjustmentCache = [:]
    List adjustments = api.local.stdAdjustmentsCache?.findAll { it[factorAttribute] != null }
    List keys
    excludeList = ["version", "typedId", "name", "id", "sku", "createDate", "createdBy", "lastUpdateDate", "lastUpdateBy", "attribute41", "attribute42", factorAttribute]
    adjustments?.each {
        keys = it.keySet()?.collect()
        keysList = keys?.minus(excludeList)
        keyNames = []
        keysList?.each {
            key ->
                keyNames << api.local.stdAdjMetaData[key]
        }
        keyValueList = []
        Map record = [:]
        keysList?.each {
            key ->
                keyValueList << it[key]
                record[api.local.stdAdjMetaData[key]] = it[key]
        }
        key = libs.vmwareUtil.util.frameKey(keyValueList)
        record.Adjustment = it[factorAttribute]
        record.Keys = keyNames
        adjustmentCache[key] = record
    }
    adjustmentCache = adjustmentCache?.sort { -it.value?.Keys?.size() }
    return adjustmentCache
}

String frameTermKeys(Map termAdj) {
    String keyString
    List keys = termAdj?.Keys
    keys?.each {
        key ->
            keyString = keyString ? keyString + "_" + termAdj[key] : termAdj[key]
    }
    return keyString
}

Map calculateComponentFactor(Map keyLists, Map adjustmentCache, Map SKUInfo) {
    BigDecimal skuAdjustment
    BigDecimal baseAdjustment
    Map skuAdjustmentInfo = [:]
    for (keyList in keyLists?.StdKeys) {
        skuAdjustmentInfo = getAdjustmentsWithMatchingFlag(adjustmentCache, SKUInfo.standardSKUProduct, SKUInfo.standardSKUOffer, SKUInfo.standardSKUBase, keyList.values().drop(3))
        if (skuAdjustmentInfo?.adjustment) {
            break
        }
    }
    skuAdjustmentInfo?.adjustment = skuAdjustmentInfo?.adjustment ?: 1
    return skuAdjustmentInfo
}

List populateAdjustmentRecord(String tableName, Map params) {
    return [
            tableName,
            params.sku,
            params.Product,
            params.Offer,
            params.Base,
            params.PriceListRegion,
            params.SubRegion,
            params.Metric,
            params.Term,
            params.SupportTier,
            params.SupportType,
            params.Segment,
            params.PurchasingProgram,
            params.DC,
            params.RegionRate,
            params.MetricAdj,
            params.TermAdj,
            params.SupportTierAdj,
            params.SupportTypeAdj,
            params.SegmentAdj,
            params.PurchasingProgramAdj,
            params.DCAdj,
            params.Payment,
            params.PaymentAdj,
            params.Currency,
            params.CurrencyAdj,
            params.RoundingType,
            params.RoundingPrecision,
            params.Tier,
            params.TierAdj,
            params.Upgrade,
            params.UpgradeAdj,
            params.Promotion,
            params.PromotionAdj,
            params.TermUoM,
            params.Uplift,
            params.OS,
            params.OSAdj,
            params.Hosting,
            params.HostingAdj,
            params.ProgramOption,
            params.ProgramOptionAdj,
            params.MatchFlag
    ]

}

void calculateBaseBundleAdjustment(Map adjustmentCache, String name, String attributeData, Integer precision) {
    BigDecimal adjustment = 0.0
    BigDecimal componentFactor = 0.0
    BigDecimal componentBasePrice = 0.0
    Map componentSKUInfo = [:]
    Map baseSKUInfo = [:]
    Map SKUInfo = [:]
    Map keyLists = [:]
    BigDecimal bundleBaseSkuPrice = 0.0
    BigDecimal basePrice = 0.0
    BigDecimal discountedBasePrice = 0.0
    BigDecimal priceForCalculation = 0.0
    BigDecimal discount = 0.0
    BigDecimal qty = 0.0
    String attributeAdjustment = name + "Adjustment".toString()
    String attributeName = "${name}".toString()
    for (bundleSku in api.local.bundleSKUDetails) {
        bundleBaseSkuPrice = 0
        if (bundleSku.value[attributeData] && !out.ExcludeAttributesCache?.contains(bundleSku.value[attributeData])) {
            subComponents = api.local.bomEntries?.findAll { it.sku == bundleSku.value.attribute14 }
            adjustment = 0
            String matchFlag = ""
            for (subComponent in subComponents) {
                componentSKUInfo = api.local.stdProductsCache.find { it.sku == subComponent.subComponentSku }
                baseSKUInfo = api.local.baseSkuDetails?.find { it.attribute1 == componentSKUInfo?.attribute14 }
                basePrice = baseSKUInfo?.attribute6 ?: 0 as BigDecimal
                discount = subComponent.discount ?: 0 as BigDecimal
                qty = subComponent.quantity ?: 0 as BigDecimal
                discountedBasePrice = (basePrice * discount * qty)
                priceForCalculation = discountedBasePrice ?: (basePrice * qty) as BigDecimal
                bundleBaseSkuPrice += priceForCalculation as BigDecimal
                keyLists = getAdjustmentsFromCacheForBundle(adjustmentCache, bundleSku.value, baseSKUInfo)
                keyLists?.BaseKeys?.unique()
                keyLists?.StdKeys?.unique()
                SKUInfo.standardSKUProduct = componentSKUInfo.attribute1
                SKUInfo.standardSKUOffer = componentSKUInfo.attribute2
                SKUInfo.standardSKUBase = componentSKUInfo.attribute4
                SKUInfo.baseSKUProduct = baseSKUInfo.attribute3
                SKUInfo.baseSKUOffer = baseSKUInfo.attribute4
                SKUInfo.baseSKUBase = baseSKUInfo.attribute5
                componentDetail = calculateComponentFactor(keyLists, adjustmentCache, SKUInfo)
                componentFactor = (componentDetail?.adjustment ?: 0) as BigDecimal
                factor = (priceForCalculation * componentFactor) as BigDecimal
                adjustment += factor
                matchFlag = matchFlag ? ((componentDetail?.matchFlag == "Partial Match- Hierarchy") ? componentDetail?.matchFlag : matchFlag) : componentDetail?.matchFlag
            }
            if (bundleBaseSkuPrice) {
                adjustment = (adjustment / bundleBaseSkuPrice)
                if (adjustment != 0) {
                    BigDecimal precisionDigit = Math.pow(10, precision)
                    adjustment = precisionDigit != 0 && precisionDigit ? Math.floor(adjustment * precisionDigit) / precisionDigit : adjustment
                    if (attributeName != "Term" && attributeName != "Currency" && attributeName != "PriceListRegion") {
                        api.local.bundleAdjustments[bundleSku.value.attribute1 + "_" + bundleSku.value.attribute2 + "_" + bundleSku.value.attribute4 + "_" + bundleSku.value[attributeData]] = [
                                "Product"  : bundleSku.value.attribute1,
                                "Offer"    : bundleSku.value.attribute2,
                                "Base"     : bundleSku.value.attribute4,
                                "Factor"   : attributeName,
                                "MatchFlag": matchFlag
                        ]
                        api.local.bundleAdjustments[bundleSku.value.attribute1 + "_" + bundleSku.value.attribute2 + "_" + bundleSku.value.attribute4 + "_" + bundleSku.value[attributeData]].putAt(attributeName, bundleSku.value[attributeData])
                        api.local.bundleAdjustments[bundleSku.value.attribute1 + "_" + bundleSku.value.attribute2 + "_" + bundleSku.value.attribute4 + "_" + bundleSku.value[attributeData]].putAt(attributeAdjustment, adjustment)
                    }
                    if (attributeName == "Term") {
                        api.local.bundleAdjustments[bundleSku.value.attribute1 + "_" + bundleSku.value.attribute2 + "_" + bundleSku.value.attribute4 + "_" + bundleSku.value.attribute6] = [
                                "Factor"        : "Term",
                                "TermAdjustment": adjustment,
                                "Product"       : bundleSku.value.attribute1,
                                "Offer"         : bundleSku.value.attribute2,
                                "Base"          : bundleSku.value.attribute4,
                                "Term"          : bundleSku.value.attribute6,
                                "TermUoM"       : bundleSku.value.attribute21,
                                "MatchFlag"     : matchFlag
                        ]
                    }
                    if (attributeName == "Currency") {
                        api.local.bundleAdjustments[bundleSku.value.attribute1 + "_" + bundleSku.value.attribute2 + "_" + bundleSku.value.attribute4 + "_" + bundleSku.value[attributeData]] = [
                                "Product"           : bundleSku.value.attribute1,
                                "Offer"             : bundleSku.value.attribute2,
                                "Base"              : bundleSku.value.attribute4,
                                "CurrencyAdjustment": adjustment,
                                "Currency"          : bundleSku.value.attribute15,
                                "PriceListRegion"   : bundleSku.value.attribute17,
                                "Factor"            : attributeName,
                                "MatchFlag"         : matchFlag,

                        ]
                    }
                    if (attributeName == "PriceListRegion") {
                        api.local.bundleAdjustments[bundleSku.value.attribute1 + "_" + bundleSku.value.attribute2 + "_" + bundleSku.value.attribute4 + "_" + bundleSku.value[attributeData]] = [
                                "Product"                  : bundleSku.value.attribute1,
                                "Offer"                    : bundleSku.value.attribute2,
                                "Base"                     : bundleSku.value.attribute4,
                                "Currency"                 : bundleSku.value.attribute15,
                                "PriceListRegionAdjustment": adjustment,
                                "PriceListRegion"          : bundleSku.value.attribute17,
                                "Factor"                   : attributeName,
                                "MatchFlag"                : matchFlag,
                        ]
                    }
                }
            }
        }
    }
}
