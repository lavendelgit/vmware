api.local.OSAdjustmentCache = [:]
api.local.OSAdjustmentCache = Lib.cacheAdjustments("attribute36")
Map populatedTermCache = [:]
Map adjustmentCache = [:]
if (api.local.OSAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.OSAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.OSAdjustmentCache?.size() > 0) {
        api.local.OSAdjustmentCache = api.local.OSAdjustmentCache + populatedTermCache
    }
}
return