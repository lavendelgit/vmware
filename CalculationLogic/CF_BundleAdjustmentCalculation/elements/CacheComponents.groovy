List filter = [
        Filter.equal("name", Constants.BOM_PX_TABLE),
        Filter.in("sku", api.local.BundleSKUBaseSKUs?.values() as List)
]
recordStream = api.stream("PX10", null, ["sku", "attribute3", "attribute7", "attribute1", "attribute4", "attribute2"], *filter)
api.local.bomEntries = recordStream?.collect {
    [
            sku             : it.sku,
            quantity        : it.attribute3 as BigDecimal,
            rawMaterial     : it.attribute7,
            subComponentSku : it.attribute1,
            discount        : it.attribute4 as BigDecimal,
            componentBaseSku: it.attribute2 as String
    ]
}
recordStream?.close()
return