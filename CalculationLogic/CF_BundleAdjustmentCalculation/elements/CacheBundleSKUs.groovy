List filter = [
        Filter.equal("name", Constants.STANDARD_PRODUCT_TABLE_NAME),
        Filter.equal("attribute24", Constants.BUNDLE_SKU_TYPE),
        Filter.equal("attribute30", libs.stdProductLib.ConstConfiguration.BUNDLE_CALCULATION_FLAG)
]
recordStream = api.stream("PX50", null, *filter)
api.local.bundleSKUDetails = recordStream?.collectEntries {
    [
            (it.sku): it
    ]
}
recordStream?.close()
return
