api.local.volumeTierAdjustmentCache = [:]
Map populatedTermCache = [:]
api.local.volumeTierAdjustmentCache = Lib.cacheAdjustments("attribute28")
Map adjustmentCache = [:]
if (api.local.volumeTierAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.volumeTierAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.volumeTierAdjustmentCache?.size() > 0) {
        api.local.volumeTierAdjustmentCache = api.local.volumeTierAdjustmentCache + populatedTermCache
    }
}
return