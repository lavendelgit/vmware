api.local.purchasingProgramAdjustmentCache = [:]
api.local.purchasingProgramAdjustmentCache = Lib.cacheAdjustments("attribute19")
Map populatedTermCache = [:]
Map adjustmentCache = [:]
if (api.local.purchasingProgramAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.purchasingProgramAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.purchasingProgramAdjustmentCache?.size() > 0) {
        api.local.purchasingProgramAdjustmentCache = api.local.purchasingProgramAdjustmentCache + populatedTermCache
    }
}
return