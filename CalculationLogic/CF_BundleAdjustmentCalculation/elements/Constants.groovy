import groovy.transform.Field

@Field final String BASE_PRICE_TABLE_NAME = "StandardBasePrice"
@Field final String STANDARD_PRODUCT_TABLE_NAME = "StandardProducts"
@Field final String BUNDLE_SKU_TYPE = "Bundle"
@Field final String STANDARD_ADJUSTMENT_TABLE = "StandardAdjustment"
@Field final String BUNDLE_ADJUSTMENT_TABLE = "BundleAdjustment"
@Field final Map STANDARD_PRODUCT_MAPPING = [
        "Segment Type": "Segment",
        "VPP Level"   : "Purchasing Program"
]
@Field final String BOM_PX_TABLE = "BOM"