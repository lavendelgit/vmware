api.local.supportTierAdjustmentCache = [:]
api.local.supportTierAdjustmentCache = Lib.cacheAdjustments("attribute16")
Map populatedTermCache = [:]
Map adjustmentCache = [:]
if (api.local.supportTierAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.supportTierAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.supportTierAdjustmentCache?.size() > 0) {
        api.local.supportTierAdjustmentCache = api.local.supportTierAdjustmentCache + populatedTermCache
    }
}
return