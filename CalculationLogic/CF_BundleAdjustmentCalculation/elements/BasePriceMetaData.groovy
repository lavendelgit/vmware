api.local.basePriceMetaData = api.find("PXAM", Filter.equal("name", Constants.BASE_PRICE_TABLE_NAME))?.collectEntries {
    [(it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()): it.fieldName]
}