List filter = [
        Filter.equal("name", "StandardProducts"),
        Filter.in("sku", api.local.bomEntries?.collect { it.subComponentSku })
]
recordStream = api.stream("PX50", null, ["sku", "attribute14"], *filter)
api.local.baseSKUCache = recordStream?.collectEntries { [(it.sku): it.attribute14] }
recordStream?.close()
return