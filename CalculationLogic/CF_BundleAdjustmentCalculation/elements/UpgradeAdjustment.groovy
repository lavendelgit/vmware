/*BigDecimal adjustment = 0.0
BigDecimal componentFactor = 0.0
BigDecimal componentBasePrice = 0.0
Map componentSKUInfo = [:]
Map baseSKUInfo = [:]
Map SKUInfo = [:]
BigDecimal bundleBaseSkuPrice = 0.0
api.local.bundleAdjustments = [:]
for (bundleSku in api.local.bundleSKUDetails) {
  //for each bundle
  //get subcomponent detail
  //calculate subcomponent
  bundleBaseSkuPrice = api.local.basePriceDetails?.find {
    it.sku == bundleSku.value.attribute14
  }?.basePrice as BigDecimal
  subComponents = api.local.bomEntries?.findAll { it.sku == bundleSku.key }//?.collect{it.subComponentSku}
  adjustment = 0
  for (subComponent in subComponents) {
    componentSKUInfo = api.local.stdProductsCache.find { it.sku == subComponent.subComponentSku }
    baseSKUInfo = api.local.baseSkuDetails?.find { it.attribute1 == componentSKUInfo?.attribute14 }
    BigDecimal basePrice = baseSKUInfo.attribute6 as BigDecimal
    discountedBasePrice = (basePrice * subComponent.discount * subComponent.quantity) as BigDecimal
    def keyLists = Lib.getAdjustmentsFromCacheForBundle(api.local.upgradeAdjustmentCache, componentSKUInfo, baseSKUInfo)
    keyLists?.BaseKeys?.unique()
    keyLists.StdKeys?.unique()
    SKUInfo.standardSKUProduct = componentSKUInfo.attribute1
    SKUInfo.standardSKUOffer = componentSKUInfo.attribute2
    SKUInfo.standardSKUBase = componentSKUInfo.attribute4
    SKUInfo.baseSKUProduct = baseSKUInfo.attribute3
    SKUInfo.baseSKUOffer = baseSKUInfo.attribute4
    SKUInfo.baseSKUBase = baseSKUInfo.attribute5
    componentFactor = Lib.calculateComponentFactor(keyLists, api.local.upgradeAdjustmentCache, SKUInfo) as BigDecimal
    factor = (discountedBasePrice * componentFactor) as BigDecimal
    factor = factor?.setScale(2, BigDecimal.ROUND_HALF_UP)
    adjustment += factor
  }
  //adjustment = adjustment.setScale(2, BigDecimal.ROUND_HALF_UP)
  if (bundleBaseSkuPrice) {
    adjustment = (adjustment / bundleBaseSkuPrice)
    if (adjustment != 0) {
      api.local.bundleAdjustments[bundleSku.value.attribute1 + "_" + bundleSku.value.attribute2 + "_" + bundleSku.value.attribute4 + "_" + bundleSku.value.attribute3] = [
          "Factor"    : "Upgrade",
          "Adjustment": adjustment
      ]
    }
  }
}
api.trace("api.local.bundleAdjustments", api.local.bundleAdjustments)*/
return