api.local.promotionAdjustmentCache = [:]
Map populatedTermCache = [:]
api.local.promotionAdjustmentCache = Lib.cacheAdjustments("attribute32")
Map adjustmentCache = [:]
if (api.local.promotionAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.promotionAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.promotionAdjustmentCache?.size() > 0) {
        api.local.promotionAdjustmentCache = api.local.promotionAdjustmentCache + populatedTermCache
    }
}
return