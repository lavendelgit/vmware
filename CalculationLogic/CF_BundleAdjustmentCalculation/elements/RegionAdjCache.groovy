api.local.regionAdjustmentCache = [:]
Map populatedTermCache = [:]
api.local.regionAdjustmentCache = Lib.cacheAdjustments("attribute34")
Map adjustmentCache = [:]
if (api.local.regionAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.regionAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.regionAdjustmentCache?.size() > 0) {
        api.local.regionAdjustmentCache = api.local.regionAdjustmentCache + populatedTermCache
    }
}
return