if (!api.global.termUnitConversionCache) {
    api.global.termUnitConversionCache = [:]
    api.global.termUnitConversionCache = api.findLookupTableValues("TermUnitConversion")?.collectEntries {
        [(it.key1 + "_" + it.key2): it.attribute1 as BigDecimal]
    }
}
if (!api.global.termUoMConversionFactorCache) {
    api.global.termUoMConversionFactorCache = [:]
    api.global.termUoMConversionFactorCache = api.findLookupTableValues("TermConversionFactor")?.collectEntries {
        [(it.key1 + "_" + it.key2): it.attribute1 as BigDecimal]
    }
}
String baseTermUoM = api.local.baseTermUoM ?: "-"
String skuTermUoM = out.TermUoM ?: "-"
BigDecimal baseUoMFactor = api.global.termUnitConversionCache[api.local.baseTerm + "_" + baseTermUoM] as BigDecimal
BigDecimal skuUoMFactor = api.global.termUnitConversionCache[out.Term + "_" + skuTermUoM] as BigDecimal
return 1
if (skuUoMFactor && baseUoMFactor) {
    if (skuTermUoM == baseTermUoM) {
        return (skuUoMFactor / baseUoMFactor) as BigDecimal
    }
    def conversionFactor = (api.global.termUoMConversionFactorCache[skuTermUoM + "_" + baseTermUoM] ?: 1) as BigDecimal
    return (skuUoMFactor / (baseUoMFactor * conversionFactor)) as BigDecimal

} else if (!skuUoMFactor) {
    api.addWarning("SKU Term UoM unit conversion factor is not found for " + api.local.skuProduct + "," + api.local.skuOffer + ", " + api.local.skuBase + ", " + out.TermUoM)
} else if (!baseUoMFactor) {
    api.addWarning("SKU Term UoM unit conversion factor is not found for " + api.local.baseProduct + "," + api.local.baseOffer + ", " + api.local.baseSKUBase + ", " + api.local.baseTermUoM)
}
return 1
