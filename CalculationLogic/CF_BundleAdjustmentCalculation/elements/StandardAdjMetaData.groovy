api.local.stdAdjMetaData = api.find("PXAM", Filter.equal("name", Constants.STANDARD_ADJUSTMENT_TABLE))?.collectEntries {
    [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
}
return