api.local.paymentAdjustmentCache = [:]
api.local.paymentAdjustmentCache = Lib.cacheAdjustments("attribute22")
Map populatedTermCache = [:]
Map adjustmentCache = [:]
if (api.local.paymentAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.paymentAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.paymentAdjustmentCache?.size() > 0) {
        api.local.paymentAdjustmentCache = api.local.paymentAdjustmentCache + populatedTermCache
    }
}
return