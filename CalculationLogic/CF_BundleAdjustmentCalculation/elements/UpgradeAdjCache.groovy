api.local.upgradeAdjustmentCache = [:]
Map populatedTermCache = [:]
api.local.upgradeAdjustmentCache = Lib.cacheAdjustments("attribute30")
Map adjustmentCache = [:] as Map
if (api.local.upgradeAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.upgradeAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.upgradeAdjustmentCache?.size() > 0) {
        api.local.upgradeAdjustmentCache = api.local.upgradeAdjustmentCache + populatedTermCache
    }
}
return