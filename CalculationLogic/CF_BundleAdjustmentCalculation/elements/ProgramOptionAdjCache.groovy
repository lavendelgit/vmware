api.local.ProgramOptionAdjustmentCache = [:]
api.local.ProgramOptionAdjustmentCache = Lib.cacheAdjustments("attribute40")
Map populatedTermCache = [:]
Map adjustmentCache = [:]
if (api.local.ProgramOptionAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.ProgramOptionAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.ProgramOptionAdjustmentCache?.size() > 0) {
        api.local.ProgramOptionAdjustmentCache = api.local.ProgramOptionAdjustmentCache + populatedTermCache
    }
}
return