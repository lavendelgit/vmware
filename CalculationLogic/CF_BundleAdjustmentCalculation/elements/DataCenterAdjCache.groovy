api.local.dataCenterAdjustmentCache = [:]
Map populatedTermCache = [:]
api.local.dataCenterAdjustmentCache = Lib.cacheAdjustments("attribute20")
Map adjustmentCache = [:] as Map
if (api.local.dataCenterAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.dataCenterAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.dataCenterAdjustmentCache?.size() > 0) {
        api.local.dataCenterAdjustmentCache = api.local.dataCenterAdjustmentCache + populatedTermCache
    }
}
return
