List filter = [
        Filter.equal("name", Constants.STANDARD_PRODUCT_TABLE_NAME),
        Filter.in("sku", api.local.bomEntries?.collect { it.subComponentSku })

]
recordStream = api.stream("PX50", null, *filter)
api.local.stdProductsCache = recordStream?.collect { it }
recordStream?.close()
return
