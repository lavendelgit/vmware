List filter = [
        Filter.equal("name", Constants.BASE_PRICE_TABLE_NAME),
        Filter.equal("attribute2", Constants.BUNDLE_SKU_TYPE),
        Filter.in("attribute1", api.local.BundleSKUBaseSKUs?.values())

]
recordStream = api.stream("PX50", null, ["attribute1", "attribute6", "attribute23"], *filter)
api.local.basePriceDetails = recordStream?.collect { baseSku ->
    [
            sku      : baseSku.attribute1,
            basePrice: baseSku.attribute23 ?: baseSku.attribute6
    ]
}
recordStream?.close()
return