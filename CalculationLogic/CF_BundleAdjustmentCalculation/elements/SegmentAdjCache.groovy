api.local.segmentAdjustmentCache = [:]
api.local.segmentAdjustmentCache = Lib.cacheAdjustments("attribute18")
Map populatedTermCache = [:]
Map adjustmentCache = [:]
if (api.local.segmentAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.segmentAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.segmentAdjustmentCache?.size() > 0) {
        api.local.segmentAdjustmentCache = api.local.segmentAdjustmentCache + populatedTermCache
    }
}
return