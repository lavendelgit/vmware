List filter = [
        Filter.equal("name", Constants.BASE_PRICE_TABLE_NAME),
        Filter.equal("attribute2", "Standalone"),
        Filter.in("attribute1", api.local.baseSKUCache?.values())

]
recordStream = api.stream("PX50", null, *filter)
api.local.baseSkuDetails = recordStream?.collect { it }
recordStream?.close()
return