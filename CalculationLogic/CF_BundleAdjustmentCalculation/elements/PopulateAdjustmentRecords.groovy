api.local.pxRecords = []
for (adjustmentRecordEntry in api.local.bundleAdjustments) {
    adjustmentRecord = adjustmentRecordEntry.value
    pXParams = [
            "sku"                 : adjustmentRecordEntry.key,
            "Product"             : adjustmentRecord.Product,
            "Offer"               : adjustmentRecord.Offer,
            "DC"                  : adjustmentRecord.DataCenter,
            "Base"                : adjustmentRecord.Base,
            "Metric"              : adjustmentRecord.Metric,
            "Term"                : adjustmentRecord.Term,
            "TermUoM"             : adjustmentRecord.TermUoM,
            "Payment"             : adjustmentRecord.Payment,
            "Tier"                : adjustmentRecord.VolumeTier,
            "Segment"             : adjustmentRecord.Segment,
            "Promotion"           : adjustmentRecord.Promotion,
            "PromotionAdj"        : adjustmentRecord.PromotionAdjustment,
            "OS"                  : adjustmentRecord.OS,
            "Hosting"             : adjustmentRecord.Hosting,
            "ProgramOption"       : adjustmentRecord.ProgramOption,
            "Quantity"            : adjustmentRecord.Quantity,
            "PurchasingProgram"   : adjustmentRecord.PurchasingProgram,
            "SupportType"         : adjustmentRecord.SupportType,
            "SupportTier"         : adjustmentRecord.SupportTier,
            "Currency"            : adjustmentRecord.Currency,
            "PriceListRegion"     : adjustmentRecord.PriceListRegion,
            "Uplift"              : adjustmentRecord.PriceListRegionAdjustment,
            "MetricAdj"           : adjustmentRecord.MetricAdjustment,
            "TermAdj"             : adjustmentRecord.TermAdjustment,
            "SupportTierAdj"      : adjustmentRecord.SupportTierAdjustment,
            "SupportTypeAdj"      : adjustmentRecord.SupportTypeAdjustment,
            "SegmentAdj"          : adjustmentRecord.SegmentAdjustment,
            "PurchasingProgramAdj": adjustmentRecord.PurchasingProgramAdjustment,
            "DCAdj"               : adjustmentRecord.DataCenterAdjustment,
            "PaymentAdj"          : adjustmentRecord.PaymentAdjustment,
            "CurrencyAdj"         : adjustmentRecord.CurrencyAdjustment,
            "TierAdj"             : adjustmentRecord.VolumeTierAdjustment,
            "OSAdj"               : adjustmentRecord.OSAdjustment,
            "HostingAdj"          : adjustmentRecord.HostingAdjustment,
            "ProgramOptionAdj"    : adjustmentRecord.ProgramOptionAdjustment,
            "QuantityAdj"         : adjustmentRecord.QuantityAdjustment,
            "MatchFlag"           : adjustmentRecord.MatchFlag
    ]
    api.local.pxRecords << Lib.populateAdjustmentRecord("BundleAdjustment", pXParams)
}
return