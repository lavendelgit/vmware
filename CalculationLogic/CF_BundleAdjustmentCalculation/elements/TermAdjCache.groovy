api.local.termAdjustmentCache = [:]
Map populatedTermCache = [:]
api.local.termAdjustmentCache = Lib.cacheAdjustments("attribute15")
Map adjustmentCache = [:]
if (api.local.termAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.termAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.termAdjustmentCache?.size() > 0) {
        api.local.termAdjustmentCache = api.local.termAdjustmentCache + populatedTermCache
    }
}
return