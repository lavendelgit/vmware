if (!api.local.stdAdjustmentsCache) {
    List filter = [
            Filter.equal("name", Constants.STANDARD_ADJUSTMENT_TABLE)
    ]
    stdAdjustmentStream = api.stream("PX50", "attribute41", *filter)
    api.local.stdAdjustmentsCache = stdAdjustmentStream?.collect { row -> row }
    stdAdjustmentStream?.close()
}
return
