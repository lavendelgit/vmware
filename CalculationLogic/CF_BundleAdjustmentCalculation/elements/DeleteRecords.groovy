if (api.local.pxRecords) {
    def deletePL = [
            "data": [
                    "filterCriteria": [
                            "operator"    : "and",
                            "_constructor": "AdvancedCriteria",
                            "criteria"    : [
                                    [
                                            "fieldName": "name",
                                            "operator" : "equals",
                                            "value"    : Constants.BUNDLE_ADJUSTMENT_TABLE
                                    ],
                                    [
                                            "fieldName": "sku",
                                            "operator" : "inSet",
                                            "value"    : api.local.bundleAdjustments.keySet() as List
                                    ]
                            ]
                    ]
            ]
    ]
    api.boundCall("boundcall", "delete/PX/batch", api.jsonEncode(deletePL), false)

    return
}