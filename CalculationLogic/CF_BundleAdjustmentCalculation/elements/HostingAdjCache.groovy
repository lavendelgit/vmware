api.local.HostingAdjustmentCache = [:]
api.local.HostingAdjustmentCache = Lib.cacheAdjustments("attribute38")
Map populatedTermCache = [:]
Map adjustmentCache = [:]
if (api.local.HostingAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.HostingAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.HostingAdjustmentCache?.size() > 0) {
        api.local.HostingAdjustmentCache = api.local.HostingAdjustmentCache + populatedTermCache
    }
}
return