api.local.currencyAdjustmentCache = [:]
Map populatedTermCache = [:]
api.local.currencyAdjustmentCache = Lib.cacheAdjustments("attribute24")
Map adjustmentCache = [:]
if (api.local.currencyAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.currencyAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.currencyAdjustmentCache?.size() > 0) {
        api.local.currencyAdjustmentCache = api.local.currencyAdjustmentCache + populatedTermCache
    }
}
return

