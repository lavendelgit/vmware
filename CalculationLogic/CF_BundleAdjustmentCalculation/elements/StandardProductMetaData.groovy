api.local.standardProductMetaData = api.find("PXAM", Filter.equal("name", Constants.STANDARD_PRODUCT_TABLE_NAME))?.collectEntries {
    [(it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()): it.fieldName]
}