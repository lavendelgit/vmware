api.local.metricAdjustmentCache = [:]
api.local.metricAdjustmentCache = Lib.cacheAdjustments("attribute14")
Map populatedTermCache = [:]
Map adjustmentCache = [:]
if (api.local.metricAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.metricAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = Lib.populateTermAltUoMAdjustments(adjustmentCache)
    if (populatedTermCache?.size() > 0 && api.local.metricAdjustmentCache?.size() > 0) {
        api.local.metricAdjustmentCache = api.local.metricAdjustmentCache + populatedTermCache
    }
}
return