import java.text.DateFormat
import java.text.SimpleDateFormat


api.local.plData = []
BigDecimal prevRecordPrice = 0.0
if (api.local.skuPrices) {
    Map record = [:]
    DateFormat df2 = new SimpleDateFormat("MMM-yyyy", Locale.US)
    List months = []
    for (sku in api.local.skuPrices) {
        prevRecordPrice = 0.0
        costs = sku.value?.PriceData?.sort { it.StartDate }
        for (cost in costs) {
            startDate = cost.StartDate
            endDate = cost.EndDate
            months = Lib.getListMonths(startDate, endDate, Locale.US, df2)

            for (month in months) {
                api.local.plData << Lib.generateRecords(sku, month, cost, out.Currency)
            }

        }


    }
}
return