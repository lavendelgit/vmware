if (api.local.groupedPriceBookData) {
    api.local.skuPrices = [:]
    List data
    for (skuData in api.local.groupedPriceBookData) {
        value = skuData.value
        if (!api.local.skuPrices[value?.PartNumber?.getAt(0)]) {
            api.local.skuPrices[value?.PartNumber?.getAt(0)] = [
                    "PlatformGroup"  : value?.PlatformGroup?.getAt(0),
                    "ProductPlatform": value?.ProductPlatform?.getAt(0),
                    "ProductFamily"  : value?.ProductFamily?.getAt(0),
                    "Product"        : value?.Product?.getAt(0),
            ]
            data = []
            for (priceData in value) {
                data << [
                        "StartDate"        : priceData?.StartDate,
                        "EndDate"          : priceData?.EndDate,
                        "ListPriceUSD"     : priceData.USD,
                        "ListPriceEUR"     : priceData.EUR,
                        "ListPriceJPY"     : priceData.JPY,
                        "ListPriceCNY"     : priceData.CNY,
                        "ListPriceAUD"     : priceData.AUD,
                        "ListPriceGBP"     : priceData.GBP,
                        "ListPriceEMEAUSD" : priceData.EMEAUSD,
                        "ListPriceEMEAUSD2": priceData.EMEAUSD2,
                        "ListPriceGUSD"    : priceData.GUSD

                ]
                api.local.skuPrices[value?.PartNumber?.getAt(0)]?.PriceData = data
            }
        }
    }
}

return