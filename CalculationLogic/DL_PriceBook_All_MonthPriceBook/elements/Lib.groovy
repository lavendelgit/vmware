import java.text.DateFormat

List getListMonths(Date dateFrom, Date dateTo, Locale locale, DateFormat df) {
    Calendar calendar = Calendar.getInstance(locale)
    calendar.setTime(dateFrom)
    List months = []
    while (calendar.getTime().getTime() <= dateTo.getTime()) {
        dateString = df.format(calendar.getTime())
        dateMonth = Date.parse("MMM-yyyy", dateString).format("MMM-yyyy")
        months.add([
                priceMonthDate: dateMonth,
                priceMonth    : dateString
        ]
        )
        calendar.add(Calendar.MONTH, 1)

    }
    return months
}

Map generateRecords(Object skuDetails, def month, def cost, String currency) {
    return [
            "SKU"              : skuDetails?.key,
            "PlatformGroup"    : skuDetails?.value?.PlatformGroup,
            "ProductPlatform"  : skuDetails?.value?.ProductPlatform,
            "ProductFamily"    : skuDetails?.value?.ProductFamily,
            "Product"          : skuDetails?.value?.Product,
            "Currency"         : currency,
            "PriceMonth"       : month.priceMonth,
            "PriceMonthDate"   : month.priceMonthDate,
            "ListPriceUSD"     : cost.ListPriceUSD,
            "ListPriceEUR"     : cost.ListPriceEUR,
            "ListPriceJPY"     : cost.ListPriceJPY,
            "ListPriceCNY"     : cost.ListPriceCNY,
            "ListPriceAUD"     : cost.ListPriceAUD,
            "ListPriceGBP"     : cost.ListPriceGBP,
            "ListPriceEMEAUSD" : cost.ListPriceEMEAUSD,
            "ListPriceEMEAUSD2": cost.ListPriceEMEAUSD2,
            "ListPriceGUSD"    : cost.ListPriceGUSD,
            "StartDate"        : cost.StartDate,
            "EndDate"          : cost.EndDate,
            "PriceChangeDate"  : cost.StartDate,

    ]

}