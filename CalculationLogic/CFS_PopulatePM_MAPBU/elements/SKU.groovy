def currObj = out.CurrentObject
def sku
sku = (currObj) ? currObj.sku : null
if (sku) return sku
