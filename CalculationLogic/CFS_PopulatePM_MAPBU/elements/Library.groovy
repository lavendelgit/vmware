def populateProductMaster(sku, shortDesc, baseSKU, productTypeSku, productTypeBaseSku) {

    def mapbuRecord = [
            sku        : sku,
            label      : shortDesc,
            attribute10: baseSKU,
            attribute11: productTypeSku,
            currency   : productTypeBaseSku

    ]
    if (mapbuRecord) api.addOrUpdate("P", mapbuRecord)
}