def sku = out.SKU
def shortDesc = out.ShortDesc
def baseSKU = out.BaseSKU
def productTypeSku = out.ProductType
def productTypeBaseSku = out.BaseType

if (sku) Library.populateProductMaster(sku, shortDesc, baseSKU, productTypeSku, productTypeBaseSku)

