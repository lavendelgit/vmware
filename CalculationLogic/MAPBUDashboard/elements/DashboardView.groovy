if (api.isSyntaxCheck()) api.abortCalculation()


def serviceList = out.Service
def datacenterList = out.Datacenter
def quantityList = out.Quantity
def subServiceList = out.SubService
def pmtTermList = out.PaymentTerm
def pmtTypeList = out.PaymentType
def tierList = out.Tier
def licenseList = out.License
def versionList = out.Version
def productTypeList = out.ProductType

serviceList = (serviceList) ?: null
datacenterList = (datacenterList) ?: null
quantityList = (quantityList) ?: null
subServiceList = (subServiceList) ?: null
pmtTermList = (pmtTermList) ?: null
pmtTypeList = (pmtTypeList) ?: null
tierList = (tierList) ?: null
licenseList = (licenseList) ?: null
versionList = (versionList) ?: null
productTypeList = (productTypeList) ?: null

filter = [
        Filter.in("name", "MAPBUProducts"),
        Filter.in("attribute1", serviceList),
        Filter.in("attribute2", datacenterList),
        Filter.in("attribute3", quantityList),
        Filter.in("attribute4", subServiceList),
        Filter.in("attribute5", pmtTermList),
        Filter.in("attribute6", pmtTypeList),
        Filter.in("attribute7", tierList),
        Filter.in("attribute8", licenseList),
        Filter.in("attribute9", versionList),
        Filter.in("attribute10", productTypeList)
]
recFullList = api.stream("PX20", "sku", *filter)

def resultMatrix = api.newMatrix("SKU Number", "SKU Short Description", "SKU Long Description", "Base SKU",
        "Segment", "Metric", "Service", "Data Center", "Quantity", "Sub Service", "Payment Term", "Term in Years", "Payment Type",
        "Tier", "License Class", "Version", "Type")

recFullList.each {
    def row = [:]
    row.put("SKU Number", it.sku)
    row.put("SKU Short Description", it.attribute11)
    row.put("SKU Long Description", it.attribute12)
    row.put("Base SKU", it.attribute16)
    row.put("Segment", it.attribute14)
    row.put("Metric", it.attribute15)
    row.put("Service", it.attribute1)
    row.put("Data Center", it.attribute2)
    row.put("Quantity", it.attribute3)
    row.put("Sub Service", it.attribute4)
    row.put("Payment Term", it.attribute5)
    row.put("Term in Years", it.attribute13)
    row.put("Payment Type", it.attribute6)
    row.put("Tier", it.attribute7)
    row.put("License Class", it.attribute8)
    row.put("Version", it.attribute9)
    row.put("Type", it.attribute10)

    resultMatrix.addRow(row)
}

resultMatrix?.setEnableClientFilter(true)
return resultMatrix