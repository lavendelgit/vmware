def fethDataFromPPTable(tableName, BU) {
    def table = api.findLookupTable(tableName)
    filter = [Filter.equal("lookupTable.id", table.id),
              Filter.equal("key1", "CN")]
    ppList = api.find("MLTV2", *filter).collect { it.key2 }
    return ppList
}