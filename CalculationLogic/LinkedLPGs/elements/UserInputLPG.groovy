/*
Fetches all LPGs so user can decide which non-bundle LPG will
be linked to the bundle LPG
*/

def LPGs = api.find("PG").label.sort()
return api.option("Linked LPG", LPGs)