if (out.DashBoardConfiguration && out.StartYear && out.EndYear && out.Region && (out.StartYear < out.EndYear)) {
    api.local.queriedPriceBook = []
    List filter = []
    out.ConstantConfiguration.FILTER_COLUMN_OPTION?.each { columnName ->
        if (api.userEntry(columnName))
            filter << Filter.in(columnName, api.userEntry(columnName))
    }
    filter << Filter.greaterOrEqual(out.ConstantConfiguration.COLUMN_END_DATE, out.StartYear)
    filter << Filter.lessOrEqual(out.ConstantConfiguration.COLUMN_START_DATE, out.EndYear)
    filter << Filter.in(Constants.COLUMN_REGION, out.Region)
    def ctx = api.getDatamartContext()
    def dm = ctx.getDataSource(Constants.DATA_SOURCE)
    def query = ctx?.newQuery(dm)
            ?.select(out.ConstantConfiguration.COLUMN_PART_NUMBER, out.ConstantConfiguration.COLUMN_PART_NUMBER)
            ?.select(out.ConstantConfiguration.COLUMN_PLATFORM_GROUP, out.ConstantConfiguration.COLUMN_PLATFORM_GROUP)
            ?.select(out.ConstantConfiguration.COLUMN_PRODUCT_PLATFORM, out.ConstantConfiguration.COLUMN_PRODUCT_PLATFORM)
            ?.select(out.ConstantConfiguration.COLUMN_PRODUCT_FAMILY, out.ConstantConfiguration.COLUMN_PRODUCT_FAMILY)
            ?.select(out.ConstantConfiguration.PRODUCT, out.ConstantConfiguration.PRODUCT)
            ?.select(Constants.COLUMN_SKU_TYPE, Constants.COLUMN_SKU_TYPE)
            ?.select(out.ConstantConfiguration.CURRENCY, out.ConstantConfiguration.CURRENCY)
            ?.select(Constants.REGION, Constants.REGION)
            ?.select(out.ConstantConfiguration.COLUMN_START_DATE, out.ConstantConfiguration.COLUMN_START_DATE)
            ?.select(out.ConstantConfiguration.COLUMN_END_DATE, out.ConstantConfiguration.COLUMN_END_DATE)
            ?.select(Constants.COLUMN_LIST_PRICE, Constants.COLUMN_LIST_PRICE)
            ?.select(Constants.DISCOUNT, Constants.DISCOUNT)
            ?.select(Constants.COLUMN_NET_COST, Constants.COLUMN_NET_COST)
            ?.select(Constants.COLUMN_ADV_DEAL_BAND1, Constants.COLUMN_ADV_DEAL_BAND1)
            ?.select(Constants.COLUMN_ADV_DEAL_BAND2, Constants.COLUMN_ADV_DEAL_BAND2)
            ?.where(*filter)
            ?.orderBy(out.ConstantConfiguration.COLUMN_PART_NUMBER)
    streamResult = ctx?.streamQuery(query)
    while (streamResult?.next()) {
        api.local.queriedPriceBook << streamResult.get()
    }
    streamResult?.close()
}
return
