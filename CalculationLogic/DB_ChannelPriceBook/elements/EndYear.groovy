endYear = api.dateUserEntry(out.ConstantConfiguration.INPUT_END_YEAR)
parameter = api.getParameter(out.ConstantConfiguration.INPUT_END_YEAR)
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
}
return endYear