import java.text.DateFormat
import java.text.SimpleDateFormat

if (api.local.skuPrices) {
    api.local.resultMatrix = api.newMatrix(out.ConstantConfiguration.PLATFORM_GROUP, out.ConstantConfiguration.PRODUCT_PLATFORM, out.ConstantConfiguration.PRODUCT_FAMILY, out.ConstantConfiguration.PRODUCT, out.ConstantConfiguration.PART_NUMBER, Constants.REGION, out.ConstantConfiguration.CURRENCY, Constants.SKU_TYPE)
    api.local.resultMatrix?.setEnableClientFilter(true)
    if (out.DashBoardConfiguration == out.ConstantConfiguration.MONTHLY_PRICE_PROJECTION) {
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(out.EndYear)
        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(out.StartYear)
        DateFormat df2 = new SimpleDateFormat("MMM-yyyy", Locale.US)
        endYear = endDate?.format("yyyy") as Integer
        monthList = libs.vmwareUtil.PopulateMonths.getListMonths(startDate, endDate, Locale.US, df2, endYear) as List
        for (month in monthList) {
            api.local.resultMatrix?.withColumn(month + Constants.LIST_PRICE)
            api.local.resultMatrix?.withColumn(month + Constants.DISCOUNT)
            api.local.resultMatrix?.withColumn(month + Constants.NET_COST)
            api.local.resultMatrix?.withColumn(month + Constants.ADV_DEAL_BAND1)
            api.local.resultMatrix?.withColumn(month + Constants.ADV_DEAL_BAND2)
        }
    } else if (out.DashBoardConfiguration == out.ConstantConfiguration.PRICE_EFFECTIVE_MONTH_PROJECTION) {
        api.local.resultMatrix?.addColumns([Constants.LIST_PRICE, Constants.DISCOUNT, Constants.NET_COST, Constants.ADV_DEAL_BAND1, Constants.ADV_DEAL_BAND2, out.ConstantConfiguration.PRICE_MONTH, Constants.CHANGED_FIELDS])
    } else if (out.DashBoardConfiguration == out.ConstantConfiguration.ALL_PRICE_EFFECTIVE_PROJECTION) {
        api.local.resultMatrix?.addColumns([Constants.LIST_PRICE, Constants.DISCOUNT, Constants.NET_COST, Constants.ADV_DEAL_BAND1, Constants.ADV_DEAL_BAND2, out.ConstantConfiguration.PRICE_MONTH])
    }
}
return