def ctx = api.getDatamartContext()
def ds = ctx.getDataSource(Constants.DATA_SOURCE)
List options = out.ConstantConfiguration.FILTER_COLUMN_OPTION
options << Constants.COLUMN_SKU_TYPE
options?.each { columnName ->
    def query = ctx.newQuery(ds)
    query.selectDistinct()
    query.select(columnName)
    query.orderBy(columnName)
    def data = ctx.executeSqlQuery("SELECT $columnName FROM t1", query)
    api.options(columnName, data?.getValue())
}
