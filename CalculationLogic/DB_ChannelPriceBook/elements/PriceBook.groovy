if (out.StartYear && out.EndYear && out.Region && (out.StartYear < out.EndYear)) {
    if (out.DashBoardConfiguration == out.ConstantConfiguration.MONTHLY_PRICE_PROJECTION) {
        Map monthWisePBMap = api.local.plData?.groupBy { [it.getAt("Part Number"), it.getAt("Currency"), it.getAt("Region"), it.getAt("List Price")] }
        List monthWisePB = []
        monthWisePBMap?.each { key, value ->
            Map monthWisePBRecords = [:]
            for (monthWisePBValue in value) {
                monthWisePBRecords.putAll(monthWisePBValue)
            }
            monthWisePB += monthWisePBRecords
        }
        api.local.resultMatrix?.withRows(monthWisePB)
    } else if (out.DashBoardConfiguration == out.ConstantConfiguration.PRICE_EFFECTIVE_MONTH_PROJECTION) {
        Map monthWisePBMap = api.local.plData?.groupBy { [it.getAt("Part Number"), it.getAt("Currency"), it.getAt("Region"), it.getAt("List Price")] }
        List monthWisePB = []
        monthWisePBMap?.each { key, value ->
            Map monthWisePBRecords = value[0]
            monthWisePB += monthWisePBRecords
        }
        api.local.resultMatrix?.withRows(monthWisePB)
    } else {
        api.local.plData?.each { data ->
            api.local.resultMatrix?.addRow(data)
        }
    }
    return api.local.resultMatrix
} else {
    api.local.resultMatrix = api.newMatrix("")
    warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.WARNING, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    api.local.resultMatrix?.addRow(warningCell)
    if (out.StartYear > out.EndYear) {
        warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.YEAR_WARNING_MESSAGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
        api.local.resultMatrix?.addRow(warningCell)
    } else {
        warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.ALL_INPUTS_WARNING_MESSAGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
        api.local.resultMatrix?.addRow(warningCell)
    }
    return api.local.resultMatrix
}