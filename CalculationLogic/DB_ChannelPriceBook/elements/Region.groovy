def ctx = api.getDatamartContext()
def ds = ctx.getDataSource(Constants.DATA_SOURCE)
def query = ctx.newQuery(ds)
        .select("Region")
def result = ctx.executeQuery(query)
def region = result?.getData()?.collect { it.Region } as List
regionOption = api.options(Constants.REGION, region)
parameter = api.getParameter(Constants.REGION)
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
}
return regionOption