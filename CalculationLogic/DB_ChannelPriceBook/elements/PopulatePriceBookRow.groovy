import java.text.DateFormat
import java.text.SimpleDateFormat

api.local.plData = []
boolean changeIndicator
changeField = []
BigDecimal prevRecordPrice = 0.0
if (api.local.skuPrices) {
    Map record = [:]
    DateFormat df2 = new SimpleDateFormat("MMM-yyyy", Locale.US)
    Date userEndDate = new SimpleDateFormat("yyyy-MM-dd").parse(out.EndYear)
    Date userStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(out.StartYear)
    userEndYear = userEndDate.format("yyyy") as Integer
    List months = []
    for (sku in api.local.skuPrices) {
        prevListPrice = 0.0
        prevDiscount = 0.0
        prevNetCost = 0.0
        prevAdvDealBand1 = 0.0
        prevAdvDealBand2 = 0.0
        previousCurrency = ''
        previousRegion = ''
        record = [(out.ConstantConfiguration.PART_NUMBER)     : sku?.key,
                  (out.ConstantConfiguration.PLATFORM_GROUP)  : sku?.value?.PlatformGroup,
                  (out.ConstantConfiguration.PRODUCT_PLATFORM): sku?.value?.ProductPlatform,
                  (out.ConstantConfiguration.PRODUCT_FAMILY)  : sku?.value?.ProductFamily,
                  (out.ConstantConfiguration.PRODUCT)         : sku?.value?.Product,
                  (Constants.SKU_TYPE)                        : sku?.value?.SKUType
        ]
        costs = (sku?.value?.priceData?.sort { it.StartDate })
        count = 0
        for (cost in costs) {
            record.Currency = cost?.Currency
            record.Region = cost?.Region
            listPrice = cost?.ListPrice ?: 0
            discount = cost?.Discount ?: 0
            netCost = cost?.NetCost ?: 0
            advDealBand1 = cost?.AdvDealBand1 ?: 0
            advDealBand2 = cost?.AdvDealBand2 ?: 0
            startDate = cost?.StartDate < userStartDate ? userStartDate : cost?.StartDate
            endDate = cost?.EndDate > userEndDate ? userEndDate : cost?.EndDate
            months = libs.vmwareUtil.PopulateMonths.getListMonths(startDate, endDate, Locale.US, df2, userEndYear)
            if (out.DashBoardConfiguration == out.ConstantConfiguration.MONTHLY_PRICE_PROJECTION) {
                for (month in months) {
                    record[month + Constants.LIST_PRICE] = cost?.ListPrice
                    record[month + Constants.DISCOUNT] = cost?.Discount
                    record[month + Constants.NET_COST] = cost?.NetCost
                    record[month + Constants.ADV_DEAL_BAND1] = cost?.AdvDealBand1
                    record[month + Constants.ADV_DEAL_BAND2] = cost?.AdvDealBand2
                }
            } else if (out.DashBoardConfiguration == out.ConstantConfiguration.PRICE_EFFECTIVE_MONTH_PROJECTION) {
                for (month in months) {
                    changeIndicator = false
                    if (prevListPrice != listPrice && listPrice) {
                        prevListPrice = cost?.ListPrice
                        changeIndicator = true
                        changeField << Constants.COLUMN_LIST_PRICE
                    }
                    if (prevDiscount != discount && discount) {
                        prevDiscount = cost?.Discount
                        changeIndicator = true
                        changeField << Constants.DISCOUNT
                    }
                    if (prevNetCost != netCost && netCost) {
                        prevNetCost = cost?.NetCost
                        changeIndicator = true
                        changeField << Constants.COLUMN_NET_COST
                    }
                    if (prevAdvDealBand1 != advDealBand1 && advDealBand1) {
                        prevAdvDealBand1 = cost?.AdvDealBand1
                        changeIndicator = true
                        changeField << Constants.COLUMN_ADV_DEAL_BAND1
                    }
                    if (prevAdvDealBand2 != advDealBand2 && advDealBand2) {
                        prevAdvDealBand2 = cost?.AdvDealBand2
                        changeIndicator = true
                        changeField << Constants.COLUMN_ADV_DEAL_BAND2
                    }
                    if (changeField as Set == Constants.FIELD_OPTIONS as Set) {
                        changeField = ["ALL"]
                    }
                    if (changeIndicator == true) {
                        if (month) {
                            count = count + 1
                        }
                        if (count == 1 || previousRegion != cost?.Region) {
                            previousRegion = cost?.Region
                            changeFieldString = ""
                        } else {
                            changeFieldString = changeField.join(", ")
                        }
                        api.local.plData << Lib.generateRecords(sku, month, cost, changeFieldString)
                        changeField = []
                    }
                }
            } else if (out.DashBoardConfiguration == out.ConstantConfiguration.ALL_PRICE_EFFECTIVE_PROJECTION) {
                for (month in months) {
                    api.local.plData << Lib.generateRecords(sku, month, cost)
                    changeField = []
                    previousCurrency = cost?.Currency
                    previousRegion = cost?.Region
                }
            }
            if (out.DashBoardConfiguration == out.ConstantConfiguration.MONTHLY_PRICE_PROJECTION) {
                api.local.plData << record
                record = [:]
                record = [(out.ConstantConfiguration.PART_NUMBER)     : sku?.key,
                          (out.ConstantConfiguration.PLATFORM_GROUP)  : sku?.value?.PlatformGroup,
                          (out.ConstantConfiguration.PRODUCT_PLATFORM): sku?.value?.ProductPlatform,
                          (out.ConstantConfiguration.PRODUCT_FAMILY)  : sku?.value?.ProductFamily,
                          (out.ConstantConfiguration.PRODUCT)         : sku?.value?.Product,
                          (Constants.SKU_TYPE)                        : sku?.value?.SKUType
                ]
            }
        }
    }
}
return