import groovy.transform.Field


@Field final String DATA_SOURCE = "ChannelPriceBook"
@Field final String COLUMN_LIST_PRICE = "ListPrice"
@Field final String COLUMN_NET_COST = "NetCost"
@Field final String COLUMN_DISCOUNT = "Discount"
@Field final String COLUMN_ADV_DEAL_BAND1 = "AdvDealBand1"
@Field final String COLUMN_ADV_DEAL_BAND2 = "AdvDealBand2"
@Field final String COLUMN_SKU_TYPE = "SKUType"
@Field final String COLUMN_REGION = "Region"
@Field final String REGION = "Region"
@Field final String SKU_TYPE = "SKU Type"
@Field final String LIST_PRICE = " List Price"
@Field final String DISCOUNT = "Discount"
@Field final String NET_COST = " Net Cost"
@Field final String ADV_DEAL_BAND1 = " Adv+DealBand1"
@Field final String ADV_DEAL_BAND2 = " Adv+DealBand2"
@Field final String CHANGED_FIELDS = "Fields Changed"
@Field final int PRECISION = 2
@Field final List FIELD_OPTIONS = [COLUMN_LIST_PRICE, DISCOUNT, COLUMN_NET_COST, COLUMN_ADV_DEAL_BAND1, COLUMN_ADV_DEAL_BAND2]

