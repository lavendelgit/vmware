if (api.local.groupedPriceBookData) {
    api.local.skuPrices = [:]
    List data
    for (skuData in api.local.groupedPriceBookData) {
        value = skuData?.value
        if (!api.local.skuPrices[value?.PartNumber?.getAt(0)]) {
            api.local.skuPrices[value?.PartNumber?.getAt(0)] = [
                    (out.ConstantConfiguration.COLUMN_PLATFORM_GROUP)  : value?.PlatformGroup?.getAt(0),
                    (out.ConstantConfiguration.COLUMN_PRODUCT_PLATFORM): value?.ProductPlatform?.getAt(0),
                    (out.ConstantConfiguration.COLUMN_PRODUCT_FAMILY)  : value?.ProductFamily?.getAt(0),
                    (out.ConstantConfiguration.COLUMN_PRODUCT)         : value?.Product?.getAt(0),
                    (Constants.COLUMN_SKU_TYPE)                        : value?.SKUType?.getAt(0),
            ]
            data = []
            for (priceData in value) {
                data << [
                        (out.ConstantConfiguration.COLUMN_START_DATE): priceData?.StartDate,
                        (out.ConstantConfiguration.COLUMN_END_DATE)  : priceData?.EndDate,
                        (out.ConstantConfiguration.COLUMN_CURRENCY)  : priceData?.Currency,
                        (Constants.COLUMN_REGION)                    : priceData?.Region,
                        (Constants.COLUMN_LIST_PRICE)                : priceData?.ListPrice,
                        (Constants.COLUMN_DISCOUNT)                  : priceData?.Discount,
                        (Constants.COLUMN_NET_COST)                  : priceData?.NetCost,
                        (Constants.COLUMN_ADV_DEAL_BAND1)            : priceData?.AdvDealBand1,
                        (Constants.COLUMN_ADV_DEAL_BAND2)            : priceData?.AdvDealBand2
                ]
                api.local.skuPrices[value?.PartNumber?.getAt(0)]?.priceData = data
            }
        }
    }
}
return