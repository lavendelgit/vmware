Map generateRecords(Object skuDetails, String month, Map cost, String changeField = null) {
    Map priceRecord = [
            (out.ConstantConfiguration.PART_NUMBER)     : skuDetails?.key,
            (out.ConstantConfiguration.PLATFORM_GROUP)  : skuDetails?.value?.PlatformGroup,
            (out.ConstantConfiguration.PRODUCT_PLATFORM): skuDetails?.value?.ProductPlatform,
            (out.ConstantConfiguration.PRODUCT_FAMILY)  : skuDetails?.value?.ProductFamily,
            (out.ConstantConfiguration.PRODUCT)         : skuDetails?.value?.Product,
            (Constants.SKU_TYPE)                        : skuDetails?.value?.SKUType,
            (out.ConstantConfiguration.CURRENCY)        : cost?.Currency,
            (Constants.REGION)                          : cost?.Region,
            (out.ConstantConfiguration.LIST_PRICE)      : cost?.ListPrice?.setScale(Constants.PRECISION, BigDecimal.ROUND_HALF_UP),
            (Constants.DISCOUNT)                        : cost?.Discount?.setScale(Constants.PRECISION, BigDecimal.ROUND_HALF_UP),
            (Constants.NET_COST)                        : cost?.NetCost?.setScale(Constants.PRECISION, BigDecimal.ROUND_HALF_UP),
            (Constants.ADV_DEAL_BAND1)                  : cost?.AdvDealBand1?.setScale(Constants.PRECISION, BigDecimal.ROUND_HALF_UP),
            (Constants.ADV_DEAL_BAND2)                  : cost?.AdvDealBand2?.setScale(Constants.PRECISION, BigDecimal.ROUND_HALF_UP),
            (out.ConstantConfiguration.PRICE_MONTH)     : month,
            (Constants.CHANGED_FIELDS)                  : changeField]
}