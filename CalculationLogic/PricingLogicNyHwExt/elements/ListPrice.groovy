BigDecimal hwPrice = 0.0
hwPrice = out.BasePrice * out.ReplacementServiceFactor * out.SegmentAdjustment
hwPrice = out.YearFactor ? (hwPrice * out.YearFactor) : hwPrice
hwPrice = out.LongevityFactor ? (hwPrice * out.LongevityFactor) : hwPrice
hwPrice = hwPrice.setScale(0, BigDecimal.ROUND_UP)
return hwPrice
