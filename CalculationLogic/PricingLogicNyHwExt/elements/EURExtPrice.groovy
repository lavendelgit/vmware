BigDecimal hwPrice = out.ExtReplacementPrice * libs.vmwareUtil.CacheManager.getEURLCFactor(out.LCFactor)
return hwPrice.setScale(0, BigDecimal.ROUND_UP)
