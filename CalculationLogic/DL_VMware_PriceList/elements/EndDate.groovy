def source = api.getDatamartRowSet('source')
Map currentRow = source?.getCurrentRow()
Map previousRow
if (source?.previous()) {
    previousRow = source?.getCurrentRow()
}
if (previousRow && currentRow?.PartNumber == previousRow?.PartNumber) {
    return previousRow?.PriceChangeEffectiveDate - 1
} else {
    return api.parseDate("yyyy-MM-dd", "2030-12-31")
}