api.local.skuInfo = getSKUInfo()
return

Map getSKUInfo() {
    Map skuInfos = [:]
    String extensionName = "SAPBaseSKU"
    List filter = [
            Filter.equal("name", extensionName),
            Filter.greaterOrEqual(Constants.PX_TABLE_SAPBASESKU_LASTUPDATEDATE, out.CFLastUpdateDateTime)
    ]
    def skuStream = api.stream("PX10", "sku", *filter)
    skuInfos = skuStream?.collectEntries {
        skuInfo ->
            [(skuInfo.sku): [
                    "ShortDesc": skuInfo.attribute1,
                    "ItemType" : Constants.SAP_SKU
            ]]
    }
    skuStream?.close()
    return skuInfos
}
