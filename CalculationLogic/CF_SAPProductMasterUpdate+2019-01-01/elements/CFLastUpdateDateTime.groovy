List filter = [Filter.equal(Constants.CF_STATUS, Constants.CF_STATUS_VALUE),
               Filter.equal(Constants.CF_UNIQUE_NAME, Constants.SAP_SKU_LABEL)]
List lastUpdatedDate = api.find("CF", 0, 1, "-lastUpdateDate", *filter)?.collect { it.lastUpdateDate }
return lastUpdatedDate