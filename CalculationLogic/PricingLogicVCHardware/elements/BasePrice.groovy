def baseDetails = libs.vmwareUtil.CacheManager.getBaseDetails(api.global.basePriceListMap, out.ProductType, out.HWModel)
return baseDetails ? baseDetails.attribute1 : 0.0 as BigDecimal
