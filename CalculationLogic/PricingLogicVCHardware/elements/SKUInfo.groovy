def sku = out.SKU
def vcHardPXObj = api.global.batch[sku].vcHardwarePXObj
if (!vcHardPXObj) {
    api.abortCalculation()
    return
}
return vcHardPXObj