import java.math.RoundingMode

def srp
def productType = out.ProductType


if (productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental") {
    srp = out.HardwareProductPrice
}

return srp?.toBigDecimal()?.setScale(0, RoundingMode.UP)