if (!api.global.batch) {
    api.global.batch = [:]
}


if (!api.global.replacementFactorMap) {
    api.global.replacementFactorMap = libs.vmwareUtil.CacheManager.cacheHwReplacementServiceFactor(["All", Constants.STR_PRODUCT], ["All", Constants.STR_HW_OFFER, Constants.STR_RENTAL_OFFER, Constants.STR_EXT_REPLACEMNET_OFFER], [Constants.STR_BASE])
}

if (!api.global.basePriceListMap) {
    api.global.basePriceListMap = libs.vmwareUtil.CacheManager.cacheBaseDetails(["All", Constants.STR_PRODUCT], ["All", Constants.STR_HW_OFFER, Constants.STR_RENTAL_OFFER, Constants.STR_EXT_REPLACEMNET_OFFER])
}
if (!api.global.billingTermFactorMap) {
    api.global.billingTermFactorMap = libs.vmwareUtil.CacheManager.cacheTermsFactor(["All", Constants.STR_PRODUCT], ["All", Constants.STR_RENTAL_OFFER, Constants.STR_EXT_REPLACEMNET_OFFER], ["All", Constants.STR_BASE])
}
if (!api.global.payFreqMap) {
    api.global.payFreqMap = libs.vmwareUtil.CacheManager.cachePaymentFrequncyLabel()
}

if (!api.global.roundingRulesMap) {
    def table = api.findLookupTable("RoundingRulesConfiguration")
    filter = [
            Filter.equal("lookupTable.id", table.id)
    ]
    api.global.roundingRulesMap = api.find("MLTV4", 0, api.getMaxFindResultsLimit(), null, ["key1", "key2", "key3", "key4", "attribute1", "attribute2"], *filter).collectEntries {
        [(it.key1 + it.key2 + it.key3 + it.key4): it]
    }
}
if (!api.global.longevityFactorMap)
    api.global.longevityFactorMap = libs.vmwareUtil.CacheManager.cacheLongevityFactors(["All", Constants.STR_PRODUCT], ["All", Constants.STR_EXT_REPLACEMNET_OFFER], ["All", Constants.STR_BASE])


api.local.pid = api.product("sku")


if (!api.global.batch[api.local.pid]) {

    api.global.batch.clear()
    def batch = api.getBatchInfo()?.collect { it[0] } ?: [api.local.pid]

    filter = [
            Filter.equal("name", "VCHardwareProducts"),
            Filter.in("sku", batch)

    ]

    def vcHardwareProductsList = api.find("PX20", *filter)

    def vcHardwarePXMap = [:]
    vcHardwarePXMap = vcHardwareProductsList.collectEntries { [(it.sku): it] }


    batch.each {
        api.global.batch[it] = [
                "vcHardwarePXObj": (vcHardwarePXMap[it] ?: null)

        ]
    }

}
return