def listPrice = out.FinalRetailPrice
def rate = out.ConversionFactorGBP
def LC = out.LCFactor
String currency = "GBP"

if (!LC || !rate || !listPrice) return ""

def lcPrice = listPrice * rate
def threshHold = 50
def roundToThresholdMet = 1
def roundToThresholdNotMet = 0.1
def substract = 0.05
def GBPCur = libs.vmwareUtil.VCRounding.getCurrencyValue(currency, out.ProductType, api.global.roundingRulesMap)

if (out.ProductType == "Perpetual- SnS") {
    return lcPrice.setScale(2, BigDecimal.ROUND_HALF_UP)
} else {
    if (LC == "LC-77") {
        return libs.vmwareUtil.RoundingLogicDesktopV2.applyRoundingLogic(lcPrice, threshHold, substract, roundToThresholdMet, roundToThresholdNotMet)?.toBigDecimal()
    } else {
        return libs.vmwareUtil.VCRounding.roundedLCPrice(lcPrice, GBPCur.RoundingType, GBPCur.Precision)
    }
}  