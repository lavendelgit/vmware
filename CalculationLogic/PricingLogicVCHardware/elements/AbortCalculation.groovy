List validTypes = ["Software", "Extended Replacement Service", "Hardware Capex", "Rental"]
if (!validTypes.contains(out.ProductType)) {
    api.abortCalculation()
}