import java.math.RoundingMode

def suggestedRetailPrice
def productType = out.ProductType
def years = out.Years?.toBigDecimal()
def basePrice = out.BasePrice?.toBigDecimal()
def tco = out.RentalTCO?.toBigDecimal()
def longevityFactor = out.LongevityFactor?.toBigDecimal()
def replacementFactor = out.ReplacementFactor?.toBigDecimal()
def billingTermFactor = out.BillingTermFactor?.toBigDecimal()
def yearsFactor = out.YearsFactor?.toBigDecimal()

if (productType == "Extended Replacement Service") {

    if (basePrice && replacementFactor)
        suggestedRetailPrice = (basePrice * replacementFactor)

    if (suggestedRetailPrice && years && longevityFactor)
        suggestedRetailPrice = suggestedRetailPrice * years * longevityFactor

} else if (productType == "Hardware Capex") {

    if (basePrice && replacementFactor)
        suggestedRetailPrice = basePrice * replacementFactor

} else if (productType == "Rental") {

    if (basePrice && tco)
        suggestedRetailPrice = basePrice * tco

    if (suggestedRetailPrice && years && billingTermFactor)
        suggestedRetailPrice = suggestedRetailPrice * years * billingTermFactor

    if (suggestedRetailPrice && replacementFactor && yearsFactor && yearsFactor != 0)
        suggestedRetailPrice = suggestedRetailPrice * replacementFactor / yearsFactor
}

return suggestedRetailPrice?.toBigDecimal()