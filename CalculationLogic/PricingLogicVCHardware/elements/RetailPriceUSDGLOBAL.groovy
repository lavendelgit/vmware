def listPrice = out.FinalRetailPrice
def rate = out.ConversionFactorUSDGLOBAL
def LC = out.LCFactor
String currency = "USD"
def USDCur = libs.vmwareUtil.VCRounding.getCurrencyValue(currency, out.ProductType, api.global.roundingRulesMap)

if (!LC || !rate || !listPrice) return ""

def lcPrice = listPrice * rate
return libs.vmwareUtil.VCRounding.roundedLCPrice(lcPrice, USDCur.RoundingType, USDCur.Precision)
