def LC = out.LCFactor
if (LC == null) return 0
def currencyFilter = Filter.equal("name", LC)
def fxRate = api.findLookupTableValues("LocalCurrencyExchangeRates", currencyFilter).attribute1[0]


if (fxRate != null) {
    return new BigDecimal(fxRate)
} else {
    return null
}