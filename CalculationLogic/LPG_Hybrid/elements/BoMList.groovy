if (api.syntaxCheck) {
    return null
}
def BoM = api.getElement("BoM")
def partNo = api.getElement("HybridSKU") ?: ""
def resultMatrix = api.newMatrix("SKU", "Sub-Component", "Quantity", "List Price")
def BoMComponentListPriceTotal = 0
if (BoM != null) {
    resultMatrix.setColumnFormat("Quantity", FieldFormatType.NUMERIC)
    resultMatrix.setColumnFormat("List Price", FieldFormatType.MONEY)
    BoM.each {
        def BoMListPrice
        def BoMListPriceTab = api.find("PX", Filter.equal("name", "ListPrice"), Filter.equal("sku", it.label))
        if (BoMListPriceTab != null) {
            BoMListPrice = BoMListPriceTab[0]?.attribute1
            BoMComponentListPriceTotal += BoMListPrice ?: 0
        }
        def row = [:]
        row.put("SKU", partNo)
        row.put("Quantity", it.quantity)
        row.put("Sub-Component", it.label)
        row.put("List Price", BoMListPrice)
        resultMatrix.addRow(row)
    }
}
api.global.BoMComponentListPriceTotal = BoMComponentListPriceTotal
return resultMatrix