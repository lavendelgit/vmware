import com.googlecode.genericdao.search.Filter

api.local.submittedItems = []
List filter = []
int start = 0

filter = [
        Filter.equal(Constants.WORKFLOW_STATUS, Constants.SUBMITTED),
        Filter.in(Constants.PRICEGRID_ID_FIELD, out.CacheLPGID)
]

int maxResults = api.getMaxFindResultsLimit()
while (items = api.namedEntities(api.find("PGI", start, maxResults, null, *filter))) {
    start += items.size()
    api.local.submittedItems.addAll(items)
}
return

