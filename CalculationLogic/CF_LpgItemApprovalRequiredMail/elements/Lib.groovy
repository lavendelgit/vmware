String getFactorColumnheader(String ColumnName) {
    "<td style='border: 1px solid black;background-color:#FFFFCC'>" + ColumnName + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC'>" + 'Previous ' + ColumnName + "</td>"
}

String getFactorValues(String CurrentFactor, String PreviousFactor) {
    "<td style='border: 1px solid black;background-color:#FFFFCC;color:DodgerBlue'>" + "<strong>" + CurrentFactor + "</strong>" + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC;color:Orange'>" + "<strong>" + PreviousFactor + "</strong>" + "</td>"
}

String getColumnheader(String ColumnName) {
    "<td style='border: 1px solid black;background-color:#FFFFCC;'>" + ColumnName + ' List Price' + "</td>" +
            "<td style='border: 1px solid ;background-color:#FFFFCC;'>" + 'Previous ' + ColumnName + ' List Price' + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC;'>" + 'Δ ' + ColumnName + ' List Price' + "</td>"
}

String getDistiColumnheader(String ColumnName) {
    "<td style='border: 1px solid black;background-color:#FFFFCC;'>" + ColumnName + ' Disti List Price' + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC;'>" + 'Previous ' + ColumnName + ' Disti List Price' + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC;'>" + 'Δ ' + ColumnName + ' Disti List Price' + "</td>"
}

String getResellerColumnheader(String ColumnName) {
    "<td style='border: 1px solid black;background-color:#FFFFCC;'>" + ColumnName + ' Reseller List Price' + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC;'>" + 'Previous ' + ColumnName + ' Reseller List Price' + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC;'>" + 'Δ ' + ColumnName + ' Reseller List Price' + "</td>"
}

String getDeltaValues(String ListPrice, String PreviousListPrice, String DeltaListPrice) {
    "<td style='border: 1px solid black;background-color:#FFFFCC;color:DodgerBlue'>" + "<strong>" + ListPrice + "</strong>" + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC;color:Orange'>" + "<strong>" + PreviousListPrice + "</strong>" + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC;color:MediumSeaGreen'>" + "<strong>" + DeltaListPrice + "</strong>" + "</td>"
}

