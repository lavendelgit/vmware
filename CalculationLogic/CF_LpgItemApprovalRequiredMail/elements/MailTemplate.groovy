List emailList = []
List emailId = []
Map getID = [:]
List uniqueEmailList
String currentFactor
String previousFactor
String listPrice
String previousListPrice
String deltaListPrice
String externalActionToken

String partitionName = api.currentPartitionName()
String url = ""
url = url + api.getBaseURL()
String message = Constants.SUBJECT_FORMAT
message += Constants.TABLE1_FORMAT
message += Constants.TABLE1_HEADER_FORMAT

for (item in api.local.submittedItems) {
    Map approveditems = api.getItemCompleteCalculationResults(item.typedId)
    approveurl = url + "/pricefx/$partitionName/directaction/approve/"
    denyurl = url + "/pricefx/$partitionName/directaction/deny/"
    getID = api.find('PGI', Filter.equal(Constants.PRICEGRID_ID_FIELD, item.priceGridId)).getAt(0)
    emailId = api.findWorkflowInfo("PGI", getID.id)?.activeStep?.userApprovers?.collect { it.email }
    emailList << emailId
    externalActionToken = api.find("W", Filter.equal(Constants.APPROVABLE_TYPE_ID, item.typedId),
            Filter.equal(Constants.WORKFLOW_STATUS, Constants.SUBMITTED))?.externalActionToken?.find()
    approveurl += "$externalActionToken/output=html"
    denyurl += "$externalActionToken/output=html"

    message += "<tr><td style='border: 1px solid black;background-color:#FFFFCC'>" + "<small>" + item.priceGridId + "</small>" + "</td> " +
            "<td style='border: 1px solid black;background-color:#FFFFCC'>" + "<small>" + item.sku + "</small>" + "</td> "

    message += Constants.TABLE2_FORMAT
    Constants.FACTOR_COLUMNS?.each { factor ->
        if ((approveditems?.Factors?.result?.entries[factor]?.getAt(0)) != (approveditems?.Factors?.result?.entries[Constants.PREVIOUS_FIELD + factor]?.getAt(0))) {
            message += Lib.getFactorColumnheader(factor)
        }
    }
    message += Constants.ROW_END_FORMAT
    Constants.FACTOR_COLUMNS?.each { factor ->
        if ((approveditems?.Factors?.result?.entries[factor]?.getAt(0)) != (approveditems?.Factors?.result?.entries[Constants.PREVIOUS_FIELD + factor]?.getAt(0))) {
            currentFactor = approveditems?.Factors?.result?.entries[factor]?.getAt(0)
            previousFactor = approveditems?.Factors?.result?.entries[Constants.PREVIOUS_FIELD + factor]?.getAt(0)
            message += Lib.getFactorValues(currentFactor, previousFactor)
        }
    }
    message += Constants.ROW_END_FORMAT
    message += Constants.TABLE_END_FORMAT

    message += Constants.TABLE2_FORMAT
    Constants.DELTA_COLUMNS?.each { pricefxdelta ->
        if (approveditems?.PricefxLocalCurrencyListPrice?.result?.entries[Constants.DELTA_FIELD + pricefxdelta + Constants.LIST_PRICE_FIELD]?.getAt(0) != 0) {
            message += Lib.getColumnheader(pricefxdelta)
        }
    }
    message += Constants.ROW_END_FORMAT
    Constants.DELTA_COLUMNS?.each { pricefxdelta ->
        if (approveditems?.PricefxLocalCurrencyListPrice?.result?.entries[Constants.DELTA_FIELD + pricefxdelta + Constants.LIST_PRICE_FIELD]?.getAt(0) != 0) {
            listPrice = approveditems?.PricefxLocalCurrencyListPrice?.result?.entries[pricefxdelta]?.getAt(0)
            previousListPrice = approveditems?.PricefxLocalCurrencyListPrice?.result?.entries[Constants.PREVIOUS_FIELD + pricefxdelta + Constants.LIST_PRICE_FIELD]?.getAt(0)
            deltaListPrice = approveditems?.PricefxLocalCurrencyListPrice?.result?.entries[Constants.DELTA_FIELD + pricefxdelta + Constants.LIST_PRICE_FIELD]?.getAt(0)
            message += Lib.getDeltaValues(listPrice, previousListPrice, deltaListPrice)
        }
    }
    message += Constants.ROW_END_FORMAT
    message += Constants.TABLE_END_FORMAT

    message += Constants.TABLE2_FORMAT
    Constants.DELTA_COLUMNS?.each { distidelta ->
        if (approveditems?.PricefxDistiListPrice?.result?.entries[Constants.DELTA_FIELD + distidelta + Constants.DISTI_LIST_PRICE_FIELD]?.getAt(0) != 0) {
            message += Lib.getDistiColumnheader(distidelta)
        }
    }
    message += Constants.ROW_END_FORMAT
    Constants.DELTA_COLUMNS?.each { distidelta ->
        if (approveditems?.PricefxDistiListPrice?.result?.entries[Constants.DELTA_FIELD + distidelta + Constants.DISTI_LIST_PRICE_FIELD]?.getAt(0) != 0) {
            listPrice = approveditems?.PricefxDistiListPrice?.result?.entries[distidelta + Constants.DISTI_LIST_PRICE_FIELD]?.getAt(0)
            previousListPrice = approveditems?.PricefxDistiListPrice?.result?.entries[Constants.PREVIOUS_FIELD + distidelta + Constants.DISTI_LIST_PRICE_FIELD]?.getAt(0)
            deltaListPrice = approveditems?.PricefxDistiListPrice?.result?.entries[Constants.DELTA_FIELD + distidelta + Constants.DISTI_LIST_PRICE_FIELD]?.getAt(0)
            message += Lib.getDeltaValues(listPrice, previousListPrice, deltaListPrice)
        }
    }
    message += Constants.ROW_END_FORMAT
    message += Constants.TABLE_END_FORMAT

    message += Constants.TABLE2_FORMAT
    Constants.DELTA_COLUMNS?.each { resellerdelta ->
        if (approveditems?.PricefxResellerListPrice?.result?.entries[Constants.DELTA_FIELD + resellerdelta + Constants.RESELLER_LIST_PRICE_FIELD]?.getAt(0) != 0) {
            message += Lib.getResellerColumnheader(resellerdelta)
        }
    }
    message += Constants.ROW_END_FORMAT
    Constants.DELTA_COLUMNS?.each { resellerdelta ->
        if (approveditems?.PricefxResellerListPrice?.result?.entries[Constants.DELTA_FIELD + resellerdelta + Constants.RESELLER_LIST_PRICE_FIELD]?.getAt(0) != 0) {
            listPrice = approveditems?.PricefxResellerListPrice?.result?.entries[resellerdelta + Constants.RESELLER_LIST_PRICE_FIELD]?.getAt(0)
            previousListPrice = approveditems?.PricefxResellerListPrice?.result?.entries[Constants.PREVIOUS_FIELD + resellerdelta + Constants.RESELLER_LIST_PRICE_FIELD]?.getAt(0)
            deltaListPrice = approveditems?.PricefxResellerListPrice?.result?.entries[Constants.DELTA_FIELD + resellerdelta + Constants.RESELLER_LIST_PRICE_FIELD]?.getAt(0)
            message += Lib.getDeltaValues(listPrice, previousListPrice, deltaListPrice)
        }
    }
    message += Constants.ROW_END_FORMAT
    message += Constants.TABLE_END_FORMAT


    message += "<td style='border: 1px solid black;background-color:#FFFFCC;color:#008000;'>" + "<a  href=  $approveurl>Approve</a>" + "</td>" +
            "<td style='border: 1px solid black;background-color:#FFFFCC;color:#FF0000;'>" + "<a  href=  $denyurl>Deny</a>" + "</td> </tr>"

}
message += Constants.TABLE_END_FORMAT
uniqueEmailList = emailList?.flatten()?.unique()?.minus(null)
uniqueEmailList?.each { email ->
    api.sendEmail(email, Constants.EMAIL_SUBJECT_FIELD, message)
}
