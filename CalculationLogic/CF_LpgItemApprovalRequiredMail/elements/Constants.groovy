import groovy.transform.Field

@Field final String ID_FIELD = "id"
@Field final String STATUS_FIELD = "status"
@Field final String PREVIOUS_FIELD = "Previous "
@Field final String DELTA_FIELD = "Δ "
@Field final String LIST_PRICE_FIELD = " List Price"
@Field final String DISTI_LIST_PRICE_FIELD = " Disti List Price"
@Field final String RESELLER_LIST_PRICE_FIELD = " Reseller List Price"
@Field final String EMAIL_SUBJECT_FIELD = "LPG Approval List"
@Field final String READY = "READY"
@Field final String SUBMITTED = "SUBMITTED"
@Field final String WORKFLOW_STATUS = "workflowStatus"
@Field final String PRICEGRID_ID_FIELD = "priceGridId"
@Field final List DELTA_COLUMNS = ["NAMUSD", "APACUSD", "CHINAUSD", "LATAMUSD", "EMEAUSD", "EMEAUSD2", "GlobalUSD", "EMEAEUR", "EMEAGBP", "APACCNY", "APACAUD", "APACJPY"]
@Field final List FACTOR_COLUMNS = ["CurrencyFactor", "PriceListRegionFactor", "PromotionFactor", "VolumeTierFactor", "DataCenterFactor", "PurchasingProgramFactor", "SegmentFactor", "SupportTypeFactor", "SupportTierFactor", "TermFactor", "TermUOMFactor", "RetentionPeriodFactor", "PSTermFactor", "PSOptionFactor", "PaymentFactor", "MetricFactor", "HostingFactor", "OSFactor", "ProgramOptionFactor", "QuantityAdjustment", "UpgradeFactor", "PaymentCycleAdjustmentFactor"]
@Field final String SUBJECT_FORMAT = "Hello <br> Please review the following lpg items and approve"
@Field final String TABLE1_FORMAT = "<p><table style='border: 1px solid black;border-collapse: collapse;'>"
@Field final String TABLE1_HEADER_FORMAT = "<tr><th style='border: 1px solid white;font-weight:bold;background-color:#0473B4;color:#FFFFFF;'>Price Grid ID</th> " +
        "<th style='border: 1px solid white;font-weight:bold;background-color:#0473B4;color:#FFFFFF;'>Part Number</th> " +
        "<th style='border: 1px solid white;font-weight:bold;background-color:#0473B4;color:#FFFFFF;'>Factors</th>" +
        "<th style='border: 1px solid white;font-weight:bold;background-color:#0473B4;color:#FFFFFF;'>Pricefx Local Currency List Price</th>" +
        "<th style='border: 1px solid white;font-weight:bold;background-color:#0473B4;color:#FFFFFF;'>Pricefx Disti List Price</th>" +
        "<th style='border: 1px solid white;font-weight:bold;background-color:#0473B4;color:#FFFFFF;'>Pricefx Reseller List Price</th>" +
        "<th style='border: 1px solid white;font-weight:bold;background-color:#0473B4;color:#FFFFFF;'>Approve</th>" +
        "<th style='border: 1px solid white;font-weight:bold;background-color:#0473B4;color:#FFFFFF;'>Deny</th></tr>"
@Field final String APPROVABLE_TYPE_ID = "approvableTypedId"
@Field final String TABLE2_FORMAT = "<td style='border: 1px solid black;'> <table style='border: 1px solid white;border-collapse: collapse;'><tr>"
@Field final String ROW_END_FORMAT = "</tr>"
@Field final String TABLE_END_FORMAT = "</table> </td>"