if (!api.local.velotypeMap) {
    api.local.velotypeMap = [:]
}

def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCAddOnBandwidthRestrictions").id)
]
def bandwidthList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key1",
        ["key1", "key2"], true, *filter)

bandwidthList.each { obj ->
    if (api.local.velotypeMap[obj.key1]) {
        api.local.velotypeMap[obj.key1] << obj.key2
    } else {
        api.local.velotypeMap[obj.key1] = [obj.key2]
    }


}

return