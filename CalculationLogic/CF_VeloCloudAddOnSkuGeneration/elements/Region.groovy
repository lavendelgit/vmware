def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("AddonLocationOptions").id)
]
api.local.locationList = api.find("LTV", 0, api.getMaxFindResultsLimit(), "name",
        ["name"], *filter).collect { it.name }


return
