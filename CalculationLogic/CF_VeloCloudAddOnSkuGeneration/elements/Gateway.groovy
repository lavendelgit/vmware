def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCAddOnGatewayRestrictions").id),
        Filter.equal("key1", "Type 3")
]
api.local.gatewayList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key1",
        ["key1", "key2"], *filter).collect { it.key2 }


return
