def sku


def prefix = api.local.itemMap[Constants.STR_ITEMTYPE]
def bandwidthList = api.local.velotypeMap[Constants.STR_ADDONTYPE3]
def locationList = api.local.locationList
def gatewayList = api.local.gatewayList
def subscriptionList = api.local.subscriptionsList
def subscriptionYearMap = api.local.subscriptionYearsMap
def paymentTypeList = api.local.paymentTypeList
def licenseList = api.local.licenseList

def addOnShortDescRuleList = out.ShortDescRule
def addOnLongDescRuleList = out.LongDescRule

def addOnShortDesc
def addOnLongDesc

def key
def paymentTermYears
def paymentFreqDesc
Map params = [:]
bandwidthList.each { bandwidth ->
    locationList.each { region ->
        gatewayList.each { gateway ->
            subscriptionList.each { subscriptionMonths ->
                paymentTypeList.each { paymentFrequency ->
                    licenseList.each { licence ->

                        key = Constants.STR_ITEMTYPE + "~" + subscriptionMonths
                        paymentTermYears = subscriptionYearMap[key]
                        paymentFreqDesc = api.local.paymentDescMap[paymentFrequency]
                        sku = Library.createType3AddOnSKU(prefix, bandwidth, region, gateway, subscriptionMonths, paymentFrequency, licence)
                        if (sku) {
                            addOnShortDesc = Library.createVCAddonShortLongDesc(addOnShortDescRuleList, null, bandwidth, null, licence, paymentFreqDesc, paymentTermYears)
                            addOnLongDesc = Library.createVCAddonShortLongDesc(addOnLongDescRuleList, null, bandwidth, null, licence, paymentFreqDesc, paymentTermYears)
                            params = [
                                    "name"             : Constants.STR_NAME,
                                    "sku"              : sku,
                                    "ShortDesc"        : addOnShortDesc,
                                    "LongDesc"         : addOnLongDesc,
                                    "ItemType"         : Constants.STR_ITEMTYPE,
                                    "WANType"          : null,
                                    "PurchaseAvail"    : null,
                                    "Bandwidth"        : bandwidth,
                                    "PaymentTermMonths": subscriptionMonths,
                                    "PaymentTermYears" : paymentTermYears,
                                    "PaymentFrequency" : paymentFrequency,
                                    "AddOnType"        : Constants.STR_ADDONTYPE3,
                                    "Region"           : region,
                                    "LicenseClass"     : licence,
                                    "Gateway"          : gateway
                            ]
                            Library.addOrUpdateAddonSKU(params)
                        }

                    }
                }

            }
        }
    }
}
return
