def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCAddOnWANTypes").id),
        Filter.equal("key1", Constants.STR_ADDONTYPE2)
]
api.local.wanType2List = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key2"], *filter).collect { it.key2 }


return
