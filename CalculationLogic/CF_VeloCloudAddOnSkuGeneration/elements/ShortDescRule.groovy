def addonfilter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCAddonShortDesc").id),
        Filter.equal("key1", Constants.STR_ITEMTYPE)
]

return api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6"], *addonfilter)
