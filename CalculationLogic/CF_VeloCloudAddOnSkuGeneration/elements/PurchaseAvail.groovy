def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("PurchaseAvailabilityOptions").id)
]
api.local.purchaseAvailList = api.find("LTV", 0, api.getMaxFindResultsLimit(), "name",
        ["name"], *filter).collect { it.name }

return
