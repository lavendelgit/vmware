def sku

def prefix = api.local.itemMap[Constants.STR_ITEMTYPE]
def wanList = api.local.wanType1List
def purchaseList = api.local.purchaseAvailList
def subscriptionList = api.local.subscriptionsList
def subscriptionYearMap = api.local.subscriptionYearsMap
def paymentTypeList = api.local.paymentTypeList
def licenseList = api.local.licenseList

def addOnShortDescRuleList = out.ShortDescRule
def addOnLongDescRuleList = out.LongDescRule

def addOnShortDesc
def addOnLongDesc
def key
def paymentTermYears
def paymentFreqDesc
Map params = [:]
wanList.each { wanType ->
    purchaseList.each { purchaseAvail ->
        subscriptionList.each { subscriptionMonths ->
            paymentTypeList.each { paymentFrequency ->
                licenseList.each { licence ->
                    key = Constants.STR_ITEMTYPE + "~" + subscriptionMonths
                    paymentTermYears = subscriptionYearMap[key]
                    paymentFreqDesc = api.local.paymentDescMap[paymentFrequency]

                    sku = Library.createType1AddOnSKU(prefix, wanType, purchaseAvail, subscriptionMonths, paymentFrequency, licence)
                    if (sku) {
                        addOnShortDesc = Library.createVCAddonShortLongDesc(addOnShortDescRuleList, wanType, null, purchaseAvail, licence, paymentFreqDesc, paymentTermYears)
                        addOnLongDesc = Library.createVCAddonShortLongDesc(addOnLongDescRuleList, wanType, null, purchaseAvail, licence, paymentFreqDesc, paymentTermYears)
                        params = [
                                "name"             : Constants.STR_NAME,
                                "sku"              : sku,
                                "ShortDesc"        : addOnShortDesc,
                                "LongDesc"         : addOnLongDesc,
                                "ItemType"         : Constants.STR_ITEMTYPE,
                                "WANType"          : wanType,
                                "PurchaseAvail"    : purchaseAvail,
                                "Bandwidth"        : null,
                                "PaymentTermMonths": subscriptionMonths,
                                "PaymentTermYears" : paymentTermYears,
                                "PaymentFrequency" : paymentFrequency,
                                "AddOnType"        : Constants.STR_ADDONTYPE1,
                                "Region"           : null,
                                "LicenseClass"     : licence,
                                "Gateway"          : null
                        ]
                        Library.addOrUpdateAddonSKU(params)
                    }
                }
            }
        }
    }
}
return

