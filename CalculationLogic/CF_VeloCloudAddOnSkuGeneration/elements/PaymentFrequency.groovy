api.local.payFrequencyMap = [:]

def descfilter = [
        Filter.equal("lookupTable.id", api.findLookupTable("PaymentFrequency").id)
]

def paymentDescList = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, ["name", "value"], *descfilter)
api.local.paymentDescMap = paymentDescList.collectEntries {
    [(it.value): it.name]
}

api.local.paymentTypeList = paymentDescList.collect { it.value }

return