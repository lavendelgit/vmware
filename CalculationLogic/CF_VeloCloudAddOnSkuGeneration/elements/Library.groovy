def addOrUpdateAddonSKU(Map params) {
    def pxRecord = [
            name       : params.name,
            sku        : params.sku,
            attribute1 : params.ShortDesc,
            attribute2 : params.LongDesc,
            attribute3 : params.ItemType,
            attribute4 : params.WANType,
            attribute6 : params.PurchaseAvail,
            attribute7 : params.Bandwidth,
            attribute8 : params.PaymentTermMonths,
            attribute9 : params.PaymentTermYears,
            attribute10: params.PaymentFrequency,
            attribute11: params.AddOnType,
            attribute12: params.Region,
            attribute13: params.LicenseClass,
            attribute14: params.Gateway

    ]

    if (pxRecord) api.addOrUpdate("PX20", pxRecord)
}

def createType1AddOnSKU(prefix, wanType, purchaseAvail, subscriptionMonths, paymentFrequency, licence) {
    prefix += "-"
    prefix += wanType
    prefix += "-"
    prefix += purchaseAvail
    prefix += subscriptionMonths
    prefix += paymentFrequency
    prefix += "-"
    prefix += licence
    return prefix
}

def createType2AddOnSKU(prefix, bandwidth, wanType, purchaseAvail, subscriptionMonths, paymentFrequency, licence) {
    prefix += bandwidth
    prefix += "-"
    prefix += wanType
    prefix += "-"
    prefix += purchaseAvail
    prefix += subscriptionMonths
    prefix += paymentFrequency
    prefix += "-"
    prefix += licence
    return prefix
}

def createType3AddOnSKU(prefix, bandwidth, region, gateway, subscriptionMonths, paymentFrequency, licence) {
    prefix += bandwidth
    prefix += "-"
    prefix += region
    prefix += "-"
    prefix += gateway
    prefix += "-"
    prefix += subscriptionMonths
    prefix += paymentFrequency
    prefix += "-"
    prefix += licence
    return prefix
}


def conditionalDesc(shortDesc, operator, value, desc, conditionName) {
    //api.trace("long desc","",shortDesc + "~"+operator+"~"+value+"~"+desc+"~"+conditionName)
    switch (operator) {
        case "=":

            if (conditionName.equals(value)) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<=":
            if (conditionName <= value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case ">":
            if (conditionName > value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<>":
            if (conditionName != value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break

    }
    return shortDesc
}

def createVCAddonShortLongDesc(ruleList, wanType, bandwidth, purchaseAvail, licence, paymentFreqDesc, paymentTermYears) {
    String shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                //shortDesc.append(rule.attribute6)
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break

            case "Conditional":
                switch (rule.attribute3) {
                    case "WanType":
                        shortDesc = conditionalDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, wanType)
                        break;
                    case "Bandwidth":
                        shortDesc = conditionalDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, bandwidth)
                        break;
                    case "PurchaseAvailability":
                        shortDesc = conditionalDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, purchaseAvail)
                        break;
                    case "LicenseClass":
                        shortDesc = conditionalDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, licence)
                        break;
                    case "PaymentFreqDesc":
                        shortDesc = conditionalDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, paymentFreqDesc)
                        break;
                    case "PaymentTermYears":
                        shortDesc = conditionalDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, paymentTermYears)
                        break;
                }
                break

        }
    }
    return shortDesc
}






