//Finds LC factor from Product Master for each of BoM components
if (api.syntaxCheck) {
    return null
}
def res
def BoM = api.getElement("BoM")
def resultMatrix = api.newMatrix("SKU", "Sub-Component", "Quantity", "USD")
if (BoM != null) {
    resultMatrix.setColumnFormat("Quantity", FieldFormatType.NUMERIC)
    resultMatrix.setColumnFormat("USD", FieldFormatType.MONEY)
    BoM.each {
        def prods = api.find("P", Filter.equal("sku", it.label))
        def lcFactor = prods.attribute9[0]
        if (lcFactor) res = lcFactor
    }
}
return res