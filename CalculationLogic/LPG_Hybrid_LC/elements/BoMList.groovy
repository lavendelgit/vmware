//Calculates prices in allcurrencies for each BoM Component
//Sums up total price of all BoM Components in particular currency

if (api.syntaxCheck) {
    return null
}
def BoM = api.getElement("BoM")
def partNo = api.getElement("HybridSKU") ?: ""
def resultMatrix = api.newMatrix("SKU", "Sub-Component", "Quantity", "USD", "AUD", "JPY", "CNY", "EUR", "GBP", "EMEA USD", "GLOBAL USD")
def BoMComponentListPriceTotalUSD = 0
def BoMComponentListPriceTotalAUD = 0
def BoMComponentListPriceTotalJPY = 0
def BoMComponentListPriceTotalCNY = 0
def BoMComponentListPriceTotalEUR = 0
def BoMComponentListPriceTotalGBP = 0
def BoMComponentListPriceTotalEMEAUSD = 0
def BoMComponentListPriceTotalEMEAUSD2 = 0
def BoMComponentListPriceTotalGLOBALUSD = 0

def BoMComponentListPriceUSD = 0
def BoMComponentListPriceAUD = 0
def BoMComponentListPriceJPY = 0
def BoMComponentListPriceCNY = 0
def BoMComponentListPriceEUR = 0
def BoMComponentListPriceGBP = 0
def BoMComponentListPriceEMEAUSD = 0
def BoMComponentListPriceEMEAUSD2 = 0
def BoMComponentListPriceGLOBALUSD = 0

def LCFactorAUD = api.getElement("LCFactorAUD") ?: 0
def LCFactorJPY = api.getElement("LCFactorJPY") ?: 0
def LCFactorCNY = api.getElement("LCFactorCNY") ?: 0
def LCFactorEUR = api.getElement("LCFactorEUR") ?: 0
def LCFactorGBP = api.getElement("LCFactorGBP") ?: 0
def LCFactorEMEAUSD = api.getElement("LCFactorEMEAUSD") ?: 0
def LCFactorEMEAUSD2 = api.getElement("LCFactorEMEAUSD2") ?: 0
def LCFactorGLOBALUSD = api.getElement("LCFactorGLOBALUSD") ?: 0


if (BoM != null) {
    resultMatrix.setColumnFormat("Quantity", FieldFormatType.NUMERIC)
    resultMatrix.setColumnFormat("USD", FieldFormatType.MONEY)
    resultMatrix.setColumnFormat("AUD", FieldFormatType.MONEY)
    resultMatrix.setColumnFormat("JPY", FieldFormatType.MONEY)
    resultMatrix.setColumnFormat("CNY", FieldFormatType.MONEY)
    resultMatrix.setColumnFormat("EUR", FieldFormatType.MONEY)
    resultMatrix.setColumnFormat("GBP", FieldFormatType.MONEY)
    resultMatrix.setColumnFormat("EMEA USD", FieldFormatType.MONEY)
    resultMatrix.setColumnFormat("EMEA USD 2", FieldFormatType.MONEY)
    resultMatrix.setColumnFormat("GLOBAL USD", FieldFormatType.MONEY)

    BoM.each {
        def BoMListPrice
        def BoMListPriceTab = api.find("PX", Filter.equal("name", "ListPrice"), Filter.equal("sku", it.label))
        def BoMListPriceTabHybrid = api.find("PX", Filter.equal("name", "ListPriceHybrid"), Filter.equal("sku", it.label))

        if (BoMListPriceTabHybrid.size() != 0) {
            BoMListPrice = BoMListPriceTabHybrid[0]?.attribute1
            if (BoMListPrice == null) return BoMListPrice = 0
            BoMComponentListPriceUSD = BoMListPrice
            BoMComponentListPriceAUD = BoMListPriceTabHybrid[0]?.attribute6.toBigDecimal()
            BoMComponentListPriceJPY = BoMListPriceTabHybrid[0]?.attribute5.toBigDecimal()
            BoMComponentListPriceCNY = BoMListPriceTabHybrid[0]?.attribute4.toBigDecimal()
            BoMComponentListPriceEUR = BoMListPriceTabHybrid[0]?.attribute2.toBigDecimal()
            BoMComponentListPriceGBP = BoMListPriceTabHybrid[0]?.attribute3.toBigDecimal()
            BoMComponentListPriceEMEAUSD = BoMListPriceTabHybrid[0]?.attribute7.toBigDecimal()
            BoMComponentListPriceEMEAUSD2 = BoMListPriceTabHybrid[0]?.attribute9.toBigDecimal()
            BoMComponentListPriceGLOBALUSD = BoMListPriceTabHybrid[0]?.attribute8.toBigDecimal()
        } else {
            if (BoMListPriceTab.size() != 0) {
                BoMListPrice = BoMListPriceTab[0]?.attribute1
                if (BoMListPrice == null) return BoMListPrice = 0
                BoMComponentListPriceUSD = BoMListPrice
                BoMComponentListPriceAUD = libs.vmwareUtil.RoundingLogicV2.applyRoundingLogic(BoMListPrice * LCFactorAUD)?.toBigDecimal()
                BoMComponentListPriceJPY = libs.vmwareUtil.RoundingLogicV2.applyRoundingLogic(BoMListPrice * LCFactorJPY)?.toBigDecimal()
                BoMComponentListPriceCNY = libs.vmwareUtil.RoundingLogicV2.applyRoundingLogic(BoMListPrice * LCFactorCNY)?.toBigDecimal()
                BoMComponentListPriceEUR = libs.vmwareUtil.RoundingLogicV2.applyRoundingLogic(BoMListPrice * LCFactorEUR)?.toBigDecimal()
                BoMComponentListPriceGBP = libs.vmwareUtil.RoundingLogicV2.applyRoundingLogic(BoMListPrice * LCFactorGBP)?.toBigDecimal()
                BoMComponentListPriceEMEAUSD = BoMListPrice * LCFactorEMEAUSD.toBigDecimal()
                BoMComponentListPriceEMEAUSD2 = BoMListPrice * LCFactorEMEAUSD2.toBigDecimal()
                BoMComponentListPriceGLOBALUSD = BoMListPrice * LCFactorGLOBALUSD.toBigDecimal()
            }
        }

        BoMComponentListPriceTotalUSD += BoMComponentListPriceUSD
        BoMComponentListPriceTotalAUD += BoMComponentListPriceAUD
        BoMComponentListPriceTotalJPY += BoMComponentListPriceJPY
        BoMComponentListPriceTotalCNY += BoMComponentListPriceCNY
        BoMComponentListPriceTotalEUR += BoMComponentListPriceEUR
        BoMComponentListPriceTotalGBP += BoMComponentListPriceGBP
        BoMComponentListPriceTotalEMEAUSD += BoMComponentListPriceEMEAUSD
        BoMComponentListPriceTotalEMEAUSD2 += BoMComponentListPriceEMEAUSD2
        BoMComponentListPriceTotalGLOBALUSD += BoMComponentListPriceGLOBALUSD

        api.trace(BoMComponentListPriceTotalGBP)

        def row = [:]
        row.put("SKU", partNo)
        row.put("Quantity", it.quantity)
        row.put("Sub-Component", it.label)
        row.put("USD", BoMListPrice)
        row.put("AUD", BoMComponentListPriceAUD)
        row.put("JPY", BoMComponentListPriceJPY)
        row.put("CNY", BoMComponentListPriceCNY)
        row.put("EUR", BoMComponentListPriceEUR)
        row.put("GBP", BoMComponentListPriceGBP)
        row.put("EMEA USD", BoMComponentListPriceEMEAUSD)
        row.put("EMEA USD 2", BoMComponentListPriceEMEAUSD2)
        row.put("GLOBAL USD", BoMComponentListPriceGLOBALUSD)
        resultMatrix.addRow(row)
    }
}

api.global.BoMComponentListPriceTotalUSD = BoMComponentListPriceTotalUSD
api.global.BoMComponentListPriceTotalAUD = BoMComponentListPriceTotalAUD
api.global.BoMComponentListPriceTotalJPY = BoMComponentListPriceTotalJPY
api.global.BoMComponentListPriceTotalCNY = BoMComponentListPriceTotalCNY
api.global.BoMComponentListPriceTotalEUR = BoMComponentListPriceTotalEUR
api.global.BoMComponentListPriceTotalGBP = BoMComponentListPriceTotalGBP
api.global.BoMComponentListPriceTotalEMEAUSD = BoMComponentListPriceTotalEMEAUSD
api.global.BoMComponentListPriceTotalEMEAUSD2 = BoMComponentListPriceTotalEMEAUSD2
api.global.BoMComponentListPriceTotalGLOBALUSD = BoMComponentListPriceTotalGLOBALUSD

return resultMatrix