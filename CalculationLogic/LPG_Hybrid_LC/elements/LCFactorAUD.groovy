import com.ibm.icu.math.BigDecimal

def LC = api.getElement("LCFactor")

def currencyFilter = Filter.equal("name", LC)
def fxRate = api.findLookupTableValues("LocalCurrencyExchangeRates", currencyFilter).attribute1[0]

//api.trace(fxRate)

if (fxRate != null) {
    return new BigDecimal(fxRate)
} else {
    return null
}