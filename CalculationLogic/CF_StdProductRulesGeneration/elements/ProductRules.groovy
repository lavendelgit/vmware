api.local.pxRecords = []
def rulesGenLib = libs.stdProductLib.RulesGenHelper
int skuIndex = 0
for (rule in api.local.productRules) {
    pXParams = [
            "sku"               : Lib.generateKey(rule, api.local.validTermUomCache),
            "Product"           : rule[0],
            "Offer"             : rule[1],
            "DC"                : rule[2],
            "Base"              : rule[3],
            "Metric"            : rule[4],
            "Term"              : rule[5],
            "Payment"           : rule[7],
            "Tier"              : rule[8],
            "Segment"           : rule[9],
            "Version"           : rule[10],
            "Promotion"         : rule[11],
            "OS"                : rule[12],
            "Quantity"          : rule[13],
            "PurchasingProgram" : rule[14],
            "TermUoM"           : api.local.validTermUomCache[rule[5]],
            "SupportType"       : rule[15],
            "SupportTier"       : rule[16],
            "Currency"          : rule[17],
            "Region"            : rule[18],
            "SubRegion"         : rule[19],
            "Hosting"           : rule[20],
            "ProgramOption"     : rule[21],
            "Rule"              : "Valid",
            "SAPBaseSKU"        : api.local.sapBaseSKU[skuIndex],
            "StandardProductSKU": api.local.SKU[skuIndex++],
    ]
    api.local.pxRecords << rulesGenLib.populateSKURecord(Constants.RULES_TABLE_NAME, pXParams)
}
return