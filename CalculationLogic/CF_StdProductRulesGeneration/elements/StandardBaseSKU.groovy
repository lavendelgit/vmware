api.local.SKU = []
api.local.sapBaseSKU = []
for (productRule in api.local.productRules) {
    pXParamSKU = [
            "Product"          : productRule[0] == "-" ? null : out.ProductCache.get(productRule[0]),
            "Offer"            : productRule[1] == "-" ? null : api.local.validOfferCache.get(productRule[1]),
            "Data Center"      : productRule[2] == "-" ? null : api.local.validDCCache.get(productRule[2]),
            "Base"             : productRule[3] == "-" ? null : api.local.validBaseCache.get(productRule[3]),
            "Metric"           : productRule[4] == "-" ? null : api.local.validMetricCache.get(productRule[4]),
            "Term"             : productRule[5] == "-" ? null : api.local.validTermCache.get(productRule[5]),
            "Payment Type"     : productRule[7] == "-" ? null : api.local.validPaymentCache.get(productRule[7]),
            "Tier"             : productRule[8] == "-" ? null : api.local.validTierCache.get(productRule[8]),
            "Segment"          : productRule[9] == "-" ? null : api.local.validSegmentCache.get(productRule[9]),
            "Version"          : productRule[10] == "-" ? null : api.local.validVersionCache.get(productRule[10]),
            "Promotion"        : productRule[11] == "-" ? null : api.local.validPromoCache.get(productRule[11]),
            "OS"               : productRule[12] == "-" ? null : api.local.validOSCache.get(productRule[12]),
            "Quantity"         : productRule[13] == "-" ? null : api.local.validQuantityCache.get(productRule[13]),
            "PurchasingProgram": productRule[14] == "-" ? null : api.local.validPurchasingProgramCache.get(productRule[14]),
            "SupportType"      : productRule[15] == "-" ? null : api.local.validSupportTypeCache.get(productRule[15]),
            "SupportTier"      : productRule[16] == "-" ? null : api.local.validSupportTierCache.get(productRule[16]),
            "Hosting"          : productRule[20] == "-" ? null : api.local.validHostingCache.get(productRule[20]),
            "ProgramOption"    : productRule[21] == "-" ? null : api.local.validProgramOptionCache.get(productRule[21]),
    ]
    api.local.SKU << libs.stdProductLib.SkuGenHelper.createSKU(pXParamSKU)
    api.local.sapBaseSKU << libs.stdProductLib.SkuGenHelper.generateSAPBaseSKU(out.SAPBaseSKUConfigCache, pXParamSKU)
}
return