if (api.local.pxRecords) {
    def deletePL = [
            "data": [
                    "filterCriteria": [
                            "operator"    : "and",
                            "_constructor": "AdvancedCriteria",
                            "criteria"    : [
                                    [
                                            "fieldName": "name",
                                            "operator" : "equals",
                                            "value"    : Constants.RULES_TABLE_NAME
                                    ],
                                    [
                                            "fieldName": "attribute1",
                                            "operator" : "inSet",
                                            "value"    : out.ProductCache ? out.ProductCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute2",
                                            "operator" : "inSet",
                                            "value"    : api.local.validOfferCache ? api.local.validOfferCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute3",
                                            "operator" : "inSet",
                                            "value"    : api.local.validBaseCache ? api.local.validBaseCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute4",
                                            "operator" : "inSet",
                                            "value"    : api.local.validCurrencyCache ? api.local.validCurrencyCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute5",
                                            "operator" : "inSet",
                                            "value"    : api.local.validPricelistRegionCache ? api.local.validPricelistRegionCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute6",
                                            "operator" : "inSet",
                                            "value"    : api.local.validsubRegionCache ? api.local.validsubRegionCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute7",
                                            "operator" : "inSet",
                                            "value"    : api.local.validOSCache ? api.local.validOSCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute8",
                                            "operator" : "inSet",
                                            "value"    : api.local.validMetricCache ? api.local.validMetricCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute9",
                                            "operator" : "inSet",
                                            "value"    : api.local.validDCCache ? api.local.validDCCache.keySet() as List : "-"
                                            //"_constructor": "AdvancedCriteria"
                                    ],
                                    [
                                            "fieldName": "attribute10",
                                            "operator" : "inSet",
                                            "value"    : api.local.validSegmentCache ? api.local.validSegmentCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute11",
                                            "operator" : "inSet",
                                            "value"    : api.local.validQuantityCache ? api.local.validQuantityCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute12",
                                            "operator" : "inSet",
                                            "value"    : api.local.validTermCache ? api.local.validTermCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute14",
                                            "operator" : "inSet",
                                            "value"    : api.local.validPaymentCache ? api.local.validPaymentCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute15",
                                            "operator" : "inSet",
                                            "value"    : api.local.validSupportTypeCache ? api.local.validSupportTypeCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute16",
                                            "operator" : "inSet",
                                            "value"    : api.local.validSupportTierCache ? api.local.validSupportTierCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute17",
                                            "operator" : "inSet",
                                            "value"    : api.local.validPurchasingProgramCache ? api.local.validPurchasingProgramCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute18",
                                            "operator" : "inSet",
                                            "value"    : api.local.validTierCache ? api.local.validTierCache?.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute19",
                                            "operator" : "inSet",
                                            "value"    : api.local.validVersionCache ? api.local.validVersionCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute21",
                                            "operator" : "inSet",
                                            "value"    : api.local.validPromoCache ? api.local.validPromoCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute22",
                                            "operator" : "inSet",
                                            "value"    : api.local.validHostingCache ? api.local.validHostingCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute23",
                                            "operator" : "inSet",
                                            "value"    : api.local.validProgramOptionCache ? api.local.validProgramOptionCache.keySet() as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute24",
                                            "operator" : "inSet",
                                            "value"    : api.local.SAPBaseSKU ? api.local.SAPBaseSKU as List : "-"
                                    ],
                                    [
                                            "fieldName": "attribute25",
                                            "operator" : "inSet",
                                            "value"    : api.local.SKU ? api.local.SKU as List : "-"
                                    ],
                            ]
                    ]
            ]
    ]
    api.boundCall("boundcall", "delete/PX/batch", api.jsonEncode(deletePL), false)

    return
}