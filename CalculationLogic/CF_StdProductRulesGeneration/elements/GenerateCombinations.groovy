api.local.pxRecords = []
def listAttributes = []
def placeholder = ["-"]
if (out.ProductCache) listAttributes << out.ProductCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validOfferCache) listAttributes << api.local.validOfferCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validDCCache) listAttributes << api.local.validDCCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validBaseCache) listAttributes << api.local.validBaseCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validMetricCache) listAttributes << api.local.validMetricCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validTermCache) {
    listAttributes << api.local.validTermCache?.keySet()
    listAttributes << Constants.TERMUOM_PLACEHOLDER
} else {
    listAttributes << placeholder
    listAttributes << placeholder
}
if (api.local.validPaymentCache) listAttributes << api.local.validPaymentCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validTierCache) listAttributes << api.local.validTierCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validSegmentCache) listAttributes << api.local.validSegmentCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validVersionCache) listAttributes << api.local.validVersionCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validPromoCache) listAttributes << api.local.validPromoCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validOSCache) listAttributes << api.local.validOSCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validQuantityCache) listAttributes << api.local.validQuantityCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validPurchasingProgramCache) listAttributes << api.local.validPurchasingProgramCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validSupportTypeCache) listAttributes << api.local.validSupportTypeCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validSupportTierCache) listAttributes << api.local.validSupportTierCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validCurrencyCache) listAttributes << api.local.validCurrencyCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validPricelistRegionCache) listAttributes << api.local.validPricelistRegionCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validsubRegionCache) listAttributes << api.local.validsubRegionCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validHostingCache) listAttributes << api.local.validHostingCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validProgramOptionCache) listAttributes << api.local.validProgramOptionCache?.keySet()
else {
    listAttributes << placeholder
}

api.local.combinations = listAttributes.combinations()
return
