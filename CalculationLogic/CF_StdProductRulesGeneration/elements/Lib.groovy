def generateKey(List rule, Map validTermUomCache) {
    List productKey = [rule[0]]
    productKey = (rule[1] && rule[1] != "-") ? productKey << rule[1] : productKey
    productKey = (rule[3] && rule[3] != "-") ? productKey << rule[3] : productKey
    productKey = (rule[12] && rule[12] != "-") ? productKey << rule[12] : productKey
    productKey = (rule[4] && rule[4] != "-") ? productKey << rule[4] : productKey
    productKey = (rule[2] && rule[2] != "-") ? productKey << rule[2] : productKey
    productKey = (rule[9] && rule[9] != "-") ? productKey << rule[9] : productKey
    productKey = (rule[13] && rule[13] != "-") ? productKey << rule[13] : productKey
    productKey = (rule[5] && rule[5] != "-") ? productKey << rule[5] : productKey
    // productKey = (rule[5] && rule[5] != "-") ? productKey << "MTH" : productKey
    productKey = (rule[5] && rule[5] != "-" && validTermUomCache[rule[5]]) ? productKey << validTermUomCache[rule[5]] : productKey
    productKey = (rule[7] && rule[7] != "-") ? productKey << rule[7] : productKey
    productKey = (rule[15] && rule[15] != "-") ? productKey << rule[15] : productKey
    productKey = (rule[16] && rule[16] != "-") ? productKey << rule[16] : productKey
    productKey = (rule[14] && rule[14] != "-") ? productKey << rule[14] : productKey
    productKey = (rule[8] && rule[8] != "-") ? productKey << rule[8] : productKey
    productKey = (rule[10] && rule[10] != "-") ? productKey << rule[10] : productKey
    productKey = (rule[11] && rule[11] != "-") ? productKey << rule[11] : productKey
    productKey = (rule[20] && rule[20] != "-") ? productKey << rule[20] : productKey
    productKey = (rule[21] && rule[21] != "-") ? productKey << rule[21] : productKey
    return productKey?.join("_")
}