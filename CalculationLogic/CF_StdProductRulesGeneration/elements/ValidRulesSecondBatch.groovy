def combinations = api.local.combinationsBatches?.getAt(1)
libs.stdProductLib.RulesGenHelper.getProductRules(combinations, api.local.invalidUniversalRuleCache, api.local.productRules, api.local.validTermUomCache, Constants.TERM_UOM_INDEX, Constants.TERM_INDEX, Constants.TERMUOM_PLACEHOLDER)?.collect()
return