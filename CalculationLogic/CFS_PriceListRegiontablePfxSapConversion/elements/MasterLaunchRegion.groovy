String masterLaunchRegion = api.currentItem().attribute3
return api.global.pfxSapCache?.find { it.key == masterLaunchRegion }?.collect { it.value }?.getAt(0) ?: masterLaunchRegion
