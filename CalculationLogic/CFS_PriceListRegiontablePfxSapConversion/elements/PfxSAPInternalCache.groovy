api.global.pfxSapCache = api.global.pfxSapCache ? api.global.pfxSapCache : api.findLookupTableValues(Constants.PFX_SAP_REGIONAL_MAPPING_PP)?.collectEntries {
    [(it.name): it.attribute1]
}
return