String channelLaunchRegion = api.currentItem().attribute2
return api.global.pfxSapCache?.find { it.key == channelLaunchRegion }?.collect { it.value }?.getAt(0) ?: channelLaunchRegion
