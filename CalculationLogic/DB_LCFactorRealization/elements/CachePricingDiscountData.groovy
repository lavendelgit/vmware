if (out.DataSelection) {
    List filter = []
    filter << out.DataSelection
    List queryColumns = libs._dto.dsPricingAndDiscounting.LC_FACTOR_COLUMNS
    String orderByField = libs._dto.dsPricingAndDiscounting.LC_FACTOR_ORDERBY
    api.local.lcFactorRealization = libs._dto._generic.getData(false, Constants.PRICING_DISCOUNTING_DM, filter, false, queryColumns, orderByField)
    if (api.local.lcFactorRealization) {
        def ctx = api.getTableContext()
        ctx.createTable((Constants.LC_TABLE_NAME), api.local.lcFactorRealization)
    }
    return
}