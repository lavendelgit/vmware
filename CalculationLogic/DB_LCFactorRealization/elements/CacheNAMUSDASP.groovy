if (api.local.lcFactorRealization?.getRowCount() > 0) {
    api.local.namusdASP = []
    def tableCtx = api.getTableContext()
    def q = tableCtx.newQuery(Constants.LC_TABLE_NAME)
    q.select(Constants.CONCAT_COLUMN)
    q.select(Constants.SUM_EXTENDED_LC, Constants.TOTAL_EXTENDED_USD)
    q.select(Constants.SUM_GROSSBOOKING_LC, Constants.TOTAL_GROSSBOOKING_USD)
    q.where(
            Constants.FILTER_USD
    )
    q.groupBy(Constants.CONCAT_COLUMN)

    def data = tableCtx.executeQuery(q)
    if (data?.getRowCount() > 0) {
        api.local.namusdData = data
    }
    return
}