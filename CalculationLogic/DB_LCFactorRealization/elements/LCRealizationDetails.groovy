if (out.DataSelection) {
    if (api.local.lcFactorRealization?.getRowCount() == 0) {
        api.local.resultMatrix = api.newMatrix("")
        warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.WARNING, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
        api.local.resultMatrix?.addRow(warningCell)
        warningCell = api.local.resultMatrix?.styledCell(Constants.NORECORDS_WARNING_MESSAGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
        api.local.resultMatrix?.addRow(warningCell)
        return api.local.resultMatrix
    }
    BigDecimal netUplift = 0.00
    BigDecimal realisation = 0.00
    BigDecimal lcTAM = 0.00
    BigDecimal realisedUplift = 0.00
    api.local.lcFactorRealization?.each { lcData ->
        data = [
                (Constants.UNIQUEID_LABEL)         : lcData?.UniqueID,
                (Constants.CONCAT_LABEL)           : lcData?.Concat,
                (Constants.QUARTER_LABEL)          : lcData?.FiscalQuarter?.length() > 2 ? lcData?.FiscalQuarter?.substring(lcData?.FiscalQuarter?.length() - 2) : '',
                (Constants.EXTENDED_BASE_USD_LABEL): lcData?.ExtendedBaseUSD ? lcData?.ExtendedBaseUSD?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00 as BigDecimal,
                (Constants.EXTENDED_LC_USD_LABEL)  : lcData?.ExtendedlistPriceLC ? lcData?.ExtendedlistPriceLC?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00 as BigDecimal,
                (Constants.UPLIFT_USD_LABEL)       : lcData?.UpliftUSD ? lcData?.UpliftUSD?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00 as BigDecimal,
                (Constants.GROSSBOOKING_USD_LABEL) : lcData?.GrossBookingsLC ? lcData?.GrossBookingsLC?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00 as BigDecimal,
                (Constants.DISCOUNT_DOLLAR_LABEL)  : lcData?.DiscountDollars ? lcData?.DiscountDollars?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00 as BigDecimal,
                (Constants.DISCOUNT_PER_LABEL)     : lcData?.DiscountPercent ? lcData?.DiscountPercent?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00 as BigDecimal,
                (Constants.UPLIFT_PER_LABEL)       : lcData?.UpliftPercentage ? lcData?.UpliftPercentage?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00 as BigDecimal,
                (Constants.ASP_LABEL)              : lcData?.ASP ? lcData?.ASP?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00 as BigDecimal,
                (Constants.NAMUSD_ASP_LABEL)       : api.local.namusdASP[lcData?.Concat]?.NAMUSDASP ?: 0.00 as BigDecimal
        ]

        if (data[(Constants.NAMUSD_ASP_LABEL)] == 0) {
            data[(Constants.NAMUSD_ASP_LABEL)] = 1.00 as BigDecimal
        }

        data[(Constants.NET_UPLIFT_LABEL)] = (((data[(Constants.ASP_LABEL)] - data[(Constants.NAMUSD_ASP_LABEL)]) / data[(Constants.NAMUSD_ASP_LABEL)] ?: 1.00)?.setScale(2, BigDecimal.ROUND_HALF_UP)) ?: 0.00 as BigDecimal

        if (((data[(Constants.NET_UPLIFT_LABEL)]) != 0 && (data[(Constants.UPLIFT_PER_LABEL)]) != 0)) {
            data[(Constants.REALISATION_LABEL)] = (data[(Constants.NET_UPLIFT_LABEL)] / data[(Constants.UPLIFT_PER_LABEL)])?.setScale(2, BigDecimal.ROUND_HALF_UP) ?: 0.00 as BigDecimal
        }

        if (data[(Constants.REALISATION_LABEL)] < 0 || data[(Constants.REALISATION_LABEL)] == null) {
            data[(Constants.ADJ_REALISED_LABEL)] = 0.00 as BigDecimal
        }

        if (data[(Constants.REALISATION_LABEL)] > 0) {
            data[(Constants.ADJ_REALISED_LABEL)] = data[(Constants.REALISATION_LABEL)] as BigDecimal
        }

        if (data[(Constants.REALISATION_LABEL)] > 1) {
            data[(Constants.ADJ_REALISED_LABEL)] = Math.floor(data[(Constants.REALISATION_LABEL)]) as BigDecimal
        }

        data[(Constants.LC_TAM_LABEL)] = ((data[(Constants.GROSSBOOKING_USD_LABEL)] ?: 0.00) * (data[(Constants.UPLIFT_PER_LABEL)] ?: 0.00))?.setScale(2, BigDecimal.ROUND_HALF_UP) ?: 0.00 as BigDecimal
        data[(Constants.REALISED_UPLIFT_LABEL)] = ((data[(Constants.ADJ_REALISED_LABEL)]) * (data[(Constants.LC_TAM_LABEL)]))?.setScale(2, BigDecimal.ROUND_HALF_UP) ?: 0.00 as BigDecimal

        api.local.resultMatrix?.addRow(data)
    }
    return api.local.resultMatrix
} else {
    api.local.resultMatrix = api.newMatrix("")
    warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.WARNING, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    api.local.resultMatrix?.addRow(warningCell)
    warningCell = api.local.resultMatrix?.styledCell(Constants.ALL_INPUTS_WARNING_MESSAGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    api.local.resultMatrix?.addRow(warningCell)
    return api.local.resultMatrix
}
