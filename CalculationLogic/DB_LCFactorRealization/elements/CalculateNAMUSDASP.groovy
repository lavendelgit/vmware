if (api.local.namusdData) {
    BigDecimal extendedUSD = 0.00
    BigDecimal grossbookingsUSD = 0.00
    BigDecimal discountDollars = 0.00
    BigDecimal discountPercentage = 0.00
    BigDecimal aspNAMUSD = 0.00
    Integer upliftUSDPercentage = 0
    api.local.namusdASP = [:]
    api.local.namusdData?.each { namUSD ->
        extendedUSD = namUSD?.(Constants.TOTAL_EXTENDED_USD) ?: 0.00
        grossbookingsUSD = namUSD?.(Constants.TOTAL_GROSSBOOKING_USD) ?: 0.00
        discountDollars = extendedUSD - grossbookingsUSD
        if (extendedUSD > 0) {
            discountPercentage = ((discountDollars ?: 0.00) / extendedUSD)
        }
        aspNAMUSD = ((1 + upliftUSDPercentage) * (1 - discountPercentage))
        api.local.namusdASP[namUSD?.Concat] = [
                (Constants.NAMUSD_ASP): aspNAMUSD ? aspNAMUSD.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00
        ]
    }
    return
}
