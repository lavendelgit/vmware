import groovy.transform.Field

@Field final String PRICING_DISCOUNTING_DM = "PricingandDiscountingDM"
@Field final String DATA_SELECTION_LABEL = "Data Selection"
@Field final String UNIQUEID_LABEL = "Unique ID"
@Field final String CONCAT_LABEL = "Concat"
@Field final String CONCAT_COLUMN = "Concat"
@Field final String QUARTER_LABEL = "Quarter"
@Field final String EXTENDED_BASE_USD_LABEL = "Extended Base USD"
@Field final String EXTENDED_LC_USD_LABEL = "Extended List Price USD"
@Field final String UPLIFT_USD_LABEL = "Uplift USD"
@Field final String GROSSBOOKING_USD_LABEL = "Gross Bookings USD"
@Field final String DISCOUNT_DOLLAR_LABEL = "Discount \$"
@Field final String DISCOUNT_PER_LABEL = "Discount % "
@Field final String UPLIFT_PER_LABEL = "Uplift %"
@Field final String ASP_LABEL = "ASP"
@Field final String NAMUSD_ASP = "NAMUSDASP"
@Field final String NAMUSD_ASP_LABEL = "ASP for NAMUSD"
@Field final String NET_UPLIFT_LABEL = "Net Uplift"
@Field final String REALISATION_LABEL = "Realisation"
@Field final String ADJ_REALISED_LABEL = "Adjusted Realisation"
@Field final String LC_TAM_LABEL = "LC TAM"
@Field final String REALISED_UPLIFT_LABEL = "Realised Uplift"
@Field final String LC_TABLE_NAME = "LC_Realization"
@Field final String SUM_EXTENDED_LC = "ROUND(SUM(ExtendedlistPriceLC),2)"
@Field final String SUM_GROSSBOOKING_LC = "ROUND(SUM(GrossBookingsLC),2)"
@Field final String TOTAL_EXTENDED_USD = "TotalExtendedUSD"
@Field final String TOTAL_GROSSBOOKING_USD = "TotalGrossBookingsUSD"
@Field final String FILTER_USD = "CurrencyCode = 'USD'"
@Field final String ALL_INPUTS_WARNING_MESSAGE = "Please select input from Data Selection Filter"
@Field final String NORECORDS_WARNING_MESSAGE = "No Records found for the selection"


