if (api.local.lcFactorRealization) {
    api.local.resultMatrix = api.newMatrix(Constants.UNIQUEID_LABEL, Constants.CONCAT_LABEL, Constants.QUARTER_LABEL, Constants.EXTENDED_BASE_USD_LABEL, Constants.EXTENDED_LC_USD_LABEL, Constants.UPLIFT_USD_LABEL, Constants.GROSSBOOKING_USD_LABEL, Constants.DISCOUNT_DOLLAR_LABEL, Constants.DISCOUNT_PER_LABEL, Constants.UPLIFT_PER_LABEL, Constants.ASP_LABEL, Constants.NAMUSD_ASP_LABEL, Constants.NET_UPLIFT_LABEL, Constants.REALISATION_LABEL, Constants.ADJ_REALISED_LABEL, Constants.LC_TAM_LABEL, Constants.REALISED_UPLIFT_LABEL)
    api.local.resultMatrix?.setEnableClientFilter(true)
}
return