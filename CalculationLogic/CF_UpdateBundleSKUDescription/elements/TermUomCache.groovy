Map validUOMCache = libs.stdProductLib.RulesGenHelper.cacheCommonAttributes(out.Configuration.TERM_TABLE, false)
return validUOMCache.collectEntries { [(it.key): it.value.attribute5] }