List filter = [
        Filter.equal("name", "StandardProducts"),
        Filter.in("attribute1", out.ProductCache?.keySet()),
]

recordStream = api.stream("PX50", null, *filter)
api.local.standardProductCache = recordStream?.collect { row -> row }
recordStream?.close()
return