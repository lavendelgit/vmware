List filter = [
        Filter.equal("name", Constants.PRODUCT_RULE),
        Filter.in("attribute1", out.ProductCache?.keySet())
]
recordStream = api.stream("PX30", null, *filter)
List productRules = recordStream?.collect { row -> row }
recordStream?.close()
api.local.ProductRules = libs.stdProductLib.RulesGenHelper.cacheValidRules(productRules, out.ProductRules, out.RuleKeyMapping, out.AttributeDelimiterCache)
return