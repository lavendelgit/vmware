String sku = ""
String shortDesc = ""
api.local.descriptionCache = []
String warning
Integer shortDescLen = out.SKUConfigCache?.ShortDescLen as Integer
shortDescLen = shortDescLen ?: Constants.SHORT_DESC_DEFAULT_LEN
Integer longDescLen = out.SKUConfigCache?.LongDescLen as Integer
longDescLen = longDescLen ?: Constants.LONG_DESC_DEFAULT_LEN
for (productRule in api.local.ProductRules) {
    longDesc = [:]
    rule = productRule.value
    key = productRule.key.split("\\" + out.AttributeDelimiterCache)?.collect { it }
    pXParams = [
            "Product"          : rule.Product == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.ProductCache[rule.Product],
            "Offer"            : rule.Offer == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.OfferCache[rule.Offer],
            "DataCenter"       : rule[Constants.DATA_CENTER] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.DataCenterCache[rule[Constants.DATA_CENTER]],
            "Base"             : rule.Base == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.BaseCache[rule.Base],
            "Metric"           : rule.Metric == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.MetricCache[rule.Metric],
            "Term"             : rule.Term == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.TermCache[rule.Term],
            "PaymentType"      : rule[Constants.PAYMENT_TYPE] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.PaymentTypeCache[rule[Constants.PAYMENT_TYPE]],
            "VolumeTier"       : rule.Tier == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.VolumeTierCache[rule.Tier],
            "Segment"          : rule.Segment == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.SegmentCache[rule.Segment],
            "Version"          : rule.Version == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.VersionCache[rule.Version],
            "Promotion"        : rule.Standard == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.PromoCache[rule.Standard],
            "OS"               : rule.OS == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.OSCache[rule.OS],
            "Quantity"         : rule.Quantity == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.QuantityCache[rule.Quantity],
            "PurchasingProgram": rule[Constants.PURCHASING_PROGRAM] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.PurchasingProgramCache[rule[Constants.PURCHASING_PROGRAM]],
            "TermUoM"          : rule.Term == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.TermUomCache[rule.Term] ?: Constants.NULL_ATTRIBUTE,
            "SupportType"      : rule[Constants.SUPPORT_TYPE] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.SupportTypeCache[rule[Constants.SUPPORT_TYPE]],
            "SupportTier"      : rule[Constants.SUPPORT_TIER] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.SupportTierCache[rule[Constants.SUPPORT_TIER]],
            "Hosting"          : rule.Hosting == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.HostingCache[rule.Hosting],
            "ProgramOption"    : rule[Constants.PROGRAM_OPTION] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : out.ProgramOptionCache[rule[Constants.PROGRAM_OPTION]],
            "ProductKey"       : key[Constants.PRODUCT_KEY_INDEX] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[Constants.PRODUCT_KEY_INDEX],
            "OfferKey"         : key[Constants.OFFER_KEY_INDEX] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[Constants.OFFER_KEY_INDEX],
            "BaseKey"          : key[Constants.BASE_KEY_INDEX] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[Constants.BASE_KEY_INDEX],
            "OSKey"            : key[Constants.OS_KEY_INDEX] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[Constants.OS_KEY_INDEX],
            "MetricKey"        : key[Constants.METRIC_KEY_INDEX] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[Constants.METRIC_KEY_INDEX],
            "DataCenterKey"    : key[Constants.DATACENTER_KEY_INDEX] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[Constants.DATACENTER_KEY_INDEX],
            "SegmentKey"       : key[Constants.SEGMENT_KEY_INDEX] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[Constants.SEGMENT_KEY_INDEX],
            "QuantityKey"      : key[Constants.QUANTITY_KEY_INDEX] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[Constants.QUANTITY_KEY_INDEX],
            "TermKey"          : key[Constants.TERM_KEY_INDEX] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[Constants.TERM_KEY_INDEX]
    ]
    paymentIndex = pXParams.TermUoM ? Constants.PAYMENT_INDEX : Constants.TERMUOM_KEY_INDEX
    pXParams.PaymentTypeKey = key[paymentIndex] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[paymentIndex]
    pXParams.SupportTypeKey = key[paymentIndex + Constants.SUPPORTTYPE_KEY] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[paymentIndex + Constants.SUPPORTTYPE_KEY]
    pXParams.SupportTierKey = key[paymentIndex + Constants.SUPPORTTIER_KEY] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[paymentIndex + Constants.SUPPORTTIER_KEY]
    pXParams.PurchasingProgramKey = key[paymentIndex + Constants.PURCHASINGPROGRAM_KEY] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[paymentIndex + Constants.PURCHASINGPROGRAM_KEY]
    pXParams.VolumeTierKey = key[paymentIndex + Constants.TIER_KEY] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[paymentIndex + Constants.TIER_KEY]
    pXParams.VersionKey = key[paymentIndex + Constants.VERSION_KEY] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[paymentIndex + Constants.VERSION_KEY]
    pXParams.PromotionKey = key[paymentIndex + Constants.PROMOTION_KEY] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[paymentIndex + Constants.PROMOTION_KEY]
    pXParams.HostingKey = key[paymentIndex + Constants.HOSTING] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[paymentIndex + Constants.HOSTING]
    pXParams.ProgramOptionKey = key[paymentIndex + Constants.PROGRAMOPTION] == Constants.PLACE_HOLDER ? Constants.NULL_ATTRIBUTE : key[paymentIndex + Constants.PROGRAMOPTION]
    sku = libs.stdProductLib.SkuGenHelper.createSKU(pXParams)
    shortDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.ShortDescConfig, pXParams)
    longDescription = libs.stdProductLib.SkuGenHelper.createDesc(out.LongDescConfig, pXParams)
    shortDescTrimmed = shortDesc
    longDescTrimmed = longDescription
    if (shortDesc.length() > shortDescLen) {
        shortDescTrimmed = shortDesc?.substring(0, shortDescLen)
        warning = Constants.SHORT_DESC_TRUNCATED_MSG
    }
    if (longDescription.length() > longDescLen) {
        longDescTrimmed = longDescription?.substring(0, longDescLen)
        warning = warning?.length() > 0 ? Constants.SHORT_LONG_DESC_TRUNCATED_MSG : Constants.LONG_DESC_TRUNCATED_MSG
    }
    api.local.descriptionCache << [out.StandardDescriptionPPCache, sku, api.jsonEncode([Product: pXParams.ProductKey, Offer: pXParams.OfferKey, Base: pXParams.BaseKey, ShortDescription: shortDescTrimmed, LongDescription: longDescTrimmed, ActualShortDescription: shortDesc, ActualLongDescription: longDescription, Warning: warning])]
}
return
