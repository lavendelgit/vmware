import groovy.transform.Field

@Field final String STANDARD_TABLE = "StandardProducts"
@Field final String PRODUCT_RULE = "BundleProductRules"
@Field final String SKU_TYPE = "Bundle"
@Field final String SKUCONFIG_TABLE_NAME = "SKUConfig"
@Field final String SHORT_DESCRIPTION_LENGTH = "ShortDescLen"
@Field final String LONG_DESCRIPTION_LENGTH = "LongDescLen"
@Field final Integer PRODUCT_KEY_INDEX = 0
@Field final Integer OFFER_KEY_INDEX = 1
@Field final Integer BASE_KEY_INDEX = 2
@Field final Integer OS_KEY_INDEX = 3
@Field final Integer METRIC_KEY_INDEX = 4
@Field final Integer DATACENTER_KEY_INDEX = 5
@Field final Integer SEGMENT_KEY_INDEX = 6
@Field final Integer QUANTITY_KEY_INDEX = 7
@Field final Integer TERM_KEY_INDEX = 8
@Field final Integer TERMUOM_KEY_INDEX = 9
@Field final Integer PAYMENT_INDEX = 10
@Field final Integer SUPPORTTYPE_KEY = 1
@Field final Integer SUPPORTTIER_KEY = 2
@Field final Integer PURCHASINGPROGRAM_KEY = 3
@Field final Integer TIER_KEY = 4
@Field final Integer VERSION_KEY = 5
@Field final Integer PROMOTION_KEY = 6
@Field final Integer HOSTING = 7
@Field final Integer PROGRAMOPTION = 8
@Field final Integer LONGDESC_START_INDEX = 0
@Field final Integer LONGDESC_END_INDEX = 1019
@Field final Integer LONGDESC_ATTRIBUTEONE_START = 255
@Field final Integer LONGDESC_ATTRIBUTEONE_END = 256
@Field final Integer LONGDESC_ATTRIBUTETWO_START = 510
@Field final Integer LONGDESC_ATTRIBUTETWO_END = 511
@Field final Integer LONGDESC_ATTRIBUTETHREE_START = 764
@Field final Integer LONGDESC_ATTRIBUTETHREE_END = 765
@Field final String KEY_NAME = "name"
@Field final List DESCRIPTION_ATTRIBUTE = ["name", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6", "attribute7"]
@Field final String NULL_ATTRIBUTE = null
@Field final String PLACE_HOLDER = "-"
@Field final String DATA_CENTER = "Data Center"
@Field final String PAYMENT_TYPE = "Payment Type"
@Field final String PURCHASING_PROGRAM = "Purchasing Program"
@Field final String SUPPORT_TYPE = "Support Type"
@Field final String SUPPORT_TIER = "Support Tier"
@Field final String PROGRAM_OPTION = "Program Option"
@Field final Integer SHORT_DESC_DEFAULT_LEN = 240
@Field final Integer LONG_DESC_DEFAULT_LEN = 4000
@Field final String SHORT_DESC_TRUNCATED_MSG = "Short Desc Truncated"
@Field final String LONG_DESC_TRUNCATED_MSG = "Long Desc Truncated"
@Field final String SHORT_LONG_DESC_TRUNCATED_MSG = "Short and Long Desc Truncated"






