api.local.roundingRulesCache = [:]
api.local.roundingRulesCache = libs.vmwareUtil.PricingHelper.cacheAdjustments(api.local.stdAdjustmentsCache, out.StandardAdjustmentMetaData, "attribute26", "attribute25", true)
Map adjustmentCache = [:]
if (api.local.roundingRulesCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.roundingRulesCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = libs.vmwareUtil.PricingHelper.populateTermAltUoMAdjustments(adjustmentCache, out.TermUoMConversionCache)
    if (populatedTermCache?.size() > 0 && api.local.roundingRulesCache?.size() > 0) {
        api.local.roundingRulesCache = api.local.roundingRulesCache + populatedTermCache
    }
}
return