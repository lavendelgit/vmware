List skusForBasePriceCache = out.CacheBaseSKUs?.values() as List
skusForBasePriceCache << out.CacheDestinationSKUs?.values() as List
skusForBasePriceCache = skusForBasePriceCache?.flatten()
List filter = [
        Filter.equal("name", "StandardBasePrice"),
        Filter.in("attribute2", [Constants.SKU_TYPE_STANDALONE, Constants.SKU_TYPE_BUNDLE]),
        Filter.in("attribute1", skusForBasePriceCache)
]
recordStream = api.stream("PX50", null, ["attribute1", "attribute2", "attribute23", "attribute6"], *filter)
api.local.basePriceDetails = recordStream?.collectEntries {
    basePriceDetail ->
        [
                (basePriceDetail.attribute1): [
                        "Type"               : basePriceDetail.attribute2,
                        "BasePrice"          : basePriceDetail.attribute6 as BigDecimal,
                        "DiscountedBasePrice": basePriceDetail.attribute23 as BigDecimal
                ]
        ]

}
recordStream?.close()
return