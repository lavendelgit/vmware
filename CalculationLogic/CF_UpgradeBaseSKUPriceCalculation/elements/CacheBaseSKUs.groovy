return api.findLookupTableValues("BaseToUpgrade", Filter.in("key2", api.local.upgradeBaseSKUs?.collect { it.attribute1 }))?.collectEntries {
    [(it.key2): it.key1]
}
