return api.findLookupTableValues("TermUoMConversion", null)?.collectEntries {
    term ->
        [
                (term.key1 + "_" + term.key2): [
                        "SrcTerm" : term.key1,
                        "SrcUoM"  : term.key2,
                        "DestTerm": term.attribute1,
                        "DestUoM" : term.attribute2
                ]
        ]
}