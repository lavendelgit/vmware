return api.findLookupTableValues("UpgradeToDestination", Filter.in("key1", api.local.upgradeBaseSKUs?.collect { it.attribute1 }))?.collectEntries {
    [(it.key1): it.key2]
}
