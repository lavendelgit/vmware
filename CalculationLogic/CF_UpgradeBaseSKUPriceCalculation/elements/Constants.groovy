import groovy.transform.Field

@Field final String STANDARD_ADJUSTMENT_TABLE = "StandardAdjustment"
@Field final String BASE_PRICE_TABLE_NAME = "StandardBasePrice"
@Field final String STANDARD_PRODUCT_TABLE_NAME = "StandardProducts"
@Field final String UPGRADE_BASESKU_TYPE = "Upgrade Base SKU"
@Field final String UPGRADE_FLAG_STD = "Standard"
@Field final String UPGRADE_FLAG_STD_NOUPLIFT = "Standard - No Uplift"
@Field final String UPGRADE_FLAG_NON_STD_CUSTOM = "Non Standard - Custom Rounding\""
@Field final String ROUNDING_CUSTOM_TYPE = "Custom"
@Field final String SKU_TYPE_STANDALONE = "Standalone"
@Field final String SKU_TYPE_BUNDLE = "Bundle"

