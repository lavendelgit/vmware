List filter = [
        Filter.equal("name", Constants.STANDARD_ADJUSTMENT_TABLE),
        Filter.or(Filter.equal("attribute1", "*"), Filter.in("attribute1", api.local.upgradeBaseSKUs?.collect { it.attribute3 }?.unique())),
        Filter.or(Filter.equal("attribute2", "*"), Filter.in("attribute2", api.local.upgradeBaseSKUs?.collect { it.attribute4 }?.unique())),
        Filter.or(Filter.equal("attribute3", "*"), Filter.in("attribute3", api.local.upgradeBaseSKUs?.collect { it.attribute5 }?.unique())),
        Filter.or(Filter.equal("sku", "*"), Filter.in("sku", api.local.upgradeBaseSKUs?.collect { it.attribute1 })),
        Filter.or(Filter.isNotNull("attribute30"), Filter.isNotNull("attribute26"))
]
stdAdjustmentStream = api.stream("PX50", "attribute41", *filter)
api.local.stdAdjustmentsCache = stdAdjustmentStream?.collect { row -> row }
stdAdjustmentStream?.close()
return