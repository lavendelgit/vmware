api.local.upgradeSKUDetails = [:]
String baseSKU
String flag
String destinationSKU
BigDecimal baseSKUPrice = 0.0
BigDecimal destinationSKUPrice = 0.0
BigDecimal upgradeAdjustment = 0.0
BigDecimal difference = 0.0
BigDecimal upgradePrice = 0.0
Map roundingRule = [:]
for (upgradeBaseSKU in api.local.upgradeBaseSKUs) {
    roundingRule = [:]
    baseSKU = out.CacheBaseSKUs[upgradeBaseSKU.attribute1]
    flag = Constants.UPGRADE_FLAG_STD
    destinationSKU = out.CacheDestinationSKUs[upgradeBaseSKU.attribute1]
    baseSKUPriceInfo = api.local.basePriceDetails[baseSKU]
    destinationSKUPriceInfo = api.local.basePriceDetails[destinationSKU]
    baseSKUPrice = (baseSKUPriceInfo?.Type == Constants.SKU_TYPE_STANDALONE) ? baseSKUPriceInfo?.BasePrice : (baseSKUPriceInfo?.DiscountedBasePrice ?: baseSKUPriceInfo?.BasePrice)
    baseSKUPrice = baseSKUPrice ?: 0 as BigDecimal
    destinationSKUPrice = destinationSKUPriceInfo?.Type == Constants.SKU_TYPE_STANDALONE ? destinationSKUPriceInfo.BasePrice : (destinationSKUPriceInfo?.DiscountedBasePrice ?: destinationSKUPriceInfo?.BasePrice)
    destinationSKUPrice = destinationSKUPrice ?: 0 as BigDecimal
    upgradeAdjustment = libs.vmwareUtil.PricingHelper.getAdjustments(api.local.upgradeAdjustmentCache, upgradeBaseSKU.attribute1, upgradeBaseSKU.attribute3, upgradeBaseSKU.attribute4, upgradeBaseSKU.attribute5)
    roundingRule = libs.vmwareUtil.PricingHelper.getPrecision(api.local.roundingRulesCache, upgradeBaseSKU.attribute1, upgradeBaseSKU.attribute3, upgradeBaseSKU.attribute4, upgradeBaseSKU.attribute5, ["*", "*"])
    precision = (roundingRule?.Precision || roundingRule?.Precision == 0 ? roundingRule?.Precision : 2) as Integer
    roundingType = roundingRule?.RoundingType ?: "Round"
    difference = destinationSKUPrice - baseSKUPrice
    upgradePrice = difference + (upgradeAdjustment / 100) * difference
    if (upgradePrice > destinationSKUPrice) {
        flag = Constants.UPGRADE_FLAG_STD_NOUPLIFT
    }
    if (roundingRule?.RoundingType == Constants.ROUNDING_CUSTOM_TYPE) {
        flag = Constants.UPGRADE_FLAG_NON_STD_CUSTOM
    }
    upgradePrice = upgradePrice <= destinationSKUPrice ? upgradePrice : destinationSKUPrice
    listPrice = libs.vmwareUtil.PricingHelper.roundedPrice(upgradePrice, roundingType, precision)
    api.local.upgradeSKUDetails[upgradeBaseSKU.attribute1] = [
            upgradePrice: listPrice,
            validFrom   : upgradeBaseSKU.attribute29,
            flag        : flag,
            attribute1  : upgradeBaseSKU.attribute1,
            sku         : upgradeBaseSKU.sku
    ]
}
return

