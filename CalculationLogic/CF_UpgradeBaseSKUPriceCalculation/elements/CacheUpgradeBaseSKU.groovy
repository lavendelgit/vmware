List filter = [
        Filter.equal("name", Constants.BASE_PRICE_TABLE_NAME),
        Filter.equal("attribute2", Constants.UPGRADE_BASESKU_TYPE),
        Filter.equal("attribute27", libs.stdProductLib.ConstConfiguration.BUNDLE_CALCULATION_FLAG)
]
recordStream = api.stream("PX50", null, *filter)
api.local.upgradeBaseSKUs = recordStream?.collect { it }
recordStream?.close()
return
