for (upgradeSKU in api.local.upgradeSKUDetails) {
    Map record = [
            name       : "StandardBasePrice",
            sku        : upgradeSKU.value.sku,
            attribute29: upgradeSKU.value.validFrom,
            attribute1 : upgradeSKU.value.attribute1,
            attribute6 : upgradeSKU.value.upgradePrice,
            attribute33: upgradeSKU.value.flag,
    ]
    if (record) api.addOrUpdate("PX50", record)
}