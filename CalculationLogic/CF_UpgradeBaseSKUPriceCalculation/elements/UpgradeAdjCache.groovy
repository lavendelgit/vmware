api.local.upgradeAdjustmentCache = [:]
Map populatedTermCache = [:]
api.local.upgradeAdjustmentCache = libs.vmwareUtil.PricingHelper.cacheAdjustments(api.local.stdAdjustmentsCache, out.StandardAdjustmentMetaData, "attribute30")
Map adjustmentCache = [:]
if (api.local.upgradeAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
    api.local.upgradeAdjustmentCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = libs.vmwareUtil.PricingHelper.populateTermAltUoMAdjustments(adjustmentCache, out.TermUoMConversionCache)
    if (populatedTermCache?.size() > 0 && api.local.upgradeAdjustmentCache?.size() > 0) {
        api.local.upgradeAdjustmentCache = api.local.upgradeAdjustmentCache + populatedTermCache
    }
}
return