BigDecimal managedPremiumPercentage = api.global.termAttributes[out.Term + "_" + out.Service]?.getAt("ManagedServicePremium") ?: 0.0
BigDecimal channelPartnerUpliftPercentage = api.global.termAttributes[out.Term + "_" + out.Service]?.ChannelPartnerUplift
BigDecimal onDemandMultiple = api.global.termAttributes[out.Term + "_" + out.Service]?.getAt("OnDemandMultiple") ?: 0.0
BigDecimal managedPremium = (out.SwBundlePerHourCost * (1 + managedPremiumPercentage))?.setScale(6, BigDecimal.ROUND_HALF_UP)
BigDecimal onDemand = (managedPremium * onDemandMultiple)?.setScale(6, BigDecimal.ROUND_HALF_UP)
return (onDemand * (1 + channelPartnerUpliftPercentage))?.setScale(6, BigDecimal.ROUND_HALF_UP)