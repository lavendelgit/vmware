api.global.termAttributes = api.global.termAttributes ?: api.findLookupTableValues("VMCTerm")?.collectEntries {
    termAttribute ->
        [(termAttribute.key1 + "_" + termAttribute.key2): [
                "OnDemandMultiple"     : termAttribute.attribute1,
                "ManagedServicePremium": termAttribute.attribute2,
                "ChannelPartnerUplift" : termAttribute.attribute3 as BigDecimal,
        ]
        ]
}