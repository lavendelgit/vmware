def perHour = api.global.factorConstants?.getAt("Per Hour") as Integer
return perHour > 0 ? (out.PerMonth / perHour).setScale(6, BigDecimal.ROUND_HALF_UP) : 0.0 as BigDecimal