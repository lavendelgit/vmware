BigDecimal AWSHwOnly = api.global.AWSHWPriceBaseDCData[out.Service]?.getAt(out.Term)
BigDecimal hardwareSupportPercentage = api.global.factorConstants?.getAt("Hardware Support") as BigDecimal
BigDecimal hardwareSupport = AWSHwOnly * hardwareSupportPercentage
return AWSHwOnly + hardwareSupport as BigDecimal
