if (out.BundleAdjustmentOverride) {
    return out.BundleAdjustmentOverride
}
if (out.BundlePriceOverride) {
    return out.PerpetualSKUPrice.ActualPrice ? (out.BundlePriceOverride - out.PerpetualSKUPrice.ActualPrice) / out.PerpetualSKUPrice.ActualPrice : 0.0 as BigDecimal
}

return out.PerpetualBundleAdj