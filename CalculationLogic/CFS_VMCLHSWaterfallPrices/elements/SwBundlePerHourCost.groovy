def TCOYears = (api.global.serviceAttributes[out.Service]?.TCOUoM == "YR" ? api.global.serviceAttributes[out.Service]?.TCOYears : (api.global.serviceAttributes[out.Service]?.TCOYears / 12)) as BigDecimal
BigDecimal snsCalculation = out.ActualBundlePrice * api.global.serviceAttributes[out.Service]?.Sns
BigDecimal TcoCalculation = out.ActualBundlePrice + (snsCalculation * TCOYears)
def perMonth = api.global.factorConstants?.getAt("Per Month") as Integer
BigDecimal perMonthCalculation = perMonth > 0 ? (TcoCalculation / perMonth).setScale(6, BigDecimal.ROUND_HALF_UP) : 0.0
def perHour = api.global.factorConstants?.getAt("Per Hour") as Integer
BigDecimal perHourCalculation = perHour > 0 ? (perMonthCalculation / perHour).setScale(6, BigDecimal.ROUND_HALF_UP) : 0.0
def perCore = api.global.factorConstants?.getAt("Per Core") as Integer
BigDecimal perCoreCalculation = perCore > 0 ? (perHourCalculation / perCore).setScale(7, BigDecimal.ROUND_HALF_UP) : 0.0
return (perCoreCalculation * api.global.serviceAttributes[out.Service]?.Hosts)?.setScale(6, BigDecimal.ROUND_HALF_UP) as BigDecimal