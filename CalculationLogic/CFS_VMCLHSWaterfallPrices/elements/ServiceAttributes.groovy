api.global.serviceAttributes = api.global.serviceAttributes ?: api.findLookupTableValues("VMCServiceAttributes")?.collectEntries {
    serviceAttribute ->
        [
                (serviceAttribute.name): [
                        "Hosts"   : serviceAttribute.attribute1 as Integer,
                        "TCOYears": serviceAttribute.attribute3 as Integer,
                        "TCOUoM"  : serviceAttribute.attribute4,
                        "Sns"     : serviceAttribute.attribute2 as BigDecimal
                ]
        ]
}
