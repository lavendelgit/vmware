String baseSKU = "VMC-AWS-" + out.Service
api.global.perpetualSKUPrice = api.global.perpetualSKUPrice ?: [:]
if (!api.global.perpetualSKUPrice?.getAt(baseSKU)) {
    api.switchSKUContext(baseSKU)
    bomList = api.bomList()?.collectEntries { [(it.label): it.quantity] }
    def filters = [
            Filter.equal("name", "VMCPerpetualSKUPrices"),
            Filter.equal("attribute1", out.Service),
            Filter.in("sku", bomList.keySet())
    ]
    def perpetualSKUList = api.find("PX", 0, bomList?.size(), "sku", ["sku", "attribute4", "attribute7"], *filters)

    def perpetualSKUPrice = perpetualSKUList?.inject([ActualPrice: 0, PriceDiscounted: 0]) {
        result, perpetual ->
            BigDecimal qty = bomList[perpetual.sku] as BigDecimal
            BigDecimal actualPrice = ((perpetual.attribute4 as BigDecimal) * qty)
            BigDecimal priceDiscounted = ((perpetual.attribute7 as BigDecimal) * qty)
            [
                    ActualPrice    : result.ActualPrice + actualPrice,
                    PriceDiscounted: result.PriceDiscounted + priceDiscounted
            ]
    }
    api.global.perpetualSKUPrice[baseSKU] = perpetualSKUPrice
}

return api.global.perpetualSKUPrice?.getAt(baseSKU)