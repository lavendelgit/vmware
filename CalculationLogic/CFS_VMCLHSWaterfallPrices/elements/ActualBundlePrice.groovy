if (out.BundlePriceOverride)
    return out.BundlePriceOverride
if (out.BundleAdjustmentOverride) {
    return (out.PerpetualSKUPrice.ActualPrice + ((out.BundleAdjustmentOverride) * out.PerpetualSKUPrice.ActualPrice))?.setScale(0, BigDecimal.ROUND_HALF_UP)
}
return out.PerpetualBundlePrice