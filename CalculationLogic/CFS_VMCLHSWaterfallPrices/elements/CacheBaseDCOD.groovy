api.global.AWSHWPriceBaseDCData = api.global.AWSHWPriceBaseDCData ?: api.findLookupTableValues("VMCAwsHwPrices", Filter.equal("key1", api.global.factorConstants?.getAt("Base DC")))?.collectEntries {
    baseDCData ->
        [
                (baseDCData.key2): [
                        "On Demand": baseDCData.attribute3 as BigDecimal,
                        "1 Year"   : baseDCData.attribute4 as BigDecimal,
                        "3 Year"   : baseDCData.attribute5 as BigDecimal,
                ]
        ]
}