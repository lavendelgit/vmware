def perMonth = api.global.factorConstants?.getAt("Per Month") as Integer
return perMonth > 0 ? (out.TCO / perMonth).setScale(6, BigDecimal.ROUND_HALF_UP) : 0.0 as BigDecimal