api.global.factorConstants = api.global.factorConstants ?: api.findLookupTableValues("VMCFactorConstants")?.collectEntries {
    [(it.name): it.attribute1]
}