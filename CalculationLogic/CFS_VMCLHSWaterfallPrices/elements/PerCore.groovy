def perCore = api.global.factorConstants?.getAt("Per Core") as Integer
return perCore > 0 ? (out.PerHour / perCore).setScale(7, BigDecimal.ROUND_HALF_UP) : 0.0 as BigDecimal
