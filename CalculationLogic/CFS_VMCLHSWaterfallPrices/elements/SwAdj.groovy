BigDecimal difference = (out.CurrentItem?.attribute1 - out.PfxBasePrice)//?.setScale(4,BigDecimal.ROUND_HALF_UP)
BigDecimal swAdj = (out.PfxSwPrice > 0 ? (difference / out.PfxSwPrice) : 0.0) * 100 as BigDecimal
return swAdj?.setScale(4, BigDecimal.ROUND_HALF_UP)