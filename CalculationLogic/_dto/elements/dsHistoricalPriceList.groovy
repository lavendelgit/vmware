import groovy.transform.Field
import net.pricefx.formulaengine.scripting.Matrix2D

@Field final String PRICE_CHANGE_DM = "Price Change DataMart"
@Field final String PRICE_CHANGE_TRANS_YR = "Price Change Transaction Year(s)"
@Field final String PRICE_CHANGE_DATA_FILTER = "Price Change Data Filter"
@Field final String END_DATE = "EndDate"
@Field final String PRICE_CHANGE_EFF_DT = "PriceChangeEffectiveDate"
@Field final String PART_NUMBER = "PartNumber"
@Field final String YYYY_MM_DD = "yyyy-MM-dd"

Matrix2D getData(boolean isDataSource, String dataTableName, List filter, boolean rollup = false, List columns, String orderByField) {
    return libs._dto._generic.getData(isDataSource, dataTableName, filter, rollup, columns, orderByField)
}