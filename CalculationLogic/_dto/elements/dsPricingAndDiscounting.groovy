import groovy.transform.Field
import net.pricefx.formulaengine.scripting.Matrix2D

@Field final String BOOKING_DETAILS_DM = "Booking Details DataMart"
@Field final String BOOKING_DETAILS_TRANS_YR = "Booking Details Transaction Year(s)"
@Field final String BOOKING_DETAILS_DATA_FILTER = "Booking Details Data Filter"
@Field final String BOOKING_DETAILS_ORDER_DATE = "OrderDate"
@Field final String BOOKING_DETAILS_PRODUCT_SKU = "ProductSKU"
@Field final String BOOKING_DETAILS_GROSS_BOOKINGSCC = "GrossBookingsCC"
@Field final List BOOKING_DETAILS_QUERY_COLUMNS = ["ProductSKU", "ProductGroup", "OrderDate", "GrossBookingsCC"]
@Field final String YYYY_MM_DD = "yyyy-MM-dd"
@Field final List LC_FACTOR_COLUMNS = ["UniqueID","Concat","FiscalQuarter","CurrencyCode","ExtendedBaseUSD","ExtendedlistPriceLC","UpliftUSD","GrossBookingsLC","DiscountDollars","DiscountPercent","UpliftPercentage","ASP"]
@Field final String LC_FACTOR_ORDERBY = "Concat"


Matrix2D getData(boolean isDataSource, String dataTableName, List filter, boolean rollup = false, List columns, String orderByField) {
    return libs._dto._generic.getData(isDataSource, dataTableName, filter, rollup, columns, orderByField)
}