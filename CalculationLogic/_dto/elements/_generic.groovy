import net.pricefx.formulaengine.scripting.Matrix2D

String getEndDate(Date date, Integer yearInput) {
    Calendar calendar = api.calendar(date)
    calendar.add(Calendar.YEAR, -(yearInput))
    return calendar.getTime().format(libs._dto.dsHistoricalPriceList.YYYY_MM_DD)
}

Matrix2D getData(boolean isDataSource, String dataTableName, List filter, boolean rollup = false, List columns, String orderByField) {
    def ctx = api.getDatamartContext()
    def dt = isDataSource ? ctx.getDataSource(dataTableName) : ctx.getDatamart(dataTableName)
    def query = ctx.newQuery(dt, rollup)
    if (columns) {
        columns.forEach {
            query.select(it)
        }
    } else {
        query.selectAll()
    }
    if (orderByField) {
        query.orderBy(orderByField)
    }
    query.where(filter)
    return ctx.executeQuery(query)?.getData()
}