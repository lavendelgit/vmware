import groovy.transform.Field

@Field final String PRICE_CHANGE_DM = "Price Change DataMart"
@Field final String PRICE_CHANGE_TRANS_YR = "Price Change Transaction Year(s)"
@Field final String PRICE_CHANGE_DATA_FILTER = "Price Change Data Filter"
@Field final String END_DATE = "EndDate"
@Field final String PRICE_CHANGE_EFF_DT = "PriceChangeEffectiveDate"
@Field final String PART_NUMBER = "PartNumber"
@Field final String YYYY_MM_DD = "yyyy-MM-dd"
@Field final List QUERY_COLUMNS = ["PartNumber", "PriceMonth", "USD", "EUR", "GBP", "JPY", "AUD", "CNY", "EMEAUSD", "EMEAUSD2", "GUSD", "PriceChangeEffectiveDate", "EndDate"]
@Field final String BOOKING_DETAILS_DM = "Booking Details DataMart"
@Field final String BOOKING_DETAILS_TRANS_YR = "Booking Details Transaction Year(s)"
@Field final String BOOKING_DETAILS_DATA_FILTER = "Booking Details Data Filter"
@Field final String BOOKING_DETAILS_ORDER_DATE = "OrderDate"
@Field final String BOOKING_DETAILS_PRODUCT_SKU = "ProductSKU"
@Field final String BOOKING_DETAILS_GROSS_BOOKINGSCC = "GrossBookingsCC"
@Field final List BOOKING_DETAILS_QUERY_COLUMNS = ["ProductSKU", "ProductGroup", "OrderDate", "GrossBookingsCC"]