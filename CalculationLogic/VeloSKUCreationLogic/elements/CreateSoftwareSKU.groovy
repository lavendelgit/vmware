if (api.isSyntaxCheck())
    return

def table = api.findLookupTable("VeloSoftwareSKUs")
def sku2, network, edition, software, gateway, support, service, paymentTerm, paymentType, licenceClass, renewal

Set distinctSoftwareProducts = []
def maxResults = api.getMaxFindResultsLimit()
def ppTable = api.findLookupTable("SoftwareAdjustments")
def recList = api.find("MLTV2", 0, maxResults, null, Filter.equal("lookupTable.id", ppTable?.id))

if (recList)
    recList.each {
        distinctSoftwareProducts.add(it.key1)
    }
api.trace("Record List", null, distinctSoftwareProducts)

distinctSoftwareProducts.each {

    network = it
    def sku = "NB-VC"
    api.global.editionList.each {
        edition = it
        api.global.softwareList.each {
            software = it
            api.global.gatewayList.each {
                gateway = it
                api.global.supportOptionList.each {
                    support = it
                    api.global.serviceLevelList.each {
                        service = it
                        api.global.billingTermList.each {
                            paymentTerm = it.key1.toInteger()
                            paymentType = it.key2

                            def pType
                            if (paymentType == "Prepaid") pType = "P"
                            else if (paymentType == "Monthly") pType = "M"
                            else if (paymentType == "Annual") pType = "A"
                            api.global.renewalList.each {
                                renewal = it
                                api.global.licenseClassList.each {
                                    licenceClass = it
                                    def sku1
                                    if (gateway && gateway.trim().length() > 0)
                                        sku1 = sku + network + "-" + edition + "-" + software + "-" + gateway + "-"
                                    else
                                        sku1 = sku + network + "-" + edition + "-" + software + "-"

                                    if (renewal.trim().length() > 0)
                                        sku2 = sku1 + support + service + "-" + paymentTerm * 12 + pType + "-" + renewal + "-" + licenceClass
                                    else
                                        sku2 = sku1 + support + service + "-" + paymentTerm * 12 + pType + "-" + licenceClass

                                    def record = [
                                            "lookupTableId"  : table?.id,
                                            "lookupTableName": table.uniqueName,
                                            "name"           : sku2,
                                            "attribute1"     : "Description",
                                            "attribute2"     : network,
                                            "attribute3"     : edition,

                                            "attribute4"     : software,
                                            "attribute5"     : gateway,
                                            "attribute6"     : support,

                                            "attribute7"     : service,
                                            "attribute8"     : paymentTerm,
                                            "attribute9"     : paymentType,
                                            "attribute10"    : licenceClass
                                    ]
                                    if ((paymentTerm != 2 || paymentTerm != 4) && (licenceClass == "C" || licenceClass == "F"))
                                        api.addOrUpdate("MLTV", record)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}