// Common to Hardware and Software
def licenseClassMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("LicenseClass")
api.global.licenseClassList = licenseClassMap.keySet()

def softwareOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("SoftwareOptions")
api.global.softwareList = softwareOptionsMap.keySet()

def renewalOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("RenewalOptions")
api.global.renewalList = renewalOptionsMap.keySet()

def maxResults = api.getMaxFindResultsLimit()
def billingTermFactorTable = api.findLookupTable("BillingTermsFactor")
api.global.billingTermList = api.find("MLTV2", 0, maxResults, null, Filter.equal("lookupTable.id", billingTermFactorTable.id))

// Hardware Only
def locationOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("LocationOptions")
api.global.locationOptionsList = locationOptionsMap.keySet()

def basePriceMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("HardwareBasePrice")
api.global.basePriceList = basePriceMap?.keySet()

// Software Only
def gatewayOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("GatewayOptions")
api.global.gatewayList = gatewayOptionsMap.keySet()

def serviceLevelFactorMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("ServiceLevelFactor")
api.global.serviceLevelList = serviceLevelFactorMap.keySet()

def supportOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("SupportOptions")
api.global.supportOptionList = supportOptionsMap.keySet()

def editionMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("EditionOptions")
api.global.editionList = editionMap.keySet()

// Accessory Factors Only
def accessoryTypeOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("AccessoryTypeOptions")
api.global.accessoryTypeOptionsList = accessoryTypeOptionsMap.keySet()

def regionOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("RegionOptions")
api.global.regionOptionsList = regionOptionsMap.keySet()

// Add On Factors Only
def wanTypeOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("WANAddOnTypeOptions")
api.global.wanTypeOptionsList = wanTypeOptionsMap.keySet()

def purchaseOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("PurchaseAvailabilityOptions")
api.global.purchaseOptionsList = purchaseOptionsMap.keySet()

def addOnLocationOptionsMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("AddonLocationOptions")
api.global.addOnLocationOptionsList = addOnLocationOptionsMap.keySet()


