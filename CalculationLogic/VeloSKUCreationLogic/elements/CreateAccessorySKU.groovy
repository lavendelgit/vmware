/*if (api.isSyntaxCheck())  
	return

def accessorySKU = "NB-VC"
def maxResults = api.getMaxFindResultsLimit()
def table = api.findLookupTable("VeloAccessorySKUs")
def edgeModel, accesoryType, region, license

def basePriceListMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("HardwareBasePrice")
def edgeModelSet = basePriceListMap?.keySet()

edgeModelSet.each{
  edgeModel = it  
  api.global.accessoryTypeOptionsList.each{
    accesoryType = it
    api.global.regionOptionsList.each{
      	region = it
      	api.global.licenseClassList.each{
          	licenseClass = it
          	api.global.billingTermList.each{               
                paymentTerm = it.key1.toInteger()
                paymentType = it.key2

                def pType              
                if(paymentType == "Prepaid") 		pType = "P"
                else if(paymentType == "Monthly")	pType = "M"
                else if(paymentType == "Annual") 	pType = "A"
                  
                accessorySKU1 = accessorySKU + "-" + "EDG"  + "-" + accesoryType + "-" + region + "-" + pType  + "-" + licenseClass

                def record = [ 
                  "lookupTableId":table.id,        
                  "lookupTableName":table.uniqueName,       
                  "name"	  : accessorySKU1, 
                  "attribute1": "Description",
                  "attribute2": "EDG",
                  "attribute3": accesoryType,
                  "attribute4": region,
                  "attribute5": licenseClass,
                  "attribute6": paymentType,
                  "attribute7": "",
                  "attribute8": ""              
                ]       
                api.trace("sku", null, accessorySKU1)
                if( (licenseClass == "C" || licenseClass == "F") && paymentType == "Prepaid" )
                    api.addOrUpdate("MLTV", record)  
            }
        }
    }
  }
}*/