if (api.isSyntaxCheck())
    return

Set deployedSKU = []
def prodStartRow = 0
def maxResults = api.getMaxFindResultsLimit()

while (prodTab = api.find("P", prodStartRow, Filter.in("attribute8", "Velo Add-On", "Software", "Hardware Capex", "Rental", "Extended Replacement Service"),
)
) {
    prodStartRow += prodTab?.size()
    if (prodTab) {
        prodTab.each {

            def sku = it.sku
            deployedSKU.add(sku)
        }
    }
}

//AddOn
Set addOnSKUList = []
prodStartRow = 0
def addOnSKUTable = api.findLookupTable("VeloAddonSKUs")
while (addonProdList = api.find("MLTV", prodStartRow, maxResults, null, Filter.equal("lookupTable.id", addOnSKUTable.id))
) {
    prodStartRow += addonProdList?.size()
    if (addonProdList)
        addonProdList.each {
            addOnSKUList.add(it.name)
        }
}

//Hardware
prodStartRow = 0
Set hardwareSKUList = []
def hardwareSKUTable = api.findLookupTable("VeloHardwareSKUs")
while (hwProdList = api.find("MLTV", prodStartRow, maxResults, null, Filter.equal("lookupTable.id", hardwareSKUTable.id))
) {
    prodStartRow += hwProdList?.size()
    if (hwProdList)
        hwProdList.each {
            hardwareSKUList.add(it.name)
        }
}

//Software
prodStartRow = 0
Set softwareSKUList = []
def softwareSKUTable = api.findLookupTable("VeloSoftwareSKUs")
while (swProdList = api.find("MLTV", prodStartRow, maxResults, null, Filter.equal("lookupTable.id", softwareSKUTable.id))
) {
    prodStartRow += swProdList?.size()
    if (swProdList)
        swProdList.each {
            softwareSKUList.add(it.name)
        }
}

api.trace("deployedSKU", null, deployedSKU.size())
api.trace("addonSKUList", null, addOnSKUList.size())
api.trace("hardwareSKUList", null, hardwareSKUList.size())
api.trace("softwareSKUList", null, softwareSKUList.size())

def addonMismatch = addOnSKUList.minus(deployedSKU)
api.trace("AddOn Mismatch Count : ", null, addonMismatch.size())
api.trace("AddOn Mismatch SKU's : ", null, addonMismatch.toString())

def hwMismatch = hardwareSKUList.minus(deployedSKU)
api.trace("Hardware Mismatch Count : ", null, hwMismatch.size())
api.trace("Hardware Mismatch SKU's : ", null, hwMismatch.toString())

def swMismatch = softwareSKUList.minus(deployedSKU)
api.trace("Software Mismatch Count : ", null, swMismatch.size())
api.trace("Software Mismatch SKU's : ", null, swMismatch.toString())


// Updating Flag in Hardware SKU Table
hardwareSKUList = hardwareSKUList.minus(deployedSKU)
hardwareSKUList.each {
    def record = [
            "lookupTableId"  : hardwareSKUTable.id,
            "lookupTableName": hardwareSKUTable.uniqueName,
            "name"           : it,
            "attribute9"     : "New"
    ]
    api.addOrUpdate("MLTV", record)
}

// Updating Flag in Software SKU Table
softwareSKUList = hardwareSKUList.minus(deployedSKU)
softwareSKUList.each {
    def record = [
            "lookupTableId"  : softwareSKUTable.id,
            "lookupTableName": softwareSKUTable.uniqueName,
            "name"           : it,
            "attribute11"    : "New"
    ]
    api.addOrUpdate("MLTV", record)
}

// Updating Flag in Addon SKU Table
addOnSKUList = hardwareSKUList.minus(deployedSKU)
addOnSKUList.each {
    def record = [
            "lookupTableId"  : addOnSKUTable.id,
            "lookupTableName": addOnSKUTable.uniqueName,
            "name"           : it,
            "attribute11"    : "New"
    ]
    api.addOrUpdate("MLTV", record)
}