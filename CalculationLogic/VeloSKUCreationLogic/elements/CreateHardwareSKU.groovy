if (api.isSyntaxCheck())
    return

def maxResults = api.getMaxFindResultsLimit()
def table = api.findLookupTable("VeloHardwareSKUs")
def hwModel, paymentTerm, paymentType, replacementService
def licenseClass, locationClass, softwareClass

Set distinctReplSet = []
def replacementServiceTable = api.findLookupTable("ReplacementFactor")
def repFList = api.find("MLTV2", 0, maxResults, null, Filter.equal("lookupTable.id", replacementServiceTable.id)
)
if (repFList)
    repFList.each {
        distinctReplSet.add(it.key1)
    }


api.global.basePriceList.each {
    hwModel = it
    def capexSKU = "NB-VC" + "-" + hwModel
    def extReplSKU = "NB-VC" + hwModel
    def rentalSKU = "NB-VC" + hwModel

    api.global.locationOptionsList.each {
        def capexSKU1
        def extReplSKU1
        def rentalSKU1
        if (hwModel == "510LTE") {
            locationClass = it
            if (locationClass.trim().length() > 0) {
                capexSKU1 = capexSKU + "-" + locationClass
                extReplSKU1 = extReplSKU + "-" + locationClass
                rentalSKU1 = rentalSKU + "-" + locationClass
            }
        } else {
            locationClass = ""
            capexSKU1 = capexSKU
            extReplSKU1 = extReplSKU
            rentalSKU1 = rentalSKU
        }

        if (api.global.billingTermList) {
            api.global.billingTermList.each {
                paymentTerm = it.key1.toInteger()
                paymentType = it.key2

                distinctReplSet.each {
                    def pType
                    replacementService = it
                    if (paymentType == "Prepaid") pType = "P"
                    else if (paymentType == "Monthly") pType = "M"
                    else if (paymentType == "Annual") pType = "A"

                    //api.global.renewalList
                    api.global.softwareList.each {
                        softwareClass = it
                        def capexSKU2 = capexSKU1 + "-" + softwareClass + "-" + paymentTerm * 12 + "-" + pType + "-" + replacementService
                        def extReplSKU2 = extReplSKU1 + "-" + "EXT" + "-" + replacementService + "-" + paymentTerm * 12 + pType
                        def rentalSKU2 = rentalSKU1 + "-" + paymentTerm * 12 + pType + "-" + replacementService

                        api.global.licenseClassList.each {
                            licenseClass = it

                            def capexSKU3 = capexSKU2 + "-" + licenseClass
                            def extReplSKU3 = extReplSKU2 + "-" + licenseClass
                            def rentalSKU3 = rentalSKU2 + "-" + licenseClass

                            def record1 = [
                                    "lookupTableId"  : table.id,
                                    "lookupTableName": table.uniqueName,
                                    "name"           : capexSKU3,
                                    "attribute1"     : "Description",
                                    "attribute2"     : hwModel,
                                    "attribute3"     : paymentTerm,
                                    "attribute4"     : paymentType,
                                    "attribute5"     : replacementService,
                                    "attribute6"     : licenseClass,
                                    "attribute7"     : locationClass,
                                    "attribute8"     : softwareClass,
                                    "attribute10"    : "Hardware Capex"
                            ]
                            if ((paymentTerm != 2 || paymentTerm != 4) && paymentType == "Prepaid" && (licenseClass == "C" || licenseClass == "F"))
                                api.addOrUpdate("MLTV", record1)

                            def record2 = [
                                    "lookupTableId"  : table.id,
                                    "lookupTableName": table.uniqueName,
                                    "name"           : extReplSKU3,
                                    "attribute1"     : "Description",
                                    "attribute2"     : hwModel,
                                    "attribute3"     : paymentTerm,
                                    "attribute4"     : paymentType,
                                    "attribute5"     : replacementService,
                                    "attribute6"     : licenseClass,
                                    "attribute7"     : locationClass,
                                    "attribute8"     : "",
                                    "attribute10"    : "Extended Replacement Service"
                            ]
                            if ((licenseClass == "C" || licenseClass == "F") && (paymentTerm != 3 || paymentTerm != 5))
                                api.addOrUpdate("MLTV", record2)

                            record3 = [
                                    "lookupTableId"  : table.id,
                                    "lookupTableName": table.uniqueName,
                                    "name"           : rentalSKU3,
                                    "attribute1"     : "Description",
                                    "attribute2"     : hwModel,
                                    "attribute3"     : paymentTerm,
                                    "attribute4"     : paymentType,
                                    "attribute5"     : replacementService,
                                    "attribute6"     : licenseClass,
                                    "attribute7"     : locationClass,
                                    "attribute8"     : "",
                                    "attribute10"    : "Rental"
                            ]
                            if ((paymentTerm != 2 || paymentTerm != 4) && (licenseClass == "C" || licenseClass == "F"))
                                api.addOrUpdate("MLTV", record3)

                            //api.trace("capexSKU", null, capexSKU3)
                            //api.trace("extReplSKU", null, extReplSKU3)
                            //api.trace("rentalSKU", null, rentalSKU3)

                        }
                    }
                }
            }
        }
    }
}