if (api.isSyntaxCheck())
    return

def addOnSKU
def sku = "NB-VC"
def maxResults = api.getMaxFindResultsLimit()
def table = api.findLookupTable("VeloAddonSKUs")
def bwOption, purchaseOption, paymentTerm, paymentType, licenseClass

Set distinctBandwidthOptions = []
def ppTable = api.findLookupTable("SoftwareAdjustments")
def recList = api.find("MLTV2", 0, maxResults, null, Filter.equal("lookupTable.id", ppTable?.id))

if (recList)
    recList.each {
        distinctBandwidthOptions?.add(it.key1)
    }

distinctBandwidthOptions.each {
    bwOption = it
    api.global.purchaseOptionsList.each {
        purchaseOption = it
        api.global.billingTermList.each {
            paymentTerm = it.key1.toInteger()
            paymentType = it.key2

            def pType
            if (paymentType == "Prepaid") pType = "P"
            else if (paymentType == "Monthly") pType = "M"
            else if (paymentType == "Annual") pType = "A"
            api.global.licenseClassList.each {
                licenseClass = it

                addOnSKU = sku + bwOption + "-" + "AD-NVSVPN-" + purchaseOption + "-" + paymentTerm * 12 + pType + "-" + licenseClass

                def record = [
                        "lookupTableId"  : table.id,
                        "lookupTableName": table.uniqueName,
                        "name"           : addOnSKU,
                        "attribute1"     : "Description",
                        "attribute2"     : "AD",
                        "attribute3"     : "",
                        "attribute4"     : purchaseOption,
                        "attribute5"     : bwOption,
                        "attribute6"     : paymentTerm,
                        "attribute7"     : paymentType,
                        "attribute8"     : "Type 2",
                        "attribute9"     : "",
                        "attribute10"    : licenseClass
                ]
                api.trace("sku", null, addOnSKU)
                if ((paymentTerm == 1 || paymentTerm == 3 || paymentTerm == 5) &&
                        (licenseClass == "C" || licenseClass == "F") &&
                        (bwOption == "005G" || bwOption == "002G" || bwOption == "001G" || bwOption == "100M" || bwOption == "200M" || bwOption == "500M")
                )
                    api.addOrUpdate("MLTV", record)
            }
        }
    }
}