List baseSkus = []
List skus = []
baseSkus = api.local.baseSkus?.flatten()?.unique()
List filters = [
        Filter.equal(Constants.COLUMN_NAME, Constants.STANDARD_PRODUCT_PX_TABLE),
        Filter.in((Constants.PX_STANDARD_PRODUCTS_BASE_PRICE_SKU), baseSkus)
]
skus = libs.vmwareUtil.TAMDBUtils.getStandardProductRecords(filters)?.sku
if (skus != null && skus?.size() > 0) {
    api.local.skus << skus?.unique()
    api.local.skus = api.local.skus?.flatten()?.unique()
}
return
