def warningStyle = out.ConstantConfiguration
ResultMatrix resultMatrix = api.newMatrix(Constants.PRODUCT, Constants.OFFER, Constants.BASE, Constants.BASE_PRICE_SKU, Constants.TYPE, Constants.SKU, Constants.SHORT_DESCRIPTION, Constants.LONG_DESCRIPTION)
if (out.ProductFilter) {
    if (!api.local.productDetails) {
        resultMatrix = api.newMatrix(" ")
        warningCell = resultMatrix?.styledCell(Constants.NO_DETAILS_WARNING_MESSAGE, warningStyle.TEXT_COLOR, warningStyle.BG_COLOR, warningStyle.TEXT_WEIGHT, warningStyle.TEXT_ALLIGN)
        resultMatrix?.addRow(warningCell)
        return resultMatrix
    }
    api.local.productDetails?.each { data ->
        resultMatrix?.addRow(data)
    }
    return resultMatrix
} else {
    resultMatrix = api.newMatrix("")
    warningCell = resultMatrix?.styledCell(warningStyle.WARNING, warningStyle.TEXT_COLOR, warningStyle.BG_COLOR, warningStyle.TEXT_WEIGHT, warningStyle.TEXT_ALLIGN)
    resultMatrix?.addRow(warningCell)
    if (out.StartYear > out.EndYear) {
        warningCell = resultMatrix?.styledCell(warningStyle.YEAR_WARNING_MESSAGE, warningStyle.TEXT_COLOR, warningStyle.BG_COLOR, warningStyle.TEXT_WEIGHT, warningStyle.TEXT_ALLIGN)
        resultMatrix?.addRow(warningCell)
    } else {
        warningCell = resultMatrix?.styledCell(Constants.ALL_INPUTS_WARNING_MESSAGE, warningStyle.TEXT_COLOR, warningStyle.BG_COLOR, warningStyle.TEXT_WEIGHT, warningStyle.TEXT_ALLIGN)
        resultMatrix?.addRow(warningCell)
    }
    return resultMatrix
}