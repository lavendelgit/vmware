List upgradeSkus = []
List finalupgradeSkus = []
upgradeSkus << api.findLookupTableValues(Constants.BASE_TO_UPGRADE_CP_TABLE, Filter.in(Constants.PP_BASE_TO_UPGRADE_BASE_SKU, api.local.baseSkus))?.(Constants.PP_BASE_TO_UPGRADE_UPGRADE_SKU)
upgradeSkus << api.findLookupTableValues(Constants.UPGRADE_TO_DESTINATION_CP_TABLE, Filter.in(Constants.PP_UPGRADE_TO_DESTINATION_DESTINATION_SKU, api.local.baseSkus))?.(Constants.PP_UPGRADE_TO_DESTINATION_UPGRADE_SKU)
List skus = upgradeSkus?.flatten()?.unique()
if (skus != null && skus?.size() > 0) {
    finalupgradeSkus << skus
    api.local.baseSkus << finalupgradeSkus
    api.local.baseSkus = api.local.baseSkus?.flatten()?.unique()
    api.local.skus << finalupgradeSkus
    api.local.skus = api.local.skus?.flatten()?.unique()
}
return
