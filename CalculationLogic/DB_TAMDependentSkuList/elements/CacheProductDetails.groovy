List filters = []
api.local.productDetails = []
Map data = [:]
Map descriptions = out.CacheDescriptions
List skuLists = (api.local.skus)?.flatten()?.unique()?.sort()
skuLists.removeAll([null])
filters << Filter.equal(Constants.COLUMN_NAME, Constants.STANDARD_PRODUCT_PX_TABLE)
filters << Filter.in(Constants.PX_STANDARD_PRODUCTS_PART_NUMBER, skuLists)
List standardProductDetails = libs.vmwareUtil.TAMDBUtils.getStandardProductRecords(filters)?.getAt(0)
for (productDetail in standardProductDetails) {
    data = [
            (Constants.PRODUCT)          : productDetail?.(Constants.PX_STANDARD_PRODUCTS_PRODUCT),
            (Constants.OFFER)            : productDetail?.(Constants.PX_STANDARD_PRODUCTS_OFFER),
            (Constants.BASE)             : productDetail?.(Constants.PX_STANDARD_PRODUCTS_BASE),
            (Constants.BASE_PRICE_SKU)   : productDetail?.(Constants.PX_STANDARD_PRODUCTS_BASE_PRICE_SKU),
            (Constants.TYPE)             : productDetail?.(Constants.PX_STANDARD_PRODUCTS_TYPE),
            (Constants.SKU)              : productDetail?.(Constants.PX_STANDARD_PRODUCTS_PART_NUMBER),
            (Constants.SHORT_DESCRIPTION): descriptions[productDetail?.(Constants.PX_STANDARD_PRODUCTS_PART_NUMBER)]?.(Constants.PP_DESCRIPTION_SHORT_DESCRIPTION),
            (Constants.LONG_DESCRIPTION) : descriptions[productDetail?.(Constants.PX_STANDARD_PRODUCTS_PART_NUMBER)]?.(Constants.PP_DESCRIPTION_LONG_DESCRIPTION)
    ]
    api.local.productDetails << data
}
standardProductSKUs = api.local.productDetails?.collect {it.SKU} as List
skuLists.removeAll(standardProductSKUs)
for (sku in skuLists ) {
    data = [
            (Constants.PRODUCT)          : ' ',
            (Constants.OFFER)            : ' ',
            (Constants.BASE)             : ' ',
            (Constants.BASE_PRICE_SKU)   : ' ',
            (Constants.TYPE)             : ' ',
            (Constants.SKU)              : sku,
            (Constants.SHORT_DESCRIPTION): ' ',
            (Constants.LONG_DESCRIPTION) : ' '
    ]
    api.local.productDetails << data
}
api.local.productDetails = api.local.productDetails?.unique().sort{it.sku}
return
