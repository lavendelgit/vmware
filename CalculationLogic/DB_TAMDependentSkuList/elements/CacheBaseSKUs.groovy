api.local.baseSkus = []
api.local.skus = []
List productSkus = []
List filters = []
def productFilter
productGroup = api.input(Constants.PRODUCT_INPUT_NAME)
if (productGroup) {
    if (productGroup.productFilterCriteria) {
        productFilter = api.filterFromMap(productGroup.productFilterCriteria)
    } else {
        productFilter = Filter.equal(productGroup.productFieldName, productGroup.productFieldValue)
    }
    filters = [Filter.equal(Constants.COLUMN_NAME, Constants.STANDARD_PRODUCT_PX_TABLE),
               productFilter
    ]
}

productSkus = libs.vmwareUtil.TAMDBUtils.getStandardProductRecords(filters)?.(Constants.PX_STANDARD_PRODUCTS_BASE_PRICE_SKU)
api.local.baseSkus = productSkus?.flatten()?.unique()
api.local.skus << api.local.baseSkus
api.local.skus = api.local.skus?.flatten()?.unique()
return