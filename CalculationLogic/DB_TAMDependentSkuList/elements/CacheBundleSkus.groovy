import java.text.SimpleDateFormat

List skus = []
List bundleSkus = []
Date currentDate = api.targetDate()
List filters = [
        Filter.equal(Constants.COLUMN_NAME, Constants.BOM_PX_TABLE),
        Filter.in(Constants.PX_BOM_SUB_COMPONENT, api.local.baseSkus),
        Filter.lessOrEqual(Constants.PX_BOM_VALID_FROM, currentDate),
        Filter.greaterOrEqual(Constants.PX_BOM_VALID_TO, currentDate)
]
skus = libs.vmwareUtil.TAMDBUtils.getBOMRecords(filters)
if (skus != null && skus?.size() > 0) {
    bundleSkus << skus.unique()
    api.local.baseSkus << bundleSkus
    api.local.baseSkus = api.local.baseSkus?.flatten()?.unique()
    api.local.skus << bundleSkus
    api.local.skus = api.local.skus?.flatten()?.unique()
}
return