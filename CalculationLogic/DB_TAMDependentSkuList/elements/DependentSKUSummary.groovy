def configuration = out.ConstantConfiguration
ResultMatrix resultMatrix
if (out.ProductFilter) {
    resultMatrix = api.newMatrix(Constants.COLUMN_FIELDS, Constants.COLUMN_VALUES)
    productFilter = (out.ProductFilter ?: Constants.NULL_PLACE_HOLDER) as String
    styleProductFilter = resultMatrix?.styledCell(Constants.PRODUCT_INPUT_NAME, configuration.TEXT_COLOR, configuration.BG_COLOR, configuration.TEXT_WEIGHT, configuration.TEXT_ALLIGN)
    styleProductFilterValue = resultMatrix?.styledCell(productFilter, configuration.TEXT_COLOR, configuration.BG_COLOR, configuration.TEXT_WEIGHT, configuration.TEXT_ALLIGN)
    resultMatrix?.addRow(styleProductFilter, styleProductFilterValue)
    return resultMatrix
}
