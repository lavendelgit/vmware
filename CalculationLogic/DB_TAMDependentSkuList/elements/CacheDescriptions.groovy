List skus = api.local.skus?.flatten()
skus.removeAll([null])
Map descriptions = libs.vmwareUtil.TAMDBUtils.getDescriptions(skus)?.collectEntries { it }
return descriptions