List filter = [
        Filter.equal("name", Constants.BUNDLE_ADJUSTMENT),
        Filter.in("attribute1", api.local.stdProductsCache?.attribute1?.unique()),
        Filter.or(Filter.in("attribute2", api.local.stdProductsCache?.attribute2?.unique()), Filter.equal("attribute2", "*")),
        Filter.or(Filter.in("attribute3", api.local.stdProductsCache?.attribute4?.unique()), Filter.equal("attribute3", "*")),
        //Filter.isNotNull("attribute7")
]
api.local.bundleAdjustmentsCache = []
bundleAdjustmentStream = api.stream("PX50", "sku,attribute43", *filter)
bundleAdjustmentStream?.collect { row ->
    if (row.attribute43 <= row.attribute44) {
        api.local.bundleAdjustmentsCache << row
    }
}
bundleAdjustmentStream?.close()
return
