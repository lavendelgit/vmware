List filter = [
        Filter.equal("name", Constants.BOM_PX_TABLE),
        Filter.in("sku", api.local.stdProductsCache?.attribute14?.unique() as List)
]

recordStream = api.stream("PX10", null, *filter)
api.local.bomEntries = recordStream?.collect {
    [
            sku            : it.sku,
            subComponentSku: it.attribute1,
            quantity       : it.attribute3 as BigDecimal,
            discount       : it.attribute4 as BigDecimal,
            validFrom      : it.attribute5,
            validTo        : it.attribute6
    ]
}
recordStream?.close()
return

