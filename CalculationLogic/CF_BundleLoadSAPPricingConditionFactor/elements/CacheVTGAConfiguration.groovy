List filter = [
        Filter.equal("name", Constants.VTGA_TABLE),
        Filter.in("sku", out.SAPSKUCache),
]
Map vtgaConfiguration = [:]
configurationRecords = api.find("PX20", *filter)
for (record in configurationRecords) {
    if (!vtgaConfiguration[record.sku]) {
        vtgaConfiguration[record.sku] = []
        vtgaConfiguration[record.sku] << [
                "AttributeName1" : record.attribute5,
                "AttributeValue1": record.attribute6,
                "AttributeName2" : record.attribute7,
                "AttributeValue2": record.attribute8,
                "AttributeName3" : record.attribute9,
                "AttributeValue3": record.attribute10,
                "AttributeName4" : record.attribute11,
                "AttributeValue4": record.attribute12,
                "AttributeName5" : record.attribute13,
                "AttributeValue5": record.attribute14,
        ]
    } else {
        vtgaConfiguration[record.sku] << [
                "AttributeName1" : record.attribute5,
                "AttributeValue1": record.attribute6,
                "AttributeName2" : record.attribute7,
                "AttributeValue2": record.attribute8,
                "AttributeName3" : record.attribute9,
                "AttributeValue3": record.attribute10,
                "AttributeName4" : record.attribute11,
                "AttributeValue4": record.attribute12,
                "AttributeName5" : record.attribute13,
                "AttributeValue5": record.attribute14,
        ]
    }
}
return vtgaConfiguration