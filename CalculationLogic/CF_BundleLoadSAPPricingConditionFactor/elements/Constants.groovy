import groovy.transform.Field

@Field final String SAP_PRICING_CONDITION_TABLE = "SAPPricingConditionFactor"
@Field final String PRICELISTREGION_TABLE = "PriceListRegion"
@Field final String BUNDLE_PRODUCT_TYPE = "Bundle"
@Field final String SAP_FILE_SKU_CONFIG_PP = "SAPFileSKUConfig"
@Field final String PRODUCT_HIERARCHY_MAPPING_PP = "ProductHierarchyMapping"
@Field final String STANDARD_PRODUCT_PX = "StandardProducts"
@Field final String BUNDLE_ADJUSTMENT = "BundleAdjustment"
@Field final List EXCLUDE_LIST = ["version", "typedId", "name", "sku", "createDate", "lastUpdateDate", "createdBy", "lastUpdateBy", "attribute1", "attribute2", "attribute3", "attribute33", "attribute43", "attribute44", "attribute25", "attribute26"]
@Field final String ADJUSTMENT = "Adjustment"
@Field final String FACTOR = "Factor"
@Field final String UPLIFT = "Uplift"
@Field final String FXRATE = "Fx Rate"
@Field final String CONTEXT_CONSTANT = "PRODUCT_ID/VARIANT"
@Field final Integer ADJUSTMENT_INDEX = 0
@Field final Integer WEIGHTAG_INDEX = 1
@Field final Integer COMBINATION_INDEX = 2
@Field final String TERM = "Term"
@Field final String PAYMENT_TYPE_TERM = "Payment Type|Term"
@Field final String TERM_UOM = "TermUoM"
@Field final String PAYMENT_TYPE = "Payment Type"
@Field final String RETENTION_PERIOD = "Retention Period"
@Field final String RETENTION_PERIOD_UOM = "RetentionPeriodUOM"
@Field final String CURRENCY = "Currency"
@Field final String ADJUSTMENT_REGION = "Price List Region"
@Field final String ATTRIBUTE_PRICE_LIST_REGION = "PriceListRegion"
@Field final String PRICELISTREGION_PX = "Price List Region"
@Field final String CHANNEL_PRICELIST_REGION = "Channel Launch Region"
@Field final String UPDATE_FACTOR = "Factor"
@Field final String UPDATE_DATE = "Date"
@Field final String COMMIT = "Commit"
@Field final String BOTH = "Both"
@Field final String OVERAGE = "Overage"
@Field final String BUNDLE_DISCOUNT = "BUNDLE_DISCOUNT"
@Field final List FACTOR_ATTRIBUTES_NAME_LIST = [ADJUSTMENT, FACTOR, UPLIFT, FXRATE]
@Field final List VALID_DYNAMIC_KEYS_COLUMNS = ["attribute21", "attribute7"]
@Field final Map FACTOR_ATTRIBUTES_MAPPING = [
        "Currency"          : [
                FactorValueAttribute: "attribute24",
                FactorAttribute     : "attribute23",
                FactorOverride      : "attribute36"
        ],

        "Price List Region" : [
                FactorValueAttribute: "attribute34",
                FactorAttribute     : "attribute4",
                FactorOverride      : "attribute36"
        ],
        "Segment Type"      : [
                FactorValueAttribute: "attribute18",
                FactorAttribute     : "attribute10",
                FactorOverride      : "attribute36"
        ],
        "Term"              : [
                FactorValueAttribute: "attribute15",
                FactorAttribute     : "attribute7",
                FactorOverride      : "attribute36"
        ],
        "Retention Period"  : [
                FactorValueAttribute: "attribute45",
                FactorAttribute     : "attribute43",
                FactorOverride      : "attribute36"
        ],
        "Payment Type"      : [
                FactorValueAttribute: "attribute22",
                FactorAttribute     : "attribute21",
                FactorOverride      : "attribute36"
        ],
        "Data Center"       : [
                FactorValueAttribute: "attribute20",
                FactorAttribute     : "attribute12",
                FactorOverride      : "attribute36"
        ],
        "Metric"            : [
                FactorValueAttribute: "attribute14",
                FactorAttribute     : "attribute6",
                FactorOverride      : "attribute36"
        ],
        "Support Tier"      : [
                FactorValueAttribute: "attribute16",
                FactorAttribute     : "attribute8",
                FactorOverride      : "attribute36"
        ],
        "Support Type"      : [
                FactorValueAttribute: "attribute17",
                FactorAttribute     : "attribute9",
                FactorOverride      : "attribute36"
        ],
        "Purchasing Program": [
                FactorValueAttribute: "attribute19",
                FactorAttribute     : "attribute11",
                FactorOverride      : "attribute36"
        ],
        "Volume Tier Size"  : [
                FactorValueAttribute: "attribute28",
                FactorAttribute     : "attribute27",
                FactorOverride      : "attribute36"
        ],
        "Promotion"         : [
                FactorValueAttribute: "attribute32",
                FactorAttribute     : "attribute31",
                FactorOverride      : "attribute36"
        ],
        "OS"                : [
                FactorValueAttribute: "attribute38",
                FactorAttribute     : "attribute37",
                FactorOverride      : "attribute36"
        ],

        "Hosting"           : [
                FactorValueAttribute: "attribute40",
                FactorAttribute     : "attribute39",
                FactorOverride      : "attribute36"
        ],
        "Program Option"    : [
                FactorValueAttribute: "attribute42",
                FactorAttribute     : "attribute41",
                FactorOverride      : "attribute36"
        ],
        "Upgrade"           : [
                FactorValueAttribute: "attribute30",
                FactorAttribute     : "attribute29",
                FactorOverride      : "attribute36"
        ],
        "Sub Region"        : [
                FactorValueAttribute: "attribute13",
                FactorAttribute     : "attribute5",
                FactorOverride      : "attribute36"
        ]
]
@Field final Map STANDARD_ATTRIBUTE_MAP = [
        "Segment Type"    : "Segment",
        "Volume Tier Size": "Volume Tier"
]
@Field final String BOM_PX_TABLE = "BOM"
@Field final String VALID_COMBINATION_ATTR = "ValidCombinationAttributes"
@Field final String VALID_COMBINATION_CONF = "ValidCombinationConfiguration"
@Field final String VTGA_TABLE = "VolumeTierGroupAssignment"
@Field final String PROGRAM_OPTION = "Program Option"
@Field final String VOLUME_TIER_SIZE = "Volume Tier Size"
@Field final String METRIC = "Metric"