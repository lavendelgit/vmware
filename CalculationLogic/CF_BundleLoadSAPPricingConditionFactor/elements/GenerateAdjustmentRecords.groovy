String product
String offer
Map pxRecord = [:]
List loadedSAPSKUdata = []
api.local.adjustmentRecords = []
api.local.updateAdjustmentsRecords = []
List attributes = Constants.FACTOR_ATTRIBUTES_MAPPING?.keySet() as List
for (productSku in api.local.stdProductsCache) {
    if (!loadedSAPSKUdata?.contains(productSku?.attribute29)) {
        uniqueAttributes = api.local.stdProductsCache?.findAll { it.attribute29 == productSku?.attribute29 }
        validConfigurationCache = out.CacheValidConfiguration?.values()?.findAll { it.Product == productSku?.attribute1 }
        validAttributes = validConfigurationCache?.findAll { it.SAPSKU == productSku.attribute29 } ?: out.CacheValidConfiguration?.values()?.findAll { it.SAPSKU == "*" }
        baseSkuDetails = [
                Product: productSku.attribute1,
                Offer  : productSku.attribute2,
                Base   : productSku.attribute4
        ]
        bundleAdjustmentSkus = api.local.bundleAdjustmentsCache?.findAll { it.attribute1 == productSku?.attribute1 }
        for (attribute in validAttributes?.Attribute?.unique()) {
            attributeValue = Lib.getAttributeValues(attribute, uniqueAttributes)
            pxRecord = [:]
            pxRecord.SAPSKU = productSku?.attribute29
            pxRecord.Product = productSku?.attribute1
            pxRecord.Offer = productSku?.attribute2
            pxRecord.Base = productSku?.attribute4
            attributeKey = attribute.replaceAll("\\s", "")
            attributeRecords = bundleAdjustmentSkus?.findAll { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorValueAttribute] != null }
            factorAttibute = Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute
            switch (attribute) {
                case Constants.PAYMENT_TYPE:
                    uniqueRecords = attributeRecords.unique(false) { a, b ->
                        a[factorAttibute] <=> b[factorAttibute] ?:
                                a["attribute7"] <=> b["attribute7"]
                    }
                    break

                case Constants.TERM:
                    uniqueRecords = attributeRecords.unique(false) { a, b ->
                        a[factorAttibute] <=> b[factorAttibute] ?: a.attribute33 <=> b.attribute33
                    }
                    break

                case Constants.ADJUSTMENT_REGION:
                    uniqueRecords = attributeRecords.unique(false) { a, b ->
                        a[factorAttibute] <=> b[factorAttibute] ?:
                                a["attribute23"] <=> b["attribute23"]
                    }
                    break

                case Constants.PROGRAM_OPTION:
                    uniqueRecords = attributeRecords.unique(false) { a, b ->
                        a[factorAttibute] <=> b[factorAttibute] ?:
                                a["attribute6"] <=> b["attribute6"]
                    }
                    break

                case Constants.RETENTION_PERIOD:
                    uniqueRecords = attributeRecords.unique(false) { a, b ->
                        a[factorAttibute] <=> b[factorAttibute] ?:
                                a.attribute44 <=> b.attribute44
                    }
                    break

                case Constants.VOLUME_TIER_SIZE:
                    vtgaConfig = out.CacheVTGAConfiguration[productSku?.attribute29]
                    uniqueRecords = sortVolumeTierAdjustment(attributeRecords, vtgaConfig)
                    break

                default:
                    uniqueRecords = attributeRecords.unique(false) { a, b ->
                        a[factorAttibute] <=> b[factorAttibute]
                    }
            }
            latestRecords = uniqueRecords?.sort { a, b -> b.attribute43 <=> a.attribute43 }?.unique()?.findAll { it != null }
            populateAdjustmentFactor(latestRecords, productSku, attribute, attributeValue, baseSkuDetails, validAttributes, pxRecord, uniqueAttributes)
        }
        subComponents = api.local.bomEntries?.findAll { it.sku == productSku?.attribute14 }
        for (subComponent in subComponents) {
            Map bundlePxRecord = [:]
            discount = subComponent.discount ?: 0 as BigDecimal
            bundlePxRecord.AttributeFactor = discount
            bundlePxRecord.SAPSKU = productSku?.attribute29
            bundlePxRecord.Product = productSku?.attribute1
            bundlePxRecord.Offer = productSku?.attribute2
            bundlePxRecord.Base = productSku?.attribute4
            bundlePxRecord.SKUType = Constants.BUNDLE_PRODUCT_TYPE
            bundlePxRecord.AttributeName = Constants.BUNDLE_DISCOUNT
            bundlePxRecord.AttributeValue = subComponent.subComponentSku
            bundlePxRecord.ValidFrom = subComponent.validFrom
            bundlePxRecord.ValidTo = subComponent.validTo
            if (bundlePxRecord.Product) {
                bundlePxRecord.SAPProductLevel = out.CacheProductHierarchyMapping?.getAt(bundlePxRecord.Product)?.SAPName
                bundlePxRecord.ProdHierarchy = out.CacheProductHierarchyMapping?.getAt(bundlePxRecord.Product)?.SAPAttName
            }
            if (bundlePxRecord.Offer && bundlePxRecord.Offer != "*") {
                bundlePxRecord.SAPProductLevel = out.CacheProductHierarchyMapping?.getAt(bundlePxRecord.Offer)?.SAPName
                bundlePxRecord.ProdHierarchy = out.CacheProductHierarchyMapping?.getAt(bundlePxRecord.Offer)?.SAPAttName
            }
            if (bundlePxRecord.Base && bundlePxRecord.Base != "*") {
                bundlePxRecord.SAPProductLevel = out.CacheProductHierarchyMapping?.getAt(bundlePxRecord.Base)?.SAPName
                bundlePxRecord.ProdHierarchy = out.CacheProductHierarchyMapping?.getAt(bundlePxRecord.Base)?.SAPAttName
            }
            bundlePxRecord.FactorMeasure = discount ? discount : null
            bundlePxRecord.sku = populateSKU(productSku?.attribute29, "", bundlePxRecord, "", "", "")
            api.local.adjustmentRecords << Lib.insertRecords(bundlePxRecord)
        }
        loadedSAPSKUdata << productSku?.attribute29
    }
}
return


void populateAdjustmentFactor(List latestRecords, Map productSku, String attribute, List attributeValue, Map baseSkuDetails, List validAttributes, Map pxRecord, List uniqueAttributes) {
    for (uniqueRecord in latestRecords) {
        String termPayment
        String exceptionValue
        skuAttribute = [
                Product: productSku.attribute1,
                Offer  : productSku.attribute2,
                Base   : productSku.attribute4,
        ]
        skuAttribute[attributeKey] = uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute]
        if (Lib.validateAdjustmentValues(attribute, attributeValue, skuAttribute[attributeKey], uniqueRecord, uniqueAttributes)) {

            switch (attribute) {
                case Constants.TERM:
                    skuAttribute[Constants.TERM_UOM] = uniqueRecord.attribute33
                    exceptionValue = out.CacheTermAttributes[uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute]]
                    break

                case Constants.PAYMENT_TYPE:
                    skuAttribute[Constants.TERM_UOM] = uniqueRecord.attribute33
                    skuAttribute[Constants.TERM] = uniqueRecord.attribute7
                    termPayment = skuAttribute[Constants.TERM_UOM] ? Constants.PAYMENT_TYPE_TERM : null
                    exceptionValue = skuAttribute[Constants.TERM_UOM] ? uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] + "|" + out.CacheTermAttributes[uniqueRecord.attribute7] : null
                    break

                case Constants.CURRENCY:
                    skuAttribute[Constants.ATTRIBUTE_PRICE_LIST_REGION] = uniqueRecord.attribute4
                    break

                case Constants.ADJUSTMENT_REGION:
                    exceptionValue = ((uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute])?.split(' ') as List)?.join('|')
                    skuAttribute[Constants.CURRENCY] = uniqueRecord.attribute23 ?: ""
                    break

                case Constants.RETENTION_PERIOD:
                    skuAttribute[Constants.RETENTION_PERIOD_UOM] = uniqueRecord.attribute44
                    exceptionValue = out.CacheSAPAttributes[skuAttribute[Constants.RETENTION_PERIOD_UOM]] ? uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] + " " + out.CacheSAPAttributes[skuAttribute[Constants.RETENTION_PERIOD_UOM]] : uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] + " " + uniqueRecord.attribute44
                    break

                case Constants.PROGRAM_OPTION:
                    skuAttribute[Constants.METRIC] = uniqueRecord.attribute6
                    exceptionValue = uniqueRecord.attribute41 && uniqueRecord.attribute6 ? uniqueRecord.attribute41 + "|" + uniqueRecord.attribute6 : uniqueRecord.attribute41
                    break

                case Constants.VOLUME_TIER_SIZE:
                    api.local.vtgaAttributeMapping?.each {
                        skuAttribute[(out.BundleAdjustmentMetaData[it])?.replaceAll("\\s", "")] = uniqueRecord[it]
                    }
                    volumeTierValue = skuAttribute["VolumeTierSize"]
                    skuAttribute?.remove("VolumeTierSize")
                    skuAttribute["VolumeTierSize"] = volumeTierValue
                    if (skuAttribute.Term) {
                        exceptionMap = skuAttribute?.drop(3)
                        exceptionMap.Term = out.CacheTermAttributes[exceptionMap.Term]
                    } else {
                        exceptionMap = skuAttribute?.drop(3)
                    }
                    exceptionValue = exceptionMap?.values()?.grep()?.join('|')
            }

            if (uniqueRecord?.attribute36) {
                adjustment = uniqueRecord?.attribute36
            } else {
                adjustment = Lib.getAdjustmentsForAttribute(api.local[attribute], baseSkuDetails, skuAttribute)
            }
            pxRecord.AttributeFactor = adjustment
            pxRecord.SKUType = Constants.BUNDLE_PRODUCT_TYPE
            pxRecord.AttributeName = validAttributes?.find { it.Attribute == attribute }?.SAPAttribute
            pxRecord.AttributeValue = exceptionValue ?: uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute]
            pxRecord.FactorMeasure = adjustment ? adjustment : null
            pxRecord.ValidFrom = bundleAdjustmentSkus?.findAll { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.last()?.attribute43
            pxRecord.ValidTo = bundleAdjustmentSkus?.findAll { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.last()?.attribute44
            product = latestRecords?.find { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.attribute1
            offer = latestRecords?.find { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.attribute2
            base = latestRecords?.find { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.attribute3

            if (product) {
                pxRecord.SAPProductLevel = out.CacheProductHierarchyMapping?.getAt(product)?.SAPName
                pxRecord.ProdHierarchy = out.CacheProductHierarchyMapping?.getAt(product)?.SAPAttName
            }
            if (offer && offer != "*") {
                pxRecord.SAPProductLevel = out.CacheProductHierarchyMapping?.getAt(offer)?.SAPName
                pxRecord.ProdHierarchy = out.CacheProductHierarchyMapping?.getAt(offer)?.SAPAttName
            }
            if (base && base != "*") {
                pxRecord.SAPProductLevel = out.CacheProductHierarchyMapping?.getAt(base)?.SAPName
                pxRecord.ProdHierarchy = out.CacheProductHierarchyMapping?.getAt(base)?.SAPAttName
            }
            pxRecord.sku = populateSKU(productSku?.attribute29, attribute, pxRecord, uniqueRecord.attribute33, uniqueRecord.attribute23, uniqueRecord.attribute44)
            if (adjustment && !api.local.sapPricingFactorCache?.sku?.contains(pxRecord.sku) && pxRecord.ValidFrom <= pxRecord.ValidTo) {
                api.local.adjustmentRecords << Lib.insertRecords(pxRecord)
            } else if (adjustment && api.local.sapPricingFactorCache?.sku?.contains(pxRecord.sku) && !api.local.sapPricingFactorCache?.findAll { it.sku == pxRecord.sku }?.attribute7?.contains(adjustment)) {
                if (api.local.sapPricingFactorCache?.findAll { it.sku == pxRecord.sku }?.getAt(0)?.attribute9 == pxRecord.ValidFrom) {
                    api.local.updateAdjustmentsRecords << Lib.populateUpdatePayload(pxRecord, Constants.UPDATE_FACTOR)
                } else if (api.local.sapPricingFactorCache?.findAll { it.sku == pxRecord.sku }?.getAt(0)?.attribute9 != pxRecord.ValidFrom && pxRecord.ValidFrom <= pxRecord.ValidTo) {
                    api.local.adjustmentRecords << Lib.insertRecords(pxRecord)
                    api.local.updateAdjustmentsRecords << Lib.populateUpdatePayload(pxRecord, Constants.UPDATE_DATE)
                }
            }
        }
    }
}

String populateSKU(String sapSKU, String attribute, Map pxRecord, String termUoM, String currency, String retentionPeriodUoM) {
    String sku = sapSKU + "-"
    if (attribute == Constants.TERM || attribute == Constants.PAYMENT_TYPE) {
        sku += termUoM ? libs.vmwareUtil.util.frameKey([pxRecord.SAPProductLevel, pxRecord.AttributeValue, termUoM]) : libs.vmwareUtil.util.frameKey([pxRecord.SAPProductLevel, pxRecord.AttributeValue])
        return sku
    } else if (attribute == Constants.ADJUSTMENT_REGION) {
        sku += currency ? libs.vmwareUtil.util.frameKey([pxRecord.SAPProductLevel, pxRecord.AttributeValue, currency]) : libs.vmwareUtil.util.frameKey([pxRecord.SAPProductLevel, pxRecord.AttributeValue])
        return sku
    } else if (attribute == Constants.RETENTION_PERIOD) {
        sku += retentionPeriodUoM ? libs.vmwareUtil.util.frameKey([pxRecord.SAPProductLevel, pxRecord.AttributeValue, retentionPeriodUoM]) : libs.vmwareUtil.util.frameKey([pxRecord.SAPProductLevel, pxRecord.AttributeValue])
        return sku
    } else if (attribute == Constants.VOLUME_TIER_SIZE) {
        sku += libs.vmwareUtil.util.frameKey([pxRecord.Product, pxRecord.Offer, pxRecord.Base, pxRecord.AttributeValue])
        return sku
    } else {
        sku += libs.vmwareUtil.util.frameKey([pxRecord.SAPProductLevel, pxRecord.AttributeValue])
        return sku
    }
}

List sortVolumeTierAdjustment(List adjustments, List vtgaConfig) {
    localAdjustment = adjustments
    String factorAttibute = "attribute27"
    List attributeData = []
    Map adjustmentMetaData = out.BundleAdjustmentMetaData?.collectEntries { [(it.value): it.key] }
    api.local.vtgaAttributeMapping = []
    uniqueRecords = []
    localUniqueRecords = []
    vtgaConfig?.each { record ->
        sortAttributes = []
        record?.each { it ->
            if (it.key?.contains("AttributeName") && it.value) {
                sortAttributes << adjustmentMetaData[out.ValidCombinationAttributeCache[it.value]]
                api.local.vtgaAttributeMapping << adjustmentMetaData[out.ValidCombinationAttributeCache[it.value]]
            }
        }
        sortAttributes?.each { it ->
            adjustments = localAdjustment?.findAll { attribute ->
                attribute[it] != null
            }
            attributeData = adjustments[it]?.grep()
            if (attributeData?.getAt(0) != null) {
                uniqueRecords = adjustments.unique(false) { a, b ->
                    a[factorAttibute] <=> b[factorAttibute] ?:
                            a[it] <=> b[it]
                }
            }
        }
        for (uRecord in uniqueRecords) {
            localUniqueRecords << uRecord
        }
    }
    return localUniqueRecords
}