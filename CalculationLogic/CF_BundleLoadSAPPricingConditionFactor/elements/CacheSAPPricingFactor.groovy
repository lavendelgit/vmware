List filter = [
        Filter.equal("name", Constants.SAP_PRICING_CONDITION_TABLE),
        Filter.in("attribute2", out.ProductsCache),
        Filter.in("attribute16", out.SAPSKUCache)
]
recordStream = api.stream("PX20", "-attribute9", *filter)
api.local.sapPricingFactorCache = recordStream?.collect {
    it
}
recordStream?.close()
return
