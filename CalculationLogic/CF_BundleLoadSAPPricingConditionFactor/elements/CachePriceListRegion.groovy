List filter = [
        Filter.equal("name", Constants.PRICELISTREGION_TABLE),
        Filter.in("attribute4", api.local.stdProductsCache?.attribute1?.unique())
]
recordStream = api.stream("PX8", "sku", *filter)
api.local.priceListRegionCache = recordStream?.collect { it }
recordStream?.close()
return