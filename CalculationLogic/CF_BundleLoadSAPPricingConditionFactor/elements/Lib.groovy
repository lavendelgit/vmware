List insertRecords(Map record) {
    return [
            Constants.SAP_PRICING_CONDITION_TABLE,
            record.sku,
            record.SAPSKU,
            record.ProdHierarchy,
            record.Product,
            record.Offer,
            record.Base,
            record.AttributeName,
            record.AttributeValue,
            record.AttributeFactor,
            record.FactorMeasure,
            record.ValidFrom,
            record.ValidTo,
            record.SAPProductLevel,
            record.SKUType
    ]
}

Map populateUpdatePayload(Map rowData, String update) {
    rowValue = api.local.sapPricingFactorCache?.find { it.sku == rowData.sku }
    if (update == Constants.UPDATE_DATE) {
        return [
                "operationType": "update",
                "data"         : [
                        "typedId"    : rowValue?.typedId,
                        "attribute10": getPreviousDate(rowData.ValidFrom)
                ],
                "oldValues"    : [
                        "version": rowValue?.version,
                        "typedId": rowValue?.typedId,
                ]
        ]
    } else if (update == Constants.UPDATE_FACTOR) {
        String sentTo
        if (rowValue?.attribute14 == Constants.COMMIT || rowValue?.attribute14 == Constants.OVERAGE || rowValue?.attribute14 == Constants.BOTH) {
            sentTo = ""
        }
        return [
                "operationType": "update",
                "data"         : [
                        "typedId"    : rowValue?.typedId,
                        "attribute7" : rowData?.AttributeFactor,
                        "attribute8" : rowData?.FactorMeasure,
                        "attribute14": sentTo
                ],
                "oldValues"    : [
                        "version": rowValue?.version,
                        "typedId": rowValue?.typedId,
                ]
        ]
    }

}

Map getSortedCombinations(String product, String offer, String base, List adjs) {
    List combinations = []
    combinations << [product, "*"]
    combinations << [offer, "*"]
    //combinations << [base, "*"]
    for (adj in adjs) {
        combinations << [adj, "*"]
    }
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ libs.vmwareUtil.CacheManager.getWeightage(it, "*") })?.sort { -it.key }
}

Map standardKeyConversion(Map keyAdjusments) {
    Map convertedAdjusments = [:]
    keyAdjusments?.each { key, value ->
        convertedAdjusments[key.toLowerCase()] = value
    }
    return convertedAdjusments
}

BigDecimal getAdjustments(Map adjustmentCache, String product, String offer, String base, List adjs) {
    convertedAdjusment = standardKeyConversion(adjustmentCache)
    Map sortedCombinations = getSortedCombinations(product, offer, base, adjs)
    BigDecimal adjustment = 0.0
    String key
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        adjustment = convertedAdjusment?.getAt(key.toLowerCase())?.Adjustment
        if (adjustment)
            return adjustment
    }
}

Map getAdjustmentsFromCache(Map adjustmentsCache, Map attributeValues) {
    Map keyLists = [:]
    List baseKeyLists = []
    List skuKeyLists = []
    Map keys = [:]
    Map baseKeys = [:]
    adjustmentsCache?.each {
        adj ->
            keys = [:]
            baseKeys = [:]
            adj.value.Keys?.each {
                key ->
                    key = key?.replaceAll("\\s", "")
                    if (attributeValues[key])
                        keys[key] = attributeValues.get(key)
            }
            skuKeyLists << keys
    }
    skuKeyLists = skuKeyLists?.minus(["Product", "Offer", "Base"])
    keyLists.StdKeys = skuKeyLists
    return keyLists

}

BigDecimal getAdjustmentsForAttribute(Map exceptionAdjustmentCache, Map stdSKUDetail, Map attributeValues) {
    Map keyLists = getAdjustmentsFromCache(exceptionAdjustmentCache, attributeValues)
    BigDecimal skuAdjustment
    keyLists?.StdKeys?.unique()
    for (keyList in keyLists?.StdKeys) {
        skuAdjustment = getAdjustments(exceptionAdjustmentCache, stdSKUDetail.Product, stdSKUDetail.Offer, stdSKUDetail.Base, keyList.values().drop(2))
        if (skuAdjustment) {
            break
        }
    }
    return skuAdjustment
}

String getPreviousDate(String validDate) {
    String dateFormat = "yyyy-MM-dd"
    Date date = api.parseDate(dateFormat, validDate)
    Calendar calendar = api.calendar(date)
    calendar.add(Calendar.DATE, -1)
    return calendar.getTime().format("yyyy-MM-dd")
}

Map cacheAdjustments(String factorAttribute, String typeAttribute = null, boolean isRoundingRule = false) {
    Map adjustmentCache = [:]
    List adjustments = api.local.bundleAdjustmentsCache?.findAll { it[factorAttribute] != null }
    List keys
    Integer termIndex = 0
    excludeList = ["version", "typedId", "id", "name", "sku", "createDate", "createdBy", "lastUpdateDate", "lastUpdateBy", "attribute3", factorAttribute, "attribute35", "attribute43", "attribute44"]
    if (isRoundingRule) {
        excludeList << typeAttribute
    }
    adjustments?.each {
        keys = it.keySet().collect()
        keysList = keys?.minus(excludeList)
        termIndex = keysList?.indexOf("attribute7")
        if (termIndex != -1) {
            keysList = keysList?.minus(["attribute33"])
            keysList = keysList.plus(termIndex + 1, "attribute33")
        }
        if (isRoundingRule) {
            keysList = keysList?.minus(["attribute4", "attribute23"])
            keysList = keysList.plus(["attribute4", "attribute23"])
        }
        keyNames = []
        keysList?.each {
            key ->
                keyNames << out.BundleAdjustmentMetaData[key]
        }
        keyValueList = []
        Map record = [:]
        keysList?.each {
            key ->
                if (it[key]) {
                    keyValueList << it[key]
                    record[out.BundleAdjustmentMetaData[key]] = it[key]
                }
        }
        key = libs.vmwareUtil.util.frameKey(keyValueList)
        if (isRoundingRule) {
            record.Precision = it[factorAttribute]
            record.Type = it[typeAttribute]
            record.Keys = keyNames
            adjustmentCache[key] = record
        } else {
            record.Adjustment = it[factorAttribute]
            record.Keys = keyNames
            adjustmentCache[key] = record
        }
    }
    adjustmentCache = adjustmentCache?.sort { -it.value?.Keys?.size() }
    return adjustmentCache
}

Map cacheAndPopulateTermUoMAdjustments(String factorAttribute) {
    Map factorAdjustmentCache = cacheAdjustments(factorAttribute)
    factorAdjustmentCache = populateTermUoMAdjustments(factorAdjustmentCache)
    return factorAdjustmentCache
}

String frameTermKeys(Map termAdj) {
    String keyString
    List keys = termAdj?.Keys
    keys?.each {
        key ->
            keyString = keyString ? keyString + "_" + termAdj[key] : termAdj[key]
    }
    return keyString
}

Map populateTermUoMAdjustments(Map factorAdjustmentCache) {
    Map populatedTermCache = [:]
    Map tempAdjustmentCache = [:]
    if (factorAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
        factorAdjustmentCache.each { key, value -> tempAdjustmentCache[key] = value.clone() }
        populatedTermCache = populateTermAltUoMAdjustments(tempAdjustmentCache)
        if (populatedTermCache?.size() > 0 && factorAdjustmentCache?.size() > 0) {
            factorAdjustmentCache = factorAdjustmentCache + populatedTermCache
        }
    }
    return factorAdjustmentCache
}

Map populateTermAltUoMAdjustments(Map adjustmentCache) {
    String term
    String termUoM
    Map populatedTermCache = [:]
    Map destTerm = [:]
    adjustmentCache?.findAll { it.value.Term != null && it.value["Term UoM"] != null }?.each {
        term = it.value.Term
        termUoM = it.value["Term UoM"]
        destTerm = out.TermUoMConversionCache[term + "_" + termUoM]
        if (destTerm) {
            it.value.Term = destTerm.DestTerm
            it.value["Term UoM"] = destTerm.DestUoM
            key = frameTermKeys(it.value)
            populatedTermCache[key] = it.value
        }
    }
    return populatedTermCache
}

List getAttributeValues(String attribute, List uniqueRecord) {
    if (attribute == Constants.CURRENCY || attribute == Constants.ADJUSTMENT_REGION) {
        attribute = attribute == Constants.ADJUSTMENT_REGION ? Constants.CHANNEL_PRICELIST_REGION : attribute
        stdProduct = uniqueRecord.attribute1?.unique()
        List filter = [Filter.in("attribute4", stdProduct)]
        return api.local.priceListRegionCache?.findAll { filter }?.getAt(out.PriceListRegionMetaData[attribute])?.unique()
    } else {
        attribute = Constants.STANDARD_ATTRIBUTE_MAP?.keySet()?.contains(attribute) ? Constants.STANDARD_ATTRIBUTE_MAP[attribute] : attribute
        return uniqueRecord[out.StdProductMetaData[attribute]]?.unique()
    }
}

boolean validateAdjustmentValues(String attribute, List attributeValue, String adjustmentValue, Map uniqueRecord, List uniqueAttributes) {
    if (uniqueRecord.attribute43 <= uniqueRecord.attribute44) {
        if (attributeValue*.toLowerCase()?.contains(adjustmentValue.toLowerCase())) {
            if (attribute == Constants.PAYMENT_TYPE && uniqueRecord.attribute7
                    && !(uniqueAttributes?.findAll { (it.attribute7).toLowerCase() == adjustmentValue.toLowerCase() }?.attribute6)?.contains(uniqueRecord.attribute7)) {
                return false
            }
            if (attribute == Constants.TERM && uniqueRecord.attribute33 &&
                    !(uniqueAttributes?.findAll { it.attribute6 == adjustmentValue }?.attribute21)?.contains(uniqueRecord.attribute33)) {
                return false
            }
            if (attribute == Constants.RETENTION_PERIOD && uniqueRecord.attribute44 &&
                    !(uniqueAttributes?.findAll { it.attribute31 == adjustmentValue }?.attribute32)?.contains(uniqueRecord.attribute44)) {
                return false
            }
            return true
        } else {
            return false
        }
    } else {
        return false
    }
}