def srp = api.getElement("SuggestedRetailPrice")
def erp = api.getElement("ExpectedPrice")

if (srp) {
    srp = Math.round(srp)
}
if (erp)
    erp = Math.round(erp)

api.trace("SuggestedRetailPrice", null, srp)
api.trace("ExpectedPrice", null, erp)

if (srp && erp && srp != erp && ((erp - srp > 1) || (srp - erp > 1)))
    return "Mismatch"
else if (srp && erp && ((srp == erp) || ((erp - srp < 1) || (srp - erp < 1))))
    return "Match"
