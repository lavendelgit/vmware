import java.math.RoundingMode

if (api.isSyntaxCheck())
    return

def srp
def basePrice = api.getElement("BasePrice")
def paymentTerm = api.getElement("PaymentTerm")
def billingTermFactor = api.getElement("BillingTermFactor")

srp = basePrice

if (basePrice && paymentTerm)
    srp = srp * (paymentTerm / 12)

if (basePrice && billingTermFactor)
    srp = srp * billingTermFactor?.toBigDecimal()

//srp = srp?.toFloat()?.round(2)?.toBigDecimal()

if (srp) return srp.setScale(0, RoundingMode.HALF_UP)
//return Math.round(srp / 10) * 10