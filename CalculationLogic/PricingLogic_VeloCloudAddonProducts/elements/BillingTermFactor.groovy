def paymentTerm = api.getElement("PaymentTerm")
def paymentFrequency = api.getElement("PaymentFrequency")

def ppTable = api.findLookupTable("AddonBillingTermsFactor")
while (recList = api.find("MLTV2", 0, 1, null,
        Filter.equal("lookupTable.id", ppTable.id),
        Filter.equal("key1", paymentTerm),
        Filter.equal("key2", paymentFrequency)
)
) {
    if (recList)
        return recList[0]?.attribute1
}