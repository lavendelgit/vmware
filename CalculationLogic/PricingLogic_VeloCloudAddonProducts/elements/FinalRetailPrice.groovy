def srp = api.getElement("SuggestedRetailPrice")

return srp?.toFloat()?.round(2)?.toBigDecimal()