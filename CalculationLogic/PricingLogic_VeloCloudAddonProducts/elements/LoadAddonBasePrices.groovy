def bandwidth = api.getElement("Bandwidth")
api.trace("Bandwidth", null, bandwidth)

def maxResults = api.getMaxFindResultsLimit()
def basePriceTable = api.findLookupTable("AddonBasePrice")

def recList = api.find("MLTV", 0, maxResults, null,
        Filter.equal("attribute2", bandwidth),
        Filter.equal("lookupTable.id", basePriceTable.id))


api.trace("Record List", null, recList.toString())

if (recList)
    return recList