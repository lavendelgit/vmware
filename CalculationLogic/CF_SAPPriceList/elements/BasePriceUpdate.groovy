Map stdProductSKUInfo = [:]
Map record = [:]
String sapSKU
BigDecimal basePrice
record = [
        "lookupTableId"  : out.SAPPriceListTable.id,
        "lookupTableName": out.SAPPriceListTable.uniqueName,
        "key1"           : Constants.BASE_PRICE,
        "attribute4"     : Constants.CURRENCY,
        "attribute5"     : Constants.SAP_BASE_SKU
]
for (productSku in out.ProductMappingAttributesValues) {
    baseSkus = api.local.baseSkuDetails?.findAll { it.product == productSku }
    for (baseSku in baseSkus) {
        basePrice = baseSku.basePrice as BigDecimal
        stdProductSKUInfo = api.local.stdProductsCache?.find { it.baseSku == baseSku.sku }
        sapSKU = stdProductSKUInfo.sapBaseSku
        if (basePrice) {
            record.key2 = sapSKU
            record.key3 = sapSKU
            record.attribute3 = basePrice
            api.addOrUpdate("MLTV3", record)
        }
    }
}
