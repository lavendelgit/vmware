List filter = [
        Filter.equal("name", "StandardProducts"),
        Filter.in("attribute1", out.ProductMappingAttributesValues)
]
recordStream = api.stream("PX30", null, *filter)
api.local.stdProductsCache = recordStream?.collect {
    [
            sku       : it.sku,
            baseSku   : it.attribute14,
            sapBaseSku: it.attribute29
    ]
}
recordStream?.close()
return