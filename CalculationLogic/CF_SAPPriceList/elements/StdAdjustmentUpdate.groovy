String prodHierarchy
String product
String offer
String base
String sapProductLevel
for (productSku in out.ProductMappingAttributesValues) {
    stdAdjustmentSkus = api.local.stdAdjustmentsCache?.findAll { it.attribute1 == productSku }
    for (stdAdjustmentSku in stdAdjustmentSkus) {
        product = out.CacheProductHierarchyMapping.find { it.value == stdAdjustmentSku.attribute1 }?.key
        offer = out.CacheProductHierarchyMapping.find { it.value == stdAdjustmentSku.attribute2 }?.key
        base = out.CacheProductHierarchyMapping.find { it.value == stdAdjustmentSku.attribute3 }?.key
        if (product) {
            sapProductLevel = product
            prodHierarchy = Constants.SAP_PROD
        }
        if (offer) {
            sapProductLevel += "_" + offer
            prodHierarchy = Constants.SAP_OFFER
        }
        if (base) {
            sapProductLevel += "_" + base
            prodHierarchy = Constants.SAP_BASE
        }
        currency = stdAdjustmentSku.attribute23
        currencyFactor = stdAdjustmentSku.attribute24
        if (currency && currencyFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, currency, currencyFactor, out.StdAdjustmentMetaData.attribute23, sapProductLevel, prodHierarchy)
        }
        metric = stdAdjustmentSku.attribute6
        metricFactor = stdAdjustmentSku.attribute14
        if (metric && metricFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, metric, metricFactor, out.StdAdjustmentMetaData.attribute6, sapProductLevel, prodHierarchy)
        }
        region = stdAdjustmentSku.attribute4
        regionFactor = stdAdjustmentSku.attribute13
        if (region && regionFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, region, regionFactor, out.StdAdjustmentMetaData.attribute4, sapProductLevel, prodHierarchy)
        }
        promotion = stdAdjustmentSku.attribute31
        promotionFactor = stdAdjustmentSku.attribute32
        if (promotion && promotionFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, promotion, promotionFactor, out.StdAdjustmentMetaData.attribute31, sapProductLevel, prodHierarchy)
        }
        upgrade = stdAdjustmentSku.attribute29
        upgradeFactor = stdAdjustmentSku.attribute30
        if (upgrade && upgradeFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, upgrade, upgradeFactor, out.StdAdjustmentMetaData.attribute29, sapProductLevel, prodHierarchy)
        }
        volumeTier = stdAdjustmentSku.attribute27
        volumeTierFactor = stdAdjustmentSku.attribute28
        if (volumeTier && volumeTierFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, volumeTier, volumeTierFactor, out.StdAdjustmentMetaData.attribute27, sapProductLevel, prodHierarchy)
        }
        dataCenter = stdAdjustmentSku.attribute12
        dataCenterFactor = stdAdjustmentSku.attribute20
        if (dataCenter && dataCenterFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, dataCenter, dataCenterFactor, out.StdAdjustmentMetaData.attribute12, sapProductLevel, prodHierarchy)
        }
        purchasingProgram = stdAdjustmentSku.attribute11
        purchasingProgramFactor = stdAdjustmentSku.attribute19
        if (purchasingProgram && purchasingProgramFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, purchasingProgram, purchasingProgramFactor, out.StdAdjustmentMetaData.attribute11, sapProductLevel, prodHierarchy)
        }
        segment = stdAdjustmentSku.attribute10
        segmentFactor = stdAdjustmentSku.attribute18
        if (segment && segmentFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, segment, segmentFactor, out.StdAdjustmentMetaData.attribute10, sapProductLevel, prodHierarchy)
        }
        supportType = stdAdjustmentSku.attribute9
        supportTypeFactor = stdAdjustmentSku.attribute17
        if (supportType && supportTypeFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, supportType, supportTypeFactor, out.StdAdjustmentMetaData.attribute9, sapProductLevel, prodHierarchy)
        }
        supportTier = stdAdjustmentSku.attribute8
        supportTierFactor = stdAdjustmentSku.attribute16
        if (supportTier && supportTierFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, supportTier, supportTierFactor, out.StdAdjustmentMetaData.attribute8, sapProductLevel, prodHierarchy)
        }
        term = stdAdjustmentSku.attribute7
        termFactor = stdAdjustmentSku.attribute15
        if (term && termFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, term, termFactor, out.StdAdjustmentMetaData.attribute7, sapProductLevel, prodHierarchy)
        }
        payment = stdAdjustmentSku.attribute21
        paymentFactor = stdAdjustmentSku.attribute22
        if (payment && paymentFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, payment, paymentFactor, out.StdAdjustmentMetaData.attribute21, sapProductLevel, prodHierarchy)
        }
        if (term && payment && paymentFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, payment + "_" + term, paymentFactor, out.StdAdjustmentMetaData.attribute21 + "_" + out.StdAdjustmentMetaData.attribute7, sapProductLevel, prodHierarchy)
        }
        hosting = stdAdjustmentSku.attribute37
        hostingFactor = stdAdjustmentSku.attribute38
        if (hosting && hostingFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, hosting, hostingFactor, out.StdAdjustmentMetaData.attribute37, sapProductLevel, prodHierarchy)
        }
        programOption = stdAdjustmentSku.attribute39
        programOptionFactor = stdAdjustmentSku.attribute40
        if (programOption && programOptionFactor) {
            Lib.sapPriceListUpdate(out.SAPPriceListTable, programOption, programOptionFactor, out.StdAdjustmentMetaData.attribute39, sapProductLevel, prodHierarchy)
        }
    }
}
