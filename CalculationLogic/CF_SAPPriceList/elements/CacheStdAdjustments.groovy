List filter = [
        Filter.equal("name", "StandardAdjustment"),
        Filter.in("attribute1", out.ProductMappingAttributesValues)
]
stdAdjustmentStream = api.stream("PX50", "sku", *filter)
api.local.stdAdjustmentsCache = stdAdjustmentStream?.collect { row -> row }
stdAdjustmentStream?.close()
return
