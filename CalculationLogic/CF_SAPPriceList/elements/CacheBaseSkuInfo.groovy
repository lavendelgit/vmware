List filter = [
        Filter.equal("name", "StandardBasePrice"),
        Filter.equal("attribute2", "Standalone"),
        Filter.in("attribute1", api.local.stdProductsCache?.collect { it.baseSku })
]
recordStream = api.stream("PX30", null, *filter)
api.local.baseSkuDetails = recordStream?.collect {
    [
            sku      : it.attribute1,
            product  : it.attribute3,
            basePrice: it.attribute6
    ]
}
recordStream?.close()
return