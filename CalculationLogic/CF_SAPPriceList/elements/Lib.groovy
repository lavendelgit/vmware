def sapPriceListUpdate(Map table, String name, BigDecimal value, String metaData, String sapProductLevel, String prodHierarchy) {
    value = value * 100
    Map record = [
            "lookupTableId"  : table.id,
            "lookupTableName": table.uniqueName,
            "key1"           : metaData,
            "key2"           : name,
            "key3"           : sapProductLevel,
            "attribute3"     : value,
            "attribute4"     : "%",
            "attribute5"     : prodHierarchy
    ]
    api.addOrUpdate("MLTV3", record)
}
