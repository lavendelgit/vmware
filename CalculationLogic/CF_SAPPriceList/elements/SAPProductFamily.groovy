List filters = [
        Filter.equal("attribute1", "Yes"),
]
return api.findLookupTableValues("SAPProductFamily", *filters)?.collect { it.name }
