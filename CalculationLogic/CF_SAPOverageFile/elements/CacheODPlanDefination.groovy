List filter = [
        Filter.equal("name", Constants.OD_PLAN_PX),
        Filter.in("attribute10", api.local.odSKUAssigment?.attribute1)
]
streamRecords = api.stream("PX20", null, *filter)
api.local.odPlanCache = streamRecords?.collect { it }
streamRecords?.close()
return