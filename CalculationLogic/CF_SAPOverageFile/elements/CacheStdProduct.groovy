List filter = [
        Filter.equal("name", Constants.STANDARD_PRODUCT),
        Filter.in("attribute29", out.SAPSKUFamily)
]
recordStream = api.stream("PX50", null, ["sku", "attribute1", "attribute2", "attribute4", "attribute14", "attribute29"], *filter)
api.local.stdProductsCache = recordStream?.collect {
    [
            sku       : it.sku,
            baseSku   : it.attribute14,
            sapBaseSku: it.attribute29,
            product   : it.attribute1,
            offer     : it.attribute2,
            base      : it.attribute4
    ]
}
recordStream?.close()
return