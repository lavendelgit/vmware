List filter = [
        Filter.equal("name", Constants.SAP_PRICING_CONDITION_TABLE),
        Filter.in("attribute16", out.SAPSKUFamily),
        Filter.or(Filter.isNull("attribute14"), Filter.equal("attribute14", "Commit"))
]
streamRecords = api.stream("PX20", "sku", *filter)
api.local.adjustmentsCache = streamRecords?.collect { row -> row }
streamRecords?.close()
return