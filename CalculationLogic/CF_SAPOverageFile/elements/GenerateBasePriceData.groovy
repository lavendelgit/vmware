List loadedSAPSKU = []
for (product in api.local.stdProductsCache?.product?.unique()) {
    baseSkus = api.local.baseSkuDetails?.values()?.findAll { it.product == product }
    for (baseSku in baseSkus) {
        sapBaseskuDetails = api.local.stdProductsCache?.find { it.baseSku == baseSku.sku }
        sapSKU = sapBaseskuDetails?.sapBaseSku
        adjustmentRecords = [:]
        adjustmentRecords[out.OverageFileMetaData[Constants.VALID_FROM]] = baseSku?.startDate
        adjustmentRecords[out.OverageFileMetaData[Constants.VALID_TO]] = baseSku?.endDate
        adjustmentRecords[out.OverageFileMetaData[Constants.TIME_FROM]] = "00:00"
        adjustmentRecords[out.OverageFileMetaData[Constants.TIME_TO]] = "23:59"
        adjustmentRecords[out.OverageFileMetaData[Constants.SAPSKU]] = sapSKU
        adjustmentRecords[out.OverageFileMetaData[Constants.USAGE_GROUP]] = api.local.odSKUAssigment?.find { it.attribute6 == sapSKU }?.attribute1
        adjustmentRecords[out.OverageFileMetaData[Constants.LIST_PRICE]] = baseSku?.basePrice
        adjustmentRecords.sku = sapSKU + "_" + Constants.SKU_REFERENCE_PRICE
        adjustmentRecords.attribute = Constants.LIST_PRICE
        currentPrice = baseSku?.basePrice
        existingPrice = api.local.overageFileCache?.findAll { it.sku == adjustmentRecords?.sku }?.getAt(0)?.getAt(out.OverageFileMetaData["List Price"])
        if (!api.local.overageFileCache?.sku?.contains(adjustmentRecords?.sku)) {
            api.local.payload << Lib.populateAdjustmentRecord(adjustmentRecords)
        } else if (currentPrice && api.local.overageFileCache?.sku?.contains(adjustmentRecords?.sku) && existingPrice != currentPrice) {
            if (api.local.overageFileCache?.findAll { it.sku == adjustmentRecords.sku }?.getAt(0)?.attribute14 == adjustmentRecords.attribute14) {
                api.local.updatePayload << Lib.populateUpdatePayload(adjustmentRecords, Constants.UPDATE_FACTOR)
            } else if (api.local.overageFileCache?.findAll { it.sku == adjustmentRecords.sku }?.getAt(0)?.attribute14 != adjustmentRecords.attribute14) {
                api.local.payload << Lib.populateAdjustmentRecord(adjustmentRecords)
                api.local.updatePayload << Lib.populateUpdatePayload(adjustmentRecords, Constants.UPDATE_DATE)
            }
        }
    }
}
return