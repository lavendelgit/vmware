return api.findLookupTableValues(Constants.OVERAGE_FILE_CONFIG_PP, Constants.PP_Attributes, null)?.collectEntries {
    attribute ->
        [
                (attribute.name): attribute
        ]
}