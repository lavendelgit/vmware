List populateAdjustmentRecord(Map params) {
    return [
            Constants.OVERAGEFILE_PX_TABLE,
            params.sku,
            params.attribute1,
            params.attribute2,
            params.attribute3,
            params.attribute4,
            params.attribute5,
            params.attribute6,
            params.attribute7,
            params.attribute8,
            params.attribute9,
            params.attribute10,
            params.attribute11,
            params.attribute12,
            params.attribute13,
            params.attribute14,
            params.attribute15,
            params.attribute16,
            params.attribute17,
            params.attribute18,
            params.attribute19,
            params.attribute20,
            params.attribute21,
            params.attribute22,
            params.attribute23,
            params.attribute24,
            params.attribute25,
            params.attribute26,
            params.attribute27,
            params.attribute28,
            params.attribute29,
            params.attribute30,
            params.attribute31,
            params.attribute32,
            params.attribute33,
            params.attribute34,
            params.attribute35,
            params.attribute36,
            params.attribute37,
            params.attribute38,
            params.attribute39,
            params.attribute40,
            params.attribute41,
            params.attribute42,
            params.attribute43,
            params.attribute44,
            params.attribute45,
            params.attribute46,
            params.attribute47,
            params.attribute48,
    ]
}


String getPreviousDate(String validDate) {
    String dateFormat = "yyyy-MM-dd"
    Date date = api.parseDate(dateFormat, validDate)
    Calendar calendar = api.calendar(date)
    calendar.add(Calendar.DATE, -1)
    return calendar.getTime().format("yyyy-MM-dd")
}


Map populateUpdatePayload(Map rowData, String update) {
    rowValue = api.local.overageFileCache?.find { it.sku == rowData.sku }
    if (update == Constants.UPDATE_DATE) {
        return [
                "operationType": "update",
                "data"         : [
                        "typedId"    : rowValue?.typedId,
                        "attribute15": getPreviousDate(rowData.attribute14)
                ],
                "oldValues"    : [
                        "version": rowValue?.version,
                        "typedId": rowValue?.typedId,
                ]
        ]
    } else if (update == Constants.UPDATE_FACTOR) {
        data = [:]
        attribute = rowData.attribute
        data.typedId = rowValue?.typedId
        data[out.OverageFileMetaData[attribute]] = rowData[out.OverageFileMetaData[attribute]]
        return [
                "operationType": "update",
                "data"         : data,
                "oldValues"    : [
                        "version": rowValue?.version,
                        "typedId": rowValue?.typedId,
                ]
        ]
    }

}