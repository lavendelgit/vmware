List filter = [
        Filter.equal("name", Constants.OVERAGEFILE_PX_TABLE),
        Filter.in("attribute26", out.SAPSKUFamily),
]
streamRecords = api.stream("PX50", "attribute26,-attribute14", *filter)
api.local.overageFileCache = streamRecords?.collect { row -> row }
streamRecords?.close()
return
