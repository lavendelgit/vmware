List filter = [
        Filter.equal("name", Constants.STANDARD_PRICE_TABLE),
        //Filter.equal("attribute2", "Standalone"),
        Filter.in("attribute1", api.local.stdProductsCache?.baseSku),

]
recordStream = api.stream("PX50", "attribute29", *filter)
api.local.baseSkuDetails = recordStream?.collectEntries {
    [(it.attribute1): [
            sku      : it.attribute1,
            product  : it.attribute3,
            basePrice: it.attribute6,
            startDate: it.attribute29,
            endDate  : it.attribute30
    ]]
}
recordStream?.close()
return