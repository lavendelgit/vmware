List filter = [
        Filter.equal("name", Constants.OD_SKU_ASSIGNMENT_PX),
        Filter.in("attribute6", out.SAPSKUFamily)
]
streamRecords = api.stream("PX20", null, *filter)
api.local.odSKUAssigment = streamRecords?.collect { it }
streamRecords?.close()
return