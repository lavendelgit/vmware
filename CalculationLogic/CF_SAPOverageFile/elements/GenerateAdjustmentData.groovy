Map adjustmentRecords = [:]
String adjustmentAttribute
BigDecimal existingAdjustment
BigDecimal currentAdjustemt
api.local.payload = []
api.local.updatePayload = []
for (adjustment in api.local.adjustmentsCache) {
    validAttributes = out.CacheValidConfiguration?.values()?.findAll { it.SAPSKU == adjustment.attribute16 } ?: out.CacheValidConfiguration?.values()?.findAll { it.SAPSKU == "*" }
    populateAdjustmentRecords(adjustment, validAttributes, out.OverageFileMetaData)
}
return


void populateAdjustmentRecords(Map adjustment, List validAttributes, Map overageFileMetaData) {
    adjustmentRecords = [:]
    adjustmentRecords.name = Constants.OVERAGEFILE_PX_TABLE
    adjustmentRecords.sku = adjustment?.sku
    adjustmentRecords[overageFileMetaData[Constants.VALID_FROM]] = adjustment?.attribute9
    adjustmentRecords[overageFileMetaData[Constants.VALID_TO]] = adjustment?.attribute10
    adjustmentRecords[overageFileMetaData[Constants.TIME_FROM]] = "00:00"
    adjustmentRecords[overageFileMetaData[Constants.TIME_TO]] = "23:59"
    adjustmentRecords[overageFileMetaData[Constants.SAPSKU]] = adjustment?.attribute16
    if (adjustment?.attribute5 == Constants.PAYMENT_TYPE_TERM) {
        adjustmentRecords.adjustmentAttribute = validAttributes?.find { it.SAPAttribute == adjustment?.attribute5 }?.Attribute
        adjustmentRecords.existingAdjustment = api.local.overageFileCache?.findAll { it.sku == adjustment?.sku }?.getAt(0)?.getAt(overageFileMetaData[Constants.BILLING_FREQUENCY_ADJUSTMENT_FACTOR])
        adjustmentRecords.currentAdjustemt = adjustment?.attribute7
        if (adjustment?.attribute6?.contains("|")) {
            adjustmentValues = adjustment?.attribute6?.split("\\" + "|") as List
        } else {
            adjustmentValues = adjustment?.attribute6
        }
        adjustmentRecords.attribute = Constants.BILLING_FREQUENCY_ADJUSTMENT_FACTOR
        adjustmentRecords[overageFileMetaData[Constants.BILLING_FREQUENCY]] = adjustmentValues?.getAt(0)
        adjustmentRecords[overageFileMetaData[Constants.BILLING_TERM]] = adjustmentValues?.getAt(1)
        adjustmentRecords[overageFileMetaData[Constants.BILLING_FREQUENCY_ADJUSTMENT_FACTOR]] = adjustment?.attribute7
        adjustmentRecords[overageFileMetaData[Constants.SAP_MATERIAL_NUMBER]] = api.local.odSKUAssigment?.find { it.attribute6 == adjustment?.attribute16 }?.attribute9
    } else {
        adjustmentRecords.adjustmentAttribute = validAttributes?.find { it.SAPAttribute == adjustment?.attribute5 }?.Attribute
        attributeFields = out.CacheOverageFileAttributeConfig[adjustmentRecords.adjustmentAttribute]?.sort { it.key }?.values()?.minus(Constants.EXCLUDE_ATTRIBUTES)?.unique()
        adjustmentRecords = populateAttributeAdjustmentRecords(adjustmentRecords, adjustment, out.OverageFileMetaData)
    }
    if (out.CacheOverageFileAttributeConfig[adjustmentRecords?.adjustmentAttribute] && !api.local.overageFileCache?.sku?.contains(adjustmentRecords?.sku)) {
        api.local.payload << Lib.populateAdjustmentRecord(adjustmentRecords)
    } else if (adjustment && api.local.overageFileCache?.sku?.contains(adjustmentRecords?.sku) && adjustmentRecords.existingAdjustment != adjustmentRecords.currentAdjustemt) {
        if (api.local.overageFileCache?.findAll { it.sku == adjustmentRecords.sku }?.getAt(0)?.attribute14 == adjustmentRecords.attribute14) {
            api.local.updatePayload << Lib.populateUpdatePayload(adjustmentRecords, Constants.UPDATE_FACTOR)
        } else if (api.local.overageFileCache?.findAll { it.sku == adjustmentRecords.sku }?.getAt(0)?.attribute14 != adjustmentRecords.attribute14) {
            api.local.payload << Lib.populateAdjustmentRecord(adjustmentRecords)
            api.local.updatePayload << Lib.populateUpdatePayload(adjustmentRecords, Constants.UPDATE_DATE)
        }
    }
}


Map populateAttributeAdjustmentRecords(Map adjustmentRecords, Map adjustment, Map overageFileMetaData) {
    for (attribute in attributeFields) {
        if (attribute && !attribute?.contains(Constants.FACTOR) && overageFileMetaData[attribute]) {
            if (attribute == Constants.SAP_MATERIAL_NUMBER) {
                adjustmentRecords[overageFileMetaData[attribute]] = api.local.odSKUAssigment?.find { it.attribute6 == adjustment?.attribute16 }?.attribute9
            } else if (attribute == Constants.PP_PROD_GROUP) {
                usageGroup = api.local.odSKUAssigment?.find { it.attribute6 == adjustment?.attribute16 }?.attribute1
                adjustmentRecords[overageFileMetaData[attribute]] = api.local.odPlanCache?.find { it.attribute10 == usageGroup }?.attribute7
            } else {
                adjustmentRecords[overageFileMetaData[attribute]] = adjustment?.attribute6
            }
        } else if (attribute && attribute?.contains(Constants.FACTOR)) {
            adjustmentRecords[overageFileMetaData[attribute]] = adjustment?.attribute7
            if (api.local.overageFileCache?.sku?.contains(adjustment?.sku)) {
                existingAdjustment = api.local.overageFileCache?.findAll { it.sku == adjustment?.sku }?.getAt(0)?.getAt(overageFileMetaData[attribute])
                currentAdjustemt = adjustmentRecords[overageFileMetaData[attribute]]
                adjustmentRecords.attribute = attribute
                adjustmentRecords.existingAdjustment = existingAdjustment
                adjustmentRecords.currentAdjustemt = currentAdjustemt
            }
        }
    }
    return adjustmentRecords
}