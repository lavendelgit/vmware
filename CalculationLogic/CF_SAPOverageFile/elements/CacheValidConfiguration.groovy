List filters = [
        Filter.in("key1", out.ProductsCache),
        Filter.or(Filter.in("key5", out.SAPSKUFamily), Filter.equal("key5", "*")),
        Filter.equal("attribute3", "Yes")
]
return api.findLookupTableValues(Constants.VALID_COMBINATION_CONFIG_PP, *filters)?.sort { it.attribute2 }?.collectEntries { configuration ->
    [
            (configuration.key1 + "_" + configuration.key2 + "_" + configuration.key3 + "_" + configuration.key4 + "_" + configuration.key5): [
                    "SAPSKU"      : configuration.key5,
                    "Product"     : configuration.key1,
                    "Offer"       : configuration.key2,
                    "Base"        : configuration.key3,
                    "Attribute"   : configuration.key4,
                    "SAPAttribute": configuration.attribute1,
                    "Sequence"    : configuration.attribute2 as Integer
            ]
    ]
}