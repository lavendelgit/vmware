if (api.local.adjustmentsCache?.attribute14?.contains(null) || api.local.adjustmentsCache?.attribute14?.contains("Commit")) {
    List updateData = []
    api.local.adjustmentsCache?.each { data ->
        updateData << populatePayload(data)
    }
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(updateData), true)
}

return


Map populatePayload(Map rowData) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"    : rowData?.typedId,
                    "attribute14": rowData?.attribute14 == Constants.COMMIT_UPLOAD ? Constants.BOTH_UPLOAD : Constants.OVERAGE_UPLOAD
            ],
            "oldValues"    : [
                    "version": rowData?.version,
                    "typedId": rowData?.typedId,
            ]
    ]
}
