if (api.isSyntaxCheck()) api.abortCalculation()

def longDesc
def components
def paymentTypeList = []
def bandwidthList = out.Bandwidth
def editionList = out.Edition
def softwareList = out.Software
def gatewayList = out.Gateway
def supportList = out.Support
def serviceList = out.Service
def subscriptionYearsList = out.PaymentTerm
def licenseList = out.LicenseClass
def renewalList = out.Renewal

def typeList = out.PaymentType
def payDescMap = api.local.payFreqMap

typeList.each { obj ->
    if (payDescMap[obj]) {
        paymentTypeList << payDescMap[obj]
    }
}


paymentTypeList = (paymentTypeList) ?: null
bandwidthList = (bandwidthList) ?: null
editionList = (editionList) ?: null
softwareList = (softwareList) ?: null
gatewayList = (gatewayList) ?: null
supportList = (supportList) ?: null
serviceList = (serviceList) ?: null
subscriptionYearsList = (subscriptionYearsList) ?: null
licenseList = (licenseList) ?: null
renewalList = (renewalList) ?: null


filter = [
        Filter.in("name", "VCSoftwareProducts"),
        Filter.in("attribute2", bandwidthList),
        Filter.in("attribute3", editionList),
        Filter.in("attribute4", softwareList),
        Filter.in("attribute5", gatewayList),
        Filter.in("attribute6", supportList),
        Filter.in("attribute7", serviceList),
        Filter.in("attribute9", subscriptionYearsList),
        Filter.in("attribute10", paymentTypeList),
        Filter.in("attribute11", licenseList),
        Filter.in("attribute12", renewalList)

]
recFullList = api.stream("PX20", "sku", *filter)

def resultMatrix = api.newMatrix("SKU Number", "SKU Short Description", "SKU Long Description", "Renewal",
        "Bandwidth", "Edition", "Orchestrator", "Gateway", "Components", "Support", "Service", "Payment Term", "Payment Type", "License Class")

recFullList.each {
    def row = [:]
    row.put("SKU Number", it.sku)
    row.put("SKU Short Description", it.attribute1)
    longDesc = it.attribute14
    longDesc = (it.attribute15) ? (longDesc + it.attribute15) : longDesc
    longDesc = (it.attribute16) ? (longDesc + it.attribute16) : longDesc
    longDesc = (it.attribute17) ? (longDesc + it.attribute17) : longDesc
    row.put("SKU Long Description", longDesc)
    row.put("Renewal", it.attribute12)
    row.put("Bandwidth", it.attribute2)
    row.put("Edition", it.attribute3)
    row.put("Orchestrator", it.attribute4)
    row.put("Gateway", it.attribute5)
    components = it.attribute4
    if (it.attribute5 && it.attribute5 != " ") {
        components += "-"
        components += it.attribute5
    }

    row.put("Components", components)
    row.put("Support", it.attribute6)
    row.put("Service", it.attribute7)
    row.put("Payment Term", it.attribute9)
    row.put("Payment Type", it.attribute10)
    row.put("License Class", it.attribute11)

    resultMatrix.addRow(row)
}

resultMatrix?.setEnableClientFilter(true)
return resultMatrix