def fetchBandwidthList() {
    def table = api.findLookupTable("BandwidthOptions")
    filter = [Filter.equal("lookupTable.id", table.id)]
    ppList = api.find("MLTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}

def fetchEditionList() {
    def table = api.findLookupTable("EditionOptionsNew")
    filter = [Filter.equal("lookupTable.id", table.id)]
    ppList = api.find("MLTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}

def fetchSoftwareList() {
    def table = api.findLookupTable("SoftwareOptions")
    filter = [Filter.equal("lookupTable.id", table.id),
              Filter.isNotEmpty("name")]
    ppList = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}

def fetchGatewayList() {
    def table = api.findLookupTable("GatewayOptions")
    filter = [Filter.equal("lookupTable.id", table.id)]
    ppList = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}

def fetchSupportList() {
    def table = api.findLookupTable("SupportOptions")
    filter = [Filter.equal("lookupTable.id", table.id)]
    ppList = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}


def fetchServiceList() {
    def table = api.findLookupTable("ServiceLevelFactor")
    filter = [Filter.equal("lookupTable.id", table.id)]
    ppList = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}

def fetchRenewalList() {
    def table = api.findLookupTable("RenewalOptions")
    filter = [Filter.equal("lookupTable.id", table.id)]
    ppList = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}

def fetchLicenseList() {
    def table = api.findLookupTable("BULicenseClass")
    filter = [
            Filter.equal("lookupTable.id", table.id),
            Filter.equal("key1", "VC")
    ]
    ppList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.key2 }
    return ppList.unique()
}


def fetchPaymentTermList() {
    def table = api.findLookupTable("VCSoftwareSubscriptionTerm")
    filter = [Filter.equal("lookupTable.id", table.id),
              Filter.equal("key1", "Software")]
    ppList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.attribute1 }
    return ppList.unique()
}

def fetchPaymentTYpeList() {
    def table = api.findLookupTable("PaymentFrequency")
    filter = [
            Filter.equal("lookupTable.id", table.id)
    ]
    ppList = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}