def table = api.findLookupTable("PaymentFrequency")
filter = [
        Filter.equal("lookupTable.id", table.id)
]
api.local.payFreqMap = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, *filter).collectEntries {
    [(it.name): it.value]
}
return