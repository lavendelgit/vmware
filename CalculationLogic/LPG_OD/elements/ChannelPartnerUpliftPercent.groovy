if (api.syntaxCheck) {
    return null
}
def tab = api.findLookupTable("Adjustments")
def dataCenter = api.getElement("DataCenter")
def tabList
if (dataCenter != null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("key1", "Channel Uplift"),
            Filter.equal("key2", dataCenter))
}
if (tabList == null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("key1", "Channel Uplift"))
}
if (tabList != null) {
    def channel = tabList[0].attribute1
    return channel
}