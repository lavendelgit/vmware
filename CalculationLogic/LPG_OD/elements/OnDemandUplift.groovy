if (api.syntaxCheck) {
    return null
}
def subscription = api.getElement("SubscriptionUplift")
def onDemandPercent = api.getElement("OnDemandUpliftPercent")
if (onDemandPercent != null && subscription != null) {
    onDemandPercent += 1
    return onDemandPercent * subscription
}