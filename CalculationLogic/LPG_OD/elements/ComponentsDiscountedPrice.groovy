if (api.syntaxCheck) {
    return null
}
def dataCenter = api.getElement("DataCenter")
def tab = api.findLookupTable("PerpetualSKUs")
def tabList
def discountedPrice = 0
if (dataCenter != null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("key2", dataCenter))
}
if (tabList == null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id))
}
if (tabList) {
    tabList.each {
        def price, discount, dPrice
        price = it.attribute2 ?: 0
        discount = it.attribute3 ?: 0
        dPrice = price * (1 - discount)
        api.trace("dPrice", null, dPrice)
        discountedPrice += dPrice
    }

}
return discountedPrice