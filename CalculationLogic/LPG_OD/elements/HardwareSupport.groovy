if (api.syntaxCheck) {
    return null
}
def aws = api.getElement("AWSi3Host")
def hwSupportPercent = api.getElement("HardwareSupportPercent")
if (aws != null && hwSupportPercent != null) {
    return aws * hwSupportPercent
}
