if (api.syntaxCheck) {
    return null
}
def threeYearTCO = api.getElement("3YearTCO") ?: 0
return threeYearTCO / 36 / 730