if (api.isSyntaxCheck()) return

def AWSOnDemand = api.getElement("UpliftedVMCOnAWSTerm")
def AWS1Year = api.getElement("VMCOnAWS1YearTerm")
def AWS3Year = api.getElement("VMCOnAWS3YearTerm")
def DIRRESEMC = api.getElement("DIRRESEMC")
def itemType = api.getElement("ItemType")

if (itemType == "On Demand") {
    if (AWSOnDemand != null && DIRRESEMC != null) {
        return AWSOnDemand * (1 - DIRRESEMC)
    }
}
if (itemType == "1 Year") {
    if (AWS1Year != null && DIRRESEMC != null) {
        return AWS1Year * (1 - DIRRESEMC)
    }
}
if (itemType == "3 Year") {
    if (AWS3Year != null && DIRRESEMC != null) {
        return AWS3Year * (1 - DIRRESEMC)
    }
}