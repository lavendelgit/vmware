def returnValue = "Standard"
if (api.getManualOverride("SnSPercent") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("SubscriptionUpliftPercent") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("OnDemandUpliftPercent") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("ChannelPartnerUpliftPercent") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("HardwareSupportPercent") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("VMCAdjustmentUplift") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("AWSAdjustmentUplift") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("TermDiscount") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("Disti") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("DIRRESDEFAULT") != null) {
    returnValue = "Non-Standard"
} else if (api.getManualOverride("DIRRESEMC") != null) {
    returnValue = "Non-Standard"
}

return returnValue