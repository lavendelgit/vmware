if (api.syntaxCheck) {
    return null
}
def dPrice = api.getElement("ComponentsDiscountedPrice") ?: 0
def sns = api.getElement("SnSPercent")
if (sns && dPrice) {
    return sns * dPrice
}