if (api.syntaxCheck) {
    return null
}
def perHour = api.getElement("PerHourPrice")
if (perHour != null) {
    return perHour / 12
}