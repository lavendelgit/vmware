def utilLib = libs.vmwareUtil.util
def distiUSD = api.getElement("DistiDollar")
def term = api.getElement("HostType")
def exchangeRate = utilLib.getExchangeRate("GBP", term)
if (exchangeRate != null)
    return distiUSD * exchangeRate
else
    return null