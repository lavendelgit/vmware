if (api.syntaxCheck) {
    return null
}
def total = api.getElement("VMCOnAWS") ?: 0
def sw = api.getElement("SW")
if (total && sw) {
    def hw = total - sw
    return hw
}