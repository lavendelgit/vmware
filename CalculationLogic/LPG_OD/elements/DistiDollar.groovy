if (api.isSyntaxCheck()) return

def AWSOnDemand = api.getElement("UpliftedVMCOnAWSTerm")
def AWS1Year = api.getElement("VMCOnAWS1YearTerm")
def AWS3Year = api.getElement("VMCOnAWS3YearTerm")
def disti = api.getElement("Disti")
def itemType = api.getElement("ItemType")

if (itemType == "On Demand") {
    if (AWSOnDemand != null && disti != null) {
        return AWSOnDemand * (1 - disti)
    }
}
if (itemType == "1 Year") {
    if (AWS1Year != null && disti != null) {
        return AWS1Year * (1 - disti)
    }
}
if (itemType == "3 Year") {
    if (AWS3Year != null && disti != null) {
        return AWS3Year * (1 - disti)
    }
}