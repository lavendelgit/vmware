if (api.isSyntaxCheck()) return
def VMCUplift = api.getElement("VMCAdjustmentUplift") ?: 0.0
VMCUplift += 1
def sw = api.getElement("SW")
if (VMCUplift != null && sw != null) {
    return VMCUplift * sw
}