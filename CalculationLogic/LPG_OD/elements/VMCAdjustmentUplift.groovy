if (api.isSyntaxCheck()) return
def tab = api.findLookupTable("VMCAdjustmentUplift")
def dataCenter = api.getElement("DataCenter")?.toString()
def tabList
if (dataCenter != null) {
    tabList = api.find("MLTV", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("name", dataCenter))
}
if (tabList != null) {
    def LCFactor = tabList[0]?.attribute3
    def latestFX = tabList[0]?.attribute4
    if (LCFactor != 0 && latestFX != 0 && LCFactor != null && latestFX != null) {
        def SaaS = LCFactor / latestFX - 1
        return SaaS
    }
}