if (api.isSyntaxCheck()) return

def AWSOnDemand = api.getElement("UpliftedVMCOnAWSTerm")
def AWS1Year = api.getElement("VMCOnAWS1YearTerm")
def AWS3Year = api.getElement("VMCOnAWS3YearTerm")
def DIRRESDEFAULT = api.getElement("DIRRESDEFAULT")
def itemType = api.getElement("ItemType")

if (itemType == "On Demand") {
    if (AWSOnDemand != null && DIRRESDEFAULT != null) {
        return AWSOnDemand * (1 - DIRRESDEFAULT)
    }
}
if (itemType == "1 Year") {
    if (AWS1Year != null && DIRRESDEFAULT != null) {
        return AWS1Year * (1 - DIRRESDEFAULT)
    }
}
if (itemType == "3 Year") {
    if (AWS3Year != null && DIRRESDEFAULT != null) {
        return AWS3Year * (1 - DIRRESDEFAULT)
    }
}