def utilLib = libs.vmwareUtil.util
def dirRESEMCDollar = api.getElement("DIRRESEMCDollar")
def term = api.getElement("HostType")
def exchangeRate = utilLib.getExchangeRate("AUD", term)
if (exchangeRate != null)
    return dirRESEMCDollar * exchangeRate
else
    return null