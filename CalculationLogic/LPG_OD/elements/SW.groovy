if (api.syntaxCheck) {
    return null
}
def total = api.getElement("VMCOnAWS") ?: 0
def channelPartnerUplift = api.getElement("ChannelPartnerUplift") ?: 0
if (total && channelPartnerUplift) {
    def sw = channelPartnerUplift
    return sw
}