def utilLib = libs.vmwareUtil.util
def dirRESDefaultDollar = api.getElement("DIRRESDEFAULTDollar")
def term = api.getElement("HostType")
def exchangeRate = utilLib.getExchangeRate("AUD", term)
if (exchangeRate != null)
    return dirRESDefaultDollar * exchangeRate
else
    return null