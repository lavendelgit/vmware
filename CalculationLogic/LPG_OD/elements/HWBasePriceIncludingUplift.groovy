if (api.isSyntaxCheck()) return
def AWSUplift = api.getElement("AWSAdjustmentUplift") ?: 0.0
AWSUplift += 1
def hw = api.getElement("HW")
if (AWSUplift != null && hw != null) {
    return AWSUplift * hw
}