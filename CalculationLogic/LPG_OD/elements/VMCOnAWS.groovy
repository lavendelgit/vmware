if (api.syntaxCheck) {
    return null
}
def channelPartnerUplift = api.getElement("ChannelPartnerUplift") ?: 0
def aws = api.getElement("AWSi3Host") ?: 0
def hardwareSupport = api.getElement("HardwareSupport") ?: 0
if (channelPartnerUplift && aws && hardwareSupport) {
    return channelPartnerUplift + aws + hardwareSupport
}