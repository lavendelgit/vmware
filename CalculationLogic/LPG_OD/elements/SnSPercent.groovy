if (api.syntaxCheck) {
    return null
}
def dataCenter = api.getElement("DataCenter")
def tab = api.findLookupTable("Adjustments")
def tabList, sns
if (dataCenter != null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("key1", "SnS"),
            Filter.equal("key2", dataCenter))
}
if (tabList == null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("key1", "SnS"))
}
if (tabList) {
    sns = tabList[0].attribute1 ?: 0
}
def override = api.getManualOverride("SnSPercent")
return sns