def licenseClass = libs.vmwareUtil.LoadPPData.getLTVAsList("LicenseClass", "name")
if (licenseClass) {
    return api.options("License Class", licenseClass)
}