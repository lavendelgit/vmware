def serviceLevelFactor = libs.vmwareUtil.LoadPPData.getLTVAsList("ServiceLevelFactor", "name")
if (serviceLevelFactor) {
    return api.options("Service Level Factor", serviceLevelFactor)
}