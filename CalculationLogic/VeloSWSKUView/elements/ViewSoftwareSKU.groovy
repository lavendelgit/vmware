if (api.isSyntaxCheck())
    return
def resultMatrix = api.newMatrix("Name", "Short Description", "Long Description", "Renewal", "Bandwidth", "Edition", "Orchestrator", "Gateway",
        "Components", "Support", "Service", "Payment Term", "Payment Type",
        "License Class")
def editionList = api.getElement("Edition")
def licenseClassList = api.getElement("LicenseClass")
def gatewayList = api.getElement("GatewayOptions")
def supportOptionsList = api.getElement("SupportOptions")
def softwareList = api.getElement("SoftwareOptions")
def serviceLevelList = api.getElement("ServiceLevelFactor")
def bandwidth = api.getElement("Bandwidth")
def paymentTerm = api.getElement("PaymentTerm")
def paymentType = api.getElement("PaymentType")
def renewalOptions = api.getElement("RenewalOptions")

def ppTable = api.findLookupTable("VeloSoftwareSKUs")
def longDescTable = api.findLookupTable("VeloSoftwareSKUsLongDescription")
def longDescValue
def startRow = 0
def maxRow = api.getMaxFindResultsLimit()
def recFullList = []
def recList

//We need to reset filters to "null" value (not []) in order to get results when filter was cleared
if (renewalOptions == []) renewalOptions = null
if (editionList == []) editionList = null
if (licenseClassList == []) licenseClassList = null
if (gatewayList == []) gatewayList = null
if (supportOptionsList == []) supportOptionsList = null
if (softwareList == []) softwareList = null
if (serviceLevelList == []) serviceLevelList = null
if (bandwidth == []) bandwidth = null
if (paymentTerm == []) paymentTerm = null
if (paymentType) paymentType = null

while (recList = api.find("MLTV", startRow, maxRow, null,
        Filter.equal("lookupTable.id", ppTable?.id),
        //Filter.in("attribute3","ENT"),
        Filter.in("attribute3", editionList),
        Filter.in("attribute10", licenseClassList),
        Filter.in("attribute2", bandwidth),
        Filter.in("attribute5", gatewayList),
        Filter.in("attribute6", supportOptionsList),
        Filter.in("attribute4", softwareList),
        Filter.in("attribute7", serviceLevelList),
        Filter.in("attribute8", paymentTerm),
        Filter.in("attribute9", paymentType),
        Filter.in("attribute15", renewalOptions)
)) {
    startRow += recList.size()
    recFullList.addAll(recList)
}

if (recFullList &&
        (editionList != null ||
                licenseClassList != null ||
                gatewayList != null ||
                supportOptionsList != null ||
                softwareList != null ||
                serviceLevelList != null ||
                bandwidth != null ||
                paymentTerm != null ||
                paymentType != null
) ) {
    recFullList.each {
        //longDescValue = api.find("JLTV", 0, 1, "", ["attributeExtension___LongDescription"], Filter.equal("lookupTable.id", longDescTable?.id), Filter.equal("name", it.name))
        def row = [:]
        row.put("Name", it.name)
        row.put("Short Description", it.attribute1)
        if (it.attribute13 == null) {
            row.put("Long Description", it.attribute12)
        } else {
            if (it.attribute14 == null) {
                row.put("Long Description", it.attribute12 + it.attribute13)
            } else {
                if (it.attribute16 == null) {
                    row.put("Long Description", it.attribute12 + it.attribute13 + it.attribute14)
                } else {
                    row.put("Long Description", it.attribute12 + it.attribute13 + it.attribute14 + it.attribute16)
                }
            }
        }
        row.put("Renewal", it.attribute15)
        row.put("Bandwidth", it.attribute2)
        row.put("Edition", it.attribute3)
        row.put("Orchestrator", it.attribute4)
        row.put("Gateway", it.attribute5)
        if (it.attribute5.trim() != "" && it.attribute4.trim() != "") {
            row.put("Components", it.attribute4 + "-" + it.attribute5)
        } else {
            row.put("Components", it.attribute4.trim() + it.attribute5.trim())
        }
        row.put("Support", it.attribute6)
        row.put("Service", it.attribute7)
        row.put("Payment Term", it.attribute8)
        row.put("Payment Type", it.attribute9)
        row.put("License Class", it.attribute10)
        resultMatrix.addRow(row)
    }
}
resultMatrix?.setEnableClientFilter(true)
return resultMatrix