def gatewayOptions = libs.vmwareUtil.LoadPPData.getLTVAsList("GatewayOptions", "name")
if (gatewayOptions) {
    return api.options("Gateway Options", gatewayOptions)
}