def renewalOptions = libs.vmwareUtil.LoadPPData.getLTVAsList("RenewalOptions", "name")
if (renewalOptions) {
    return api.options("Renewal Options", renewalOptions)
}