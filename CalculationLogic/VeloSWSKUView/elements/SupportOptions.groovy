def supportOptions = libs.vmwareUtil.LoadPPData.getLTVAsList("SupportOptions", "name")
if (supportOptions) {
    return api.options("Support Options", supportOptions)
}