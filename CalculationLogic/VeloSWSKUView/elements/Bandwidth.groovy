def table = api.findLookupTable("VeloSoftwareSKUs")
int start = 0
def recordList = []
def fieldValueList = []
int maxResults = api.getMaxFindResultsLimit()
while (records = api.find("MLTV", start, maxResults, "attribute2", ["attribute2"], true, Filter.equal("lookupTable.id", table.id))) {
    start += records.size()
    recordList.addAll(records)
}
if (recordList != null) {
    for (record in recordList) {
        fieldValueList.add(record.attribute2?.toString())
    }
}
//return fieldValueList
return api.options("Bandwidth", fieldValueList)