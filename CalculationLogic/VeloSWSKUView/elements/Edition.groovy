def editionList = libs.vmwareUtil.LoadPPData.getLTVAsList("EditionOptions", "name")
if (editionList) {
    return api.options("Edition", editionList)
}