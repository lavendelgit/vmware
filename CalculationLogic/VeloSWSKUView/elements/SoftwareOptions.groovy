def softwareOptions = libs.vmwareUtil.LoadPPData.getLTVAsList("SoftwareOptions", "name")
if (softwareOptions) {
    return api.options("Orchestrator", softwareOptions)
}