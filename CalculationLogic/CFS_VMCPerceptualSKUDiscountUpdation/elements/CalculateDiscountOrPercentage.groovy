api.local.discountPrice = out.CurrentItem.attribute7 ?: 0.0 as BigDecimal
api.local.discountPercentage = out.CurrentItem.attribute6 ?: 0.0 as BigDecimal
if (out.OverrideType == "Absolute" && out.ActualPrice > 0) {
    api.local.discountPercentage = (api.local.discountPrice - out.ActualPrice) / out.ActualPrice
} else if (out.OverrideType == "Percentage") {
    api.local.discountPrice = (out.ActualPrice + ((api.local.discountPercentage) * out.ActualPrice))?.setScale(0, BigDecimal.ROUND_HALF_UP)
}