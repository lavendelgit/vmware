def discountedCommercialLicense(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def commercialLPrice = libs.standardWaterfall.CommercialLicensePrice.commercialLicensePrice(currentRow)
    def licenseAdjustment = libs.standardWaterfall.LicenseAdjustment.licenseAdjustment(currentRow)

    if (ext != null) {
        if (ext == "C" || ext == "C-L1" ||
                ext == "C-L2" || ext == "C-L3" || ext == "C-L4" || ext == "C-T1" ||
                ext == "C-T2" || ext == "C-T3" || ext == "C-L7" || ext == "C-L8" ||
                ext == "C-L9" || ext == "C-L10") {
            if (commercialLPrice != null && licenseAdjustment != null) {
                licenseAdjustment = 1 - licenseAdjustment
                return commercialLPrice * licenseAdjustment
            }
        }
    }
}