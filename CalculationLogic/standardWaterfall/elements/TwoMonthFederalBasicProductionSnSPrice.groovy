def twoMonthFederalBasicProductionSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def twoMonthCommercialSnS = libs.standardWaterfall.TwoMonthCommercialBasicProductionSnSPrice.twoMonthCommercialBasicProductionSnSPrice(currentRow)
    if (ext != null) {
        if (ext == "2M-GSSS-F" || ext == "2M-PSSS-F") {
            if (twoMonthCommercialSnS != null) {
                return twoMonthCommercialSnS
            }
        }
    }
}