def oneYearAcademicBasicSupportSubscriptionPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def oneYearAcademicSnSPercent = libs.standardWaterfall.OneYearAcademicBasicSnSPercent.oneYearAcademicBasicSnSPercent(currentRow)
    def oneYearAcademicSnSPrice = libs.standardWaterfall.OneYearAcademicBasicSnSPrice.oneYearAcademicBasicSnSPrice(currentRow)
    def oneYearAcademicSSPercent = libs.standardWaterfall.OneYearAcademicBasicSupportSubscriptionPercent.oneYearAcademicBasicSupportSubscriptionPercent(currentRow)
    if (ext != null) {
        if (ext == "GSUB-A" || ext == "GSUP-A" ||
                ext == "3GSUB-A" || ext == "3GSUP-A" ||
                ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
            if (oneYearAcademicSnSPrice != null && oneYearAcademicSSPercent != null && oneYearAcademicSSPercent != null) {
                def result = (oneYearAcademicSnSPrice * oneYearAcademicSSPercent) / oneYearAcademicSnSPercent
                result = result?.toString()
                return libs.standardWaterfall.Library.truncate(result)
            }
        }
    }
}