def threeYearCommercialBasicProductionSnSPrice(currentRow) {

    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def threeYearDiscount = libs.standardWaterfall.ThreeYearDiscount.threeYearDiscount(currentRow)
    def oneYearCommercialSnS = libs.standardWaterfall.OneYearCommercialBasicProductionSnSPrice.oneYearCommercialBasicProductionSnSPrice(currentRow)
    if (ext != null) {
        if (ext == "3G-SSS-C" || ext == "3P-SSS-C" ||
                ext == "3G-SSS-F" || ext == "3P-SSS-F" ||
                ext == "3P-SSS-A") {
            if (threeYearDiscount != null && oneYearCommercialSnS != null) {
                threeYearDiscount = 1 - threeYearDiscount
                return 3 * threeYearDiscount * oneYearCommercialSnS
            }
        }
    }
}