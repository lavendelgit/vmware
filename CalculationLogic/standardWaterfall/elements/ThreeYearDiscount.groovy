def threeYearDiscount(currentRow) {

    if (api.syntaxCheck) {
        return null
    }

    def extension = libs.standardWaterfall.Extension.extension(currentRow)
    def multiYearTab
    if (extension == "3G-SSS-C" || extension == "3GSUB-C" || extension == "3GSUP-C" || extension == "3P-SSS-C" || extension == "3PSUB-C" || extension == "3PSUP-C" ||
            extension == "3G-SSS-A" || extension == "3GSUB-A" || extension == "3GSUP-A" || extension == "3P-SSS-A" || extension == "3PSUB-A" || extension == "3PSUP-A" ||
            extension == "3G-SSS-F" || extension == "3GSUB-F" || extension == "3GSUP-F" || extension == "3P-SSS-F" || extension == "3PSUB-F" || extension == "3PSUP-F") {
        multiYearTab = api.find("MLTV", api.filter("lookupTable.uniqueName", "MultiYearSnSSUPSUB"),
                api.filter("name", "3 Year"))
    }

    if (multiYearTab != null) {
        return multiYearTab[0]?.attribute2
    }
}