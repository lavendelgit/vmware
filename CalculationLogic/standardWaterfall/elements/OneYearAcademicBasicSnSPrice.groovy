def oneYearAcademicBasicSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def academicPrice = libs.standardWaterfall.AcademicLicense.academicLicense(currentRow)
    def oneYearAcademicSnS = libs.standardWaterfall.OneYearAcademicBasicSnSPercent.oneYearAcademicBasicSnSPercent(currentRow)
    if (ext != null) {
        if (ext == "G-SSS-A" || ext == "GSUB-A" || ext == "GSUP-A" ||
                ext == "3G-SSS-A" || ext == "3GSUB-A" || ext == "3GSUP-A" ||
                ext == "2M-GSSS-A" || ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
            if (academicPrice != null && oneYearAcademicSnS != null) {
                def result = academicPrice * oneYearAcademicSnS
                result = libs.standardWaterfall.Library.round(result, 0)
                return result
            }
        }
    }
}