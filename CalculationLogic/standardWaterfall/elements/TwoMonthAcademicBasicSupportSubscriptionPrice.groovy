def twoMonthAcademicBasicSupportSubscriptionPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def oneYearAcademicSS = libs.standardWaterfall.OneYearAcademicBasicSupportSubscriptionPrice.oneYearAcademicBasicSupportSubscriptionPrice(currentRow)
    if (ext != null) {
        if (ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
            if (oneYearAcademicSS != null) {
                return oneYearAcademicSS / 6
            }
        }
    }
}