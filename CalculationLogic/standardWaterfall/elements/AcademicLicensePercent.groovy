def academicLicensePercent(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def licenseTab
    if (ext == "A" ||
            ext == "G-SSS-A" || ext == "GSUB-A" || ext == "GSUP-A" ||
            ext == "3G-SSS-A" || ext == "3GSUB-A" || ext == "3GSUP-A" ||
            ext == "2M-GSSS-A" || ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
        licenseTab = api.find("MLTV", api.filter("lookupTable.uniqueName", "LicenseAdjustment"),
                api.filter("name", "A"))
    }
    if (licenseTab != null) {
        return licenseTab[0]?.attribute1
    }
}