def commercialLicensePrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }

    if (currentRow?.bundlePrice) return currentRow?.bundlePrice

    def commercialL = libs.standardWaterfall.CommercialLicense.commercialLicense(currentRow)
    def commercialLTab
    if (commercialL) {
        commercialLTab = api.find("PX", Filter.equal("name", "ListPrice"), Filter.equal("sku", commercialL))
    }
    if (commercialLTab != null) {
        def result = commercialLTab[0]?.attribute1
        return result
    }
}