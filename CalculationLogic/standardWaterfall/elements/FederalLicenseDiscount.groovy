def federalLicenseDiscount(currentRow) {

    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def licenseTab
    if (ext == "F-L1" || ext == "F-L2" || ext == "F-L3" || ext == "F-L4") {
        licenseTab = api.find("MLTV", api.filter("lookupTable.uniqueName", "LicenseAdjustment"),
                api.filter("name", ext))
    }
    if (licenseTab != null) {
        return licenseTab[0]?.attribute1
    }
}