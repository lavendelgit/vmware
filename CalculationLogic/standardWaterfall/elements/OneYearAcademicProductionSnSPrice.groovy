def oneYearAcademicProductionSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def price = libs.standardWaterfall.OneYearCommercialBasicProductionSnSPrice.oneYearCommercialBasicProductionSnSPrice(currentRow)
    if (ext == "P-SSS-A") {
        return price
    }
}