def academicLicense(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def commercialLPrice = libs.standardWaterfall.CommercialLicensePrice.commercialLicensePrice(currentRow)
    def acadPercent = libs.standardWaterfall.AcademicLicensePercent.academicLicensePercent(currentRow)
    if (ext != null) {
        if (ext == "A" ||
                ext == "G-SSS-A" || ext == "GSUB-A" || ext == "GSUP-A" ||
                ext == "3G-SSS-A" || ext == "3GSUB-A" || ext == "3GSUP-A" ||
                ext == "2M-GSSS-A" || ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
            if (commercialLPrice != null && acadPercent != null) {
                acadPercent = 1 - acadPercent
                def result = commercialLPrice * acadPercent
                result = libs.standardWaterfall.Library.round(result, 0)
                return result
            }
        }
    }
}