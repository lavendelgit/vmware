def threeYearAcademicProductionSSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def price = libs.standardWaterfall.ThreeYearCommercialBasicProductionSupportSubscriptionPrice.threeYearCommercialBasicProductionSupportSubscriptionPrice(currentRow)
    if (ext == "3PSUB-A" || ext == "3PSUP-A") {
        return price
    }
}