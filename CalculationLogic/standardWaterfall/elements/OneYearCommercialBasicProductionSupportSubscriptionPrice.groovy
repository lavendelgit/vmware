def oneYearCommercialBasicProductionSupportSubscriptionPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }

    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def oneYearCommercialSnSPercent = libs.standardWaterfall.OneYearCommercialBasicProductionSnSPercent.oneYearCommercialBasicProductionSnSPercent(currentRow)
    def oneYearCommercialSnSPrice = libs.standardWaterfall.OneYearCommercialBasicProductionSnSPrice.oneYearCommercialBasicProductionSnSPrice(currentRow)
    def oneYearCommercialSSPercent = libs.standardWaterfall.OneYearCommercialBasicProductionSupportSubscriptionPercent.oneYearCommercialBasicProductionSupportSubscriptionPercent(currentRow)
    if (ext != null) {
        if (ext == "GSUB-C" || ext == "GSUP-C" ||
                ext == "3GSUB-C" || ext == "3GSUP-C" ||
                ext == "PSUB-C" || ext == "PSUP-C" ||
                ext == "3PSUB-C" || ext == "3PSUP-C" ||
                ext == "2M-GSUB-C" || ext == "2M-GSUP-C" ||
                ext == "2M-PSUB-C" || ext == "2M-PSUP-C" ||
                ext == "GSUB-F" || ext == "GSUP-F" ||
                ext == "3GSUB-F" || ext == "3GSUP-F" ||
                ext == "PSUB-F" || ext == "PSUP-F" ||
                ext == "3PSUB-F" || ext == "3PSUP-F" ||
                ext == "2M-GSUB-F" || ext == "2M-GSUP-F" ||
                ext == "2M-PSUB-F" || ext == "2M-PSUP-F" ||
                ext == "PSUB-A" || ext == "PSUP-A" ||
                ext == "3PSUB-A" || ext == "3PSUP-A" ||
                ext == "2M-PSUB-A" || ext == "2M-PSUP-A") {
            if (oneYearCommercialSnSPrice != null && oneYearCommercialSSPercent != null && oneYearCommercialSSPercent != null) {
                def result = (oneYearCommercialSnSPrice * oneYearCommercialSSPercent) / oneYearCommercialSnSPercent
//perc
                result = result?.toString()
                return libs.standardWaterfall.Library.truncate(result)
            }
        }
    }
}