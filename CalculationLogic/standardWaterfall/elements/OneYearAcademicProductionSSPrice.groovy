def oneYearAcademicProductionSSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def price = libs.standardWaterfall.OneYearCommercialBasicProductionSupportSubscriptionPrice.oneYearCommercialBasicProductionSupportSubscriptionPrice(currentRow)
    if (ext == "PSUB-A" || ext == "PSUP-A") {
        return price
    }
}