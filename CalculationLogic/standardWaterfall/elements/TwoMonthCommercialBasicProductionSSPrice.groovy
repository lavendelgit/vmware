def twoMonthCommercialBasicProductionSSPrice(currentRow) {

    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def oneYearCommercialSS = libs.standardWaterfall.OneYearCommercialBasicProductionSupportSubscriptionPrice.oneYearCommercialBasicProductionSupportSubscriptionPrice(currentRow)
    if (ext != null) {
        if (ext == "2M-GSUB-C" || ext == "2M-GSUP-C" || ext == "2M-PSUB-C" || ext == "2M-PSUP-C" ||
                ext == "2M-GSUB-F" || ext == "2M-GSUP-F" || ext == "2M-PSUB-F" || ext == "2M-PSUP-F" ||
                ext == "2M-PSUB-A" || ext == "2M-PSUP-A") {
            if (oneYearCommercialSS != null) {
                return oneYearCommercialSS / 6
            }
        }
    }
}