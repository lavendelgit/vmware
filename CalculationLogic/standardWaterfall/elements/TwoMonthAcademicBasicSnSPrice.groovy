def twoMonthAcademicBasicSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def oneYearAcademicSnS = libs.standardWaterfall.OneYearAcademicBasicSnSPrice.oneYearAcademicBasicSnSPrice(currentRow)
    if (ext != null) {
        if (ext == "2M-GSSS-A") {
            if (oneYearAcademicSnS != null) {
                return oneYearAcademicSnS / 6
            }
        }
    }
}