def threeYearAcademicBasicSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def threeYearDiscount = libs.standardWaterfall.ThreeYearDiscount.threeYearDiscount(currentRow)
    def oneYearAcademicSnS = libs.standardWaterfall.OneYearAcademicBasicSnSPrice.oneYearAcademicBasicSnSPrice(currentRow)
    if (ext != null) {
        if (ext == "3G-SSS-A") {
            if (threeYearDiscount != null && oneYearAcademicSnS != null) {
                threeYearDiscount = 1 - threeYearDiscount
                return 3 * threeYearDiscount * oneYearAcademicSnS
            }
        }
    }
}