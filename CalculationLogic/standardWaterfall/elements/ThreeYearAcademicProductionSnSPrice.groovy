def threeYearAcademicProductionSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def price = libs.standardWaterfall.ThreeYearCommercialBasicProductionSnSPrice.threeYearCommercialBasicProductionSnSPrice(currentRow)
    if (ext == "3P-SSS-A") {
        return price
    }
}
