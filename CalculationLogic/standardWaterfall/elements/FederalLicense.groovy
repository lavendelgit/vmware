def federalLicense(currentRow) {
    if (api.syntaxCheck) {
        return null
    }

    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def commercialLPrice = libs.standardWaterfall.CommercialLicensePrice.commercialLicensePrice(currentRow)
    def fedPercent = libs.standardWaterfall.FederalLicensePercent.federalLicensePercent(currentRow)

    if (ext != null) {

        if (ext == "F" || ext == "F-L1" || ext == "F-L2" || ext == "F-L3" || ext == "F-L4") {
            if (commercialLPrice != null && fedPercent != null) {
                fedPercent = 1 - fedPercent
                def result = commercialLPrice * fedPercent
                result = result?.toString()
                return libs.standardWaterfall.Library.truncate(result)
            }
        }
    }
}