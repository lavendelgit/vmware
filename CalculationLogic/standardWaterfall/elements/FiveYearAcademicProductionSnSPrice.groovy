def fiveYearAcademicProductionSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def fiveYearDiscount = libs.standardWaterfall.FiveYearDiscount.fiveYearDiscount(currentRow)
    def oneYearCommercialSnS = libs.standardWaterfall.OneYearCommercialBasicProductionSnSPrice.oneYearCommercialBasicProductionSnSPrice(currentRow)
    if (ext != null) {
        if (ext == "5P-SSS-A") {
            if (fiveYearDiscount != null && oneYearCommercialSnS != null) {
                fiveYearDiscount = 1 - fiveYearDiscount
                return 5 * fiveYearDiscount * oneYearCommercialSnS
            }
        }
    }
}