def oneYearCommercialBasicProductionSupportSubscriptionPercent(currentRow) {
    if (api.syntaxCheck) {
        return null
    }

    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    if (ext == "3GSUB-C" || ext == "2M-GSUB-C" ||
            ext == "3GSUB-F" || ext == "2M-GSUB-F" || ext == "GSUB-F") {
        ext = "GSUB-C"
    }
    if (ext == "3PSUB-C" || ext == "2M-PSUB-C" ||
            ext == "3PSUB-F" || ext == "2M-PSUB-F" || ext == "PSUB-F" ||
            ext == "3PSUB-A" || ext == "2M-PSUB-A" || ext == "PSUB-A") {
        ext = "PSUB-C"
    }
    if (ext == "3GSUP-C" || ext == "2M-GSUP-C" ||
            ext == "3GSUP-F" || ext == "2M-GSUP-F" || ext == "GSUP-F") {
        ext = "GSUP-C"
    }
    if (ext == "3PSUP-C" || ext == "2M-PSUP-C" ||
            ext == "3PSUP-F" || ext == "2M-PSUP-F" || ext == "PSUP-F" ||
            ext == "3PSUP-A" || ext == "2M-PSUP-A" || ext == "PSUP-A") {
        ext = "PSUP-C"
    }
    def snsTab
    if (ext != null) {
        snsTab = api.find("MLTV", api.filter("lookupTable.uniqueName", "SupportMaintenance"),
                api.filter("name", ext))
    }
    if (snsTab != null) {
        return snsTab[0]?.attribute1
    }
}