def threeYearFederalBasicProductionSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def threeYearCommercialSnS = libs.standardWaterfall.ThreeYearCommercialBasicProductionSnSPrice.threeYearCommercialBasicProductionSnSPrice(currentRow)
    if (ext != null) {
        if (ext == "3G-SSS-F" || ext == "3P-SSS-F") {
            if (threeYearCommercialSnS != null) {
                return threeYearCommercialSnS
            }
        }
    }
}