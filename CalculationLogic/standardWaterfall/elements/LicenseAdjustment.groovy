def licenseAdjustment(currentRow) {
    if (api.syntaxCheck) {
        return null
    }

    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def licenseTab
    if (ext != null) {
        licenseTab = api.find("MLTV", api.filter("lookupTable.uniqueName", "LicenseAdjustment"),
                api.filter("name", ext))
    }
    if (licenseTab != null) {
        return licenseTab[0]?.attribute1
    }
}