def threeYearAcademicBasicSupportSubscriptionPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def threeYearDiscount = libs.standardWaterfall.ThreeYearDiscount.threeYearDiscount(currentRow)
    def oneYearAcademicSS = libs.standardWaterfall.OneYearAcademicBasicSupportSubscriptionPrice.oneYearAcademicBasicSupportSubscriptionPrice(currentRow)
    if (ext != null) {
        if (ext == "3GSUB-A" || ext == "3GSUP-A") {
            if (threeYearDiscount != null && oneYearAcademicSS != null) {
                threeYearDiscount = 1 - threeYearDiscount
                return 3 * threeYearDiscount * oneYearAcademicSS
            }
        }
    }
}