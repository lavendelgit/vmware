def threeYearFederalBasicProductionSupportSubscriptionPrice(currentRow) {

    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def threeYearCommercialSS = libs.standardWaterfall.ThreeYearCommercialBasicProductionSupportSubscriptionPrice.threeYearCommercialBasicProductionSupportSubscriptionPrice(currentRow)
    if (ext != null) {
        if (ext == "3GSUB-F" || ext == "3GSUP-F" || ext == "3PSUB-F" || ext == "3PSUP-F") {
            if (threeYearCommercialSS != null) {
                return threeYearCommercialSS
            }
        }
    }
}