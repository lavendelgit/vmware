def oneYearCommercialBasicProductionSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def commercialLPrice = libs.standardWaterfall.CommercialLicensePrice.commercialLicensePrice(currentRow)
    def oneYearCommercialSnS = libs.standardWaterfall.OneYearCommercialBasicProductionSnSPercent.oneYearCommercialBasicProductionSnSPercent(currentRow)
    if (ext != null) {
        if (ext == "G-SSS-C" || ext == "GSUB-C" || ext == "GSUP-C" ||
                ext == "3G-SSS-C" || ext == "3GSUB-C" || ext == "3GSUP-C" ||
                ext == "P-SSS-C" || ext == "PSUB-C" || ext == "PSUP-C" ||
                ext == "3P-SSS-C" || ext == "3PSUB-C" || ext == "3PSUP-C" ||
                ext == "2M-GSSS-C" || ext == "2M-GSUB-C" || ext == "2M-GSUP-C" ||
                ext == "2M-PSSS-C" || ext == "2M-PSUB-C" || ext == "2M-PSUP-C" ||
                ext == "G-SSS-F" || ext == "GSUB-F" || ext == "GSUP-F" ||
                ext == "3G-SSS-F" || ext == "3GSUB-F" || ext == "3GSUP-F" ||
                ext == "P-SSS-F" || ext == "PSUB-F" || ext == "PSUP-F" ||
                ext == "3P-SSS-F" || ext == "3PSUB-F" || ext == "3PSUP-F" ||
                ext == "2M-GSSS-F" || ext == "2M-GSUB-F" || ext == "2M-GSUP-F" ||
                ext == "2M-PSSS-F" || ext == "2M-PSUB-F" || ext == "2M-PSUP-F" ||
                ext == "PSUB-A" || ext == "PSUP-A" || ext == "P-SSS-A" ||
                ext == "3P-SSS-A" || ext == "3PSUB-A" || ext == "3PSUP-A" ||
                ext == "2M-PSSS-A" || ext == "2M-PSUB-A" || ext == "2M-PSUP-A" ||
                ext == "5P-SSS-C" || ext == "5P-SSS-A" || ext == "5P-SSS-F") {
            if (commercialLPrice != null && oneYearCommercialSnS != null) {
                def result
                if (currentRow?.sumOfComponents) {
                    result = currentRow.sumOfComponents * oneYearCommercialSnS
                } else {
                    result = commercialLPrice * oneYearCommercialSnS
                }
                result = libs.standardWaterfall.Library.round(result, 0)
                return result
            }
        }
    }
}