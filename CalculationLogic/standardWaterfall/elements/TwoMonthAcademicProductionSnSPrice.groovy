def twoMonthAcademicProductionSnSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def price = libs.standardWaterfall.TwoMonthCommercialBasicProductionSnSPrice.twoMonthCommercialBasicProductionSnSPrice(currentRow)
    if (ext == "2M-PSSS-A") {
        return price
    }
}