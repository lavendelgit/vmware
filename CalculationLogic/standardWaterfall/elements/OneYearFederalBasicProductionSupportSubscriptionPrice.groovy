def oneYearFederalBasicProductionSupportSubscriptionPrice(currentRow) {

    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def oneYearCommercialSS = libs.standardWaterfall.OneYearCommercialBasicProductionSupportSubscriptionPrice.oneYearCommercialBasicProductionSupportSubscriptionPrice(currentRow)
    if (ext != null) {
        if (ext == "GSUB-F" || ext == "GSUP-F" || ext == "PSUB-F" || ext == "PSUP-F") {
            if (oneYearCommercialSS != null) {
                return oneYearCommercialSS
            }
        }
    }
}