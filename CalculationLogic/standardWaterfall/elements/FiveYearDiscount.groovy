def fiveYearDiscount(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def extension = libs.standardWaterfall.Extension.extension(currentRow)
    def multiYearTab
    if (extension == "5P-SSS-C" || extension == "5P-SSS-A" || extension == "5P-SSS-F") {
        multiYearTab = api.find("MLTV", api.filter("lookupTable.uniqueName", "MultiYearSnSSUPSUB"),
                api.filter("name", "5 Year"))
    }
    if (multiYearTab != null) {
        return multiYearTab[0]?.attribute2
    }
}