def twoMonthFederalBasicProductionSSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def twoMonthCommercialSS = libs.standardWaterfall.TwoMonthCommercialBasicProductionSSPrice.twoMonthCommercialBasicProductionSSPrice(currentRow)
    if (ext != null) {
        if (ext == "2M-GSUB-F" || ext == "2M-GSUP-F" || ext == "2M-PSUB-F" || ext == "2M-PSUP-F") {
            if (twoMonthCommercialSS != null) {
                return twoMonthCommercialSS
            }
        }
    }
}