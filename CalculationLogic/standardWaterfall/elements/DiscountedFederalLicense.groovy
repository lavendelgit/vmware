def discountedFederalLicense(currentRow) {

    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def federalLPrice = libs.standardWaterfall.FederalLicense.federalLicense(currentRow)
    def licenseAdjustment = libs.standardWaterfall.FederalLicenseDiscount.federalLicenseDiscount(currentRow)
    if (ext != null) {
        if (ext == "F-L1" || ext == "F-L2" || ext == "F-L3" || ext == "F-L4") {
            if (federalLPrice != null && licenseAdjustment != null) {
                licenseAdjustment = 1 - licenseAdjustment
                return federalLPrice * licenseAdjustment
            }
        }
    }
}