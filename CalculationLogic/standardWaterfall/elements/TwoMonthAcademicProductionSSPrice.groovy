def twoMonthAcademicProductionSSPrice(currentRow) {
    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def price = libs.standardWaterfall.TwoMonthCommercialBasicProductionSSPrice.twoMonthCommercialBasicProductionSSPrice(currentRow)
    if (ext == "2M-PSUB-A" || ext == "2M-PSUP-A") {
        return price
    }
}