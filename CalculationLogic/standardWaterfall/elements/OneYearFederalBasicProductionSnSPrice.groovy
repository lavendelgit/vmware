def oneYearFederalBasicProductionSnSPrice(currentRow) {

    if (api.syntaxCheck) {
        return null
    }
    def ext = libs.standardWaterfall.Extension.extension(currentRow)
    def oneYearCommercialSnS = libs.standardWaterfall.OneYearCommercialBasicProductionSnSPrice.oneYearCommercialBasicProductionSnSPrice(currentRow)
    if (ext != null) {
        if (ext == "G-SSS-F" || ext == "P-SSS-F") {
            if (oneYearCommercialSnS != null) {
                return oneYearCommercialSnS
            }
        }
    }
}