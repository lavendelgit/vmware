BigDecimal baseDCOneHourOD = api.global.AWSHWPriceBaseDCData?.getAt(out.ServiceKey)?.OneHourOD ?: 0.0
BigDecimal oneHourOD = out.CurrentItem?.attribute3 ?: 0.0
return ((baseDCOneHourOD > 0 ? (oneHourOD - baseDCOneHourOD) / baseDCOneHourOD : 0.0) * 100)?.setScale(10, BigDecimal.ROUND_HALF_UP)



