if (!api.global.baseDC) {
    api.global.baseDC = api.findLookupTableValues("VMCFactorConstants", Filter.equal("name", "Base DC")).attribute1?.getAt(0) ?: "US West (Oregon)"
}