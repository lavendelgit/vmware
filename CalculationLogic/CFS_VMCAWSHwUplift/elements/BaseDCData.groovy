if (!api.global.AWSHWPriceBaseDCData) {
    api.global.AWSHWPriceBaseDCData = api.findLookupTableValues("VMCAwsHwPrices", Filter.equal("key1", api.global.baseDC))?.collectEntries {
        AWSHwPrice ->
            [(AWSHwPrice.key2): [
                    "OneHourOD"                 : AWSHwPrice.attribute3,
                    "OneYearEffectiveHrlyRate"  : AWSHwPrice.attribute4,
                    "ThreeYearEffectiveHrlyRate": AWSHwPrice.attribute5
            ]
            ]
    }
}
