BigDecimal baseDCOneYearHrlyRate = api.global.AWSHWPriceBaseDCData?.getAt(out.ServiceKey)?.OneYearEffectiveHrlyRate ?: 0.0
BigDecimal oneYrHrlyRate = out.CurrentItem?.attribute4 ?: 0.0
return ((baseDCOneYearHrlyRate > 0 ? (oneYrHrlyRate - baseDCOneYearHrlyRate) / baseDCOneYearHrlyRate : 0.0) * 100)?.setScale(10, BigDecimal.ROUND_HALF_UP)
