BigDecimal baseDCThreeYearHrlyRate = api.global.AWSHWPriceBaseDCData?.getAt(out.ServiceKey)?.ThreeYearEffectiveHrlyRate ?: 0.0
BigDecimal ThreeYrHrlyRate = out.CurrentItem?.attribute5 ?: 0.0
return ((baseDCThreeYearHrlyRate > 0 ? (ThreeYrHrlyRate - baseDCThreeYearHrlyRate) / baseDCThreeYearHrlyRate : 0.0) * 100)?.setScale(10, BigDecimal.ROUND_HALF_UP)
