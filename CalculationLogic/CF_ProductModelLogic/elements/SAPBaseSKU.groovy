return api.findLookupTableValues(Constants.SAP_BASE_SKU_TABLE, Filter.equal(Constants.INCLUSION_FLAG_COLUMN, Constants.YES))?.collect { it.name }?.getAt(0)
