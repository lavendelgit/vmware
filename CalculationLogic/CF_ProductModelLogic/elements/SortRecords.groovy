api.local.attributeRecords?.sort { firstAttribute, secondAttribute ->
    firstAttribute.attribute <=> secondAttribute.attribute ?: firstAttribute.sequence <=> secondAttribute.sequence
}
return


