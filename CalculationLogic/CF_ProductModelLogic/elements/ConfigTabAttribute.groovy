return api.findLookupTableValues(Constants.CONFIG_TAB_ATTRIBUTES_TABLE)?.findAll {
    it.attribute1 == Constants.YES
}?.collectEntries { attribute ->
    [
            (attribute.name.replaceAll("\\s", "").minus(Constants.SIZE)): [
                    "Type"   : attribute.attribute2,
                    "SAPName": attribute.attribute3
            ]
    ]

}