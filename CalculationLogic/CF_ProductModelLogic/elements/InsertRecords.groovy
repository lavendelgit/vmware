if (api.local.pxRecords) {
    payload = [
            "data": [
                    "data"   : api.local.pxRecords,
                    "header" : [
                            "name",
                            "sku",
                            "attribute1",
                            "attribute2",
                            "attribute3",
                            "attribute4",
                            "attribute6",
                            "attribute7",
                            "attribute8",
                            "attribute9",
                            "attribute10",
                    ],
                    "options": [
                            "detectJoinFields"    : true,
                            "maxJoinFieldsLengths": []
                    ]
            ]]
    api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
    return
}
