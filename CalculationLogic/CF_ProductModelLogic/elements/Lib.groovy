List insertRecords(String tableName, Map params) {
    return [
            tableName,
            params.sku,
            params.attribute,
            params.value,
            params.sequence,
            params.type,
            params.startDate,
            params.endDate,
            params.Product,
            params.Offer,
            params.Base,
    ]
}


Map cachePriceRegionAttributes(String tableName) {
    return api.findLookupTableValues(tableName)?.findAll { it.attribute1 == Constants.YES }?.collectEntries {
        [(it.name): it]
    }
}

String getStandardProductAttributeName(String stdAttributeName) {
    List stdAttributeList = libs.stdProductLib.ConstConfiguration.STD_ATTRIBUTES_LIST
    Map standardProductAttributeMap = libs.stdProductLib.ConstConfiguration.STANDARDPRODUCT_ATTRIBUTE_MAPPING
    return stdAttributeList?.contains(stdAttributeName) ? standardProductAttributeMap?.find { it.value == stdAttributeName }?.key : stdAttributeName
}