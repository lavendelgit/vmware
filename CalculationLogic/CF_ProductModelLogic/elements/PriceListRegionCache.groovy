api.local.currency = []
api.local.priceListRegion = []
List filter = [
        Filter.equal(Constants.NAME, Constants.PRICELIST_REGION_ATTRIBUTE_NAME),
        Filter.in(Constants.SKU, api.local.skus),
]
recordStream = api.stream("PX20", null, [Constants.CURRENCY_COLUMN, Constants.CHANNEL_LAUNCH_REGION_COLUMN, Constants.MASTER_LAUNCH_REGION_COLUMN], *filter)
recordStream?.collect {
    api.local.currency << it.attribute1
    api.local.priceListRegion << it.attribute2 ?: it.attribute3
}
recordStream?.close()
api.local.currency = api.local.currency?.unique()
api.local.priceListRegion = api.local.priceListRegion?.unique()
return