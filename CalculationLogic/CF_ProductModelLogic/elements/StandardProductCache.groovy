List filter = [
        Filter.equal(Constants.NAME, Constants.STANDARD_PRODUCT_TABLE),
        Filter.in(Constants.SAP_BASE_SKU_COLUMN, out.SAPBaseSKU),
]
recordStream = api.stream("PX50", null, *filter)
standardProducts = recordStream?.collect { row -> row }
recordStream?.close()
api.local.validStandardProductCache = libs.stdProductLib.SkuGenHelper.cacheValidRules(standardProducts, out.StandardProductMetaData, out.RuleKeyMapping, true)
return