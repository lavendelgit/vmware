api.local.skus = []
for (standardProduct in api.local.validStandardProductCache) {
    api.local.skus << standardProduct?.value?.StandardProductSKU
}
return