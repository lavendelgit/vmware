List sapBaseSkuKey = []
List configAttributeKey = []
List valueKey = []

for (record in api.local.attributeRecords) {
    sapBaseSkuKey << record.sku
    configAttributeKey << record.attribute
    valueKey << record.value
}

List uniqueSapBaseSku = sapBaseSkuKey.unique()
List uniqueConfigAttribute = configAttributeKey.unique()
List uniqueValue = valueKey.unique()

if (api.local.pxRecords) {
    def deletePL = [
            "data": [
                    "filterCriteria": [
                            "operator"    : "and",
                            "_constructor": "AdvancedCriteria",
                            "criteria"    : [
                                    [
                                            "fieldName": "name",
                                            "operator" : "equals",
                                            "value"    : Constants.CONFIG_TAB_TABLE
                                    ],
                                    [
                                            "fieldName": "sku",
                                            "operator" : "inSet",
                                            "value"    : uniqueSapBaseSku
                                    ],
                                    [
                                            "fieldName": "attribute1",
                                            "operator" : "inSet",
                                            "value"    : uniqueConfigAttribute
                                    ],
                                    [
                                            "fieldName": "attribute2",
                                            "operator" : "inSet",
                                            "value"    : uniqueValue
                                    ]
                            ]
                    ]
            ]
    ]
    api.boundCall("boundcall", "delete/PX/batch", api.jsonEncode(deletePL), false)
    return
}