List configAttributes = out.ConfigTabAttribute?.keySet() as List
String attributeName = Constants.ATTRIBUTE5_COLUMN

configAttributes?.each {
    attributeName = (it == Constants.TERM_ATTRIBUTE_NAME || it == Constants.RETENTION_PERIOD_ATTRIBUTE_NAME) ? Constants.ATTRIBUTE6_COLUMN : Constants.ATTRIBUTE5_COLUMN
    api.local[it] = (it == Constants.PRICELIST_REGION_ATTRIBUTE_NAME || it == Constants.CURRENCY_ATTRIBUTE_NAME) ? Lib.cachePriceRegionAttributes(it)?.collectEntries {
        [(it.key): it.value.attribute2]
    }?.sort {
        it.value
    } : libs.stdProductLib.RulesGenHelper.cacheCommonAttributes(it, false)?.collectEntries {
        [(it.key): it.value[attributeName]]
    }?.sort { it.value }
}
return