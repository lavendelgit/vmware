api.local.attributeRecords = []
api.local.productAttributes = []
api.local.pricingChannelAtrributes = []
api.local.pricingMasterAtrributes = []
Map pxProductParams = [:]
String mappedConfigAttribute
List validConfigAttributes = out.ConfigTabAttribute?.keySet() as List
for (configAttribute in validConfigAttributes) {
    mappedConfigAttribute = Lib.getStandardProductAttributeName(configAttribute)
    if (mappedConfigAttribute == Constants.CURRENCY || mappedConfigAttribute == Constants.PRICELIST_REGION_ATTRIBUTE_NAME) {
        attributeValues = mappedConfigAttribute == Constants.CURRENCY ? api.local.currency : api.local.priceListRegion
    } else {
        attributeValues = api.local.validStandardProductCache?.values()?.getAt(mappedConfigAttribute)?.unique()
    }
    attributeValues?.each { attributeValue ->
        pxProductParams = [:]
        pxProductParams.sku = out.SAPBaseSKU
        pxProductParams.attribute = out.ConfigTabAttribute[configAttribute]?.SAPName
        pxProductParams.value = attributeValue
        pxProductParams.sequence = api.local[configAttribute]?.getAt(attributeValue)
        pxProductParams.type = out.ConfigTabAttribute[configAttribute]?.Type
        pxProductParams.Product = api.local.validStandardProductCache?.values()?.getAt(Constants.PRODUCT_COLUMN)?.find()
        pxProductParams.Offer = api.local.validStandardProductCache?.values()?.getAt(Constants.OFFER_COLUMN)?.find()
        pxProductParams.Base = api.local.validStandardProductCache?.values()?.getAt(Constants.BASE_COLUMN)?.find()
        api.local.productAttributes << pxProductParams
    }
}
String product = api.local.validStandardProductCache?.values()?.getAt(Constants.PRODUCT_COLUMN)?.find()
String offer = api.local.validStandardProductCache?.values()?.getAt(Constants.OFFER_COLUMN)?.find()
String base = api.local.validStandardProductCache?.values()?.getAt(Constants.BASE_COLUMN)?.find()
String sapBaseSKU = out.SAPBaseSKU
Map parameter = [
        Product   : product,
        Offer     : offer,
        Base      : base,
        SapBaseSKU: sapBaseSKU
]
out.RegionCache?.each { region ->
    if (region.attribute3 == Constants.YES) {
        parameter.startDate = region.attribute4
        parameter.endDate = region.attribute5
        parameter.attributeType = Constants.RTM_CHANNEL
        api.local.pricingChannelAtrributes << populatePricingAttributes(region, parameter)
    }
    if (region.attribute8 == Constants.YES) {
        parameter.startDate = region.attribute6
        parameter.endDate = region.attribute7
        parameter.attributeType = Constants.RTM_DIRECT
        api.local.pricingMasterAtrributes << populatePricingAttributes(region, parameter)
    }
}
api.local.attributeRecords = api.local.productAttributes + api.local.pricingChannelAtrributes + api.local.pricingMasterAtrributes
return

Map populatePricingAttributes(region, parameter) {
    pxPricingParams = [:]
    pxPricingParams.sku = parameter.SapBaseSKU
    pxPricingParams.attribute = parameter.attributeType
    pxPricingParams.value = region.name
    pxPricingParams.sequence = region.attribute2
    pxPricingParams.type = Constants.PRICING
    pxPricingParams.startDate = parameter.startDate
    pxPricingParams.endDate = parameter.endDate
    pxPricingParams.Product = parameter.Product
    pxPricingParams.Offer = parameter.Offer
    pxPricingParams.Base = parameter.Base
    return pxPricingParams
}