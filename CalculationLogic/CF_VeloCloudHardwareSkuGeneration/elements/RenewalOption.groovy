api.local.renewalOptionMap = [:]
def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwareRenewalOptions").id)
]

def renewalList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), null, ["key1", "key2"], *filter)

api.local.renewal = renewalList.collect { it.key2 }.unique()

renewalList.each { obj ->
    if (api.local.renewalOptionMap[obj.key1]) {
        api.local.renewalOptionMap[obj.key1] << obj.key2
    } else {
        api.local.renewalOptionMap[obj.key1] = [obj.key2]
    }
}

return