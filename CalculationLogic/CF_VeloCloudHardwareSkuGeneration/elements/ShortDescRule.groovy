def servicefilter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwareShortDesc").id),
        Filter.equal("key1", "Extended Replacement Service")
]

api.local.serviceShortDesc = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6"], *servicefilter)

def rentalfilter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwareShortDesc").id),
        Filter.equal("key1", "Rental")
]

api.local.rentalShortDesc = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6"], *rentalfilter)


def capexfilter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwareShortDesc").id),
        Filter.equal("key1", "Hardware Capex")
]

api.local.capexShortDesc = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6"], *capexfilter)

return