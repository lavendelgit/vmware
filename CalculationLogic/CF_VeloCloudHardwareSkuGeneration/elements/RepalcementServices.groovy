def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwareReplacementServiceFactor").id)
]
def replacementServiceList = api.find("MLTV3", 0, api.getMaxFindResultsLimit(), null, ["key1"], true, *filter)

api.local.replacementServices = replacementServiceList.collect { it.key1 }

return