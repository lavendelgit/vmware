def itemMap = api.local.itemMap
def modelList = api.local.modelList
def locationMap = api.local.locationMap
def locationList = api.local.location
def softwareList = api.local.softwareList
def softwareMap = api.local.softwareMap
def subscriptionMap = api.local.subscriptionMap
def subscriptionList = api.local.subscription
def payFrequencyMap = api.local.payFrequencyMap
def payFrequencyList = api.local.payFrequency
def replacementServicesList = api.local.replacementServices
def licenseList = api.local.license
def renewalOptionMap = api.local.renewalOptionMap
def renewalList = api.local.renewal
def subscriptionYearsMap = api.local.subscriptionYearsMap


def serviceShortDescRuleList = api.local.serviceShortDesc
def rentalShortDescRuleList = api.local.rentalShortDesc
def capexShortDescRuleList = api.local.capexShortDesc

def capexLongDescRuleList = api.local.capexLongDesc
def rentalLongDescRuleList = api.local.rentalLongDesc
def serviceLongDescRuleList = api.local.serviceLongDesc
def paymentDescMap = api.local.paymentDescMap


def prefix
def sku
def shortDesc
def longDesc
def location
def software
def renewal
def subscriptionYears
def renewalNullSku
def renewalFlag
def serviceSKU
def serviceshortDesc
def servicelongDesc
def capexSKU
def capexshortDesc
def capexlongDesc
def rentalsku
def rentalshortDesc
def rentallongDesc
def paymentFreqDesc
def nonrentalshortDesc
def nonrentallongDesc

def itemList = itemMap.keySet()

itemList.each { itemType ->
    prefix = itemMap[itemType]

    modelList.each { edgeModel ->


        locationList.each { locationObj ->
            if (locationMap[edgeModel]) {
                location = (locationMap[edgeModel].contains(locationObj)) ? locationObj : null
            } else {
                location = null
            }

            softwareList.each { softwareObj ->
                if (softwareMap[itemType]) {
                    software = (softwareMap[itemType].contains(softwareObj)) ? softwareObj : null
                } else {
                    software = null
                }

                subscriptionList.each { subscriptionMonthsObj ->
                    if (subscriptionMap[itemType]) {
                        subscriptionMonths = (subscriptionMap[itemType]?.contains(subscriptionMonthsObj)) ? subscriptionMonthsObj : null
                    } else {
                        subscriptionMonths = null
                    }

                    payFrequencyList = payFrequencyMap[itemType]
                    payFrequencyList.each { paymentFrequency ->

                        replacementServicesList.each { replacementService ->

                            licenseList.each { licence ->

                                renewalList.each { renewalObj ->
                                    if (renewalOptionMap[itemType]) {
                                        renewal = (renewalOptionMap[itemType].contains(renewalObj)) ? renewalObj : null
                                    } else {
                                        renewal = null
                                    }

                                    subscriptionYears = subscriptionYearsMap[itemType + "~" + subscriptionMonths]
                                    paymentFreqDesc = paymentDescMap[paymentFrequency]
                                    //  api.trace("paymentFreqDesc","",paymentFreqDesc)
                                    switch (itemType) {
                                        case "Extended Replacement Service":
                                            if (subscriptionMonths) {
                                                serviceSKU = Library.createReplacementServiceSKU(prefix, edgeModel, location, replacementService, subscriptionMonths, paymentFrequency, licence)
                                                if (serviceSKU) {
                                                    serviceshortDesc = Library.createReplacementServiceShortDesc(serviceShortDescRuleList, edgeModel, subscriptionYears, subscriptionMonths, paymentFreqDesc, replacementService, location, licence)
                                                    servicelongDesc = Library.createReplacementServiceLongDesc(serviceLongDescRuleList, subscriptionYears, paymentFreqDesc, replacementService, location, licence, edgeModel)
                                                    Library.addOrUpdateHardwareSKU(serviceSKU, serviceshortDesc, servicelongDesc, itemType, edgeModel, location, software, replacementService, subscriptionMonths, subscriptionYears, licence, renewal, paymentFrequency)
                                                }
                                            }
                                            break

                                        case "Hardware Capex":
                                            capexSKU = Library.createCapexSKU(prefix, edgeModel, location, paymentFrequency, replacementService, licence)
                                            if (capexSKU) {
                                                capexshortDesc = Library.createCapexShortDesc(capexShortDescRuleList, edgeModel, location, subscriptionYears, software, licence)
                                                capexlongDesc = Library.createCapexLongDesc(capexLongDescRuleList, replacementService, edgeModel, location, licence)
                                                Library.addOrUpdateHardwareSKU(capexSKU, capexshortDesc, capexlongDesc, itemType, edgeModel, location, software, replacementService, subscriptionMonths, subscriptionYears, licence, renewal, paymentFrequency)
                                            }
                                            //api.trace("capexlongDesc","",capexlongDesc)

                                            break
                                        case "Rental":
                                            if (subscriptionMonths) {
                                                rentalsku = Library.createRentalSKU(prefix, edgeModel, location, subscriptionMonths, paymentFrequency, renewal, replacementService, licence)
                                                renewalFlag = null
                                                renewalNullSku = Library.createRentalSKU(prefix, edgeModel, location, subscriptionMonths, paymentFrequency, renewalFlag, replacementService, licence)
                                                if (rentalsku) {
                                                    rentalshortDesc = Library.createRentalShortDesc(rentalShortDescRuleList, edgeModel, subscriptionYears, subscriptionMonths, paymentFreqDesc, renewal, location, licence)
                                                    rentallongDesc = Library.createRentalLongDesc(rentalLongDescRuleList, edgeModel, subscriptionYears, paymentFreqDesc, replacementService, location, licence)
                                                    Library.addOrUpdateHardwareSKU(rentalsku, rentalshortDesc, rentallongDesc, itemType, edgeModel, location, software, replacementService, subscriptionMonths, subscriptionYears, licence, renewal, paymentFrequency)
                                                }
                                                if (renewalNullSku) {
                                                    nonrentalshortDesc = Library.createRentalShortDesc(rentalShortDescRuleList, edgeModel, subscriptionYears, subscriptionMonths, paymentFreqDesc, renewalFlag, location, licence)
                                                    nonrentallongDesc = Library.createRentalLongDesc(rentalLongDescRuleList, edgeModel, subscriptionYears, paymentFreqDesc, replacementService, location, licence)
                                                    Library.addOrUpdateHardwareSKU(renewalNullSku, nonrentalshortDesc, nonrentallongDesc, itemType, edgeModel, location, software, replacementService, subscriptionMonths, subscriptionYears, licence, renewalFlag, paymentFrequency)
                                                }
                                            }
                                            break
                                    }


                                }

                            }

                        }

                    }

                }

            }


        }

    }

}


return

