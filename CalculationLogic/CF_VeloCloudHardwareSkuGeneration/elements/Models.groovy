def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwareBasePrice").id)
]
api.local.modelList = api.find("MLTV", 0, api.getMaxFindResultsLimit(), "name",
        ["name"], true, *filter).collect { it.name }

return