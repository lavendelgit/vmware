api.local.locationMap = [:]
def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwareModelLocationMapping").id)
]
def locationList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2"], *filter)

locationList.each { obj ->
    if (api.local.locationMap[obj.key1]) {
        api.local.locationMap[obj.key1] << obj.key2
    } else {
        api.local.locationMap[obj.key1] = [obj.key2]
    }

}
api.local.location = locationList.collect { it.key2 }.unique()

return
