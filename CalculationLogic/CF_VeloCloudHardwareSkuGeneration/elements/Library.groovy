def addOrUpdateHardwareSKU(sku, shortDesc, longDesc, itemType, edgeModel, location, softwareRun, replacementService, paymentTermMonths, paymentTermYears, license, renewalOption, paymentFrequency) {
    def pxRecord
    switch (itemType) {
        case "Extended Replacement Service":
            pxRecord = [
                    name       : "VCHardwareProducts",
                    sku        : sku,
                    attribute1 : shortDesc,
                    attribute2 : longDesc,
                    attribute3 : edgeModel,
                    attribute4 : location,
                    attribute6 : itemType,
                    attribute7 : paymentTermMonths,
                    attribute8 : paymentTermYears,
                    attribute9 : paymentFrequency,
                    attribute10: replacementService,
                    attribute11: license
            ]
            break
        case "Hardware Capex":
            pxRecord = [
                    name       : "VCHardwareProducts",
                    sku        : sku,
                    attribute1 : shortDesc,
                    attribute2 : longDesc,
                    attribute3 : edgeModel,
                    attribute4 : location,
                    attribute5 : softwareRun,
                    attribute6 : itemType,
                    attribute7 : paymentTermMonths,
                    attribute8 : paymentTermYears,
                    attribute9 : paymentFrequency,
                    attribute10: replacementService,
                    attribute11: license

            ]
            break
        case "Rental":
            pxRecord = [
                    name       : "VCHardwareProducts",
                    sku        : sku,
                    attribute1 : shortDesc,
                    attribute2 : longDesc,
                    attribute3 : edgeModel,
                    attribute4 : location,
                    attribute6 : itemType,
                    attribute7 : paymentTermMonths,
                    attribute8 : paymentTermYears,
                    attribute9 : paymentFrequency,
                    attribute10: replacementService,
                    attribute11: license,
                    attribute12: renewalOption
            ]
            break
    }

    if (pxRecord) api.addOrUpdate("PX20", pxRecord)
}

def createCapexSKU(prefix, edgeModel, location, paymentFrequency, replacementService, licence) {

    prefix += edgeModel
    if (location) {
        prefix += "-"
        prefix += location
    }
    if (paymentFrequency) {
        prefix += "-"
        prefix += paymentFrequency
    }
    prefix += "-"
    prefix += replacementService
    prefix += "-"
    prefix += licence
    return prefix

}

def createReplacementServiceSKU(prefix, edgeModel, location, replacementService, subscriptionMonths, paymentFrequency, licence) {
    prefix += edgeModel
    if (location) {
        prefix += "-"
        prefix += location
    }
    prefix += "-"
    prefix += "EX"
    //prefix += "-"
    prefix += replacementService
    prefix += "-"
    prefix += subscriptionMonths
    prefix += paymentFrequency
    prefix += "-"
    prefix += licence


    return prefix
}

def createRentalSKU(prefix, edgeModel, location, subscriptionMonths, paymentFrequency, renewal, replacementService, licence) {
    prefix += edgeModel

    if (location) {
        prefix += "-"
        prefix += location
    }
    prefix += "-"
    prefix += subscriptionMonths
    prefix += paymentFrequency
    if (renewal) {
        prefix += "-"
        prefix += renewal
    }
    prefix += "-"
    prefix += replacementService
    prefix += "-"
    prefix += licence

    return prefix
}

def createCapexShortDesc(List ruleList, String edgeModel, String location, String subscriptionYears, String softwareRun, String license) {
    String shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break
            case "Replace":
                if (rule.attribute2.equals("EdgeModel")) {
                    if (shortDesc) {
                        shortDesc += edgeModel
                    } else {
                        shortDesc = edgeModel
                    }
                }
                if (rule.attribute2.equals("SubscriptionYears") && subscriptionYears) {
                    if (shortDesc) {
                        shortDesc += subscriptionYears
                    } else {
                        shortDesc = subscriptionYears
                    }
                }
                break
            case "Conditional":
                if (rule.attribute3.equals("SoftwareRun")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, softwareRun)
                }
                if (rule.attribute3.equals("License")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, license)
                }
                if (rule.attribute3.equals("Location")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, location)
                }
                if (rule.attribute3.equals("EdgeModel")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, edgeModel)
                }
                break

        }
    }
    return shortDesc
}

def conditionalShortDesc(shortDesc, operator, value, desc, conditionName) {
    //api.trace("long desc","",shortDesc + "~"+operator+"~"+value+"~"+desc+"~"+conditionName)
    switch (operator) {
        case "=":

            if (conditionName.equals(value)) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<=":
            if (conditionName <= value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case ">":
            if (conditionName > value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<>":
            if (conditionName != value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break

    }
    return shortDesc
}

def createReplacementServiceShortDesc(List ruleList, String edgeModel, String subscriptionYears, String subscriptionMonths, String paymentFreqDesc, String replacementService, String location, String license) {
    String shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                //shortDesc.append(rule.attribute6)
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break
            case "Replace":
                if (rule.attribute2.equals("EdgeModel")) {
                    if (shortDesc) {
                        shortDesc += edgeModel
                    } else {
                        shortDesc = edgeModel
                    }
                }
                if (rule.attribute2.equals("SubscriptionYears")) {
                    if (shortDesc) {
                        shortDesc += subscriptionYears
                    } else {
                        shortDesc = subscriptionYears
                    }
                }
                if (rule.attribute2.equals("SubscriptionMonths")) {
                    if (shortDesc) {
                        shortDesc += subscriptionMonths
                    } else {
                        shortDesc = subscriptionMonths
                    }
                }
                if (rule.attribute2.equals("PaymentFreqDesc")) {
                    if (shortDesc) {
                        shortDesc += paymentFreqDesc
                    } else {
                        shortDesc = paymentFreqDesc
                    }
                }
                break
            case "Conditional":
                if (rule.attribute3.equals("ReplacementService")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, replacementService)
                }
                if (rule.attribute3.equals("License")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, license)
                }
                if (rule.attribute3.equals("Location")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, location)
                }
                if (rule.attribute3.equals("EdgeModel")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, edgeModel)
                }
                break

        }
    }
    return shortDesc
}

def createRentalShortDesc(List ruleList, String edgeModel, String subscriptionYears, String subscriptionMonths, String paymentFreqDesc, String renewalOption, String location, String license) {
    String shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                //shortDesc.append(rule.attribute6)
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break
            case "Replace":
                if (rule.attribute2.equals("EdgeModel")) {
                    if (shortDesc) {
                        shortDesc += edgeModel
                    } else {
                        shortDesc = edgeModel
                    }
                }
                if (rule.attribute2.equals("SubscriptionYears")) {
                    if (shortDesc) {
                        shortDesc += subscriptionYears
                    } else {
                        shortDesc = subscriptionYears
                    }
                }
                if (rule.attribute2.equals("SubscriptionMonths")) {
                    if (shortDesc) {
                        shortDesc += subscriptionMonths
                    } else {
                        shortDesc = subscriptionMonths
                    }
                }
                if (rule.attribute2.equals("PaymentFreqDesc")) {
                    if (shortDesc) {
                        shortDesc += paymentFreqDesc
                    } else {
                        shortDesc = paymentFreqDesc
                    }
                }
                break
            case "Conditional":
                if (rule.attribute3.equals("RenewalOption")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, renewalOption)
                }
                if (rule.attribute3.equals("License")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, license)
                }
                if (rule.attribute3.equals("Location")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, location)
                }
                if (rule.attribute3.equals("EdgeModel")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, edgeModel)
                }
                break

        }
    }
    return shortDesc
}

def createCapexLongDesc(List ruleList, String replacementService, String edgeModel, String location, String license) {
    String shortDesc

    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                //shortDesc.append(rule.attribute6)
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break
            case "Replace":
                break
            case "Conditional":
                if (rule.attribute3.equals("ReplacementService")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, replacementService)
                }
                if (rule.attribute3.equals("Location")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, location)
                }
                if (rule.attribute3.equals("License")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, license)
                }
                if (rule.attribute3.equals("EdgeModel")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, edgeModel)
                }
                break

        }
    }
    return shortDesc
}


def createReplacementServiceLongDesc(List ruleList, String subscriptionYears, String paymentFreqDesc, String replacementService, String location, String license, String edgeModel) {
    String shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                //shortDesc.append(rule.attribute6)
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break
            case "Replace":
                if (rule.attribute2.equals("SubscriptionYears")) {
                    if (shortDesc) {
                        shortDesc += subscriptionYears
                    } else {
                        shortDesc = subscriptionYears
                    }
                }
                if (rule.attribute2.equals("PaymentFreqDesc")) {
                    if (shortDesc) {
                        shortDesc += paymentFreqDesc
                    } else {
                        shortDesc = paymentFreqDesc
                    }
                }
                if (rule.attribute2.equals("EdgeModel")) {
                    if (shortDesc) {
                        shortDesc += edgeModel + " "
                    } else {
                        shortDesc = edgeModel + " "
                    }
                }
                break
            case "Conditional":
                if (rule.attribute3.equals("ReplacementService")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, replacementService)
                }
                if (rule.attribute3.equals("Location")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, location)
                }
                if (rule.attribute3.equals("License")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, license)
                }
                if (rule.attribute3.equals("EdgeModel")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, edgeModel)
                }
                break

        }
    }
    return shortDesc
}

def createRentalLongDesc(List ruleList, String edgeModel, String subscriptionYears, String paymentFreqDesc, String replacementService, String location, String license) {
    String shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                //shortDesc.append(rule.attribute6)
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break
            case "Replace":
                if (rule.attribute2.equals("EdgeModel")) {
                    if (shortDesc) {
                        shortDesc += edgeModel
                    } else {
                        shortDesc = edgeModel
                    }
                }
                if (rule.attribute2.equals("SubscriptionYears")) {
                    if (shortDesc) {
                        shortDesc += subscriptionYears
                    } else {
                        shortDesc = subscriptionYears
                    }
                }
                if (rule.attribute2.equals("PaymentFreqDesc")) {
                    if (shortDesc) {
                        shortDesc += paymentFreqDesc
                    } else {
                        shortDesc = paymentFreqDesc
                    }
                }
                break
            case "Conditional":
                if (rule.attribute3.equals("ReplacementService")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, replacementService)
                }
                if (rule.attribute3.equals("Location")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, location)
                }
                if (rule.attribute3.equals("License")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, license)
                }
                if (rule.attribute3.equals("EdgeModel")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, edgeModel)
                }
                break

        }
    }
    return shortDesc
}