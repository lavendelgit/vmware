api.local.subscriptionMap = [:]
api.local.subscriptionYearsMap = [:]

def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwareSubscriptionTerm").id)
]

def subscriptionList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), null, ["key1", "key2", "attribute1"], *filter)

api.local.subscription = subscriptionList.collect { it.key2 }.unique()

subscriptionList.each { obj ->
    if (api.local.subscriptionMap[obj.key1]) {
        api.local.subscriptionMap[obj.key1] << obj.key2
    } else {
        api.local.subscriptionMap[obj.key1] = [obj.key2]
    }

}

api.local.subscriptionYearsMap = subscriptionList.collectEntries {
    [(it.key1 + "~" + it.key2): it.attribute1]
}

return