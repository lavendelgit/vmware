def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCItemTypes").id),
        Filter.equal("key1", "Hardware")
]
def itemList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1"], *filter)

api.local.itemMap = itemList.collectEntries {
    [(it.key2): it.attribute1]
}

return