api.local.softwareMap = [:]
def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwareSoftwareRun").id)
]
def softwareList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2"], *filter)

softwareList.each { obj ->
    if (api.local.softwareMap[obj.key1]) {
        api.local.softwareMap[obj.key1] << obj.key2
    } else {
        api.local.softwareMap[obj.key1] = [obj.key2]
    }


}
api.local.softwareList = softwareList.collect { it.key2 }.unique()

return