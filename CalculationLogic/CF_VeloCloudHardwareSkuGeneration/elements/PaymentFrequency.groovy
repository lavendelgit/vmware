api.local.payFrequencyMap = [:]
def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCHardwarePaymentFrequency").id)
]

def paymentList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), null, ["key1", "key2"], *filter)

api.local.payFrequency = paymentList.collect { it.key2 }.unique()

paymentList.each { obj ->
    if (api.local.payFrequencyMap[obj.key1]) {
        api.local.payFrequencyMap[obj.key1] << obj.key2
    } else {
        api.local.payFrequencyMap[obj.key1] = [obj.key2]
    }
}

def descfilter = [
        Filter.equal("lookupTable.id", api.findLookupTable("PaymentFrequency").id)
]

def paymentDescList = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, ["name", "value"], *descfilter)
api.local.paymentDescMap = paymentDescList.collectEntries {
    [(it.value): it.name]
}

return