def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BULicenseClass").id),
        Filter.equal("key1", "VC")
]
def licenseList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), null, ["key1", "key2"], true, *filter)

api.local.license = licenseList.collect { it.key2 }

return