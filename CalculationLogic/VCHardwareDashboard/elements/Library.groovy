def fetchEdgeModelList() {
    def table = api.findLookupTable("VCHardwareBasePrice")
    filter = [Filter.equal("lookupTable.id", table.id)]
    ppList = api.find("MLTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}

def fetchReplacementServiceList() {
    def table = api.findLookupTable("VCHardwareReplacementServiceFactor")
    filter = [Filter.equal("lookupTable.id", table.id)]
    ppList = api.find("MLTV3", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.key1 }
    return ppList.unique()
}

def fetchLicenseList() {
    def table = api.findLookupTable("BULicenseClass")
    filter = [
            Filter.equal("lookupTable.id", table.id),
            Filter.equal("key1", "VC")
    ]
    ppList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.key2 }
    return ppList.unique()
}

def fetchOfferingList() {
    def table = api.findLookupTable("VCItemTypes")
    filter = [
            Filter.equal("lookupTable.id", table.id),
            Filter.equal("key1", "Hardware")
    ]
    ppList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.key2 }
    return ppList.unique()
}

def fetchPaymentTermList() {
    def table = api.findLookupTable("VCHardwareSubscriptionTerm")
    filter = [
            Filter.equal("lookupTable.id", table.id)
    ]
    ppList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.attribute1 }
    return ppList.unique()
}

def fetchPaymentTYpeList() {
    def table = api.findLookupTable("PaymentFrequency")
    filter = [
            Filter.equal("lookupTable.id", table.id)
    ]
    ppList = api.find("LTV", 0, api.getMaxFindResultsLimit(), null, *filter).collect { it.name }
    return ppList.unique()
}