if (api.isSyntaxCheck()) api.abortCalculation()

def paymentTypeList = []
def edgeModelList = out.EdgeModel
def replacementServiceList = out.ReplacementService
def licenceList = out.LicenseClass
def offeringList = out.Offering
def termList = out.PaymentTerm
def typeList = out.PaymentType
def payDescMap = api.local.payFreqMap

typeList.each { obj ->
    if (payDescMap[obj]) {
        paymentTypeList << payDescMap[obj]
    }
}


paymentTypeList = (paymentTypeList) ?: null
edgeModelList = (edgeModelList) ?: null
replacementServiceList = (replacementServiceList) ?: null
licenceList = (licenceList) ?: null
offeringList = (offeringList) ?: null
termList = (termList) ?: null


filter = [
        Filter.in("name", "VCHardwareProducts"),
        Filter.in("attribute3", edgeModelList),
        Filter.in("attribute10", replacementServiceList),
        Filter.in("attribute11", licenceList),
        Filter.in("attribute6", offeringList),
        Filter.in("attribute8", termList),
        Filter.in("attribute9", paymentTypeList)

]
recFullList = api.stream("PX20", "sku", *filter)

def resultMatrix = api.newMatrix("SKU Number", "SKU Short Description", "SKU Long Description", "Offering",
        "Edge Model", "Payment Term", "Payment Type", "Replacement Service", "License Class", "Location", "Software")

recFullList.each {
    def row = [:]
    row.put("SKU Number", it.sku)
    row.put("SKU Short Description", it.attribute1)
    row.put("SKU Long Description", it.attribute2)
    row.put("Offering", it.attribute6)
    row.put("Edge Model", it.attribute3)
    row.put("Payment Term", it.attribute8)
    row.put("Payment Type", it.attribute9)
    row.put("Replacement Service", it.attribute10)
    row.put("License Class", it.attribute11)
    row.put("Location", it.attribute4)
    row.put("Software", it.attribute5)


    resultMatrix.addRow(row)
}

resultMatrix?.setEnableClientFilter(true)
return resultMatrix