def source = api.getDatamartRowSet('source')
Map currentRow = source?.getCurrentRow()
Map previousRow
if (source?.previous()) {
    previousRow = source?.getCurrentRow()
}
if ((previousRow && (currentRow?.PartNumber == previousRow?.PartNumber) && (currentRow?.Currency == previousRow?.Currency) && (currentRow?.Region == previousRow?.Region))) {
    return previousRow?.PriceChangeEffectiveDate - 1
} else {
    return api.parseDate("yyyy-MM-dd", api.global.endDateCache)
}