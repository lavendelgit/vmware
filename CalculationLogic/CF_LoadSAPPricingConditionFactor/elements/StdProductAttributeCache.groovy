List attributes = Constants.FACTOR_ATTRIBUTES_MAPPING?.keySet() as List
for (attribute in attributes) {
    adjustments = Lib.cacheAndPopulateTermUoMAdjustments(Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorValueAttribute)
    if (adjustments) {
        api.local[attribute] = adjustments
    }
}
