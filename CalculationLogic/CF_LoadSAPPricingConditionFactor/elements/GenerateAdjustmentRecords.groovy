String product
String offer
Map pxRecord = [:]
api.local.skus = []
List loadedSAPSKUdata = []
api.local.adjustmentRecords = []
api.local.updateAdjustmentsRecords = []
List attributes = Constants.FACTOR_ATTRIBUTES_MAPPING?.keySet() as List
for (productSku in api.local.stdProductsCache) {
    if (!loadedSAPSKUdata?.contains(productSku?.attribute29)) {
        uniqueAttributes = api.local.stdProductsCache?.findAll { it.attribute29 == productSku?.attribute29 }
        validConfigurationCache = out.CacheValidConfiguration?.values()?.findAll { it.Product == productSku?.attribute1 }
        validAttributes = validConfigurationCache?.findAll { it.SAPSKU == productSku.attribute29 } ?: out.CacheValidConfiguration?.values()?.findAll { it.SAPSKU == "*" }
        baseSkuDetails = [
                ProductGroup: productSku.attribute39,
                Product     : productSku.attribute1,
                Offer       : productSku.attribute2,
                Base        : productSku.attribute4
        ]
        stdAdjustmentSkus = api.local.stdAdjustmentsCache?.findAll {
            ((it.attribute50 == baseSkuDetails.ProductGroup || it.attribute50 == "*") && (it.attribute1 == baseSkuDetails.Product || it.attribute1 == "*") && (it.attribute2 == baseSkuDetails.Offer || it.attribute2 == "*") &&
                    (it.attribute3 == baseSkuDetails.Base || it.attribute3 == "*"))
        }?.sort { standard, priority ->
            if (priority.attribute2 == "*") {
                priority.attribute3 <=> standard.attribute3
            } else {
                priority.attribute2 <=> standard.attribute2
            }
        }
        for (attribute in validAttributes?.Attribute?.unique()) {
            attributeValue = Lib.getAttributeValues(attribute, uniqueAttributes)
            pxRecord = [:]
            pxRecord.SAPSKU = productSku?.attribute29
            attributeKey = attribute.replaceAll("\\s", "")
            attributeRecords = stdAdjustmentSkus?.findAll { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorValueAttribute] != null }
            factorAttibute = Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute
            switch (attribute) {
                case Constants.PAYMENT_TYPE:
                    uniqueRecords = attributeRecords.unique(false) { first, second ->
                        first[factorAttibute] <=> second[factorAttibute] ?:
                                first["attribute7"] <=> second["attribute7"]
                    }
                    break

                case Constants.TERM:
                    uniqueRecords = attributeRecords.unique(false) { first, second ->
                        first[factorAttibute] <=> second[factorAttibute] ?: first.attribute33 <=> second.attribute33
                    }
                    break

                case Constants.ADJUSTMENT_REGION:
                    uniqueRecords = attributeRecords.unique(false) { first, second ->
                        first[factorAttibute] <=> second[factorAttibute] ?:
                                first["attribute23"] <=> second["attribute23"]
                    }
                    break

                case Constants.PROGRAM_OPTION:
                    uniqueRecords = attributeRecords.unique(false) { first, second ->
                        first[factorAttibute] <=> second[factorAttibute] ?:
                                first["attribute6"] <=> second["attribute6"]
                    }
                    break
                case Constants.RETENTION_PERIOD:
                    uniqueRecords = attributeRecords.unique(false) { first, second ->
                        first[factorAttibute] <=> second[factorAttibute] ?:
                                first.attribute44 <=> second.attribute44
                    }
                    break
                case Constants.VOLUME_TIER_SIZE:
                    vtgaConfig = out.CacheVTGAConfiguration[productSku?.attribute29]
                    uniqueRecords = sortVolumeTierAdjustment(attributeRecords, vtgaConfig)
                    break

                default:
                    uniqueRecords = attributeRecords.unique(false) { first, second ->
                        first[factorAttibute] <=> second[factorAttibute]
                    }
            }
            latestRecords = uniqueRecords?.sort { first, second -> second.attribute41 <=> first.attribute41 }?.unique()?.findAll { it != null }
            populateAdjustmentFactor(latestRecords, productSku, attribute, attributeValue, baseSkuDetails, validAttributes, pxRecord, uniqueAttributes)
        }
        loadedSAPSKUdata << productSku?.attribute29
    }
}
return


void populateAdjustmentFactor(List latestRecords, Map productSku, String attribute, List attributeValue, Map baseSkuDetails, List validAttributes, Map pxRecord, List uniqueAttributes) {
    for (uniqueRecord in latestRecords) {
        pxRecord.Product = uniqueRecord?.attribute1
        pxRecord.Offer = uniqueRecord?.attribute2
        pxRecord.Base = uniqueRecord?.attribute3
        String termPayment
        String exceptionValue
        skuAttribute = [
                ProductGroup: productSku.attribute39,
                Product     : productSku.attribute1,
                Offer       : productSku.attribute2,
                Base        : productSku.attribute4,
        ]
        skuAttribute[attributeKey] = uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute]
        if (Lib.validateAdjustmentValues(attribute, attributeValue, skuAttribute[attributeKey], uniqueRecord, uniqueAttributes)) {
            switch (attribute) {
                case Constants.TERM:
                    skuAttribute[Constants.TERM_UOM] = uniqueRecord.attribute33
                    exceptionValue = out.CacheTermAttributes[uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute]]
                    break

                case Constants.PAYMENT_TYPE:
                    skuAttribute[Constants.TERM_UOM] = uniqueRecord.attribute33
                    skuAttribute[Constants.TERM] = uniqueRecord.attribute7
                    termPayment = skuAttribute[Constants.TERM_UOM] ? Constants.PAYMENT_TYPE_TERM : null
                    exceptionValue = skuAttribute[Constants.TERM_UOM] ? uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] + "|" + out.CacheTermAttributes[uniqueRecord.attribute7] : null
                    break

                case Constants.CURRENCY:
                    skuAttribute[Constants.ATTRIBUTE_PRICE_LIST_REGION] = uniqueRecord.attribute4
                    break

                case Constants.ADJUSTMENT_REGION:
                    exceptionValue = ((uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute])?.split(' ') as List)?.join('|')
                    skuAttribute[Constants.CURRENCY] = uniqueRecord.attribute23 ?: ""
                    break

                case Constants.RETENTION_PERIOD:
                    skuAttribute[Constants.RETENTION_PERIOD_UOM] = uniqueRecord.attribute44
                    exceptionValue = out.CacheSAPAttributes[skuAttribute[Constants.RETENTION_PERIOD_UOM]] ? uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] + " " + out.CacheSAPAttributes[skuAttribute[Constants.RETENTION_PERIOD_UOM]] : uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] + " " + uniqueRecord.attribute44
                    break
                case Constants.PROGRAM_OPTION:
                    skuAttribute[Constants.METRIC] = uniqueRecord.attribute6
                    exceptionValue = uniqueRecord.attribute39 && uniqueRecord.attribute6 ? uniqueRecord.attribute39 + "|" + uniqueRecord.attribute6 : uniqueRecord.attribute39
                    break
                case Constants.VOLUME_TIER_SIZE:
                    api.local.vtgaAttributeMapping?.each {
                        skuAttribute[(out.StdAdjustmentMetaData[it])?.replaceAll("\\s", "")] = uniqueRecord[it]
                    }
                    volumeTierValue = skuAttribute["VolumeTierSize"]
                    skuAttribute?.remove("VolumeTierSize")
                    skuAttribute["VolumeTierSize"] = volumeTierValue
                    if (skuAttribute.Term) {
                        exceptionMap = skuAttribute?.drop(3)
                        exceptionMap.Term = out.CacheTermAttributes[exceptionMap.Term]
                    } else {
                        exceptionMap = skuAttribute?.drop(3)
                    }
                    exceptionValue = exceptionMap?.values()?.grep()?.join('|')
            }
            adjustment = Lib.getAdjustmentsForAttribute(api.local[attribute], baseSkuDetails, skuAttribute)
            pxRecord.AttributeFactor = adjustment
            pxRecord.AttributeName = validAttributes?.find { it.Attribute == attribute }?.SAPAttribute
            pxRecord.AttributeValue = exceptionValue ?: uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute]
            pxRecord.FactorMeasure = adjustment ? adjustment : null
            pxRecord.ValidFrom = stdAdjustmentSkus?.findAll { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.last()?.attribute41
            pxRecord.ValidTo = stdAdjustmentSkus?.findAll { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.last()?.attribute42
            product = latestRecords?.find { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.attribute1
            offer = latestRecords?.find { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.attribute2
            base = latestRecords?.find { it[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] == uniqueRecord[Constants.FACTOR_ATTRIBUTES_MAPPING[attribute]?.FactorAttribute] }?.attribute3

            if (product) {
                pxRecord.SAPProductLevel = out.CacheProductHierarchyMapping?.getAt(product + "_" + Constants.PRODUCT)?.SAPName
                pxRecord.ProdHierarchy = out.CacheProductHierarchyMapping?.getAt(product + "_" + Constants.PRODUCT)?.SAPAttName
            }
            if (offer && offer != "*") {
                pxRecord.SAPProductLevel = out.CacheProductHierarchyMapping?.getAt(offer + "_" + Constants.OFFER)?.SAPName
                pxRecord.ProdHierarchy = out.CacheProductHierarchyMapping?.getAt(product + "_" + Constants.OFFER)?.SAPAttName
            }
            if (base && base != "*") {
                pxRecord.SAPProductLevel = out.CacheProductHierarchyMapping?.getAt(base + "_" + Constants.BASE)?.SAPName
                pxRecord.ProdHierarchy = out.CacheProductHierarchyMapping?.getAt(base + "_" + Constants.BASE)?.SAPAttName
            }
            pxRecord.sku = populateSKU(productSku?.attribute29, attribute, pxRecord, uniqueRecord.attribute33, uniqueRecord.attribute23, uniqueRecord.attribute44, skuAttribute)
            if (!api.local.skus?.contains(pxRecord.sku)) {

                if (adjustment && !api.local.sapPricingFactorCache?.sku?.contains(pxRecord.sku) && pxRecord.ValidFrom <= pxRecord.ValidTo) {
                    api.local.adjustmentRecords << Lib.insertRecords(pxRecord)
                } else if (adjustment && api.local.sapPricingFactorCache?.sku?.contains(pxRecord.sku) && !api.local.sapPricingFactorCache?.findAll { it.sku == pxRecord.sku }?.attribute7?.contains(adjustment)) {
                    if (api.local.sapPricingFactorCache?.findAll { it.sku == pxRecord.sku }?.getAt(0)?.attribute9 == pxRecord.ValidFrom) {
                        api.local.updateAdjustmentsRecords << Lib.populateUpdatePayload(pxRecord, Constants.UPDATE_FACTOR)
                    } else if (api.local.sapPricingFactorCache?.findAll { it.sku == pxRecord.sku }?.getAt(0)?.attribute9 != pxRecord.ValidFrom && pxRecord.ValidFrom <= pxRecord.ValidTo) {
                        api.local.adjustmentRecords << Lib.insertRecords(pxRecord)
                        api.local.updateAdjustmentsRecords << Lib.populateUpdatePayload(pxRecord, Constants.UPDATE_DATE)
                    }
                }
                api.local.skus << pxRecord.sku
            }
        }
    }
}

String populateSKU(String sapSKU, String attribute, Map pxRecord, String termUoM, String currency, String retentionPeriodUoM, Map skuAttribute) {
    String sku = ""
    if (attribute == Constants.TERM || attribute == Constants.PAYMENT_TYPE) {
        sku += termUoM ? libs.vmwareUtil.util.frameKey([pxRecord.Product, pxRecord.Offer, pxRecord.Base, pxRecord.AttributeValue, termUoM]) : libs.vmwareUtil.util.frameKey([pxRecord.Product, pxRecord.Offer, pxRecord.Base, pxRecord.AttributeValue])
        return sku
    } else if (attribute == Constants.ADJUSTMENT_REGION) {
        sku += currency ? libs.vmwareUtil.util.frameKey([pxRecord.Product, pxRecord.Offer, pxRecord.Base, pxRecord.AttributeValue, currency]) : libs.vmwareUtil.util.frameKey([pxRecord.Product, pxRecord.Offer, pxRecord.Base, pxRecord.AttributeValue])
        return sku
    } else if (attribute == Constants.RETENTION_PERIOD) {
        sku += retentionPeriodUoM ? libs.vmwareUtil.util.frameKey([pxRecord.Product, pxRecord.Offer, pxRecord.Base, pxRecord.AttributeValue, retentionPeriodUoM]) : libs.vmwareUtil.util.frameKey([pxRecord.Product, pxRecord.Offer, pxRecord.Base, pxRecord.AttributeValue])
        return sku
    } else if (attribute == Constants.VOLUME_TIER_SIZE) {
        sku += libs.vmwareUtil.util.frameKey([pxRecord.Product, pxRecord.Offer, pxRecord.Base, pxRecord.AttributeValue])
        return sku
    } else {
        sku += libs.vmwareUtil.util.frameKey([pxRecord.Product, pxRecord.Offer, pxRecord.Base, pxRecord.AttributeValue])
        return sku
    }
}

List sortVolumeTierAdjustment(List adjustments, List vtgaConfig) {
    localAdjustment = adjustments
    String factorAttibute = "attribute27"
    List attributeData = []
    Map adjustmentMetaData = out.StdAdjustmentMetaData?.collectEntries { [(it.value): it.key] }
    api.local.vtgaAttributeMapping = []
    uniqueRecords = []
    localUniqueRecords = []
    vtgaConfig?.each { record ->
        sortAttributes = []
        record?.each { it ->
            if (it.key?.contains("AttributeName") && it.value) {
                sortAttributes << adjustmentMetaData[out.ValidCombinationAttributeCache[it.value]]
                api.local.vtgaAttributeMapping << adjustmentMetaData[out.ValidCombinationAttributeCache[it.value]]
            }
        }
        sortAttributes?.each { it ->
            adjustments = localAdjustment?.findAll { attribute ->
                attribute[it] != null
            }
            attributeData = adjustments[it]?.grep()
            if (attributeData?.getAt(0) != null) {
                uniqueRecords = adjustments.unique(false) { first, second ->
                    first[factorAttibute] <=> second[factorAttibute] ?:
                            first[it] <=> second[it]
                }
            }
        }
        for (uRecord in uniqueRecords) {
            localUniqueRecords << uRecord
        }
    }
    /*api.local.vtgaAttributeMapping?.unique()?.each{it->

        if(adjustments[it]?.grep()?.getAt(0)!=null){
            uniqueRecords = adjustments.unique(false) { first, second ->
                first[factorAttibute] <=> second[factorAttibute] ?:
                        first[it] <=> second[it]
            }
        }
    }*/
    return localUniqueRecords
}
