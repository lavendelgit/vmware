List filter = [
        Filter.equal("name", Constants.STANDARD_PRODUCT_PX),
        Filter.equal("attribute24", Constants.STD_PRODUCT_TYPE),
        Filter.in("attribute29", out.SAPSKUCache)
]
recordStream = api.stream("PX50", "attribute29", *filter)
api.local.stdProductsCache = recordStream?.collect { it }
recordStream?.close()
return