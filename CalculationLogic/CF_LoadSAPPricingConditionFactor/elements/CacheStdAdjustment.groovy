List filter = [
        Filter.equal("name", Constants.STANDARD_ADJUSTMENT),
        Filter.in("attribute1", api.local.stdProductsCache?.attribute1?.unique()),
        Filter.or(Filter.in("attribute2", api.local.stdProductsCache?.attribute2?.unique()), Filter.equal("attribute2", "*")),
        Filter.or(Filter.in("attribute3", api.local.stdProductsCache?.attribute4?.unique()), Filter.equal("attribute3", "*")),
        //Filter.isNotNull("attribute7")
]
api.local.stdAdjustmentsCache = []
stdAdjustmentStream = api.stream("PX50", "sku,attribute41", *filter)
stdAdjustmentStream?.collect { row ->
    if (row.attribute41 <= row.attribute42) {
        api.local.stdAdjustmentsCache << row
    }
}
stdAdjustmentStream?.close()
return
