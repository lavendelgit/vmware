if (api.local.adjustmentRecords) {
    payload = [
            "data": [
                    "data"   : api.local.adjustmentRecords,
                    "header" : [
                            "name",
                            "sku",
                            "attribute1",
                            "attribute2",
                            "attribute3",
                            "attribute4",
                            "attribute5",
                            "attribute6",
                            "attribute7",
                            "attribute8",
                            "attribute9",
                            "attribute10",
                            "attribute15"
                    ],
                    "options": [
                            "detectJoinFields"    : false,
                            "maxJoinFieldsLengths": []
                    ]
            ]]
    api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
    return
}
