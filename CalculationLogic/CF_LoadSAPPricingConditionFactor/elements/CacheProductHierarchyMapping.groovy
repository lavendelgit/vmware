return api.findLookupTableValues(Constants.PRODUCT_HIERARCHY_MAPPING_PP)?.collectEntries {
    [(it.attribute1 + "_" + it.key1): ["PricefxAttName": it.attribute1, "SAPName": it.key2, "SAPAttName": it.attribute2]]
}