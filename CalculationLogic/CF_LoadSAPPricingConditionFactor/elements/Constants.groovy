import groovy.transform.Field

@Field final String SAP_PRICING_CONDITION_TABLE = "SAPPricingConditionFactor"
@Field final String STANDARD_ADJUSTMENT_TABLE = "StandardAdjustment"
@Field final String PRODUCT = "Product"
@Field final String OFFER = "Offer"
@Field final String BASE = "Base"
@Field final String PRICELISTREGION_TABLE = "PriceListRegion"
@Field final String STD_PRODUCT_TYPE = "Standalone"
@Field final String SAP_FILE_SKU_CONFIG_PP = "SAPFileSKUConfig"
@Field final String PRODUCT_HIERARCHY_MAPPING_PP = "ProductHierarchyMapping"
@Field final String STANDARD_PRODUCT_PX = "StandardProducts"
@Field final String STANDARD_ADJUSTMENT = "StandardAdjustment"
@Field final String VTGA_TABLE = "VolumeTierGroupAssignment"
@Field final List EXCLUDE_LIST = ["version", "typedId", "name", "sku", "createDate", "lastUpdateDate", "createdBy", "lastUpdateBy", "attribute1", "attribute2", "attribute3", "attribute33", "attribute41", "attribute42", "attribute25", "attribute26"]
@Field final String ADJUSTMENT = "Adjustment"
@Field final String FACTOR = "Factor"
@Field final String UPLIFT = "Uplift"
@Field final String FXRATE = "Fx Rate"
@Field final String CONTEXT_CONSTANT = "PRODUCT_ID/VARIANT"
@Field final Integer ADJUSTMENT_INDEX = 0
@Field final Integer WEIGHTAG_INDEX = 1
@Field final Integer COMBINATION_INDEX = 2
@Field final String TERM = "Term"
@Field final String PAYMENT_TYPE_TERM = "Payment Type|Term"
@Field final String TERM_UOM = "TermUoM"
@Field final String PAYMENT_TYPE = "Payment Type"
@Field final String PROGRAM_OPTION = "Program Option"
@Field final String VOLUME_TIER_SIZE = "Volume Tier Size"
@Field final String METRIC = "Metric"
@Field final String RETENTION_PERIOD = "Retention Period"
@Field final String RETENTION_PERIOD_UOM = "RetentionPeriodUOM"
@Field final String CURRENCY = "Currency"
@Field final String ADJUSTMENT_REGION = "Price List Region"
@Field final String ATTRIBUTE_PRICE_LIST_REGION = "PriceListRegion"
@Field final String PRICELISTREGION_PX = "Price List Region"
@Field final String MASTER_PRICELIST_REGION = "Master Launch Region"
@Field final String UPDATE_FACTOR = "Factor"
@Field final String UPDATE_DATE = "Date"
@Field final String COMMIT = "Commit"
@Field final String BOTH = "Both"
@Field final String OVERAGE = "Overage"
@Field final List FACTOR_ATTRIBUTES_NAME_LIST = [ADJUSTMENT, FACTOR, UPLIFT, FXRATE]
@Field final List VALID_DYNAMIC_KEYS_COLUMNS = ["attribute21", "attribute7"]
@Field final Map FACTOR_ATTRIBUTES_MAPPING = [
        "Currency"          : [
                FactorValueAttribute: "attribute24",
                FactorAttribute     : "attribute23"
        ],

        "Price List Region" : [
                FactorValueAttribute: "attribute34",
                FactorAttribute     : "attribute4",
        ],
        "Segment Type"      : [
                FactorValueAttribute: "attribute18",
                FactorAttribute     : "attribute10"
        ],
        "Term"              : [
                FactorValueAttribute: "attribute15",
                FactorAttribute     : "attribute7"
        ],
        "Retention Period"  : [
                FactorValueAttribute: "attribute45",
                FactorAttribute     : "attribute43"
        ],
        "Payment Type"      : [
                FactorValueAttribute: "attribute22",
                FactorAttribute     : "attribute21"
        ],
        "Data Center"       : [
                FactorValueAttribute: "attribute20",
                FactorAttribute     : "attribute12"
        ],
        "Metric"            : [
                FactorValueAttribute: "attribute14",
                FactorAttribute     : "attribute6"
        ],
        "Support Tier"      : [
                FactorValueAttribute: "attribute16",
                FactorAttribute     : "attribute8"
        ],
        "Support Type"      : [
                FactorValueAttribute: "attribute17",
                FactorAttribute     : "attribute9"
        ],
        "Purchasing Program": [
                FactorValueAttribute: "attribute19",
                FactorAttribute     : "attribute11"
        ],
        "Volume Tier Size"  : [
                FactorValueAttribute: "attribute28",
                FactorAttribute     : "attribute27"
        ],
        "Promotion"         : [
                FactorValueAttribute: "attribute32",
                FactorAttribute     : "attribute31"
        ],
        "OS"                : [
                FactorValueAttribute: "attribute36",
                FactorAttribute     : "attribute35"],

        "Hosting"           : [
                FactorValueAttribute: "attribute38",
                FactorAttribute     : "attribute37"
        ],
        "Program Option"    : [
                FactorValueAttribute: "attribute40",
                FactorAttribute     : "attribute39"
        ],
        "Upgrade"           : [
                FactorValueAttribute: "attribute30",
                FactorAttribute     : "attribute29"
        ],
        "Sub Region"        : [
                FactorValueAttribute: "attribute13",
                FactorAttribute     : "attribute5"
        ],
        "PS Term"           : [
                FactorValueAttribute: "attribute47",
                FactorAttribute     : "attribute46"
        ],
        "PS Option"         : [
                FactorValueAttribute: "attribute49",
                FactorAttribute     : "attribute48"
        ]

]
@Field final Map STANDARD_ATTRIBUTE_MAP = [
        "Segment Type"    : "Segment",
        "Volume Tier Size": "Volume Tier"
]
