Map pxTableSelection = [
        "ValidCombination": "Valid Combination",
        "ConfigTab"       : "Product Model"
]

api.inputBuilderFactory()
        .createOptionEntry(Constants.PX_TABLE_SELECTION)
        .setLabel(Constants.PX_TABLE_SELECTION_LABEL)
        .setRequired(false)
        .setOptions(pxTableSelection.keySet() as List)
        .setLabels(pxTableSelection)
        .getInput()