def controller = api.newController()
String fileName = api.input(Constants.FILE_NAME)
String url
String tableSelection = input.PXTableSelection

fileName = fileName?:tableSelection

if (tableSelection) {
    url = "/formulamanager.executeformula/PXTableDownload?output=xls&&fileName=" + fileName
} else {
    ResultMatrix resultMatrix = api.newMatrix(Constants.FIELD_NAME, Constants.WARNING)
    resultMatrix.addRow([(Constants.FIELD_NAME): Constants.PX_TABLE_SELECTION_LABEL, (Constants.WARNING): Constants.WARNING_MSG])
    return resultMatrix
}

Map payload = [
        filter: api.input(Constants.PRODUCT_INPUT_NAME),
        table : tableSelection
]

controller.addDownloadButton(out.ExportConfiguration.DOWNLOAD_EXCEL, url, api.jsonEncode(payload))
return controller