api.local.factor = out.CurrentItem.attribute1 ?: 0.0 as BigDecimal
api.local.amount = out.CurrentItem.attribute2 ?: 0.0 as BigDecimal
if (out.OverrideType == "Amount") {
    api.local.factor = (api.local.amount / api.global.basePrice)?.setScale(6, BigDecimal.ROUND_HALF_UP)
} else if (out.OverrideType == "Factor") {
    api.local.amount = (api.local.factor * api.global.basePrice)?.setScale(2, BigDecimal.ROUND_HALF_UP)
}