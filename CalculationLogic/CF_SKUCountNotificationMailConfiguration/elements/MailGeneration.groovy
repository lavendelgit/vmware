int skuCount = out.SKUCount
String eMailContent = ""
String shortdescContent = " "
String longdescContent = " "
String descupdateContent = " "
String eMailAttributes = " "

eMailContent = Lib.generateEMailContent(Constants.PRODUCT_TABLE_NAME, api.local.Product, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.OFFER_TABLE_NAME, api.local.validOfferCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.DATA_CENTER, api.local.validDataCenterCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.BASE_TABLE, api.local.validBaseCache, eMailContent)
eMailAttributes = Lib.generateEMailContentNoDesc(Constants.SAP_BASE_TABLE, api.local.SAPBaseSKUCache, eMailAttributes)
eMailContent = Lib.generateEMailContent(Constants.METRIC_TABLE, api.local.validMetricCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.TERM_TABLE, api.local.validTermCacheDesc, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.PAYMENT_TABLE, api.local.validPaymentTypeCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.TIER_TABLE, api.local.validVolumeTierCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.SEGMENT_TABLE, api.local.validSegmentCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.VERSION_TABLE, api.local.validVersionCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.PROMOTION_TABLE, api.local.validPromoCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.OS_TABLE, api.local.validOSCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.HOSTING_TABLE, api.local.validHostingCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.PROGRAM_OPTION_TABLE, api.local.validProgramOptionCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.QUANTITY_TABLE, api.local.validQuantityCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.PURCHASING_PROGRAM_TABLE, api.local.validPurchasingProgramCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.SUPPORT_TYPE_TABLE, api.local.validSupportTypeCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.SUPPORT_TIER_TABLE, api.local.validSupportTierCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.RETENTION_PERIOD_TABLE, api.local.validRetentionPeriodCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.PS_TERM_TABLE, api.local.validPSTermCache, eMailContent)
eMailContent = Lib.generateEMailContent(Constants.PS_OPTION_TABLE, api.local.validPSOptionCache, eMailContent)
eMailAttributes = Lib.generateEMailContentNoDesc(Constants.CURRENCY_TABLE, api.local.validCurrencyCache, eMailAttributes)
eMailAttributes = Lib.generateEMailContentNoDesc(Constants.SAP_CONFIGURATION_TABLE, api.local.SAPBaseSKUConfigurationCache, eMailAttributes)
eMailAttributes = Lib.generateEMailContentNoDesc(Constants.CONFIG_TAB_TABLE, api.local.ConfigTabAttributesCache, eMailAttributes)
eMailAttributes = Lib.generateEMailContentNoDesc(Constants.PRICE_LIST_REGION_TABLE, api.local.validPricelistRegionCache, eMailAttributes)
eMailAttributes = Lib.generateEMailContentNoDesc(Constants.SUB_REGION_TABLE, api.local.validsubRegionCache, eMailAttributes)
shortdescContent = Lib.generateDescContent(Constants.SHORT_DESC_TABLE, out.ShortDescCache, shortdescContent)
longdescContent = Lib.generateDescContent(Constants.LONG_DESC_TABLE, out.LongDescCache, longdescContent)

Map eMailId = out.eMailConfiguration
if (skuCount) {
    eMailId?.each {
        api.sendEmail(it.value, "$Constants.EMAIL_SUBJECT" + "$skuCount", "$Constants.EMAIL_HEAD" + "$it.key" + ",<br>" + "$Constants.EMAIL_MESSAGE_SKUCOUNT" + "$skuCount" + "$Constants.EMAIL_MESSAGE_TABLE_HEADER" + "$eMailContent" + "$Constants.EMAIL_MESSAGE_END_ELEMENT" + "$Constants.EMAIL_MESSAGE_TABLE_ATTRIBUTE" + "$eMailAttributes" + "$Constants.EMAIL_MESSAGE_END_ELEMENT" + "$Constants.EMAIL_MESSAGE_TABLE_HEADER_SHORT_DESC" + "$shortdescContent" + "$Constants.EMAIL_MESSAGE_END_ELEMENT" + "$Constants.EMAIL_MESSAGE_TABLE_HEADER_LONG_DESC" + "$longdescContent")

    }
}