return api.findLookupTableValues("SKUCountNotificationMailConfiguration", Filter.equal("Status", "Active")).collectEntries {
    [(it.name): it.attribute1]
}