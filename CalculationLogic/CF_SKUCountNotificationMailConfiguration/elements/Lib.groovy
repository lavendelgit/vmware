String generateEMailContentNoDesc(String attributeName, Map attributeData, String eMailContent) {
    if (attributeData != [:]) {
        StringBuilder eMailContentBuilder = new StringBuilder()
        eMailContentBuilder.append("<tr>").append("<td>").append(attributeName ?: 'N/A').append("</td>").append("<td>").append(attributeData.keySet().join(', ') ?: 'N/A').append("</td>").append("</td>").append("</tr>")
        eMailText = eMailContentBuilder.toString()
        eMailContent = eMailText ? (eMailContent + eMailText) : eMailContent
    }
    return eMailContent
}

String generateEMailContent(String attributeName, Map attributeData, String eMailContent) {
    if (attributeData != [:]) {
        StringBuilder eMailContentBuilder = new StringBuilder()
        labeList = attributeData?.collectEntries { [(it.key): it.value.attribute1] }
        label = labeList?.findAll { it.value != null }
        shortDesclist = attributeData?.collectEntries { [(it.key): it.value.attribute2] }
        shortDesc = shortDesclist?.findAll { it.value != null }
        longDescList = attributeData?.collectEntries { [(it.key): it.value.attribute3] }
        longDesc = longDescList?.findAll { it.value != null }
        eMailContentBuilder?.append("<tr>").append("<td>").append(attributeName ?: 'N/A').append("</td>").append("<td>").append(attributeData?.keySet()?.join(',') ?: 'N/A').append("</td>").append("<td>").append(label?.values()?.join(',') ?: 'N/A').append("</td>").append("<td>").append(shortDesc?.values()?.join(",") ?: 'N/A').append("</td>").append("<td>").append(longDesc?.values()?.join(",") ?: 'N/A').append("</td>").append("</tr>")
        eMailText = eMailContentBuilder?.toString()
        eMailContent = eMailText ? (eMailContent + eMailText) : eMailContent
    }
    return eMailContent
}

String generateDescContent(String attributeName, List attributeDataList, String descContent) {
    for (attributeData in attributeDataList) {
        StringBuilder eMailContentBuilder = new StringBuilder()
        eMailContentBuilder.append("<tr>").append("<td>").append(attributeData.name).append("</td>").append("<td>").append(attributeData.attribute1 ?: 'N/A').append("</td>").append("<td>").append(attributeData.attribute2 ?: 'N/A').append("</td>").append("<td>").append(attributeData.attribute3 ?: 'N/A').append("</td>").append("<td>").append(attributeData.attribute4 ?: 'N/A').append("</td>").append("</tr>")
        eMailText = eMailContentBuilder.toString()
        descContent = eMailText ? (descContent + eMailText) : descContent
    }
    return descContent
}
String generateUpdateDescContent(String attributeName, List attributeDataList, String descContent) {
    for (attributeData in attributeDataList) {
        StringBuilder eMailContentBuilder = new StringBuilder()
        eMailContentBuilder.append("<tr>").append("<td>").append(attributeData[1]).append("</td>").append("<td>").append(attributeData[2] ?: 'N/A').append("</td>").append("<td>").append(attributeData[3] ?: 'N/A').append("</td>").append("<td>")
        eMailText = eMailContentBuilder.toString()
        descContent = eMailText ? (descContent + eMailText) : descContent
    }
    return descContent
}
