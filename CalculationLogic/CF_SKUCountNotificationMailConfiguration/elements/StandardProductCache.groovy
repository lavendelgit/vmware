List filter = [
        Filter.equal("name", Constants.STANDARD_TABLE),
        Filter.in(Constants.STD_PRODUCTS_PRODUCT_COLUMN, api.local.Product?.keySet()),
        Filter.in(Constants.STD_PRODUCTS_OFFER_COLUMN, api.local.validOfferCache ? api.local.validOfferCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_BASE_COLUMN, api.local.validBaseCache ? api.local.validBaseCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_OS_COLUMN, api.local.validOSCache ? api.local.validOSCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_METRIC_COLUMN, api.local.validMetricCache ? api.local.validMetricCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_DC_COLUMN, api.local.validDataCenterCache ? api.local.validDataCenterCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_PS_TERM_COLUMN, api.local.validPSTermCache ? api.local.validPSTermCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_PS_OPTION_COLUMN, api.local.validPSOptionCache ? api.local.validPSOptionCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_SEGMENT_COLUMN, api.local.validSegmentCache ? api.local.validSegmentCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_QUANTITY_COLUMN, api.local.validQuantityCache ? api.local.validQuantityCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_TERM_UOM_COLUMN, api.local.validTermUomCache ? api.local.validTermCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_PAYMENT_COLUMN, api.local.validPaymentTypeCache ? api.local.validPaymentTypeCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_SUPPORT_TYPE_COLUMN, api.local.validSupportTypeCache ? api.local.validSupportTypeCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_SUPPORT_TIER_COLUMN, api.local.validSupportTierCache ? api.local.validSupportTierCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_PURCHASING_PROGRAM_COLUMN, api.local.validPurchasingProgramCache ? api.local.validPurchasingProgramCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_TIER_COLUMN, api.local.validVolumeTierCache ? api.local.validVolumeTierCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_VERSION_COLUMN, api.local.validVersionCache ? api.local.validVersionCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_PROMO_COLUMN, api.local.validPromoCache ? api.local.validPromoCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_HOSTING_COLUMN, api.local.validHostingCache ? api.local.validHostingCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_PROGRAM_OPTION_COLUMN, api.local.validProgramOptionCache ? api.local.validProgramOptionCache.keySet() : null),
        Filter.in(Constants.STD_PRODUCTS_RETENTION_PERIOD_COLUMN, api.local.validRetentionPeriodCache ? api.local.validRetentionPeriodCache.keySet() : null),

]
recordStream = api.stream("PX50", null, *filter)
api.local.standardProductCache = recordStream?.collect { row -> row }
recordStream?.close()
api.local.validStandardProductsCache = libs.stdProductLib.SkuGenHelper.cacheValidRules(api.local.standardProductCache, out.StandardProductMetaData, out.RuleKeyIndexMapping, true, out.AttributeDelimiterCache)
return
