api.local.pxRecords = []
def listAttributes = []
def placeholder = ["-"]
if (api.local.Product) listAttributes << api.local.Product?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validOfferCache) listAttributes << api.local.validOfferCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validDataCenterCache) listAttributes << api.local.validDataCenterCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validBaseCache) listAttributes << api.local.validBaseCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validMetricCache) listAttributes << api.local.validMetricCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validTermCache) {
    listAttributes << api.local.validTermCache?.keySet()
    listAttributes << Constants.TERMUOM_PLACEHOLDER
} else {
    listAttributes << placeholder
    listAttributes << placeholder
}
if (api.local.validPaymentTypeCache) listAttributes << api.local.validPaymentTypeCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validVolumeTierCache) listAttributes << api.local.validVolumeTierCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validSegmentCache) listAttributes << api.local.validSegmentCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validVersionCache) listAttributes << api.local.validVersionCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validPromoCache) listAttributes << api.local.validPromoCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validOSCache) listAttributes << api.local.validOSCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validQuantityCache) listAttributes << api.local.validQuantityCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validPurchasingProgramCache) listAttributes << api.local.validPurchasingProgramCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validSupportTypeCache) listAttributes << api.local.validSupportTypeCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validSupportTierCache) listAttributes << api.local.validSupportTierCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validHostingCache) listAttributes << api.local.validHostingCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validProgramOptionCache) listAttributes << api.local.validProgramOptionCache?.keySet()
else {
    listAttributes << placeholder
}
if (api.local.validRetentionPeriodCache) {
    listAttributes << api.local.validRetentionPeriodCache?.keySet()
    listAttributes << Constants.TERMUOM_PLACEHOLDER
} else {
    listAttributes << Constants.PLACEHOLDER
    listAttributes << Constants.PLACEHOLDER
}
if (api.local.validPSTermCache ) {
    listAttributes << api.local.validPSTermCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (api.local.validPSOptionCache) {
    listAttributes << api.local.validPSOptionCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
api.local.combinations = listAttributes.combinations()
return
