Map validUOMCache = libs.stdProductLib.RulesGenHelper.cacheCommonAttributes(out.Configuration.RETENTION_PERIOD_TABLE, false)
return validUOMCache?.collectEntries { [(it.key): it.value.attribute5] }