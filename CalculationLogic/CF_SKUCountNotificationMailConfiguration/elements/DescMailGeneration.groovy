int descskuCount = out.DescSKUCount
String descupdateContent = " "

descupdateContent = Lib.generateUpdateDescContent(Constants.UPDATE_DESC_TABLE, api.local.descriptionCache, descupdateContent)

Map eMailId = out.eMailConfiguration
if (descskuCount) {
    eMailId?.each {
        api.sendEmail(it.value, "$Constants.EMAIL_SUBJECT" + "$descskuCount", "$Constants.EMAIL_HEAD" + "$it.key" + ",<br>" + "$Constants.EMAIL_MESSAGE_DESCSKUCOUNT" + "$descskuCount" + "$Constants.EMAIL_MESSAGE_TABLE_HEADER_DESC_CHANGES" + "$descupdateContent")
    }
}