int batchSize = (Math.round(api.local.combinations?.size() / 5))
api.local.combinationsBatches = api.local.combinations?.collate(batchSize, true)
return