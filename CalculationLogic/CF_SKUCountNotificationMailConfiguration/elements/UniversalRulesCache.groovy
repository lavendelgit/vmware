def rulesGenLib = libs.stdProductLib.RulesGenHelper
def universalRecords = rulesGenLib.cacheInvalidUniversalRecords()
api.local.invalidUniversalRuleCache = [:]
api.local.invalidUniversalRuleCache = rulesGenLib.cacheInvalidRules(universalRecords, out.UniversalRulesMetaData, out.RuleKeyIndexMapping)
api.local.invalidUniversalRuleCache = rulesGenLib.populateTermUoMRules(api.local.invalidUniversalRuleCache, out.TermUoMConversionCache)
return