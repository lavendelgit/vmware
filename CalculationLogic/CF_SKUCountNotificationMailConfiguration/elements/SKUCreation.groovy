api.local.descriptionCache = []
String shortDesc = ""
for (standardProduct in api.local.validStandardProductsCache) {
    rule = standardProduct.value
    pXParams = [
            "TermUoM"             : rule.Term ? out.TermUomCache[rule.Term] : Constants.NULL_ATTRIBUTE_VALUE_PLACEHOLDER,
            "RetentionPeriodUOM"  : rule["Retention Period"] ? out.RetentionPeriodUomCache[rule["Retention Period"]] : Constants.NULL_ATTRIBUTE_VALUE_PLACEHOLDER,
            "ProductKey"          : rule?.Product,
            "OfferKey"            : rule?.Offer,
            "DataCenterKey"       : rule["Data Center"],
            "BaseKey"             : rule?.Base,
            "MetricKey"           : rule?.Metric,
            "TermKey"             : rule?.Term,
            "PaymentTypeKey"      : rule["Payment Type"],
            "VolumeTierKey"       : rule["Volume Tier"],
            "SegmentKey"          : rule?.Segment,
            "VersionKey"          : rule?.Version,
            "PromotionKey"        : rule?.Promotion,
            "PSTermKey"           : rule["PS Term"],
            "PSOptionKey"         : rule["PS Option"],
            "QuantityKey"         : rule?.Quantity,
            "SupportTierKey"      : rule["Support Tier"],
            "SupportTypeKey"      : rule["Support Type"],
            "PurchasingProgramKey": rule["Purchasing Program"],
            "OSKey"               : rule?.OS,
            "HostingKey"          : rule?.Hosting,
            "ProgramOptionKey"    : rule["Program Option"],
            "RetentionPeriodKey"  : rule["Retention Period"],
    ]
    sku = rule.StandardProductSKU
    shortDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.ShortDescCache, pXParams)
    longDescription = libs.stdProductLib.SkuGenHelper.createDesc(out.LongDescCache, pXParams)
    api.local.descriptionCache << [out.StandardDescriptionPPCache, sku, shortDesc, longDescription]
}
return