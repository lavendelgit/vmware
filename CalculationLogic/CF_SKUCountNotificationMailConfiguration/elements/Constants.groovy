import groovy.transform.Field

@Field final String STANDARD_TABLE = "StandardProducts"
@Field final String PRODUCT_TABLE_NAME = "Product"
@Field final String OFFER_TABLE_NAME = "Offer"
@Field final String DATA_CENTER = "DataCenter"
@Field final String BASE_TABLE = "Base"
@Field final String SAP_BASE_TABLE = "SAPBaseSKU"
@Field final String METRIC_TABLE = "Metric"
@Field final String TERM_TABLE = "Term"
@Field final String PAYMENT_TABLE = "PaymentType"
@Field final String TIER_TABLE = "Tier"
@Field final String SEGMENT_TABLE = "Segment"
@Field final String VERSION_TABLE = "Version"
@Field final String PROMOTION_TABLE = "Promo"
@Field final String OS_TABLE = "OS"
@Field final String HOSTING_TABLE = "Hosting"
@Field final String PROGRAM_OPTION_TABLE = "ProgramOption"
@Field final String QUANTITY_TABLE = "Quantity"
@Field final String PURCHASING_PROGRAM_TABLE = "PurchasingProgram"
@Field final String SUPPORT_TYPE_TABLE = "SupportType"
@Field final String SUPPORT_TIER_TABLE = "SupportTier"
@Field final String RETENTION_PERIOD_TABLE = "RetentionPeriod"
@Field final String PS_OPTION_TABLE = "PSOption"
@Field final String PS_TERM_TABLE = "PSTerm"
@Field final String CURRENCY_TABLE = "Currency"
@Field final String SAP_CONFIGURATION_TABLE = "SAPBaseSKUConfiguration"
@Field final String CONFIG_TAB_TABLE = "ConfigTabAttributes"
@Field final String PRICE_LIST_REGION_TABLE = "PriceListRegion"
@Field final String SUB_REGION_TABLE = "SubRegion"
@Field final String SHORT_DESC_TABLE = "ShortDescConfig"
@Field final String LONG_DESC_TABLE = "LongDescConfig"
@Field final String UPDATE_DESC_TABLE = "StandardProductsDescription"
@Field final String PLACEHOLDER = "-"
@Field final String RULES_TABLE_NAME = "ProductRule"
@Field final List DESCRIPTION_ATTRIBUTES = ["name", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6", "attribute7"]
@Field final String TERMUOM_PLACEHOLDER = "#"
@Field final Integer TERM_UOM_INDEX = 6
@Field final Integer TERM_INDEX = 5
@Field final String HTML_TAG_TR_START_ELEMENT = "<tr>"
@Field final String HTML_TAG_TR_END_ELEMENT = "</tr>"
@Field final String HTML_TAG_TD_START_ELEMENT = "<td>"
@Field final String HTML_TAG_TD_END_ELEMENT = "</td>"
@Field final String EMAIL_SUBJECT = " Total SKU Count :"
@Field final String EMAIL_HEAD = " Dear "
@Field final String NULL_ATTRIBUTE_VALUE_PLACEHOLDER = null
@Field final String EMAIL_MESSAGE_SKUCOUNT = "\nThe number of Skus generated for the following configuration are : "
@Field final String EMAIL_MESSAGE_DESCSKUCOUNT = "\nThe number of Skus generated for the following Descriptions configuration are : "
@Field final String STD_PRODUCTS_PRODUCT_COLUMN ="attribute1"
@Field final String STD_PRODUCTS_OFFER_COLUMN ="attribute2"
@Field final String STD_PRODUCTS_BASE_COLUMN ="attribute4"
@Field final String STD_PRODUCTS_OS_COLUMN ="attribute22"
@Field final String STD_PRODUCTS_METRIC_COLUMN ="attribute5"
@Field final String STD_PRODUCTS_DC_COLUMN ="attribute3"
@Field final String STD_PRODUCTS_PS_TERM_COLUMN ="attribute12"
@Field final String STD_PRODUCTS_PS_OPTION_COLUMN ="attribute13"
@Field final String STD_PRODUCTS_SEGMENT_COLUMN ="attribute9"
@Field final String STD_PRODUCTS_QUANTITY_COLUMN ="attribute16"
@Field final String STD_PRODUCTS_TERM_UOM_COLUMN ="attribute6"
@Field final String STD_PRODUCTS_PAYMENT_COLUMN ="attribute7"
@Field final String STD_PRODUCTS_SUPPORT_TYPE_COLUMN ="attribute19"
@Field final String STD_PRODUCTS_SUPPORT_TIER_COLUMN ="attribute18"
@Field final String STD_PRODUCTS_PURCHASING_PROGRAM_COLUMN ="attribute20"
@Field final String STD_PRODUCTS_TIER_COLUMN ="attribute8"
@Field final String STD_PRODUCTS_VERSION_COLUMN ="attribute10"
@Field final String STD_PRODUCTS_PROMO_COLUMN ="attribute11"
@Field final String STD_PRODUCTS_HOSTING_COLUMN ="attribute23"
@Field final String STD_PRODUCTS_PROGRAM_OPTION_COLUMN ="attribute25"
@Field final String STD_PRODUCTS_RETENTION_PERIOD_COLUMN ="attribute31"

@Field final String EMAIL_MESSAGE_TABLE_HEADER = "<html><head>\n" +
        "<style>\n" +
        "table, th, td {\n" +
        "  border: 1px solid black;\n" +
        "border-collapse: collapse;\n" +
        "text-align: center;\n" +
        " height: 30px;\n" +
        "}\n" +
        "</style>\n" +
        "</head>\n" +
        "<body>\n" +
        "\n" +
        "<h1>Combination</h1>\n" +
        "\n" +
        "<table cellspacing='2'>\n" +
        " <tr>\n" +
        "    <th>Attribute</th>\n" +
        "    <th>Name</th>\n" +
        "    <th>Label</th>\n" +
        "    <th>Short Description</th>\n" +
        "    <th>Long Description</th>\n" +
        "  </tr>\n"

@Field final String EMAIL_MESSAGE_TABLE_ATTRIBUTE = "<html><head>\n" +
        "<style>\n" +
        "table, th, td {\n" +
        "border: 1px solid black;\n" +
        "border-collapse: collapse;\n" +
        "text-align: center;\n" +
        " height: 30px;\n" +
        "}\n" +
        "</style>\n" +
        "</head>\n" +
        "<body>\n" +
        "\n" +
        "<h1>Misc Attributes</h1>\n" +
        "\n" +
        "<table cellspacing='2'>\n" +
        " <tr>\n" +
        "    <th>Attribute</th>\n" +
        "    <th>Value</th>\n" +
        "  </tr>\n"

@Field final String EMAIL_MESSAGE_TABLE_HEADER_SHORT_DESC = "<html><header>\n" +
        "<style>\n" +
        "table, th, td {\n" +
        "  border: 1px solid black;\n" +
        "border-collapse: collapse;\n" +
        "text-align: center;\n" +
        " height: 30px;\n" +
        "}\n" +
        "</style>\n" +
        "</header>\n" +
        "<body>\n" +
        "\n" +
        "<h1> Short Description</h1>\n" +
        "\n" +
        "<table cellspacing='2'>\n" +
        "  <tr>\n" +
        "    <th>Sequence</th>\n" +
        "    <th>Type</th>\n" +
        "    <th>Description</th>\n" +
        "    <th>Table Name</th>\n" +
        "    <th>Attribute Name</th>\n" +
        "  </tr>\n"

@Field final String EMAIL_MESSAGE_TABLE_HEADER_LONG_DESC = "<html><header>\n" +
        "<style>\n" +
        "table, th, td {\n" +
        "  border: 1px solid black;\n" +
        "border-collapse: collapse;\n" +
        "text-align: center;\n" +
        " height: 30px;\n" +
        "}\n" +
        "</style>\n" +
        "</header>\n" +
        "<body>\n" +
        "\n" +
        "<h1> Long Description</h1>\n" +
        "\n" +
        "<table cellspacing='2'>\n" +
        "  <tr>\n" +
        "    <th>Sequence</th>\n" +
        "    <th>Type</th>\n" +
        "    <th>Description</th>\n" +
        "    <th>Table Name</th>\n" +
        "    <th>Attribute Name</th>\n" +
        "  </tr>\n"

@Field final String EMAIL_MESSAGE_TABLE_HEADER_DESC_CHANGES = "<html><header>\n" +
        "<style>\n" +
        "table, th, td {\n" +
        "  border: 1px solid black;\n" +
        "border-collapse: collapse;\n" +
        "text-align: center;\n" +
        " height: 30px;\n" +
        "}\n" +
        "</style>\n" +
        "</header>\n" +
        "<body>\n" +
        "\n" +
        "<h1> Desc Changes</h1>\n" +
        "\n" +
        "<table cellspacing='2'>\n" +
        "  <tr>\n" +
        "    <th>Part Number</th>\n" +
        "    <th>Short Description</th>\n" +
        "    <th>Long Description</th>\n" +
        "  </tr>\n"

@Field final String EMAIL_MESSAGE_END_ELEMENT =
        "</tr>" +
                "</table>\n" +
                "\n" +
                "</body>\n" +
                "</html>\n"





