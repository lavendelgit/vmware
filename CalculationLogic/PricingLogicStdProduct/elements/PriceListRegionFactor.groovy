if (!out.Region && !api.local.baseRegion || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Region))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Region",
        Value: api.local.baseRegion ?: null
]
Map stdSkuAttribute = [
        Name : "Region",
        Value: out.Region ?: null
]
exceptionAdjustmentCache = api.global.regionAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.regionAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)