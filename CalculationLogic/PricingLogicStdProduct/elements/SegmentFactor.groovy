if (!out.SegmentType && !api.local.baseSegmentType || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.SegmentType))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Segment",
        Value: api.local.baseSegmentType ?: null
]
Map stdSkuAttribute = [
        Name : "Segment",
        Value: out.SegmentType ?: null
]

exceptionAdjustmentCache = api.global.segmentAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.segmentAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)