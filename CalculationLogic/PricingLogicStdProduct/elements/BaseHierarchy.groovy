api.local.baseProductGroup = out.BasePriceSKUInfo?.attribute36
api.local.baseProduct = out.BasePriceSKUInfo?.attribute3
api.local.baseOffer = out.BasePriceSKUInfo?.attribute4
api.local.baseSKUBase = out.BasePriceSKUInfo?.attribute5
api.local.baseSkuDetails = [
        ProductGroup: api.local.baseProductGroup,
        Product     : api.local.baseProduct,
        Offer       : api.local.baseOffer,
        Base        : api.local.baseSKUBase
]
return