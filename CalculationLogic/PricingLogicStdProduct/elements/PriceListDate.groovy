import java.text.SimpleDateFormat

def date = new Date()
standardFormat = new SimpleDateFormat("yyyy-MM-dd")
priceListDate = api.dateUserEntry("PriceListDate")
defaultDate = standardFormat.format(date)
parameter = api.getParameter("PriceListDate")
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
    parameter.setValue(defaultDate)
}
return api.attributedResult(priceListDate)
        .withBackgroundColor(Constants.BACKGROUND_COLOR_ATTRIBUTES)