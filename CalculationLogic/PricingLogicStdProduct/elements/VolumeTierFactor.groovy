if (!out.VolumeTierSize && !api.local.baseVolumeTierSize || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.VolumeTierSize))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Volume Tier",
        Value: api.local.baseVolumeTierSize ?: null
]
Map stdSkuAttribute = [
        Name : "Volume Tier",
        Value: out.VolumeTierSize ?: null
]
exceptionAdjustmentCache = api.global.volumeTierAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.volumeTierAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)