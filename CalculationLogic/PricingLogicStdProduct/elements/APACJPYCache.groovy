upliftCache = api.local.upliftCache?.findAll {
    it.value.Currency == Constants.JPY_CURRENCY && it.value["Price List Region"] == Constants.REGION_APAC_JPY
}

currencyAdjustmentCache = api.local.currencyAdjustmentCache?.findAll {
    it.value.Currency == Constants.JPY_CURRENCY && it.value["Price List Region"] == Constants.REGION_APAC_JPY
}

roundingRulesCache = api.local.roundingRulesCache?.findAll {
    it.value.Currency == Constants.JPY_CURRENCY || it.value.Currency == "*"
}
return Lib.convertToSpecificCurrenyRegion(api.local.upliftKeyLists, api.local.roundingRulesKeyLists, api.local.productPriceListRegionCache, upliftCache, currencyAdjustmentCache, roundingRulesCache, api.local.stdSkuDetails, out.SKU, Constants.REGION_APAC_JPY, Constants.JPY_CURRENCY)