if (!out.Metric && !api.local.baseMetric || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Metric))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Metric",
        Value: api.local.baseMetric ?: null
]

Map stdSkuAttribute = [
        Name : "Metric",
        Value: out.Metric ?: null
]

exceptionAdjustmentCache = api.global.metricAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.metricAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)