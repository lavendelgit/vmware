if (!out.Hosting && !api.local.baseHosting || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Hosting))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Hosting",
        Value: api.local.baseHosting ?: null
]
Map stdSkuAttribute = [
        Name : "Hosting",
        Value: out.Hosting ?: null
]
Map exceptionAdjustmentCache = api.global.hostingAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

Map defaultAdjustmentCache = api.global.hostingAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)