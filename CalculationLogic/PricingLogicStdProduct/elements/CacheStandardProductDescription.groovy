return api.findLookupTableValues(Constants.PP_STANDARD_PRODUCT_DESCRIPTION_TABLE, [Constants.NAME, Constants.SHORT_DESCRIPTION_NAME, Constants.LONG_DESCRIPTION_NAME], null, Filter.equal(Constants.NAME, out.SKU))?.collectEntries {
    [(it.name): [
            (Constants.SHORT_DESCRIPTION): it.attributeExtension___ShortDescription,
            (Constants.LONG_DESCRIPTION) : it.attributeExtension___LongDescription
    ]
    ]
}
