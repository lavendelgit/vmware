if (!out.Upgrade && !api.local.baseUpgrade || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Upgrade))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Upgrade",
        Value: api.local.baseUpgrade ?: null
]
Map stdSkuAttribute = [
        Name : "Upgrade",
        Value: out.Upgrade ?: null
]
exceptionAdjustmentCache = api.global.upgradeAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.upgradeAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)