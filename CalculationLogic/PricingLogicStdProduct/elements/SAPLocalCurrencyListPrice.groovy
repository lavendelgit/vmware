import net.pricefx.server.dto.calculation.ResultMatrix

List sapListPrices = Constants.SAP_LOCAL_CURRENCY_LIST
ResultMatrix sapLocalCurrencyListPrice = api.newMatrix(sapListPrices)
sapLocalCurrencyListPrice.addRow(
        [
                out.SAPNAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_NAM_USD_ListPrice ?: 0.0,
                (out.SAPNAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_NAM_USD_ListPrice ?: 0.0),
                out.SAPAPACUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_APAC_USD_ListPrice ?: 0.0,
                (out.SAPAPACUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_APAC_USD_ListPrice ?: 0.0),
                out.SAPCHINAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_CHINA_USD_ListPrice ?: 0.0,
                (out.SAPCHINAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_CHINA_USD_ListPrice ?: 0.0),
                out.SAPLATAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_LATAM_USD_ListPrice ?: 0.0,
                (out.SAPLATAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_LATAM_USD_ListPrice ?: 0.0),
                out.SAPEMEAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_EMEA_USD_ListPrice ?: 0.0,
                (out.SAPEMEAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_EMEA_USD_ListPrice ?: 0.0),
                out.SAPEMEAUSD2 ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_EMEA_USD2_ListPrice ?: 0.0,
                (out.SAPEMEAUSD2 ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_EMEA_USD2_ListPrice ?: 0.0),
                out.SAPGlobalUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_Global_USD_ListPrice ?: 0.0,
                (out.SAPGlobalUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_Global_USD_ListPrice ?: 0.0),
                out.SAPEMEAEUR ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_EMEA_EUR_ListPrice ?: 0.0,
                (out.SAPEMEAEUR ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_EMEA_EUR_ListPrice ?: 0.0),
                out.SAPEMEAGBP ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_EMEA_GBP_ListPrice ?: 0.0,
                (out.SAPEMEAGBP ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_EMEA_GBP_ListPrice ?: 0.0),
                out.SAPAPACCNY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_APAC_CNY_ListPrice ?: 0.0,
                (out.SAPAPACCNY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_APAC_CNY_ListPrice ?: 0.0),
                out.SAPAPACAUD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_APAC_AUD_ListPrice ?: 0.0,
                (out.SAPAPACAUD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_APAC_AUD_ListPrice ?: 0.0),
                out.SAPAPACJPY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_APAC_JPY_ListPrice ?: 0.0,
                (out.SAPAPACJPY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.SAP_APAC_JPY_ListPrice ?: 0.0),
        ]
)
return sapLocalCurrencyListPrice
