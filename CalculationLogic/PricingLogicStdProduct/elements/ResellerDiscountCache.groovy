String table = "ResellerDiscount"
api.global.resellerDiscountCache = api.global.resellerDiscountCache ?: api.findLookupTableValues(table, "key5", Filter.lessOrEqual("key5", out.PriceListDate)).collectEntries { [(it.key1 + "_" + it.key2 + "_" + it.key3 + "_" + it.key4): it.attribute1] }
return