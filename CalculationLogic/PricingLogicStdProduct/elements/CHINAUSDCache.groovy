upliftCache = api.local.USDupliftCache?.findAll {
    it.value["Price List Region"] == Constants.REGION_CHINA_USD
}
currencyAdjustmentCache = api.local.USDcurrencyAdjustmentCache?.findAll {
    it.value["Price List Region"] == Constants.REGION_CHINA_USD
}

return Lib.convertToSpecificCurrenyRegion(api.local.upliftKeyLists, api.local.roundingRulesKeyLists, api.local.productPriceListRegionCache, upliftCache, currencyAdjustmentCache, api.local.USDroundingRulesCache, api.local.stdSkuDetails, out.SKU, Constants.REGION_CHINA_USD, Constants.USD_CURRENCY)