Map getPrecision(Map rulesCache, String productGroup, String product, String offer, String base, List adjs) {
    String key
    Map rounding = [:]
    sortedCombinations = getSortedCombinations(productGroup, product, offer, base, adjs)
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        rounding.Precision = rulesCache?.getAt(key)?.Precision as Integer
        rounding.RoundingType = rulesCache?.getAt(key)?.Type
        if (rounding.RoundingType)
            return rounding
    }
}

Map getSortedCombinations(String productGroup, String product, String offer, String base, List adjs) {
    List combinations = []
    combinations << [productGroup, "*"]
    combinations << [product, "*"]
    combinations << [offer, "*"]
    combinations << [base, "*"]
    for (adj in adjs) {
        combinations << [adj]
    }
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ libs.vmwareUtil.CacheManager.getWeightage(it, "*") })?.sort { -it.key }
}


BigDecimal getAdjustments(Map adjustmentCache, String productGroup, String product, String offer, String base, List adjs) {
    convertedAdjusment = standardKeyConversion(adjustmentCache)
    Map sortedCombinations = getSortedCombinations(productGroup, product, offer, base, adjs)
    BigDecimal adjustment = 0.0
    String key
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        adjustment = convertedAdjusment?.getAt(key?.toLowerCase())?.Adjustment
        if (adjustment) {
            return adjustment
        }
    }
}

Map getFxRateAndUplift(Map currencyFxRateCache, Map upliftCache, String productGroup, String product, String offer, String base, List adjs) {
    Map sortedCombinations = getSortedCombinations(productGroup, product, offer, base, adjs)
    BigDecimal adjustment = 0.0
    String key
    Map fxRateUplift = [:]
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        adjustment = currencyFxRateCache?.getAt(key)?.Adjustment
        if (adjustment) {
            fxRateUplift.CurrencyFxRate = adjustment
            fxRateUplift.Uplift = upliftCache?.getAt(key)?.Adjustment
            return fxRateUplift
        }

    }
}

Map getAdjustmentsFromCache(Map adjustmentsCache) {
    Map keyLists = [:]
    List baseKeyLists = []
    List skuKeyLists = []
    Map keys = [:]
    Map baseKeys = [:]
    adjustmentsCache?.each {
        adj ->
            keys = [:]
            baseKeys = [:]
            adj.value.Keys?.each {
                key ->
                    key = key?.replaceAll("\\s", "")
                    if (out[key])
                        keys[key] = out[key]
                    if (api.local["base" + key])
                        baseKeys[key] = api.local["base" + key]
            }
            baseKeyLists << baseKeys
            skuKeyLists << keys
    }
    skuKeyLists = skuKeyLists?.minus(["Product Group", "Product", "Offer", "Base"])
    baseKeyLists = baseKeyLists?.minus(["Product Group", "Product", "Offer", "Base"])
    keyLists.StdKeys = skuKeyLists
    keyLists.BaseKeys = baseKeyLists
    return keyLists

}

Map populateTermAltUoMAdjustments(Map adjustmentCache) {
    String term
    String termUoM
    Map populatedTermCache = [:]
    Map destTerm = [:]
    adjustmentCache?.findAll { it.value.Term != null && it.value["Term UoM"] != null }?.each {
        term = it.value.Term
        termUoM = it.value["Term UoM"]
        destTerm = api.global.termUoMConversionCache[term + "_" + termUoM]
        if (destTerm) {
            it.value.Term = destTerm.DestTerm
            it.value["Term UoM"] = destTerm.DestUoM
            key = frameTermKeys(it.value)
            populatedTermCache[key] = it.value
        }
    }
    return populatedTermCache
}

Map cacheAdjustments(String factorAttribute, String typeAttribute = null, boolean isRoundingRule = false) {
    Map adjustmentCache = [:]
    List adjustments = api.global.stdAdjustmentsCache?.findAll { it[factorAttribute] != null }
    List keys
    Integer termIndex = 0
    Integer productGroupIndex = 0
    Integer productIndex = 0
    excludeList = ["version", "typedId", "id", "name", "sku", "createDate", "createdBy", "lastUpdateDate", "lastUpdateBy", factorAttribute, "attribute42", "attribute41"]
    if (isRoundingRule) {
        excludeList << typeAttribute
    }
    adjustments?.each {
        keys = it.keySet().collect()
        keysList = keys?.minus(excludeList)
        termIndex = keysList?.indexOf("attribute7")
        termUoMIndex = keysList?.indexOf("attribute33")
        productGroupIndex = keysList?.indexOf("attribute50")
        productIndex = keysList?.indexOf("attribute1")
        if (termIndex != -1 && termUoMIndex != -1) {
            keysList = keysList?.minus(["attribute33"])
            keysList = keysList.plus(termIndex + 1, "attribute33")
        }
        if (productGroupIndex != -1) {
            keysList = keysList?.minus(["attribute50"])
            keysList = keysList.plus(productIndex, "attribute50")
        }
        if (isRoundingRule) {
            keysList = keysList?.minus(["attribute4", "attribute23"])
            keysList = keysList.plus(["attribute4", "attribute23"])
        }

        keyNames = []
        keysList?.each {
            key ->
                keyNames << api.global.stdAdjMetaData[key]
        }
        keyValueList = []
        Map record = [:]
        keysList?.each {
            key ->
                if (it[key]) {
                    keyValueList << it[key]
                    record[api.global.stdAdjMetaData[key]] = it[key]
                }
        }
        key = libs.vmwareUtil.util.frameKey(keyValueList)
        if (isRoundingRule) {
            record.Precision = it[factorAttribute]
            record.Type = it[typeAttribute]
            record.Keys = keyNames
            adjustmentCache[key] = record
        } else {
            record.Adjustment = it[factorAttribute]
            record.Keys = keyNames
            adjustmentCache[key] = record
        }
    }
    adjustmentCache = adjustmentCache?.sort { -it.value?.Keys?.size() }
    return adjustmentCache
}

String frameTermKeys(Map termAdj) {
    String keyString
    List keys = termAdj?.Keys
    keys?.each {
        key ->
            keyString = keyString ? keyString + "_" + termAdj[key] : termAdj[key]
    }
    return keyString
}

Map cacheAndPopulateTermUoMAdjustments(String factorAttribute) {
    Map factorAdjustmentCache = cacheAdjustments(factorAttribute)
    factorAdjustmentCache = populateTermUoMAdjustments(factorAdjustmentCache)
    return factorAdjustmentCache
}

Map populateTermUoMAdjustments(Map factorAdjustmentCache) {
    Map populatedTermCache = [:]
    Map tempAdjustmentCache = [:]
    if (factorAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
        factorAdjustmentCache.each { key, value -> tempAdjustmentCache[key] = value.clone() }
        populatedTermCache = populateTermAltUoMAdjustments(tempAdjustmentCache)
        if (populatedTermCache?.size() > 0 && factorAdjustmentCache?.size() > 0) {
            factorAdjustmentCache = factorAdjustmentCache + populatedTermCache
        }
    }
    return factorAdjustmentCache
}

BigDecimal getAdjustmentsForAttribute(Map exceptionAdjustmentCache, Map defaultAdjustmentCache, Map baseSKUDetail, Map stdSKUDetail, Map baseAdjustmentDetail, Map stdAdjustmentDetail) {

    Map keyLists = getAdjustmentsFromCache(exceptionAdjustmentCache)
    Map defaultkeyLists = getAdjustmentsFromCache(defaultAdjustmentCache)
    BigDecimal skuAdjustment
    BigDecimal baseAdjustment
    keyLists?.BaseKeys?.unique()

    for (keyList in keyLists?.BaseKeys) {
        baseAdjustment = getAdjustments(exceptionAdjustmentCache, baseSKUDetail.ProductGroup, baseSKUDetail.Product, baseSKUDetail.Offer, baseSKUDetail.Base, keyList.values().drop(4))
        if (baseAdjustment) {
            break
        }
    }
    keyLists?.StdKeys?.unique()
    for (keyList in keyLists?.StdKeys) {
        skuAdjustment = getAdjustments(exceptionAdjustmentCache, stdSKUDetail.ProductGroup, stdSKUDetail.Product, stdSKUDetail.Offer, stdSKUDetail.Base, keyList.values().drop(4))
        if (skuAdjustment) {
            break
        }
    }
    if (!baseAdjustment) {
        defaultkeyLists?.BaseKeys?.unique()
        for (keyList in defaultkeyLists?.BaseKeys) {
            baseAdjustment = getAdjustments(defaultAdjustmentCache, baseSKUDetail.ProductGroup, baseSKUDetail.Product, baseSKUDetail.Offer, baseSKUDetail.Base, keyList.values().drop(4))
            if (baseAdjustment) {
                break
            }
        }
    }
    if (!skuAdjustment) {
        defaultkeyLists?.StdKeys?.unique()
        for (keyList in defaultkeyLists?.StdKeys) {
            skuAdjustment = getAdjustments(defaultAdjustmentCache, stdSKUDetail.ProductGroup, stdSKUDetail.Product, stdSKUDetail.Offer, stdSKUDetail.Base, keyList.values().drop(4))
            if (skuAdjustment) {
                break
            }
        }
    }
    if (!baseAdjustment) {
        if (baseAdjustmentDetail.Value) {
            api.addWarning("Base " + baseAdjustmentDetail.Name + "factor not found for " + baseSKUDetail.ProductGroup + "," + baseSKUDetail.Product + "," + baseSKUDetail.Offer + ", " + baseSKUDetail.Base + ", " + baseAdjustmentDetail.Value)
        } else {
            baseAdjustment = 1
        }

    }
    if (!skuAdjustment) {
        if (stdAdjustmentDetail?.Value) {
            api.addWarning("SKU " + stdAdjustmentDetail.Name + "factor not found for " + stdSKUDetail.ProductGroup + "," + stdSKUDetail.Product + "," + stdSKUDetail.Offer + ", " + stdSKUDetail.Base + ", " + stdAdjustmentDetail?.Value)
        } else {
            skuAdjustment = 1
        }
    }
    if (baseAdjustment && skuAdjustment) {
        return (skuAdjustment / baseAdjustment) as BigDecimal
    }
    return 1
}

Map convertToSpecificCurrenyRegion(def upliftKeyLists, def roundingRulesKeyLists, Map currencyRegionRulesCache, Map upliftCache, Map currencyAdjustmentCache, Map roundingRulesCache, Map stdSkuDetails, String product, String region, String currency) {
    if (currencyRegionRulesCache[product]?.find { it.Currency == currency && it.PriceListRegion == region }) {
        BigDecimal uplift
        BigDecimal currencyFxRate
        Map fxRateUplift = [:]
        Map fxRateRoundedPrice = [:]
        for (keyList in upliftKeyLists) {
            upliftKeyList = keyList.values().drop(4)
            upliftKeyList << region
            upliftKeyList << currency
            fxRateUplift = getFxRateAndUplift(currencyAdjustmentCache, upliftCache, stdSkuDetails.ProductGroup, stdSkuDetails.Product, stdSkuDetails.Offer, stdSkuDetails.Base, upliftKeyList)
            if (fxRateUplift) {
                uplift = fxRateUplift.Uplift
                currencyFxRate = fxRateUplift.CurrencyFxRate
                break
            }
        }
        if (!uplift)
            api.addWarning("Uplift not found")
        else if (!currencyFxRate) {
            api.addWarning("FXRate not found")
        }
        if (uplift && currencyFxRate) {
            Map roundingRule = [:]
            boolean isRegion = roundingRulesCache.collect { it.value.Keys }?.flatten()?.contains("Price List Region")
            if (isRegion) {
                for (keyList in roundingRulesKeyLists) {
                    upliftKeyList = keyList.values().drop(4)
                    upliftKeyList << region
                    upliftKeyList << currency
                    roundingRule = getPrecision(roundingRulesCache, stdSkuDetails.ProductGroup, stdSkuDetails.Product, stdSkuDetails.Offer, stdSkuDetails.Base, upliftKeyList)
                    if (roundingRule?.Precision != null) {
                        break
                    }
                }
            }
            if (roundingRule?.Precision == null) {
                for (keyList in roundingRulesKeyLists) {
                    upliftKeyList = keyList.values().drop(4)
                    upliftKeyList << currency
                    roundingRule = getPrecision(roundingRulesCache, stdSkuDetails.ProductGroup, stdSkuDetails.Product, stdSkuDetails.Offer, stdSkuDetails.Base, upliftKeyList)
                    if (roundingRule?.Precision != null) {
                        break
                    }
                }
            }
            precision = ((roundingRule?.Precision != null) ? roundingRule.Precision : 2) as Integer
            roundingType = roundingRule?.RoundingType ?: "Round"
            listPrice = out.ListPrice * uplift * currencyFxRate
            fxRateRoundedPrice.Precision = precision
            fxRateRoundedPrice.RoundingType = roundingType
            fxRateRoundedPrice.Uplift = uplift
            fxRateRoundedPrice.CurrencyFxRate = currencyFxRate
            fxRateRoundedPrice.RoundedPrice = libs.vmwareUtil.PricingHelper.roundedPrice(listPrice, roundingType, precision)
            return fxRateRoundedPrice
        }

        return null
    }

}

BigDecimal getDiscount(Map discountCombination, String productGroup, String product, String base, String offer) {
    BigDecimal distiDiscount = 0.0
    List adjusment = null
    Map sortedCombinations = getSortedCombinations(productGroup, product, base, offer, adjusment)
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        distiDiscount = discountCombination?.get(key) as BigDecimal
        if (distiDiscount) {
            return distiDiscount
        }
    }
    return null
}

BigDecimal calculateDistiDiscount(BigDecimal price, BigDecimal distiDiscount, String roundingType, int precision) {
    BigDecimal calculatedDiscount = price - (price * distiDiscount)
    BigDecimal distiDiscountPrice = libs.vmwareUtil.PricingHelper.roundedPrice(calculatedDiscount, roundingType, precision)
    return distiDiscountPrice
}

Map standardKeyConversion(Map keyAdjusments) {
    Map convertedAdjusments = [:]
    keyAdjusments?.each { key, value ->
        convertedAdjusments[key.toLowerCase()] = value
    }
    return convertedAdjusments
}

BigDecimal getRegionCurrencySpecificSAPPrice(Map lpgSequence, Map currencyCache) {
    BigDecimal sapListPrice = out.BasePrice
    lpgSequence?.each { key, value ->
        attributeKey = key.replaceAll("\\s", "") + "Factor"
        if (out[attributeKey] != null) {
            BigDecimal factor = out[attributeKey]
            if (attributeKey == Constants.PRICE_LIST_REGION_FACTOR) {
                factor = currencyCache?.Uplift ?: 1
            }
            if (attributeKey == Constants.CURRENCY_FACTOR) {
                factor = currencyCache?.CurrencyFxRate ?: 1
            }
            if (value.Operation == Constants.MULTIPLY) {
                sapListPrice = (sapListPrice * factor) as BigDecimal
                sapListPrice = libs.vmwareUtil.PricingHelper.roundedPrice(sapListPrice, currencyCache?.RoundingType ?: "Round", currencyCache?.Precision ?: 2)
            }

            if (value.Operation == Constants.DIVIDE) {
                sapListPrice = (sapListPrice / factor) as BigDecimal
                sapListPrice = libs.vmwareUtil.PricingHelper.roundedPrice(sapListPrice, currencyCache?.RoundingType ?: "Round", currencyCache?.Precision ?: 2)
            }
        }
    }
    return sapListPrice
}
