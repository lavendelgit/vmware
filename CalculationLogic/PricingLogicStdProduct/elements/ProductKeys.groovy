api.local.productPriceListRegionCache = api.local.priceListRegionCache?.findAll { it.key == "*" || it.key == out.SKU }
api.local.upliftCache = api.global.upliftCache?.findAll { (it.value["Product Group"] == "*" || it.value["Product Group"] == out.ProductGroup) && (it.value["Product"] == "*" || it.value["Product"] == out.Product) }

api.local.currencyAdjustmentCache = api.global.currencyAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" || it.value["Product Group"] == out.ProductGroup) && (it.value["Product"] == "*" || it.value["Product"] == out.Product)
}
api.local.roundingRulesCache = api.global.roundingRulesCache?.findAll {
    (it.value["Product Group"] == "*" || it.value["Product Group"] == out.ProductGroup) && (it.value["Product"] == "*" || it.value["Product"] == out.Product)
}
return