import net.pricefx.server.dto.calculation.ResultMatrix

List distiListPrices = Constants.DISTI_LIST_PRICE_LIST
ResultMatrix pricefxDistiListPrice = api.newMatrix(distiListPrices)
pricefxDistiListPrice.addRow(
        [
                out.DistiListPriceNAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceNAMUSDpfx ?: 0.0,
                (out.DistiListPriceNAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceNAMUSDpfx ?: 0.0),
                out.DistiListPriceAPACUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceAPACUSDpfx ?: 0.0,
                (out.DistiListPriceAPACUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceAPACUSDpfx ?: 0.0),
                out.DistiListPriceCHINAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceCHINAUSDpfx ?: 0.0,
                (out.DistiListPriceCHINAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceCHINAUSDpfx ?: 0.0),
                out.DistiListPriceLATAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceLATAMUSDpfx ?: 0.0,
                (out.DistiListPriceLATAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceLATAMUSDpfx ?: 0.0),
                out.DistiListPriceEMEAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceEMEAUSDpfx ?: 0.0,
                (out.DistiListPriceEMEAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceEMEAUSDpfx ?: 0.0),
                out.DistiListPriceEMEAUSD2 ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceEMEAUSD2pfx ?: 0.0,
                (out.DistiListPriceEMEAUSD2 ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceEMEAUSD2pfx ?: 0.0),
                out.DistiListPriceGlobalUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceGlobalUSDpfx ?: 0.0,
                (out.DistiListPriceGlobalUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceGlobalUSDpfx ?: 0.0),
                out.DistiListPriceEMEAEUR ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceEMEAEURpfx ?: 0.0,
                (out.DistiListPriceEMEAEUR ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceEMEAEURpfx ?: 0.0),
                out.DistiListPriceEMEAGBP ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceEMEAGBPpfx ?: 0.0,
                (out.DistiListPriceEMEAGBP ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceEMEAGBPpfx ?: 0.0),
                out.DistiListPriceAPACCNY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceAPACCNYpfx ?: 0.0,
                (out.DistiListPriceAPACCNY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceAPACCNYpfx ?: 0.0),
                out.DistiListPriceAPACAUD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceAPACAUDpfx ?: 0.0,
                (out.DistiListPriceAPACAUD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceAPACAUDpfx ?: 0.0),
                out.DistiListPriceAPACJPY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceAPACJPYpfx ?: 0.0,
                (out.DistiListPriceAPACJPY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPriceAPACJPYpfx ?: 0.0),
        ]
)
return pricefxDistiListPrice