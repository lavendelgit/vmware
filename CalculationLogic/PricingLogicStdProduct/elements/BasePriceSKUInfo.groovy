return api.attributedResult(api.global.baseSKUCache?.getAt(out.BasePriceSKU))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_ATTRIBUTES)