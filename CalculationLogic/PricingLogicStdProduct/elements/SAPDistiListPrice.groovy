import net.pricefx.server.dto.calculation.ResultMatrix

List sapDistiListPrices = Constants.SAP_DISTI_LIST_PRICE_LIST
ResultMatrix sapDistiListPrice = api.newMatrix(sapDistiListPrices)
sapDistiListPrice.addRow(
        [
                out.DistiListPriceSAPNAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_NAM_USD_pfx ?: 0.0,
                (out.DistiListPriceSAPNAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_NAM_USD_pfx ?: 0.0),
                out.DistiListPriceSAPAPACUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_APAC_USD_pfx ?: 0.0,
                (out.DistiListPriceSAPAPACUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_APAC_USD_pfx ?: 0.0),
                out.DistiListPriceSAPCHINAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_CHINA_USD_pfx ?: 0.0,
                (out.DistiListPriceSAPCHINAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_CHINA_USD_pfx ?: 0.0),
                out.DistiListPriceSAPLATAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_LATAM_USD_pfx ?: 0.0,
                (out.DistiListPriceSAPLATAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_LATAM_USD_pfx ?: 0.0),
                out.DistiListPriceSAPEMEAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_EMEA_USD_pfx ?: 0.0,
                (out.DistiListPriceSAPEMEAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_EMEA_USD_pfx ?: 0.0),
                out.DistiListPriceSAPEMEAUSD2 ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_EMEA_USD_pfx ?: 0.0,
                (out.DistiListPriceSAPEMEAUSD2 ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_EMEA_USD_pfx ?: 0.0),
                out.DistiListPriceSAPGlobalUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_Global_USD_pfx ?: 0.0,
                (out.DistiListPriceSAPGlobalUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_Global_USD_pfx ?: 0.0),
                out.DistiListPriceSAPEMEAEUR ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_EMEA_EUR_pfx ?: 0.0,
                (out.DistiListPriceSAPEMEAEUR ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_EMEA_EUR_pfx ?: 0.0),
                out.DistiListPriceSAPEMEAGBP ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_EMEA_GBP_pfx ?: 0.0,
                (out.DistiListPriceSAPEMEAGBP ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_EMEA_GBP_pfx ?: 0.0),
                out.DistiListPriceSAPAPACCNY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_APAC_CNY_pfx ?: 0.0,
                (out.DistiListPriceSAPAPACCNY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_APAC_CNY_pfx ?: 0.0),
                out.DistiListPriceSAPAPACAUD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_APAC_AUD_pfx ?: 0.0,
                (out.DistiListPriceSAPAPACAUD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_APAC_AUD_pfx ?: 0.0),
                out.DistiListPriceSAPAPACJPY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_APAC_JPY_pfx ?: 0.0,
                (out.DistiListPriceSAPAPACJPY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.DistiListPrice_SAP_APAC_JPY_pfx ?: 0.0),
        ]
)
return sapDistiListPrice
