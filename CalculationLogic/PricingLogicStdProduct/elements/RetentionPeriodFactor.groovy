if (!out.RetentionPeriod && !api.local.baseRetentionPeriod || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.RetentionPeriod))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Retention Period",
        Value: api.local.baseRetentionPeriod ?: null
]
Map stdSkuAttribute = [
        Name : "Retention Period",
        Value: out.RetentionPeriod ?: null
]
exceptionAdjustmentCache = api.global.retentionPeriodAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.retentionPeriodAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)