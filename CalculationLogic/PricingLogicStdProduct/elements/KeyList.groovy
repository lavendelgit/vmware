api.local.upliftKeyLists = Lib.getAdjustmentsFromCache(api.local.upliftCache)?.StdKeys?.unique()
api.local.currencyKeyLists = Lib.getAdjustmentsFromCache(api.local.currencyAdjustmentCache)?.StdKeys?.unique()
api.local.roundingRulesKeyLists = Lib.getAdjustmentsFromCache(api.local.roundingRulesCache)?.StdKeys?.unique()
return