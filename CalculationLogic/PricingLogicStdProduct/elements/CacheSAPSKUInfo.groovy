if (!api.global.sapBaseSKUCache) {
    List filter = [
            Filter.equal("name", "SAPBASESKU")
    ]
    recordStream = api.stream("PX10", null, *filter)
    api.global.sapBaseSKUCache = recordStream?.collectEntries { [(it.sku): ["ShortDescription": it.attribute1, "LongDescription": it.attribute2, "LongDescription1": it.attribute3, "LongDescription2": it.attribute4, "LongDescription3": it.attribute5]] }
    recordStream?.close()
}
return