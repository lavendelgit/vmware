if (!api.global.roundingRulesCache) {
    api.global.roundingRulesCache = [:]
    api.global.roundingRulesCache = Lib.cacheAdjustments("attribute26", "attribute25", true)
    api.global.roundingRulesCache = Lib.populateTermUoMAdjustments(api.global.roundingRulesCache)
}
return