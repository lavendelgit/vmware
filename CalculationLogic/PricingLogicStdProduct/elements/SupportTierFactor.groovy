if (!out.SupportTier && !api.local.baseSupportTier || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.SupportTier))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Support Tier",
        Value: api.local.baseSupportTier ?: null
]
Map stdSkuAttribute = [
        Name : "Support Tier",
        Value: out.SupportTier ?: null
]
exceptionAdjustmentCache = api.global.supportTierAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.supportTierAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)