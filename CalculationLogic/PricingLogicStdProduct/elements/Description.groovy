import net.pricefx.server.dto.calculation.ResultMatrix

Map descriptionValues = out.CacheStandardProductDescription
List descriptionLabels = [Constants.SHORT_DESCRIPTION_LABEL, Constants.LONG_DESCRIPTION_LABEL]
ResultMatrix stdProductDescription = api.newMatrix(descriptionLabels)
stdProductDescription.addRow(
        [
                descriptionValues[out.SKU]?.ShortDescription,
                descriptionValues[out.SKU]?.LongDescription
        ]
)
return stdProductDescription