api.local.pid = api.product("sku")
api.global.batch = api.global.batch ?: [:]
api.global.pricechangebatch = api.global.pricechangebatch ?: [:]
api.global.bookingDetailsbatch = api.global.bookingDetailsbatch ?: [:]
if (!api.global.batch[api.local.pid]) {
    api.global.batch.clear()
    def batch = api.getBatchInfo()?.collect { it[0] } ?: [api.local.pid]
    filter = [
            Filter.equal("name", "StandardProducts"),
            Filter.in("sku", batch)
    ]
    def stdProducts = api.find("PX50", 0, api.getMaxFindResultsLimit(), "sku", *filter)?.collectEntries {
        [(it.sku): it]
    }
    batch.each {
        api.global.batch[it] = [
                "stdProductInfo": (stdProducts[it] ?: null),
                "tx"            : false
        ]
        api.global.pricechangebatch[it] = [
                "stdProductInfo": (stdProducts[it] ?: null),
                "tx"            : false
        ]
        api.global.bookingDetailsbatch[it] = [
                "stdProductInfo": (stdProducts[it] ?: null),
                "tx"            : false
        ]
    }
}
return