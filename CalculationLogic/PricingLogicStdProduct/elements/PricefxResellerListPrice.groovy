import net.pricefx.server.dto.calculation.ResultMatrix

List resellerPrices = Constants.RESELLER_LIST
ResultMatrix pricefxResellerListPrice = api.newMatrix(resellerPrices)
pricefxResellerListPrice.addRow(
        [
                out.ResellerListPriceNAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceNAMUSDpfx ?: 0.0,
                (out.ResellerListPriceNAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceNAMUSDpfx ?: 0.0),
                out.ResellerListPriceAPACUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceAPACUSDpfx ?: 0.0,
                (out.ResellerListPriceAPACUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceAPACUSDpfx ?: 0.0),
                out.ResellerListPriceCHINAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceCHINAUSDpfx ?: 0.0,
                (out.ResellerListPriceCHINAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceCHINAUSDpfx ?: 0.0),
                out.ResellerListPriceLATAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceLATAMUSDpfx ?: 0.0,
                (out.ResellerListPriceLATAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceLATAMUSDpfx ?: 0.0),
                out.ResellerListPriceEMEAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceEMEAUSDpfx ?: 0.0,
                (out.ResellerListPriceEMEAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceEMEAUSDpfx ?: 0.0),
                out.ResellerListPriceEMEAUSD2 ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceEMEAUSD2pfx ?: 0.0,
                (out.ResellerListPriceEMEAUSD2 ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceEMEAUSD2pfx ?: 0.0),
                out.ResellerListPriceGlobalUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceGlobalUSDpfx ?: 0.0,
                (out.ResellerListPriceGlobalUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceGlobalUSDpfx ?: 0.0),
                out.ResellerListPriceEMEAEUR ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceEMEAEURpfx ?: 0.0,
                (out.ResellerListPriceEMEAEUR ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceEMEAEURpfx ?: 0.0),
                out.ResellerListPriceEMEAGBP ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceEMEAGBPpfx ?: 0.0,
                (out.ResellerListPriceEMEAGBP ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceEMEAGBPpfx ?: 0.0),
                out.ResellerListPriceAPACCNY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceAPACCNYpfx ?: 0.0,
                (out.ResellerListPriceAPACCNY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceAPACCNYpfx ?: 0.0),
                out.ResellerListPriceAPACAUD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceAPACAUDpfx ?: 0.0,
                (out.ResellerListPriceAPACAUD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceAPACAUDpfx ?: 0.0),
                out.ResellerListPriceAPACJPY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceAPACJPYpfx ?: 0.0,
                (out.ResellerListPriceAPACJPY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPriceAPACJPYpfx ?: 0.0),
        ]
)
return pricefxResellerListPrice