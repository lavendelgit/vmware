Map data = (api.global.pcdsBatch?.values()?.priceChangeInfo)?.groupBy { it?.PartNumber }
List categoryData = (data[out.SKU]?.PriceMonth)?.unique()
List seriesData = [
        [
                name: (Constants.USD_CURRENCY),
                data: data[out.SKU]?.USD
        ],
        [
                name: (Constants.EUR_CURRENCY),
                data: data[out.SKU]?.EUR

        ],
        [

                name: (Constants.GBP_CURRENCY),
                data: data[out.SKU]?.GBP
        ],
        [
                name: (Constants.JPY_CURRENCY),
                data: data[out.SKU]?.JPY
        ],
        [
                name: (Constants.AUD_CURRENCY),
                data: data[out.SKU]?.AUD
        ],
        [
                name: (Constants.CNY_CURRENCY),
                data: data[out.SKU]?.CNY
        ],
        [

                name: (Constants.EMEA_USD_CURRENCY),
                data: data[out.SKU]?.EMEAUSD
        ],
        [

                name: (Constants.EMEA_USD2_CURRENCY),
                data: data[out.SKU]?.EMEAUSD2
        ],
        [
                name: (Constants.REGION_GLOBAL_USD),
                data: data[out.SKU]?.GUSD
        ]
]

def definition = [
        chart      : [
                type: 'line'
        ],
        title      : [
                text: 'Pricing Analysis'
        ],
        subtitle   : [
                text: ''
        ],
        xAxis      : [
                categories: categoryData,
                title     : [
                        text: 'Price Change Month'
                ]
        ],
        yAxis      : [
                title: [
                        text: 'Price'
                ]
        ],
        plotOptions: [
                line: [
                        dataLabels         : [
                                enabled: true
                        ],
                        enableMouseTracking: false
                ]
        ],
        credits    : [
                enabled: false
        ],

        series     : seriesData


]
return api.buildHighchart(definition)