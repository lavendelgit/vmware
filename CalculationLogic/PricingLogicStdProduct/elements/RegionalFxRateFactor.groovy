import net.pricefx.server.dto.calculation.ResultMatrix

List fxRates = Constants.FX_RATE_LIST
ResultMatrix regionalFxRate = api.newMatrix(fxRates)
regionalFxRate.addRow(
        [
                out.NAMUSDCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.NAMUSDCurrencyFxRateFactor ?: 0.0,
                (out.NAMUSDCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.NAMUSDCurrencyFxRateFactor ?: 0.0),
                out.APACUSDCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACUSDCurrencyFxRateFactor ?: 0.0,
                (out.APACUSDCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACUSDCurrencyFxRateFactor ?: 0.0),
                out.CHINAUSDCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.CHINAUSDCurrencyFxRateFactor ?: 0.0,
                (out.CHINAUSDCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.CHINAUSDCurrencyFxRateFactor ?: 0.0),
                out.LATAMUSDCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.LATAMUSDCurrencyFxRateFactor ?: 0.0,
                (out.LATAMUSDCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.LATAMUSDCurrencyFxRateFactor ?: 0.0),
                out.EMEAUSDCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSDCurrencyFxRateFactor ?: 0.0,
                (out.EMEAUSDCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSDCurrencyFxRateFactor ?: 0.0),
                out.EMEAUSD2Cache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSD2CurrencyFxRateFactor ?: 0.0,
                (out.EMEAUSD2Cache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSD2CurrencyFxRateFactor ?: 0.0),
                out.GlobalUSDCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.GlobalUSDCurrencyFxRateFactor ?: 0.0,
                (out.GlobalUSDCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.GlobalUSDCurrencyFxRateFactor ?: 0.0),
                out.EMEAEURCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAEURCurrencyFxRateFactor ?: 0.0,
                (out.EMEAEURCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAEURCurrencyFxRateFactor ?: 0.0),
                out.EMEAGBPCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAGBPCurrencyFxRateFactor ?: 0.0,
                (out.EMEAGBPCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAGBPCurrencyFxRateFactor ?: 0.0),
                out.APACCNYCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACCNYCurrencyFxRateFactor ?: 0.0,
                (out.APACCNYCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACCNYCurrencyFxRateFactor ?: 0.0),
                out.APACAUDCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACAUDCurrencyFxRateFactor ?: 0.0,
                (out.APACAUDCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACAUDCurrencyFxRateFactor ?: 0.0),
                out.APACJPYCache?.CurrencyFxRate ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACJPYCurrencyFxRateFactor ?: 0.0,
                (out.APACJPYCache?.CurrencyFxRate ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACJPYCurrencyFxRateFactor ?: 0.0),
        ]
)
return regionalFxRate
