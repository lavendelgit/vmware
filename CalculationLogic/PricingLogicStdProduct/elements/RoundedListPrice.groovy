return api.attributedResult(libs.vmwareUtil?.PricingHelper?.roundedPrice(out.ListPrice, api.local.roundingType, api.local.precision))
        .withBackgroundColor(Constants.BACKGROUND_EMPTY_DATA)