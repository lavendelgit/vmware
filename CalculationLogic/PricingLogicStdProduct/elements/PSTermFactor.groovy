if (!out.PSTerm && !api.local.basePSTerm || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.PSTerm))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "PS Term",
        Value: api.local.basePSTerm ?: null
]
Map stdSkuAttribute = [
        Name : "PS Term",
        Value: out.PSTerm ?: null
]

exceptionAdjustmentCache = api.global.psTermAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.psTermAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)