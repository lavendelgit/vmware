import net.pricefx.server.dto.calculation.ResultMatrix

List upliftFactors = Constants.UPLIFT_FACTOR_LIST
ResultMatrix regionalUpliftFactor = api.newMatrix(upliftFactors)
regionalUpliftFactor.addRow(
        [
                out.NAMUSDCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.NAMUSDUpliftFactor ?: 0.0,
                (out.NAMUSDCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.NAMUSDUpliftFactor ?: 0.0),
                out.APACUSDCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACUSDUpliftFactor ?: 0.0,
                (out.APACUSDCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACUSDUpliftFactor ?: 0.0),
                out.CHINAUSDCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.CHINAUSDUpliftFactor ?: 0.0,
                (out.CHINAUSDCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.CHINAUSDUpliftFactor ?: 0.0),
                out.LATAMUSDCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.LATAMUSDUpliftFactor ?: 0.0,
                (out.LATAMUSDCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.LATAMUSDUpliftFactor ?: 0.0),
                out.EMEAUSDCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSDUpliftFactor ?: 0.0,
                (out.EMEAUSDCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSDUpliftFactor ?: 0.0),
                out.EMEAUSD2Cache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSD2UpliftFactor ?: 0.0,
                (out.EMEAUSD2Cache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSD2UpliftFactor ?: 0.0),
                out.GlobalUSDCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.GlobalUSDUpliftFactor ?: 0.0,
                (out.GlobalUSDCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.GlobalUSDUpliftFactor ?: 0.0),
                out.EMEAEURCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAEURUpliftFactor ?: 0.0,
                (out.EMEAEURCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAEURUpliftFactor ?: 0.0),
                out.EMEAGBPCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAGBPUpliftFactor ?: 0.0,
                (out.EMEAGBPCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAGBPUpliftFactor ?: 0.0),
                out.APACCNYCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACCNYUpliftFactor ?: 0.0,
                (out.APACCNYCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACCNYUpliftFactor ?: 0.0),
                out.APACAUDCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACAUDUpliftFactor ?: 0.0,
                (out.APACAUDCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACAUDUpliftFactor ?: 0.0),
                out.APACJPYCache?.Uplift ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACJPYUpliftFactor ?: 0.0,
                (out.APACJPYCache?.Uplift ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACJPYUpliftFactor ?: 0.0),
        ]
)
return regionalUpliftFactor
