api.local.priceListRegionCache = [:]
String attribute = ""
Map currencyDetails = [:]
List fields = ["sku", "attribute1"]
List filter = [
        Filter.equal("name", "PriceListRegion"),
        Filter.equal("sku", out.SKU)
]
if (out.PriceListLaunchRegion == "Channel") {
    fields << "attribute2"
    filter << Filter.isNotNull("attribute2")
    attribute = "attribute2"
} else if (out.PriceListLaunchRegion == "Master") {
    fields << "attribute3"
    filter << Filter.isNotNull("attribute3")
    attribute = "attribute3"
}
productPriceListRegionStream = api.stream("PX20", "sku", fields, *filter)
productsPriceListRegionCache = productPriceListRegionStream?.collect { row -> row }
productPriceListRegionStream?.close()
for (standardProduct in productsPriceListRegionCache) {
    currencyDetails = [
            "Currency"       : standardProduct.attribute1,
            "PriceListRegion": standardProduct[attribute]

    ]
    if (api.local.priceListRegionCache[standardProduct.sku]) {
        api.local.priceListRegionCache[standardProduct.sku] << currencyDetails
    } else {
        api.local.priceListRegionCache[standardProduct.sku] = [currencyDetails]
    }
}
return