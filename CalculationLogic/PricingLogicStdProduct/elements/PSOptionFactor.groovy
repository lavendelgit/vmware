if (!out.PSOption && !api.local.basePSOption || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.PSOption))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "PS Option",
        Value: api.local.basePSOption ?: null
]
Map stdSkuAttribute = [
        Name : "PS Option",
        Value: out.PSOption ?: null
]

exceptionAdjustmentCache = api.global.psOptionAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.psOptionAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)