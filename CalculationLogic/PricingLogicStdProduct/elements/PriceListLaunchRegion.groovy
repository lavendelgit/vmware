priceListLaunchRegion = api.option("PriceListLaunchRegion", ["Master", "Channel"])
String defaultValue = "Channel"
parameter = api.getParameter("PriceListLaunchRegion")
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
    parameter.setValue(defaultValue)
}
return priceListLaunchRegion