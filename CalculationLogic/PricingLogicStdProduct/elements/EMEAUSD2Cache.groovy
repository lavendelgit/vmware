upliftCache = api.local.USDupliftCache?.findAll {
    it.value["Price List Region"] == Constants.REGION_EMEA_USD2
}

currencyAdjustmentCache = api.local.USDcurrencyAdjustmentCache?.findAll {
    it.value["Price List Region"] == Constants.REGION_EMEA_USD2
}

return Lib.convertToSpecificCurrenyRegion(api.local.upliftKeyLists, api.local.roundingRulesKeyLists, api.local.productPriceListRegionCache, upliftCache, currencyAdjustmentCache, api.local.USDroundingRulesCache, api.local.stdSkuDetails, out.SKU, Constants.REGION_EMEA_USD2, Constants.USD_CURRENCY)