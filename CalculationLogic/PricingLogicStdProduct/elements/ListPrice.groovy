BigDecimal listPrice = (out.BasePrice * out.CurrencyFactor * out.PriceListRegionFactor * out.PromotionFactor * out.VolumeTierFactor * out.DatacenterFactor * out.PurchasingProgramFactor *
        out.SegmentFactor * out.SupportTypeFactor * out.SupportTierFactor * out.TermFactor * out.TermUoMFactor * out.PaymentFactor * out.MetricFactor * out.QuantityAdjustment * out.UpgradeFactor * out.ProgramOptionFactor * out.HostingFactor * out.OSFactor * out.RetentionPeriodFactor * out.PSTermFactor * out.PSOptionFactor) as BigDecimal
if (out.PaymentCycleAdjustmentFactor)
    listPrice = listPrice / out.PaymentCycleAdjustmentFactor as BigDecimal
return api.attributedResult(listPrice)
        .withBackgroundColor(Constants.BACKGROUND_EMPTY_DATA)
