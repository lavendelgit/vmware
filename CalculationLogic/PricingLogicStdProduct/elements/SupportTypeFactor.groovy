if (!out.SupportType && !api.local.baseSupportType || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.SupportType))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Support Type",
        Value: api.local.baseSupportType ?: null
]
Map stdSkuAttribute = [
        Name : "Support Type",
        Value: out.SupportType ?: null
]
exceptionAdjustmentCache = api.global.supportTypeAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.supportTypeAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)