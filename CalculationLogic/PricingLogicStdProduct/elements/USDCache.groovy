String currency = "USD"
api.local.USDupliftCache = api.local.upliftCache?.findAll {
    it.value.Currency == currency
}
api.local.USDcurrencyAdjustmentCache = api.local.currencyAdjustmentCache?.findAll {
    it.value.Currency == currency
}
api.local.USDroundingRulesCache = api.local.roundingRulesCache?.findAll {
    it.value.Currency == currency || it.value.Currency == "*"
}
return