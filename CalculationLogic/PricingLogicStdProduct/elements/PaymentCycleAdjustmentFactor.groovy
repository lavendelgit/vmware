String term = out.Term
String paymentType = out.PaymentType
BigDecimal factor = 1

if (paymentType?.toLowerCase() == "annual") {
    factor = (out.Term as BigDecimal) / 12 as BigDecimal
} else if (paymentType?.toLowerCase() == "monthly") {
    factor = term as BigDecimal
}

return api.attributedResult(factor as BigDecimal)
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)