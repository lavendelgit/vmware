BigDecimal baseQty = api.local.baseQuantity as BigDecimal
BigDecimal skuQty = out.Quantity as BigDecimal
if (skuQty && baseQty) {
    return api.attributedResult(skuQty / baseQty)
            .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)
}
return api.attributedResult(1)
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)