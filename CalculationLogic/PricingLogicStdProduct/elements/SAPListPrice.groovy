BigDecimal sapListPrice = out.BasePrice

out.CacheLPGSequence?.each { key, value ->
    attributeKey = key.replaceAll("\\s", "") + "Factor"
    if (out[attributeKey] != null) {
        if (value.Operation == Constants.MULTIPLY) {
            sapListPrice = (sapListPrice * out[attributeKey]) as BigDecimal
            sapListPrice = libs.vmwareUtil.PricingHelper.roundedPrice(sapListPrice, api.local.roundingType, api.local.precision)
        }

        if (value.Operation == Constants.DIVIDE) {
            sapListPrice = (sapListPrice / out[attributeKey]) as BigDecimal
            sapListPrice = libs.vmwareUtil.PricingHelper.roundedPrice(sapListPrice, api.local.roundingType, api.local.precision)
        }
    }
}
return sapListPrice

