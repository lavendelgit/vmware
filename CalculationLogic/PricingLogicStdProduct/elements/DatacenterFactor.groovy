if (!out.DataCenter && !api.local.baseDataCenter || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.DataCenter))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Data Center",
        Value: api.local.baseDataCenter ?: null
]
Map stdSkuAttribute = [
        Name : "Data Center",
        Value: out.DataCenter ?: null
]

Map exceptionAdjustmentCache = api.global.dataCenterAdjustmentCache?.findAll {
    ( it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product ) || ( it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*" )
}

Map defaultAdjustmentCache = api.global.dataCenterAdjustmentCache?.findAll {
    ( it.value["Product Group"] == "*" && it.value["Product"] == out.Product ) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}

return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)