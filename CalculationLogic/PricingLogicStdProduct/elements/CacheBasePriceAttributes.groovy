if (!api.global.baseSKUCache) {
    List filter = [
            Filter.equal("name", "StandardBasePrice"),
            Filter.lessOrEqual("attribute29", out.PriceListDate),
    ]
    stdAdjustmentStream = api.stream("PX50", "sku,attribute29", *filter)
    api.global.baseSKUCache = stdAdjustmentStream?.collectEntries { [(it.attribute1): it] }
    stdAdjustmentStream?.close()
}
return
