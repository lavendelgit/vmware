api.global.pcdsBatch = api.global.pcdsBatch ?: [:]
if (api.global.pricechangebatch[api.local.pid].tx == false) {
    String dataMart = api.global.lpgInput?.(libs._dto.dsHistoricalPriceList.PRICE_CHANGE_DM)
    Integer yearInput = api.global.lpgInput?.(libs._dto.dsHistoricalPriceList.PRICE_CHANGE_TRANS_YR)
    Date date = new Date()
    String endDate = date.format(libs._dto.dsHistoricalPriceList.YYYY_MM_DD)
    String startDate = libs._dto._generic.getEndDate(date, yearInput)
    List filters = []
    filterMap = api.global.lpgInput?.(libs._dto.dsHistoricalPriceList.PRICE_CHANGE_DATA_FILTER)
    filter = api.filterFromMap(filterMap)
    if (filter) {
        filters << filters
    }
    filters << Filter.greaterOrEqual(libs._dto.dsHistoricalPriceList.END_DATE, startDate)
    filters << Filter.lessOrEqual(libs._dto.dsHistoricalPriceList.PRICE_CHANGE_EFF_DT, endDate)
    filters << Filter.in(libs._dto.dsHistoricalPriceList.PART_NUMBER, api.global.pricechangebatch.keySet())
    List queryColumns = []
    List priceChangeDmConfig = api.findLookupTableValues(Constants.PP_INLINE_ANALYTICS_CONFIG)
    if ((priceChangeDmConfig?.key1)?.contains(dataMart)) {
        queryColumns = priceChangeDmConfig?.findAll { it.key1 == dataMart && it.attribute1 == (Constants.PP_CONFIG_INCLUSION_FLAG_VALUE) }?.key2
    } else {
        api.addWarning(Constants.WARNING_MESSAGE)
    }
    String orderByField = libs._dto.dsHistoricalPriceList.PRICE_CHANGE_EFF_DT
    Matrix2D priceChange = libs._dto.dsHistoricalPriceList.getData(false, dataMart, filters, false, queryColumns, orderByField)
    priceChange?.each {
        api.global.pcdsBatch[it?.PartNumber + it?.PriceMonth] = [
                (Constants.PRICE_CHANGE_INFO): (it ?: null)
        ]
    }
    api.global.pricechangebatch.keySet().each {
        api.global.pricechangebatch[it].tx = true
    }
}
return