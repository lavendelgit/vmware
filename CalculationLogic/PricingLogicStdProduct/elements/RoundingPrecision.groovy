def keyLists = Lib.getAdjustmentsFromCache(api.local.roundingRulesCache)
Map roundingRule = [:]
List roundingKeysList = []
List uniqueKeys = keyLists?.StdKeys?.unique()
for (keyList in uniqueKeys) {
    roundingKeysList = keyList.values().drop(3)
    roundingRule = Lib.getPrecision(api.local.roundingRulesCache, api.local.skuProductGroup, api.local.skuProduct, api.local.skuOffer, api.local.skuBase, roundingKeysList)
    if (roundingRule?.RoundingType) {
        break
    }
}
api.local.precision = (roundingRule?.Precision ?: 2) as Integer
api.local.roundingType = roundingRule?.RoundingType ?: "Round"
return