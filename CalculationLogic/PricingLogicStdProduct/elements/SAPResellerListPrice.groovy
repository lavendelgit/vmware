import net.pricefx.server.dto.calculation.ResultMatrix

List sapResellerPrices = Constants.SAP_RESELLER_LIST_PRICE_LIST
ResultMatrix sapResellerListPrice = api.newMatrix(sapResellerPrices)
sapResellerListPrice.addRow(
        [
                out.ResellerListPriceSAPNAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_NAM_USD_pfx ?: 0.0,
                (out.ResellerListPriceSAPNAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_NAM_USD_pfx ?: 0.0),
                out.ResellerListPriceSAPAPACUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_APAC_USD_pfx ?: 0.0,
                (out.ResellerListPriceSAPAPACUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_APAC_USD_pfx ?: 0.0),
                out.ResellerListPriceSAPCHINAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_CHINA_USD_pfx ?: 0.0,
                (out.ResellerListPriceSAPCHINAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_CHINA_USD_pfx ?: 0.0),
                out.ResellerListPriceSAPLATAMUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_LATAM_USD_pfx ?: 0.0,
                (out.ResellerListPriceSAPLATAMUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_LATAM_USD_pfx ?: 0.0),
                out.ResellerListPriceSAPEMEAUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_EMEA_USD_pfx ?: 0.0,
                (out.ResellerListPriceSAPEMEAUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_EMEA_USD_pfx ?: 0.0),
                out.ResellerListPriceSAPEMEAUSD2 ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_EMEA_USD2_pfx ?: 0.0,
                (out.ResellerListPriceSAPEMEAUSD2 ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_EMEA_USD2_pfx ?: 0.0),
                out.ResellerListPriceSAPGlobalUSD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_Global_USD_pfx ?: 0.0,
                (out.ResellerListPriceSAPGlobalUSD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_Global_USD_pfx ?: 0.0),
                out.ResellerListPriceSAPEMEAEUR ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_EMEA_EUR_pfx ?: 0.0,
                (out.ResellerListPriceSAPEMEAEUR ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_EMEA_EUR_pfx ?: 0.0),
                out.ResellerListPriceSAPEMEAGBP ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_EMEA_GBP_pfx ?: 0.0,
                (out.ResellerListPriceSAPEMEAGBP ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_EMEA_GBP_pfx ?: 0.0),
                out.ResellerListPriceSAPAPACCNY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_APAC_CNY_pfx ?: 0.0,
                (out.ResellerListPriceSAPAPACCNY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_APAC_CNY_pfx ?: 0.0),
                out.ResellerListPriceSAPAPACAUD ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_APAC_AUD_pfx ?: 0.0,
                (out.ResellerListPriceSAPAPACAUD ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_APAC_AUD_pfx ?: 0.0),
                out.ResellerListPriceSAPAPACJPY ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_APAC_JPY_pfx ?: 0.0,
                (out.ResellerListPriceSAPAPACJPY ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.ResellerListPrice_SAP_APAC_JPY_pfx ?: 0.0),
        ]
)
return sapResellerListPrice