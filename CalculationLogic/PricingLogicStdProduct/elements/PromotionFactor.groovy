if (!out.Promotion && !api.local.basePromotion || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Promotion))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Promotion",
        Value: api.local.basePromotion ?: null
]
Map stdSkuAttribute = [
        Name : "Promotion",
        Value: out.Promotion ?: null
]
exceptionAdjustmentCache = api.global.promotionAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.promotionAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)