import net.pricefx.server.dto.calculation.ResultMatrix

List localCurrencyListPrices = Constants.LOCAL_CURRENCY_LIST
ResultMatrix priceFxLocalCurrencyListPrice = api.newMatrix(localCurrencyListPrices)
priceFxLocalCurrencyListPrice.addRow(
        [
                out.NAMUSDCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.NAMUSDListPrice ?: 0.0,
                (out.NAMUSDCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.NAMUSDListPrice ?: 0.0),
                out.APACUSDCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACUSDListPrice ?: 0.0,
                (out.APACUSDCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACUSDListPrice ?: 0.0),
                out.CHINAUSDCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.CHINAUSDListPrice ?: 0.0,
                (out.CHINAUSDCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.CHINAUSDListPrice ?: 0.0),
                out.LATAMUSDCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.LATAMUSDListPrice ?: 0.0,
                (out.LATAMUSDCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.LATAMUSDListPrice ?: 0.0),
                out.EMEAUSDCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSDListPrice ?: 0.0,
                (out.EMEAUSDCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSDListPrice ?: 0.0),
                out.EMEAUSD2Cache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSD2ListPrice ?: 0.0,
                (out.EMEAUSD2Cache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAUSD2ListPrice ?: 0.0),
                out.GlobalUSDCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.GlobalUSDListPrice ?: 0.0,
                (out.GlobalUSDCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.GlobalUSDListPrice ?: 0.0),
                out.EMEAEURCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAEURListPrice ?: 0.0,
                (out.EMEAEURCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAEURListPrice ?: 0.0),
                out.EMEAGBPCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAGBPListPrice ?: 0.0,
                (out.EMEAGBPCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.EMEAGBPListPrice ?: 0.0),
                out.APACCNYCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACCNYListPrice ?: 0.0,
                (out.APACCNYCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACCNYListPrice ?: 0.0),
                out.APACAUDCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACAUDListPrice ?: 0.0,
                (out.APACAUDCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACAUDListPrice ?: 0.0),
                out.APACJPYCache?.RoundedPrice ?: 0.0,
                api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACJPYListPrice ?: 0.0,
                (out.APACJPYCache?.RoundedPrice ?: 0.0) - (api.global.dsBatch[out.SKU]?.pricefxPriceBookInfo?.APACJPYListPrice ?: 0.0)
        ]
)
return priceFxLocalCurrencyListPrice