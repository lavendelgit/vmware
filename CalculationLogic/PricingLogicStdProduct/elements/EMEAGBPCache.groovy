upliftCache = api.local.upliftCache?.findAll {
    it.value.Currency == Constants.GBP_CURRENCY && it.value["Price List Region"] == Constants.REGION_EMEA_GBP
}
currencyAdjustmentCache = api.local.currencyAdjustmentCache?.findAll {
    it.value.Currency == Constants.GBP_CURRENCY && it.value["Price List Region"] == Constants.REGION_EMEA_GBP
}
roundingRulesCache = api.local.roundingRulesCache?.findAll {
    it.value.Currency == Constants.GBP_CURRENCY || it.value.Currency == "*"
}
return Lib.convertToSpecificCurrenyRegion(api.local.upliftKeyLists, api.local.roundingRulesKeyLists, api.local.productPriceListRegionCache, upliftCache, currencyAdjustmentCache, roundingRulesCache, api.local.stdSkuDetails, out.SKU, Constants.REGION_EMEA_GBP, Constants.GBP_CURRENCY)