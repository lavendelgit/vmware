if (!api.global.stdAdjustmentsCache) {
    List filter = [
        Filter.equal("name", "StandardAdjustment"),
        Filter.lessOrEqual("attribute41", out.PriceListDate),

    ]
    stdAdjustmentStream = api.stream("PX50", "sku,attribute41", *filter)
    api.global.stdAdjustmentsCache = stdAdjustmentStream?.collect { row -> row }
    stdAdjustmentStream?.close()
}
return
