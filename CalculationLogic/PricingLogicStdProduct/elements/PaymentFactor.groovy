if (!out.PaymentType && !api.local.basePaymentType || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.PaymentType))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Payment",
        Value: api.local.basePaymentType ?: null
]
Map stdSkuAttribute = [
        Name : "Payment",
        Value: out.PaymentType ?: null
]
exceptionAdjustmentCache = api.global.paymentAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.paymentAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)