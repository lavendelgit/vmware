if (!out.PurchasingProgram && !api.local.basePurchasingProgram || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.PurchasingProgram))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Purchasing Program",
        Value: api.local.basePurchasingProgram ?: null
]
Map stdSkuAttribute = [
        Name : "Purchasing Program",
        Value: out.PurchasingProgram ?: null
]
exceptionAdjustmentCache = api.global.purchasingProgramAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.purchasingProgramAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)