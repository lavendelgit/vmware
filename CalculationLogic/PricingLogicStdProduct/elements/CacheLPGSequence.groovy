if (!api.global.lpSequence) {
    api.global.lpSequence = [:]
    List filters = [Filter.equal(Constants.INCLUSION_FLAG_COLUMN, "Yes")]
    api.global.lpSequence = api.findLookupTableValues(Constants.LPG_SEQUENCE_PP_TABLE, *filters)?.sort { it.name }.collectEntries { sequence ->

        [(sequence[Constants.ATTRIBUTE_NAME_COLUMN]): ["Operation": sequence[Constants.OPERATION_COLUMN]]]

    }
}
return