if (!out.Currency && !api.local.baseCurrency || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Currency))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Currency",
        Value: api.local.baseCurrency ?: null
]
Map stdSkuAttribute = [
        Name : "Currency",
        Value: out.Currency ?: null
]
Map exceptionAdjustmentCache = api.global.currencyAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}
Map defaultAdjustmentCache = api.global.currencyAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
