if (!out.ProgramOption && !api.local.baseProgramOption || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.ProgramOption))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "ProgramOption",
        Value: api.local.baseProgramOption ?: null
]
Map stdSkuAttribute = [
        Name : "ProgramOption",
        Value: out.ProgramOption ?: null
]
exceptionAdjustmentCache = api.global.programOptionAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}

defaultAdjustmentCache = api.global.programOptionAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)