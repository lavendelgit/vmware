if (!out.OS && !api.local.baseOS || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.OS))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "OS",
        Value: api.local.baseOS ?: null
]
Map stdSkuAttribute = [
        Name : "OS",
        Value: out.OS ?: null
]
Map exceptionAdjustmentCache = api.global.OSAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}
Map defaultAdjustmentCache = api.global.OSAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)
