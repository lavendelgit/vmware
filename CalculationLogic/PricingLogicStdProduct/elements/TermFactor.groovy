if (!out.Term && !api.local.baseTerm || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Term))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Term",
        Value: api.local.baseTerm ?: null
]
Map stdSkuAttribute = [
        Name : "Term",
        Value: out.Term ?: null
]
exceptionAdjustmentCache = api.global.termAdjustmentCache?.findAll {
    (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == out.Product) || (it.value["Product Group"] == out.ProductGroup && it.value["Product"] == "*")
}
defaultAdjustmentCache = api.global.termAdjustmentCache?.findAll {
    (it.value["Product Group"] == "*" && it.value["Product"] == out.Product) || (it.value["Product Group"] == "*" && it.value["Product"] == "*")
}
return api.attributedResult(Lib.getAdjustmentsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute))
        .withBackgroundColor(Constants.BACKGROUND_COLOR_FACTOR)