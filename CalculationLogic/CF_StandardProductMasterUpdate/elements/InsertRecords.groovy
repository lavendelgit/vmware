Map stdRecord = [:]
Integer shortDescLen = out.SKUConfigCache?.ShortDescLen as Integer
shortDescLen = shortDescLen ?: Constants.SHORT_DESC_DEFAULT_LEN
api.local.skuInfo.each {
    stdRecord.sku = it.key
    shortDesc = it.value.ShortDesc
    shortDescTrimmed = it.value.ShortDesc
    if (shortDesc.length() > shortDescLen) {
        shortDescTrimmed = shortDesc?.substring(0, shortDescLen)
    }
    stdRecord.label = shortDescTrimmed
    stdRecord.attribute1 = it.value.Product
    stdRecord.attribute2 = it.value.Offer
    stdRecord.attribute5 = it.value.Base
    stdRecord.attribute8 = it.value.ItemType

    if (stdRecord) api.addOrUpdate("P", stdRecord)
}
return

