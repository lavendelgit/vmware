api.local.skuInfo = getSKUInfo()
return

Map getSKUInfo() {
    Map skuInfos = [:]
    String extensionName = Constants.PX_TABLE_STANDARDPRODUCTS
    List filter = [
            Filter.equal(Constants.PX_TABLE_STANDARDPRODUCTS_NAME, extensionName),
            Filter.notIn(Constants.PX_TABLE_STANDARDPRODUCTS_SKUTYPE, [Constants.PX_TABLE_STDPRODS_SKUTYPE_BUNDLE, Constants.PX_TABLE_STDPRODS_SKUTYPE_UPGRADESKU, Constants.PX_TABLE_STDPRODS_SKUTYPE_UPGRADEBASESKU]),
            Filter.greaterThan(Constants.PX_TABLE_STDPRODS_LASTUPDATEDATE, out.CFLastUpdateDateTime)
    ]
    def skuStream = api.stream("PX50", Constants.PX_TABLE_STDPRODS_SKU, [Constants.PX_TABLE_STDPRODS_SKU, Constants.PX_TABLE_STDPRODS_PRODUCT, Constants.PX_TABLE_STDPRODS_OFFER, Constants.PX_TABLE_STDPRODS_BASE], *filter)
    skuInfos = skuStream?.collectEntries {
        skuInfo ->
            [(skuInfo.sku): [
                    (Constants.SHORT_DESC): "",
                    (Constants.ITEM_TYPE) : Constants.STANDARD_WATERFALL,
                    (Constants.PRODUCT)   : skuInfo.attribute1,
                    (Constants.OFFER)     : skuInfo.attribute2,
                    (Constants.BASE)      : skuInfo.attribute4,
            ]]
    }
    skuStream?.close()
    return skuInfos
}

