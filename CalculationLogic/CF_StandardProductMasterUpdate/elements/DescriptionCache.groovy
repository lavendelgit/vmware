skuList = api.local.skuInfo?.collect { it.key } as List
filter = [Filter.greaterThan(Constants.PP_TABLE_STDDESC_LASTUPDATEDATE, out.CFLastUpdateDateTime),
          Filter.in(Constants.KEY_NAME, skuList)]
shortDescription = api.findLookupTableValues(Constants.PP_TABLE_STANDARDDESCRIPTION, *filter)?.collectEntries { [(it.name): it[Constants.PP_TABLE_SHORTDESCRIPTION]] }
skuList?.each { sku ->
    api.local.skuInfo[sku]?.ShortDesc = shortDescription?.getAt(sku) ?: ""
}
return

