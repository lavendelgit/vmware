List filter = [Filter.equal(Constants.CF_STATUS, Constants.CF_STATUS_VALUE),
               Filter.equal(Constants.CF_UNIQUE_NAME,Constants.STANDARD_PRODUCT_MASTER_UPDATE_LABEL)]
String lastUpdatedDate = api.find("CF",0,"-lastUpdateDate", *filter)?.collect { it.lastUpdateDate }.getAt(0)
return lastUpdatedDate

