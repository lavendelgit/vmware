/* 
Rounds number specified in input parameter to the standard VMWare rounding logic DesktopV2
EXAMPLE OF USAGE
----------------------------------------
def LC = api.getElement("LCFactor")
def LCprice = listPrice * rate
def threshHold = 50
def roundToThresholdMet = 1.00
def roundToThresholdNotMet = 0.10
def substract = 0.05

if (LC == "LC-77") {
  	return libs.vmwareUtil.RoundingLogicDesktopV2.applyRoundingLogic(LCprice, threshHold, substract, roundToThresholdMet, roundToThresholdNotMet)?.toBigDecimal();
} else { 
  	return libs.vmwareUtil.RoundingLogicV2.applyRoundingLogic(LCprice)?.toBigDecimal();
}

*/

def applyRoundingLogic(number, threshold, substract, roundToThresholdMet, roundToThresholdNotMet) {
    int numberOfDecPlaces
    if (number < threshold) {

        numberOfDecPlaces = (int) Math.log10(roundToThresholdNotMet) * (-1)
        return number.setScale(numberOfDecPlaces, BigDecimal.ROUND_UP)

    } else {

        numberOfDecPlaces = (int) Math.log10(roundToThresholdMet) * (-1)
        return (number.setScale(numberOfDecPlaces, BigDecimal.ROUND_UP)) - substract
    }
}