//TBC
List getAdjustments(Map adjustmentCache, String product, String offer, String base, List adjs, boolean getCombination = false) {
    convertedAdjusment = libs.vmwareUtil.PricingHelper.standardKeyConversion(adjustmentCache)
    Map sortedCombinations = getSortedCombinations(product, offer, base, adjs)
    BigDecimal adjustment = 0.0
    String key
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        adjustment = convertedAdjusment?.getAt(key.toLowerCase())?.Adjustment
        if (adjustment && !getCombination) {
            return adjustment
        } else if (adjustment && getCombination) {
            return [adjustment, combination.key, key]
        }
    }
}

Map getSortedCombinations(String product, String offer, String base, List adjs) {
    List combinations = []
    combinations << [product, "*"]
    combinations << [offer, "*"]
    combinations << [base, "*"]
    for (adj in adjs) {
        combinations << [adj, "*"]
    }
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ libs.vmwareUtil.CacheManager.getWeightage(it, "*") })?.sort { -it.key }
}

def getDistiDiscount(Map discountCombination, String product, String base, String offer) {
    def distiDiscount
    List adjusment = null
    Map sortedCombinations = getSortedCombinations(product, base, offer, adjusment)
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        distiDiscount = discountCombination?.get(key)
        if (distiDiscount) {
            return distiDiscount
        }
    }
    return null
}

Map getAdjustmentsFromCache(Map adjustmentsCache) {
    Map keyLists = [:]
    List baseKeyLists = []
    List skuKeyLists = []
    Map keys = [:]
    Map baseKeys = [:]
    adjustmentsCache?.each {
        adj ->
            keys = [:]
            baseKeys = [:]
            adj.value.Keys?.each {
                key ->
                    key = key.replaceAll("\\s", "")
                    if (out[key])
                        keys[key] = out[key]
                    if (api.local["base" + key])
                        baseKeys[key] = api.local["base" + key]
            }
            baseKeyLists << baseKeys
            skuKeyLists << keys
    }
    skuKeyLists = skuKeyLists?.minus(["Product", "Offer", "Base"])
    baseKeyLists = baseKeyLists?.minus(["Product", "Offer", "Base"])
    keyLists.StdKeys = skuKeyLists
    keyLists.BaseKeys = baseKeyLists
    return keyLists

}


Map populateTermAltUoMAdjustments(Map adjustmentCache, Map termUoMConversionCache) {
    String term
    String termUoM
    Map populatedTermCache = [:]
    Map destTerm = [:]
    adjustmentCache?.findAll { it.value.Term != null && it.value["Term UoM"] != null }?.each {
        term = it.value.Term
        termUoM = it.value["Term UoM"]
        destTerm = termUoMConversionCache[term + "_" + termUoM]
        if (destTerm) {
            it.value.Term = destTerm.DestTerm
            it.value["Term UoM"] = destTerm.DestUoM
            key = frameTermKeys(it.value)
            populatedTermCache[key] = it.value
        }
    }
    return populatedTermCache
}

String frameTermKeys(Map termAdj) {
    String keyString
    List keys = termAdj?.Keys
    keys?.each {
        key ->
            keyString = keyString ? keyString + "_" + termAdj[key] : termAdj[key]
    }
    return keyString
}

Map populateTermUoMAdjustments(Map factorAdjustmentCache) {
    Map populatedTermCache = [:]
    Map tempAdjustmentCache = [:]
    if (factorAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
        factorAdjustmentCache.each { key, value -> tempAdjustmentCache[key] = value.clone() }
        populatedTermCache = populateTermAltUoMAdjustments(tempAdjustmentCache)
        if (populatedTermCache?.size() > 0 && factorAdjustmentCache?.size() > 0) {
            factorAdjustmentCache = factorAdjustmentCache + populatedTermCache
        }
    }
    return factorAdjustmentCache
}


Map cacheAdjustments(List stdAdjustmentsCache, Map metaData, String factorAttribute, String typeAttribute = null, boolean isRoundingRule = false) {
    Map adjustmentCache = [:]
    List adjustments = stdAdjustmentsCache?.findAll { it[factorAttribute] != null }
    List keys
    Integer termIndex = 0
    excludeList = ["version", "typedId", "name", "createDate", "createdBy", "lastUpdateDate", "lastUpdateBy", factorAttribute, "attribute42", "attribute41", "id"]
    if (isRoundingRule) {
        excludeList << typeAttribute
    }
    adjustments?.each {
        keys = it.keySet().collect()
        keysList = keys?.minus(excludeList)
        termIndex = keysList?.indexOf("attribute7")
        if (termIndex != -1) {
            keysList = keysList?.minus(["attribute33"])
            keysList = keysList.plus(termIndex + 1, "attribute33")
        }
        if (isRoundingRule && (it["attribute4"] || it["attribute23"])) {
            keysList = keysList?.minus(["attribute4", "attribute23"])
            keysList = keysList.plus(["attribute4", "attribute23"])
        }
        keyNames = []
        keysList?.each {
            key ->
                keyNames << metaData[key]
        }
        keyValueList = []
        Map record = [:]
        keysList?.each {
            key ->
                if (it[key]) {
                    keyValueList << it[key]
                    record[metaData[key]] = it[key]
                }
        }
        key = libs.vmwareUtil.util.frameKey(keyValueList)
        if (isRoundingRule) {
            record.Precision = it[factorAttribute]
            record.Type = it[typeAttribute]
            record.Keys = keyNames
            adjustmentCache[key] = record
        } else {
            record.Adjustment = it[factorAttribute]
            record.Keys = keyNames
            adjustmentCache[key] = record
        }
    }
    adjustmentCache = adjustmentCache?.sort { -it.value?.Keys?.size() }
    return adjustmentCache
}

Map standardKeyConversion(Map keyAdjusments) {
    Map convertedAdjusments = [:]
    keyAdjusments?.each { key, value ->
        convertedAdjusments[key.toLowerCase()] = value
    }
    return convertedAdjusments
}


Map getSortedCombinations(String sku, String product, String offer, String base) {
    List combinations = []
    combinations << [sku, "*"]
    combinations << [product, "*"]
    combinations << [offer, "*"]
    combinations << [base, "*"]
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ libs.vmwareUtil.CacheManager.getWeightage(it, "*") })?.sort { -it.key }
}


BigDecimal getAdjustments(Map adjustmentCache, String sku, String product, String offer, String base) {

    convertedAdjusment = standardKeyConversion(adjustmentCache)
    Map sortedCombinations = getSortedCombinations(sku, product, offer, base)
    BigDecimal adjustment = 0.0
    String key
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        adjustment = convertedAdjusment?.getAt(key.toLowerCase())?.Adjustment
        if (adjustment)
            return adjustment
    }
}

Map getPrecision(Map rulesCache, String sku, String product, String offer, String base, List adjs) {
    String key
    Map rounding = [:]
    if (adjs) {
        sortedCombinations = getSortedCombinations(sku, product, offer, base, adjs)
    } else {
        sortedCombinations = getSortedCombinations(sku, product, offer, base)
    }

    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        rounding.Precision = rulesCache?.getAt(key)?.Precision as Integer
        rounding.RoundingType = rulesCache?.getAt(key)?.Type
        if (rounding.RoundingType)
            return rounding
    }
}

Map getSortedCombinations(String sku, String product, String offer, String base, List adjs) {
    List combinations = []
    combinations << [sku, "*"]
    combinations << [product, "*"]
    combinations << [offer, "*"]
    combinations << [base, "*"]
    for (adj in adjs) {
        combinations << [adj, "*"]
    }
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ libs.vmwareUtil.CacheManager.getWeightage(it, "*") })?.sort { -it.key }
}

BigDecimal roundedPrice(BigDecimal listPrice, String roundingType, Integer precision) {
    BigDecimal roundedPrice = listPrice
    switch (roundingType) {
        case "RoundUp":
            roundedPrice = listPrice?.setScale(precision, BigDecimal.ROUND_UP)
            break
        case "RoundDown":
            roundedPrice = listPrice?.setScale(precision, BigDecimal.ROUND_DOWN)
            break
        case "Round":
            roundedPrice = listPrice?.setScale(precision, BigDecimal.ROUND_HALF_UP)
            break

        case "Custom":
            roundedPrice = listPrice
            if (listPrice > 1000) {
                absListPrice = (Math.log10(Math.abs(listPrice))) as Integer
                roundUpPrecision = 3 - (1 + absListPrice)
                roundedUpPrice = listPrice?.setScale(roundUpPrecision, BigDecimal.ROUND_UP) as Integer
                roundedPrice = roundedUpPrice
            } else if (listPrice > 500) {
                roundedPrice = 5 * (Math.ceil(Math.abs(listPrice / 5)))
            } else if (listPrice > 50) {
                roundedPrice = 0.5 * (Math.ceil(Math.abs(listPrice / 0.5)))
            } else if (listPrice > 10) {
                roundedPrice = 0.05 * (Math.ceil(Math.abs(listPrice / 0.05)))
                roundedPrice = roundedPrice?.setScale(2, BigDecimal.ROUND_HALF_UP)
            }
            break

        case "Custom2":
            roundedPrice = listPrice
            if (listPrice < 100) {
                roundedPrice = listPrice?.setScale(2, BigDecimal.ROUND_HALF_UP)
            } else if (listPrice < 1000) {
                roundedPrice = Math.round(listPrice)
            } else if (listPrice < 10000) {
                Integer integerPrice = listPrice
                reminder = integerPrice % 5
                if (reminder > 5) {
                    roundedPrice = 5 * (Math.ceil(Math.abs(listPrice / 5)))
                } else {
                    if (reminder >= 2 && reminder != 0 && reminder != 1) {
                        roundedPrice = 5 * (Math.ceil(Math.abs(listPrice / 5)))
                    } else if (reminder <= 3 && reminder != 0 && reminder != 1) {
                        roundedPrice = 5 * (Math.floor(Math.abs(listPrice / 5)))
                    } else if (reminder == 0 || reminder == 1) {
                        roundedPriceHigh = 5 * (Math.ceil(Math.abs(listPrice / 5)))
                        roundedPriceLow = 5 * (Math.floor(Math.abs(listPrice / 5)))
                        diffrence = Math.round(roundedPriceHigh - listPrice)
                        if (diffrence < 25 && diffrence > 5) {
                            roundedPrice = 5 * (Math.ceil(Math.abs(listPrice / 5)))
                        } else if (diffrence >= 25 || diffrence <= 5) {
                            roundedPrice = 5 * (Math.floor(Math.abs(listPrice / 5)))
                        }
                    }
                }
            } else if (listPrice > 10000) {
                Integer integerPrice = listPrice
                reminder = integerPrice % 50
                if (reminder > 50) {
                    roundedPrice = 50 * (Math.ceil(Math.abs(listPrice / 50)))
                } else {
                    if (reminder > 25 && reminder != 0 && reminder != 10) {
                        roundedPrice = 50 * (Math.ceil(Math.abs(listPrice / 50)))
                    } else if (reminder <= 25 && reminder != 0 && reminder != 10) {
                        roundedPrice = 50 * (Math.floor(Math.abs(listPrice / 50)))
                    } else if (reminder == 0 || reminder == 10) {
                        roundedPriceHigh = 50 * (Math.ceil(Math.abs(listPrice / 50)))
                        roundedPriceLow = 50 * (Math.floor(Math.abs(listPrice / 50)))
                        diffrence = Math.round(roundedPriceHigh - listPrice)
                        if (diffrence < 25 && diffrence >= 5) {
                            roundedPrice = 50 * (Math.ceil(Math.abs(listPrice / 50)))
                        } else if (diffrence >= 25 || diffrence < 5) {
                            roundedPrice = 50 * (Math.floor(Math.abs(listPrice / 50)))
                        }
                    }
                }
            }
            break
    }
    return roundedPrice
}

Map getAdjustmentsFromCache(Map adjustmentsCache, Map skuDetails) {
    Map keyLists = [:]
    List baseKeyLists = []
    Map keys = [:]
    Map baseKeys = [:]
    adjustmentsCache?.each {
        adj ->
            keys = [:]
            baseKeys = [:]
            adj.value.Keys?.each {
                key ->
                    key = key != "Term UoM" ? key?.replaceAll("\\s", "") : key
                    if (skuDetails[out.BasePriceMetaData[key]])
                        baseKeys[key] = skuDetails[out.BasePriceMetaData[key]]
            }
            baseKeyLists << baseKeys

    }
    baseKeyLists = baseKeyLists?.minus(["Product", "Offer", "Base"])
    keyLists.BaseKeys = baseKeyLists
    return keyLists

}
