//Rounds number in input parameter according to VMWare rounding logic V2

def applyRoundingLogic(number) {
    if (number <= 10) return number.setScale(2, BigDecimal.ROUND_DOWN)
    def log = Math.log10(Math.abs(number))
    int roundedDown = Math.floor(log)
    def digitsRoundUpTo = Math.pow(10, 3 - (roundedDown + 1))
    def a

    if (digitsRoundUpTo != 0) {
        int res = Math.ceil(number * digitsRoundUpTo)
        a = res / digitsRoundUpTo
    } else {
        a = Math.ceil(number)
    }

    BigDecimal res1 = Math.pow(10, (roundedDown - 3)) * 5
    def b = res1.setScale(2, BigDecimal.ROUND_HALF_EVEN)
    return a - b
}