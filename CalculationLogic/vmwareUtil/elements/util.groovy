def getExchangeRate(currency, term) {
    def currencyFilter = Filter.equal("key1", currency)
    def termFilter = Filter.equal("key2", term)
    def fxRate = api.findLookupTableValues("CurrencyExchange", currencyFilter, termFilter).attribute1[0]
    if (fxRate != null)
        return new BigDecimal(fxRate)
    else
        return null
}


List frameKeyLists(String product, String offer, String base, List adjs) {
    def keyList = []
    List keys = [[product, offer, base], adjs]?.flatten()
    keyList.add(frameKey(keys))
    keys = [[product, "*", base], adjs]?.flatten()
    keyList.add(frameKey(keys))
    keys = [[product, offer, "*"], adjs]?.flatten()
    keyList.add(frameKey(keys))
    keys = [[product, "*", "*"], adjs]?.flatten()
    keyList.add(frameKey(keys))
    keys = [["*", "*", "*"], adjs]?.flatten()
    keyList.add(frameKey(keys))
    return keyList
}


String frameKey(List keys) {
    String formattedKey
    keys.each {
        key ->
            formattedKey = formattedKey ? (formattedKey += "_" + key) : key
    }
    return formattedKey
}