void saveFilter(String percentageChange, Map filter) {

    List ctValues = api.findLookupTableValues("TAMFilter")?.getAt("name")
    List listOfSequences = ctValues.collect { it.toInteger() }
    Integer sequence = listOfSequences ? listOfSequences?.max() : 0
    sequence = sequence ? sequence + 1 : 1
    def tableId = api.findLookupTable("TAMFilter")?.id
    api.addOrUpdate("JLTV", [
            "lookupTableId"                        : tableId,
            "lookupTableName"                      : "TAMFilter",
            "name"                                 : sequence,
            "attributeExtension___PercentageChange": percentageChange,
            "attributeExtension___DataFilter"      : api.jsonEncode(filter)
    ])
}

Map getTAMFilter(String tableName) {
    return api.findLookupTableValues(tableName)?.collectEntries {
        [
                (it.name): [
                        "DataFilter"      : it["attributeExtension___DataFilter"],
                        "PercentageChange": it["attributeExtension___PercentageChange"]
                ]
        ]
    }
}

List getStandardProductRecords(List filters) {
    def constants = libs.vmwareUtil.Constants
    List stdProductdetails = []
    recordStream = api.stream(constants.PX_STANDARD_PRODUCT_TABLE_TYPE_CODE, null, [constants.PX_STANDARD_PRODUCTS_PART_NUMBER, constants.PX_STANDARD_PRODUCTS_PRODUCT, constants.PX_STANDARD_PRODUCTS_OFFER, constants.PX_STANDARD_PRODUCTS_BASE, constants.PX_STANDARD_PRODUCTS_BASE_PRICE_SKU, constants.PX_STANDARD_PRODUCTS_TYPE], *filters)
    stdProductdetails << recordStream?.collect { it }
    recordStream?.close()
    return stdProductdetails
}

List getBOMRecords(List filters) {
    def constants = libs.vmwareUtil.Constants
    List bundleDetails = []
    recordStream = api.stream(constants.PX_BOM_TYPE_CODE, null, [constants.PX_BOM_PART_NUMBER], *filters)
    bundleDetails << recordStream?.collect { it }
    recordStream?.close()
    return bundleDetails.sku
}

Map getDescriptions(List skus) {
    def constants = libs.vmwareUtil.Constants
    List filters = []
    filters << Filter.in(constants.COLUMN_NAME, skus)
    return api.findLookupTableValues(constants.PP_STANDARD_PRODUCT_DESCRIPTION_TABLE, [constants.COLUMN_NAME, constants.PP_STANDARD_PRODUCT_DESCRIPTION_SHORT_DESCRIPTION, constants.PP_STANDARD_PRODUCT_DESCRIPTION_LONG_DESCRIPTION], null, *filters)?.collectEntries {
        [
                (it.name): [
                        (constants.SHORT_DESCRIPTION): it?.attributeExtension___ShortDescription,
                        (constants.LONG_DESCRIPTION) : it?.attributeExtension___LongDescription
                ]
        ]
    }
}

