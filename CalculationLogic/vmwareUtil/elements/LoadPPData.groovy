def getLTV(def tableName, def field) {
    def table = api.findLookupTable(tableName)
    def all = []
    int start = 0
    int maxResults = api.getMaxFindResultsLimit()
    while (records = api.find("LTV", start, maxResults, field, [field], true, Filter.equal("lookupTable.id", table.id))) {
        start += records.size()
        all.addAll(records)
    }
    return all
}

def getLTV(def tableName) {
    def table = api.findLookupTable(tableName)
    def all = []
    int start = 0
    int maxResults = api.getMaxFindResultsLimit()
    while (records = api.find("LTV", start, maxResults, "value", ["name", "value"], Filter.equal("lookupTable.id", table.id))) {
        start += records.size()
        all.addAll(records)
    }
    return all
}

def getLTVAsList(def tableName, def field) {
    def fieldValueList = []
    def recordList = getLTV(tableName, field)
    if (recordList) {
        for (record in recordList) {
            if (field == "name")
                fieldValueList.add(record.name)
            else if (field == "value")
                fieldValueList.add(record.value)
        }
    }
    return fieldValueList
}

def getLTVAsMap(def tableName) {
    def recordMap = [:]
    def recordList = getLTV(tableName)
    if (recordList) {
        for (record in recordList) {
            if (record.name)
                recordMap.put(record.name, record.value)
        }
    }
    recordMap = recordMap.sort { it.value }
    return recordMap
}


def getMLTV(def tableName, def field) {
    def table = api.findLookupTable(tableName)
    def all = []
    int start = 0
    int maxResults = api.getMaxFindResultsLimit()
    while (records = api.find("MLTV", start, maxResults, field, [field], true, Filter.equal("lookupTable.id", table.id))) {
        start += records.size()
        all.addAll(records)
    }
    return all
}

def getMLTVAsList(def tableName, def field) {
    def fieldValueList = []
    def recordList = getMLTV(tableName, field)
    if (recordList) {
        for (record in recordList) {
            fieldValueList.add(record.field)
        }
    }
    return fieldValueList
}


def getMLTVAsMap(def tableName, def key, def value) {
    def recordMap = [:]
    def table = api.findLookupTable(tableName)
    def all = []
    int start = 0
    int maxResults = api.getMaxFindResultsLimit()
    while (records = api.find("MLTV", start, maxResults, value, [key, value], true, Filter.equal("lookupTable.id", table.id))) {
        start += records.size()
        all.addAll(records)
    }


    if (all) {
        for (record in all) {
            if (record.key)
                recordMap.put(record.key, record.value)
        }
    }
    return recordMap
}


def getMLTV2AsMap(def tableName, def key1) {

    def recordMap = [:]
    def table = api.findLookupTable(tableName)
    def all = []
    int start = 0
    int maxResults = api.getMaxFindResultsLimit()
    while (records = api.find("MLTV2", start, maxResults, null, Filter.equal("lookupTable.id", table.id),
            Filter.equal("key1", key1)
    )
    ) {
        start += records.size()
        all.addAll(records)
    }

    if (all) {
        for (record in all) {
            if (record.key2)
                recordMap.put(record.key2, record.attribute1)
        }
    }
    api.trace("recordMap", null, recordMap.toString())

    return recordMap
}


def getMLTV4(def tableName, def field) {
    def table = api.findLookupTable(tableName)
    def all = []
    int start = 0
    int maxResults = api.getMaxFindResultsLimit()
    while (records = api.find("MLTV4", start, maxResults, field, [field], true, Filter.equal("lookupTable.id", table.id))) {
        start += records.size()
        all.addAll(records)
    }
    return all
}

def getMLTV4AsList(def tableName, def field) {
    def fieldValueList = []
    def recordList = getMLTV4(tableName, field)
    if (recordList) {
        for (record in recordList) {
            fieldValueList.add(record.field)
        }
    }
    return fieldValueList
}


def getMLTV4AsMap(def tableName, def key, def value) {
    def recordMap = [:]
    def table = api.findLookupTable(tableName)
    def all = []
    int start = 0
    int maxResults = api.getMaxFindResultsLimit()
    while (records = api.find("MLTV4", start, maxResults, key, [key, value], true, Filter.equal("lookupTable.id", table.id))) {
        start += records.size()
        all.addAll(records)
    }


    if (all) {
        for (record in all) {
            if (record.key)
                recordMap.put(record.key, record.value)
        }
    }
    return recordMap
}