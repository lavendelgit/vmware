def roundedLCPrice(BigDecimal listPrice, String roundingType, Integer precision) {
    BigDecimal roundedLCPrice = listPrice
    switch (roundingType) {
        case "RoundUp":
            roundedLCPrice = listPrice?.setScale(precision, BigDecimal.ROUND_UP)
            break
        case "RoundDown":
            roundedLCPrice = listPrice?.setScale(precision, BigDecimal.ROUND_DOWN)
            break
        case "Round":
            roundedLCPrice = listPrice?.setScale(precision, BigDecimal.ROUND_HALF_UP)
            break
    }
    return roundedLCPrice
}

def getCurrencyValue(String currency, String productType, Map roundingRulesMap) {
    def roundRecord
    Map roundinglookup = [:]
    switch (productType) {
        case libs.vmwareUtil.Constants.STR_HW_OFFER:
            roundRecord = roundingRulesMap[libs.vmwareUtil.Constants.STR_PRODUCT + libs.vmwareUtil.Constants.STR_HW_OFFER + libs.vmwareUtil.Constants.STR_BASE + currency]
            break;
        case libs.vmwareUtil.Constants.STR_EXT_REPLACEMNET_OFFER:
            roundRecord = roundingRulesMap[libs.vmwareUtil.Constants.STR_PRODUCT + libs.vmwareUtil.Constants.STR_EXT_REPLACEMNET_OFFER + libs.vmwareUtil.Constants.STR_BASE + currency]
            break;
        case libs.vmwareUtil.Constants.STR_RENTAL_OFFER:
            roundRecord = roundingRulesMap[libs.vmwareUtil.Constants.STR_PRODUCT + libs.vmwareUtil.Constants.STR_RENTAL_OFFER + libs.vmwareUtil.Constants.STR_BASE + currency]
            break;
        case libs.vmwareUtil.Constants.STR_SW_OFFER:
            roundRecord = roundingRulesMap[libs.vmwareUtil.Constants.STR_PRODUCT + libs.vmwareUtil.Constants.STR_SW_OFFER + libs.vmwareUtil.Constants.STR_BASE + currency]
            break;
        case libs.vmwareUtil.Constants.STR_ADDON_OFFER:
            roundRecord = roundingRulesMap[libs.vmwareUtil.Constants.STR_PRODUCT + libs.vmwareUtil.Constants.STR_ADDON_OFFER + libs.vmwareUtil.Constants.STR_BASE + currency]
            break;
    }

    roundinglookup.RoundingType = (roundRecord) ? roundRecord.attribute1 : null
    roundinglookup.Precision = ((roundRecord) ? roundRecord.attribute2 : null) as Integer
    return roundinglookup
}