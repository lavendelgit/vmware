import java.text.DateFormat

List<String> getListMonths(Date dateFrom, Date dateTo, Locale locale, DateFormat df, Integer userEndYear) {
    Calendar calendar = Calendar.getInstance(locale)
    calendar.setTime(dateFrom)
    List months = []
    while (calendar.getTime().getTime() <= dateTo.getTime()) {
        months.add(df.format(calendar.getTime()))
        calendar.add(Calendar.MONTH, 1)
        if (calendar.get(1) > userEndYear) {
            break
        }
    }
    return months
}