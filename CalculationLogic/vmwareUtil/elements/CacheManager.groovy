def cacheSegmentAdjustments(List product, List offer, List base) {
    Map segmentAdjustmentCache = [:]
    def filter = [
            Filter.in("key2", product),
            Filter.in("key3", offer),
            Filter.in("key4", base)
    ]
    def segmentAdjustmentValues = api.findLookupTableValues("SegmentAdjustment", *filter)
    if (segmentAdjustmentValues) {
        def segmentAdjustments = segmentAdjustmentValues.groupBy({
            getWeightage(it.key2, it.key3, it.key4) + "." + it.key1
        })
        def sortedAdjs = segmentAdjustments.sort()*.key
        def segmentAdj
        def key
        for (adj in sortedAdjs) {
            segmentAdjustmentRecords = segmentAdjustments[adj]
            for (segmentAdjustment in segmentAdjustmentRecords) {
                key = (segmentAdjustment.key3 == "All") ? segmentAdjustment.key1 : segmentAdjustment.key3 + "_" + segmentAdjustment.key1
                segmentAdjustmentCache[key] = segmentAdjustment.attribute1
            }
        }
    }
    return segmentAdjustmentCache
}

def cacheSupportAdjustments(List product, List offer, List base) {
    Map supportFactorCache = [:]
    def filter = [
            Filter.in("key1", product),
            Filter.in("key2", offer),
            Filter.in("key3", base)
    ]
    def supportAdjustmentValues = api.findLookupTableValues("SupportOption", *filter)
    if (supportAdjustmentValues) {
        def supportAdjustments = supportAdjustmentValues.groupBy({
            getWeightage(it.key2, it.key3, it.key4) + "." + it.key1
        })
        def sortedAdjs = supportAdjustments.sort()*.key
        def key
        for (adj in sortedAdjs) {
            supportAdjustmentRecords = supportAdjustments[adj]
            for (supportAdjustment in supportAdjustmentRecords) {
                key = (supportAdjustment.key2 == "All") ? supportAdjustment.key4 : supportAdjustment.key2 + "_" + supportAdjustment.key4
                supportFactorCache[key] = supportAdjustment.attribute2
            }
        }
    }
    return supportFactorCache
}

def cacheServiceLevelFactors(List product, List offer, List base) {
    Map serviceLevelFactorCache = [:]
    def filter = [
            Filter.in("key1", product),
            Filter.in("key2", offer),
            Filter.in("key3", base)
    ]
    def serviceFactorValues = api.findLookupTableValues("ServiceLevelFactors", *filter)
    if (serviceFactorValues) {
        def serviceLevelFactors = serviceFactorValues.groupBy({
            getWeightage(it.key2, it.key3, it.key4) + "." + it.key1
        })
        def sortedFactors = serviceLevelFactors.sort()*.key
        def key
        for (factor in sortedFactors) {
            serviceLevelRecords = serviceLevelFactors[factor]
            for (serviceLevelRecord in serviceLevelRecords) {
                key = (serviceLevelRecord.key2 == "All") ? serviceLevelRecord.key4 : serviceLevelRecord.key2 + "_" + serviceLevelRecord.key4
                serviceLevelFactorCache[key] = serviceLevelRecord.attribute1
            }
        }
    }
    return serviceLevelFactorCache
}

def cacheTermsFactor(List product, List offer, List base) {

    Map termsCache = [:]

    def filter = [
            Filter.in("key1", product),
            Filter.in("key2", offer),
            Filter.in("key3", base)
    ]
    def termFactorValues = api.findLookupTableValues("Terms", *filter)
    if (termFactorValues) {
        def termFactors = termFactorValues.groupBy({
            getWeightage(it.key2, it.key3) + "." + it.key4 + "." + it.key5
        })
        def sortedFactors = termFactors.sort()*.key
        def key
        for (factor in sortedFactors) {
            termRecords = termFactors[factor]
            for (termRecord in termRecords) {
                termsCache[termRecord.key2 + "_" + termRecord.key4 + "_" + termRecord.key5] = [
                        "Factor"      : termRecord.attribute1,
                        "PaymentCycle": termRecord.attribute4
                ]
            }
        }
    }
    return termsCache
}


def cacheVolumeTier() {

    Map volumeTierCache = [:]
    volumeTierCache = api.findLookupTableValues("VolumeTier", null)?.collectEntries {
        tier ->
            [
                    (tier.key4 + "_" + tier.key5 + "_" + tier.key6): [
                            "Min"     : tier.key4,
                            "Max"     : tier.key5,
                            "UoM"     : tier.key6,
                            "TierName": tier.attribute4,
                            "Factor"  : tier.attribute1
                    ]
            ]
    }
    return volumeTierCache
}

def cacheYearFactors() {
    return api.findLookupTableValues("YearsFactor", null)?.collectEntries { [(it.name): it.attribute2] }
}

def cachePaymentFrequncyLabel() {
    return api.findLookupTableValues("PaymentFrequency", null)?.collectEntries { [(it.value): it.name] }
}

def cacheNormalizationFactors(List offer, List base) {
    Map normalizationCache = [:]
    def filter = [
            Filter.in("key1", offer),
            Filter.in("key2", base)
    ]
    def normValues = api.findLookupTableValues("NyansaNormalizationFactor", *filter)
    if (normValues) {
        def normRecords = normValues.groupBy({
            getWeightage(it.key2, it.key3) + "." + it.key1
        })
        def sortedRecords = normRecords.sort()*.key
        for (factor in sortedRecords) {
            records = normRecords[factor]
            for (record in records) {
                normalizationCache[record.key3 + "_" + record.key4] = record.attribute1
            }
        }
    }
    return normalizationCache
}


/*def cacheLongevityFactors(List product,List offer,List base) {
  
    Map longevityFactorCache = [:]
    def filter = [
      Filter.in("key1", product),
      Filter.in("key2", offer),
      Filter.in("key3", base)
    ]
    def longevityFactors = api.findLookupTableValues("LongevityFactors", *filter)
    if (longevityFactors) {
      def gplongevityFactors = longevityFactors.groupBy({
        getWeightage(it.key1, it.key2, it.key3) + "." + it.key4
      })
      def sortedlongEvityFactors = gplongevityFactors.sort()*.key
      def longEvityRecords
      def key
      for (sortedlongEvityRec in sortedlongEvityFactors) {
        longEvityRecords = gplongevityFactors[sortedlongEvityRec]
        for( longEvityRecord  in longEvityRecords) {
          key = (longEvityRecord.key3 == "All")?longEvityRecord.key2+"_"+longEvityRecord.key4:longEvityRecord.key2+"_"+longEvityRecord.key3+"_"+longEvityRecord.key4
          longevityFactorCache[key] = longEvityRecord.attribute1
        }
      }
    }
  return longevityFactorCache
}*/

def cacheLongevityFactors(List product, List offer, List base) {

    Map longevityFactorCache = [:]
    def filter = [
            Filter.in("key1", product),
            Filter.in("key2", offer),
            Filter.in("key3", base)
    ]
    def longevityFactors = api.findLookupTableValues("Terms", *filter)
    if (longevityFactors) {
        def gplongevityFactors = longevityFactors.groupBy({
            getWeightage(it.key1, it.key2, it.key3) + "." + it.key4
        })
        def sortedlongEvityFactors = gplongevityFactors.sort()*.key
        def longEvityRecords
        def key
        for (sortedlongEvityRec in sortedlongEvityFactors) {
            longEvityRecords = gplongevityFactors[sortedlongEvityRec]
            for (longEvityRecord in longEvityRecords) {
                key = (longEvityRecord.key3 == "All") ? longEvityRecord.key2 + "_" + longEvityRecord.key5 : longEvityRecord.key2 + "_" + longEvityRecord.key3 + "_" + longEvityRecord.key5
                longevityFactorCache[key] = longEvityRecord.attribute2
            }
        }
    }
    return longevityFactorCache
}

def cacheHwReplacementServiceFactor(List product, List offer, List base) {

    Map replacementFactorCache = [:]
    def filter = [
            Filter.in("key1", product),
            Filter.in("key2", offer),
            Filter.in("key3", base)
    ]
    def hardwareReplacementServiceFactors = api.findLookupTableValues("HardwareReplacementServiceFactor", *filter)
    if (hardwareReplacementServiceFactors) {
        def replacementServiceFactors = hardwareReplacementServiceFactors.groupBy({
            getWeightage(it.key1, it.key2, it.key3) + "." + it.key4 + "_" + it.key5
        })
        def sortedReplacements = replacementServiceFactors.sort()*.key
        def replacementServiceFactorRecords
        def key
        for (replacement in sortedReplacements) {
            replacementServiceFactorRecords = replacementServiceFactors[replacement]
            for (replacementRecord in replacementServiceFactorRecords) {
                key = (replacementRecord.key3 == "All") ? replacementRecord.key2 + "_" + replacementRecord.key4 + "_" + replacementRecord.key5 : replacementRecord.key2 + "_" + replacementRecord.key3 + "_" + replacementRecord.key4 + "_" + replacementRecord.key5
                replacementFactorCache[key] = replacementRecord.attribute1
            }
        }
    }
    return replacementFactorCache
}

def cacheLCFactor(lcFactor) {
    return api.findLookupTableValues("LocalCurrencyExchangeRates", Filter.in("name", [lcFactor]))?.collectEntries {
        [(it.name): it]
    }
}

def cacheBasePrice(List product, List offer) {
    Map basePriceCache = [:]

    def filter = [
            Filter.in("key1", product),
            Filter.in("key2", offer)
    ]
    def basePrices = api.findLookupTableValues("BasePrice", *filter)
    if (basePrices) {
        def groupedBasePrices = basePrices.groupBy({
            getWeightage(it.key1, it.key2) + "." + it.key2 + "_" + it.key3
        })
        def sortedBasePrices = groupedBasePrices.sort()*.key
        def key
        for (sortedBasePrice in sortedBasePrices) {
            basePrices = groupedBasePrices[sortedBasePrice]
            for (basePrice in basePrices) {
                key = (basePrice.key2 == "All") ? basePrice.key3 : basePrice.key2 + "_" + basePrice.key3
                basePriceCache[key] = basePrice.attribute1
            }
        }
    }
    return basePriceCache
}

def cacheBaseDetails(List product, List offer) {
    Map basePriceCache = [:]

    def filter = [
            Filter.in("key1", product),
            Filter.in("key2", offer)
    ]
    def basePrices = api.findLookupTableValues("BasePrice", *filter)
    if (basePrices) {
        def groupedBasePrices = basePrices.groupBy({
            getWeightage(it.key1, it.key2) + "." + it.key2 + "_" + it.key3
        })
        def sortedBasePrices = groupedBasePrices.sort()*.key
        def key
        for (sortedBasePrice in sortedBasePrices) {
            basePrices = groupedBasePrices[sortedBasePrice]
            for (basePrice in basePrices) {
                key = (basePrice.key2 == "All") ? basePrice.key3 : basePrice.key2 + "_" + basePrice.key3
                basePriceCache[key] = basePrice
            }
        }
    }
    return basePriceCache
}


def getBaseDetails(Map baseCache, String offer, String base) {
    String key = frameKey(offer, base)
    baseValue = baseCache?.getAt(key)
    return baseValue
}


def getBasePrice(Map basePriceCache, String offer, String base) {
    String key = frameKey(offer, base)
    BigDecimal basePrice = basePriceCache?.getAt(key)
    basePrice = basePrice ?: (basePriceCache?.getAt(base) ?: 0.0)
    return basePrice
}

def getReplacementServiceFactor(Map replacementFactorCache, String offer, String base, String replacementServiceType, String boxSize) {
    String key = frameKey(offer, base, replacementServiceType, boxSize)
    BigDecimal replacementFactor = replacementFactorCache?.getAt(key)
    key = frameKey(offer, replacementServiceType, boxSize)
    replacementFactor = replacementFactor ?: (replacementFactorCache?.getAt(key) ?: 0.0)
    return replacementFactor
}

def getlongevityFactor(Map longevityFactorCache, String offer, String base, String year) {
    String key = frameKey(offer, base, year)
    BigDecimal longevityFactor = longevityFactorCache?.getAt(key)
    key = frameKey(offer, year)
    longevityFactor = longevityFactor ?: (longevityFactorCache?.getAt(key) ?: 0.0)
    return longevityFactor
}

def getSegmentAdjustment(Map segmentAdjustmentCache, String offer, String segment) {
    String key = frameKey(offer, segment)
    BigDecimal segmentAdjustment = segmentAdjustmentCache?.getAt(key)
    segmentAdjustment = segmentAdjustment ?: segmentAdjustmentCache?.getAt(segment)
    return segmentAdjustment
}


def getSupportFactor(Map supportFactorCache, String offer, String supportName) {
    String key = frameKey(offer, supportName)
    BigDecimal supportFactor = supportFactorCache?.getAt(key)
    supportFactor = supportFactor ?: supportFactorCache?.getAt(supportName)
    return supportFactor
}

def getServiceFactor(Map serviceLevelFactorCache, String offer, String service) {
    String key = frameKey(offer, service)
    BigDecimal serviceFactor = serviceLevelFactorCache?.getAt(key)
    serviceFactor = serviceFactor ?: serviceLevelFactorCache?.getAt(service)
    return serviceFactor
}

def getTermFactor(Map termsCache, String term, String year) {
    String key = frameKey(term, year)
    BigDecimal termFactor = termsCache?.getAt(key)?.Factor
    return termFactor
}

def getTermFactor(Map termsCache, String offer, String term, String year) {
    String key = frameKey(offer, term, year)
    BigDecimal termFactor = termsCache?.getAt(key)?.Factor
    return termFactor
}

def getPaymentCycle(Map termsCache, String term, String year) {
    String key = frameKey(term, year)
    BigDecimal paymentCycle = termsCache?.getAt(key)?.PaymentCycle
    return paymentCycle
}

def getPaymentCycle(Map termsCache, String offer, String term, String year) {
    String key = frameKey(offer, term, year)
    BigDecimal paymentCycle = termsCache?.getAt(key)?.PaymentCycle
    return paymentCycle
}


/*def getBasePrice( String offer , String base) {
  String key = frameKey( offer,base)
  BigDecimal basePrice = api.global.basePriceCache?.getAt(key)
  basePrice = basePrice?:(api.global.basePriceCache?.getAt(base)?:0.0)
  return basePrice
}

def getReplacementServiceFactor(String offer, String base, String replacementServiceType, String boxSize) {
  String key = frameKey(offer,base,replacementServiceType,boxSize)
  BigDecimal replacementFactor =api.global.replacementFactorCache?.getAt(key)
  key = frameKey(offer,replacementServiceType,boxSize)
  replacementFactor = replacementFactor?:(api.global.replacementFactorCache?.getAt(key)?:0.0)
  return replacementFactor
}

def getlongevityFactor(String offer, String base, String year ) {
  String key = frameKey(offer,base,year)
  BigDecimal longevityFactor =api.global.longevityFactorCache?.getAt(key)
  key = frameKey(offer,year)
  longevityFactor = longevityFactor?:(api.global.longevityFactorCache?.getAt(key)?:0.0)
  return longevityFactor
}

def getSegmentAdjustment(String offer, String segment) {
  String key = frameKey(offer, segment)
  BigDecimal segmentAdjustment = api.global.segmentAdjustmentCache?.getAt(key)
  segmentAdjustment = segmentAdjustment ?: api.global.segmentAdjustmentCache?.getAt(segment)
  return segmentAdjustment
}


def getSupportFactor(String offer, String supportName) {
  String key = frameKey(offer, supportName)
  BigDecimal supportFactor = api.global.supportFactorCache?.getAt(key)
  supportFactor = supportFactor ?: api.global.supportFactorCache?.getAt(supportName)
  return supportFactor
}

def getServiceFactor(String offer, String service) {
  String key = frameKey(offer, service)
  BigDecimal serviceFactor = api.global.serviceLevelFactorCache?.getAt(key)
  serviceFactor = serviceFactor ?: api.global.serviceLevelFactorCache?.getAt(service)
  return serviceFactor
}

def getTermFactor(String term, String year) {
  String key = frameKey(term, year)
  BigDecimal termFactor = api.global.termsCache?.getAt(key)?.Factor
  return termFactor
}

def getPaymentCycle(String term, String year) {
  String key = frameKey(term, year)
  BigDecimal paymentCycle = api.global.termsCache?.getAt(key)?.PaymentCycle
  return paymentCycle
}
*/

def frameKey(String... keys) {
    String formattedKey
    keys.each {
        key ->
            formattedKey = formattedKey ? (formattedKey += "_" + key) : key
    }
    return formattedKey
}

def getWeightage(String... keys) {
    def weightage = 0 as Integer
    keys.eachWithIndex { key, index ->
        def weightageIndex = index ? 2.power(index) : 1
        weightage += (("All".equals(key) ? 0 : weightageIndex))
    }
    return weightage
}


Integer getWeightage(List keys, String defaultValue) {
    Integer weightage = 0
    Integer weightageIndex
    keys.eachWithIndex { key, index ->
        weightageIndex = index ? 2.power(index) : 1
        weightage += ((defaultValue.equals(key) ? 0 : weightageIndex))
    }
    return weightage
}

def getAUDLCFactor(lcFactor) {
    return api.global.LCFactorCache?.getAt(lcFactor)?.attribute1 as BigDecimal
}

def getJPYLCFactor(lcFactor) {
    return api.global.LCFactorCache?.getAt(lcFactor)?.attribute2 as BigDecimal
}

def getCNYLCFactor(lcFactor) {
    return api.global.LCFactorCache?.getAt(lcFactor)?.attribute3 as BigDecimal
}

def getEURLCFactor(lcFactor) {
    return api.global.LCFactorCache?.getAt(lcFactor)?.attribute4 as BigDecimal
}

def getGBPLCFactor(lcFactor) {
    return api.global.LCFactorCache?.getAt(lcFactor)?.attribute5 as BigDecimal
}

