import com.googlecode.genericdao.search.Filter
import java.text.SimpleDateFormat

BigDecimal getLocalExchangeRate(String fromCurrency, String toCurrency) {
    //BigDecimal exchangeRate
    Date date = new Date()
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
    String toDay = sdf.format(date)
    /*Filter fromCurrencyFilter = Filter.equal("key1", fromCurrency)
    Filter toCurrencyFilter = Filter.equal("key2", toCurrency)
    Filter dateFromFilter = Filter.lessOrEqual("key3", toDay)
    Filter dateToFilter = Filter.greaterOrEqual("key4", toDay)*/
    List filters = [
            Filter.equal("key1", fromCurrency),
            Filter.equal("key2", toCurrency),
            Filter.lessOrEqual("key3", toDay),
            Filter.greaterOrEqual("key4", toDay)
    ]
    //String exchangeRate = api.findLookupTableValues("TrainingExchangeRate", fromCurrencyFilter, toCurrencyFilter, dateFromFilter, dateToFilter)?.attribute2[0]
    BigDecimal exchangeRate = api.findLookupTableValues("TrainingExchangeRate", *filters)?.getAt(0).attribute2
    return exchangeRate ?: 0
}

// List Filters
// use getat and find class
// change exchangerate