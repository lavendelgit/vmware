def OneYearPrice(hostType, itemType, dataCenter, upliftType) {
    if (hostType != null && itemType != null) {
        if (upliftType == null) {
            upliftType = "LC Factor"
        }
        //discounted price
        def tab1 = api.findLookupTable("PerpetualSKUs")
        def tabList1
        def discountedPrice = 0
        tabList1 = api.find("MLTV2", Filter.equal("lookupTable.id", tab1?.id),
                Filter.equal("key2", dataCenter))
        if (tabList1) {
            tabList1.each {
                /*def price, discount,dPrice
                price = it.attribute2?:0
                discount = it.attribute3?:0
                dPrice = price*(1-discount)*/
                def dPrice
                dPrice = it.attribute4 ?: 0
                discountedPrice += dPrice
            }
        }
        if (discountedPrice == null) {
            discountedPrice = 0
        }

        //sns calculation
        def tab2 = api.findLookupTable("Adjustments")
        def tabList2, sns, snsVal
        tabList2 = api.find("MLTV2", Filter.equal("lookupTable.id", tab2?.id),
                Filter.equal("key1", "SnS"),
                Filter.equal("key2", dataCenter))
        if (tabList2) {
            sns = tabList2[0].attribute1 ?: 0
        }
        if (sns == null) {
            sns = 0
        }
        if (discountedPrice && sns) {
            snsVal = discountedPrice * sns
        }
        if (snsVal == null) {
            snsVal = 0
        }

        //3YearTCO
        def threeYearTCO
        snsVal = snsVal * 3
        threeYearTCO = snsVal + discountedPrice
        if (threeYearTCO == null) {
            threeYearTCO = 0
        }
        //api.trace("3YearTCO",null,threeYearTCO)

        //perHour
        def perHour, perCore
        perHour = threeYearTCO / 36 / 730

        //perCore
        perCore = perHour / 12

        //perHourHost
        def host
        def tab3 = api.findLookupTable("HostPerCore")
        def tabList3
        tabList3 = api.find("MLTV", Filter.equal("lookupTable.id", tab3?.id),
                Filter.equal("name", hostType))
        if (tabList3 != null) {
            host = tabList3[0]?.attribute1
        }
        def perHourHost
        if (perCore != null && host != null) {
            perHourHost = perCore * host
        }
        //api.trace("perHourHost",null,perHourHost)

        //subscriptionUplift
        def tabList4
        def subscription
        def subscriptionUplift
        tabList4 = api.find("MLTV2", Filter.equal("lookupTable.id", tab2?.id),
                Filter.equal("key1", "Subscription Uplift"),
                Filter.equal("key2", dataCenter))
        if (tabList4 != null) {
            subscription = tabList4[0]?.attribute1
            if (subscription == null) {
                subscription = 0
            }
            subscription += 1
            subscription = subscription?.toBigDecimal()
            perHourHost = perHourHost?.toBigDecimal()
            if (subscription != null && perHourHost != null)
                subscriptionUplift = subscription * perHourHost
        }
        //api.trace("subscriptionUplift",null,subscriptionUplift)

        //onDemandUplift
        def tabList5
        def onDemand
        def onDemandUplift
        tabList5 = api.find("MLTV2", Filter.equal("lookupTable.id", tab2?.id),
                Filter.equal("key1", "On Demand Uplift"),
                Filter.equal("key2", dataCenter))
        if (tabList5 != null) {
            onDemand = tabList5[0]?.attribute1
            //api.trace("test",null,onDemand)
            if (onDemand == null) {
                onDemand = 0
            }
            if (itemType == "1 Year" || itemType == "3 Year") {
                onDemand = 0
            }
            onDemand += 1
        }
        //api.trace("onDemandPerc",null,onDemand)
        if (onDemand != null && subscriptionUplift != null) {
            onDemandUplift = onDemand * subscriptionUplift
        }
        //api.trace("onDemandUplift",null,onDemandUplift)

        //channelPartnerUplift
        def tabList6
        def channel
        def channelUplift
        tabList6 = api.find("MLTV2", Filter.equal("lookupTable.id", tab2?.id),
                Filter.equal("key1", "Channel Uplift"),
                Filter.equal("key2", dataCenter))
        if (tabList6 != null) {
            channel = tabList6[0]?.attribute1
            if (channel == null) {
                channel = 0
            }
            channel += 1
        }
        if (channel != null && onDemandUplift != null) {
            channelUplift = channel * onDemandUplift
        }
        //api.trace("channelUplift",null,channelUplift)

        //AWS
        def tab7 = api.findLookupTable("AWSHWPrice")
        def tabList7
        def aws
        tabList7 = api.find("MLTV2", Filter.equal("lookupTable.id", tab7?.id),
                Filter.equal("key1", dataCenter),
                Filter.equal("key2", hostType))
        if (tabList7 != null) {
            if (itemType == "On Demand") {
                aws = tabList7[0]?.attribute2
            }
            if (itemType == "1 Year") {
                aws = tabList7[0]?.attribute3
            }
            if (itemType == "3 Year") {
                aws = tabList7[0]?.attribute4
            }
        }
        if (aws == null) {
            aws = 0
        }

        //HW Support
        def tabList8
        def hwSupportPerc
        def hwSupport
        tabList8 = api.find("MLTV2", Filter.equal("lookupTable.id", tab2?.id),
                Filter.equal("key1", "Hardware Support"),
                Filter.equal("key2", dataCenter))
        if (tabList8 != null) {
            hwSupportPerc = tabList8[0]?.attribute1 ?: 0
        }
        hwSupport = aws * hwSupportPerc

        //VMC on AWS
        def VMConAWS
        if (channelUplift != null && aws != null && hwSupport != null)
            VMConAWS = channelUplift + aws + hwSupport

        //SW/HW
        def sw = channelUplift
        def hw
        if (VMConAWS != null && sw != null)
            hw = VMConAWS - sw

        //SW Adjustment Uplift
        def SaaS
        def tab11 = api.findLookupTable("AWSHWPrice")
        def govCloudList
        def usWestOregonList
        api.trace("dataCenterOneYear", null, dataCenter)
        api.trace("UpliftTypeOneYear", null, upliftType)
        if (dataCenter == "AWS GovCloud US" && upliftType == "AWS HW") {
            def tab22 = api.findLookupTable("AWSHWPrice")
            def AWSUpliftList = api.find("MLTV2", Filter.equal("lookupTable.id", tab22?.id),
                    Filter.equal("key1", "AWS GovCloud US"),
                    Filter.equal("key2", hostType))
            if (AWSUpliftList != null) {
                SaaS = AWSUpliftList[0]?.attribute6
                SaaS += 1
            }
        } else if ((dataCenter == "AWS GovCloud US" && upliftType == "LC Factor") || upliftType == "AWS HW") {
            govCloudList = api.find("MLTV2", Filter.equal("lookupTable.id", tab11?.id),
                    Filter.equal("key1", dataCenter),
                    Filter.equal("key2", hostType))
            usWestOregonList = api.find("MLTV2", Filter.equal("lookupTable.id", tab11?.id),
                    Filter.equal("key1", "US West Oregon"),
                    Filter.equal("key2", hostType))
            if (govCloudList != null && usWestOregonList != null) {
                def govCloudVal
                def usWestOregonVal
                if (itemType == "1 Year") {
                    govCloudVal = govCloudList[0]?.attribute3?.toDouble()
                    usWestOregonVal = usWestOregonList[0]?.attribute3?.toDouble()
                    if (usWestOregonVal != 0 && usWestOregonVal != null && govCloudVal != null) {
                        def val = (govCloudVal - usWestOregonVal) / usWestOregonVal
                        if (val == null) {
                            val = 0
                        }
                        val += 1
                        SaaS = val?.toBigDecimal()
                        api.trace("SaasOneYearSW Perc HW", null, SaaS)
                    }
                }
            }
        } else {
            def tab9 = api.findLookupTable("VMCAdjustmentUplift")
            def tabList9
            tabList9 = api.find("MLTV", Filter.equal("lookupTable.id", tab9?.id),
                    Filter.equal("name", dataCenter))
            if (tabList9 != null) {
                def LCFactor = tabList9[0]?.attribute3?.toDouble()
                def latestFX = tabList9[0]?.attribute4?.toDouble()
                if (LCFactor != 0 && latestFX != 0 && LCFactor != null && latestFX != null) {
                    SaaS = LCFactor / latestFX - 1
                    if (SaaS == null) {
                        SaaS = 0
                    }
                    SaaS += 1
                    SaaS = SaaS?.toBigDecimal()
                    api.trace("SaasOneYearSW Perc LC", null, SaaS)
                }
            }
        }
        api.trace("SW Uplift Perc", null, SaaS)

        //SW with uplift
        def SWwithUplift
        if (sw != null && SaaS != null)
            SWwithUplift = sw * SaaS

        //TermDiscount
        def termDiscount
        def tab10 = api.findLookupTable("TermDiscounts")
        def tabList10
        tabList10 = api.find("MLTV2", Filter.equal("lookupTable.id", tab10?.id),
                Filter.equal("key1", itemType),
                Filter.equal("key2", hostType))
        if (tabList10 != null) {
            termDiscount = tabList10[0]?.attribute1
        }
        if (termDiscount == null) {
            termDiscount = 0
        } else termDiscount = termDiscount / 100
        termDiscount += 1
        api.trace("termDiscount", null, termDiscount)

        //SW with uplift and term discount
        def SWwithUpliftandTerm
        if (SWwithUplift != null && termDiscount != null)
            SWwithUpliftandTerm = SWwithUplift * termDiscount

        //VMC on AWS with uplift and term discount
        def VMCUpliftwithTermDiscount
        if (hw != null && SWwithUpliftandTerm != null)
            VMCUpliftwithTermDiscount = hw + SWwithUpliftandTerm

        api.trace("sw", null, SWwithUpliftandTerm)
        //api.trace("hw",null,hw)

        //base
        def result
        if (itemType == "On Demand" && VMCUpliftwithTermDiscount != null) {
            result = VMCUpliftwithTermDiscount
            result = round(result, 6)
        } else if (itemType == "1 Year" && VMCUpliftwithTermDiscount != null) {
            result = VMCUpliftwithTermDiscount * 730 * 12
            result = round(result, 2)
        } else if (itemType == "3 Year" && VMCUpliftwithTermDiscount != null) {
            result = VMCUpliftwithTermDiscount * 730 * 12 * 3
            result = round(result, 2)
        }
        return result
    }
}

def round(Number num, int decimalPlaces) {
    if (num == null) {
        return null
    }

    def factor = (int) Math.pow(10, decimalPlaces)

    return Math.round(num * factor) / factor
}