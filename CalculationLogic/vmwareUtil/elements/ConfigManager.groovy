Map attributesConfigManager() {
    return [
            WARNING                   : libs.vmwareUtil.Constants.WARNING,
            ALL_INPUTS_WARNING_MESSAGE: libs.vmwareUtil.Constants.ALL_INPUTS_WARNING_MESSAGE,
            YEAR_WARNING_MESSAGE      : libs.vmwareUtil.Constants.YEAR_WARNING_MESSAGE,
            TEXT_COLOR                : libs.vmwareUtil.Constants.TEXT_COLOR,
            BG_COLOR                  : libs.vmwareUtil.Constants.BG_COLOR,
            TEXT_WEIGHT                : libs.vmwareUtil.Constants.TEXT_WEIGHT,
            TEXT_ALLIGN               : libs.vmwareUtil.Constants.TEXT_ALLIGN]
}