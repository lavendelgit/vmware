def resultMatrix = api.newMatrix("product Family",
        "product Id",
        "Description (SAP description)",
        "Usage Item",
        "Currency",
        "Region",
        "Data Center",
        "Billing Term",
        "Billing Term Frequency",
        "Billing Term UOM",
        "Price")
api.trace("lpg name:", api.getElement("LPGName"))
def Nodei3LpgList = api.find("PG", 0, 1, null,
        Filter.equal("label", api.getElement("LPGName")))
def lpgMeta
if (Nodei3LpgList != null && Nodei3LpgList[0] != null)
    lpgMeta = api.find("PGIM", Filter.equal("priceGridId", Nodei3LpgList[0].id))
api.local.mappingData = [:]
api.trace("lpgMeta:", lpgMeta)
if (lpgMeta != null)
    lpgMeta.each {

        api.local.mappingData << [((it.elementName ? it.elementName : it.fieldName)): [fieldName  : it.fieldName,
                                                                                       label      : it.label,
                                                                                       formatType : it.formatType,
                                                                                       elementName: it.elementName]]
    }

api.trace("productFamily filters:", api.getElement("ProductFamilyFilters"))
api.trace("ProductId filters:", api.getElement("ProductIdFilters"))
api.trace("UsageItem filters:", api.getElement("UsageItemFilters"))
api.trace("DataCenter filters:", api.getElement("DataCenterFilters"))
api.trace("BillingTerm filters:", api.getElement("BillingTermFilters"))
api.trace("BillingFrequency filters:", api.getElement("BillingFrequencyFilters"))
//api.trace("Nodei3LpgList:", Nodei3LpgList)

def productFamilyAttributeName = getAttributeName("Description")
def productIdAttributeName = getAttributeName("SAPmaterialnumber")
def usageItemLongdescAttributeName = getAttributeName("UsageItemLongDescription")
def usageItemAttributeName = getAttributeName("UsageItem")
def datacenterAttributeName = getAttributeName("Datacentercode")
def billingTermAttributeName = getAttributeName("BillingTerm")
def billingTermFrequencyAttributeName = getAttributeName("BillingTermFrequency")
def billingTermUOMAttributeName = getAttributeName("BillingTermUOM")
def resultPriceAttributeName = getAttributeName("ResultPrice")
def resultPriceEURAttributeName = getAttributeName("ResultPriceEUR")
def resultPriceGBPAttributeName = getAttributeName("ResultPriceGBP")
def resultPriceAUDAttributeName = getAttributeName("ResultPriceAUD")
def resultPriceCNYAttributeName = getAttributeName("ResultPriceCNY")
def resultPriceJPYAttributeName = getAttributeName("ResultPriceJPY")
/*api.logInfo("*****************Attributes names*************","")
api.logInfo("getAttributeName(Description)",productFamilyAttributeName)
api.logInfo("getAttributeName(SAPmaterialnumber)",productIdAttributeName)
api.logInfo("getAttributeName(UsageItemLongDescriptionSAP)",usageItemLongdescAttributeName)
api.logInfo("getAttributeName(UsageItem)",usageItemAttributeName)
api.logInfo("getAttributeName(Datacentercode)",datacenterAttributeName)
api.logInfo("getAttributeName(BillingTerm)",billingTermAttributeName)
api.logInfo("getAttributeName(BillingTermFrequency)",billingTermFrequencyAttributeName)
api.logInfo("getAttributeName(BillingTermUOM)",billingTermUOMAttributeName)
api.logInfo("*****************Attributes names - END*************","")*/
def filters = [] as List
def itemList
api.trace("Nodei3LpgList:" + Nodei3LpgList + "	Nodei3LpgList[0]:", Nodei3LpgList[0])
if (Nodei3LpgList != null && Nodei3LpgList[0] != null) {
    //api.logInfo("adding filters*********************")
    filters.add(Filter.equal("priceGridId", Nodei3LpgList[0].id))
    if (api.getElement("ProductFamilyFilters") != null && api.getElement("ProductFamilyFilters").size() > 0) filters.add(Filter.in(getAttributeName("Description"), api.getElement("ProductFamilyFilters")))
//"attribute45"
    if (api.getElement("ProductIdFilters") != null && api.getElement("ProductIdFilters").size() > 0) filters.add(Filter.in(getAttributeName("SAPmaterialnumber"), api.getElement("ProductIdFilters")))
//"attribute66" - SAP Material No
    if (api.getElement("UsageItemFilters") != null && api.getElement("UsageItemFilters").size() > 0) filters.add(Filter.in(getAttributeName("UsageItem"), api.getElement("UsageItemFilters")))
//"attribute69"
    if (api.getElement("DataCenterFilters") != null && api.getElement("DataCenterFilters").size() > 0) filters.add(Filter.in(getAttributeName("Datacentercode"), api.getElement("DataCenterFilters")))
//"attribute62"
    if (api.getElement("BillingTermFilters") != null && api.getElement("BillingTermFilters").size() > 0) filters.add(Filter.in(getAttributeName("BillingTerm"), api.getElement("BillingTermFilters")))
//"attribute63"
    if (api.getElement("BillingFrequencyFilters") != null && api.getElement("BillingFrequencyFilters").size() > 0) filters.add(Filter.in(getAttributeName("BillingTermFrequency"), api.getElement("BillingFrequencyFilters")))
//"attribute96"
    //api.logInfo("filters:", filters)
    itemList = api.find("PGI", 0, 200, null, *filters)
}

//if(itemList != null)api.logInfo("itemList:",itemList.size())
if (itemList != null)
    itemList.each {
//while(itemList?.hasNext())  {
        //it = itemList.next() as Map
        //api.logInfo("item",it)
        resultRow = [:]
        def productFamily = it[productFamilyAttributeName]//it.attribute45
        def productId = it[productIdAttributeName]//it.attribute66
        def sapLongDesc = it[usageItemLongdescAttributeName]//it.attribute71
        def usageItem = it[usageItemAttributeName]//it.attribute69
        def dataCenter = it[datacenterAttributeName]//it.attribute62
        def billingTerm = it[billingTermAttributeName]//it.attribute63
        def billingTermFrequency = it[billingTermFrequencyAttributeName]//it.attribute96
        def billingTermUOM = it[billingTermUOMAttributeName]//it.attribute65
        api.logInfo("productFamily:" + productFamily + "	productId:" + productId
                +"	sapLongDesc" + sapLongDesc + "	usageItem:" + usageItem
                +"	dataCenter:" + dataCenter + "	billingTerm:" + billingTerm
                +"	billingTermFrequency:" + billingTermFrequency + "	billingTermUOM:", billingTermUOM)
        def currency = "USD"
        def region = "NA"
        def price = it[resultPriceAttributeName]//getAttributeName("ResultPrice")//it.attribute38
        resultRow = [:]
        resultRow.put("product Family", productFamily)
        resultRow.put("product Id", productId)
        resultRow.put("Description (SAP description)", sapLongDesc)
        resultRow.put("Usage Item", usageItem)
        resultRow.put("Currency", currency)
        resultRow.put("Region", region)
        resultRow.put("Data Center", dataCenter)
        resultRow.put("Billing Term", billingTerm)
        resultRow.put("Billing Term Frequency", billingTermFrequency)
        resultRow.put("Billing Term UOM", billingTermUOM)
        resultRow.put("Price", price)//USD
        resultMatrix.addRow(resultRow)
        //2nd price - EUR
        currency = "EUR"
        region = "EMEA"
        price = it[resultPriceEURAttributeName]//getAttributeName("ResultPriceEUR")//it.attribute39
        resultRow = [:]
        resultRow.put("product Family", productFamily)
        resultRow.put("product Id", productId)
        resultRow.put("Description (SAP description)", sapLongDesc)
        resultRow.put("Usage Item", usageItem)
        resultRow.put("Currency", currency)
        resultRow.put("Region", region)
        resultRow.put("Data Center", dataCenter)
        resultRow.put("Billing Term", billingTerm)
        resultRow.put("Billing Term Frequency", billingTermFrequency)
        resultRow.put("Billing Term UOM", billingTermUOM)
        resultRow.put("Price", price)
        resultMatrix.addRow(resultRow)
        //3rd price - GBP
        currency = "GBP"
        region = "EMEA"
        price = it[resultPriceGBPAttributeName]//getAttributeName("ResultPriceGBP")//it.attribute40
        resultRow = [:]
        resultRow.put("product Family", productFamily)
        resultRow.put("product Id", productId)
        resultRow.put("Description (SAP description)", sapLongDesc)
        resultRow.put("Usage Item", usageItem)
        resultRow.put("Currency", currency)
        resultRow.put("Region", region)
        resultRow.put("Data Center", dataCenter)
        resultRow.put("Billing Term", billingTerm)
        resultRow.put("Billing Term Frequency", billingTermFrequency)
        resultRow.put("Billing Term UOM", billingTermUOM)
        resultRow.put("Price", price)
        resultMatrix.addRow(resultRow)
        //4th price - AUD
        currency = "AUD"
        region = "APAC"
        price = it[resultPriceAUDAttributeName]//getAttributeName("ResultPriceAUD")//it.attribute41
        resultRow = [:]
        resultRow.put("product Family", productFamily)
        resultRow.put("product Id", productId)
        resultRow.put("Description (SAP description)", sapLongDesc)
        resultRow.put("Usage Item", usageItem)
        resultRow.put("Currency", currency)
        resultRow.put("Region", region)
        resultRow.put("Data Center", dataCenter)
        resultRow.put("Billing Term", billingTerm)
        resultRow.put("Billing Term Frequency", billingTermFrequency)
        resultRow.put("Billing Term UOM", billingTermUOM)
        resultRow.put("Price", price)
        resultMatrix.addRow(resultRow)
        //5th price - CNY
        currency = "CNY"
        region = "APAC"
        price = it[resultPriceCNYAttributeName]//getAttributeName("ResultPriceCNY")//it.attribute42
        resultRow = [:]
        resultRow.put("product Family", productFamily)
        resultRow.put("product Id", productId)
        resultRow.put("Description (SAP description)", sapLongDesc)
        resultRow.put("Usage Item", usageItem)
        resultRow.put("Currency", currency)
        resultRow.put("Region", region)
        resultRow.put("Data Center", dataCenter)
        resultRow.put("Billing Term", billingTerm)
        resultRow.put("Billing Term Frequency", billingTermFrequency)
        resultRow.put("Billing Term UOM", billingTermUOM)
        resultRow.put("Price", price)
        resultMatrix.addRow(resultRow)
        //6th price - JPY
        currency = "JPY"
        region = "APAC"
        price = it[resultPriceJPYAttributeName]//getAttributeName("ResultPriceJPY")//it.attribute43
        resultRow = [:]
        resultRow.put("product Family", productFamily)
        resultRow.put("product Id", productId)
        resultRow.put("Description (SAP description)", sapLongDesc)
        resultRow.put("Usage Item", usageItem)
        resultRow.put("Currency", currency)
        resultRow.put("Region", region)
        resultRow.put("Data Center", dataCenter)
        resultRow.put("Billing Term", billingTerm)
        resultRow.put("Billing Term Frequency", billingTermFrequency)
        resultRow.put("Billing Term UOM", billingTermUOM)
        resultRow.put("Price", price)
        resultMatrix.addRow(resultRow)
        //7th price - EMEA USD
        currency = "USD"
        region = "EMEA"
        price = it[resultPriceAttributeName]//getAttributeName("ResultPrice")//it.attribute38
        resultRow = [:]
        resultRow.put("product Family", productFamily)
        resultRow.put("product Id", productId)
        resultRow.put("Description (SAP description)", sapLongDesc)
        resultRow.put("Usage Item", usageItem)
        resultRow.put("Currency", currency)
        resultRow.put("Region", region)
        resultRow.put("Data Center", dataCenter)
        resultRow.put("Billing Term", billingTerm)
        resultRow.put("Billing Term Frequency", billingTermFrequency)
        resultRow.put("Billing Term UOM", billingTermUOM)
        resultRow.put("Price", price)
        resultMatrix.addRow(resultRow)
        //8th price - APAC USD
        currency = "USD"
        region = "APAC"
        price = it[resultPriceAttributeName]//getAttributeName("ResultPrice")//it.attribute38
        resultRow = [:]
        resultRow.put("product Family", productFamily)
        resultRow.put("product Id", productId)
        resultRow.put("Description (SAP description)", sapLongDesc)
        resultRow.put("Usage Item", usageItem)
        resultRow.put("Currency", currency)
        resultRow.put("Region", region)
        resultRow.put("Data Center", dataCenter)
        resultRow.put("Billing Term", billingTerm)
        resultRow.put("Billing Term Frequency", billingTermFrequency)
        resultRow.put("Billing Term UOM", billingTermUOM)
        resultRow.put("Price", price)
        resultMatrix.addRow(resultRow)
    }


def getAttributeName(elementName) {
    def retValue = (api.local.mappingData.get(elementName) ? api.local.mappingData.get(elementName).fieldName : elementName)
    return retValue
}

resultMatrix.enableClientFilter = true
//api.logInfo("returning result matrix")
return resultMatrix