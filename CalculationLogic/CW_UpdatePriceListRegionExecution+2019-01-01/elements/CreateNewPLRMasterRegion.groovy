import net.pricefx.common.api.InputType

String message


List filter = [
        Filter.equal("name", "PriceListRegion"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()
List pxRecords = []
List masterLaunchRegionConfig = out.MasterLaunchRegionConfig
Map masterLaunchDatesinConfig = out.MasterLaunchRegionConfig?.collectEntries{[(it.name) : it.attribute6]}
List masterRegions = masterLaunchRegionConfig?.collect { it.name }
List channelRegions = out.ChannelLaunchRegionConfig?.collect { it.name }
for (masterRegion in masterRegions) {
  regionInfo = masterLaunchRegionConfig?.find{it.name == masterRegion}
 selectedRegionRecords = selectedRecords?.findAll { it.attribute3 == masterRegion  }
  if (!selectedRegionRecords || selectedRegionRecords?.size() == 0) {
    selectedSKUs = selectedRecords?.collect { it.sku }?.unique()
    for (sku in selectedSKUs) {
      skuInfo = selectedRecords?.find{it.sku == sku}
       pxRecords << [
                    "PriceListRegion",
                    skuInfo.sku,
                    skuInfo.attribute4,
                    skuInfo.attribute5,
                    skuInfo.attribute6,
                    masterRegion,
                    masterRegion?.split(' ')?.getAt(1)?.replaceAll("[0-9]",""),
               masterLaunchDatesinConfig[masterRegion]
            ]
    }
  }
}



if (pxRecords) {
    payload = [
            "data": [
                    "data"   : pxRecords,
                    "header" : [
                            "name",
                            "sku",
                            "attribute4",
                            "attribute5",
                            "attribute6",
                            "attribute3",
                            "attribute1",
                            "attribute7",
                    ],
                    "options": [
                            "detectJoinFields"    : false,
                            "maxJoinFieldsLengths": []
                    ]
            ]]
    api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
     return
}


