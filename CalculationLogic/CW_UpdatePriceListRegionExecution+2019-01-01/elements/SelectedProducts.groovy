import net.pricefx.common.api.InputType

api.local.selectedProducts = getProductIdsForProductFilter()
return

List getProductIdsForProductFilter() {
    def productGroup = input[Constants.PRODUCT_INPUT_NAME]
    def products = []
    if (productGroup) {
        if (productGroup.productFilterCriteria) {
            productFilter = api.filterFromMap(productGroup.productFilterCriteria)
        } else {
            productFilter = Filter.equal(productGroup.productFieldName, productGroup.productFieldValue)
        }
        products = api.find('P', 0, api.maxFindResultsLimit, null, null, productFilter)
    }
    return products.sku
}
