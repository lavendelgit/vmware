import net.pricefx.common.api.InputType

List filter = [
        Filter.equal("name", "ValidCombination"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()
List pxRecords = []
List channelLaunchRegionConfig = out.ChannelLaunchRegionConfig
List channelRegions = channelLaunchRegionConfig?.collect { it.name }
for (channelRegion in channelRegions) {
    regionInfo = channelLaunchRegionConfig?.find { it.name == channelRegion }
    selectedRegionRecords = selectedRecords?.findAll { it.attribute13 == channelRegion }
    if (!selectedRegionRecords || selectedRegionRecords?.size() == 0) {
        selectedSKUs = selectedRecords?.collect { it.sku }?.unique()
        for (sku in selectedSKUs) {
            skuInfo = selectedRecords?.find { it.sku == sku }
            pxRecords << [
                    "ValidCombination",
                    skuInfo.sku,
                    skuInfo.attribute11,
                    skuInfo.attribute12,
                    skuInfo.attribute1,
                    skuInfo.attribute2,
                    regionInfo.attribute9,
                    regionInfo.attribute9,
                    regionInfo.attribute9,
                    regionInfo.attribute9,
                    regionInfo.attribute4,
                    regionInfo.attribute5,
                    channelRegion
            ]
        }
    }
}


if (pxRecords) {
    payload = [
            "data": [
                    "data"   : pxRecords,
                    "header" : [
                            "name",
                            "sku",
                            "attribute11",
                            "attribute12",
                            "attribute1",
                            "attribute2",
                            "attribute3",
                            "attribute4",
                            "attribute5",
                            "attribute6",                            
                            "attribute8",
                            "attribute10",
                            "attribute13"
                    ],
                    "options": [
                            "detectJoinFields"    : false,
                            "maxJoinFieldsLengths": []
                    ]
            ]]
    api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
    return
}


