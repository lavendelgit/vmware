
import net.pricefx.common.api.InputType

api.local.errorMessages = []
if (!api.local.selectedProducts && api.local.selectedProducts?.size() == 0) {
    api.local.errorMessages << "Please select Products!"
}
List filter = [
        Filter.equal("name", "PriceListRegion"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()
List masterLaunchDateDiffSKUs = []
List masterEndDateDiffSKUs = []
Map masterLaunchDatesinConfig = out.MasterLaunchRegionConfig?.collectEntries{[(it.name) : it.attribute6]}
Map masterEndDatesinConfig = out.MasterLaunchRegionConfig?.collectEntries{[(it.name) : it.attribute7]}
List masterLaunchRegions = out.MasterLaunchRegionConfig?.collect { it.name }
launchRegions = selectedRecords?.findAll {masterLaunchRegions.contains(it.attribute3) }
for(region in launchRegions) {
    if(region.attribute8 != masterEndDatesinConfig[region.attribute3]) {
        masterLaunchDateDiffSKUs << region
    }
}
List record = []
for( regionRecord in masterLaunchDateDiffSKUs) {
    selectedDate = out.MasterLaunchRegionConfig?.find{it.name == regionRecord.attribute3}?.attribute7
    record << populatePayload(regionRecord, selectedDate)
}

if(record?.size() > 0) {
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(record), true)
}

Map populatePayload(Map rowData, def selectedDate) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"    : rowData?.typedId,
                    "attribute8": selectedDate
            ],
            "oldValues"    : [
                    "version": rowData?.version,
                    "typedId": rowData?.typedId,
            ]
    ]
}

