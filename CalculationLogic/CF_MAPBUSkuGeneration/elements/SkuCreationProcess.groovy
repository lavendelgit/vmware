if (api.isSyntaxCheck()) return

def BU = out.BU


def ServiceMap
def SubServiceMap
def PaymentTypeMap
def baseSKU
def termYears
def segment
def metric

ServiceMap = out.BUServiceMap
def ServiceList = (ServiceMap) ? ServiceMap.keySet() : []
def datacenterMap = out.BUDatacenterMap
def datacenterList = (datacenterMap) ? datacenterMap.keySet() : []
def licenseMap = out.BULicenseMap
def licenseList = (licenseMap) ? licenseMap.keySet() : []
def paymenttermMap = out.BUPaymentTermMap
def paymenttermList = (paymenttermMap) ? paymenttermMap.keySet() : []
PaymentTypeMap = out.BUPaymentTypeMap
def paymentTypeList = (PaymentTypeMap) ? PaymentTypeMap.keySet() : []
def productTypeMap = out.BUProductTypeMap
def productTypeList = (productTypeMap) ? productTypeMap.keySet() : []
def quantityMap = out.BUQuantityMap
def quantityList = (quantityMap) ? quantityMap.keySet() : []
SubServiceMap = out.BUSubServiceMap
def SubServiceList = (SubServiceMap) ? SubServiceMap.keySet() : []
def tierMap = out.BUTierMap
def tierList = (tierMap) ? tierMap.keySet() : []
def versionMap = out.BUVerionMap
def versionList = (versionMap) ? versionMap.keySet() : []


ServiceList.each { Service ->
    datacenterList.each { Datacenter ->
        quantityList.each { Quantity ->
            SubServiceList.each { SubService ->
                paymenttermList.each { PaymentTerm ->
                    paymentTypeList.each { PaymentType ->
                        tierList.each { Tier ->
                            licenseList.each { License ->
                                versionList.each { Version ->
                                    productTypeList.each { ProductType ->


                                        def sku = libs.SkuCreationUtilsLib.MAPBUskucreation.createMAPBUSKU(BU, Service, Datacenter, Quantity, SubService,
                                                PaymentTerm, PaymentType, Tier, License, Version, ProductType)

                                        def ShortDesc = libs.SkuCreationUtilsLib.MAPBUskucreation.createShortDesc(License, Service, Quantity, SubService,
                                                PaymentType, PaymentTerm, ServiceMap, SubServiceMap, PaymentTypeMap)


                                        def LongDesc = libs.SkuCreationUtilsLib.MAPBUskucreation.createLongDesc(License, Service, Quantity, SubService,
                                                PaymentType, PaymentTerm)

                                        baseSKU = ServiceMap[Service].attribute2

                                        if (PaymentTerm) termYears = PaymentTerm / 12
                                        if (License) segment = licenseMap[License]
                                        if (SubService) metric = SubServiceMap[SubService]

//Insert records into MAPBUProducts PX table
                                        libs.SkuCreationUtilsLib.MAPBUskucreation.addOrUpdateMAPBUPXTable(sku, Service, Datacenter, Quantity, SubService,
                                                PaymentTerm, PaymentType, Tier, License, Version, ProductType, ShortDesc, LongDesc, baseSKU, termYears, segment, metric)


                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
    }

}