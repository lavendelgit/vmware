def BU = out.BU
def versionMap

versionMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BUVersions", BU)

return versionMap