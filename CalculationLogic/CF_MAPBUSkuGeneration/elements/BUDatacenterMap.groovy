def BU = out.BU
def datacenterMap

datacenterMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BUDataCenter", BU)

return datacenterMap