def BU = out.BU
def paymenttypeMap

paymenttypeMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BUPaymentType", BU)

return paymenttypeMap