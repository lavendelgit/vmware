def BU = out.BU
def tierMap

tierMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BUTiers", BU)

return tierMap