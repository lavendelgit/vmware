def BU = out.BU
def paymenttermMap

paymenttermMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BUPaymentTerm", BU)

return paymenttermMap