def BU = out.BU
def quantityMap

quantityMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BUQuantity", BU)

return quantityMap