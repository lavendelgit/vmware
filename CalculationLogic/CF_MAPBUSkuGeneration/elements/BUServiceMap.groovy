def BU = out.BU
def serviceMap
def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BUServices").id),
        Filter.equal("key1", "CN")
]
//serviceMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BUServices",BU)
def buServicesList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2"], *filter)

serviceMap = buServicesList.collectEntries {
    [(it.key2): it]
}
return serviceMap