def BU = out.BU
def producttypeMap

producttypeMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BUProductType", BU)

return producttypeMap