def BU = out.BU
def subserviceMap

subserviceMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BUSubServices", BU)

return subserviceMap