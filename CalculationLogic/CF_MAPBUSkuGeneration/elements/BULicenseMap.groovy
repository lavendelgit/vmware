def BU = out.BU
def licenseMap

licenseMap = libs.vmwareUtil.LoadPPData.getMLTV2AsMap("BULicenseClass", BU)

return licenseMap