def versionMap


def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BUVersions").id),
        Filter.equal("key1", out.BU),
        Filter.equal("attribute1", "Yes")
]

def versionList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1"], *filter)

versionMap = versionList.collectEntries {
    [(it.key2): it]
}

return versionMap