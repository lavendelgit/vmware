def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BUPaymentType").id),
        Filter.equal("key1", out.BU),
        Filter.equal("attribute2", "Yes")
]
def buPaymentTypeList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key2", "attribute1"], *filter)

paymentTypeMap = buPaymentTypeList.collectEntries {
    [(it.key2): it]
}
return paymentTypeMap