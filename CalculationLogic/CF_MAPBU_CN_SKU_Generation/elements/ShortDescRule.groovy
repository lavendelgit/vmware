def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("MAPBUShortDesc").id),
        Filter.equal("key1", out.BU),
        Filter.equal("attribute7", "Yes")
]

api.local.ShortDesc = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6", "attribute7"], *filter)


return