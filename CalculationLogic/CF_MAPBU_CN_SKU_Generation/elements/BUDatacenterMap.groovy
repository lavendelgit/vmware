Map datacenterMap = [:]
def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BUDataCenter").id),
        Filter.equal("key1", out.BU),
        Filter.equal("attribute2", "Yes")
]
def buServicesList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1",], *filter)

datacenterMap = buServicesList.collectEntries {
    [(it.key2): it]
}

return datacenterMap