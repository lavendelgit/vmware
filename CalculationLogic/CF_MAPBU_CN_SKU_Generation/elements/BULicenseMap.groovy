def licenseMap

def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BULicenseClass").id),
        Filter.equal("key1", out.BU),
        Filter.equal("attribute3", "Yes")
]

def buLicenseList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3"], *filter)

licenseMap = buLicenseList.collectEntries {
    [(it.key2): it]
}
return licenseMap