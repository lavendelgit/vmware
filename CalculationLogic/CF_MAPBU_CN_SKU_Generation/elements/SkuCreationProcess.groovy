if (api.isSyntaxCheck()) return

def BU = out.BU


def ServiceMap
def SubServiceMap
def PaymentTypeMap
def baseSKU
def termYears
def segment
def metric

ServiceMap = out.BUServiceMap
def ServiceList = (ServiceMap) ? ServiceMap.keySet() : []

def datacenterMap = out.BUDatacenterMap
def datacenterList = (datacenterMap) ? datacenterMap.keySet() : []
def licenseMap = out.BULicenseMap
def licenseList = (licenseMap) ? licenseMap.keySet() : []
def paymenttermMap = out.BUPaymentTermMap
def paymenttermList = (paymenttermMap) ? paymenttermMap.keySet() : []
PaymentTypeMap = out.BUPaymentTypeMap
def paymentTypeList = (PaymentTypeMap) ? PaymentTypeMap.keySet() : []
def productTypeMap = out.BUProductTypeMap
def productTypeList = (productTypeMap) ? productTypeMap.keySet() : []
def quantityMap = out.BUQuantityMap
def quantityList = (quantityMap) ? quantityMap.keySet() : []
SubServiceMap = out.BUSubServiceMap
def SubServiceList = (SubServiceMap) ? SubServiceMap.keySet() : []
def tierMap = out.BUTierMap
def tierList = (tierMap) ? tierMap.keySet() : []
def versionMap = out.BUVersionMap
def versionList = (versionMap) ? versionMap.keySet() : []

def shortDescRuleList = api.local.ShortDesc

ServiceList.each { Service ->
    datacenterList.each { Datacenter ->
        quantityList.each { Quantity ->
            SubServiceList.each { SubService ->
                paymenttermList.each { PaymentTerm ->
                    paymentTypeList.each { PaymentType ->
                        tierList.each { Tier ->
                            licenseList.each { License ->
                                versionList.each { Version ->
                                    productTypeList.each { ProductType ->
                                        def sku = Library.createMAPBUSKU(BU, Service, Datacenter, Quantity, SubService,
                                                PaymentTerm, PaymentType, Tier, License, Version, ProductType)
                                        shortDesc = Library.createShortDesc(shortDescRuleList, License, Service, Quantity, SubService, PaymentType, PaymentTerm)
                                        baseSKU = ServiceMap[Service].attribute2


                                        if (PaymentTerm) termYears = (PaymentTerm as BigDecimal) / 12
                                        if (License) segment = licenseMap[License]?.attribute1
                                        if (SubService) metric = SubServiceMap[SubService]?.attribute1
                                        Library.addOrUpdateMAPBUPXTable(sku, Service, Datacenter, Quantity, SubService,
                                                PaymentTerm, PaymentType, Tier, License, Version, ProductType, shortDesc, baseSKU, termYears, segment, metric)


                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
    }

}