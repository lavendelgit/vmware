def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BUTiers").id),
        Filter.equal("key1", out.BU),
        Filter.equal("attribute2", "Yes")
]
def buTiersList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key2", "attribute1"], *filter)

tiersMap = buTiersList.collectEntries {
    [(it.key2): it]
}

return tiersMap