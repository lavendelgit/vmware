def createMAPBUSKU(BU, Service, Datacenter, Quantity, SubService, PaymentTerm, PaymentType, Tier, License, Version, ProductType) {

    def sku = BU + Service + "-" + Datacenter + Quantity + SubService + "-" +
            PaymentTerm + PaymentType + Tier + "-" + License + Version + ProductType
    return sku
}


def addOrUpdateMAPBUPXTable(sku, Service, Datacenter, Quantity, SubService, PaymentTerm, PaymentType, Tier, License, Version, ProductType, ShortDesc, baseSKU, termYears, segment, metric) {

    def mapbuRecord = [
            name       : "MAPBUProducts",
            sku        : sku,
            attribute11: ShortDesc,
            //attribute12 : LongDesc,
            attribute1 : Service,
            attribute2 : Datacenter,
            attribute3 : Quantity,
            attribute4 : SubService,
            attribute5 : PaymentTerm,
            attribute6 : PaymentType,
            attribute7 : Tier,
            attribute8 : License,
            attribute9 : Version,
            attribute10: ProductType,
            attribute13: termYears,
            attribute14: segment,
            attribute15: metric,
            attribute16: baseSKU

    ]
    if (mapbuRecord) api.addOrUpdate("PX20", mapbuRecord)
}

def conditionalShortDesc(shortDesc, operator, value, desc, conditionName) {
    switch (operator) {
        case "=":

            if (conditionName.equals(value)) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<=":
            if (conditionName <= value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case ">":
            if (conditionName > value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<>":
            if (conditionName != value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break

    }
    return shortDesc
}


def createShortDesc(ruleList, license, service, quantity, subService, paymentType, paymentTerm) {
    def shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break

            case "Conditional":
                if (rule.attribute3.equals("Service")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, service)
                }
                if (rule.attribute3.equals("LicenseClass")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, license)
                }
                if (rule.attribute3.equals("Quantity")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, quantity)
                }
                if (rule.attribute3.equals("SubService")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, subService)
                }
                if (rule.attribute3.equals("PaymentTerm")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, paymentTerm)
                }
                if (rule.attribute3.equals("PaymentType")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, paymentType)
                }

                break
            case "Replace":
                if ((rule.attribute2.equals("Quantity")) && (rule.attribute3.equals("Quantity"))) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, quantity, quantity)
                }
                shortDesc = shortDesc ? shortDesc + " " : shortDesc
                break

        }
    }
    return shortDesc
}
