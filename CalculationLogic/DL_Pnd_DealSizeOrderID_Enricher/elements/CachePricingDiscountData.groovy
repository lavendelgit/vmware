if(!api.global.queriedPriceingDiscount) {
    List filter = []
    api.global.queriedPriceingDiscount = [:]
    if (api.currentItem("incremental")) {
        filter << [Filter.greaterOrEqual("lastUpdateDate", api.currentItem("incLoadDate"))]
    }
    def ctx = api.getDatamartContext()
    def dm = ctx.getDataSource("PricingandDiscounting")
    def query = ctx?.newQuery(dm, true)
    query.select("OrderId")
    query.select("GrossBookingsLC")
    streamResult = ctx.streamQuery(query)

    while (streamResult?.next()) {
        record =  streamResult?.get()
        String key = record?.OrderId
        api.global.queriedPriceingDiscount[key]=record?.GrossBookingsLC
    }
    streamResult?.close()
}
return