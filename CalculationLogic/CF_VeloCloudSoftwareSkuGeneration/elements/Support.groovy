def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("SupportOptions").id)
]
api.local.supportList = api.find("LTV", 0, api.getMaxFindResultsLimit(), "name",
        ["name"], *filter).collect { it.name }


return
