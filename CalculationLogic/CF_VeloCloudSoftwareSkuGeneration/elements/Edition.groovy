def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("EditionOptionsNew").id)
]
api.local.editionList = api.find("MLTV", 0, api.getMaxFindResultsLimit(), "name",
        ["name"], *filter).collect { it.name }


return
