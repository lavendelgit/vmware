def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("SoftwareOptions").id),
        Filter.isNotEmpty("name")
]
api.local.softwareList = api.find("LTV", 0, api.getMaxFindResultsLimit(), "name",
        ["name"], *filter).collect { it.name }


return
