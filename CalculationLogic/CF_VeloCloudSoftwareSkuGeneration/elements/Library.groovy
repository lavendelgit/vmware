def addOrUpdateSoftwareSKU(sku, shortDesc, longDesc, bandwidth, edition, softwareRun, gateway, support, service, paymentTermMonths, paymentTermYears, paymentFrequency, license, renewalOption, itemType) {
    def pxRecord = [
            name       : "VCSoftwareProducts",
            sku        : sku,
            attribute1 : shortDesc,
            attribute2 : bandwidth,
            attribute3 : edition,
            attribute4 : softwareRun,
            attribute5 : gateway,
            attribute6 : support,
            attribute7 : service,
            attribute8 : paymentTermMonths,
            attribute9 : paymentTermYears,
            attribute10: paymentFrequency,
            attribute11: license,
            attribute12: renewalOption,
            attribute13: itemType

    ]

    def descLength = longDesc.length()

    if (descLength < 1019 && descLength > 764) {
        def descRecord = [
                "attribute14": longDesc.substring(0, 255),
                "attribute15": longDesc.substring(255, 510),
                "attribute16": longDesc.substring(510, 764),
                "attribute17": longDesc.substring(764, descLength)

        ]
        pxRecord << descRecord
    } else {

        if (descLength < 765 && descLength > 510) {
            def descRecord = [
                    "attribute14": longDesc.substring(0, 255),
                    "attribute15": longDesc.substring(255, 510),
                    "attribute16": longDesc.substring(510, descLength)
            ]
            pxRecord << descRecord
        } else {
            if (descLength < 511 && descLength > 255) {
                def descRecord = [
                        "attribute14": longDesc.substring(0, 255),
                        "attribute15": longDesc.substring(255, descLength)
                ]
                pxRecord << descRecord
            } else {
                if (descLength < 256) {
                    def descRecord = [
                            "attribute14": longDesc
                    ]
                    pxRecord << descRecord
                }
            }
        }
    }
    if (pxRecord) api.addOrUpdate("PX20", pxRecord)
}


def createSoftwareSKU(prefix, bandwidth, edition, softwareRun, gateway, support, service, subscriptionMonths, paymentFrequency, renewalOption, licence) {
    prefix += bandwidth
    prefix += "-"
    prefix += edition
    if (softwareRun) {
        prefix += "-"
        prefix += softwareRun
    }
    //prefix += "-"
    if (gateway && gateway != " ") {
        prefix += gateway
        prefix += "-"
    } else {
        prefix += "-"
    }
    prefix += support
    prefix += service
    prefix += "-"
    prefix += subscriptionMonths
    prefix += paymentFrequency
    if (renewalOption && renewalOption != " ") {
        prefix += "-"
        prefix += renewalOption
    }
    prefix += "-"
    prefix += licence

    return prefix
}


def conditionalShortDesc(shortDesc, operator, value, desc, conditionName) {
    //api.trace("long desc","",shortDesc + "~"+operator+"~"+value+"~"+desc+"~"+conditionName)
    switch (operator) {
        case "=":

            if (conditionName.equals(value)) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<=":
            if (conditionName <= value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case ">":
            if (conditionName > value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<>":
            if (conditionName != value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break

    }
    return shortDesc
}

def createSoftwareShortDesc(ruleList, bandwidth, licence, subscriptionYears, renewalOption, paymentFreqDesc, softwareGateway, edition) {
    def shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                //shortDesc.append(rule.attribute6)
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break

            case "Conditional":
                if (rule.attribute3.equals("Bandwidth")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, bandwidth)
                }
                if (rule.attribute3.equals("LicenseClass")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, licence)
                }
                if (rule.attribute3.equals("PaymentFreqDesc")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, paymentFreqDesc)
                }
                if (rule.attribute3.equals("RenewalOption")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, renewalOption)
                }
                if (rule.attribute3.equals("Software-Gateway")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, softwareGateway)
                }
                if (rule.attribute3.equals("SoftwareEdition")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, edition)
                }
                if (rule.attribute3.equals("SubscriptionYears")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, subscriptionYears)
                }
                break

        }
    }
    return shortDesc
}


def createSoftwareLongDesc(ruleList, subscriptionYears, paymentFreqDesc, bandwidth, gateway, licence, renewalOption, service, softwareGateway, edition, support) {
    def shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":

                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break

            case "Conditional":
                if (rule.attribute3.equals("Bandwidth")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, bandwidth)
                }
                if (rule.attribute3.equals("Gateway")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, gateway)
                }
                if (rule.attribute3.equals("LicenseClass")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, licence)
                }
                if (rule.attribute3.equals("PaymentFreqDesc")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, paymentFreqDesc)
                }
                if (rule.attribute3.equals("RenewalOption")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, renewalOption)
                }
                if (rule.attribute3.equals("Service")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, service)
                }
                if (rule.attribute3.equals("Software-Gateway")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, softwareGateway)
                }
                if (rule.attribute3.equals("SoftwareEdition")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, edition)
                }
                if (rule.attribute3.equals("SubscriptionYears")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, subscriptionYears)
                }
                if (rule.attribute3.equals("Support")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, support)
                }
                break

        }
    }
    return shortDesc
}

