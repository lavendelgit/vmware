List filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BandwidthOptions").id)
]
api.local.bandwidthList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key1",
        ["key1", "key2"], true, *filter).collect { it.key1 }

return