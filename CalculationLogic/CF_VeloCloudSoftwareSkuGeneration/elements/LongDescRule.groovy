def softwarefilter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VCSoftwareLongDesc").id),
        Filter.equal("key1", "Software")
]

api.local.softwareLongDesc = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6"], *softwarefilter)


return