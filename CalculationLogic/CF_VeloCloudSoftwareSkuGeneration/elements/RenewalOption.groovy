def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("RenewalOptions").id)
]
api.local.renewalList = api.find("LTV", 0, api.getMaxFindResultsLimit(), "name",
        ["name"], *filter).collect { it.name }


return
