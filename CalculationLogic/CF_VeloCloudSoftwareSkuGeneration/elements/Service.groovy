def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("ServiceLevelFactor").id)
]
api.local.serviceList = api.find("LTV", 0, api.getMaxFindResultsLimit(), "name",
        ["name"], *filter).collect { it.name }


return
