def prefix = api.local.itemMap["Software"]
def bandwidthList = api.local.bandwidthList
def editionList = api.local.editionList
def softwareList = api.local.softwareList
def gatewayList = api.local.gatewayList
def supportList = api.local.supportList
def serviceList = api.local.serviceList
def subscriptionMonthsList = api.local.subscriptionMap["Software"]
def paymentTypeList = api.local.paymentTypeList
def renewalList = api.local.renewalList
def licenseList = api.local.licenseList


def softwareShortDescRuleList = api.local.softwareShortDesc
def softwareLongDescRuleList = api.local.softwareLongDesc

def softwareSKU
def softwareShortDesc
def softwareLongDesc
def subscriptionYears
def softwareGateway
def paymentFreqDesc

bandwidthList.each { bandwidth ->
    editionList.each { edition ->
        softwareList.each { softwareRun ->
            gatewayList.each { gateway ->
                supportList.each { support ->
                    serviceList.each { service ->
                        subscriptionMonthsList.each { subscriptionMonths ->
                            paymentTypeList.each { paymentFrequency ->
                                renewalList.each { renewalOption ->
                                    licenseList.each { licence ->

                                        subscriptionYears = api.local.subscriptionYearsMap["Software" + "~" + subscriptionMonths]

                                        if (softwareRun && gateway) {
                                            softwareGateway = softwareRun + "-" + gateway
                                        } else if (softwareRun) {
                                            softwareGateway = softwareRun
                                        } else if (gateway) {
                                            softwareGateway = gateway
                                        }

                                        paymentFreqDesc = api.local.paymentDescMap[paymentFrequency]

                                        softwareSKU = Library.createSoftwareSKU(prefix, bandwidth, edition, softwareRun, gateway, support, service, subscriptionMonths, paymentFrequency, renewalOption, licence)
                                        softwareShortDesc = Library.createSoftwareShortDesc(softwareShortDescRuleList, bandwidth, licence, subscriptionYears, renewalOption, paymentFreqDesc, softwareGateway, edition)
                                        softwareLongDesc = Library.createSoftwareLongDesc(softwareLongDescRuleList, subscriptionYears, paymentFreqDesc, bandwidth, gateway, licence, renewalOption, service, softwareGateway, edition, support)
                                        if (softwareSKU) Library.addOrUpdateSoftwareSKU(softwareSKU, softwareShortDesc, softwareLongDesc, bandwidth, edition, softwareRun, gateway, support, service, subscriptionMonths, subscriptionYears, paymentFrequency, licence, renewalOption, "Software")


                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
    }
}

return

