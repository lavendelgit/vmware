def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("GatewayOptions").id)
]
api.local.gatewayList = api.find("LTV", 0, api.getMaxFindResultsLimit(), "name",
        ["name"], *filter).collect { it.name }


return
