((Constants.FACTOR_ATTRIBUTE_MAPPING)?.keySet()).each { factor ->
    api.local[Constants.FACTOR_ATTRIBUTE_MAPPING[factor]] = Lib.cacheAndPopulateTermUoMAdjustments(factor)
}
return
