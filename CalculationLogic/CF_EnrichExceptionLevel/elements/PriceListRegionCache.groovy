api.local.priceListRegionCache = [:]
String attribute = ""
Map currencyDetails = [:]
List fields = ["sku", "attribute1", "attribute2"]
List filter = [
        Filter.equal("name", "PriceListRegion"),
        Filter.in("sku", api.local.stdProducts?.collect { (it.PartNumber) ?: (it.sku) })
]
productPriceListRegionStream = api.stream("PX20", "sku", fields, *filter)
productsPriceListRegionCache = productPriceListRegionStream?.collect { row -> row }
productPriceListRegionStream?.close()
for (standardProduct in productsPriceListRegionCache) {
    currencyDetails = [
            "Currency"       : standardProduct.attribute1,
            "PriceListRegion": standardProduct.attribute2
    ]
    if (api.local.priceListRegionCache[standardProduct.sku]) {
        api.local.priceListRegionCache[standardProduct.sku] << currencyDetails
    } else {
        api.local.priceListRegionCache[standardProduct.sku] = [currencyDetails]
    }
}
return

