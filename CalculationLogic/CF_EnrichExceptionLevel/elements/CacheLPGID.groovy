List lpgLists = []
int start = 0
int maxResults = api.getMaxFindResultsLimit()
while (items = api.find("PG", start, maxResults, null, [Constants.ID_FIELD], Filter.equal(Constants.STATUS_FIELD, Constants.READY))) {
    start += items.size()
    lpgLists.addAll(items.id)
}
return lpgLists
