List products = api.local.stdProducts?.Product?.unique()
List filters = [
        Filter.equal(Constants.NAME_FIELD, Constants.STANDARD_ADJUSTMENT_PX_TABLE),
        Filter.or(
                Filter.in(Constants.PRODUCT, products),
                Filter.in(Constants.PRODUCT, "*")
        )
]
stdAdjustmentStream = api.stream("PX50", "sku,attribute41", *filters)
api.local.stdAdjustmentsCache = stdAdjustmentStream?.collect { row -> row }
stdAdjustmentStream?.close()
return