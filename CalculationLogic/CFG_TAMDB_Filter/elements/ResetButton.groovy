import net.pricefx.common.api.InputType

api.local.filterData = api.findLookupTableValues(Constants.TAM_FILTER_CP_TABLE)
if (api.local.filterData) {
    def ce = api.createConfiguratorEntry()
    ce.createParameter(InputType.BUTTON, Constants.BUTTON_NAME_RESET_FILTER_SETUP)
    ce?.firstInput?.label = (Constants.BUTTON_NAME_RESET_FILTER_SETUP)
    return ce
}