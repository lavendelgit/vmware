import net.pricefx.server.dto.calculation.ContextParameter
ConfiguratorEntry configurationEntry = api.createConfiguratorEntry(InputType.USERENTRY, Constants.PERCENTAGE_CHANGE_LABEL)
ContextParameter contextParameter = configurationEntry.getFirstInput()
return configurationEntry