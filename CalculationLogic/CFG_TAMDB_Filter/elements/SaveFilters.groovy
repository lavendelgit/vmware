boolean isSaveTriggered = out.SaveButton.firstInput.value
List tableRows = []

if (isSaveTriggered) {
    String percentageChange = out.PercentageChange?.firstInput?.value
    Map filter = out.DataFilter?.firstInput?.value
    libs.vmwareUtil.TAMDBUtils.saveFilter(percentageChange, filter)

    Map selectedFilters = libs.vmwareUtil.TAMDBUtils.getTAMFilter(Constants.TAM_FILTER_CP_TABLE)

    def ce = api.createConfiguratorEntry(InputType.INPUTMATRIX, Constants.FILTER_SELECTION)
    ce?.getFirstInput()?.addParameterConfigEntry(Constants.COLUMNS, [Constants.COLUMN_DATA_FILTER, Constants.COLUMN_PERCENTAGE_CHANGE])
    ce?.getFirstInput()?.addParameterConfigEntry(Constants.READ_ONLY_COLUMNS, [Constants.COLUMN_DATA_FILTER, Constants.COLUMN_PERCENTAGE_CHANGE])
    ce?.getFirstInput()?.addParameterConfigEntry(Constants.CAN_MODIFY_ROWS, false)
    ce?.getFirstInput()?.addParameterConfigEntry(Constants.DISABLE_ROWS_SELECTION, true)
    selectedFilters?.each {
        filterMap = it?.value?.DataFilter
        filters = api.filterFromMap(api.jsonDecode(filterMap))
        filterDisplay = filters.toString()
        it.value.DataFilter = filterDisplay
        tableRows << it?.value
    }
    ce?.getFirstInput()?.setValue(tableRows)
    out.SaveButton?.firstInput?.value = false
    return ce
}
return
