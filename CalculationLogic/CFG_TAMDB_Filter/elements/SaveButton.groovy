import groovy.transform.Field
import net.pricefx.common.api.InputType

ConfiguratorEntry ce = api.createConfiguratorEntry(InputType.BUTTON, Constants.BUTTON_NAME_SAVE_FILTER_SETUP)
ce?.firstInput?.label = Constants.BUTTON_NAME_SAVE_FILTER_SETUP_LABEL
return ce