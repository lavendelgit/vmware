Map reset = [:]
Long tableId = api.findLookupTable(Constants.TAM_FILTER_CP_TABLE)?.id
if (out.ResetButton.firstInput.value == true) {
    for (filterData in api.local.filterData) {
        reset = [
                (Constants.LOOKUP_TABLE_ID)                : tableId,
                (Constants.LOOKUP_TABLE_NAME)              : (Constants.TAM_FILTER_CP_TABLE),
                (Constants.ID)                             : filterData?.id,
                (Constants.VERSION)                        : filterData?.version,
                (Constants.NAME)                           : filterData?.name,
                (Constants.CP_TAM_FILTER_PERCENTAGE_CHANGE): filterData?.attributeExtension___PercentageChange,
                (Constants.CP_TAM_FILTER_DATA_FILTER)      : filterData?.attributeExtension___DataFilter
        ]
        return api.delete("JLTV", reset)
    }
}