ConfiguratorEntry configurationEntry = api.createConfiguratorEntry()
configurationEntry.createParameter(InputType.DMFILTERBUILDER, Constants.DATA_FILTER_LABEL)
        .addParameterConfigEntry(Constants.DM_SOURCE_NAME, Constants.PRICING_DISCOUNTING_DM)
return configurationEntry
