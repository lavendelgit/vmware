if (api.local.pxRecords) {
    payload = [
            "data": [
                    "data"   : api.local.pxRecords,
                    "header" : [
                            "name",
                            "sku",
                            "attribute1",
                            "attribute2",
                            "attribute3",
                            "attribute4",
                            "attribute5",
                            "attribute6",
                            "attribute7",
                            "attribute8",
                            "attribute9",
                            "attribute10",
                            "attribute11",
                            "attribute12",
                            "attribute13",
                            "attribute14",
                            "attribute15",
                            "attribute16",
                            "attribute17",
                            "attribute18",
                            "attribute19",
                            "attribute20",
                            "attribute21",
                            "attribute22",
                            "attribute23",
                            "attribute25",
                            "attribute24"
                    ],
                    "options": [
                            "detectJoinFields"    : false,
                            "maxJoinFieldsLengths": []
                    ]
            ]]
    api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
    return
}