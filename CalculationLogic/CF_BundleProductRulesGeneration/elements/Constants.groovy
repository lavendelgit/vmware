@Field final String RULES_TABLE_NAME = "BundleProductRules"
@Field final String UNV_RULE_TABLE_NAME = "UniversalRule"
@Field final Integer PRODUCT_INDEX = 0
@Field final Integer OFFER_INDEX = 1
@Field final Integer DATACENTER_INDEX = 2
@Field final Integer BASE_INDEX = 3
@Field final Integer METRIC_INDEX = 4
@Field final String TERMUOM_PLACEHOLDER = "#"
@Field final Integer TERM_UOM_INDEX = 6
@Field final Integer TERM_INDEX = 5
@Field final Integer PAYMENT_TYPE_INDEX = 7
@Field final Integer VOLUME_TIER_INDEX = 8
@Field final Integer SEGMENT_INDEX = 9
@Field final Integer VERSION_INDEX = 10
@Field final Integer PROMOTION_INDEX = 11
@Field final Integer OS_INDEX = 12
@Field final Integer QUANTITY_INDEX = 13
@Field final Integer PURCHASING_PROGRAM_INDEX = 14
@Field final Integer SUPPORT_TYPE_INDEX = 15
@Field final Integer SUPPORT_TIER_INDEX = 16
@Field final Integer CURRENCY_INDEX = 17
@Field final Integer REGION_INDEX = 18
@Field final Integer SUB_REGION_INDEX = 19
@Field final Integer HOSTING_INDEX = 20
@Field final Integer PROGRAM_OPTION_INDEX = 21


