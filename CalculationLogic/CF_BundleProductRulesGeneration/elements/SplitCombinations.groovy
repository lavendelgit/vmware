def batchSize = (Math.round(api.local.combinations?.size() / 5)) as Integer
api.local.combinationsBatches = api.local.combinations?.collate(batchSize, true)
return
