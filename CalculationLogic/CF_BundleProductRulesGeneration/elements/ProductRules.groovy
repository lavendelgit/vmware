api.local.pxRecords = []
def rulesGenLib = libs.stdProductLib.RulesGenHelper
int skuIndex = 0

for (rule in api.local.productRules) {
    pXParams = [
            "sku"               : Lib.generateKey(rule),
            "Product"           : rule[Constants.PRODUCT_INDEX],
            "Offer"             : rule[Constants.OFFER_INDEX],
            "DataCenter"        : rule[Constants.DATACENTER_INDEX],
            "Base"              : rule[Constants.BASE_INDEX],
            "Metric"            : rule[Constants.METRIC_INDEX],
            "Term"              : rule[Constants.TERM_INDEX],
            "PaymentType"       : rule[Constants.PAYMENT_TYPE_INDEX],
            "VolumeTier"        : rule[Constants.VOLUME_TIER_INDEX],
            "Segment"           : rule[Constants.SEGMENT_INDEX],
            "Version"           : rule[Constants.VERSION_INDEX],
            "Promotion"         : rule[Constants.PROMOTION_INDEX],
            "OS"                : rule[Constants.OS_INDEX],
            "Quantity"          : rule[Constants.QUANTITY_INDEX],
            "PurchasingProgram" : rule[Constants.PURCHASING_PROGRAM_INDEX],
            "TermUoM"           : api.local.validTermUomCache[rule[Constants.TERM_INDEX]],
            "SupportType"       : rule[Constants.SUPPORT_TYPE_INDEX],
            "SupportTier"       : rule[Constants.SUPPORT_TIER_INDEX],
            "Currency"          : rule[Constants.CURRENCY_INDEX],
            "Region"            : rule[Constants.REGION_INDEX],
            "SubRegion"         : rule[Constants.SUB_REGION_INDEX],
            "Hosting"           : rule[Constants.HOSTING_INDEX],
            "ProgramOption"     : rule[Constants.PROGRAM_OPTION_INDEX],
            "Rule"              : "Valid",
            "SAPBaseSKU"        : out.SAPBaseSKU,
            "StandardProductSKU": api.local.SKU[skuIndex++],
    ]
    api.local.pxRecords << rulesGenLib.populateSKURecord(Constants.RULES_TABLE_NAME, pXParams)
}
return