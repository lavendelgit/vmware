api.local.SKU = []
for (productRule in api.local.productRules) {
    pXParamSKU = [
            "Product"          : productRule[Constants.PRODUCT_INDEX] == "-" ? null : out.ProductCache.get(productRule[0]),
            "Offer"            : productRule[Constants.OFFER_INDEX] == "-" ? null : api.local.validOfferCache.get(productRule[1]),
            "DataCenter"       : productRule[Constants.DATACENTER_INDEX] == "-" ? null : api.local.validDataCenterCache.get(productRule[2]),
            "Base"             : productRule[Constants.BASE_INDEX] == "-" ? null : api.local.validBaseCache.get(productRule[3]),
            "Metric"           : productRule[Constants.METRIC_INDEX] == "-" ? null : api.local.validMetricCache.get(productRule[4]),
            "Term"             : productRule[Constants.TERM_INDEX] == "-" ? null : api.local.validTermCache.get(productRule[5]),
            "PaymentType"      : productRule[Constants.PAYMENT_TYPE_INDEX] == "-" ? null : api.local.validPaymentTypeCache.get(productRule[7]),
            "VolumeTier"       : productRule[Constants.VOLUME_TIER_INDEX] == "-" ? null : api.local.validVolumeTierCache.get(productRule[8]),
            "Segment"          : productRule[Constants.SEGMENT_INDEX] == "-" ? null : api.local.validSegmentCache.get(productRule[9]),
            "Version"          : productRule[Constants.VERSION_INDEX] == "-" ? null : api.local.validVersionCache.get(productRule[10]),
            "Promotion"        : productRule[Constants.PROMOTION_INDEX] == "-" ? null : api.local.validPromoCache.get(productRule[11]),
            "OS"               : productRule[Constants.OS_INDEX] == "-" ? null : api.local.validOSCache.get(productRule[12]),
            "Quantity"         : productRule[Constants.QUANTITY_INDEX] == "-" ? null : api.local.validQuantityCache.get(productRule[13]),
            "PurchasingProgram": productRule[Constants.PURCHASING_PROGRAM_INDEX] == "-" ? null : api.local.validPurchasingProgramCache.get(productRule[14]),
            "SupportType"      : productRule[Constants.SUPPORT_TYPE_INDEX] == "-" ? null : api.local.validSupportTypeCache.get(productRule[15]),
            "SupportTier"      : productRule[Constants.SUPPORT_TIER_INDEX] == "-" ? null : api.local.validSupportTierCache.get(productRule[16]),
            "Hosting"          : productRule[Constants.HOSTING_INDEX] == "-" ? null : api.local.validHostingCache.get(productRule[20]),
            "ProgramOption"    : productRule[Constants.PROGRAM_OPTION_INDEX] == "-" ? null : api.local.validProgramOptionCache.get(productRule[21]),
    ]
    def SKU = libs.stdProductLib.SkuGenHelper.createSKU(pXParamSKU)
    api.local.SKU << SKU
}
return