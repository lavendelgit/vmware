api.local.shortDescription = api.findLookupTableValues("StandardProductsDescription")?.collectEntries { [(it.name): it["attributeExtension___ShortDescription"]] }
return