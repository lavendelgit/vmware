api.local.skuInfo.each {
    def stdRecord = [:]
    stdRecord = [
            sku         : it.key,
            label       : it.value.ShortDesc,
            "attribute8": it.value.ItemType
    ]
    if (stdRecord) api.addOrUpdate("P", stdRecord)
}
return