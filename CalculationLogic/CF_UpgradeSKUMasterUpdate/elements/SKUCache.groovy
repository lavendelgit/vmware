api.local.skuInfo = getSKUInfo()
return

Map getSKUInfo() {
    Map skuInfos = [:]
    String extensionName = "StandardProducts"
    List filter = [
            Filter.equal("name", extensionName),
            Filter.in("attribute24", ["Upgrade SKU", "Upgrade Base SKU"])
    ]
    def skuStream = api.stream("PX50", "sku", *filter)
    skuInfos = skuStream?.collectEntries {
        skuInfo ->
            [(skuInfo.sku): [
                    "ShortDesc": api.local.shortDescription?.getAt(skuInfo.sku) ?: "",
                    "ItemType" : skuInfo.attribute24
            ]]
    }
    skuStream?.close()
    return skuInfos
}
