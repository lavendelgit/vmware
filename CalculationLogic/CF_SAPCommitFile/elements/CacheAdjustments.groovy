List filter = [
        Filter.equal(Constants.NAME, Constants.SAP_PRICING_CONDITION_TABLE),
        Filter.in(Constants.SAP_LOAD_PXTABLE_PRODUCT, api.local.stdProductsCache?.product?.unique()),
        Filter.or(Filter.in(Constants.SAP_LOAD_PXTABLE_OFFER, api.local.stdProductsCache?.offer?.unique()), Filter.equal(Constants.SAP_LOAD_PXTABLE_OFFER, Constants.DEFAULT), Filter.in(Constants.SAP_LOAD_PXTABLE_SAPSKU, out.SAPSKUFamily)),
        Filter.or(Filter.in(Constants.SAP_LOAD_PXTABLE_BASE, api.local.stdProductsCache?.base?.unique()), Filter.equal(Constants.SAP_LOAD_PXTABLE_BASE, Constants.DEFAULT), Filter.in(Constants.SAP_LOAD_PXTABLE_SAPSKU, out.SAPSKUFamily)),
        Filter.or(Filter.isNull(Constants.SENT_TO_SAP_COMMIT_OVERAGE), Filter.equal(Constants.SENT_TO_SAP_COMMIT_OVERAGE, Constants.OVERAGE_UPLOAD))
]
stdAdjustmentStream = api.stream("PX20", Constants.SKU, *filter)
api.local.adjustmentsCache = stdAdjustmentStream?.collect { row -> row }
stdAdjustmentStream?.close()
return
