List filter = [
        Filter.equal(Constants.NAME, Constants.STANDARD_PRICE_TABLE),
        Filter.in(Constants.BASE_PRICE_SKU, api.local.stdProductsCache?.baseSku),

]
recordStream = api.stream("PX50", Constants.BASE_PRICE_START_DATE, [Constants.BASE_PRICE_SKU, Constants.BASE_PRICE_PRODUCT, Constants.BASE_PRICE_DISCOUNTED_BP, Constants.BASE_PRICE_BP, Constants.BASE_PRICE_START_DATE, Constants.BASE_PRICE_END_DATE], *filter)
api.local.baseSkuDetails = recordStream?.collectEntries {
    [(it.attribute1): [
            sku      : it.attribute1,
            product  : it.attribute3,
            basePrice: it.attribute23 ? it.attribute23 : it.attribute6,
            startDate: it.attribute29,
            endDate  : it.attribute30
    ]]
}
recordStream?.close()
return