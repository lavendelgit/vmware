List filter = [
        Filter.equal(Constants.NAME, Constants.STANDARD_PRODUCT),
        Filter.in(Constants.STANDARD_PRODUCT_SAP_BASE_SKU, out.SAPSKUFamily)
]
recordStream = api.stream("PX50", null, [Constants.SKU, Constants.STANDARD_PRODUCT_PRODUCT, Constants.STANDARD_PRODUCT_OFFER, Constants.STANDARD_PRODUCT_BASE, Constants.STANDARD_PRODUCT_BASESKU, Constants.STANDARD_PRODUCT_SAP_BASE_SKU], *filter)
api.local.stdProductsCache = recordStream?.collect {
    [
            sku       : it.sku,
            baseSku   : it.attribute14,
            sapBaseSku: it.attribute29,
            product   : it.attribute1,
            offer     : it.attribute2,
            base      : it.attribute4
    ]
}
recordStream?.close()
return