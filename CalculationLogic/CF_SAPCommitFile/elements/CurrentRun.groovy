int currentRun = ((out.SAPPriceListCache?.collect { it.attribute8 as Integer }.unique()?.sort()?.reverse()?.getAt(0))?: 0) + 1
return currentRun as Integer