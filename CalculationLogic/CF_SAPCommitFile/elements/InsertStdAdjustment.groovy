Map record = [:]
Integer runCount = out.CurrentRun
BigDecimal factor
for (productSku in api.local.stdProductsCache?.product?.unique()) {
    stdAdjustmentSkus = api.local.adjustmentsCache?.findAll { it.attribute2 == productSku }
    for (stdAdjustmentSku in stdAdjustmentSkus) {
        sapSKU = stdAdjustmentSku?.attribute16
        record = [:]
        record.key1 = (stdAdjustmentSku?.sku)
        record.attribute1 = stdAdjustmentSku?.attribute5
        record.attribute2 = stdAdjustmentSku?.attribute1
        if (stdAdjustmentSku?.attribute5 == "BUNDLE_DISCOUNT") {
            record.attribute3 = stdAdjustmentSku?.attribute6
            record.attribute4 = stdAdjustmentSku?.attribute15
        } else {
            record.attribute3 = stdAdjustmentSku?.attribute15
            record.attribute4 = stdAdjustmentSku?.attribute6
        }
        factor = stdAdjustmentSku?.attribute8 ? stdAdjustmentSku?.attribute8 * 100 : 0
        if (out.SAPPriceListCache?.key1?.contains(record.key1) && !(out.SAPPriceListCache?.find { it.key1 == record.key1 }?.attribute5 == stdAdjustmentSku?.attribute8)) {
            // change valid to date
            newEndDate = Lib.getPreviousDate(stdAdjustmentSku?.attribute9)
            oldDate = out.SAPPriceListCache?.find { it.key1 == record.key1 }?.key2
            oldfactor = out.SAPPriceListCache?.find { it.key1 == record.key1 }?.attribute5
            record << generateRecords(oldfactor, oldDate, newEndDate, sapSKU, runCount)
            api.addOrUpdate("MLTV2", record)
            // add new record with updated date
            record << generateRecords(factor, stdAdjustmentSku?.attribute9, stdAdjustmentSku?.attribute10, sapSKU, runCount)
            api.addOrUpdate("MLTV2", record)
        } else if (!out.SAPPriceListCache?.key1?.contains(record.key1)) {
            // add new record
            record << generateRecords(factor, stdAdjustmentSku?.attribute9, stdAdjustmentSku?.attribute10, sapSKU, runCount)
            api.addOrUpdate("MLTV2", record)
        }
    }
}


Map generateRecords(BigDecimal factor, String endDate, String startDate, String sapSKU, Integer runCount) {
    return [
            lookupTableId  : out.SAPPriceListTable.id,
            lookupTableName: out.SAPPriceListTable.uniqueName,
            attribute6     : Constants.PERCENTAGE,
            key2           : endDate,
            attribute5     : factor,
            attribute7     : startDate,
            attribute8     : runCount,
    ]
}
