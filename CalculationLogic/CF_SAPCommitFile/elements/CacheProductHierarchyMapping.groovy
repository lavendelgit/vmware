return api.findLookupTableValues(Constants.PRODUCT_HIERARCHY_MAPPING)?.collectEntries {
    [(it.attribute1): [(Constants.PRICEFX_ATTNAME): it.attribute1, (Constants.SAP_NAME): it.key2, (Constants.SAP_ATTNAME): it.attribute2]]
}