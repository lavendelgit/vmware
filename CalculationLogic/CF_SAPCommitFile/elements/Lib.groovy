String getPreviousDate(String validDate) {
    String dateFormat = "yyyy-MM-dd"
    Date date = api.parseDate(dateFormat, validDate)
    Calendar calendar = api.calendar(date)
    calendar.add(Calendar.DATE, -1)
    return calendar.getTime().format("yyyy-MM-dd")
}

Map generateRecords(String key, String sapSKU, BigDecimal price, String startDate, String endDate, Integer runCount) {
    return [
            key1      : key,
            key2      : startDate,
            attribute3: sapSKU,
            attribute5: price,
            attribute7: endDate,
            //attribute9: sapSKU,
            attribute8: runCount
    ]
}

Map getSortedCombinations(String product, String offer, String base, List adjs) {
    List combinations = []
    combinations << [product, "*"]
    combinations << [offer, "*"]
    combinations << [base, "*"]
    for (adj in adjs) {
        combinations << [adj, "*"]
    }
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ libs.vmwareUtil.CacheManager.getWeightage(it, "*") })?.sort { -it.key }
}


List getDiscount(Map discountCombination, String product, String base, String offer) {
    def distiDiscount
    List adjusment = null
    Map sortedCombinations = getSortedCombinations(product, offer, base, adjusment)
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        distiDiscount = discountCombination?.get(key)
        if (distiDiscount) {
            return [distiDiscount, key]
        }
    }
    return null
}

String getHierarchy(String combination) {
    List keys = combination?.split('_') as List
    String hierarchyLevel = ""
    if (keys?.getAt(0) && keys?.getAt(0) != "*") {
        hierarchyLevel = Constants.DISTI_PRODUCT_CONTEXT
    }
    if (keys?.getAt(1) && keys?.getAt(1) != "*") {
        hierarchyLevel = Constants.DISTI_OFFER_CONTEXT
    }
    if (keys?.getAt(2) && keys?.getAt(2) != "*") {
        hierarchyLevel = Constants.DISTI_BASE_CONTEXT
    }
    return hierarchyLevel
}