Map record = [:]
String sapSKU
BigDecimal basePrice
BigDecimal distiDiscoutPercentage
Map distiDiscountRecord = [:]
List distDiscount = []
Map resellerPriceRecord = [:]
distiDiscountRecord = [
        "lookupTableId"  : out.SAPPriceListTable.id,
        "lookupTableName": out.SAPPriceListTable.uniqueName,
        "attribute1"     : Constants.KEY_DISTI_DISCOUNT,
        "attribute6"     : Constants.PERCENTAGE,
        "attribute4"     : Constants.DISTI_VC_KEY_CONSTANT
]
resellerPriceRecord = [
        "lookupTableId"  : out.SAPPriceListTable.id,
        "lookupTableName": out.SAPPriceListTable.uniqueName,
        "attribute1"     : Constants.KEY_DISTI_PRICE,
        "attribute6"     : Constants.PERCENTAGE,
        "attribute2"     : Constants.DISTI_DISCOUNT_HIERARCHY,
        "attribute4"     : Constants.RESELLER_VC_KEY_CONSTANT
]

basePriceRecord = [
        "lookupTableId"  : out.SAPPriceListTable.id,
        "lookupTableName": out.SAPPriceListTable.uniqueName,
        "attribute1"     : Constants.BASE_PRICE,
        "attribute6"     : Constants.CURRENCY,
        "attribute2"     : Constants.SAP_BASE_SKU
]
List stdProducts = api.local.stdProductsCache?.product?.unique()
for (product in stdProducts) {
    baseSkus = api.local.baseSkuDetails?.values()?.findAll { it.product == product }
    for (baseSku in baseSkus) {
        basePrice = baseSku.basePrice as BigDecimal
        sapBaseskuDetails = api.local.stdProductsCache?.find { it.baseSku == baseSku.sku }
        sapSKU = sapBaseskuDetails?.sapBaseSku
        base = sapBaseskuDetails?.base
        offer = sapBaseskuDetails?.offer
        key = Constants.BASE_PRICE + "-" + Constants.SAP_BASE_SKU + "-" + sapSKU
        distDiscount = Lib.getDiscount(out.DistiDicountCache, product, base, offer)
        distiDiscoutPercentage = (distDiscount?.getAt(0)?.Discount) ? ((distDiscount?.getAt(0)?.Discount * 100) * -1) : 0
        resellerDiscount = Lib.getDiscount(out.ResellerDicountCache, product, base, offer)
        distiPrice = (resellerDiscount?.getAt(0)?.Discount) ? ((resellerDiscount?.getAt(0)?.Discount * 100) * -1) : 0
        distiHierarchylevel = Lib.getHierarchy(distDiscount?.getAt(1))
        distiKey = distDiscount?.getAt(1) + Constants.DISTI_DISCOUNT
        distiPriceKey = resellerDiscount?.getAt(1) + Constants.RESELLER_DISCOUNT
        distiDiscountRecord.attribute2 = distiHierarchylevel
        if (out.SAPPriceListCache?.key1?.contains(key) && !(out.SAPPriceListCache?.find { it.key1 == key }?.attribute5 == basePrice) || out.SAPPriceListCache?.key1?.contains(distiKey) && !(out.SAPPriceListCache?.find { it.key1 == distiPriceKey }?.attribute5 == distiDiscoutPercentage)) {
            if (!(out.SAPPriceListCache?.findAll { it.key1 == key }?.attribute5?.contains(basePrice as String))) {
                updateBasePrice(key, baseSku, basePriceRecord, sapSKU, basePrice, out.CurrentRun)
            }
            oldDistiDiscount = out.SAPPriceListCache?.find { it.key1 == distiKey }?.attribute5
            oldResllerDiscount = out.SAPPriceListCache?.find { it.key1 == distiPriceKey }?.attribute5
            if ((oldDistiDiscount != distiDiscoutPercentage) || (oldResllerDiscount != distiPrice)) {
                updateDistiDiscount(distiKey, distiPriceKey, distiPrice, distDiscount, sapSKU, oldDistiDiscount, distiDiscoutPercentage, distiDiscountRecord, resellerPriceRecord, out.CurrentRun)
            }
        } else if (!out.SAPPriceListCache?.key1?.contains(key)) {
            // add new record
            insertNewRow(key, distiKey, distiPriceKey, sapSKU, baseSku, distDiscount, basePrice, distiDiscoutPercentage, distiPrice, basePriceRecord, distiDiscountRecord, resellerPriceRecord, out.CurrentRun)
        }
        if ((out.SAPPriceListCache?.key1?.contains(key)) && !(out.SAPPriceListCache?.find { it.key1 == distiKey }?.attribute5 == distiDiscoutPercentage)) {
            oldDistiDiscount = out.SAPPriceListCache?.find { it.key1 == distiKey }?.attribute5
            updateDistiDiscount(distiKey, distiPriceKey, distiPrice, distDiscount, sapSKU, oldDistiDiscount, distiDiscoutPercentage, distiDiscountRecord, resellerPriceRecord, out.CurrentRun)
        }
    }
}

void updateBasePrice(String key, Map baseSku, Map basePriceRecord, String sapSKU, BigDecimal basePrice, Integer runCount) {
    oldDate = out.SAPPriceListCache?.find { it.key1 == key }?.key2
    oldPrice = out.SAPPriceListCache?.find { it.key1 == key }?.attribute5
    newEndDate = Lib.getPreviousDate(baseSku?.startDate)
    basePriceRecord << Lib.generateRecords(key, sapSKU, oldPrice, oldDate, newEndDate, runCount)
    api.addOrUpdate("MLTV2", basePriceRecord)
    basePriceRecord << Lib.generateRecords(key, sapSKU, basePrice, baseSku?.startDate, baseSku?.endDate, runCount)
    api.addOrUpdate("MLTV2", basePriceRecord)
}

void updateDistiDiscount(String distiKey, String distiPriceKey, BigDecimal distiPrice, List distDiscount, String sapSKU, BigDecimal oldDistiDiscount, BigDecimal distiDiscoutPercentage,
                         Map distiDiscountRecord, Map resellerPriceRecord, Integer runCount) {
    oldDistiDate = out.SAPPriceListCache?.find { it.key1 == distiKey }?.key2
    newDistiEndDate = Lib.getPreviousDate(distDiscount?.getAt(0)?.StartDate)
    distiDiscountRecord << Lib.generateRecords(distiKey, sapSKU, oldDistiDiscount, oldDistiDate, newDistiEndDate, runCount)
    api.addOrUpdate("MLTV2", distiDiscountRecord)
    distiDiscountRecord << Lib.generateRecords(distiKey, sapSKU, distiDiscoutPercentage, distDiscount?.getAt(0)?.StartDate, distDiscount?.getAt(0)?.EndDate, runCount)
    api.addOrUpdate("MLTV2", distiDiscountRecord)
    resellerPriceRecord << Lib.generateRecords(distiPriceKey, sapSKU, distiPrice, oldDistiDate, newDistiEndDate, runCount)
    api.addOrUpdate("MLTV2", resellerPriceRecord)
    resellerPriceRecord << Lib.generateRecords(distiPriceKey, sapSKU, distiPrice, distDiscount?.getAt(0)?.StartDate, distDiscount?.getAt(0)?.EndDate, runCount)
    api.addOrUpdate("MLTV2", resellerPriceRecord)
}

void insertNewRow(String key, String distiKey, String distiPriceKey, String sapSKU, Map baseSku, List distDiscount, BigDecimal basePrice,
                  BigDecimal distiDiscoutPercentage, BigDecimal distiPrice, Map basePriceRecord, Map distiDiscountRecord, Map resellerPriceRecord, Integer runCount) {
    basePriceRecord << Lib.generateRecords(key, sapSKU, basePrice, baseSku?.startDate, baseSku?.endDate, runCount)
    api.addOrUpdate("MLTV2", basePriceRecord)
    distiDiscountRecord << Lib.generateRecords(distiKey, sapSKU, distiDiscoutPercentage, distDiscount?.getAt(0)?.StartDate, distDiscount?.getAt(0)?.EndDate, runCount)
    api.addOrUpdate("MLTV2", distiDiscountRecord)
    resellerPriceRecord << Lib.generateRecords(distiPriceKey, sapSKU, distiPrice, distDiscount?.getAt(0)?.StartDate, distDiscount?.getAt(0)?.EndDate, runCount)
    api.addOrUpdate("MLTV2", resellerPriceRecord)
}