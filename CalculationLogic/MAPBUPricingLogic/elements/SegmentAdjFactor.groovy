def segment = out.Segment
def segmentPPMap = api.global.segmentMap
def segmentAdj
segmentAdj = (segmentPPMap[segment]) ? segmentPPMap[segment].attribute1 : null
if (segmentAdj) return segmentAdj
