api.retainGlobal = true

if (api.isSyntaxCheck()) return

//Initializing maps
if (!api.global.batch) {
    api.global.batch = [:]
}


if (!api.global.baseSKUPriceMap) {
    api.global.baseSKUPriceMap = [:]
}

if (!api.global.metricMap) {
    api.global.metricMap = [:]
}


if (!api.global.quantityMap) {
    api.global.quantityMap = [:]
}

if (!api.global.termMap) {
    api.global.termMap = [:]
}

if (!api.global.segmentMap) {
    api.global.segmentMap = [:]
}

if (!api.global.exchangeMap) {
    api.global.exchangeMap = [:]
}

if (!api.global.currencyRoudningMap) {
    api.global.currencyRoudningMap = [:]
}
/**
 * Load Currency Rounding Method
 */
if (!api.global.currencyRoudningMap) {


    def currencyRoundingStream = api.stream("MLTV2", "key2",
            Filter.and(
                    Filter.equal("lookupTable.id", api.findLookupTable("BUCurrencyRounding").id),
                    Filter.equal("key1", "CN")
            )
    )
    api.global.currencyRoudningMap = currencyRoundingStream.collectEntries {

        [(it.key2): it.attribute1]
    }
    currencyRoundingStream.close()
}
/**
 * Load MAPBUBaseSkuPriceTable PP table data
 */
if (!api.global.baseSKUPriceMap) {


    def baseSKUStream = api.stream("MLTV", "name", Filter.equal("lookupTable.id", api.findLookupTable("MAPBUBaseSkuPriceTable").id))
    api.global.baseSKUPriceMap = baseSKUStream.collectEntries {

        [(it.name): it]
    }
    baseSKUStream.close()
}

/**
 * Load MAPBUMetricAdj PP table data
 */
if (!api.global.metricMap) {


    def metricStream = api.stream("MLTV", "name", Filter.equal("lookupTable.id", api.findLookupTable("MAPBUMetricAdj").id))
    api.global.metricMap = metricStream.collectEntries {

        [(it.name): it]
    }
    metricStream.close()
}

/**
 * Load MAPBUQuantityAdj PP table data
 */
if (!api.global.quantityMap) {


    def quantityStream = api.stream("MLTV", "name", Filter.equal("lookupTable.id", api.findLookupTable("MAPBUQuantityAdj").id))
    api.global.quantityMap = quantityStream.collectEntries {

        [(it.name): it]
    }
    quantityStream.close()
}

/**
 * Load MAPBUTermDiscount PP table data
 */
if (!api.global.termMap) {


    def termStream = api.stream("MLTV", "name", ["name", "attribute1", "attribute2"], Filter.equal("lookupTable.id", api.findLookupTable("MAPBUTermDiscount").id))
    api.global.termMap = termStream.collectEntries {

        [(it.name): it]
    }
    termStream.close()
}

/**
 * Load MAPBUSegmentAdj PP table data
 */
if (!api.global.segmentMap) {


    def segmentStream = api.stream("MLTV", "name", Filter.equal("lookupTable.id", api.findLookupTable("MAPBUSegmentAdj").id))
    api.global.segmentMap = segmentStream.collectEntries {

        [(it.name): it]
    }
    segmentStream.close()
}

/**
 * Load ExchangeRate PP table data
 */
if (!api.global.exchangeMap) {

    def exchangeStream = api.stream("MLTV3", "key3", Filter.equal("lookupTable.id", api.findLookupTable("ExchangeRate").id))
    def key
    exchangeStream.each { obj ->
        key = obj.key1 + "~" + obj.key2
        api.global.exchangeMap[key] = obj

    }
    exchangeStream.close()
}


api.local.pid = api.product("sku")

/**
 * Batch load of PX table MAPBUProducts
 */
if (!api.global.batch[api.local.pid]) {

    api.global.batch.clear()
    def batch = api.getBatchInfo()?.collect { it[0] } ?: [api.local.pid]

    filter = [
            Filter.equal("name", "MAPBUProducts"),
            Filter.in("sku", batch)

    ]

    def MAPBUProductsList = api.find("PX20", *filter)

    def MAPBUPXMap = [:]
    MAPBUPXMap = MAPBUProductsList.collectEntries { [(it.sku): it] }


    batch.each {
        api.global.batch[it] = [
                "MAPBUPXObj": (MAPBUPXMap[it] ?: null)

        ]
    }

}