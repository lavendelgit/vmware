def rounding(param) {
    if (param == null) return 0

    bmt = new BigDecimal(param)?.setScale(2, BigDecimal.ROUND_UP)

    return bmt
}

def calculateBOMPrice(bomList, baseskuMap, bundleDiscount) {
    def subComponent
    def quantity
    def price = 0
    def subComponentPrice
    bomList.each { obj ->
        subComponent = obj.label
        quantity = obj.quantity
        subComponentPrice = (baseskuMap) ? baseskuMap[subComponent].attribute2 : null
        if (subComponentPrice && quantity) price += subComponentPrice * quantity
    }
    if (price) price = price * (1 - bundleDiscount)
    return price
}

