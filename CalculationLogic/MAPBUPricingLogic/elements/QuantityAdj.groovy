def qty = out.Quantity
def qtyPPMap = api.global.quantityMap
def qtyAdj
qtyAdj = (qtyPPMap[qty]) ? qtyPPMap[qty].attribute1 : null
if (qtyAdj) return qtyAdj
