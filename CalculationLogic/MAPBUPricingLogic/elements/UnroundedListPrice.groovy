def basePrice = out.BasePrice?.toBigDecimal()
def baseQty = out.BaseQuantity?.toBigDecimal()
def metricAdj = out.MetricAdj?.toBigDecimal()
def qty = out.Quantity?.toBigDecimal()
def qtyAdj = out.QuantityAdj?.toBigDecimal()
def term = out.Term?.toBigDecimal()
def termAdj = out.TermDiscountPct?.toBigDecimal()
def segAdj = out.SegmentAdjFactor?.toBigDecimal()

if (basePrice) {

    def step1 = basePrice / baseQty

    def step2 = step1 * metricAdj

    def step3 = step2 * qty * qtyAdj

    def step4 = step3 * term * termAdj

    def step5 = step4 * segAdj

    return step5
}