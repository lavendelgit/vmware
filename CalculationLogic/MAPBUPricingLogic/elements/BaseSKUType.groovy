def basesku = out.BaseSKU
def baseskuPPMap = api.global.baseSKUPriceMap
def type
type = (baseskuPPMap[basesku]) ? baseskuPPMap[basesku].attribute5 : null
if (type) return type
