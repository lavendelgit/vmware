def sku = out.SKU
def mapbuPXObj = api.global.batch[sku].MAPBUPXObj
def qty = (mapbuPXObj) ? mapbuPXObj.attribute3 : null
if (qty) return qty
