def basesku = out.BaseSKU
def baseskuPPMap = api.global.baseSKUPriceMap

def baseskuType = out.BaseSKUType
def bundleDiscount = out.BundleDiscount

def basePrice

def bomList


switch (baseskuType) {
    case "Standalone":
        basePrice = (baseskuPPMap) ? baseskuPPMap[basesku].attribute2 : null
        break
    case "Bundle":
        api.switchSKUContext(basesku)
        bomList = api.bomList()
        basePrice = Library.calculateBOMPrice(bomList, baseskuPPMap, bundleDiscount)
        break

}
api.resetSKUContextSwitch()
if (basePrice) return basePrice