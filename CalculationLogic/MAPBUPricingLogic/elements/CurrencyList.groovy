def tableName = "ExchangeRate"
filter = [
        Filter.equal("lookupTable.id", api.findLookupTable(tableName).id)
]
def exchangeList = api.find("MLTV3", 0, api.getMaxFindResultsLimit(), null, ["key2"], true, *filter)

def currencyList = (exchangeList) ? exchangeList.collect { it.key2 } : null

if (currencyList) return currencyList