def unroundPrice = out.UnroundedListPrice
def roundPrice = unroundPrice?.toFloat()?.round(2)?.toBigDecimal()
if (roundPrice) return roundPrice