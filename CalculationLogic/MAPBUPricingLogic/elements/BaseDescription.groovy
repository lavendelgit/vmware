def basesku = out.BaseSKU
def baseskuPPMap = api.global.baseSKUPriceMap
def desc
desc = (baseskuPPMap[basesku]) ? baseskuPPMap[basesku].attribute1 : null
if (desc) return desc
