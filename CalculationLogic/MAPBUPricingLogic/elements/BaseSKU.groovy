def sku = out.SKU
def mapbuPXObj = api.global.batch[sku].MAPBUPXObj
def baseSku = (mapbuPXObj) ? mapbuPXObj.attribute16 : null
if (baseSku) return baseSku
