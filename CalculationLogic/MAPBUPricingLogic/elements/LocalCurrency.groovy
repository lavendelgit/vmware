def currList = out.CurrencyList
def param = api.option("Local Currency", currList)
def p = api.getParameter("Local Currency")
if (p != null && p.getValue() == null) {
    p.setRequired(true)
}
return param