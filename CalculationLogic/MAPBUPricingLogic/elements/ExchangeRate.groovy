def fromCurr = out.Currency
def toCurr = out.LocalCurrency
def currkey = fromCurr + "~" + toCurr
def exchangePPMap = api.global.exchangeMap
def rate = (exchangePPMap) ? exchangePPMap[currkey].attribute1 : null
if (rate) return rate