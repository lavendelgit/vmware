def roundingMethodMap = api.global.currencyRoudningMap
def localCurrency = out.LocalCurrency
def exchangeRate = out.ExchangeRate?.toBigDecimal()
def rateAdj = out.FxAdj?.toBigDecimal()
def unRoundPrice = out.UnroundedListPrice?.toBigDecimal()
if (unRoundPrice) {


    def localPrice = unRoundPrice * exchangeRate * rateAdj

    def decimalRounding = roundingMethodMap[localCurrency]

    def finalPrice

    switch (decimalRounding) {
        case 0:
            finalPrice = localPrice.toBigDecimal()?.setScale(0, BigDecimal.ROUND_HALF_UP)
            break
        case 2:
            finalPrice = localPrice.toBigDecimal()?.setScale(2, BigDecimal.ROUND_HALF_UP)
            break
    }
    return finalPrice
}

