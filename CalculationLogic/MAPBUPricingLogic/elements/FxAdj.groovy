def fromCurr = out.Currency
def toCurr = out.LocalCurrency
def currkey = fromCurr + "~" + toCurr
def exchangePPMap = api.global.exchangeMap
def rateAdj = (exchangePPMap) ? exchangePPMap[currkey].attribute2 : null
if (rateAdj) return rateAdj