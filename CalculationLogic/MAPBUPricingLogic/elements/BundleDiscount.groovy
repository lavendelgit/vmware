def basesku = out.BaseSKU
def baseskuPPMap = api.global.baseSKUPriceMap
def discount
discount = (baseskuPPMap[basesku]) ? baseskuPPMap[basesku].attribute6 : null
if (discount) return discount
