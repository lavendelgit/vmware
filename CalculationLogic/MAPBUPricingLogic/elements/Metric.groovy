def sku = out.SKU
def mapbuPXObj = api.global.batch[sku].MAPBUPXObj
def metric = (mapbuPXObj) ? mapbuPXObj.attribute15 : null
if (metric) return metric
