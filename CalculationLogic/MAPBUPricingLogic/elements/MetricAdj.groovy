def metric = out.Metric
def metricPPMap = api.global.metricMap
def metricAdj
metricAdj = (metricPPMap[metric]) ? metricPPMap[metric].attribute1 : null
if (metricAdj) return metricAdj
