import com.ibm.icu.math.BigDecimal

def term = out.Term
def termPPMap = api.global.termMap
def termDiscount
term = term.toString()
termDiscount = (termPPMap[term]) ? termPPMap[term].attribute2 : null
if (termDiscount) return termDiscount
