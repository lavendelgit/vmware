def basesku = out.BaseSKU
def baseskuPPMap = api.global.baseSKUPriceMap
def qty
qty = (baseskuPPMap[basesku]) ? baseskuPPMap[basesku].attribute4 : null
if (qty) return qty
