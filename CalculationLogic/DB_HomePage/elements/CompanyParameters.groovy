List priceParameters = api.find(Constants.PRICE_PARAMETER_CODE)
return priceParameters?.collectEntries { [((it.uniqueName) ?: (Constants.NULL_PLACE_HOLDER)), ((it.typedId)?.toString()?.replaceAll("\\.LT", ""))] }

