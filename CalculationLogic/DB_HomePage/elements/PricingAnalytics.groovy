def controller = api.newController()
String baseURL = out.BaseURL
Map dl = out.DataLoad
Map ds = out.DataSource
controller.addHTML("<h2 style=color:#FFFFFF;background-color:#78BE20;>Pricing Analytics </h2>")
String EndPA = "<hr />"
controller.addHTML(EndPA)
controller.addHTML("<style>a:hover {color: blue;background-color: transparent;text-decoration: underline;}</style>")
controller.addHTML("<h4 style = color:#FFFFFF;background-color:#1D428A;><u>Dashboards</u></h4")
controller.addHTML("<h4>Price Book Reports</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/dashboards/HistoricalPriceList'>Historical Master Price Book Reports </a></li><li><a href='${baseURL}/app/modules/#/dashboards/DB_ChannelPriceBook'>Historical Channel Price Book Reports</a></li></ol>")
controller.addHTML("<h4>Total Available Market(TAM)</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/dashboards/TAMDashboard'>TAM - Gross Bookings CC – Single Currency</a></li><li><a href='${baseURL}/app/modules/#/dashboards/TAMMultiCurrencyDashBoard'>TAM - Gross Bookings CC – Multi Currency</a></li><li><a href='${baseURL}/app/modules/#/dashboards/DependentSKUsTAMCalculation'>TAM- Gross Bookings CC- Dependent SKUs</a></li></ol>")
controller.addHTML("<h4>GPA</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/dashboards/DependentSKUDetails'>Dependent SKU List</a></li><li> <a href='${baseURL}/app/modules/#/dashboards/DB_UpgradeSkuDetails'>Upgrade SKU Search</a></li></ol>")
controller.addHTML("<h4 style = color:#FFFFFF;background-color:#1D428A;><u>Data Uploads</u></h4")
controller.addHTML("<h4>Master Price Book</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/pa/data-manager/datasources/${ds[Constants.DS_HISTORICAL_PRICE_DATA]}'>Master Price Book Upload</a></li><li> <a href='${baseURL}/app/modules/#/pa/data-manager/dataloads/${dl[Constants.DL_HISTORICAL_PRICE_LIST]}'>Refresh Action - Master Price Book DataMart</a></li></ol>")
controller.addHTML("<h4>Channel Price Book</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/pa/data-manager/datasources/${ds[Constants.DS_CHANNEL_PRICE_BOOK]}'>Channel Price Book Upload</a></li><li> <a href='${baseURL}/app/modules/#/pa/data-manager/dataloads/${dl[Constants.DL_CHANNEL_PRICE_BOOK_DM]}'>Refresh Action - Channel Price Book DataMart</a></li></ol>")
controller.addHTML("<h4>Pricing and Discounting</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/pa/data-manager/datasources/${ds[Constants.DS_PRICING_AND_DISCOUNTING]}'>Pricing and Discounting Upload</a></li><li> <a href='${baseURL}/app/modules/#/pa/data-manager/dataloads/${dl[Constants.DL_PRICING_AND_DISCOUNTING_DM]}'>Refresh Action - Pricing and Discounting DataMart</a></li></ol>")
controller.addHTML("<h4 style = color:#FFFFFF;background-color:#1D428A;><u>Data Analyzer</u></h4")
controller.addHTML("<ol type = 1>" +
        "<li> <a href='${baseURL}/app/modules/#/pa/data-analyzer/bar'>Bar & Line</a></li>" +
        "<li> <a href='${baseURL}/app/modules/#/pa/data-analyzer/scatter'>Scatter</a></li>" +
        "<li> <a href='${baseURL}/app/modules/#/pa/data-analyzer/pie'>Pie</a></li>" +
        "<li> <a href='${baseURL}/app/modules/#/pa/data-analyzer/timeseries'>TimeSeries</a><h6 style = color:#3AAA26;>(Note: Navigate- Analytics-> Data Analyzer for all Chart Types)</h6></li></ol>")
return controller