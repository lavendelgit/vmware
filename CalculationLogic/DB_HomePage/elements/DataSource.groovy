List reqdDatasources = Constants.REQD_DATASOURCES
List filters = []
filters << Filter.in(Constants.LABEL, reqdDatasources)
Map dataSource = api.find(Constants.DATASOURCE_CODE, *filters)?.collectEntries { [(it.label), (it.typedId)] }
return dataSource