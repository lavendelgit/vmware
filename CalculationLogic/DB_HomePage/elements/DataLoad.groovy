List reqdDataloads = Constants.REQD_DATALOADS
List filters = []
List actions = Constants.DATALOAD_ACTIONS
filters << Filter.in(Constants.TYPE, actions)
filters << Filter.in(Constants.LABEL, reqdDataloads)
Map dataLoad = api.find(Constants.DATALOAD_CODE, *filters)?.collectEntries { [(it.label), (it.typedId)] }
return dataLoad
