def controller = api.newController()
String baseURL = out.BaseURL
Map dl = out.DataLoad
Map ds = out.DataSource
Map cp = out.CompanyParameters
controller.addHTML("<h2 style=color:#FFFFFF;background-color:#78BE20;>Price Builder </h2>")
String EndPB = "<hr />"
controller.addHTML(EndPB)
controller.addHTML("<style>a:hover {color: blue;background-color: transparent;text-decoration: underline;}</style>")
controller.addHTML("<h4 style = color:#FFFFFF;background-color:#1D428A;><u>Standard Products</u></h4")
controller.addHTML("<h5><a href='${baseURL}/app/modules/#/md/product-extensions/StandardProducts'>SKU & Attributes (Standard Products)</a></h5>")
controller.addHTML("<h5><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_STANDARD_PRODUCT_DESCRIPTION_TABLE]}'>SKU Short & Long Description</a></h5>")
controller.addHTML("<h4 style = color:#FFFFFF;background-color:#1D428A;><u>Price Setup</u></h4")
controller.addHTML("<h5><a href='${baseURL}/app/modules/#/md/product-extensions/StandardAdjustment'>Price factor setups (Standard Adjustment)</a></h5>")
controller.addHTML("<h5><a href='${baseURL}/app/modules/#/md/product-extensions/StandardBasePrice'>Base Price setups (Standard Base Price)</a></h5>")
controller.addHTML("<h5><a href='${baseURL}/app/modules/#/pb/lpgs'>Live Price Grid</a></h5>")
controller.addHTML("<h5><a href='${baseURL}/app/modules/#/administration/configuration/system-configuration/calculation-flow'>Price Change Summary Email</a><h6 style = color:#3AAA26;>(Search : LPGItemApprovalRequiredMail)</h6></h5>")
controller.addHTML("<h5><a href='${baseURL}/app/modules/#/pa/data-manager/dataloads/${dl[Constants.DL_PRICEFX_PRICE_BOOK]}'>Approve Price to Datasource</a></h5>")
controller.addHTML("<h5><a href='${baseURL}/app/modules/#/pa/data-manager/datasources/${ds[Constants.DS_PRICEFX_PRICE_BOOK]}'>PriceFx PriceBook</a></h5>")
controller.addHTML("<h4>Bundles</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/md/product-extensions'>BOM</a></li>" +
        "<li> <a href='${baseURL}/app/modules/#/administration/configuration/system-configuration/calculation-flow'>Bundle Base Price calculation program</a><h6 style = color:#3AAA26;>(Search : Bundle Base Price Calculation)</h6></li>" +
        "<li> <a href='${baseURL}/app/modules/#/md/product-extensions/BundleAdjustment'>Bundle Price Factor Setups</a></li>" +
        "<li> <a href='${baseURL}/app/modules/#/md/product-extensions/StandardProducts'>Bundle SKUs and Attributes</a><h6 style = color:#3AAA26;>(Search : SKU Type = Bundle)</h6></li></ol>")
controller.addHTML("<h4>Upgrades</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_BASE_TO_UPGRADE_TABLE]}'>Base to upgrade relationship</a></li>" +
        "<li> <a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_UPGRADE_TO_DESTINATION_TABLE]}'>Upgrade to Destination relationship</a></li>" +
        "<li> <a href='${baseURL}/app/modules/#/administration/configuration/system-configuration/calculation-flow'>Upgrade Base Price Calculation Program</a><h6 style = color:#3AAA26;>(Search : Calculate Upgrade Base SKU)</h6></li>" +
        "<li> <a href='${baseURL}/app/modules/#/md/product-extensions/StandardProducts'>Upgrade SKUs and Attributes</a><h6 style = color:#3AAA26;>(Search : SKU Type = Upgrade)</h6></li></ol>")
controller.addHTML("<h4 style = color:#FFFFFF;background-color:#1D428A;><u>Standard SKU creation</u></h4")
controller.addHTML("<h4>Standard Product Attribute</h4>")
controller.addHTML("<ol type = 1><li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_PRODUCT_TABLE]}'>Product</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_OFFER_TABLE]}'>Offer</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_BASE_TABLE]}'>Base</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_DATA_CENTER_TABLE]}'>Data Center</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_METRIC_TABLE]}'>Metric</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_TERM_TABLE]}'>Term</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_PAYMENT_TYPE_TABLE]}'>Payment Type</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_VOLUME_TIER_TABLE]}'>Volume Tier</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_SEGMENT_TABLE]}'>Segment</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_VERSION_TABLE]}'>Version</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_PROMOTION_TABLE]}'>Promotion</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_PURCHASING_PROGRAM_TABLE]}'>Purchasing Program</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_SUPPORT_TYPE_TABLE]}'>Support Type</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_SUPPORT_TIER_TABLE]}'>Support Tier</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_QUANTITY_TABLE]}'>Quantity</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_OS_TABLE]}'>OS</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_CURRENCY_TABLE]}'>Currency</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_HOSTING_TABLE]}'>Hosting</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_PRICE_LIST_REGION_TABLE]}'>Price List Region</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_PROGRAM_OPTION_TABLE]}'>Program Option</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_RETENTION_PERIOD_TABLE]}'>Retention Period</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_PS_TERM_TABLE]}'>PS Term</a></li>" +
        "<li><a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_PS_OPTION_TABLE]}'>PS Option</a></li></ol>")
controller.addHTML("<h4>Description Rules</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_SHORT_DESC_CONFIG_TABLE]}'>Short Description Rules</a></li><li> <a href='${baseURL}/app/modules/#/parameters/${cp[Constants.PP_LONG_DESC_CONFIG_TABLE]}'>Long Description Rules</a></li></ol>")
controller.addHTML("<h4><a href='${baseURL}/app/modules/#/md/product-extensions/UniversalRule'>Universal Rules for valid combination</a></h4>")
controller.addHTML("<h4>Program Run</h4>")
controller.addHTML("<ol type = 1><li> <a href='${baseURL}/app/modules/#/administration/configuration/system-configuration/calculation-flow'>Standard SKU Creation</a><h6 style = color:#3AAA26;>(Search : Generate Standard Product Skus)</h6></li><li> <a href='${baseURL}/app/modules/#/administration/configuration/system-configuration/calculation-flow'>Product Master Update</a><h6 style = color:#3AAA26;>(Search : Standard Product Master Update)</h6></li><h6 style = color:blue;>Note: Please run Product Master Update after the completion of Standard SKU creation. Please check the status <a href='${baseURL}/app/modules/#/administration/logs/jobs'</a>here</h6></ol>")
return controller
