def year = (out.Length / 12)?.toString()
return libs.vmwareUtil.CacheManager.getPaymentCycle(api.global.termsCache, out.Offer, out.Terms, year) as BigDecimal