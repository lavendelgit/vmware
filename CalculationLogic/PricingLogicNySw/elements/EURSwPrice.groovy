BigDecimal swPrice = out.SwPrice * libs.vmwareUtil.CacheManager.getEURLCFactor(out.LCFactor)
return swPrice.setScale(out.RoundingDecimal, BigDecimal.ROUND_UP)
