BigDecimal swPrice = out.SwPrice * libs.vmwareUtil.CacheManager.getCNYLCFactor(out.LCFactor)
return swPrice.setScale(out.RoundingDecimal, BigDecimal.ROUND_UP)
