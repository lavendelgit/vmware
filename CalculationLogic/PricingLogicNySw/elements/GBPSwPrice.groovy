BigDecimal swPrice = out.SwPrice * libs.vmwareUtil.CacheManager.getGBPLCFactor(out.LCFactor)
return swPrice.setScale(out.RoundingDecimal, BigDecimal.ROUND_UP)
