BigDecimal swPrice = out.SwPrice * libs.vmwareUtil.CacheManager.getJPYLCFactor(out.LCFactor)
return swPrice.setScale(out.RoundingDecimal, BigDecimal.ROUND_UP)
