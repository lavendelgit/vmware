if (input.Analytics == "Yes") {
    List dmList = out.CacheDM
    ConfiguratorEntry ce = api.createConfiguratorEntry()
    ce.createParameter(InputType.OPTION, Constants.PRICE_CHANGE_DM_LABEL)
            .setValueOptions(dmList)
            .setRequired(true)
    return ce
}