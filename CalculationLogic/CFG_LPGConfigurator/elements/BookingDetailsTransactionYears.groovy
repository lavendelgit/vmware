import net.pricefx.common.api.InputType
if (input.Analytics == "Yes") {
    defaultYear = (Constants.DEFAULT_YEAR)
    ConfiguratorEntry ce = api.createConfiguratorEntry()
    def bookingDetailsyear = ce.createParameter(InputType.INTEGERUSERENTRY, Constants.BOOKING_DETAILS_TRAN_LABEL)
    def previousvalue = bookingDetailsyear.getValue()
    bookingDetailsyear.setRequired(true)
    bookingDetailsyear.setValue(previousvalue ?: defaultYear)
    return ce
}