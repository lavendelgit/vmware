if (input.(Constants.BOOKING_DETAILS_DM_LABEL) != null && input.Analytics == "Yes") {
    ConfiguratorEntry ce = api.createConfiguratorEntry()
    ce.createParameter(InputType.DMFILTERBUILDER, Constants.BOOKING_DETAILS_DATA_FILTER_LABEL)
            .addParameterConfigEntry(Constants.DM_SOURCE_NAME, input.(Constants.BOOKING_DETAILS_DM_LABEL))
    return ce
}