if (input.Analytics == "Yes") {
    List dmList = out.CacheDM
    ConfiguratorEntry ce = api.createConfiguratorEntry()
    ce.createParameter(InputType.OPTION, Constants.BOOKING_DETAILS_DM_LABEL)
            .setValueOptions(dmList)
            .setValue(Constants.PRICING_AND_DISCOUNTING_DM)
            .setRequired(true)
    return ce
}