def section = api.createConfiguratorEntry()

section.createParameter(InputType.OPTION, "Analytics")
        .setValueOptions("Yes", "No")
        .setRequired(true)

return section