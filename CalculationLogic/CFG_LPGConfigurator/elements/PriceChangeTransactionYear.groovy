import net.pricefx.common.api.InputType
if (input.Analytics == "Yes") {
    defaultYear = (Constants.DEFAULT_YEAR)
    ConfiguratorEntry ce = api.createConfiguratorEntry()
    def priceChangeYear = ce.createParameter(InputType.INTEGERUSERENTRY, Constants.PRICE_CHANGE_TRAN_LABEL)
    def previousvalue = priceChangeYear.getValue()
    priceChangeYear.setRequired(true)
    priceChangeYear.setValue(previousvalue ?: defaultYear)
    return ce
}