def resultMatrix = api.newMatrix("product Family",
        "product Id",
        "Description (SAP description)",
        "Usage Item",
        "Currency",
        "Region",
        "Data Center",
        "Billing Term",
        "Billing Term Frequency",
        "Billing Term UOM",
        "Price")

def Nodei3LpgList = api.find("PG", 0, 1, null,
        Filter.equal("label", api.getElement("LPGName")))
def itemList
api.trace("productFamily filters:", api.getElement("ProductFamilyFilters"))
api.trace("ProductId filters:", api.getElement("ProductIdFilters"))
api.trace("UsageItem filters:", api.getElement("UsageItemFilters"))
api.trace("DataCenter filters:", api.getElement("DataCenterFilters"))
if (Nodei3LpgList != null && Nodei3LpgList[0] != null)
    itemList = api.find("PGI", 0, 200, null,
            Filter.equal("priceGridId", Nodei3LpgList[0].id),
            Filter.in("attribute45", api.getElement("ProductFamilyFilters")),
            Filter.in("attribute66", api.getElement("ProductIdFilters")),
            Filter.in("attribute69", api.getElement("UsageItemFilters")),
            Filter.in("attribute62", api.getElement("DataCenterFilters")))

if (itemList != null)
    api.trace("itemList.size():", itemList.size())
itemList.each {

    resultRow = [:]
    productFamily = it.attribute45
    def productId = it.attribute66
    def sapLongDesc = it.attribute71
    def usageItem = it.attribute69
    def dataCenter = it.attribute62
    def billingTerm = it.attribute63
    def billingTermFrequency = "PREPAID"
    def billingTermUOM = it.attribute65
    def currency = "USD"
    def region = "NA"
    def price = it.attribute38
    resultRow = [:]
    resultRow.put("product Family", productFamily)
    resultRow.put("product Id", productId)
    resultRow.put("Description (SAP description)", sapLongDesc)
    resultRow.put("Usage Item", usageItem)
    resultRow.put("Currency", currency)
    resultRow.put("Region", region)
    resultRow.put("Data Center", dataCenter)
    resultRow.put("Billing Term", billingTerm)
    resultRow.put("Billing Term Frequency", billingTermFrequency)
    resultRow.put("Billing Term UOM", billingTermUOM)
    resultRow.put("Price", price)//USD
    resultMatrix.addRow(resultRow)
    //2nd price - EUR
    currency = "EUR"
    region = "EMEA"
    price = it.attribute39
    resultRow = [:]
    resultRow.put("product Family", productFamily)
    resultRow.put("product Id", productId)
    resultRow.put("Description (SAP description)", sapLongDesc)
    resultRow.put("Usage Item", usageItem)
    resultRow.put("Currency", currency)
    resultRow.put("Region", region)
    resultRow.put("Data Center", dataCenter)
    resultRow.put("Billing Term", billingTerm)
    resultRow.put("Billing Term Frequency", billingTermFrequency)
    resultRow.put("Billing Term UOM", billingTermUOM)
    resultRow.put("Price", price)
    resultMatrix.addRow(resultRow)
    //3rd price - GBP
    currency = "GBP"
    region = "EMEA"
    price = it.attribute40
    resultRow = [:]
    resultRow.put("product Family", productFamily)
    resultRow.put("product Id", productId)
    resultRow.put("Description (SAP description)", sapLongDesc)
    resultRow.put("Usage Item", usageItem)
    resultRow.put("Currency", currency)
    resultRow.put("Region", region)
    resultRow.put("Data Center", dataCenter)
    resultRow.put("Billing Term", billingTerm)
    resultRow.put("Billing Term Frequency", billingTermFrequency)
    resultRow.put("Billing Term UOM", billingTermUOM)
    resultRow.put("Price", price)
    resultMatrix.addRow(resultRow)
    //4th price - AUD
    currency = "AUD"
    region = "APAC"
    price = it.attribute41
    resultRow = [:]
    resultRow.put("product Family", productFamily)
    resultRow.put("product Id", productId)
    resultRow.put("Description (SAP description)", sapLongDesc)
    resultRow.put("Usage Item", usageItem)
    resultRow.put("Currency", currency)
    resultRow.put("Region", region)
    resultRow.put("Data Center", dataCenter)
    resultRow.put("Billing Term", billingTerm)
    resultRow.put("Billing Term Frequency", billingTermFrequency)
    resultRow.put("Billing Term UOM", billingTermUOM)
    resultRow.put("Price", price)
    resultMatrix.addRow(resultRow)
    //5th price - CNY
    currency = "CNY"
    region = "APAC"
    price = it.attribute42
    resultRow = [:]
    resultRow.put("product Family", productFamily)
    resultRow.put("product Id", productId)
    resultRow.put("Description (SAP description)", sapLongDesc)
    resultRow.put("Usage Item", usageItem)
    resultRow.put("Currency", currency)
    resultRow.put("Region", region)
    resultRow.put("Data Center", dataCenter)
    resultRow.put("Billing Term", billingTerm)
    resultRow.put("Billing Term Frequency", billingTermFrequency)
    resultRow.put("Billing Term UOM", billingTermUOM)
    resultRow.put("Price", price)
    resultMatrix.addRow(resultRow)
    //6th price - JPY
    currency = "JPY"
    region = "APAC"
    price = it.attribute43
    resultRow = [:]
    resultRow.put("product Family", productFamily)
    resultRow.put("product Id", productId)
    resultRow.put("Description (SAP description)", sapLongDesc)
    resultRow.put("Usage Item", usageItem)
    resultRow.put("Currency", currency)
    resultRow.put("Region", region)
    resultRow.put("Data Center", dataCenter)
    resultRow.put("Billing Term", billingTerm)
    resultRow.put("Billing Term Frequency", billingTermFrequency)
    resultRow.put("Billing Term UOM", billingTermUOM)
    resultRow.put("Price", price)
    resultMatrix.addRow(resultRow)
    //7th price - EMEA USD
    currency = "USD"
    region = "EMEA"
    price = it.attribute38
    resultRow = [:]
    resultRow.put("product Family", productFamily)
    resultRow.put("product Id", productId)
    resultRow.put("Description (SAP description)", sapLongDesc)
    resultRow.put("Usage Item", usageItem)
    resultRow.put("Currency", currency)
    resultRow.put("Region", region)
    resultRow.put("Data Center", dataCenter)
    resultRow.put("Billing Term", billingTerm)
    resultRow.put("Billing Term Frequency", billingTermFrequency)
    resultRow.put("Billing Term UOM", billingTermUOM)
    resultRow.put("Price", price)
    resultMatrix.addRow(resultRow)
    //8th price - APAC USD
    currency = "USD"
    region = "APAC"
    price = it.attribute38
    resultRow = [:]
    resultRow.put("product Family", productFamily)
    resultRow.put("product Id", productId)
    resultRow.put("Description (SAP description)", sapLongDesc)
    resultRow.put("Usage Item", usageItem)
    resultRow.put("Currency", currency)
    resultRow.put("Region", region)
    resultRow.put("Data Center", dataCenter)
    resultRow.put("Billing Term", billingTerm)
    resultRow.put("Billing Term Frequency", billingTermFrequency)
    resultRow.put("Billing Term UOM", billingTermUOM)
    resultRow.put("Price", price)
    resultMatrix.addRow(resultRow)
}

resultMatrix.enableClientFilter = true

return resultMatrix
