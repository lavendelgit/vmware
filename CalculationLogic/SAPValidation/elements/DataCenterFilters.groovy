/*if (api.syntaxCheck){
  return null
}*/
def Nodei3LpgList = api.find("PG", 0, 1, null,
        Filter.equal("label", api.getElement("LPGName")))
def dataCenterList
if (Nodei3LpgList != null && Nodei3LpgList[0] != null)
    dataCenterList = api.find("PGI", 0, 200, "attribute62", ["attribute62"], true,
            Filter.equal("priceGridId", Nodei3LpgList[0].id)).attribute62
api.logInfo("dataCenterList:", dataCenterList)
if (dataCenterList != null)
    return api.options("Data Center", dataCenterList)