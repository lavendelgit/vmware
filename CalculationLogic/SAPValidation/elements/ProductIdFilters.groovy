/*if (api.syntaxCheck){
  return null
}*/
def lpgName = api.getElement("LPGName")?.toString()
def Nodei3LpgList = api.find("PG", 0, 1, null,
        Filter.equal("label", lpgName))
def prodIdList
if (Nodei3LpgList != null && Nodei3LpgList[0] != null)
    prodIdList = api.find("PGI", 0, 200, "attribute66", ["attribute66"], true,
            Filter.equal("priceGridId", Nodei3LpgList[0].id)).attribute66
api.logInfo("prodIdList:", prodIdList)
if (prodIdList != null)
    return api.options("Product Id", prodIdList)