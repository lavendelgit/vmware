/*if (api.syntaxCheck){
  return null
}*/
def Nodei3LpgList = api.find("PG", 0, 1, null,
        Filter.equal("label", api.getElement("LPGName")))
def usageItemList
if (Nodei3LpgList != null && Nodei3LpgList[0] != null)
    usageItemList = api.find("PGI", 0, 200, "attribute69", ["attribute69"], true,
            Filter.equal("priceGridId", Nodei3LpgList[0].id)).attribute69
api.logInfo("usageItemList:", usageItemList)
if (usageItemList != null)
    return api.options("Usage Item", usageItemList)