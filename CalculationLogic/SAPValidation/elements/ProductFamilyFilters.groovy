/*if (api.syntaxCheck){
  return null
}*/
def lpgName = api.getElement("LPGName")?.toString()
def Nodei3LpgList = api.find("PG", 0, 1, null, Filter.equal("label", lpgName))
def productFamilyList = []
if (Nodei3LpgList != null && Nodei3LpgList[0] != null)
    productFamilyList = api.find("PGI", 0, 200, "attribute45", ["attribute45"], true,
            Filter.equal("priceGridId", Nodei3LpgList[0].id)).attribute45
api.logInfo("ProductFamilyList:", productFamilyList)
return api.options("Product Family", productFamilyList)