def destinationPrice = api.getElement("DestinationPrice")
def basePrice = api.getElement("BasePrice")

if (!destinationPrice || !basePrice) return ""

return (destinationPrice - basePrice).toBigDecimal()