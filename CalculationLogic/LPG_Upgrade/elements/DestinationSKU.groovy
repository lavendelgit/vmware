def upgradeSKU = api.getElement("UpgradeSKU")

if (!upgradeSKU) return ""
return api.findLookupTableValues("UpgradeToDestination", Filter.equal("attribute1", upgradeSKU)).attribute2[0]