def destinationPriceMinusBasePrice = api.getElement("DestinationPriceMinusBasePrice")
def destinationPrice = api.getElement("DestinationPrice")

if (!destinationPriceMinusBasePrice || !destinationPrice) return ""

def upgradePrice = (destinationPriceMinusBasePrice) * 1.1

if (upgradePrice > destinationPrice) {
    return destinationPriceMinusBasePrice
} else {
    return upgradePrice
}
return ""