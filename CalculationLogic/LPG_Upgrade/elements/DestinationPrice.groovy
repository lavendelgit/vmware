def destinationSKU = api.getElement("DestinationSKU")
def upgradeSKU = api.getElement("UpgradeSKU")

if (!upgradeSKU) return ""

return api.find("PX", Filter.equal("name", "ListPrice"), Filter.equal("sku", destinationSKU)).attribute1[0]?.toBigDecimal()