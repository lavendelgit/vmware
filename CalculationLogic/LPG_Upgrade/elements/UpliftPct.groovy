def destinationPriceMinusBasePrice = api.getElement("DestinationPriceMinusBasePrice")
def upgradePrice = api.getElement("UpgradePrice")

if (!destinationPriceMinusBasePrice || !upgradePrice) return ""

return 100 / destinationPriceMinusBasePrice * (upgradePrice - destinationPriceMinusBasePrice) / 100