if (api.syntaxCheck) {
    return null
}

def destinationPriceMinusBasePrice = api.getElement("DestinationPriceMinusBasePrice")
def uplift = api.getElement("UpliftPct")

if (!destinationPriceMinusBasePrice || uplift == null) return ""
return destinationPriceMinusBasePrice * (1 + uplift)