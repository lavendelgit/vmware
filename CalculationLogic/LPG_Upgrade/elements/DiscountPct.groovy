if (api.syntaxCheck) {
    return null
}

def upgradePrice = api.getElement("UpgradePrice")
def resultPrice = api.getElement("ResultPrice")

if (!upgradePrice || !resultPrice) return ""

def discount = 100 / upgradePrice.toBigDecimal() * (upgradePrice.toBigDecimal() - resultPrice.toBigDecimal()) / 100
if (discount < 0) return 0
return discount