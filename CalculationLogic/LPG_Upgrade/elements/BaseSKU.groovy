def upgradeSKU = api.getElement("UpgradeSKU")

if (!upgradeSKU) return ""

return api.findLookupTableValues("BaseToUpgrade", Filter.equal("attribute2", upgradeSKU)).attribute1[0]