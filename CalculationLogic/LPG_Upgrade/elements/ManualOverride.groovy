//Displays information on what has been changed by user override
if (api.syntaxCheck) {
    return null
}

//function "api.getManualOverride" returns null when not overriden, otherwise returns value of the override
def upgradePriceOverride = api.getManualOverride("UpgradePrice")
def UpliftPctOverride = api.getManualOverride("UpliftPct")
def discount = (api.getElement("UpliftPct") == 0) ? "Non-Uplifted Upgrade" : "Uplifted Upgrade"
def resultString = ""


if (upgradePriceOverride != null && UpliftPctOverride != null) resultString = " - adjusted by Upgrade Price and by Uplift Percentage"
if (upgradePriceOverride == null && UpliftPctOverride != null) resultString = " - adjusted by Uplift Percentage"
if (upgradePriceOverride != null && UpliftPctOverride == null) resultString = " - adjusted by Upgrade Price"
return discount + resultString