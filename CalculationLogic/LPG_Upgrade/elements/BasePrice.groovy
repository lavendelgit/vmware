def baseSKU = api.getElement("BaseSKU")

if (!baseSKU) return ""

return api.find("PX", Filter.equal("name", "ListPrice"), Filter.equal("sku", baseSKU)).attribute1[0]?.toBigDecimal()