Map selectedFilters = libs.vmwareUtil.TAMDBUtils.getTAMFilter(Constants.TAM_FILTER_CP_TABLE)
BigDecimal totalGrossBookingsCC = 0.0
ResultMatrix resultMatrix = api.newMatrix(Constants.COLUMN_DATA_FILTER_LABEL, Constants.COLUMN_PERCENTAGE_CHANGE_LABEL, Constants.COLUMN_GROSS_BOOKING_CC_LABEL, Constants.COLUMN_TAM_LABEL)
Map row = [:]
List queriedPriceingDiscount = []
for (selectedFilter in selectedFilters) {
    row = [:]
    queriedPriceingDiscount = []
    filterMap = selectedFilter?.value?.DataFilter
    filters = api.filterFromMap(api.jsonDecode(filterMap))
    filterDisplay = filters.toString()
    percentageChange = (selectedFilter?.value?.PercentageChange?.toBigDecimal()) ?: 0.0
    totalGrossBookingsCC = 0.0
    def ctx = api.getDatamartContext()
    def dm = ctx.getDatamart(Constants.PRICING_DISCOUNTING_DM)
    def query = ctx?.newQuery(dm)
            ?.select(Constants.COLUMN_GROSS_BOOKING_CC, Constants.COLUMN_GROSS_BOOKING_CC)
            ?.where(filters)

    streamResult = ctx?.streamQuery(query)
    while (streamResult?.next()) {
        queriedPriceingDiscount << streamResult.get()
    }
    streamResult?.close()

    queriedPriceingDiscount?.each { data ->
        totalGrossBookingsCC += data?.GrossBookingsCC ? data?.GrossBookingsCC?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00
    }
    estimatedTAM = (totalGrossBookingsCC + (totalGrossBookingsCC * (percentageChange / 100))).setScale(2, BigDecimal.ROUND_HALF_UP)

    row.put(Constants.COLUMN_DATA_FILTER_LABEL, filterDisplay)
    row.put(Constants.COLUMN_PERCENTAGE_CHANGE_LABEL, percentageChange)
    row.put(Constants.COLUMN_GROSS_BOOKING_CC_LABEL, totalGrossBookingsCC)
    row.put(Constants.COLUMN_TAM_LABEL, estimatedTAM)
    resultMatrix.addRow(row)
    resultMatrix
}
return resultMatrix