Map payload = [data: [filterCriteria: [:]]]
def table = api.findLookupTable(Constants.TAM_FILTER_CP_TABLE)
api.boundCall("boundcall", "lookuptablemanager.delete/${table.id}/batch", api.jsonEncode(payload))