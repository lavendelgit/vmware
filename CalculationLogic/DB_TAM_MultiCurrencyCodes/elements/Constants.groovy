import groovy.transform.Field

@Field final String INPUT_FILTER = "Input Filter"
@Field final String CFG_TAMDB_FILTER = "CFG_TAMDB_Filter"
@Field final String COLUMN_GROSS_BOOKING_CC = "GrossBookingsCC"
@Field final String COLUMN_DATA_FILTER_LABEL = "Data Filter"
@Field final String COLUMN_PERCENTAGE_CHANGE_LABEL = "Percentage Change(%)"
@Field final String COLUMN_GROSS_BOOKING_CC_LABEL = "Gross Booking CC"
@Field final String COLUMN_TAM_LABEL = "Estimated TAM"
@Field final String PRICING_DISCOUNTING_DM = "PricingandDiscountingDM"
@Field final String TAM_FILTER_CP_TABLE = "TAMFilter"

