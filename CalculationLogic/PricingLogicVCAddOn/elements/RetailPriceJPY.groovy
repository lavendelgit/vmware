def listPrice = out.FinalRetailPrice
def rate = out.ConversionFactorJPY
def LC = out.LCFactor
String currency = "JPY"

if (!LC || !rate || !listPrice) return ""

def lcPrice = listPrice * rate
def threshHold = 5000
def roundToThresholdMet = 100
def roundToThresholdNotMet = 10
def substract = 10
def JPYCur = libs.vmwareUtil.VCRounding.getCurrencyValue(currency, out.ProductType, api.global.roundingRulesMap)

if (out.ProductType == "Perpetual- SnS") {
    return lcPrice.setScale(0, BigDecimal.ROUND_HALF_UP)

} else {
    if (LC == "LC-77") {
        return libs.vmwareUtil.RoundingLogicDesktopV2.applyRoundingLogic(lcPrice, threshHold, substract, roundToThresholdMet, roundToThresholdNotMet)?.toBigDecimal()
    } else {
        return libs.vmwareUtil.VCRounding.roundedLCPrice(lcPrice, JPYCur.RoundingType, JPYCur.Precision)
    }
}