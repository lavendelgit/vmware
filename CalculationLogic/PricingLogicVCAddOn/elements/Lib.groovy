BigDecimal getFx(String LC, String attributeName) {
    return api.global.localCurrencyExchangeMap[LC]?.getAt(attributeName) as BigDecimal
}