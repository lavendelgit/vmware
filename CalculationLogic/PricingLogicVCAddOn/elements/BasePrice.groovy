def addOnType
def basePrice
def sku = out.SKU
def basePricesRecords = out.LoadAddonBasePrices

/*Sort needed because "DHO" addon type is a subset of "BLC-DHO" addon type, resulting into two matches
due to "sku.contains(addOnType)" function present below.
*/

basePricesRecords.sort {
    it.attribute1.size()
}

/*Sort descending order, so longer addon types are picked up first,
after first match, lookup ends, achieved by "&& basePrice == null".
*/
basePricesRecords = basePricesRecords.reverse()


basePricesRecords.each {
    addOnType = it.attribute1

    if (sku.contains(addOnType) && basePrice == null) {
        basePrice = it?.attribute3
    }
}

return basePrice