if (!api.global.batch) {
    api.global.batch = [:]
}

if (!api.global.billingTermFactorMap) {
    api.global.billingTermFactorMap = libs.vmwareUtil.CacheManager.cacheTermsFactor(["All", Constants.STR_PRODUCT], ["All", Constants.STR_ADDON_OFFER], ["All", Constants.STR_BASE])
}


if (!api.global.payFreqMap) {
    api.global.payFreqMap = libs.vmwareUtil.CacheManager.cachePaymentFrequncyLabel()
}

if (!api.global.localCurrencyExchangeMap) {
    def table = api.findLookupTable("LocalCurrencyExchangeRates")
    filter = [
            Filter.equal("lookupTable.id", table.id)
    ]
    api.global.localCurrencyExchangeMap = api.find("MLTV", 0, api.getMaxFindResultsLimit(), null, ["name", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6", "attribute7", "attribute8"], *filter).collectEntries {
        [(it.name): it]
    }
}

if (!api.global.roundingRulesMap) {
    def table = api.findLookupTable("RoundingRulesConfiguration")
    filter = [
            Filter.equal("lookupTable.id", table.id)
    ]
    api.global.roundingRulesMap = api.find("MLTV4", 0, api.getMaxFindResultsLimit(), null, ["key1", "key2", "key3", "key4", "attribute1", "attribute2"], *filter).collectEntries {
        [(it.key1 + it.key2 + it.key3 + it.key4): it]
    }
}

api.local.pid = api.product("sku")


if (!api.global.batch[api.local.pid]) {

    api.global.batch.clear()
    def batch = api.getBatchInfo()?.collect { it[0] } ?: [api.local.pid]

    filter = [
            Filter.equal("name", "VCAddOnProducts"),
            Filter.in("sku", batch)

    ]

    def vcAddOnPXMap = api.find("PX20", *filter)?.collectEntries { [(it.sku): it] }

    batch.each {
        api.global.batch[it] = [
                "vcAddOnPXObj": (vcAddOnPXMap[it] ?: null)

        ]
    }

}

return