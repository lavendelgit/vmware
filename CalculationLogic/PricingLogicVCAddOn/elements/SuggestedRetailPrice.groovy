if (!out.BasePrice)
    return
BigDecimal cycle = out.YearsFactor?.toBigDecimal() ?: null
BigDecimal srp = out.BasePrice
srp = out.Years ? (srp * out.Years) : srp
srp = out.BillingTermFactor ? (srp * out.BillingTermFactor as BigDecimal) : srp
srp = cycle ? (srp / cycle) : srp

return (srp ? srp.setScale(0, RoundingMode.UP) : 0.0) as BigDecimal