def bandwidth = out.Bandwidth

def maxResults = api.getMaxFindResultsLimit()
def basePriceTable = api.findLookupTable("AddonBasePrice")

def recList = api.find("MLTV", 0, maxResults, null,
        Filter.equal("attribute2", bandwidth),
        Filter.equal("lookupTable.id", basePriceTable.id))


if (recList)
    return recList