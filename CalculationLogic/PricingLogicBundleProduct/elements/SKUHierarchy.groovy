api.local.skuProductGroup = out.SKUInfo?.attribute39
api.local.skuProduct = out.SKUInfo?.attribute1
api.local.skuOffer = out.SKUInfo?.attribute2
api.local.skuBase = out.SKUInfo?.attribute4
api.local.stdSkuDetails = [
        ProductGroup: api.local.skuProductGroup,
        Product     : api.local.skuProduct,
        Offer       : api.local.skuOffer,
        Base        : api.local.skuBase
]
return