if (!out.VPPLevel && !api.local.baseVPPLevel || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.VPPLevel))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Purchasing Program",
        Value: api.local.baseVPPLevel ?: null
]
Map stdSkuAttribute = [
        Name : "Purchasing Program",
        Value: out.VPPLevel ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.purchasingProgramBundleAdjustmentCache, api.global.purchasingProgramStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
