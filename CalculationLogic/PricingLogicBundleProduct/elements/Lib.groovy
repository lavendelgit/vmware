Map getPrecision(Map rulesCache, String productGroup, String product, String offer, String base, List adjs) {
    String key
    Map rounding = [:]
    sortedCombinations = getSortedCombinations(productGroup, product, offer, base, adjs)
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        rounding.Precision = rulesCache?.getAt(key)?.Precision as Integer
        rounding.RoundingType = rulesCache?.getAt(key)?.Type
        if (rounding.RoundingType)
            return rounding
    }
}

Map getSortedCombinations(String productGroup, String product, String offer, String base, List adjs) {
    List combinations = []
    combinations << [productGroup, "*"]
    combinations << [product, "*"]
    combinations << [offer, "*"]
    combinations << [base, "*"]
    for (adj in adjs) {
        combinations << [adj, "*"]
    }
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ libs.vmwareUtil.CacheManager.getWeightage(it, "*") })?.sort { -it.key }

}


BigDecimal getAdjustments(Map adjustmentCache, String productGroup, String product, String offer, String base, List adjs) {
    Map sortedCombinations = getSortedCombinations(productGroup, product, offer, base, adjs)
    BigDecimal adjustment
    String key
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        adjustment = adjustmentCache?.getAt(key)?.Adjustment
        if (adjustment) {
            return adjustment
        }
    }
}

List getAdjustmentsFromCache(Map adjustmentsCache, Boolean isBaseAdj) {
    List keyLists = []
    Map keys = [:]
    Map baseKeys = [:]
    adjustmentsCache?.each {
        adj ->
            keys = [:]
            baseKeys = [:]
            adj.value.Keys?.each {
                key ->
                    key = key?.replaceAll("\\s", "")
                    if (isBaseAdj) {
                        if (api.local["base" + key])
                            keys[key] = api.local["base" + key]
                    } else {
                        if (out[key])
                            keys[key] = out[key]
                    }
            }

            keyLists << keys
    }

    keyLists = keyLists?.minus(Constants.PRODUCT_HIERARCHIES)
    return keyLists

}

Map populateTermAltUoMAdjustments(Map adjustmentCache) {
    String term
    String termUoM
    Map populatedTermCache = [:]
    Map destTerm = [:]
    adjustmentCache?.findAll { it.value.Term != null && it.value["Term UoM"] != null }?.each {
        term = it.value.Term
        termUoM = it.value["Term UoM"]
        destTerm = api.global.termUoMConversionCache[term + "_" + termUoM]
        if (destTerm) {
            it.value.Term = destTerm.DestTerm
            it.value["Term UoM"] = destTerm.DestUoM
            key = frameTermKeys(it.value)
            populatedTermCache[key] = it.value
        }
    }
    return populatedTermCache
}

Map cacheAdjustments(List adjustmentCache, Map metaData, String factorAttribute, String typeAttribute = null, boolean isRoundingRule = false, boolean isBundleAdjustment = false) {
    Map factorAdjustmentCache = [:]
    List adjustments = adjustmentCache?.findAll { it[factorAttribute] != null }
    List keys
    int termIndex
    int productIndex
    int productGroupIndex
    if (!isBundleAdjustment) {
        excludeList = ["version", "typedId", "id", "name", "sku", "createDate", "createdBy", "lastUpdateDate", "lastUpdateBy", factorAttribute, "attribute41", "attribute42"]
    }
    if (isBundleAdjustment) {
        excludeList = ["version", "typedId", "id", "name", "sku", "createDate", "createdBy", "lastUpdateDate", "lastUpdateBy", factorAttribute, "attribute36", "attribute35", "attribute43", "attribute44"]
    }
    if (isRoundingRule)
        excludeList << typeAttribute
    adjustments?.each {
        keys = it.keySet().collect()
        keysList = keys?.minus(excludeList)
        termIndex = keysList?.indexOf("attribute7")
        if (termIndex != -1) {
            keysList = keysList?.minus(["attribute33"])
            keysList = keysList.plus(termIndex + 1, "attribute33")
        }
        if (!isBundleAdjustment) {
            if (productGroupIndex != -1) {
                keysList = keysList?.minus(["attribute50"])
                keysList = keysList.plus(productIndex, "attribute50")
            }
        }
        if (isBundleAdjustment) {
            if (productGroupIndex != -1) {
                keysList = keysList?.minus(["attribute45"])
                keysList = keysList.plus(productIndex, "attribute45")
            }
        }
        if (isRoundingRule) {
            regionIndex = keysList?.indexOf("attribute4")
            currencyIndex = keysList?.indexOf("attribute23")
            if (regionIndex != -1) {
                keysList = keysList?.minus(["attribute4"])
                keysList << "attribute4"
            }
            if (currencyIndex != -1) {
                keysList = keysList?.minus(["attribute23"])
                keysList << "attribute23"
            }

        }
        keyNames = []
        keysList?.each {
            key ->
                keyNames << metaData[key]
        }
        keyValueList = []
        Map record = [:]
        keysList?.each {
            key ->
                if (it[key]) {
                    keyValueList << it[key]
                    record[metaData[key]] = it[key]
                }
        }
        key = libs.vmwareUtil.util.frameKey(keyValueList)
        if (!isRoundingRule) {
            record.Adjustment = it.attribute36 ?: it[factorAttribute]
            record.Keys = keyNames
            factorAdjustmentCache[key] = record
        } else {
            record.Precision = it[factorAttribute]
            record.Type = it[typeAttribute]
            record.Keys = keyNames
            factorAdjustmentCache[key] = record
        }
    }
    factorAdjustmentCache = factorAdjustmentCache?.sort { -it.value?.Keys?.size() }
    return factorAdjustmentCache
}

String frameTermKeys(Map termAdj) {
    String keyString
    List keys = termAdj?.Keys
    keys?.each {
        key ->
            keyString = keyString ? keyString + "_" + termAdj[key] : termAdj[key]
    }
    return keyString
}

Map cacheAndPopulateTermUoMAdjustments(List adjustmentCache, Map metaDataCache, String factorAttribute, Boolean isBundleAdjustment = false) {

    Map factorAdjustmentCache = cacheAdjustments(adjustmentCache, metaDataCache, factorAttribute, null, false, isBundleAdjustment)
    factorAdjustmentCache = populateTermUoMAdjustments(factorAdjustmentCache)
    return factorAdjustmentCache
}

Map populateTermUoMAdjustments(Map factorAdjustmentCache) {
    Map populatedTermCache = [:]
    Map tempAdjustmentCache = [:]
    if (factorAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
        factorAdjustmentCache.each { key, value -> tempAdjustmentCache[key] = value.clone() }
        populatedTermCache = populateTermAltUoMAdjustments(tempAdjustmentCache)
        if (populatedTermCache?.size() > 0 && factorAdjustmentCache?.size() > 0) {
            factorAdjustmentCache = factorAdjustmentCache + populatedTermCache
        }
    }
    return factorAdjustmentCache
}

BigDecimal getAdjustmentsForAttribute(Map skuAdjustmentCache, Map baseAdjustmentCache, Map baseSKUDetail, Map stdSKUDetail, Map baseAdjustmentDetail, Map stdAdjustmentDetail) {
    List baseAdjkeyLists = getAdjustmentsFromCache(baseAdjustmentCache, true)
    List skuAdjKeyLists = getAdjustmentsFromCache(skuAdjustmentCache, false)
    BigDecimal skuAdjustment
    BigDecimal baseAdjustment
    baseAdjkeyLists?.unique()
    for (keyList in baseAdjkeyLists) {
        baseAdjustment = getAdjustments(baseAdjustmentCache, baseSKUDetail.ProductGroup, baseSKUDetail.Product, baseSKUDetail.Offer, baseSKUDetail.Base, keyList.values().drop((Constants.PRODUCT_HIERARCHIES)?.size()))
        if (baseAdjustment) {
            break
        }
    }
    skuAdjKeyLists?.unique()
    for (keyList in skuAdjKeyLists) {
        skuAdjustment = getAdjustments(skuAdjustmentCache, stdSKUDetail.ProductGroup, stdSKUDetail.Product, stdSKUDetail.Offer, stdSKUDetail.Base, keyList.values().drop((Constants.PRODUCT_HIERARCHIES)?.size()))
        if (skuAdjustment) {
            break
        }
    }

    if (!baseAdjustment) {
        if (baseAdjustmentDetail.Value) {
            api.addWarning("Base " + baseAdjustmentDetail.Name + "factor not found for " + baseSKUDetail.Product + "," + baseSKUDetail.Offer + ", " + baseSKUDetail.Base + ", " + baseAdjustmentDetail.Value)
        } else {
            baseAdjustment = 1
        }

    }
    if (!skuAdjustment) {
        if (stdAdjustmentDetail?.Value) {
            api.addWarning("SKU " + stdAdjustmentDetail.Name + "factor not found for " + stdSKUDetail.Product + "," + stdSKUDetail.Offer + ", " + stdSKUDetail.Base + ", " + stdAdjustmentDetail?.Value)
        } else {
            skuAdjustment = 1
        }
    }
    if (baseAdjustment && skuAdjustment) {
        return (skuAdjustment / baseAdjustment) as BigDecimal
    }

    return 1
}

BigDecimal convertToSpecificCurrenyRegion(Map currencyRegionRulesCache, Map upliftCache, Map currencyAdjustmentCache, Map roundingRulesCache, Map stdSkuDetails, String product, String region, String currency) {

    if (currencyRegionRulesCache[product]?.findAll { it.Currency == currency && it.PriceListRegion == region }) {
        List keyLists = getAdjustmentsFromCache(upliftCache, false)
        BigDecimal uplift
        BigDecimal currencyFxRate
        for (keyList in keyLists) {
            upliftKeyList = keyList.values().drop((Constants.PRODUCT_HIERARCHIES)?.size())
            upliftKeyList << region
            upliftKeyList << currency
            uplift = getAdjustments(upliftCache, stdSkuDetails.ProductGroup, stdSkuDetails.Product, stdSkuDetails.Offer, stdSkuDetails.Base, upliftKeyList)
            if (uplift) {
                break
            }
        }

        keyLists = getAdjustmentsFromCache(currencyAdjustmentCache, false)
        for (keyList in keyLists) {
            upliftKeyList = keyList.values().drop((Constants.PRODUCT_HIERARCHIES)?.size())
            upliftKeyList << region
            upliftKeyList << currency
            currencyFxRate = getAdjustments(currencyAdjustmentCache, stdSkuDetails.ProductGroup, stdSkuDetails.Product, stdSkuDetails.Offer, stdSkuDetails.Base, upliftKeyList)
            if (currencyFxRate) {
                break
            }
        }
        if (!uplift)
            api.addWarning("Uplift not found")
        else if (!currencyFxRate) {
            api.addWarning("FXRate not found")
        }
        if (uplift && currencyFxRate) {
            keyLists = getAdjustmentsFromCache(roundingRulesCache, false)
            Map roundingRule = [:]
            for (keyList in keyLists) {
                upliftKeyList = keyList.values().drop((Constants.PRODUCT_HIERARCHIES)?.size())
                upliftKeyList << region
                upliftKeyList << currency
                roundingRule = getPrecision(roundingRulesCache, stdSkuDetails.ProductGroup, stdSkuDetails.Product, stdSkuDetails.Offer, stdSkuDetails.Base, upliftKeyList)
                if (roundingRule?.Precision != null) {
                    break
                }
            }
            if (roundingRule?.Precision == null) {
                for (keyList in keyLists) {
                    upliftKeyList = keyList.values().drop((Constants.PRODUCT_HIERARCHIES)?.size())
                    upliftKeyList << currency
                    roundingRule = getPrecision(roundingRulesCache, stdSkuDetails.ProductGroup, stdSkuDetails.Product, stdSkuDetails.Offer, stdSkuDetails.Base, upliftKeyList)
                    if (roundingRule?.Precision != null) {
                        break
                    }
                }
            }
            precision = ((roundingRule?.Precision != null) ? roundingRule.Precision : 2) as Integer
            roundingType = roundingRule?.RoundingType ?: "Round"
            listPrice = out.ListPrice * uplift * currencyFxRate
            return libs.vmwareUtil.PricingHelper.roundedPrice(listPrice, roundingType, precision)
        }
        return null
    }
}
