if (!out.Promotion && !api.local.basePromotion || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Promotion))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Promotion",
        Value: api.local.basePromotion ?: null
]
Map stdSkuAttribute = [
        Name : "Promotion",
        Value: out.Promotion ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.promotionBundleAdjustmentCache, api.global.promotionStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
