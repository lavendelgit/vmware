if (!out.SupportTier && !api.local.baseSupportTier || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.SupportTier))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Support Tier",
        Value: api.local.baseSupportTier ?: null
]
Map stdSkuAttribute = [
        Name : "Support Tier",
        Value: out.SupportTier ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.supportTierBundleAdjustmentCache, api.global.supportTierStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)