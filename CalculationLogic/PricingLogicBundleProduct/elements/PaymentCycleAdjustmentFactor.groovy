String term = out.Term
String paymentType = out.PaymentType
BigDecimal factor = 1

if (paymentType == "Annual") {
    factor = (out.Term as BigDecimal) / 12 as BigDecimal
} else if (paymentType == "Monthly") {
    factor = term as BigDecimal
}

return factor as BigDecimal