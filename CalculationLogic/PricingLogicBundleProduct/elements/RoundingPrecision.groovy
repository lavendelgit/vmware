def keyLists = Lib.getAdjustmentsFromCache(api.global.roundingRulesCache, false)
Map roundingRule = [:]
List roundingKeysList = []
for (keyList in keyLists) {
    roundingKeysList = keyList.values().drop((Constants.PRODUCT_HIERARCHIES)?.size())
    roundingKeysList << "*"
    roundingKeysList << "*"
    roundingRule = Lib.getPrecision(api.global.roundingRulesCache, api.local.skuProductGroup, api.local.skuProduct, api.local.skuOffer, api.local.skuBase, roundingKeysList)
    if (roundingRule?.Precision != null) {
        break
    }
}
api.local.precision = (roundingRule?.Precision != null ? roundingRule?.Precision : 2) as Integer
api.local.roundingType = roundingRule?.RoundingType ?: "Round"
return