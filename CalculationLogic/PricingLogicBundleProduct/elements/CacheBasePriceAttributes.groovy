if (!api.global.basepriceCache) {
    List filter = [
            Filter.equal("name", "StandardBasePrice"),
            //Filter.equal("attribute27", "Yes")
    ]
    stdAdjustmentStream = api.stream("PX50", "sku", *filter)
    api.global.basepriceCache = stdAdjustmentStream?.collectEntries { [(it.attribute1): it] }
    stdAdjustmentStream?.close()
}
return
