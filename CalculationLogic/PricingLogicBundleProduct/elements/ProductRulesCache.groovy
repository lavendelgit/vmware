if (!api.global.productRulesCache) {
    api.global.productRulesCache = [:]
    List fields = ["sku", "attribute4", "attribute5", "attribute6"]
    List filter = [
            Filter.equal("name", "BundleProductRules"),
            Filter.equal("attribute20", "Valid")
    ]
    productRulesStream = api.stream("PX30", null, fields, *filter)
    productRulesCache = productRulesStream?.collect { row -> row }
    productRulesStream?.close()
    for (productRules in productRulesCache) {
        def currencyDetails = [
                "Currency"       : productRules.attribute4,
                "PriceListRegion": productRules.attribute5,
        ]
        if (api.global.productRulesCache[productRules.sku]) {
            api.global.productRulesCache[productRules.sku] << currencyDetails
        } else {
            api.global.productRulesCache[productRules.sku] = [currencyDetails]
        }
    }
}
return
