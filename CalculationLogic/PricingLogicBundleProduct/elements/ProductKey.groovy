List productKey = [out.ProductGroup]
if (out.Product)
    productKey << out.Product
if (out.Offer)
    productKey << out.Offer
if (out.Base)
    productKey << out.Base
if (out.OS)
    productKey << out.OS
if (out.Metric)
    productKey << out.Metric
if (out.DataCenter)
    productKey << out.DataCenter
if (out.SegmentType)
    productKey << out.SegmentType
if (out.Quantity)
    productKey << out.Quantity
if (out.Term)
    productKey << out.Term
if (out.TermUoM)
    productKey << out.TermUoM
if (out.PaymentType)
    productKey << out.PaymentType
if (out.SupportType)
    productKey << out.SupportType
if (out.SupportTier)
    productKey << out.SupportTier
if (out.VPPLevel)
    productKey << out.VPPLevel
if (out.VolumeTierSize)
    productKey << out.VolumeTierSize
if (out.Version)
    productKey << out.Version
if (out.Promotion)
    productKey << out.Promotion
if (out.Hosting)
    productKey << out.Hosting
if (out.ProgramOption)
    productKey << out.ProgramOption

return productKey?.join("_")
