import groovy.transform.Field

@Field final String CURRENCY = "USD"
@Field final String REGION_NAM_USD = "NA-USD"
@Field final String REGION_APAC_USD = "APAC-USD"
@Field final String REGION_CHINA_USD = "C-USD"
@Field final String REGION_LATAM_USD = "LA-USD"
@Field final String REGION_EMEA_USD = "EMEA-USD"
@Field final String REGION_EMEA_USD2 = "EMEA2-USD"
@Field final String REGION_GLOBAL_USD = "G-USD"
@Field final String REGION_EMEA_EUR = "EMEA-EUR"
@Field final String REGION_EMEA_GBP = "EMEA-GBP"
@Field final String REGION_CHINA_CNY = "APAC-CNY"
@Field final String REGION_APAC_AUD = "APAC-AUD"
@Field final String REGION_APAC_JPY = "APAC-JPY"
@Field final List PRODUCT_HIERARCHIES = ["Product Group", "Product", "Offer", "Base"]
