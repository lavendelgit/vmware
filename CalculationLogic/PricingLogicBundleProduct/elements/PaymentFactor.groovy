if (!out.PaymentType && !api.local.basePaymentType || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.PaymentType))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Payment",
        Value: api.local.basePaymentType ?: null
]
Map stdSkuAttribute = [
        Name : "Payment",
        Value: out.PaymentType ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.paymentBundleAdjustmentCache, api.global.paymentStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
