if (!out.Upgrade && !api.local.baseUpgrade || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Upgrade))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Upgrade",
        Value: api.local.baseUpgrade ?: null
]
Map stdSkuAttribute = [
        Name : "Upgrade",
        Value: out.Upgrade ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.upgradeAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)