if (!out.DataCenter && !api.local.baseDataCenter || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.DataCenter))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Data Center",
        Value: api.local.baseDataCenter ?: null
]
Map stdSkuAttribute = [
        Name : "Data Center",
        Value: out.DataCenter ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.dataCenterBundleAdjustmentCache, api.global.dataCenterStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
