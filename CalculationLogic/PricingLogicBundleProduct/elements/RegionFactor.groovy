if (!out.Region && !api.local.baseRegion || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Region))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Region",
        Value: api.local.baseRegion ?: null
]
Map stdSkuAttribute = [
        Name : "Region",
        Value: out.Region ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.regionBundleAdjustmentCache, api.global.regionStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
