if (api.global.baseSKUCache) {
    List filter = [
            Filter.equal("name", "StandardProducts"),
            Filter.in("attribute14", api.global.basepriceCache?.keySet())
    ]
    recordStream = api.stream("PX50", null, ["sku", "attribute14"], *filter)
    api.global.baseSKUCache = recordStream?.collectEntries { [(it.sku): it.attribute14] }
    recordStream?.close()
}

return