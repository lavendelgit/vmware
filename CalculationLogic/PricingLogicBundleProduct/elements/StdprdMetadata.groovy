api.local.standardProductMetaData = api.find("PXAM", Filter.equal("name", "StandardProducts"))?.collectEntries {
    [(it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()): it.fieldName]
}

