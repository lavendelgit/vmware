api.local.standardBasePriceMetaData = api.find("PXAM", Filter.equal("name", "StandardBasePrice"))?.collectEntries {
    [(it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()): it.fieldName]
}

