if (!out.Term && !api.local.baseTerm || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Term))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Term",
        Value: api.local.baseTerm ?: null
]
Map stdSkuAttribute = [
        Name : "Term",
        Value: out.Term ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.termBundleAdjustmentCache, api.global.termStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)