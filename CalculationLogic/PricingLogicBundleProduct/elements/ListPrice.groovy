BigDecimal listPrice = (out.BasePrice * out.CurrencyFactor * out.RegionFactor * out.PromotionFactor * out.VolTierFactor * out.DCFactor * out.PurchasingProgramFactor *
        out.SegmentFactor * out.SupportTypeFactor * out.SupportTierFactor * out.TermTierFactor * out.TermUoMFactor * out.PaymentFactor * out.MetricFactor * out.QuantityAdjustment * out.UpgradeFactor * out.HostingFactor * out.ProgramOptionFactor) as BigDecimal
if (out.PaymentCycleAdjustmentFactor)
    listPrice = listPrice / out.PaymentCycleAdjustmentFactor as BigDecimal
return listPrice
