if (!api.global.stdAdjustmentsCache) {
    List filter = [
            Filter.equal("name", "StandardAdjustment"),
    ]
    stdAdjustmentStream = api.stream("PX50", "sku", *filter)
    api.global.stdAdjustmentsCache = stdAdjustmentStream?.collect { row -> row }
    stdAdjustmentStream?.close()
}
return
