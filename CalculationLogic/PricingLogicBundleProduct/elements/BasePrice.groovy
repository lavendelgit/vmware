BigDecimal baseprice = 0.0
if (out.SKUInfo[api.local.standardProductMetaData["Bundle Base Reference"]] == "Bundle") {
    baseprice = api.global.basepriceCache[out.BasePriceSKU]?.attribute23 ?: api.global.basepriceCache[out.BasePriceSKU]?.attribute6
} else if (out.SKUInfo[api.local.standardProductMetaData["Bundle Base Reference"]] == "Sum of Components") {
    baseprice = api.global.basepriceCache[out.BasePriceSKU]?.attribute6
}
return baseprice

