if (!out.ProgramOption && !api.local.baseProgramOption || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.ProgramOption))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "ProgramOption",
        Value: api.local.baseProgramOption ?: null
]
Map stdSkuAttribute = [
        Name : "ProgramOption",
        Value: out.ProgramOption ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.ProgrammaticBundleAdjustmentCache, api.global.programmaticStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
