if (!out.Hosting && !api.local.baseHosting || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Hosting))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Hosting",
        Value: api.local.baseHosting ?: null
]
Map stdSkuAttribute = [
        Name : "Hosting",
        Value: out.Hosting ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.HostingBundleAdjustmentCache, api.global.hostingStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
