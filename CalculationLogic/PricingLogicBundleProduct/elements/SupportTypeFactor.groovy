if (!out.SupportType && !api.local.baseSupportType || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.SupportType))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Support Type",
        Value: api.local.baseSupportType ?: null
]
Map stdSkuAttribute = [
        Name : "Support Type",
        Value: out.SupportType ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.supportTypeBundleAdjustmentCache, api.global.supportTypeStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)