BigDecimal baseQty = api.local.baseQuantity as BigDecimal
BigDecimal skuQty = out.Quantity as BigDecimal
if (skuQty && baseQty) {
    return skuQty / baseQty
}
return 1
