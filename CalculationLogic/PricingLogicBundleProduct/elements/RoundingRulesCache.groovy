if (!api.global.roundingRulesCache) {
    api.global.roundingRulesCache = [:]
    api.global.roundingRulesCache = Lib.cacheAdjustments(api.global.bundleAdjustmentsCache, api.global.bundleAdjMetaData, "attribute26", "attribute25", true, true)
    api.global.roundingRulesCache = Lib.populateTermUoMAdjustments(api.global.roundingRulesCache)
}
return
//@todo Roundin rules cache from Bundle Adjustment