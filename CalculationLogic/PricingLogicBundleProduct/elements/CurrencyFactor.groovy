if (!out.Currency && !api.local.baseCurrency || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Currency))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Currency",
        Value: api.local.baseCurrency ?: null
]
Map stdSkuAttribute = [
        Name : "Currency",
        Value: out.Currency ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.currencyBundleAdjustmentCache, api.global.currencyStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)


