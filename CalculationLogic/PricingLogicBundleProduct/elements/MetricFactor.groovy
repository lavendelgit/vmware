if (!out.Metric && !api.local.baseMetric || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.Metric))) {
    return 1
}
Map baseSkuAttribute = [
        Name : "Metric",
        Value: api.local.baseMetric ?: null
]
Map stdSkuAttribute = [
        Name : "Metric",
        Value: out.Metric ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.metricBundleAdjustmentCache, api.global.metricStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
