if (!out.SegmentType && !api.local.baseSegmentType || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.SegmentType))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Segment",
        Value: api.local.baseSegmentType ?: null
]
Map stdSkuAttribute = [
        Name : "Segment",
        Value: out.SegmentType ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.segmentBundleAdjustmentCache, api.global.segmentStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
