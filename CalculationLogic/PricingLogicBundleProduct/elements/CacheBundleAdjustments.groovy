if (!api.global.bundleAdjustmentsCache) {
    List filter = [
            Filter.equal("name", "BundleAdjustment"),
    ]
    stdAdjustmentStream = api.stream("PX50", "sku", *filter)
    api.global.bundleAdjustmentsCache = stdAdjustmentStream?.collect { row -> row }
    stdAdjustmentStream?.close()
}
return
