if (!out.VolumeTierSize && !api.local.baseVolumeTierSize || (libs.stdProductLib.SkuGenHelper.validAttribute(api.global.excludeAttributesCache, out.VolumeTierSize))) {
    return 1
}

Map baseSkuAttribute = [
        Name : "Volume Tier",
        Value: api.local.baseVolumeTierSize ?: null
]
Map stdSkuAttribute = [
        Name : "Volume Tier",
        Value: out.VolumeTierSize ?: null
]
return Lib.getAdjustmentsForAttribute(api.global.volumeTierBundleAdjustmentCache, api.global.volumeTierStdAdjustmentCache, api.local.baseSkuDetails, api.local.stdSkuDetails, baseSkuAttribute, stdSkuAttribute)
