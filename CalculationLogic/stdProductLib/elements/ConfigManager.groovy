import groovy.transform.Field

Map attributesConfigManager() {
    return [
            PRODUCT_TABLE              : libs.stdProductLib.ConstConfiguration.PRODUCT_TABLE,
            OFFER_TABLE                : libs.stdProductLib.ConstConfiguration.OFFER_TABLE,
            DATACENTER_TABLE           : libs.stdProductLib.ConstConfiguration.DATACENTER_TABLE,
            BASE_TABLE                 : libs.stdProductLib.ConstConfiguration.BASE_TABLE,
            METRIC_TABLE               : libs.stdProductLib.ConstConfiguration.METRIC_TABLE,
            TERM_TABLE                 : libs.stdProductLib.ConstConfiguration.TERM_TABLE,
            PAYMENTTYPE_TABLE          : libs.stdProductLib.ConstConfiguration.PAYMENTTYPE_TABLE,
            VOLUMETIER_TABLE           : libs.stdProductLib.ConstConfiguration.VOLUMETIER_TABLE,
            SEGMENT_TABLE              : libs.stdProductLib.ConstConfiguration.SEGMENT_TABLE,
            VERSION_TABLE              : libs.stdProductLib.ConstConfiguration.VERSION_TABLE,
            PROMOTION_TABLE            : libs.stdProductLib.ConstConfiguration.PROMOTION_TABLE,
            OS_TABLE                   : libs.stdProductLib.ConstConfiguration.OS_TABLE,
            QUANTITY_TABLE             : libs.stdProductLib.ConstConfiguration.QUANTITY_TABLE,
            SUPPORTTYPE_TABLE          : libs.stdProductLib.ConstConfiguration.SUPPORTTYPE_TABLE,
            SUPPORTTIER_TABLE          : libs.stdProductLib.ConstConfiguration.SUPPORTTIER_TABLE,
            PURCHASINGPROGRAM_TABLE    : libs.stdProductLib.ConstConfiguration.PURCHASINGPROGRAM_TABLE,
            CURRENCY_TABLE             : libs.stdProductLib.ConstConfiguration.CURRENCY_TABLE,
            PRICELISTREGION_TABLE      : libs.stdProductLib.ConstConfiguration.PRICELISTREGION_TABLE,
            SUBREGION_TABLE            : libs.stdProductLib.ConstConfiguration.SUBREGION_TABLE,
            HOSTING_TABLE              : libs.stdProductLib.ConstConfiguration.HOSTING_TABLE,
            PROGRAM_OPTION_TABLE       : libs.stdProductLib.ConstConfiguration.PROGRAM_OPTION_TABLE,
            SAP_BASE_SKU_TABLE         : libs.stdProductLib.ConstConfiguration.SAP_BASE_SKU_TABLE,
            CONFIG_TAB_ATTRIBUTES_TABLE: libs.stdProductLib.ConstConfiguration.CONFIG_TAB_ATTRIBUTES_TABLE,
            RETENTION_PERIOD_TABLE     : libs.stdProductLib.ConstConfiguration.RETENTION_PERIOD_TABLE,
            PS_TERM_TABLE              : libs.stdProductLib.ConstConfiguration.PS_TERM_TABLE,
            PS_Option_TABLE            : libs.stdProductLib.ConstConfiguration.PS_OPTION_TABLE
    ]
}