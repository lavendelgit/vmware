List getRuleParams(List ruleKeyIndexes, List params, Map termUoMCache, Integer termUoMIndex, Integer termIndex, String termUoMPlaceholder) {
    def keyLists = []
    def termUoM
    for (ruleKey in ruleKeyIndexes) {
        termUoM = ""
        keys = params?.getAt(ruleKey)
        if (ruleKey?.contains(termUoMIndex)) {
            termUoM = termUoMCache?.getAt(params.getAt(termIndex)) ?: "-"
            keys.set(keys.indexOf(termUoMPlaceholder), termUoM)
        }

        String key = keys?.join("_")
        keyLists << key
    }
    return keyLists
}

Boolean isValidRule(List keyLists, List rulesKeySet) {
    for (key in keyLists) {
        if (rulesKeySet.contains(key)) {
            return true
        }
    }
    return false
}

Map cachePXMetaData(String tableName) {
    return api.find("PXAM", Filter.equal("name", tableName))?.collectEntries {
        [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
    }
}

Map cacheAttributesMetaData(String tableName) {
    return api.find("MLTVM", Filter.equal("lookupTableId", api.findLookupTable(tableName).id))?.collectEntries {
        [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
    }
}

Boolean isInvalidRule(List keyLists, List rulesKeySet) {

    for (key in keyLists) {
        if (rulesKeySet.contains(key)) {
            return true
        }
    }
    return false
}

List populateSKURecord(String tableName, Map params) {
    return [
            tableName,
            params.sku,
            params.Product,
            params.Offer,
            params.Base,
            params.Currency,
            params.Region,
            params.SubRegion,
            params.OS,
            params.Metric,
            params.DataCenter,
            params.Segment,
            params.Quantity,
            params.Term,
            params.TermUoM,
            params.PaymentType,
            params.SupportType,
            params.SupportTier,
            params.PurchasingProgram,
            params.VolumeTier,
            params.Version,
            params.Rule,
            params.Promotion,
            params.Hosting,
            params.ProgramOption,
            params.SAPBaseSKU,
            params.StandardProductSKU,
    ]
}

Map cacheInvalidRules(def universalRecords, def universalRuleMetaData, def ruleKeyIndexMapping) {
    Map rulesCache = [:]
    def rules = universalRecords?.findAll { it?.attribute17 == "Invalid" }
    def keys
    excludeList = ["version", "typedId", "name", "sku", "createDate", "createdBy", "lastUpdateDate", "lastUpdateBy", "attribute17", "id"]
    rules?.each { rule ->
        keys = rule.keySet().collect()
        keysList = keys.minus(excludeList)
        keyNames = []
        keyIndex = []
        keysList.each {
            key ->
                if (rule[key] != "*") {
                    keyNames << universalRuleMetaData[key]
                    keyAttribute = universalRuleMetaData[key]
                    keyForIndex = keyAttribute?.replaceAll("\\s", "")
                    keyIndex << ruleKeyIndexMapping[keyForIndex]

                }
        }
        keyValueList = []
        Map record = [:]
        keysList?.each {
            key ->
                if (rule[key] != "*") {
                    keyValueList << rule[key]
                    record[universalRuleMetaData[key]] = rule[key]
                }
        }

        key = keyValueList?.join("_")
        record.Keys = keyNames
        record.KeyIndex = keyIndex
        rulesCache[key] = record
    }
    return rulesCache
}


String frameTermKeys(Map termAdj) {
    String keyString
    List keys = termAdj?.Keys
    keys.each {
        key ->
            keyString = keyString ? keyString + "_" + termAdj[key] : termAdj[key]
    }
    return keyString
}

Map populateTermAltUoMAdjustments(Map adjustmentCache, Map termUoMConversionCache) {
    String term
    String termUoM
    Map populatedTermCache = [:]
    Map destTerm = [:]
    adjustmentCache?.findAll { it.value.Term != null && it.value["Term UoM"] != null }?.each {
        term = it.value.Term
        termUoM = it.value["Term UoM"]
        destTerm = termUoMConversionCache[term + "_" + termUoM]
        if (destTerm) {
            it.value.Term = destTerm.DestTerm
            it.value["Term UoM"] = destTerm.DestUoM
            key = frameTermKeys(it.value)
            populatedTermCache[key] = it.value
        }
    }
    return populatedTermCache
}

Map populateRuleKeyIndexMapping() {
    ruleKeyIndex = [
            "Product"           : 0,
            "Offer"             : 1,
            "DataCenter"        : 2,
            "Base"              : 3,
            "Metric"            : 4,
            "Term"              : 5,
            "TermUoM"           : 6,
            "PaymentType"       : 7,
            "Tier"              : 8,
            "Segment"           : 9,
            "Version"           : 10,
            "Standard"          : 11,
            "OS"                : 12,
            "Quantity"          : 13,
            "PurchasingProgram" : 14,
            "SupportType"       : 15,
            "SupportTier"       : 16,
            "Currency"          : 17,
            "PriceListRegion"   : 18,
            "SubRegion"         : 19,
            "Hosting"           : 20,
            "ProgramOption"     : 21,
            "RetentionPeriod"   : 22,
            "RetentionPeriodUOM": 23
    ]
    return ruleKeyIndex
}

Map cacheTermUoMConversion() {

    termUoMConversionCache = api.findLookupTableValues("TermUoMConversion", null)?.collectEntries {
        term ->
            [
                    (term.key1 + "_" + term.key2): [
                            "SrcTerm" : term.key1,
                            "SrcUoM"  : term.key2,
                            "DestTerm": term.attribute1,
                            "DestUoM" : term.attribute2
                    ]
            ]
    }
    return termUoMConversionCache

}

Map cacheCommonAttributes(String tableName, Boolean labelOnly = true) {
    if (labelOnly) {
        return api.findLookupTableValues(tableName)?.findAll { it.attribute4 == "Yes" }?.collectEntries {
            [(it.name): it.attribute1]
        }
    } else {
        return api.findLookupTableValues(tableName)?.findAll { it.attribute4 == "Yes" }?.collectEntries {
            [(it.name): it]
        }
    }

}

Map cachePriceRegionAttributes(String tableName) {
    return api.findLookupTableValues(tableName)?.findAll { it.attribute1 == "Yes" }?.collectEntries {
        [(it.name): it.name]
    }
}

List cacheDescriptionConfig(String tableName, String sortBy, List attributes) {
    List filter = [
            Filter.equal("lookupTable.id", api.findLookupTable(tableName).id)
    ]
    return api.find("MLTV", 0, api.getMaxFindResultsLimit(), sortBy, attributes, *filter)

}

List cacheInvalidUniversalRecords() {
    List filter = [
            Filter.equal("name", "UniversalRule"),
            Filter.equal("attribute17", "Invalid")
    ]
    recordStream = api.stream("PX50", null, *filter)
    universalRecords = recordStream?.collect { row -> row }
    recordStream?.close()
    return universalRecords
}

Map populateTermUoMRules(Map rulesCache, Map termUoMConversionCache) {
    Map inValidRulesCache = [:]
    if (rulesCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
        rulesCache.each { key, value -> inValidRulesCache[key] = value.clone() }
        populatedTermCache = populateTermAltUoMAdjustments(inValidRulesCache, termUoMConversionCache)
        if (populatedTermCache?.size() > 0 && rulesCache?.size() > 0) {
            rulesCache = rulesCache + populatedTermCache
        }
    }
    return rulesCache
}

List getProductRules(List combinations, Map rulesCache, List productRules, Map termUoMCache, Integer termUoMIndex, Integer termIndex, String termUoMPlaceholder) {
    List rulesKeyIndexes = rulesCache?.collect { it.value.KeyIndex }
    List rulesKeySet = rulesCache?.keySet() as List
    rulesKeySet = rulesKeySet*.toLowerCase()
    for (combination in combinations) {
        keyParams = getRuleParams(rulesKeyIndexes, combination, termUoMCache, termUoMIndex, termIndex, termUoMPlaceholder)
        keyParams = keyParams*.toLowerCase()
        invalidUnivRule = isInvalidRule(keyParams, rulesKeySet)
        if (!invalidUnivRule) {
            productRules << combination
        }
    }
    return productRules
}

Map cacheValidRules(List ruleRecords, Map productRuleMetaData, Map ruleKeyIndex, String separator = ".", boolean cachePartNumber = false) {
    Map rulesCache = [:]
    List rules = ruleRecords?.findAll { it?.attribute20 == "Valid" }
    List keys
    excludeList = ["version", "typedId", "name", "sku", "createDate", "createdBy", "lastUpdateDate", "lastUpdateBy", "attribute4", "attribute5", "attribute6", "attribute20", "attribute25", "attribute24"]
    rules?.each { rule ->
        keys = rule.keySet().collect()
        keysList = keys.minus(excludeList)
        keyNames = []
        keyIndex = []
        keysList.each {
            key ->
                if (rule[key] != "*") {
                    keyAttribute = productRuleMetaData[key]
                    keyForIndex = keyAttribute?.replaceAll("\\s", "")
                    keyIndex << ruleKeyIndex[keyForIndex]
                    keyNames << productRuleMetaData[key]
                }
        }
        keyValueList = []
        Map record = [:]
        keysList?.each {
            key ->
                if (rule[key] != "*") {
                    keyValueList << rule[key]
                    record[productRuleMetaData[key]] = rule[key]
                }
        }
        key = keyValueList?.join(separator)
        record.Keys = keyNames
        record.KeyIndex = keyIndex
        if (cachePartNumber) {
            record.StandardProductSKU = rule.attribute25
        }
        rulesCache[key] = record
    }
    return rulesCache
}

Map cacheSAPBaseSKUConfig() {
    List filter = [
            Filter.isNotEmpty("attribute1")
    ]
    Map configurationData = api.findLookupTableValues("SAPBaseSKUConfiguration", *filter).collectEntries { [(it.name): it.attribute1] }
    Map configurationDataSort = configurationData.sort { it.value }
    Map sapBaseSKUConfig = configurationDataSort?.collectEntries {
        [(it.key.replaceAll("\\s", "").minus("Size")): it.value]
    }
    return sapBaseSKUConfig
}

void cacheAttribute(String configTable) {
    api.local[configTable] = cacheCommonAttributes(configTable, false)
}

void cacheMetaData(String configTable) {
    String key = configTable + "_MetaData"
    api.local[key] = cacheAttributesMetaData(configTable)
}

String cacheAttributeDelimiter() {
    return api.findLookupTableValues("SKUConfig", Filter.equal("name", "Separator")).attribute1?.getAt(0)
}

Map cacheSAPAttributes(String tableName) {
    if (tableName) {
        return api.findLookupTableValues(tableName)?.findAll { it.attribute1 == "Yes" }?.collectEntries {
            [(it.name): it]
        }
    }
}