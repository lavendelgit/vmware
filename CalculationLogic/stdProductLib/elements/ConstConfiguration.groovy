import groovy.transform.Field

@Field final String PRODUCT_TABLE = "Product"
@Field final String OFFER_TABLE = "Offer"
@Field final String DATACENTER_TABLE = "DataCenter"
@Field final String BASE_TABLE = "Base"
@Field final String METRIC_TABLE = "Metric"
@Field final String TERM_TABLE = "Term"
@Field final String PAYMENTTYPE_TABLE = "PaymentType"
@Field final String VOLUMETIER_TABLE = "VolumeTier"
@Field final String SEGMENT_TABLE = "Segment"
@Field final String VERSION_TABLE = "Version"
@Field final String PROMOTION_TABLE = "Promotion"
@Field final String OS_TABLE = "OS"
@Field final String PS_TERM_TABLE = "PSTerm"
@Field final String PS_OPTION_TABLE = "PSOption"
@Field final String QUANTITY_TABLE = "Quantity"
@Field final String SUPPORTTYPE_TABLE = "SupportType"
@Field final String SUPPORTTIER_TABLE = "SupportTier"
@Field final String PURCHASINGPROGRAM_TABLE = "PurchasingProgram"
@Field final String CURRENCY_TABLE = "Currency"
@Field final String PRICELISTREGION_TABLE = "PriceListRegion"
@Field final String SUBREGION_TABLE = "SubRegion"
@Field final String HOSTING_TABLE = "Hosting"
@Field final String PROGRAM_OPTION_TABLE = "ProgramOption"
@Field final String RETENTION_PERIOD_TABLE = "RetentionPeriod"
@Field final String SAP_BASE_SKU_TABLE = "SAPBaseSKU"
@Field final String CONFIG_TAB_ATTRIBUTES_TABLE = "ConfigTabAttributes"
@Field final List DESCRIPTION_ATTRIBUTE = ["name", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6", "attribute7"]
@Field final List STD_ATTRIBUTES_LIST = ["PaymentType", "Term", "SupportTier", "SupportType", "DataCenter", "PurchasingProgram", "ProgramOption", "VolumeTier", "RetentionPeriod", "PSTerm", "PSOption"]
@Field final Map STANDARDPRODUCT_ATTRIBUTE_MAPPING = [
        "Payment Type"      : "PaymentType",
        "Term"              : "Term",
        "Support Tier"      : "SupportTier",
        "Support Type"      : "SupportType",
        "Data Center"       : "DataCenter",
        "Purchasing Program": "PurchasingProgram",
        "Program Option"    : "ProgramOption",
        "Volume Tier"       : "VolumeTier",
        "Retention Period"  : "RetentionPeriod",
        "PS Term"           : "PSTerm",
        "PS Option"         : "PSOption",
]
@Field final String BUNDLE_CALCULATION_FLAG = "Yes"

@Field final String COMMIT_PRICELIST_TABLE = "SAPCommitPriceList"
@Field final String RUN = "Run"
@Field final String KEY = "Key"
@Field final String CONTEXT = "Context"
@Field final String CONTEXT_VALUE = "Context Value"
@Field final String CONTEXT_VALUE_DOWNLOAD = "ContextValue"
@Field final String VARIANT_CONDITION = "Variant Condition"
@Field final String VARIANT_CONDITION_DOWNLOAD = "VariantCondition"
@Field final String VALID_FROM = "Valid From"
@Field final String VALID_FROM_DOWNLOAD = "ValidFrom"
@Field final String VALID_TO = "Valid To"
@Field final String VALID_TO_DOWNLOAD = "ValidTo"
@Field final String RATE = "Rate"
@Field final String CURRENCY = "Currency"
@Field final String DATE_YYYYMMDD_FORMAT = "yyyy-MM-dd"
@Field final String DATE_MMDDYYYY_FORMAT = "MM/dd/yyyy"
@Field final String DATE_DDMMYYYY_FORMAT = "dd-MM-yyyy"
@Field final String KEY1_COLUMN_NAME = "key1"
@Field final String KEY2_COLUMN_NAME = "key2"
@Field final String ATTRIBUTE1_COLUMN_NAME = "attribute1"
@Field final String ATTRIBUTE2_COLUMN_NAME = "attribute2"
@Field final String ATTRIBUTE3_COLUMN_NAME = "attribute3"
@Field final String ATTRIBUTE4_COLUMN_NAME = "attribute4"
@Field final String ATTRIBUTE5_COLUMN_NAME = "attribute5"
@Field final String ATTRIBUTE6_COLUMN_NAME = "attribute6"
@Field final String ATTRIBUTE7_COLUMN_NAME = "attribute7"
@Field final String ATTRIBUTE8_COLUMN_NAME = "attribute8"
@Field final List SAP_COMMIT_PP_TABLE_FIELDS = [KEY, CONTEXT, CONTEXT_VALUE, VARIANT_CONDITION, RATE, CURRENCY, VALID_FROM, VALID_TO, RUN]
@Field final String VALID_COMBINATION_PX_TABLE = "ValidCombination"
@Field final String PRODUCT_MODEL_PX_TABLE = "ConfigTab"
@Field final String DOWNLOAD_EXCEL = "Download Excel"
@Field final List PX_TABLE_DATE_ATTRIBUTES = ["Master End Date", "Channel Launch Date", "Master Launch Date", "Channel End Date", "Start Date", "End Date"]
@Field final Map VALID_COMBINATION_COLUMN_NAME_ORDER = [
        sku        : "Part Number",
        attribute11: "SAP Base SKU",
        attribute14: "Product",
        attribute16: "Base",
        attribute15: "Offer",
        attribute12: "Charge ID",
        attribute1 : "Concatenated Segments",
        attribute2 : "Values",
        attribute3 : "Master Launch Regions",
        attribute5 : "Master EOL Regions",
        attribute4 : "Channel Launch Regions",
        attribute6 : "Channel EOL Regions",
        attribute7 : "Master Launch Date",
        attribute9 : "Master End Date",
        attribute8 : "Channel Launch Date",
        attribute10: "Channel End Date",
        attribute13: "Pfx Region Key",
        attribute17: "Attribute Type"
]
@Field final Map PRODUCT_MODEL_COLUMN_NAME_ORDER = [
        sku        : "Part Number",
        attribute1 : "Config Attribute",
        attribute2 : "Value",
        attribute3 : "Sequence",
        attribute4 : "Type",
        attribute5 : "Charge ID",
        attribute8 : "Product",
        attribute9 : "Offer",
        attribute10: "Base",
        attribute6 : "Start Date",
        attribute7 : "End Date"
]
@Field final String COLUMN_PLATFORM_GROUP = "PlatformGroup"
@Field final String COLUMN_PRODUCT_PLATFORM = "ProductPlatform"
@Field final String COLUMN_PRODUCT_FAMILY = "ProductFamily"
@Field final String COLUMN_PART_NUMBER = "PartNumber"
@Field final String COLUMN_PRODUCT = "Product"
@Field final String COLUMN_CURRENCY = "Currency"
@Field final String COLUMN_START_DATE = "StartDate"
@Field final String COLUMN_END_DATE = "EndDate"
@Field final String PLATFORM_GROUP = "Platform Group"
@Field final String PRODUCT_PLATFORM = "Product Platform"
@Field final String PRODUCT_FAMILY = "Product Family"
@Field final String PRODUCT = "Product"
@Field final String PART_NUMBER = "Part Number"
@Field final String INPUT_START_YEAR = "Start Year"
@Field final String INPUT_END_YEAR = "End Year"
@Field final String NULL_PLACEHOLDER = null
@Field final String DB_CONFIGURATION = "DashBoard Configuration"
@Field final String MONTHLY_PRICE_PROJECTION = "Month-wise Price Book"
@Field final String PRICE_EFFECTIVE_MONTH_PROJECTION = "Price Change Month Price Book"
@Field final String ALL_PRICE_EFFECTIVE_PROJECTION = "All Month Price Book"
@Field final List CONFIGURATION_OPTIONS = [MONTHLY_PRICE_PROJECTION, ALL_PRICE_EFFECTIVE_PROJECTION, PRICE_EFFECTIVE_MONTH_PROJECTION]
@Field final List FILTER_COLUMN_OPTION = [COLUMN_PLATFORM_GROUP, COLUMN_PRODUCT_FAMILY, COLUMN_PRODUCT_PLATFORM, COLUMN_PART_NUMBER]
@Field final String PRICE_MONTH = "Price Month"
@Field final String LIST_PRICE = "List Price"
@Field final String WARNING = "Warning"
@Field final String ALL_INPUTS_WARNING_MESSAGE = "Please select all mandatory user inputs : DashBoard Configuration, Start Year, End Year and Currency"
@Field final String YEAR_WARNING_MESSAGE = "Please select valid Start Year and End Year"
@Field final String TEXT_COLOR = "#000000"
@Field final String BG_COLOR = "transparent"
@Field final String TEXT_WEIGHT = "900"
@Field final String TEXT_ALLIGN = "Left"


