import groovy.transform.Field

Map attributesExportConfigManager() {
    return [
            COMMIT_PRICELIST_TABLE    : libs.stdProductLib.ConstConfiguration.COMMIT_PRICELIST_TABLE,
            RUN                       : libs.stdProductLib.ConstConfiguration.RUN,
            KEY                       : libs.stdProductLib.ConstConfiguration.KEY,
            CONTEXT                   : libs.stdProductLib.ConstConfiguration.CONTEXT,
            CONTEXT_VALUE             : libs.stdProductLib.ConstConfiguration.CONTEXT_VALUE,
            CONTEXT_VALUE_DOWNLOAD    : libs.stdProductLib.ConstConfiguration.CONTEXT_VALUE_DOWNLOAD,
            VARIANT_CONDITION         : libs.stdProductLib.ConstConfiguration.VARIANT_CONDITION,
            VARIANT_CONDITION_DOWNLOAD: libs.stdProductLib.ConstConfiguration.VARIANT_CONDITION_DOWNLOAD,
            VALID_FROM                : libs.stdProductLib.ConstConfiguration.VALID_FROM,
            VALID_FROM_DOWNLOAD       : libs.stdProductLib.ConstConfiguration.VALID_FROM_DOWNLOAD,
            VALID_TO                  : libs.stdProductLib.ConstConfiguration.VALID_TO,
            VALID_TO_DOWNLOAD         : libs.stdProductLib.ConstConfiguration.VALID_TO_DOWNLOAD,
            RATE                      : libs.stdProductLib.ConstConfiguration.RATE,
            CURRENCY                  : libs.stdProductLib.ConstConfiguration.CURRENCY,
            DATE_YYYYMMDD_FORMAT      : libs.stdProductLib.ConstConfiguration.DATE_YYYYMMDD_FORMAT,
            DATE_MMDDYYYY_FORMAT      : libs.stdProductLib.ConstConfiguration.DATE_MMDDYYYY_FORMAT,
            DATE_DDMMYYYY_FORMAT      : libs.stdProductLib.ConstConfiguration.DATE_DDMMYYYY_FORMAT,
            KEY1_COLUMN_NAME          : libs.stdProductLib.ConstConfiguration.KEY1_COLUMN_NAME,
            KEY2_COLUMN_NAME          : libs.stdProductLib.ConstConfiguration.KEY2_COLUMN_NAME,
            ATTRIBUTE1_COLUMN_NAME    : libs.stdProductLib.ConstConfiguration.ATTRIBUTE1_COLUMN_NAME,
            ATTRIBUTE2_COLUMN_NAME    : libs.stdProductLib.ConstConfiguration.ATTRIBUTE2_COLUMN_NAME,
            ATTRIBUTE3_COLUMN_NAME    : libs.stdProductLib.ConstConfiguration.ATTRIBUTE3_COLUMN_NAME,
            ATTRIBUTE4_COLUMN_NAME    : libs.stdProductLib.ConstConfiguration.ATTRIBUTE4_COLUMN_NAME,
            ATTRIBUTE5_COLUMN_NAME    : libs.stdProductLib.ConstConfiguration.ATTRIBUTE5_COLUMN_NAME,
            ATTRIBUTE6_COLUMN_NAME    : libs.stdProductLib.ConstConfiguration.ATTRIBUTE6_COLUMN_NAME,
            ATTRIBUTE7_COLUMN_NAME    : libs.stdProductLib.ConstConfiguration.ATTRIBUTE7_COLUMN_NAME,
            ATTRIBUTE8_COLUMN_NAME    : libs.stdProductLib.ConstConfiguration.ATTRIBUTE8_COLUMN_NAME,
            VALID_COMBINATION_PX_TABLE: libs.stdProductLib.ConstConfiguration.VALID_COMBINATION_PX_TABLE,
            PRODUCT_MODEL_PX_TABLE    : libs.stdProductLib.ConstConfiguration.PRODUCT_MODEL_PX_TABLE,
            DOWNLOAD_EXCEL            : libs.stdProductLib.ConstConfiguration.DOWNLOAD_EXCEL
    ]
}