def createSKU(Map params) {
    if (!params.Product)
        return null
    def sku = params.Product
    sku = params.Offer ? sku + params.Offer : sku
    sku = params.Base ? sku + params.Base : sku
    sku += "-"
    sku = params.SupportType ? sku + params.SupportType : sku
    sku = params.SupportTier ? sku + params.SupportTier : sku
    sku = params.OS ? sku + params.OS : sku
    sku = params.DataCenter ? sku + params.DataCenter : sku
    sku = params.Hosting ? sku + params.Hosting : sku
    sku = params.Metric ? sku + params.Metric : sku
    sku = params.Quantity ? sku + params.Quantity : sku
    sku = params.Version ? sku + params.Version : sku
    sku += "-"
    sku = params.Term ? sku + params.Term : sku
    sku = params.PaymentType ? sku + params.PaymentType : sku
    sku = params.RetentionPeriod ? sku + params.RetentionPeriod : sku
    sku = params.VolumeTier ? sku + params.VolumeTier : sku
    sku = params.PurchasingProgram ? sku + params.PurchasingProgram : sku
    sku = params.ProgramOption ? sku + params.ProgramOption : sku
    sku = params.PSTerm ? sku + params.PSTerm : sku
    sku = params.PSOption ? sku + params.PSOption : sku
    sku = params.Segment ? sku + params.Segment : sku
    sku = params.Promotion ? sku + params.Promotion : sku

    return sku
}


def createDesc(ruleList, Map params) {
    String description
    String configDesc
    String key
    String tableName

    ruleList.each { rule ->
        tableName = rule.attribute3?.rule.attribute3.replaceAll("\\s", "").minus("Size")
        switch (rule.attribute1) {
            case "Configuration":
                key = params[tableName + "Key"]
                name = tableName + "_" + "MetaData"
                attribute = api.local[name].find { it.value == rule.attribute4 }?.key
                configDesc = api.local[tableName]?.getAt(key)?.getAt(attribute)

                if (configDesc) {
                    if (description) {
                        description += configDesc
                    } else {
                        description = configDesc
                    }
                }
                break
            case "Plain Text":
                if (description) {
                    description += rule.attribute2
                } else {
                    description = rule.attribute2
                }
                break
            case "Conditional Text":
                attributeDesc = frameConditionalDesc(rule.attribute6, rule.attribute7, rule.attribute2, params?.getAt(rule.attribute5 + "Key"))
                if (attributeDesc) {
                    description = description ? (description + attributeDesc) : description
                }
                break
            case "Conditional Table":
                key = params[tableName + "Key"]
                name = tableName + "_" + "MetaData"
                attribute = api.local[name].find { it.value == rule.attribute4 }?.key
                configDesc = api.local[tableName]?.getAt(key)?.getAt(attribute)
                isValidDesc = isValidConditionalTableDesc(rule.attribute6, rule.attribute7, params?.getAt(rule.attribute5 + "Key"))
                if (configDesc && isValidDesc) {
                    if (description) {
                        description += configDesc
                    } else {
                        description = configDesc
                    }
                }
                break
        }
    }
    return description
}

boolean isValidConditionalTableDesc(String operator, String configValue, String paramValue) {
    boolean isValidDesc = false
    switch (operator) {
        case "=":
            if (paramValue.equals(configValue)) {
                isValidDesc = true
            }
            break
        case "<=":
            if (paramValue <= configValue) {
                isValidDesc = true
            }
            break
        case ">=":
            if (paramValue >= configValue) {
                isValidDesc = true
            }
            break
        case "<":
            if (paramValue < configValue) {
                isValidDesc = true
            }
            break
        case ">":
            if (conditionName > configValue) {
                isValidDesc = true
            }
            break
        case "<>":
            if (conditionName != configValue) {
                isValidDesc = true
            }
            break

    }
    return isValidDesc
}

String frameConditionalDesc(String operator, String configValue, String desc, String paramValue) {
    String attributeDesc
    switch (operator) {
        case "=":
            if (paramValue.equals(configValue)) {
                attributeDesc = desc
            }
            break
        case "<=":
            if (paramValue <= configValue) {
                attributeDesc = desc
            }
            break
        case ">=":
            if (paramValue >= configValue) {
                attributeDesc = desc
            }
            break
        case "<":
            if (paramValue < configValue) {
                attributeDesc = desc
            }
            break
        case ">":
            if (conditionName > configValue) {
                attributeDesc = desc
            }
            break
        case "<>":
            if (conditionName != configValue) {
                attributeDesc = desc
            }
            break

    }
    return attributeDesc ?: null
}

def insertRecords(String tableName, Map params, String skuType = "Standalone") {
    return [
            tableName,
            params.sku,
            params.ProductKey,
            params.OfferKey,
            params.DataCenterKey,
            params.BaseKey,
            params.MetricKey,
            params.TermKey,
            params.PaymentTypeKey,
            params.VolumeTierKey,
            params.SegmentKey,
            params.VersionKey,
            params.PromotionKey,
            params.QuantityKey,
            params.SupportTierKey,
            params.SupportTypeKey,
            params.PurchasingProgramKey,
            params.TermUoM,
            params.OSKey,
            skuType,
            params.HostingKey,
            params.ProgramOptionKey,
            params.SAPBaseSKU,
            params.RetentionPeriodKey,
            params.RetentionPeriodUoM,
            params.PSTermKey,
            params.PSOptionKey,
    ]
}

String generateSAPBaseSKU(Map validSAPBaseSKUCombination, Map pxParam) {
    String sku = ""
    for (attribute in validSAPBaseSKUCombination) {
        String attributeName = attribute.key.replaceAll("\\s", "").minus("Size")
        key = pxParam[attributeName] ? pxParam[attributeName] : ""
        if (["Product", "Offer", "Base"].contains(attributeName)) {
            sku = sku ? sku + key : key
        } else {
            sku = sku ? sku + "_" + key : "_" + key
        }
    }
    return sku
}

Map cacheValidRules(List ruleRecords, Map productRuleMetaData, Map ruleKeyIndex, boolean cachePartNumber = false, String separator = ".") {
    Map rulesCache = [:]
    Map regionCache = [:]
    List keys
    if (cachePartNumber) {
        excludeList = ["version", "typedId", "sku", "name", "createDate", "createdBy", "lastUpdateDate", "lastUpdateBy", "attribute15", "attribute17", "attribute26", "attribute27", "attribute28", "attribute29", "attribute24", "attribute14", "id"]
    } else {
        excludeList = ["attribute17", "attribute18", "attribute19", "attribute20"]
    }
    ruleRecords?.each { rule ->
        keys = rule.keySet().collect()
        keysList = keys.minus(excludeList)
        keyNames = []
        keyIndex = []
        keysList.each {
            key ->
                if (rule[key] != "*") {
                    keyAttribute = productRuleMetaData[key]
                    keyForIndex = keyAttribute?.replaceAll("\\s", "")
                    keyIndex << ruleKeyIndex[keyForIndex]
                    keyNames << productRuleMetaData[key]
                }
        }
        keyValueList = []
        Map record = [:]
        keysList?.each {
            key ->
                if (rule[key] != "*") {
                    keyValueList << rule[key]
                    record[productRuleMetaData[key]] = rule[key]
                }

        }

        key = keyValueList?.join(separator)
        record.Keys = keyNames
        record.KeyIndex = keyIndex
        if (cachePartNumber) {
            record.StandardProductSKU = rule.sku
        } else {
            currencyRegioncache = regionCache[key] ?: []
            currencyRegioncache << [
                    "Currency"       : rule.attribute18,
                    "PriceListRegion": rule.attribute19
            ]
            regionCache[key] = currencyRegioncache
            record.CurrencyRegion = regionCache[key]
        }
        rulesCache[key] = record
    }

    return rulesCache
}


Map populateRuleKeyIndexMapping() {
    ruleKeyIndex = [
            "Product"           : 0,
            "Offer"             : 1,
            "DataCenter"        : 2,
            "Base"              : 3,
            "Metric"            : 4,
            "Term"              : 5,
            "TermUoM"           : 6,
            "PaymentType"       : 7,
            "VolumeTier"        : 8,
            "Segment"           : 9,
            "Version"           : 10,
            "Promotion"         : 11,
            "OS"                : 12,
            "Quantity"          : 13,
            "PurchasingProgram" : 14,
            "SupportType"       : 15,
            "SupportTier"       : 16,
            "Currency"          : 17,
            "PriceListRegion"   : 18,
            "SubRegion"         : 19,
            "Hosting"           : 20,
            "ProgramOption"     : 21,
            "RetentionPeriod"   : 22,
            "RetentionPeriodUOM": 23

    ]
    return ruleKeyIndex
}

boolean validAttribute(List excludeAttributes, String attribute) {
    if (attribute) {
        return excludeAttributes?.contains(attribute)
    }
}

List cacheExcludeAttributes() {
    return api.findLookupTableValues("SKUConfig", Filter.equal("name", "Exclude Attributes")).getAt(0).attribute1?.split(',')*.trim()
}
