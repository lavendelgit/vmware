def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("NyansaLongDesc").id),
        // Filter.equal("attribute7","Yes")
]

api.local.LongDesc = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3", "attribute4"], *filter)

api.trace("api.local.LongDesc", api.local.LongDesc)
return