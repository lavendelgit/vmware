def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BasePrice").id),
        Filter.equal("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute7", "yes")
]
Map baseCache = api.find("MLTV3", 0, api.getMaxFindResultsLimit(), "key2",
        ["key2", "key3", "attribute2", "attribute4", "attribute5", "attribute6"], *filter)?.collectEntries {
    record ->
        [
                (record.key2 + "_" + record.key3): [
                        "Key"    : record.key2 + "_" + record.key3,
                        "Offer"  : record.key2,
                        "Base"   : record.key3,
                        "Type"   : record.attribute4,
                        "BoxSize": record.attribute2
                ]
        ]
}
return baseCache
