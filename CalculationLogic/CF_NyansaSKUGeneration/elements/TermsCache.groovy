def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("Terms").id),
        Filter.equal("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute8", "yes")
]
Map termsCache = api.find("MLTV5", 0, api.getMaxFindResultsLimit(), "key4",
        ["key2", "key4", "key5", "attribute3", "attribute4", "attribute5", "attribute6", "attribute7"], *filter)?.collectEntries {
    record ->
        [
                (record.key2 + "_" + record.key4 + "_" + record.key5): [
                        "Offer"       : record.key2,
                        "Key"         : record.key2 + "_" + record.key4 + "_" + record.key5,
                        "Frequency"   : record.key4,
                        "Years"       : record.key5 as BigDecimal,
                        "PaymentCycle": record.attribute4,
                        "Label"       : record.attribute5,
                        "ShortDesc"   : record.attribute6,
                        "LongDesc"    : record.attribute7
                ]
        ]
}
return termsCache

