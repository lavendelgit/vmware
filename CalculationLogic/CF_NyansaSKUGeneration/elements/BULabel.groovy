return api.findLookupTableValues("BULabels", Filter.equal("key1", Constants.STR_PRODUCT))?.collectEntries {
    [(it.key2): it.attribute1]
}