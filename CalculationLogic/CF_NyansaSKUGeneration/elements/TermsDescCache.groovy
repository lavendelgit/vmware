def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("Terms").id),
        Filter.equal("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute8", "yes")
]
api.local["Terms_MetaData"] = api.find("MLTVM", Filter.equal("lookupTableId", api.findLookupTable("Terms").id))?.collectEntries {
    [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
}
api.local["Terms"] = api.findLookupTableValues("Terms", *filter)?.collectEntries {
    [(it.key2 + "_" + it.key4 + "_" + it.key5): it]
}
return

