def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("HardwareReplacementServiceFactor").id),
        Filter.equal("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute5", "yes")
]
Map replacementServiceCache = api.find("MLTV5", 0, api.getMaxFindResultsLimit(), "key2",
        ["key2", "key4", "attribute2", "attribute3", "attribute4"], *filter)?.collectEntries {
    record ->
        [
                (record.key2 + "_" + record.key4): [
                        "Key"               : record.key2 + "_" + record.key4,
                        "Offer"             : record.key2,
                        "ReplacementService": record.key4,
                        "Label"             : record.attribute2,
                        "ShortDesc"         : record.attribute3,
                        "LongDesc"          : record.attribute4
                ]
        ]
}
return replacementServiceCache
