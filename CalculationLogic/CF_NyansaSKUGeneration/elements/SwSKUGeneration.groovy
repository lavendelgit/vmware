def baseCache = out.BaseCache
def gpOfferCache = out.BaseCache.groupBy { it.value.Offer }
def shortDescCache = api.local.ShortDesc.groupBy { it.key1 }
api.local["SwShortDesc"] = shortDescCache["Software"]
api.local["HwShortDesc"] = shortDescCache["Hardware Capex"]
api.local["ExtShortDesc"] = shortDescCache["Extended Replacement Service"]

api.local["Software"] = gpOfferCache["Software"]
api.local["Hardware Capex"] = gpOfferCache["Hardware Capex"]
api.local["Extended Replacement Service"] = gpOfferCache["Extended Replacement Service"]

def longDescCache = api.local.LongDesc.groupBy { it.key1 }
api.local["SwLongDesc"] = longDescCache["Software"]
api.local["HwLongDesc"] = longDescCache["Hardware Capex"]
api.local["ExtLongDesc"] = longDescCache["Extended Replacement Service"]


def softwareOfferCache = api.local["Software"]
def tierCache = out.TierCache
def supportCache = out.SupportOptionCache
def serviceCache = out.ServiceLevelCache
def termsCache = out.TermsCache?.groupBy { it.value.Offer }?.getAt("Software")
def segmentCache = out.SegmentCache
def buLabel = out.BULabel["Software"]
def longDesc
def shortDesc
Map params = [:]
Map pXParams = [:]
softwareOfferCache.each {
    offerBase ->
        tierCache.each {
            tier ->
                supportCache.each {
                    support ->
                        serviceCache.each {
                            service ->
                                termsCache.each {
                                    term ->
                                        segmentCache.each {
                                            segment ->
                                                params = [
                                                        "BULabel"               : buLabel,
                                                        "Offer"                 : offerBase.value.Offer,
                                                        "Base"                  : offerBase.value.Base,
                                                        "TierName"              : tier.value.Name,
                                                        "TierLabel"             : tier.value.Label,
                                                        "Support"               : support.value.Label,
                                                        "Service"               : service.value.Label,
                                                        "TermMonths"            : (term.value.Years * 12) as String,
                                                        "TermFreq"              : term.value.Label,
                                                        "Segment"               : segment.value.Label,
                                                        "BasePriceKey"          : offerBase.value.Key,
                                                        "VolumeTierKey"         : tier.value.Key,
                                                        "SupportOptionKey"      : support.value.Key,
                                                        "SegmentAdjustmentKey"  : segment.value.Key,
                                                        "ServiceLevelFactorsKey": service.value.Key,
                                                        "TermsKey"              : term.value.Key,
                                                ]
                                                def sku = Library.createSKU(offerBase.value.Offer, params)
                                                shortDesc = Library.createShortDesc(api.local["SwShortDesc"], params)
                                                longDesc = Library.createLongDesc(api.local["SwLongDesc"], params)

                                                if (sku) {
                                                    pXParams = [
                                                            "sku"       : sku,
                                                            "Offer"     : offerBase.value.Offer,
                                                            "Base"      : offerBase.value.Base,
                                                            "TierName"  : tier.value.Name,
                                                            "TierLabel" : tier.value.Label,
                                                            "Support"   : support.value.Key,
                                                            "Service"   : service.value.Key,
                                                            "Segment"   : segment.value.Key,
                                                            "TermFreq"  : term.value.Label,
                                                            "TermMonths": (term.value.Years * 12) as String,
                                                            "ShortDesc" : shortDesc,
                                                            "LongDesc"  : longDesc

                                                    ]
                                                    Library.insertRecords(pXParams)
                                                }
                                        }
                                }
                        }

                }

        }


}