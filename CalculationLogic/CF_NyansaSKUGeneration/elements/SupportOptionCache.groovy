def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("SupportOption").id),
        Filter.equal("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute5", "yes")
]
Map supportOptionCache = api.find("MLTV4", 0, api.getMaxFindResultsLimit(), "key4",
        ["key4", "attribute1", "attribute3", "attribute4"], *filter)?.collectEntries {
    record ->
        [
                (record.key4): [
                        "Key"  : record.key4,
                        "Label": record.attribute1
                ]
        ]
}
return supportOptionCache

