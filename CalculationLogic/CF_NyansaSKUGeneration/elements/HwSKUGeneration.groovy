def capexOfferCache = api.local["Hardware Capex"]
def replacementServiceCache = out.ReplacementServiceCache?.groupBy { it.value.Offer }?.getAt("Hardware Capex")
def segmentCache = out.SegmentCache
def buLabel = out.BULabel["Hardware Capex"]
Map params = [:]
def shortDesc
def longDesc
def sku
capexOfferCache.each {
    offerBase ->
        replacementServiceCache.each {
            replacementService ->
                segmentCache.each {
                    segment ->
                        params = [
                                "BULabel"                            : buLabel,
                                "Offer"                              : offerBase.value.Offer,
                                "Base"                               : offerBase.value.Base,
                                "Type"                               : offerBase.value.Type,
                                "ReplacementService"                 : replacementService.value.Label,
                                "Segment"                            : segment.value.Label,
                                "BasePriceKey"                       : offerBase.value.Key,
                                "SegmentAdjustmentKey"               : segment.value.Key,
                                "HardwareReplacementServiceFactorKey": replacementService.value.Key,

                        ]
                        sku = Library.createSKU("Hardware Capex", params)
                        shortDesc = Library.createShortDesc(api.local["HwShortDesc"], params)
                        longDesc = Library.createLongDesc(api.local["HwLongDesc"], params)
                        if (sku) {
                            pXParams = [
                                    "sku"               : sku,
                                    "Offer"             : offerBase.value.Offer,
                                    "Base"              : offerBase.value.Base,
                                    "Type"              : offerBase.value.Type,
                                    "Segment"           : segment.value.Key,
                                    "ReplacementService": replacementService.value.ReplacementService,
                                    "BoxSize"           : offerBase.value.BoxSize,
                                    "ShortDesc"         : shortDesc,
                                    "LongDesc"          : longDesc
                            ]
                            Library.insertRecords(pXParams)
                        }

                }
        }
}