def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BasePrice").id),
        Filter.in("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute7", "yes")
]

def metafilter = [
        Filter.equal("lookupTableId", api.findLookupTable("BasePrice").id)
]

api.local["BasePrice_MetaData"] = api.find("MLTVM", *metafilter)?.collectEntries {
    [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
}
api.local["BasePrice"] = api.findLookupTableValues("BasePrice", *filter)?.collectEntries {
    [(it.key2 + "_" + it.key3): it]
}
return
