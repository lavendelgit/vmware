def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("NyansaShortDesc").id),
        // Filter.equal("attribute7","Yes")
]

api.local.ShortDesc = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3", "attribute4"], *filter)

api.trace("api.local.ShortDesc", api.local.ShortDesc)
return