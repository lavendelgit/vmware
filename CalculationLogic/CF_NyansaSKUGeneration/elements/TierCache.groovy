def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VolumeTierDetails").id),
        Filter.equal("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute8", "yes")
]
Map tierCache = api.find("MLTV6", 0, api.getMaxFindResultsLimit(), "attribute4",
        ["attribute4", "attribute5", "attribute6", "attribute7"], *filter)?.collectEntries {
    record ->
        [
                (record.attribute4): [
                        "Key"  : record.attribute4,
                        "Name" : record.attribute4,
                        "Label": record.attribute5
                ]
        ]
}
return tierCache
