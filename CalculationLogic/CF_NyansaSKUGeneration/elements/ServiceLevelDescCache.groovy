def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("ServiceLevelFactors").id),
        Filter.equal("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute5", "yes")
]
api.local["ServiceLevelFactors_MetaData"] = api.find("MLTVM", Filter.equal("lookupTableId", api.findLookupTable("ServiceLevelFactors").id))?.collectEntries {
    [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
}

api.local["ServiceLevelFactors"] = api.findLookupTableValues("ServiceLevelFactors", *filter)?.collectEntries {
    [(it.key4): it]
}
return
