def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("SegmentAdjustment").id),
        Filter.equal("key2", Constants.STR_PRODUCT),
        Filter.equal("attribute5", "yes")
]
Map segmentCache = api.find("MLTV4", 0, api.getMaxFindResultsLimit(), "key4",
        ["key1", "attribute2", "attribute3", "attribute4"], *filter)?.collectEntries {
    record ->
        [
                (record.key1): [
                        "Key"  : record.key1,
                        "Label": record.attribute2
                ]
        ]
}
return segmentCache

