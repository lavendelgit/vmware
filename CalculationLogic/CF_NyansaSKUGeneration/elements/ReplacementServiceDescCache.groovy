def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("HardwareReplacementServiceFactor").id),
        Filter.equal("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute5", "yes")
]
api.local["HardwareReplacementServiceFactor_MetaData"] = api.find("MLTVM", Filter.equal("lookupTableId", api.findLookupTable("HardwareReplacementServiceFactor").id))?.collectEntries {
    [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
}

api.local["HardwareReplacementServiceFactor"] = api.findLookupTableValues("HardwareReplacementServiceFactor", *filter)?.collectEntries {
    [(it.key2 + "_" + it.key4): it]
}
return


