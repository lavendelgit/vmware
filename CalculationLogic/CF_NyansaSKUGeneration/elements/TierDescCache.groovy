def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("VolumeTierDetails").id),
        Filter.equal("attribute8", "yes")
]
api.local["VolumeTier_MetaData"] = api.find("MLTVM", Filter.equal("lookupTableId", api.findLookupTable("VolumeTierDetails").id))?.collectEntries {
    [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
}
api.local["VolumeTier"] = api.findLookupTableValues("VolumeTierDetails", *filter)?.collectEntries { [(it.attribute4): it] }
return