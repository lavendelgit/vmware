def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("SupportOption").id),
        Filter.equal("key1", Constants.STR_PRODUCT),
        Filter.equal("attribute5", "yes")
]

api.local["SupportOption_MetaData"] = api.find("MLTVM", Filter.equal("lookupTableId", api.findLookupTable("SupportOption").id))?.collectEntries {
    [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
}

api.local["SupportOption"] = api.findLookupTableValues("SupportOption", *filter)?.collectEntries { [(it.key4): it] }
return