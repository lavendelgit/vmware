def extOfferBase = api.local["Extended Replacement Service"]

def termsCache = out.TermsCache?.groupBy { it.value.Offer }?.getAt("Extended Replacement Service")
def replacementServiceCache = out.ReplacementServiceCache?.groupBy {
    it.value.Offer
}?.getAt("Extended Replacement Service")
def segmentCache = out.SegmentCache
def buLabel = out.BULabel["Extended Replacement Service"]
Map params = [:]
extOfferBase.each {
    offerBase ->
        replacementServiceCache.each {
            replacementService ->
                termsCache.each {
                    term ->
                        segmentCache.each {
                            segment ->
                                params = [
                                        "BULabel"                            : buLabel,
                                        "Offer"                              : offerBase.value.Offer,
                                        "Base"                               : offerBase.value.Base,
                                        "TermMonths"                         : (term.value.Years * 12) as String,
                                        "TermFreq"                           : term.value.Label,
                                        "ReplacementService"                 : replacementService.value.Label,
                                        "Segment"                            : segment.value.Label,
                                        "BasePriceKey"                       : offerBase.value.Key,
                                        "SegmentAdjustmentKey"               : segment.value.Key,
                                        "TermsKey"                           : term.value.Key,
                                        "Type"                               : offerBase.value.Type,
                                        "HardwareReplacementServiceFactorKey": replacementService.value.Key,

                                ]
                                def sku = Library.createSKU(offerBase.value.Offer, params)
                                shortDesc = Library.createShortDesc(api.local["ExtShortDesc"], params)
                                longDesc = Library.createLongDesc(api.local["ExtLongDesc"], params)
                                if (sku) {
                                    pXParams = [
                                            "sku"               : sku,
                                            "Offer"             : offerBase.value.Offer,
                                            "Base"              : offerBase.value.Base,
                                            "Type"              : offerBase.value.Type,
                                            "Segment"           : segment.value.Key,
                                            "ReplacementService": replacementService.value.ReplacementService,
                                            "BoxSize"           : offerBase.value.BoxSize,
                                            "TermFreq"          : term.value.Label,
                                            "TermMonths"        : (term.value.Years * 12) as String,
                                            "ShortDesc"         : shortDesc,
                                            "LongDesc"          : longDesc
                                    ]
                                    Library.insertRecords(pXParams)
                                }

                        }
                }


        }


}