def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("SegmentAdjustment").id),
        Filter.equal("key2", Constants.STR_PRODUCT),
        Filter.equal("attribute5", "yes")
]
api.local["SegmentAdjustment_MetaData"] = api.find("MLTVM", Filter.equal("lookupTableId", api.findLookupTable("SegmentAdjustment").id))?.collectEntries {
    [(it.fieldName): it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()]
}

api.local["SegmentAdjustment"] = api.findLookupTableValues("SegmentAdjustment", *filter)?.collectEntries {
    [(it.key1): it]
}
return
