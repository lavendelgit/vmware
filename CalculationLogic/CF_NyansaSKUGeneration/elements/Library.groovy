def insertRecords(Map params) {
    def nyansaRecord = [:]
    if (params.Offer == "Software") {
        nyansaRecord = [
                name       : "NyansaProducts",
                sku        : params.sku,
                attribute1 : params.Offer,
                attribute2 : params.Base,
                attribute6 : params.Segment,
                attribute8 : params.Support,
                attribute9 : params.Service,
                attribute11: params.TermFreq,
                attribute12: params.TermMonths,
                attribute13: params.TierName,
                attribute14: params.TierLabel,
                attribute7 : params.ShortDesc,
                attribute10: params.LongDesc
        ]
    } else if (params.Offer == "Hardware Capex") {
        nyansaRecord = [
                name       : "NyansaProducts",
                sku        : params.sku,
                attribute1 : params.Offer,
                attribute2 : params.Base,
                attribute3 : params.Type,
                attribute4 : params.BoxSize,
                attribute5 : params.ReplacementService,
                attribute6 : params.Segment,
                attribute7 : params.ShortDesc,
                attribute10: params.LongDesc,
        ]
    } else if (params.Offer == "Extended Replacement Service") {
        nyansaRecord = [
                name       : "NyansaProducts",
                sku        : params.sku,
                attribute1 : params.Offer,
                attribute2 : params.Base,
                attribute3 : params.Type,
                attribute4 : params.BoxSize,
                attribute5 : params.ReplacementService,
                attribute6 : params.Segment,
                attribute11: params.TermFreq,
                attribute12: params.TermMonths,
                attribute7 : params.ShortDesc,
                attribute10: params.LongDesc,
        ]
    }
    if (nyansaRecord) api.addOrUpdate("PX20", nyansaRecord)
}

def createSKU(Map params) {
    String BU = params.BULabel
    String offer = params.Offer
    String base = params.Base
    String tierName = params.TierName
    String support = params.Support
    String service = params.Service
    String replacementService = params.ReplacementService
    String segment = params.Segment
    String termMonths = params.TermMonths
    String termFreq = params.TermFreq
    String sku = BU
    baseLabel = offer == "Extended Replacement Service" ? base + "-EXT" : base
    if (offer == "Software") {
        sku += "-" + tierName + "-" + support + service + "-" + termMonths + termFreq + "-" + segment
    } else if (offer == "Hardware Capex") {
        sku += "-" + baseLabel + "-" + replacementService + "-" + segment
    } else if (offer == "Extended Replacement Service") {
        sku += "-" + baseLabel + "-" + replacementService + "-" + termMonths + "P" + "-" + segment
    }
    return sku
}

def createSKU(String offer, Map params) {
    String BU = params.BULabel
    // String offer = params.Offer
    String base = params.Base
    String tierName = params.TierName
    String support = params.Support
    String service = params.Service
    String replacementService = params.ReplacementService
    String segment = params.Segment
    String termMonths = params.TermMonths
    String termFreq = params.TermFreq
    String type = params.Type
    String sku = BU
    baseLabel = offer == "Extended Replacement Service" ? base + "-EXT" : base
    if (offer == "Software") {
        sku += "-" + tierName + "-" + support + service + "-" + termMonths + termFreq + "-" + segment
    } else if (offer == "Hardware Capex") {
        sku += "-" + type + baseLabel + "-" + replacementService + "-P-" + segment
    } else if (offer == "Extended Replacement Service") {
        sku += "-" + type + baseLabel + "-" + replacementService + "-" + termMonths + "P" + "-" + segment
    }
    return sku
}


def createShortDesc(ruleList, Map params) {
    String shortDesc
    String desc
    String key

    ruleList.each { rule ->
        switch (rule.attribute1) {
            case "Configuration":
                key = params[rule.attribute3 + "Key"]
                name = rule.attribute3 + "_" + "MetaData"
                attribute = api.local[name].find { it.value == rule.attribute4 }?.key
                desc = api.local[rule.attribute3]?.getAt(key)?.getAt(attribute)
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
                break
            case "Plain Text":
                if (shortDesc) {
                    shortDesc += rule.attribute2
                } else {
                    shortDesc = rule.attribute2
                }
                break
        }
    }
    return shortDesc
}


def createLongDesc(ruleList, Map params) {
    String longDesc
    String desc
    String key

    ruleList.each { rule ->
        switch (rule.attribute1) {
            case "Configuration":
                key = params[rule.attribute3 + "Key"]
                name = rule.attribute3 + "_" + "MetaData"
                attribute = api.local[name].find { it.value == rule.attribute4 }?.key
                desc = api.local[rule.attribute3]?.getAt(key)?.getAt(attribute)
                if (longDesc) {
                    longDesc += desc
                } else {
                    longDesc = desc
                }
                break
            case "Plain Text":
                if (longDesc) {
                    longDesc += rule.attribute2
                } else {
                    longDesc = rule.attribute2
                }
                break
        }
    }
    return longDesc
}
