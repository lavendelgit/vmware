import groovy.transform.Field

@Field final String PRICE_LIST_REGION = "Price List Region"
@Field final String PFX_SAP_REGIONAL_MAPPING_PP = "PfxSAPRegionMappingInternal"