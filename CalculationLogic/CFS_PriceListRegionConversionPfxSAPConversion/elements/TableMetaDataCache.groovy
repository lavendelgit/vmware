String tableName

if (out.TableName) {
    tableName = out.TableName
}

if (!api.global.priceListRegionAtt && tableName) {
    cacheMetaData = libs.stdProductLib.RulesGenHelper.cachePXMetaData(tableName)?.collectEntries { [(it.value): it.key] }
    api.global.priceListRegionAtt = cacheMetaData[Constants.PRICE_LIST_REGION]
}

return


