def table = api.findLookupTable("VeloHardwareSKUs")
int start = 0
def recordList = []
def fieldValueList = []
int maxResults = api.getMaxFindResultsLimit()
while (records = api.find("MLTV", start, maxResults, "attribute5", ["attribute5"], true, Filter.equal("lookupTable.id", table.id))) {
    start += records.size()
    recordList.addAll(records)
}
if (recordList != null) {
    for (record in recordList) {
        fieldValueList.add(record.attribute5?.toString())
    }
}
//return fieldValueList
api.trace(fieldValueList)
return api.options("Replacement Service", fieldValueList)

/*def replacementService = libs.vmwareUtil.LoadPPData.getLTVAsList("ReplacementService", "name")
if(replacementService){
  return api.options("Replacement Service", replacementService)
}*/