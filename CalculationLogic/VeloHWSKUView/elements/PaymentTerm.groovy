def table = api.findLookupTable("VeloHardwareSKUs")
int start = 0
def recordList = []
def fieldValueList = []
int maxResults = api.getMaxFindResultsLimit()
while (records = api.find("MLTV", start, maxResults, "attribute3", ["attribute3"], true, Filter.equal("lookupTable.id", table.id))) {
    start += records.size()
    recordList.addAll(records)
}
if (recordList != null) {
    for (record in recordList) {
        fieldValueList.add(record.attribute3)
    }
}
//return fieldValueList
return api.options("Payment Term", fieldValueList)

/*def paymentTerm= libs.vmwareUtil.LoadPPData.getLTVAsList("PaymentTerm", "name")
if(paymentTerm){
  return api.options("Payment Term", paymentTerm)
}*/