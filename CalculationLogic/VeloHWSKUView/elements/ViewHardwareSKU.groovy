if (api.isSyntaxCheck())
    return
def resultMatrix = api.newMatrix("Name", "Short Description", "Long Description", "Offering", "Edge Model",
        "Payment Term", "Payment Type", "Replacement Service", "License Class",
        "Location", "Software")

def edgeModel = api.getElement("EdgeModel")
def licenseClassList = api.getElement("LicenseClass")
def offering = api.getElement("Offering")
def paymentTerm = api.getElement("PaymentTerm")
def paymentType = api.getElement("PaymentType")
def replacementService = api.getElement("ReplacementService")

def ppTable = api.findLookupTable("VeloHardwareSKUs")
def startRow = 0
def maxRow = api.getMaxFindResultsLimit()
def recFullList = []
def recList

if (edgeModel == []) edgeModel = null
if (licenseClassList == []) licenseClassList = null
if (offering == []) offering = null
if (replacementService == []) replacementService = null
if (paymentTerm == []) paymentTerm = null
if (paymentType) paymentType = null

while (recList = api.find("MLTV", startRow, maxRow, null,
        Filter.equal("lookupTable.id", ppTable?.id),
        Filter.in("attribute2", edgeModel),
        Filter.in("attribute6", licenseClassList),
        Filter.in("attribute5", replacementService),
        Filter.in("attribute10", offering),
        Filter.in("attribute3", paymentTerm),
        Filter.in("attribute4", paymentType)
)) {
    startRow += recList.size()
    recFullList.addAll(recList)
    api.trace("startRow", null, startRow)
}
api.trace("list", null, recFullList)
if (recFullList &&
        (licenseClassList != null ||
                paymentTerm != null ||
                paymentType != null ||
                edgeModel != null ||
                replacementService != null ||
                offering != null)) {
    recFullList.each {
        def row = [:]
        row.put("Name", it.name)
        row.put("Short Description", it.attribute1)
        row.put("Long Description", it.attribute11)
        row.put("Offering", it.attribute10)
        row.put("Edge Model", it.attribute2)
        row.put("Payment Term", it.attribute3)
        row.put("Payment Type", it.attribute4)
        row.put("Replacement Service", it.attribute5)
        row.put("License Class", it.attribute6)
        row.put("Location", it.attribute7)
        row.put("Service", it.attribute8)
        resultMatrix.addRow(row)
    }
}
resultMatrix?.setEnableClientFilter(true)
return resultMatrix