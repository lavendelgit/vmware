def table = api.findLookupTable("VeloHardwareSKUs")
int start = 0
def recordList = []
def fieldValueList = []
int maxResults = api.getMaxFindResultsLimit()
while (records = api.find("MLTV", start, maxResults, "attribute4", ["attribute4"], true, Filter.equal("lookupTable.id", table.id))) {
    start += records.size()
    recordList.addAll(records)
}
if (recordList != null) {
    for (record in recordList) {
        fieldValueList.add(record.attribute4?.toString())
    }
}
//return fieldValueList
return api.options("Payment Type", fieldValueList)

/*
def paymentType= libs.vmwareUtil.LoadPPData.getLTVAsList("PaymentType", "name")
if(paymentType){
  return api.options("Payment Type", paymentType)
}*/