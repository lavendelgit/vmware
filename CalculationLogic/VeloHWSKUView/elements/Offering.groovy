def table = api.findLookupTable("VeloHardwareSKUs")
int start = 0
def recordList = []
def fieldValueList = []
int maxResults = api.getMaxFindResultsLimit()
while (records = api.find("MLTV", start, maxResults, "attribute10", ["attribute10"], true, Filter.equal("lookupTable.id", table.id))) {
    start += records.size()
    recordList.addAll(records)
}
if (recordList != null) {
    for (record in recordList) {
        fieldValueList.add(record.attribute10?.toString())
    }
}
//return fieldValueList
return api.options("Offering", fieldValueList)

/*def offering= libs.vmwareUtil.LoadPPData.getLTVAsList("Offering", "name")
if(offering){
  return api.options("Offering", offering)
}*/