String inputDate
String formatedDate
String outputData
Map pxTableMetaData
List pxTablecolumNames

def exportConfigLib = libs.stdProductLib.ConstConfiguration
Map exportConfiguration = out.ExportConfiguration

ResultMatrix resultMatrix
String pxTableName = out.table
products = out.ProductCache

if (pxTableName == exportConfiguration.VALID_COMBINATION_PX_TABLE) {
    pxTableMetaData = exportConfigLib.VALID_COMBINATION_COLUMN_NAME_ORDER
}

if (pxTableName == exportConfiguration.PRODUCT_MODEL_PX_TABLE) {
    pxTableMetaData = exportConfigLib.PRODUCT_MODEL_COLUMN_NAME_ORDER
}

if (pxTableMetaData) {
    pxTablecolumNames = pxTableMetaData?.collect { it.value } as List
    resultMatrix = api.newMatrix(pxTablecolumNames)
    products?.each { record ->
        Map row = [:]
        pxTableMetaData?.each { key, value ->
            outputData = record[key]
            if (outputData != null && exportConfigLib.PX_TABLE_DATE_ATTRIBUTES.contains(value)) {
                inputDate = outputData?.toString()
                formatedDate = inputDate ? Date.parse(exportConfiguration.DATE_YYYYMMDD_FORMAT, inputDate).format(exportConfiguration.DATE_DDMMYYYY_FORMAT) : ""
                outputData = formatedDate
            }
            row << [(value): outputData ? outputData : null]
        }
        resultMatrix.addRow(row)
    }
}
return resultMatrix
