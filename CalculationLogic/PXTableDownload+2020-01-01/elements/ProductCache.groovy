String pxTableName
if (out.table != null) {
    pxTableName = out.table
}

productGroup = api.input(Constants.FILTER)

filter = [
        Filter.equal("name", pxTableName)
]

if (productGroup) {
    if (productGroup.productFilterCriteria) {
        productFilter = api.filterFromMap(productGroup.productFilterCriteria)
    } else {
        productFilter = Filter.equal(productGroup.productFieldName, productGroup.productFieldValue)
    }
    Map propertyMappings = ["ConfigTab__attribute1":Constants.ATTRIBUTE1,"ConfigTab__attribute2":Constants.ATTRIBUTE2,"ConfigTab__attribute3":Constants.ATTRIBUTE3,"ConfigTab__attribute4":Constants.ATTRIBUTE4,"ConfigTab__attribute5":Constants.ATTRIBUTE5,"ConfigTab__attribute6":Constants.ATTRIBUTE6,"ConfigTab__attribute7":Constants.ATTRIBUTE7,"ConfigTab__attribute8":Constants.ATTRIBUTE8,"ConfigTab__attribute9":Constants.ATTRIBUTE9,"ConfigTab__attribute10":Constants.ATTRIBUTE10,"ValidCombination__attribute1":Constants.ATTRIBUTE1,"ValidCombination__attribute2":Constants.ATTRIBUTE2,"ValidCombination__attribute3":Constants.ATTRIBUTE3,"ValidCombination__attribute4":Constants.ATTRIBUTE4,"ValidCombination__attribute5":Constants.ATTRIBUTE5,"ValidCombination__attribute6":Constants.ATTRIBUTE6,"ValidCombination__attribute7":Constants.ATTRIBUTE7,"ValidCombination__attribute8":Constants.ATTRIBUTE8,"ValidCombination__attribute9":Constants.ATTRIBUTE9,"ValidCombination__attribute10":Constants.ATTRIBUTE10,"ValidCombination__attribute11":Constants.ATTRIBUTE11,"ValidCombination__attribute12":Constants.ATTRIBUTE12,"ValidCombination__attribute13":Constants.ATTRIBUTE13,"ValidCombination__attribute14":Constants.ATTRIBUTE14,"ValidCombination__attribute15":Constants.ATTRIBUTE15,"ValidCombination__attribute16":Constants.ATTRIBUTE16,"ValidCombination__attribute17":Constants.ATTRIBUTE17]

    api.walkFilter(productFilter, null, { filter ->
        if (filter != null && propertyMappings[filter.property] != null) {
            filter.property = propertyMappings[filter.property]
        }
        filter
    }, false)

    filter = [
            Filter.equal("name", pxTableName),
            productFilter

    ]
}
pxTableStream = api.stream("PX", "sku", *filter)
products = pxTableStream?.collect { row -> row }
pxTableStream?.close()
return products
 



