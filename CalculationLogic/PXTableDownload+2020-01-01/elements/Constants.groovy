import groovy.transform.Field

@Field final String TABLE = "table"
@Field final String FILTER = "filter"
@Field final String ATTRIBUTE1 = "attribute1"
@Field final String ATTRIBUTE2 = "attribute2"
@Field final String ATTRIBUTE3 = "attribute3"
@Field final String ATTRIBUTE4 = "attribute4"
@Field final String ATTRIBUTE5 = "attribute5"
@Field final String ATTRIBUTE6 = "attribute6"
@Field final String ATTRIBUTE7 = "attribute7"
@Field final String ATTRIBUTE8 = "attribute8"
@Field final String ATTRIBUTE9 = "attribute9"
@Field final String ATTRIBUTE10 = "attribute10"
@Field final String ATTRIBUTE11 = "attribute11"
@Field final String ATTRIBUTE12 = "attribute12"
@Field final String ATTRIBUTE13 = "attribute13"
@Field final String ATTRIBUTE14 = "attribute14"
@Field final String ATTRIBUTE15 = "attribute15"
@Field final String ATTRIBUTE16 = "attribute16"
@Field final String ATTRIBUTE17 = "attribute17"