def controller = api.newController()
String url
String fileName = api.input(Constants.FILE_NAME)

fileName = fileName ?: Constants.DEFAULT_FILE_NAME

Map payload = [
        Key             : out.Key,
        Run             : out.Run,
        Context         : out.Context,
        ContextValue    : out.ContextValue,
        VariantCondition: out.VariantCondition,
        ValidFrom       : out.ValidFrom,
        ValidTo         : out.ValidTo
]

Map selectPayload = payload.findAll { it.value != null }

if (selectPayload) {
    url = "/formulamanager.executeformula/SAPCommitFileDownload?output=xls&&fileName=" + fileName
} else {
    ResultMatrix resultMatrix = api.newMatrix(Constants.FIELD_NAME, Constants.WARNING)
    resultMatrix.addRow([(Constants.FIELD_NAME): Constants.INPUT_FIELD, (Constants.WARNING): Constants.WARNING_MSG])
    return resultMatrix
}
controller.addDownloadButton(out.ExportConfiguration.DOWNLOAD_EXCEL, url, api.jsonEncode(payload))
return controller