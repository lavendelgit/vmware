runs = out.SAPCommitFileCache?.collect { it[out.ExportConfiguration.ATTRIBUTE8_COLUMN_NAME] }?.unique()
runFilter = api.options(out.ExportConfiguration.RUN, runs as List)
return runFilter