import groovy.transform.Field

@Field final String DEFAULT_FILE_NAME = "SAPCommitFile"
@Field final String FILE_NAME = "File Name"
@Field final String WARNING = "Warning"
@Field final String FIELD_NAME = "FieldName"
@Field final String WARNING_MSG = "Select Input fields to proceed"
@Field final String INPUT_FIELD = "Input fields"

