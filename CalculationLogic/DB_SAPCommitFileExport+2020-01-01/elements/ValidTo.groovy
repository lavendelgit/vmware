Map exportConfiguration = out.ExportConfiguration
validToDates = out.SAPCommitFileCache?.collect { it[exportConfiguration.ATTRIBUTE7_COLUMN_NAME] }?.unique()
validToDatesFilter = api.options(exportConfiguration.VALID_TO, validToDates as List)
parameter = api.getParameter(exportConfiguration.VALID_TO)
return validToDatesFilter
