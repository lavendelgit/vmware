Map exportConfiguration = out.ExportConfiguration
contexts = out.SAPCommitFileCache?.collect { it[exportConfiguration.ATTRIBUTE2_COLUMN_NAME] }?.unique()
contextsFilter = api.options(exportConfiguration.CONTEXT, contexts as List)
parameter = api.getParameter(exportConfiguration.CONTEXT)
return contextsFilter
