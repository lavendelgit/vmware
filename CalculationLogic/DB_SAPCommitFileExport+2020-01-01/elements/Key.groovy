Map exportConfiguration = out.ExportConfiguration
keys = out.SAPCommitFileCache?.collect { it[exportConfiguration.ATTRIBUTE1_COLUMN_NAME] }?.unique()
keyFilter = api.options(exportConfiguration.KEY, keys as List)
parameter = api.getParameter(exportConfiguration.KEY)
return keyFilter
