String validFromDateString
String validToDateString
String validFromFormattedDate
String ValidToFormattedDate
List records
def exportConfigLib = libs.stdProductLib.ConstConfiguration
Map exportConfiguration = out.ExportConfiguration
ResultMatrix resultMatrix = api.newMatrix(exportConfigLib.SAP_COMMIT_PP_TABLE_FIELDS)
if (out.SelectedSAPCommitFileCache) {
    records = out.SelectedSAPCommitFileCache
    records?.each { record ->
        validFromDateString = record[exportConfiguration.KEY2_COLUMN_NAME]?.toString()
        validFromFormattedDate = validFromDateString ? Date.parse(exportConfiguration.DATE_YYYYMMDD_FORMAT, validFromDateString).format(exportConfiguration.DATE_MMDDYYYY_FORMAT) : ""
        validToDateString = record[exportConfiguration.ATTRIBUTE7_COLUMN_NAME]?.toString()
        ValidToFormattedDate = validToDateString ? Date.parse(exportConfiguration.DATE_YYYYMMDD_FORMAT, validToDateString).format(exportConfiguration.DATE_MMDDYYYY_FORMAT) : ""

        row = [
                record[exportConfiguration.ATTRIBUTE1_COLUMN_NAME],
                record[exportConfiguration.ATTRIBUTE2_COLUMN_NAME],
                record[exportConfiguration.ATTRIBUTE3_COLUMN_NAME],
                record[exportConfiguration.ATTRIBUTE4_COLUMN_NAME],
                record[exportConfiguration.ATTRIBUTE5_COLUMN_NAME],
                record[exportConfiguration.ATTRIBUTE6_COLUMN_NAME],
                validFromFormattedDate,
                ValidToFormattedDate,
                record[exportConfiguration.ATTRIBUTE8_COLUMN_NAME]
        ]
        resultMatrix.addRow(row)
    }
}

return resultMatrix