Map exportConfiguration = out.ExportConfiguration
variantConditionKeys = out.SAPCommitFileCache?.collect { it[exportConfiguration.ATTRIBUTE4_COLUMN_NAME] }?.unique()
variantConditionKeysFilter = api.options(exportConfiguration.VARIANT_CONDITION, variantConditionKeys as List)
parameter = api.getParameter(exportConfiguration.VARIANT_CONDITION)
return variantConditionKeysFilter
