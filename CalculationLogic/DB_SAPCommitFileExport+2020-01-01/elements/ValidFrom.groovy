Map exportConfiguration = out.ExportConfiguration
validFromDates = out.SAPCommitFileCache?.collect { it[exportConfiguration.KEY2_COLUMN_NAME] }?.unique()
validFromDatesFilter = api.options(exportConfiguration.VALID_FROM, validFromDates as List)
parameter = api.getParameter(exportConfiguration.VALID_FROM)
return validFromDatesFilter
