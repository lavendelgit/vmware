Map exportConfiguration = out.ExportConfiguration
contextValues = out.SAPCommitFileCache?.collect { it[exportConfiguration.ATTRIBUTE3_COLUMN_NAME] }?.unique()
contextValuesFilter = api.options(exportConfiguration.CONTEXT_VALUE, contextValues as List)
parameter = api.getParameter(exportConfiguration.CONTEXT_VALUE)
return contextValuesFilter
