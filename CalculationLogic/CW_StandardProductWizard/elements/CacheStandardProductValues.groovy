api.local.standardProducts = []
List filters = [
        Filter.equal(Constants.NAME, Constants.PX_STANDARDPRODUCTS),
]
List fields = [Constants.PX_STANDARDPRODUCTS_PRODUCT, Constants.PX_STANDARDPRODUCTS_OFFER, Constants.PX_STANDARDPRODUCTS_BASE, Constants.PX_STANDARDPRODUCTS_PRODUCTGROUP]
Object stdProductsStream = api.stream("PX50", null, fields, *filters)
api.local.standardProducts = stdProductsStream?.collect { row -> row }
stdProductsStream?.close()
return