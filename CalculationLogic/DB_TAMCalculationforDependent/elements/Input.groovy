api.local.baseSkus =[]
api.local.skus =[]
List productSkus = []
List filter = []
if (out.StartYear && out.EndYear && (out.StartYear < out.EndYear)) {
    filter << out.DataSelection
    filter << Filter.greaterOrEqual(Constants.COLUMN_ORDER_DATE, out.StartYear)
    filter << Filter.lessOrEqual(Constants.COLUMN_ORDER_DATE, out.EndYear)
    def ctx = api.getDatamartContext()
    def dm = ctx.getDatamart(Constants.PRICING_DISCOUNTING_DM)
    def query = ctx?.newQuery(dm)
            ?.select(Constants.COLUMN_PRODUCT_SKU, Constants.COLUMN_PRODUCT_SKU)
            ?.where(*filter)
            ?.orderBy(Constants.COLUMN_PRODUCT_SKU)
    streamResult = ctx?.streamQuery(query)
    while (streamResult?.next()) {
        productSkus << streamResult?.get()
    }
    streamResult?.close()
    api.local.baseSkus =  productSkus?.ProductSKU?.unique()
    api.local.skus << api.local.baseSkus
}
return

