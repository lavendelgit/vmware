if (out.StartYear && out.EndYear && (out.StartYear < out.EndYear)) {
    api.local.queriedPriceingDiscount = []
    List dependentSkus = api.local.skus?.flatten()?.unique()?.sort()
    dependentSkus.removeAll([null])
    List filters = [
            Filter.in(Constants.COLUMN_PRODUCT_SKU,dependentSkus ),
            Filter.greaterOrEqual(Constants.COLUMN_ORDER_DATE, out.StartYear),
            Filter.lessOrEqual(Constants.COLUMN_ORDER_DATE, out.EndYear)
    ]
    def ctx = api.getDatamartContext()
    def dm = ctx.getDatamart(Constants.PRICING_DISCOUNTING_DM)
    def query = ctx?.newQuery(dm)
            ?.select(Constants.COLUMN_PRODUCT_CLASS, Constants.PRODUCT_CLASS_LABEL)
            ?.select(Constants.COLUMN_SUBCLASS_GROUP, Constants.SUBCLASS_GROUP_LABEL)
            ?.select(Constants.COLUMN_PLATFORM_GROUP, Constants.PLATFORM_GROUP_LABEL)
            ?.select(Constants.COLUMN_PRODUCT_GROUP, Constants.PRODUCT_GROUP_LABEL)
            ?.select(Constants.COLUMN_PRODUCT_DIVISION, Constants.PRODUCT_DIVISION_LABEL)
            ?.select(Constants.COLUMN_CUSTOMER_PURCHASE_SKU, Constants.CUSTOMER_PURCHASE_SKU_LABEL)
            ?.select(Constants.COLUMN_PRODUCT_SKU, Constants.PRODUCT_SKU_LABEL)
            ?.select(Constants.COLUMN_PHANTOM_SKU, Constants.PHANTOM_SKU_LABEL)
            ?.select(Constants.COLUMN_ORDER_DATE, Constants.ORDER_DATE_LABEL)
            ?.select(Constants.COLUMN_GROSS_BOOKING_CC, Constants.GROSS_BOOKING_CC_LABEL)
            ?.select(Constants.COLUMN_CURRENCY_CODE, Constants.CURRENCY_CODE_LABEL)
            ?.where(*filters)
            ?.orderBy(Constants.COLUMN_PRODUCT_SKU)
    streamResult = ctx?.streamQuery(query)
    while (streamResult?.next()) {
        api.local.queriedPriceingDiscount << streamResult.get()
    }
    streamResult?.close()
}
return
