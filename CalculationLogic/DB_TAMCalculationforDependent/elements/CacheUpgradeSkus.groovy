List upgradeSkus = []
upgradeSkus << api.findLookupTableValues(Constants.BASE_TO_UPGRADE_CP_TABLE, Filter.in(Constants.PP_BASE_TO_UPGRADE_BASE_SKU, api.local.baseSkus))?.key2
upgradeSkus << api.findLookupTableValues(Constants.UPGRADE_TO_DESTINATION_CP_TABLE, Filter.in(Constants.PP_UPGRADE_TO_DESTINATION_DESTINATION_SKU, api.local.baseSkus))?.key1
List finalupgradeSkus = upgradeSkus?.flatten()?.unique()
api.local.skus << finalupgradeSkus
api.local.skus = api.local.skus?.flatten()?.unique()
api.local.baseSkus << finalupgradeSkus
api.local.baseSkus = api.local.baseSkus?.flatten()?.unique()
return
