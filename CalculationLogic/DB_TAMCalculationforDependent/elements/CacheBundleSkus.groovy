List bundleSkus =[]
List filters = [
        Filter.equal(Constants.COLUMN_NAME, Constants.BOM_PX_TABLE),
        Filter.in(Constants.COLUMN_SUB_COMPONENT, api.local.baseSkus)
]
bundleSkus << libs.vmwareUtil.TAMDBUtils.getBOMRecords(filters)
bundleSkus = bundleSkus?.unique()
api.local.skus << bundleSkus
api.local.skus = api.local.skus?.flatten()?.unique()
api.local.baseSkus << bundleSkus
api.local.baseSkus = api.local.baseSkus?.flatten()?.unique()
return