List baseSkus = []
baseSkus = api.local.baseSkus?.flatten()
baseSkus.removeAll([null])
List filters = [
        Filter.equal(Constants.COLUMN_NAME, Constants.STANDARD_PRODUCT_PX_TABLE),
        Filter.in(Constants.COLUMN_BASE_SKU, baseSkus)
]
api.local.skus << libs.vmwareUtil.TAMDBUtils.getStandardProductRecords(filters).sku
api.local.skus = api.local.skus?.flatten()?.unique()
return
