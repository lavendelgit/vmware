BigDecimal totalGrossBookingsCC = 0.00
BigDecimal percentageIncrease
BigDecimal estimatedTAM

ResultMatrix resultMatrix = api.newMatrix( Constants.PRODUCT_CLASS_LABEL, Constants.SUBCLASS_GROUP_LABEL, Constants.PLATFORM_GROUP_LABEL, Constants.PRODUCT_GROUP_LABEL, Constants.PRODUCT_DIVISION_LABEL, Constants.CUSTOMER_PURCHASE_SKU_LABEL,Constants.PRODUCT_SKU_LABEL,Constants.PHANTOM_SKU_LABEL,Constants.ORDER_DATE_LABEL,Constants.CURRENCY_CODE_LABEL,Constants.GROSS_BOOKING_CC_LABEL)
resultMatrix?.setEnableClientFilter(true)

if (!api.local.queriedPriceingDiscount) {
    resultMatrix = api.newMatrix(" ")
    warningCell = resultMatrix?.styledCell(Constants.NO_DETAILS_WARNING_MESSAGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    resultMatrix?.addRow(warningCell)
}
if (out.StartYear && out.EndYear && (out.StartYear < out.EndYear)) {
    totalGrossBookingsCC = 0.00

    api.local.queriedPriceingDiscount?.each { data ->
        data[Constants.GROSS_BOOKING_CC_LABEL] = data[Constants.GROSS_BOOKING_CC_LABEL]?.setScale(2, BigDecimal.ROUND_HALF_UP)
        resultMatrix?.addRow(data)
        totalGrossBookingsCC += data[Constants.GROSS_BOOKING_CC_LABEL] ? data[Constants.GROSS_BOOKING_CC_LABEL]?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00
    }
    api.local.totalGrossBookingsCC = totalGrossBookingsCC
    return resultMatrix
}