BigDecimal estimatedTAM = 0.0
if (out.StartYear && out.EndYear && (out.StartYear < out.EndYear)) {
    ResultMatrix resultMatrix = api.newMatrix(Constants.COLUMN_FIELDS, Constants.COLUMN_VALUES)
    percentageChange = api.input(Constants.PERCENTAGE_CHANGE) ?: 0
    estimatedTAM = api.input(Constants.PERCENTAGE_CHANGE) ? (api.local.totalGrossBookingsCC + (api.local.totalGrossBookingsCC * (percentageChange / 100)))?.setScale(2, BigDecimal.ROUND_HALF_UP) : api.local.totalGrossBookingsCC
    dataSelection = (out.DataSelection ?: Constants.PLACE_HOLDER) as String
    startDate = (api.userEntry(Constants.INPUT_START_YEAR))
    endDate = (api.userEntry(Constants.INPUT_END_YEAR))


    styleTotal = getStyle(resultMatrix, Constants.COLUMN_TOTAL, out.ConstantConfiguration)
    styleGrossBookingCC = getStyle(resultMatrix, api.local.totalGrossBookingsCC.toString(), out.ConstantConfiguration)
    styleEstimateTAM = getStyle(resultMatrix, Constants.COLUMN_ESTIMATED_TAM, out.ConstantConfiguration)
    styleEstimatedTAMValue = getStyle(resultMatrix, estimatedTAM.toString(), out.ConstantConfiguration)
    stylePercentageChange = getStyle(resultMatrix, Constants.PERCENTAGE_CHANGE, out.ConstantConfiguration)
    stylePercentageChangeValue = getStyle(resultMatrix, percentageChange.toString(), out.ConstantConfiguration)
    styleDataFilter = getStyle(resultMatrix, Constants.DATA_SELECTION_LABEL, out.ConstantConfiguration)
    styleDataFilterValue = getStyle(resultMatrix, dataSelection, out.ConstantConfiguration)
    styleStartDate = getStyle(resultMatrix, Constants.INPUT_START_YEAR, out.ConstantConfiguration)
    styleStartDateValue = getStyle(resultMatrix, startDate, out.ConstantConfiguration)
    styleEndDate = getStyle(resultMatrix, Constants.INPUT_END_YEAR, out.ConstantConfiguration)
    styleEndDateValue = getStyle(resultMatrix, endDate, out.ConstantConfiguration)

    resultMatrix?.addRow(styleTotal, styleGrossBookingCC)
    resultMatrix?.addRow(styleEstimateTAM, styleEstimatedTAMValue)
    resultMatrix?.addRow(stylePercentageChange, stylePercentageChangeValue)
    resultMatrix?.addRow(styleDataFilter, styleDataFilterValue)
    resultMatrix?.addRow(styleStartDate, styleStartDateValue)
    resultMatrix?.addRow(styleEndDate, styleEndDateValue)
    return resultMatrix
} else {
    resultMatrix = api.newMatrix("")
    warningCell = getStyle(resultMatrix, out.ConstantConfiguration.WARNING, out.ConstantConfiguration)
    resultMatrix?.addRow(warningCell)
    if (out.StartYear > out.EndYear) {
        warningCell = getStyle(resultMatrix, out.ConstantConfiguration.YEAR_WARNING_MESSAGE, out.ConstantConfiguration)
        resultMatrix?.addRow(warningCell)
    } else {
        warningCell = getStyle(resultMatrix, Constants.ALL_INPUTS_WARNING_MESSAGE, out.ConstantConfiguration)
        resultMatrix?.addRow(warningCell)
    }
    return resultMatrix
}

def getStyle(ResultMatrix resultMatrix, String data, Map Configuration) {
    return resultMatrix?.styledCell(data, Configuration.TEXT_COLOR, Configuration.BG_COLOR, Configuration.TEXT_WEIGHT, Configuration.TEXT_ALLIGN)
}

