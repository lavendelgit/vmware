api.local.skuInfo = getSKUInfo()
return

Map getSKUInfo() {
    Map skuInfos = [:]
    def extensionName = "StandardProducts"
    def filter = [
            Filter.equal("name", extensionName),
            Filter.equal("attribute24", "Bundle"),
    ]
    def skuStream = api.stream("PX50", "sku", *filter)
    skuInfos = skuStream?.collectEntries {
        skuInfo ->
            [(skuInfo.sku): [
                    "ShortDesc": api.local.shortDescription?.getAt(skuInfo.sku),
                    "ItemType" : "Bundle Product"
            ]]
    }
    skuStream?.close()
    return skuInfos
}
