def basePrice
def swDetails = out.SKUInfo
def supportLevel = out.SupportLevel
def priceDetails = out.LoadSWBasePrices
def priceDetailsL14L34 = out.LoadSWBasePricesL14L34
def editionBasePrice = out.EditionBasePrice
def editionFactor = out.EditionFactor
def bandwidthUpcharge = priceDetailsL14L34?.attribute1
def l34Discount = priceDetailsL14L34?.attribute2
def hostedGatewayUpcharge = priceDetails?.attribute3
def softwareGatewayUpcharge = priceDetails?.attribute4
if (bandwidthUpcharge) bandwidthUpcharge = bandwidthUpcharge.toBigDecimal()
if (l34Discount) l34Discount = l34Discount.toBigDecimal()
if (hostedGatewayUpcharge) hostedGatewayUpcharge = hostedGatewayUpcharge.toBigDecimal()
if (softwareGatewayUpcharge) softwareGatewayUpcharge = softwareGatewayUpcharge.toBigDecimal()
if (editionFactor) editionFactor = editionFactor.toBigDecimal()
if (editionBasePrice) editionBasePrice = editionBasePrice.toBigDecimal()

if (supportLevel == Constants.STR_SL14) {
    basePrice = bandwidthUpcharge + editionBasePrice

}
if (supportLevel == Constants.STR_SL34) {
    if (bandwidthUpcharge != null && l34Discount != null && editionBasePrice != null)
        basePrice = (bandwidthUpcharge + editionBasePrice) - l34Discount
}

if (swDetails?.attribute5.equals(Constants.STR_SW_GATEWAY)) {
    if (basePrice && softwareGatewayUpcharge && editionFactor) {
        basePrice = basePrice + (softwareGatewayUpcharge * editionFactor)

    }
}
if (swDetails?.attribute5.equals(Constants.STR_HO_GATEWAY)) {
    if (basePrice && hostedGatewayUpcharge && editionFactor) {
        basePrice = basePrice + (hostedGatewayUpcharge * editionFactor)

    }
}
return basePrice