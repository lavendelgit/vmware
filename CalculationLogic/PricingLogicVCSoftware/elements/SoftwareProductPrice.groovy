import java.math.RoundingMode

def suggestedRetailPrice

def years = out.Years?.toBigDecimal()
def basePrice = out.BasePrice?.toBigDecimal()
def billingTermFactor = out.BillingTermFactor?.toBigDecimal()
def serviceLevelFactor = out.ServiceLevelFactor?.toBigDecimal()
def yearsFactor = out.YearsFactor?.toBigDecimal()
def productType = out.ProductType
if (productType == "Software") {

    if (basePrice && years && billingTermFactor)
        suggestedRetailPrice = basePrice * years * billingTermFactor * out.SegmentAdjustmentFactor

    if (suggestedRetailPrice && serviceLevelFactor && yearsFactor != 0 && yearsFactor != null)
        suggestedRetailPrice = suggestedRetailPrice * serviceLevelFactor / yearsFactor
}

return suggestedRetailPrice?.toBigDecimal()
