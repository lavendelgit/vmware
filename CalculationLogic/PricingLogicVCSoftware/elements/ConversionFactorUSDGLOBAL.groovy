def LC = out.LCFactor
if (LC == null) return 0

def exchangeRecord = api.global.localCurrencyExchangeMap[LC]
def fxRate = (exchangeRecord) ? exchangeRecord.attribute7 : null

if (fxRate != null) {
    return new BigDecimal(fxRate)
} else {
    return null
}