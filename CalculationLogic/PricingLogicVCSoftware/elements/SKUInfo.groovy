def sku = out.SKU
def vcSoftPXObj = api.global.batch[sku].vcSoftwarePXObj
if (!vcSoftPXObj) {
    api.abortCalculation()
    return
}
return vcSoftPXObj