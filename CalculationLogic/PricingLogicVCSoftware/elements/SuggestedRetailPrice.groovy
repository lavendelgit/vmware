import java.math.RoundingMode

def srp
def productType = out.ProductType
def swProductPrice = out.SoftwareProductPrice

if (productType == "Software" && swProductPrice != null) {
    srp = swProductPrice
}


return srp?.toBigDecimal()?.setScale(0, RoundingMode.UP)