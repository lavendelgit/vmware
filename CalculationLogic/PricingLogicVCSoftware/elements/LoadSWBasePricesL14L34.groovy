def bandwidth = out.Bandwidth
def edition = out.Edition
if (bandwidth && edition) {

    def key = bandwidth + edition
    return api.global.lPriceMap[key]

}

