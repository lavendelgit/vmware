def listPrice = out.FinalRetailPrice
def rate = out.ConversionFactorCNY
def LC = out.LCFactor
String currency = "EUR"

if (!LC || !rate || !listPrice) return ""

def lcPrice = listPrice * rate
def threshHold = 500
def roundToThresholdMet = 10
def roundToThresholdNotMet = 1
def substract = 1
def EURCur = libs.vmwareUtil.VCRounding.getCurrencyValue(currency, out.ProductType, api.global.roundingRulesMap)

if (out.ProductType == "Perpetual- SnS") {
    return lcPrice.setScale(2, BigDecimal.ROUND_HALF_UP)
} else {
    if (LC == "LC-77") {
        return libs.vmwareUtil.RoundingLogicDesktopV2.applyRoundingLogic(lcPrice, threshHold, substract, roundToThresholdMet, roundToThresholdNotMet)?.toBigDecimal()
    } else {
        return libs.vmwareUtil.VCRounding.roundedLCPrice(lcPrice, EURCur.RoundingType, EURCur.Precision)
    }
}  