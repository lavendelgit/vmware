List filter = []
Map exportConfiguration = out.ExportConfiguration

List selectedKey = out.Key
filter = selectedKey? filter << Filter.in(exportConfiguration.ATTRIBUTE1_COLUMN_NAME, selectedKey) : filter

List selectedContext = out.Context
filter = selectedContext? filter << Filter.in(exportConfiguration.ATTRIBUTE2_COLUMN_NAME, selectedContext) : filter

List selectedContextValue = out.ContextValue
filter = selectedContextValue? filter << Filter.in(exportConfiguration.ATTRIBUTE3_COLUMN_NAME, selectedContextValue) : filter

List selectedVariantConditionKey = out.VariantConditionKey
filter = selectedVariantConditionKey? filter << Filter.in(exportConfiguration.ATTRIBUTE4_COLUMN_NAME, selectedVariantConditionKey) : filter

List selectedValidFrom = out.ValidFrom
filter = selectedValidFrom? filter << Filter.in(exportConfiguration.KEY2_COLUMN_NAME, selectedValidFrom) : filter

List selectedValidTo = out.ValidTo
filter = selectedValidTo? filter << Filter.in(exportConfiguration.ATTRIBUTE7_COLUMN_NAME, selectedValidTo) : filter

List selectedRun = out.Run
filter = selectedRun? filter << Filter.in(exportConfiguration.ATTRIBUTE8_COLUMN_NAME, selectedRun) : filter

if (filter) {
    return api.findLookupTableValues(exportConfiguration.COMMIT_PRICELIST_TABLE, *filter)
}



