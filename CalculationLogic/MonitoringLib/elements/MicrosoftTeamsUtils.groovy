import groovy.transform.Field

/**
 * This is mandatory parameter! Unless you will specify the webHookUrl parameter in the sendMessage() call.
 * It is an url of a Teams channel where the messages should be send by default.
 * The URL is generated when you create the connector Incoming webhook in a Teams channel (https://outlook.office.com/webhook/...).
 */
@Field DETAULT_TEAMS_WEBHOOK_URL = ""

/**
 * Pricefx logo (link from Twitter)
 */
@Field TEAMS_ICON_URL = "https://www.pricefx.com/wp-content/uploads/2019/11/Pricefx_logo_2019_Dark-blue_fx.png"

/**
 * Theme color of the teams message
 */
@Field THEME_COLOR = "0076D7"


/**
 * Sends a message to Microsoft Teams channel
 * See more: https://docs.microsoft.com/en-us/microsoftteams/platform/concepts/connectors/connectors-using#creating-messages-through-office-365-connectors
 *
 * @param title The title of the message in Teams
 * @param facts List of Maps
 * @param webHookUrl The URL generated when you create a connector by the Incoming webhook in a Teams channel.
 * @return a Map returned by httpCall holding two keys:'responseBody' and 'errorCode'
 */
def sendMessage(String title, List<Map> facts, String webHookUrl) {
    Map requestBody = [
            "@type"     : "MessageCard",
            "themeColor": THEME_COLOR,
            "summary"   : title,
            "sections"  : [
                    [
                            "activityTitle"   : "![PfxLogo](${TEAMS_ICON_URL})${title}",
                            "activitySubtitle": "on ${api.currentPartitionName()}",
                            "activityImage"   : TEAMS_ICON_URL
                    ]
            ]
    ]

    facts.each { fact ->
        def factsList = fact.collect { key, value ->
            ["name": key, "value": value]
        }

        requestBody.sections.add([
                "facts": factsList
        ])
    }

    String reqBodyJson = api.jsonEncode(requestBody)
    api.httpCall(webHookUrl, reqBodyJson, "POST", "application/json")
}


/**
 * Sends a message to the default Microsoft Teams channel defined by DETAULT_TEAMS_WEBHOOK_URL
 * See more: https://docs.microsoft.com/en-us/microsoftteams/platform/concepts/connectors/connectors-using#creating-messages-through-office-365-connectors
 *
 * @param title The title of the message in Teams
 * @param facts List of Maps. See the documentation above for more details
 * @param webHookUrl The URL of a Teams channel where the message should be sent to
 * @return a Map returned by httpCall holding two keys:'responseBody' and 'errorCode'
 */

def sendMessage(String title, List<Map> facts) {
    return sendMessage(title, facts, DETAULT_TEAMS_WEBHOOK_URL)
}