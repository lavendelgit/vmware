import java.text.DateFormat

List getListMonths(Date dateFrom, Date dateTo, Locale locale, DateFormat df) {
    Calendar calendar = Calendar.getInstance(locale)
    calendar.setTime(dateFrom)
    List months = []
    while (calendar?.getTime()?.getTime() <= dateTo?.getTime()) {
        dateString = df.format(calendar.getTime())
        months.add([
                priceMonth: dateString
        ]
        )
        calendar.add(Calendar.MONTH, 1)

    }
    return months
}

Map generateRecords(Object skuDetails, def month, def cost) {
    return [
            (Constants.COLUMN_PART_NUMBER)                : skuDetails?.key,
            (Constants.COLUMN_PRICE_CHANGE_EFFECTIVE_DATE): cost?.PriceChangeEffectiveDate,
            (Constants.COLUMN_PLATFORM_GROUP)             : skuDetails?.value?.PlatformGroup,
            (Constants.COLUMN_PRODUCT_PLATFORM)           : skuDetails?.value?.ProductPlatform,
            (Constants.COLUMN_PRODUCT_FAMILY)             : skuDetails?.value?.ProductFamily,
            (Constants.PRODUCT)                           : skuDetails?.value?.Product,
            (Constants.COLUMN_PRICE_MONTH)                : month.priceMonth,
            (Constants.USD)                               : cost.USD?.setScale(2, BigDecimal.ROUND_HALF_UP),
            (Constants.EUR)                               : cost.EUR?.setScale(2, BigDecimal.ROUND_HALF_UP),
            (Constants.JPY)                               : cost.JPY?.setScale(2, BigDecimal.ROUND_HALF_UP),
            (Constants.CNY)                               : cost.CNY?.setScale(2, BigDecimal.ROUND_HALF_UP),
            (Constants.AUD)                               : cost.AUD?.setScale(2, BigDecimal.ROUND_HALF_UP),
            (Constants.GBP)                               : cost.GBP?.setScale(2, BigDecimal.ROUND_HALF_UP),
            (Constants.EMEA_USD)                          : cost.EMEAUSD?.setScale(2, BigDecimal.ROUND_HALF_UP),
            (Constants.EMEA_USD2)                         : cost.EMEAUSD2?.setScale(2, BigDecimal.ROUND_HALF_UP),
            (Constants.GUSD)                              : cost.GUSD?.setScale(2, BigDecimal.ROUND_HALF_UP),
            (Constants.COLUMN_END_DATE)                   : cost.EndDate,
    ]

}