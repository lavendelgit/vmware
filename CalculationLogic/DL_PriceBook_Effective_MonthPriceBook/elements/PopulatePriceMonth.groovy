import java.text.DateFormat
import java.text.SimpleDateFormat


api.local.plData = []
Map prevCost = [:]
if (api.local.skuPrices) {
    DateFormat df2 = new SimpleDateFormat("MMM-yyyy", Locale.US)
    List months = []
    for (sku in api.local.skuPrices) {
        prevRecordPrice = 0.0
        costs = sku.value?.PriceData?.sort { it.PriceChangeEffectiveDate }
        for (cost in costs) {
            PriceChangeEffectiveDate = cost.PriceChangeEffectiveDate
            endDate = cost.EndDate
            months = Lib.getListMonths(PriceChangeEffectiveDate, endDate, Locale.US, df2)
            for (month in months) {
                costInfo = getChangedCost(cost, prevCost)
                if (costInfo.changed == true) {
                    prevCost = costInfo.cost
                    api.local.plData << Lib.generateRecords(sku, month, cost)
                }
            }
        }
    }
}
return

Map getChangedCost(Map cost, Map prevCost) {
    Map costInfo = [:]
    costInfo.changed = false
    List currencies = [(Constants.USD), (Constants.EUR), (Constants.JPY), (Constants.CNY), (Constants.AUD), (Constants.GBP), (Constants.EMEA_USD), (Constants.EMEA_USD2), (Constants.GUSD)]
    for (currency in currencies) {
        BigDecimal ctCost = cost[currency]?.setScale(2, BigDecimal.ROUND_HALF_UP)
        BigDecimal prev = prevCost[currency]?.setScale(2, BigDecimal.ROUND_HALF_UP)
        if (ctCost && ctCost != prev) {
            costInfo.cost = cost
            costInfo.changed = true
            break
        }
    }
    return costInfo
}