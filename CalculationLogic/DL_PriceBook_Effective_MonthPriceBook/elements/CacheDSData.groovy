queriedPriceBook = []
def source = api.getDatamartRowSet("source")
while (source?.next()) {
    def row = source?.getCurrentRow()
    queriedPriceBook << row
}
api.local.groupedPriceBookData = queriedPriceBook?.groupBy { it.PartNumber }
return