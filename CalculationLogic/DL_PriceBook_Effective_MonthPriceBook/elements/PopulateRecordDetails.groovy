if (api.local.groupedPriceBookData) {
    api.local.skuPrices = [:]
    List data
    for (skuData in api.local.groupedPriceBookData) {
        value = skuData.value
        if (!api.local.skuPrices[value?.PartNumber?.getAt(0)]) {
            api.local.skuPrices[value?.PartNumber?.getAt(0)] = [
                    (Constants.COLUMN_PLATFORM_GROUP)  : value?.PlatformGroup?.getAt(0),
                    (Constants.COLUMN_PRODUCT_PLATFORM): value?.ProductPlatform?.getAt(0),
                    (Constants.COLUMN_PRODUCT_FAMILY)  : value?.ProductFamily?.getAt(0),
                    (Constants.PRODUCT)                : value?.Product?.getAt(0),
            ]
            data = []
            for (priceData in value) {
                data << [
                        (Constants.COLUMN_PRICE_CHANGE_EFFECTIVE_DATE): priceData?.PriceChangeEffectiveDate,
                        (Constants.COLUMN_END_DATE)                   : priceData?.EndDate,
                        (Constants.USD)                               : priceData.USD,
                        (Constants.EUR)                               : priceData.EUR,
                        (Constants.JPY)                               : priceData.JPY,
                        (Constants.CNY)                               : priceData.CNY,
                        (Constants.AUD)                               : priceData.AUD,
                        (Constants.GBP)                               : priceData.GBP,
                        (Constants.EMEA_USD)                          : priceData.EMEAUSD,
                        (Constants.EMEA_USD2)                         : priceData.EMEAUSD2,
                        (Constants.GUSD)                              : priceData.GUSD

                ]
                api.local.skuPrices[value?.PartNumber?.getAt(0)]?.PriceData = data
            }
        }
    }
}
return