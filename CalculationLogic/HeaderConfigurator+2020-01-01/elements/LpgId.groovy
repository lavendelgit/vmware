def ce = api.createConfiguratorEntry(InputType.HIDDEN, Constants.CONF_LPGID)
if(api.local.lpgId){
    ce.getFirstInput()?.setValue(api.local.lpgId)
}
ce
