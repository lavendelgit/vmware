def lpgId = api.input(Constants.LPGID)
def conf_lpgId = api.input(Constants.CONF_LPGID)
def inputs_lpgId = api.input(Constants.INPUTS_LPGID)
api.local.lpgId = lpgId?:conf_lpgId?:inputs_lpgId
return true