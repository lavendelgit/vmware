def ce = api.createConfiguratorEntry(InputType.HIDDEN, Constants.INPUTS_LPGID)
if (!ce.getFirstInput()?.getValue())
    ce.getFirstInput()?.setValue(api.local.lpgId)
def lpgId = ce.getFirstInput()?.getValue()
List filters = []
List productValues = []
if (lpgId) {
    filters << Filter.equal(Constants.PRICE_GRID_ID, lpgId)
    productValues = getLPGQuery(filters, null, null)?.collect { it?.(Constants.PRODUCT) }.unique() as List
}
def product = ce.createParameter(InputType.OPTION, Constants.PRODUCT)
product.valueOptions = productValues
def offer = ce.createParameter(InputType.OPTION, Constants.OFFER)
def base = ce.createParameter(InputType.OPTION, Constants.BASE)
if (lpgId && filters) {
  if (product.getValue() != null) {
        offer.valueOptions = getLPGQuery(filters, Constants.PRODUCT, product?.getValue())?.collect { it?.(Constants.OFFER) }.unique() as List
    }
if (offer.getValue() != null) {
        base.valueOptions = getLPGQuery(filters, Constants.OFFER, offer?.getValue())?.collect { it?.(Constants.BASE) }.unique() as List
    }
}
return ce
def getLPGQuery(List filters, def attribute = null, def lookupValue = null) {
    if (attribute) {
        filters << Filter.equal(attribute, lookupValue)
    }
    return api.namedEntities(api.find("PGI", 0, api.getMaxFindResultsLimit(), null, null, *filters))
}
