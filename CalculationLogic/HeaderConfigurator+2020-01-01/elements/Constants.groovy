import groovy.transform.Field

@Field final String PRICE_GRID_ID = "priceGridId"
@Field final String ID = "id"
@Field final String LPGID = "LpgId"
@Field final String CONF_LPGID = "conf_LpgId"
@Field final String INPUTS_LPGID = "inputs_LpgId"
@Field final String PRODUCT = "Product"
@Field final String OFFER = "Offer"
@Field final String BASE = "Base"
@Field final String COLUMN_PRODUCT = "attribute3"
@Field final String COLUMN_OFFER = "attribute4"
@Field final String COLUMN_BASE = "attribute5"