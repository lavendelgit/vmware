List replaceWithStandardProductAttribute(List configTabAttributes) {
    List standardProductAttributeList = []
    List stdAttributeList = libs.stdProductLib.ConstConfiguration.STD_ATTRIBUTES_LIST
    Map standardProductAttributeMap = libs.stdProductLib.ConstConfiguration.STANDARDPRODUCT_ATTRIBUTE_MAPPING
    for (attribute in configTabAttributes) {
        if (stdAttributeList.contains(attribute)) {
            standardProductAttributeList << standardProductAttributeMap?.find { it.value == attribute }?.key
        } else {
            standardProductAttributeList << attribute
        }
    }
    return standardProductAttributeList
}

List replaceWithConfigTabAttribute(List productRuleAttributes) {
    List configTabAttributeList = []
    Map standardProductAttributeMap = libs.stdProductLib.ConstConfiguration.STANDARDPRODUCT_ATTRIBUTE_MAPPING
    for (attribute in productRuleAttributes) {
        if (Constants.STANDARD_PRODUCT_ATTRIBUTES_LIST.contains(attribute)) {
            configTabAttributeList << standardProductAttributeMap?.find { it.key == attribute }?.value
        } else {
            configTabAttributeList << attribute
        }
    }
    return configTabAttributeList
}

List insertRecords(String tableName, Map params) {
    return [
            tableName,
            params.SKU,
            params.Attribute,
            params.Value,
            params.SAPBaseSKU,
            params.ChargeId,
            params.Product,
            params.Offer,
            params.Base,
            params.AttributeType,

    ]
}

Map generateRecords(Map vcPxParams, List sapAttributes, List attributeValues) {
    Map record = [:]
    record = [
            SKU          : vcPxParams.StandardSKU,
            Product      : vcPxParams.Product,
            Offer        : vcPxParams.Offer,
            Base         : vcPxParams.Base,
            SAPBaseSKU   : vcPxParams.SAPSKU,
            ChargeId     : vcPxParams.ChargeId,
            AttributeType: vcPxParams.AttributeType,
            Attribute    : sapAttributes?.join(Constants.SEPARATER),
            Value        : attributeValues?.join(Constants.SEPARATER)
    ]
    return record
}

String getChargeID(String chargeId, String billingModel) {
    if (Constants.VALID_BILLING_MODEL_LIST.contains(billingModel)) {
        return chargeId
    }
}