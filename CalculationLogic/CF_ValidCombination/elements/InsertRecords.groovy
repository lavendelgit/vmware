if (api.local.pxRecords) {
    payload = [
            "data": [
                    "data"   : api.local.pxRecords,
                    "header" : [
                            "name",
                            "sku",
                            "attribute1",
                            "attribute2",
                            "attribute11",
                            "attribute12",
                            "attribute14",
                            "attribute15",
                            "attribute16",
                            "attribute17",
                    ],
                    "options": [
                            "detectJoinFields"    : false,
                            "maxJoinFieldsLengths": []
                    ]
            ]]
    api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
    return
}
