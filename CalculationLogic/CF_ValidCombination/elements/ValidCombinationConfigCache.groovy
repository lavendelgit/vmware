List products = api.local.validStandardProductsCache?.values()?.Product
List filters = [
        Filter.in(Constants.KEY1_COLUMN_NAME, products),
        Filter.equal(Constants.INC_FLAG_COLUMN_NAME, Constants.YES_INDICATOR)
]
String StdprodAtt
return api.findLookupTableValues(Constants.VALID_COMBINATION_CONFIGURATION_PP_TABLE, *filters)?.sort { it.attribute2 }.collectEntries { configuration ->
    StdprodAtt = Constants.STANDARD_ATTRIBUTE_MAP?.keySet()?.contains(configuration.key4) ? Constants.STANDARD_ATTRIBUTE_MAP[configuration.key4] : configuration.key4
    [
            (configuration.key1 + "_" + configuration.key2 + "_" + configuration.key3 + "_" + StdprodAtt): [
                    "Product"     : configuration.key1,
                    "Offer"       : configuration.key2,
                    "Base"        : configuration.key3,
                    "Attribute"   : StdprodAtt,
                    "SAPAttribute": configuration.attribute1,
                    "Sequence"    : configuration.attribute2 as Integer
            ]
    ]
}