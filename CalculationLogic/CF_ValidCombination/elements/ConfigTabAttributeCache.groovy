return api.findLookupTableValues(Constants.CONFIG_TAB_ATTRIBUTES_TABLE, Filter.equal(Constants.INCLUSION_FLAG_COLUMN_NAME, Constants.YES_INDICATOR))?.collectEntries {
    [(it.name): it.attribute3]
}