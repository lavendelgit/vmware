if (api.local.standardProductsCache) {
    api.local.validConfigAttributes = []
    api.local.pxRecords = []
    api.local.attributeRecords = []
    Map pxParams = [:]
    Map validCombinationParams = [:]
    Map weightedAttributes = out.ValidCombinationConfigCache?.groupBy({ libs.vmwareUtil.CacheManager.getWeightage([it.value.Product, it.value.Offer, it.value.Base], "*") })
    List sortedValues = weightedAttributes?.values() as List
    List validConfigAttributes = out.ValidCombinationConfigCache?.collect { it.value.Attribute } as List
    List validAttributes = []
    Map validAttributeInfo = [:]
    Map attributeData = [:]
    String attributeType = out.AttributeTypeCache
    Map chargeIDCache = out.ChargeIDConfigCache
    List chargeIdValues = out.ChargeIDConfigCache?.attribute2?.split(',')*.trim()
    for (standardProduct in api.local.validStandardProductsCache) {
        stdProductAttributes = standardProduct?.value?.subMap(validConfigAttributes)
        sapAttributes = []
        attributeValues = []
        validAttributes = []
        validAttributes = getValidAttributes(standardProduct, stdProductAttributes, sortedValues)
        if (validAttributes) {
            validCombinationParams = [:]
            sequencedAttributes = validAttributes?.sort { attribute1, attribute2 -> attribute1.Sequence <=> attribute2.Sequence }
            sapAttributes = sequencedAttributes?.collect { it.SAPName }
            attributeValues = sequencedAttributes?.collect { it.Value }
            validCombinationParams.StandardSKU = standardProduct.value.StandardProductSKU
            validCombinationParams.SAPSKU = out.SAPBaseSKU
            validCombinationParams.Product = standardProduct.value.Product
            validCombinationParams.Offer = standardProduct.value.Offer
            validCombinationParams.Base = standardProduct.value.Base
            validCombinationParams.AttributeType = attributeType
            validCombinationParams.ChargeId = Lib.getChargeID(chargeIDCache?.attribute5, chargeIDCache?.attribute4)
            pxParams = Lib.generateRecords(validCombinationParams, sapAttributes, attributeValues)
            api.local.attributeRecords << pxParams
            api.local.pxRecords << Lib.insertRecords(Constants.VALID_COMBINATION_TABLE, pxParams)
        }
    }
}
return


Map populateValidAttributeInfo(Map attributeData, List validAttributeConfig) {
    Map validAttributeInfo = [:]
    String sapName
    Integer sequence
    List keys = libs.vmwareUtil.util.frameKeyLists(attributeData.Product, attributeData.Offer, attributeData.Base, [attributeData.Attribute])
    while (keys && keys.size() > 0) {
        key = keys.getAt(0)
        sapName = validAttributeConfig?.getAt(key)?.SAPAttribute?.getAt(0)
        sequence = validAttributeConfig?.getAt(key)?.Sequence?.getAt(0)
        if (sapName) {
            break
        }
        keys.removeAt(0)
    }
    if (sapName && attributeData.Value) {
        validAttributeInfo = [SAPName : sapName,
                              Value   : attributeData.Value,
                              Sequence: sequence]

    }
    return validAttributeInfo
}

List getValidAttributes(Object standardProduct, Map standardAttributes, List sortedValues) {
    Map validAttributeInfo = [:]
    List validAttributesValue = []
    for (pdtAttribute in standardAttributes) {
        if (pdtAttribute.key && pdtAttribute.value) {
            attributeData = [Product  : standardProduct.value.Product,
                             Offer    : standardProduct.value.Offer,
                             Base     : standardProduct.value.Base,
                             Attribute: pdtAttribute.key,
                             Value    : pdtAttribute.value]
            validAttributeInfo = populateValidAttributeInfo(attributeData, sortedValues)
            if (validAttributeInfo) {
                validAttributesValue << validAttributeInfo
            }
        }
    }
    return validAttributesValue
}