if (out.SAPBaseSKU) {
    List filter = [
            Filter.equal(Constants.NAME, Constants.STANDARD_PRODUCT_TABLE_NAME),
            Filter.in(Constants.SAPBASE_SKU_COLUMN_NAME, out.SAPBaseSKU),
            Filter.equal(Constants.SKU_TYPE_COLUMN_NAME, Constants.STANDALONE)
    ]

    recordStream = api.stream("PX50", null, *filter)
    api.local.standardProductsCache = recordStream?.collect { row -> row }
    recordStream?.close()
}
return