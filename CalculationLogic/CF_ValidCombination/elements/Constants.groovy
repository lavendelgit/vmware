import groovy.transform.Field

@Field final String STANDARD_PRODUCT_TABLE_NAME = "StandardProducts"
@Field final String SAP_BASE_SKU_TABLE = "SAPBaseSKU"
@Field final String CONFIG_TAB_ATTRIBUTES_TABLE = "ConfigTabAttributes"
@Field final String VALID_COMBINATION_TABLE = "ValidCombination"
@Field final String PRICE_LIST_REGION_PP_TABLE = "PriceListRegion"
@Field final List STANDARD_PRODUCT_ATTRIBUTES_LIST = ["Payment Type", "Term", "Support Tier", "Support Type", "Data Center", "Purchasing Program", "Programatic Discount", "Volume Tier", "Retention Period"]
@Field final String SEPARATER = "."
@Field final String ALL_PRICELISTREGION = "ALL"
@Field final int STARTINDEX = 0
@Field final String END_OF_LAUNCH = "EOL"
@Field final String YES_INDICATOR = "Yes"
@Field final String LaunchDate = "LaunchDate"
@Field final String EndDate = "EndDate"
@Field final Map STANDARD_ATTRIBUTE_MAP = [
        "Segment Type"    : "Segment",
        "Volume Tier Size": "Volume Tier"
]
@Field final String SKU_CONFIG = "SKUConfig"
@Field final String ATTRIBUTE_TYPE = "AttributeType"
@Field final List VALID_BILLING_MODEL_LIST = ["COMMIT_REQUIRED", "COMMIT_OPTIONAL"]
@Field final String CHARGE_ID_CONFIGURATION_PP_TABLE = "ChargeIDConfiguration"
@Field final String VALID_COMBINATION_CONFIGURATION_PP_TABLE = "ValidCombinationConfiguration"
@Field final String NAME = "name"
@Field final String SKU = "sku"
@Field final String STANDALONE = "Standalone"
@Field final String PX50 = "PX50"
@Field final String PX20 = "PX20"
@Field final String COLUMN_NAME = "attribute1"
@Field final String COLUMN_VALUE = "attribute2"
@Field final String BILLING_MODEL = "attribute4"
@Field final String CHARGE_ID = "attribute5"
@Field final String KEY1_COLUMN_NAME = "key1"
@Field final String INCLUSION_FLAG_COLUMN_NAME = "attribute1"
@Field final String INC_FLAG_COLUMN_NAME = "attribute3"
@Field final String CHANNEL_LAUNCH_REGION_COLUMN = "attribute2"
@Field final String MASTER_LAUNCH_REGION_COLUMN = "attribute3"
@Field final String SKU_TYPE_COLUMN_NAME = "attribute24"
@Field final String SAPBASE_SKU_COLUMN_NAME = "attribute29"

