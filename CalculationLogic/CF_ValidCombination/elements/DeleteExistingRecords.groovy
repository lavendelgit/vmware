List skuKey = []

for (record in api.local.attributeRecords) {
    skuKey << record.SKU
}

List uniqueSkuKey = skuKey.unique()

if (api.local.pxRecords) {
    deletePL = [
            "data": [
                    "filterCriteria": [
                            "operator"    : "and",
                            "_constructor": "AdvancedCriteria",
                            "criteria"    : [
                                    [
                                            "fieldName": "name",
                                            "operator" : "equals",
                                            "value"    : "ValidCombination"
                                    ],
                                    [
                                            "fieldName": "sku",
                                            "operator" : "inSet",
                                            "value"    : uniqueSkuKey
                                    ]


                            ]
                    ]
            ]
    ]
    api.boundCall("boundcall", "delete/PX/batch", api.jsonEncode(deletePL), false)
    return
}