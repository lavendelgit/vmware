return api.findLookupTableValues(Constants.SAP_BASE_SKU_TABLE, Filter.equal(Constants.INCLUSION_FLAG_COLUMN_NAME, Constants.YES_INDICATOR))?.getAt(0)?.name
