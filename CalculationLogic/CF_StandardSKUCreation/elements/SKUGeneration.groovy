api.local.pxRecords = []
api.local.pxSAPRecords = []
api.local.pricelistRegionRecords = []
api.local.descriptionCache = []
String sku
String sapSKU
String attribute
String longDesc
int descLength
String combination
String warning
List localSAPSKUs = []
Map pXParams = [:]
Map pXSAPParams = [:]
Map longDescParams = [:]
Map masterLaunchConfig = out.MasterLaunchRegionConfig?.collectEntries { [(it.name): it.attribute6] }
Map channelLaunchConfig = out.ChannelLaunchRegionConfig?.collectEntries { [(it.name): it.attribute4] }
Integer shortDescLen = out.SKUConfigCache?.ShortDescLen as Integer
shortDescLen = shortDescLen ?: Constants.SHORT_DESC_DEFAULT_LEN
Integer longDescLen = out.SKUConfigCache?.LongDescLen as Integer
longDescLen = longDescLen ?: Constants.LONG_DESC_DEFAULT_LEN
List attributeKeys = out.SAPBaseSKUConfigCache?.keySet() as List
for (standardProduct in api.local.validStandardProducts) {
    rule = standardProduct.value
    key = standardProduct.key.split("\\" + out.AttributeDelimiterCache)?.collect { it }
    pXParams = [
            "Product"             : rule.Product == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.ProductCache[rule.Product],
            "Offer"               : rule.Offer == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.OfferCache[rule.Offer],
            "DataCenter"          : rule[Constants.DATA_CENTER] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.DataCenterCache[rule[Constants.DATA_CENTER]],
            "Base"                : rule.Base == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.BaseCache[rule.Base],
            "Metric"              : rule.Metric == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.MetricCache[rule.Metric],
            "Term"                : rule.Term == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.TermCache[rule.Term],
            "RetentionPeriod"     : rule[Constants.RETENTION_PERIOD] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.RetentionPeriodCache[rule[Constants.RETENTION_PERIOD]],
            "PSTerm"              : rule[Constants.PS_TERM] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.PSTermCache[rule[Constants.PS_TERM]],
            "PSOption"            : rule[Constants.PS_OPTION] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.PSOptionCache[rule[Constants.PS_OPTION]],
            "PaymentType"         : rule[Constants.PAYMENT_TYPE] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.PaymentTypeCache[rule[Constants.PAYMENT_TYPE]],
            "VolumeTier"          : rule[Constants.VOLUME_TIER] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.VolumeTierCache[rule[Constants.VOLUME_TIER]],
            "Segment"             : rule.Segment == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.SegmentCache[rule.Segment],
            "Version"             : rule.Version == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.VersionCache[rule.Version],
            "Promotion"           : rule.Promotion == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.PromoCache[rule.Promotion],
            "OS"                  : rule.OS == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.OSCache[rule.OS],
            "Quantity"            : rule.Quantity == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.QuantityCache[rule.Quantity],
            "PurchasingProgram"   : rule[Constants.PURCHASING_PROGRAM] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.PurchasingProgramCache[rule[Constants.PURCHASING_PROGRAM]],
            "TermUoM"             : rule.Term == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.TermUomCache[rule.Term] ?: Constants.NULL_ATTRIBUTE,
            "RetentionPeriodUoM"  : rule[Constants.RETENTION_PERIOD] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.RetentionPeriodUomCache[rule[Constants.RETENTION_PERIOD]] ?: Constants.NULL_ATTRIBUTE,
            "SupportType"         : rule[Constants.SUPPORT_TYPE] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.SupportTypeCache[rule[Constants.SUPPORT_TYPE]],
            "SupportTier"         : rule[Constants.SUPPORT_TIER] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.SupportTierCache[rule[Constants.SUPPORT_TIER]],
            "Hosting"             : rule.Hosting == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.HostingCache[rule.Hosting],
            "ProgramOption"       : rule[Constants.PROGRAM_OPTION] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : out.ProgramOptionCache[rule[Constants.PROGRAM_OPTION]],
            "ProductKey"          : rule?.Product == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Product,
            "OfferKey"            : rule?.Offer == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Offer,
            "DataCenterKey"       : rule[Constants.DATA_CENTER] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.DATA_CENTER],
            "BaseKey"             : rule?.Base == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Base,
            "MetricKey"           : rule?.Metric == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Metric,
            "TermKey"             : rule?.Term == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Term,
            "PaymentTypeKey"      : rule[Constants.PAYMENT_TYPE] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.PAYMENT_TYPE],
            "VolumeTierKey"       : rule[Constants.VOLUME_TIER] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.VOLUME_TIER],
            "SegmentKey"          : rule?.Segment == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Segment,
            "VersionKey"          : rule?.Version == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Version,
            "PromotionKey"        : rule?.Promotion == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Promotion,
            "PSTermKey"           : rule[Constants.PS_TERM] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.PS_TERM],
            "PSOptionKey"         : rule[Constants.PS_OPTION] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.PS_OPTION],
            "QuantityKey"         : rule?.Quantity == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Quantity,
            "SupportTierKey"      : rule[Constants.SUPPORT_TIER] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.SUPPORT_TIER],
            "SupportTypeKey"      : rule[Constants.SUPPORT_TYPE] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.SUPPORT_TYPE],
            "PurchasingProgramKey": rule[Constants.PURCHASING_PROGRAM] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.PURCHASING_PROGRAM],
            "OSKey"               : rule?.OS == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.OS,
            "HostingKey"          : rule?.Hosting == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule?.Hosting,
            "ProgramOptionKey"    : rule[Constants.PROGRAM_OPTION] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.PROGRAM_OPTION],
            "RetentionPeriodKey"  : rule[Constants.RETENTION_PERIOD] == Constants.PLACEHOLDER ? Constants.NULL_ATTRIBUTE : rule[Constants.RETENTION_PERIOD],
    ]
    for (attributeKey in attributeKeys) {
        pXSAPParams[attributeKey] = pXParams[attributeKey]
        pXSAPParams[attributeKey + Constants.KEY] = pXParams[attributeKey + Constants.KEY]
    }
    pXParams.SAPBaseSKU = libs.stdProductLib.SkuGenHelper.generateSAPBaseSKU(out.SAPBaseSKUConfigCache, pXSAPParams)
    sapSKU = pXParams.SAPBaseSKU
    if (!out.SAPSKUCache?.contains(sapSKU) && !localSAPSKUs?.contains(sapSKU)) {
        pXSAPParams.ShortDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.ShortDescConfig, pXSAPParams)
        longDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.LongDescConfig, pXSAPParams)
        if (longDesc) {
            longDescParams = Lib.getDescription([:], Constants.LONGDESC_ATTRIBUTE_SIZE, Constants.LONGDESC_KEY_PREFIX, Constants.DESC_BUFFER_LENGTH, longDesc)
        }
        pXSAPParams.SAPSKU = sapSKU
        localSAPSKUs << sapSKU
        api.local.pxSAPRecords << [
                Constants.SAPBASESKU_TABLE_NAME,
                pXSAPParams.SAPSKU,
                pXSAPParams.ShortDesc,
                longDescParams.LongDesc,
                longDescParams.LongDesc1,
                longDescParams.LongDesc2,
                longDescParams.LongDesc3,
                pXParams.ProductKey,
                pXParams.OfferKey,
                pXParams.BaseKey,
        ]
    }
    sku = libs.stdProductLib.SkuGenHelper.createSKU(pXParams)
    warning = ""
    if (!api.local.standardProductSKUCache?.contains(sku)) {
        shortDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.ShortDescConfig, pXParams)
        longDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.LongDescConfig, pXParams)
        shortDescTrimmed = shortDesc
        longDescTrimmed = longDesc
        if (shortDesc.length() > shortDescLen) {
            shortDescTrimmed = shortDesc?.substring(0, shortDescLen)
            warning = Constants.SHORT_DESC_TRUNCATED_MSG
        }
        if (longDesc.length() > longDescLen) {
            longDescTrimmed = longDesc?.substring(0, longDescLen)
            warning = warning?.length() > 0 ? Constants.SHORT_LONG_DESC_TRUNCATED_MSG : Constants.LONG_DESC_TRUNCATED_MSG
        }

        if (sku) {
            api.local.descriptionCache << [out.StandardDescriptionPPCache, sku, api.jsonEncode([Product: pXParams.ProductKey, Offer: pXParams.OfferKey, Base: pXParams.BaseKey, ShortDescription: shortDescTrimmed, LongDescription: longDescTrimmed, ActualShortDescription: shortDesc, ActualLongDescription: longDesc, Warning: warning])]
            pXParams.sku = sku
            api.local.pxRecords << libs.stdProductLib.SkuGenHelper.insertRecords(Constants.PRODUCT_TABLE_NAME, pXParams)
        }
    }
    for (region in rule.CurrencyRegion) {
        if (masterLaunchConfig?.keySet()?.contains(region.PriceListRegion) && channelLaunchConfig?.keySet()?.contains(region.PriceListRegion)) {
            combination = sku + "-" + region.Currency + "-" + region.PriceListRegion + "-" + region.PriceListRegion
            if (!api.local.currencyRegionCache?.contains(combination)) {
                api.local.pricelistRegionRecords << [
                        Constants.PRICELISTREGION_TABLE_NAME,
                        sku,
                        region.Currency,
                        region.PriceListRegion,
                        region.PriceListRegion,
                        pXParams.ProductKey,
                        pXParams.OfferKey,
                        pXParams.BaseKey,
                        masterLaunchConfig[region.PriceListRegion],
                        channelLaunchConfig[region.PriceListRegion],
                ]
            }
        } else if (masterLaunchConfig?.keySet()?.contains(region.PriceListRegion)) {
            combination = sku + "-" + region.Currency + "--" + region.PriceListRegion
            if (!api.local.currencyRegionCache?.contains(combination)) {
                api.local.pricelistRegionRecords << [
                        Constants.PRICELISTREGION_TABLE_NAME,
                        sku,
                        region.Currency,
                        "",
                        region.PriceListRegion,
                        pXParams.ProductKey,
                        pXParams.OfferKey,
                        pXParams.BaseKey,
                        masterLaunchConfig[region.PriceListRegion],
                        "",
                ]
            }
        } else if (channelLaunchConfig?.keySet()?.contains(region.PriceListRegion)) {
            combination = sku + "-" + region.Currency + "-" + region.PriceListRegion + "-" + region.PriceListRegion
            if (!api.local.currencyRegionCache?.contains(combination)) {
                api.local.pricelistRegionRecords << [
                        Constants.PRICELISTREGION_TABLE_NAME,
                        sku,
                        region.Currency,
                        region.PriceListRegion,
                        region.PriceListRegion,
                        pXParams.ProductKey,
                        pXParams.OfferKey,
                        pXParams.BaseKey,
                        "",
                        channelLaunchConfig[region.PriceListRegion],
                ]
            }
        }


    }
}
return
