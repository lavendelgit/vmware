if (api.local.pricelistRegionRecords) {
    Map payload = [
            "data": [
                    "data"   : api.local.pricelistRegionRecords,
                    "header" : [
                            "name",
                            "sku",
                            "attribute1",
                            "attribute2",
                            "attribute3",
                            "attribute4",
                            "attribute5",
                            "attribute6",
                            "attribute7",
                            "attribute9",
                    ],
                    "options": [
                            "detectJoinFields"    : false,
                            "maxJoinFieldsLengths": []
                    ]
            ]]
    api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
    return
}

