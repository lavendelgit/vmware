Map validTermCache = libs.stdProductLib.RulesGenHelper.cacheCommonAttributes(out.Configuration.TERM_TABLE, false)
return validTermCache?.collectEntries { [(it.key): it.value.attribute1] }