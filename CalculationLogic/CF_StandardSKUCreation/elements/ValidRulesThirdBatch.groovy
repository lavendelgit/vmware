List combinations = api.local.combinationsBatches?.getAt(2)
libs.stdProductLib.RulesGenHelper.getProductRules(combinations, api.local.invalidUniversalRuleCache, api.local.productRules, out.TermUomCache, Constants.TERM_UOM_INDEX, Constants.TERM_INDEX, Constants.TERMUOM_PLACEHOLDER)?.collect()
return