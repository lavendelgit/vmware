List generateStandardProduct(List rules, Map termUomCache, Map retentionTermUoMCache) {
    List productRules = []
    for (rule in rules) {
        productRules << [
                "attribute1" : rule[Constants.PRODUCT_RULE_INDEX],
                "attribute2" : rule[Constants.OFFER_RULE_INDEX],
                "attribute6" : rule[Constants.DATACENTER_RULE_INDEX],
                "attribute3" : rule[Constants.BASE_RULE_INDEX],
                "attribute5" : rule[Constants.METRIC_RULE_INDEX],
                "attribute9" : rule[Constants.TERM_RULE_INDEX],
                "attribute10": termUomCache[rule[Constants.TERM_RULE_INDEX]] ?: Constants.TERMUOM_NULL_PLACEHOLDER,
                "attribute11": rule[Constants.PAYMENT_TYPE_RULE_INDEX],
                "attribute15": rule[Constants.VOLUME_TIER_RULE_INDEX],
                "attribute7" : rule[Constants.SEGMENT_RULE_INDEX],
                "attribute16": rule[Constants.VERSION_RULE_INDEX],
                "attribute23": rule[Constants.PROMOTION_RULE_INDEX],
                "attribute8" : rule[Constants.QUANTITY_RULE_INDEX],
                "attribute13": rule[Constants.SUPPORT_TIER_RULE_INDEX],
                "attribute12": rule[Constants.SUPPORT_TYPE_RULE_INDEX],
                "attribute14": rule[Constants.PURCHASING_PROGRAM_RULE_INDEX],
                "attribute4" : rule[Constants.OS_RULE_INDEX],
                "attribute18": rule[Constants.CURRENCY_RULE_INDEX],
                "attribute19": rule[Constants.PRICE_LIST_REGION_RULE_INDEX],
                "attribute20": rule[Constants.PRICE_LIST_SUBREGION_RULE_INDEX],
                "attribute21": rule[Constants.HOSTING_RULE_INDEX],
                "attribute22": rule[Constants.PROGRAM_OPTION_RULE_INDEX],
                "attribute24": rule[Constants.RETENTIONPERIOD_RULE_INDEX],
                "attribute25": retentionTermUoMCache[rule[Constants.RETENTIONPERIOD_RULE_INDEX]] ?: Constants.TERMUOM_NULL_PLACEHOLDER,
                "attribute26": rule[Constants.PS_TERM_RULE_INDEX],
                "attribute27": rule[Constants.PS_OPTION_RULE_INDEX],
        ]
    }
    return productRules
}

Map getDescription(Map pXParams, Integer descAttributeSize, String keyPrefix, Integer descBufferLength, String desc) {
    descAttributeSize.times { index ->
        int descLength = desc.length()
        String key = keyPrefix + (index ?: "")
        int startIndex = index * descBufferLength
        int endIndex = ((index + 1) * descBufferLength) - 1
        if (endIndex >= descLength) endIndex = descLength - 1
        pXParams[key] = (startIndex < descLength) ? desc[startIndex..endIndex] : ""
    }
    return pXParams
}