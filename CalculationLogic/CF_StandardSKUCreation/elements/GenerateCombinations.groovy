api.local.pxRecords = []
List listAttributes = []
if (out.ProductCache) {
    listAttributes << out.ProductCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.OfferCache) {
    listAttributes << out.OfferCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.DataCenterCache) {
    listAttributes << out.DataCenterCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.BaseCache) {
    listAttributes << out.BaseCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.MetricCache) {
    listAttributes << out.MetricCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.TermCache) {
    listAttributes << out.TermCache?.keySet()
    listAttributes << Constants.TERMUOM_PLACEHOLDER
} else {
    listAttributes << Constants.PLACEHOLDER
    listAttributes << Constants.TERMUOM_PLACEHOLDER
}
if (out.PaymentTypeCache) {
    listAttributes << out.PaymentTypeCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.VolumeTierCache) {
    listAttributes << out.VolumeTierCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.SegmentCache) {
    listAttributes << out.SegmentCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.VersionCache) {
    listAttributes << out.VersionCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.PromoCache) {
    listAttributes << out.PromoCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.OSCache) {
    listAttributes << out.OSCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.QuantityCache) {
    listAttributes << out.QuantityCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.PurchasingProgramCache) {
    listAttributes << out.PurchasingProgramCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.SupportTypeCache) {
    listAttributes << out.SupportTypeCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.SupportTierCache) {
    listAttributes << out.SupportTierCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.CurrencyCache) {
    listAttributes << out.CurrencyCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.PriceListRegionCache) {
    listAttributes << out.PriceListRegionCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.SubRegionCache) {
    listAttributes << out.SubRegionCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.HostingCache) {
    listAttributes << out.HostingCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.ProgramOptionCache) {
    listAttributes << out.ProgramOptionCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.RetentionPeriodCache) {
    listAttributes << out.RetentionPeriodCache?.keySet()
    listAttributes << Constants.TERMUOM_PLACEHOLDER
} else {
    listAttributes << Constants.PLACEHOLDER
    listAttributes << Constants.PLACEHOLDER
}
if (out.PSTermCache) {
    listAttributes << out.PSTermCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
if (out.PSOptionCache) {
    listAttributes << out.PSOptionCache?.keySet()
} else {
    listAttributes << Constants.PLACEHOLDER
}
api.local.combinations = listAttributes?.combinations()
return
