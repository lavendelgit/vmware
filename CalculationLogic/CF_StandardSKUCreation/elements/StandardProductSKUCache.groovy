List filter = [
        Filter.equal(Constants.KEY_NAME, Constants.PRODUCT_TABLE_NAME),
        Filter.in(Constants.PRODUCT,out.ProductCache.keySet())
]
productSKUs = api.stream("PX50", null, [Constants.KEY_SKU], *filter)
api.local.standardProductSKUCache = productSKUs?.collect { it.sku }
productSKUs?.close()
return
