List filter = [
        Filter.equal(Constants.KEY_NAME, Constants.PRICELISTREGION_TABLE_NAME),
        Filter.in(Constants.FIELD_PRODUCT, out.ProductCache?.keySet())
]
recordStream = api.stream("PX20", null, [Constants.KEY_SKU, Constants.CURRENCY, Constants.CHANNEL_LAUNCH_REGION, Constants.MASTER_LAUNCH_REGION], *filter)
api.local.currencyRegionCache = recordStream?.collect { it.sku + "-" + it.attribute1 + "-" + it.attribute2 + "-" + it.attribute3 }
recordStream.close()
return