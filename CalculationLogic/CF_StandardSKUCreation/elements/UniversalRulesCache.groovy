def skuGenLib = libs.stdProductLib.RulesGenHelper
def universalRecords = skuGenLib.cacheInvalidUniversalRecords()
api.local.invalidUniversalRuleCache = [:]
api.local.invalidUniversalRuleCache = skuGenLib.cacheInvalidRules(universalRecords, out.UniversalRulesMetaData, out.RuleKeyIndexMapping)
api.local.invalidUniversalRuleCache = skuGenLib.populateTermUoMRules(api.local.invalidUniversalRuleCache, out.TermUoMConversionCache)
return