if (api.local.descriptionCache) {
    Map ppRecords = [data:
                             [
                                     header: [
                                             'lookupTable',
                                             'name',
                                             'attributeExtension'
                                     ],
                                     data  : api.local.descriptionCache
                             ]
    ]
    api.boundCall("boundcall", '/loaddata/JLTV', api.jsonEncode(ppRecords), true)
}
return
