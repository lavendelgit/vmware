List filter = [
        Filter.equal(Constants.KEY_NAME, Constants.SAPBASESKU_TABLE_NAME)
]
streamRecords = api.stream("PX10", null, [Constants.KEY_SKU], *filter)
sapBaseSKUCache = streamRecords?.collect { it.sku }
streamRecords?.close()
return sapBaseSKUCache