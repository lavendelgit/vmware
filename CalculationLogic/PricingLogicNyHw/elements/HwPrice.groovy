BigDecimal hwPrice = 0.0
hwPrice = out.BasePrice * out.ReplacementServiceFactor * out.SegmentAdjustment
hwPrice = hwPrice.setScale(0, BigDecimal.ROUND_UP)
return hwPrice

//@Todo Rounding to nearest 1