def sku = (api.currentItem("sku")) ?: api.product("sku")
return sku