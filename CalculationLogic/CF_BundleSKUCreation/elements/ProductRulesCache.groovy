List filter = [
        Filter.equal("name", Constants.RULES_TABLE_NAME),
        Filter.in("attribute1", out.ProductCache?.keySet()),
        Filter.in("attribute2", api.local.validOfferCache ? api.local.validOfferCache.keySet() : "-"),
        Filter.in("attribute3", api.local.validBaseCache ? api.local.validBaseCache.keySet() : "-"),
        Filter.in("attribute7", api.local.validOSCache ? api.local.validOSCache.keySet() : "-"),
        Filter.in("attribute8", api.local.validMetricCache ? api.local.validMetricCache.keySet() : "-"),
        Filter.in("attribute9", api.local.validDataCenterCache ? api.local.validDataCenterCache.keySet() : "-"),
        Filter.in("attribute10", api.local.validSegmentCache ? api.local.validSegmentCache.keySet() : "-"),
        Filter.in("attribute11", api.local.validQuantityCache ? api.local.validQuantityCache.keySet() : "-"),
        Filter.in("attribute12", api.local.validTermCache ? api.local.validTermCache.keySet() : "-"),
        Filter.in("attribute14", api.local.validPaymentTypeCache ? api.local.validPaymentTypeCache.keySet() : "-"),
        Filter.in("attribute15", api.local.validSupportTypeCache ? api.local.validSupportTypeCache.keySet() : "-"),
        Filter.in("attribute16", api.local.validSupportTierCache ? api.local.validSupportTierCache.keySet() : "-"),
        Filter.in("attribute17", api.local.validPurchasingProgramCache ? api.local.validPurchasingProgramCache.keySet() : "-"),
        Filter.in("attribute18", api.local.validVolumeTierCache ? api.local.validVolumeTierCache.keySet() : "-"),
        Filter.in("attribute19", api.local.validVersionCache ? api.local.validVersionCache.keySet() : "-"),
        Filter.in("attribute21", api.local.validPromoCache ? api.local.validPromoCache.keySet() : "-"),
        Filter.in("attribute22", api.local.validHostingCache ? api.local.validHostingCache.keySet() : "-"),
        Filter.in("attribute23", api.local.validProgramOptionCache ? api.local.validProgramOptionCache.keySet() : "-")
]
recordStream = api.stream("PX30", null, *filter)
productRules = recordStream?.collect { row -> row }
recordStream?.close()
api.local.validProductRuleCache = libs.stdProductLib.RulesGenHelper.cacheValidRules(productRules, out.ProductRulesMetaData, out.RuleKeyMapping, out.AttributeDelimiterCache)
return
