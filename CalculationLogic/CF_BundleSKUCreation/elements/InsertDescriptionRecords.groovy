if (api.local.descriptionCache) {
    def ppRecords = [data:
                             [
                                     header: [
                                             'lookupTable',
                                             'name',
                                             'attributeExtension'
                                     ],
                                     data  : api.local.descriptionCache
                             ]
    ]
    api.boundCall("boundcall", '/loaddata/JLTV', api.jsonEncode(ppRecords), true)
}
return
