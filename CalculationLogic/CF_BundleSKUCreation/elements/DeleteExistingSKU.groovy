if (api.local.pxRecords) {
    def deletePL = [
            "data": [
                    "filterCriteria": [
                            "operator"    : "and",
                            "_constructor": "AdvancedCriteria",
                            "criteria"    : [
                                    [
                                            "fieldName": "name",
                                            "operator" : "equals",
                                            "value"    : Constants.PRODUCT_TABLE_NAME
                                    ],
                                    [
                                            "fieldName": "sku",
                                            "operator" : "inSet",
                                            "value"    : api.local.skus
                                    ],
                                    [
                                            "fieldName": "attribute24",
                                            "operator" : "equals",
                                            "value"    : Constants.SKU_TYPE
                                    ]
                            ]
                    ]
            ]
    ]
    api.boundCall("boundcall", "delete/PX/batch", api.jsonEncode(deletePL), false)
    return
}