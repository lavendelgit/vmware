api.local.pxRecords = []
api.local.skus = []
api.local.descriptionCache = []
String warning
String sku
Integer shortDescLen = out.SKUConfigCache?.ShortDescLen as Integer
shortDescLen = shortDescLen ?: Constants.SHORT_DESC_DEFAULT_LEN
Integer longDescLen = out.SKUConfigCache?.LongDescLen as Integer
longDescLen = longDescLen ?: Constants.LONG_DESC_DEFAULT_LEN
for (productRule in api.local.validProductRuleCache) {
    rule = productRule.value
    key = productRule.key.split("\\" + out.AttributeDelimiterCache)?.collect { it }
    pXParams = [
            "Product"          : rule.Product == Constants.PLACEHOLDER ? null : out.ProductCache[rule.Product],
            "Offer"            : rule.Offer == Constants.PLACEHOLDER ? null : api.local.validOfferCache[rule.Offer],
            "DataCenter"       : rule[Constants.DATA_CENTER] == Constants.PLACEHOLDER ? null : api.local.validDataCenterCache[rule[Constants.DATA_CENTER]],
            "Base"             : rule.Base == Constants.PLACEHOLDER ? null : api.local.validBaseCache[rule.Base],
            "Metric"           : rule.Metric == Constants.PLACEHOLDER ? null : api.local.validMetricCache[rule.Metric],
            "Term"             : rule.Term == Constants.PLACEHOLDER ? null : api.local.validTermCache[rule.Term],
            "PaymentType"      : rule[Constants.PAYMENT_TYPE] == Constants.PLACEHOLDER ? null : api.local.validPaymentTypeCache[rule[Constants.PAYMENT_TYPE]],
            "VolumeTier"       : rule.Tier == Constants.PLACEHOLDER ? null : api.local.validVolumeTierCache[rule.Tier],
            "Segment"          : rule.Segment == Constants.PLACEHOLDER ? null : api.local.validSegmentCache[rule.Segment],
            "Version"          : rule.Version == Constants.PLACEHOLDER ? null : api.local.validVersionCache[rule.Version],
            "Promotion"        : rule.Standard == Constants.PLACEHOLDER ? null : api.local.validPromoCache[rule.Standard],
            "OS"               : rule.OS == Constants.PLACEHOLDER ? null : api.local.validOSCache[rule.OS],
            "Quantity"         : rule.Quantity == Constants.PLACEHOLDER ? null : api.local.validQuantityCache[rule.Quantity],
            "PurchasingProgram": rule[Constants.PURCHASING_PROGRAM] == Constants.PLACEHOLDER ? null : api.local.validPurchasingProgramCache[rule[Constants.PURCHASING_PROGRAM]],
            "TermUoM"          : rule.Term == Constants.PLACEHOLDER ? null : api.local.validTermUomCache[rule.Term] ?: null,
            "SupportType"      : rule[Constants.SUPPORT_TYPE] == Constants.PLACEHOLDER ? null : api.local.validSupportTypeCache[rule[Constants.SUPPORT_TYPE]],
            "SupportTier"      : rule[Constants.SUPPORT_TIER] == Constants.PLACEHOLDER ? null : api.local.validSupportTierCache[rule[Constants.SUPPORT_TIER]],
            "Hosting"          : rule.Hosting == Constants.PLACEHOLDER ? null : api.local.validHostingCache[rule.Hosting],
            "ProgramOption"    : rule[Constants.PROGRAM_OPTION] == Constants.PLACEHOLDER ? null : api.local.validProgramOptionCache[rule[Constants.PROGRAM_OPTION]],
            "ProductKey"       : key[Constants.PRODUCT_KEY_INDEX] == Constants.PLACEHOLDER ? null : key[Constants.PRODUCT_KEY_INDEX],
            "OfferKey"         : key[Constants.OFFER_KEY_INDEX] == Constants.PLACEHOLDER ? null : key[Constants.OFFER_KEY_INDEX],
            "BaseKey"          : key[Constants.BASE_KEY_INDEX] == Constants.PLACEHOLDER ? null : key[Constants.BASE_KEY_INDEX],
            "OSKey"            : key[Constants.OS_KEY] == Constants.PLACEHOLDER ? null : key[Constants.OS_KEY],
            "MetricKey"        : key[Constants.METRIC_KEY_INDEX] == Constants.PLACEHOLDER ? null : key[Constants.METRIC_KEY_INDEX],
            "DataCenterKey"    : key[Constants.DATACENTER_KEY_INDEX] == Constants.PLACEHOLDER ? null : key[Constants.DATACENTER_KEY_INDEX],
            "SegmentKey"       : key[Constants.SEGMENT_KEY] == Constants.PLACEHOLDER ? null : key[Constants.SEGMENT_KEY],
            "QuantityKey"      : key[Constants.QUANTITY_KEY] == Constants.PLACEHOLDER ? null : key[Constants.QUANTITY_KEY],
            "TermKey"          : key[Constants.TERM_INDEX] == Constants.PLACEHOLDER ? null : key[Constants.TERM_INDEX]
    ]
    paymentIndex = pXParams.TermUoM ? Constants.PAYMENT_INDEX : Constants.TERM_UOM_INDEX
    pXParams.PaymentTypeKey = key[paymentIndex] == Constants.PLACEHOLDER ? null : key[paymentIndex]
    pXParams.SupportTypeKey = key[paymentIndex + Constants.SUPPORT_TYPE_KEY] == Constants.PLACEHOLDER ? null : key[paymentIndex + Constants.SUPPORT_TYPE_KEY]
    pXParams.SupportTierKey = key[paymentIndex + Constants.SUPPORT_TIER_KEY] == Constants.PLACEHOLDER ? null : key[paymentIndex + Constants.SUPPORT_TIER_KEY]
    pXParams.PurchasingProgramKey = key[paymentIndex + Constants.PURCHASINGPROGRAM_KEY] == Constants.PLACEHOLDER ? null : key[paymentIndex + Constants.PURCHASINGPROGRAM_KEY]
    pXParams.VolumeTierKey = key[paymentIndex + Constants.TIER_KEY] == Constants.PLACEHOLDER ? null : key[paymentIndex + Constants.TIER_KEY]
    pXParams.VersionKey = key[paymentIndex + Constants.VERSION_KEY] == Constants.PLACEHOLDER ? null : key[paymentIndex + Constants.VERSION_KEY]
    pXParams.PromotionKey = key[paymentIndex + Constants.PROMOTION_KEY] == Constants.PLACEHOLDER ? null : key[paymentIndex + Constants.PROMOTION_KEY]
    pXParams.HostingKey = key[paymentIndex + Constants.HOSTING_KEY] == Constants.PLACEHOLDER ? null : key[paymentIndex + Constants.HOSTING_KEY]
    pXParams.ProgramOptionKey = key[paymentIndex + Constants.PROGRAMOPTION_KEY] == Constants.PLACEHOLDER ? null : key[paymentIndex + Constants.PROGRAMOPTION_KEY]
    sku = libs.stdProductLib.SkuGenHelper.createSKU(pXParams)
    shortDesc = libs.stdProductLib.SkuGenHelper.createDesc(api.local.ShortDescConfig, pXParams)
    longDesc = libs.stdProductLib.SkuGenHelper.createDesc(api.local.LongDescConfig, pXParams)
    shortDescTrimmed = shortDesc
    longDescTrimmed = longDesc
    if (shortDesc?.length() > shortDescLen) {
        shortDescTrimmed = shortDesc?.substring(0, shortDescLen)
        warning = Constants.SHORT_DESC_TRUNCATED_MSG
    }
    if (longDesc?.length() > longDescLen) {
        longDescTrimmed = longDesc?.substring(0, longDescLen)
        warning = warning?.length() > 0 ? Constants.SHORT_LONG_DESC_TRUNCATED_MSG : Constants.LONG_DESC_TRUNCATED_MSG
    }
    pXParams.SAPBaseSKU = out.SAPBaseSKU
    if (sku) {
        api.local.descriptionCache << [out.StandardDescriptionPPCache, sku, api.jsonEncode([Product: pXParams.ProductKey, Offer: pXParams.OfferKey, Base: pXParams.BaseKey, ShortDescription: shortDescTrimmed, LongDescription: longDescTrimmed, ActualShortDescription: shortDesc, ActualLongDescription: longDesc, Warning: warning])]
        pXParams.sku = sku
        api.local.pxRecords << libs.stdProductLib.SkuGenHelper.insertRecords(Constants.PRODUCT_TABLE_NAME, pXParams, Constants.SKU_TYPE)
    }
    api.local.skus << sku
}
return
