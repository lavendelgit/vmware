Map validTermCache = libs.stdProductLib.RulesGenHelper.cacheCommonAttributes(out.Configuration.TERM_TABLE, false)
api.local.validTermCache = validTermCache.collectEntries { [(it.key): it.value.attribute1] }
api.local.validTermUomCache = [:]
api.local.validTermUomCache = validTermCache.collectEntries { [(it.key): it.value.attribute5] }
return