//Gets all prices for each BoM Component from Product Extension table
if (api.syntaxCheck) {
    return null
}
def BoM = api.getElement("BoM")
def partNo = api.getElement("PartNumber") ?: ""
def resultMatrix = api.newMatrix("SKU", "Sub-Component", "List Price", "Quantity", "Total Price")
def BoMComponentListPriceTotal = 0
if (BoM != null) {
    resultMatrix.setColumnFormat("List Price", FieldFormatType.MONEY)
    resultMatrix.setColumnFormat("Quantity", FieldFormatType.NUMERIC)
    resultMatrix.setColumnFormat("Total Price", FieldFormatType.MONEY)
    BoM.each {
        def BoMListPrice
        def BoMListPriceTab = api.find("PX", Filter.equal("name", "ListPrice"), Filter.equal("sku", it.label))
        if (BoMListPriceTab != null) {
            BoMListPrice = BoMListPriceTab[0]?.attribute1
            BoMComponentListPriceTotal += (it.quantity * BoMListPrice)
        }
        def row = [:]
        row.put("SKU", partNo)
        row.put("Sub-Component", it.label)
        row.put("List Price", BoMListPrice)
        row.put("Quantity", it.quantity)
        row.put("Total Price", BoMListPrice * it.quantity)
        resultMatrix.addRow(row)
    }
}
api.global.BoMComponentListPriceTotal = BoMComponentListPriceTotal
return resultMatrix