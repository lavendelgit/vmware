def listPrice = api.getElement("SumOfComponents")
def bundlePrice = api.getElement("BundlePrice")
if (listPrice && bundlePrice) return 100 / listPrice * (listPrice - bundlePrice) / 100
return 0