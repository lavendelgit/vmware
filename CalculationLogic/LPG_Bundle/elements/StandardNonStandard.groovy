//api.getManualOverride 
//returns null when not overriden, otherwise returns value of the override
def discountPctOverride = api.getManualOverride("DiscountPercent")
def bundlePriceOverride = api.getManualOverride("BundlePrice")
def discount = (api.getElement("Discount") == 0) ? "Non-Discounted Bundle" : "Discounted Bundle"
def resultString = ""


if (discountPctOverride != null && bundlePriceOverride != null) resultString = " - adjusted by Discounted Percentage and by L. Price"
if (discountPctOverride == null && bundlePriceOverride != null) resultString = " - adjusted by List Price"
if (discountPctOverride != null && bundlePriceOverride == null) resultString = " - adjusted by Discounted Percentage"
return discount + resultString