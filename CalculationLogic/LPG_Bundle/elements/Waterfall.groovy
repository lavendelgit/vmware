if (api.syntaxCheck) {
    return null
}

def discount = api.getElement("Discount")
discount = discount * -1

//Add values of all components into first column
def priceMap = []
def matrix = api.getElement("BoMList").getEntries()
matrix.each {
    priceMap.add([name: it."Sub-Component", y: it."List Price"])
}

//Add additinal columns to the waterfall
priceMap.add([name: "Sum Of Component Price", isSum: true])
priceMap.add([name: "Discount", y: discount])
priceMap.add([name: "Result Price", isSum: true])

def chartDef1 = [
        title : [text: "SKU Discount"],
        series: [
                [data: priceMap]
        ]
]
return api.buildFlexChart("hc_wf_base", chartDef1)