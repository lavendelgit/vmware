import java.math.RoundingMode

return (out.SumOfComponents - out.Discount).setScale(2, RoundingMode.HALF_UP)