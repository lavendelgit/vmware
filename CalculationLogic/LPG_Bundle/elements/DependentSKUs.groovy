import java.math.RoundingMode

//Looks up for SKUs which are dependent on the current item and puts them into result matrix

if (api.syntaxCheck) {
    return null
}
Map<String, String> map = new LinkedHashMap<String, String>()

def sku = api.getElement("PartNumber")
if (sku == null) return ""

//search for dependent SKUs based on "Commercial License" field in PM
def dependentSKUs = api.find("P", Filter.like("attribute10", sku))
dependentSKUs.sort { it.attribute5 }

//If nothing found in previous step, derive by it's name
if (dependentSKUs == null) {
    //search for dependent SKUs based on string up to second dash "-"
    def lookupSKU = sku.split(/-/)[0] + "-" + sku.split(/-/)[1] + "-%"
    //adding "%" to work ok with the "Filter.like" operator
    dependentSKUs = api.find("P", Filter.like("sku", lookupSKU))
}

def resultMatrix = api.newMatrix("SKU", "SKU Sequence", "List Price")
resultMatrix.setEnableClientFilter(true) //allow user to use filters

def row = [:]
if (dependentSKUs != null) {
    resultMatrix.setColumnFormat("List Price", FieldFormatType.MONEY)
    dependentSKUs.each {

        map.clear()
        map["attribute6"] = it.attribute6
        map["attribute10"] = it.attribute10
        map["sumOfComponents"] = api.getElement("SumOfComponents")
        map["bundlePrice"] = api.getElement("ResultPrice")

        row = [:]
        row.put("SKU", it.sku)
        row.put("SKU Sequence", it.attribute5)
        row.put("List Price", libs.standardWaterfall.ListPrice.listPrice(map))
        resultMatrix.addRow(row)
    }
}

return resultMatrix