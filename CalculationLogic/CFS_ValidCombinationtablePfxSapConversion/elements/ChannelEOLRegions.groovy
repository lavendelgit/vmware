String channelEOLRegions = api.currentItem().attribute6
return api.global.pfxSapCache?.find { it.key == channelEOLRegions }?.collect { it.value }?.getAt(0) ?: channelEOLRegions
