String pfxSapKey
if (out.MasterLaunchRegions != null) {
    pfxSapKey = out.MasterLaunchRegions
    return pfxSapKey
}

if (out.ChannelLaunchRegions != null) {
    pfxSapKey = out.ChannelLaunchRegions
    return pfxSapKey
}

if (out.MasterEOLRegions != null) {
    pfxSapKey = out.MasterEOLRegions
    return pfxSapKey
}

if (out.ChannelEOLRegions != null) {
    pfxSapKey = out.ChannelEOLRegions
    return pfxSapKey
}