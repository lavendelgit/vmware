String masterLaunchRegions = api.currentItem().attribute3
return api.global.pfxSapCache?.find { it.key == masterLaunchRegions }?.collect { it.value }?.getAt(0) ?: masterLaunchRegions