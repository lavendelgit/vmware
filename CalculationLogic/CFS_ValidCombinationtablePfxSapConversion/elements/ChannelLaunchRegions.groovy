String channelLaunchRegions = api.currentItem().attribute4
return api.global.pfxSapCache?.find { it.key == channelLaunchRegions }?.collect { it.value }?.getAt(0) ?: channelLaunchRegions
