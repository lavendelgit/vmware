String masterEOLRegions = api.currentItem().attribute5
return api.global.pfxSapCache?.find { it.key == masterEOLRegions }?.collect { it.value }?.getAt(0) ?: masterEOLRegions