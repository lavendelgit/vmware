api.local.indexedCombination = []
String value
List keys = out.SAPBaseSKUConfigCache?.keySet() as List
keys?.contains(Constants.STD_TERM) ? keys << "TermUoM" : keys
List configurationIndex = out.RuleKeyIndexMapping?.findAll { keys.contains((it.key == Constants.TERM || it.key == Constants.DATACENTER ? Constants.STD + it.key : it.key)) }?.values() as List
int combinationSize = out.RuleKeyIndexMapping?.size()
List indexCombination = []
for (combination in api.local.combinations) {
    indexCombination[combinationSize] = []
    for (index in configurationIndex) {
        if (index == Constants.TERM_UOM_INDEX) {
            value = Constants.TERMUOM_NULL_PLACEHOLDER
            indexCombination.add(index, value)
        } else {
            attribute = out.RuleKeyIndexMapping?.find { it.value == index }?.key
            attribute = (attribute == Constants.TERM || attribute == Constants.DATACENTER) ? Constants.STD + attribute : attribute
            value = combination[out.SAPBaseSKUConfigCache[attribute] - Constants.STANDARD_INDEX]
            indexCombination.add(index, value)
        }
    }
    api.local.indexedCombination << indexCombination
    indexCombination = []
}
return configurationIndex
