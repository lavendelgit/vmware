List combination = []
List keys = out.SAPBaseSKUConfigCache?.keySet() as List
for (key in keys) {
    combination << api.local[key]?.keySet()
    if (key == Constants.STD_TERM) {
        combination << Constants.TERMUOM_NULL_PLACEHOLDER
    }
}
api.local.combinations = combination?.combinations()
return