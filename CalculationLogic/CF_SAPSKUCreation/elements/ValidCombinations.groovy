api.local.validRules = []
List rules = []
if (!api.local.StdTerm) // Generated based on RuleKeyIndex
{
    for (combination in api.local.splitCombinations) {
        api.local.validRules = libs.stdProductLib.RulesGenHelper.getProductRules(combination, api.local.invalidUniversalRuleCache, rules, null, null, null, null)
    }
} else {
    for (combination in api.local.splitCombinations) {
        api.local.validRules = libs.stdProductLib.RulesGenHelper.getProductRules(combination, api.local.invalidUniversalRuleCache, rules, api.local.StdTermUoM, Constants.TERM_UOM_INDEX, Constants.TERM_RULE_INDEX, Constants.TERMUOM_NULL_PLACEHOLDER)
    }
}
return