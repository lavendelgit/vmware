import groovy.transform.Field

@Field final String SAPBASESKU_TABLE_NAME = "SAPBaseSKU"
@Field final String UNIVERSAL_RULES_TABLE_NAME = "UniversalRule"
@Field final String SHORT_DESCRIPTION_TABLE_NAME = "ShortDescConfig"
@Field final String LONG_DESCRIPTION_TABLE_NAME = "LongDescConfig"
@Field final List DESCRIPTION_ATTRIBUTES = ["name", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6", "attribute7"]
@Field final Integer LONGDESC_START_INDEX = 0
@Field final Integer LONGDESC_END_INDEX = 1019
@Field final Integer LONGDESC_ATTRIBUTEONE_START = 255
@Field final Integer LONGDESC_ATTRIBUTEONE_END = 256
@Field final Integer LONGDESC_ATTRIBUTETWO_START = 510
@Field final Integer LONGDESC_ATTRIBUTETWO_END = 511
@Field final Integer LONGDESC_ATTRIBUTETHREE_START = 764
@Field final Integer LONGDESC_ATTRIBUTETHREE_END = 765
@Field final Integer TERM_RULE_INDEX = 5
@Field final String TERMUOM_NULL_PLACEHOLDER = "*"
@Field final Integer TERM_UOM_INDEX = 6
@Field final Integer STANDARD_INDEX = 1
@Field final String DATACENTER = "DataCenter"
@Field final String TERM = "Term"
@Field final String STD = "Std"
@Field final String STD_TERM = "StdTerm"
@Field final String STD_TERMUOM = "UoM"