List keys = out.SAPBaseSKUConfigCache?.keySet() as List
String attribute
for (key in keys) {
    attribute = key?.minus("Std")?.toUpperCase() + "_TABLE"
    api.local[key] = libs.stdProductLib.RulesGenHelper.cacheCommonAttributes(out.Configuration[attribute], true)
    libs.stdProductLib.RulesGenHelper.cacheAttribute(out.Configuration[attribute])
    libs.stdProductLib.RulesGenHelper.cacheMetaData(out.Configuration[attribute])
    if (key == Constants.STD_TERM) {
        api.local[key + Constants.STD_TERMUOM] = libs.stdProductLib.RulesGenHelper.cacheCommonAttributes(out.Configuration.TERM_TABLE, false)?.collectEntries { [(it.key): it.value.attribute5] }
    }
}
return

