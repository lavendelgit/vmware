int batchSize = (Math.round(api.local.indexedCombination?.size() / 5))
api.local.splitCombinations = api.local.indexedCombination?.collate(batchSize, true)
return