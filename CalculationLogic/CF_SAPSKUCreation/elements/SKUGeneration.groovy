Map pXParams = [:]
api.local.pxRecords = []
List localSAPSKUs = []
String attributeKey
int ruleIndex
int descLength
List attributeKeys = out.SAPBaseSKUConfigCache?.keySet() as List
List attributeIndex = out.SAPBaseSKUConfigCache?.values() as List
for (rule in api.local.validRules) {
    pXParams = [:]
    attributeIndex?.each { index ->
        attributeKey = attributeKeys[index - Constants.STANDARD_INDEX]
        ruleIndex = out.RuleKeyIndexMapping[attributeKey?.minus(Constants.STD)]
        pXParams[attributeKey] = api.local[attributeKey]?.getAt(rule[ruleIndex])?.attribute1
        pXParams[attributeKey + "Key"] = rule[ruleIndex]
    }
    sapSKU = libs.stdProductLib.SkuGenHelper.generateSAPBaseSKU(out.SAPBaseSKUConfigCache, pXParams)
    if (!api.local.sapBaseSKUCache?.contains(sapSKU) && !localSAPSKUs?.contains(sapSKU)) {
        pXParams.ShortDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.ShortDescConfig, pXParams)
        longDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.LongDescConfig, pXParams)
        if (longDesc) {
            descLength = longDesc?.length()
            if (descLength < Constants.LONGDESC_END_INDEX && descLength > Constants.LONGDESC_ATTRIBUTETHREE_START) {
                pXParams.LongDesc = longDesc.substring(Constants.LONGDESC_START_INDEX, Constants.LONGDESC_ATTRIBUTEONE_START)
                pXParams.LongDesc1 = longDesc.substring(Constants.LONGDESC_ATTRIBUTEONE_START, Constants.LONGDESC_ATTRIBUTETWO_START)
                pXParams.LongDesc2 = longDesc.substring(Constants.LONGDESC_ATTRIBUTETWO_START, Constants.LONGDESC_ATTRIBUTETHREE_START)
                pXParams.LongDesc3 = longDesc.substring(Constants.LONGDESC_ATTRIBUTETHREE_START, descLength)
            } else {
                if (descLength < Constants.LONGDESC_ATTRIBUTETHREE_END && descLength > Constants.LONGDESC_ATTRIBUTETWO_START) {
                    pXParams.LongDesc = longDesc.substring(Constants.LONGDESC_START_INDEX, Constants.LONGDESC_ATTRIBUTEONE_START)
                    pXParams.LongDesc1 = longDesc.substring(Constants.LONGDESC_ATTRIBUTEONE_START, Constants.LONGDESC_ATTRIBUTETWO_START)
                    pXParams.LongDesc2 = longDesc.substring(Constants.LONGDESC_ATTRIBUTETWO_START, descLength)
                    pXParams.LongDesc3 = ""
                } else {
                    if (descLength < Constants.LONGDESC_ATTRIBUTETWO_END && descLength > Constants.LONGDESC_ATTRIBUTEONE_START) {

                        pXParams.LongDesc = longDesc.substring(Constants.LONGDESC_START_INDEX, Constants.LONGDESC_ATTRIBUTEONE_START)
                        pXParams.LongDesc1 = longDesc.substring(Constants.LONGDESC_ATTRIBUTEONE_START, descLength)
                        pXParams.LongDesc2 = ""
                        pXParams.LongDesc3 = ""
                    } else {
                        if (descLength < Constants.LONGDESC_ATTRIBUTEONE_END) {
                            pXParams.LongDesc = longDesc
                            pXParams.LongDesc1 = ""
                            pXParams.LongDesc2 = ""
                            pXParams.LongDesc3 = ""
                        }
                    }
                }
            }
        }
        pXParams.SAPSKU = sapSKU
        localSAPSKUs << sapSKU
        api.local.pxRecords << Lib.insertRecords(Constants.SAPBASESKU_TABLE_NAME, pXParams)
    }
}
return