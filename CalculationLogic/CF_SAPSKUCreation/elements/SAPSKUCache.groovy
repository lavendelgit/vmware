List filter = [
        Filter.equal("name", Constants.SAPBASESKU_TABLE_NAME)
]
streamRecords = api.stream("PX10", null, ["sku"], *filter)
api.local.sapBaseSKUCache = streamRecords?.collect { it.sku }
streamRecords?.close()
return
