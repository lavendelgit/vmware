def BU = out.BU
def subserviceMap


def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BUSubServices").id),
        Filter.equal("key1", "PV"),
        Filter.equal("attribute2", "Yes")
]

def buSubServiceList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2"], *filter)

subserviceMap = buSubServiceList.collectEntries {
    [(it.key2): it]
}

return subserviceMap