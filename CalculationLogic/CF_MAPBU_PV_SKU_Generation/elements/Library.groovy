def createMAPBUSKU(bu, service, quantity, subService, paymentTerm, licenseType, license, productType) {
    bu += "-"
    if (service) {
        bu += service
        bu += "-"
    }
    if (service != "TDS") {
        bu += quantity
        bu += subService
        bu += "-"
    }
    bu += paymentTerm
    bu += productType
    bu += "-"
    bu += licenseType
    bu += "-"
    bu += license
    return bu
}


def addOrUpdateMAPBUPXTable(sku, Service, Quantity, SubService, PaymentTerm, License, ProductType, ShortDesc, LongDesc, baseSKU, termYears, segment, metric) {

    def mapbuRecord = [
            name       : "MAPBUProducts",
            sku        : sku,
            attribute11: ShortDesc,
            attribute12: LongDesc,
            attribute1 : Service,

            attribute3 : Quantity,
            attribute4 : SubService,
            attribute5 : PaymentTerm,


            attribute8 : License,

            attribute10: ProductType,
            attribute13: termYears,
            attribute14: segment,
            attribute15: metric,
            attribute16: baseSKU

    ]
    if (mapbuRecord) api.addOrUpdate("PX20", mapbuRecord)
}


def conditionalShortDesc(shortDesc, operator, value, desc, conditionName) {
    //api.trace("long desc","",shortDesc + "~"+operator+"~"+value+"~"+desc+"~"+conditionName)
    switch (operator) {
        case "=":

            if (conditionName.equals(value)) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<=":
            if (conditionName <= value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case ">":
            if (conditionName > value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break
        case "<>":
            if (conditionName != value) {
                if (shortDesc) {
                    shortDesc += desc
                } else {
                    shortDesc = desc
                }
            }
            break

    }
    return shortDesc
}

def createShortDesc(ruleList, service, subServiceDesc, quantity, productTypeDesc, subscriptionYears, licenseClass, licenseType) {
    def shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":
                //shortDesc.append(rule.attribute6)
                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break

            case "Conditional":
                if (rule.attribute3.equals("Service")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, service)
                }
                if (rule.attribute3.equals("LicenseClass")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, licenseClass)
                }
                if (rule.attribute3.equals("LicenseType")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, licenseType)
                }

                break
            case "Replace":
                if ((rule.attribute2.equals("Quantity")) && (rule.attribute3.equals("Service"))) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, quantity, service)
                }
                if ((rule.attribute2.equals("SubServiceDesc")) && (rule.attribute3.equals("Service"))) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, subServiceDesc, service)
                }
                if (rule.attribute2.equals("ProductTypeDesc")) {
                    if (shortDesc) {
                        shortDesc += productTypeDesc
                    } else {
                        shortDesc = productTypeDesc
                    }
                }
                if (rule.attribute2.equals("SubscriptionYears")) {
                    if (shortDesc) {
                        shortDesc += subscriptionYears
                    } else {
                        shortDesc = subscriptionYears
                    }
                }
                break

        }
    }
    return shortDesc
}


def createLongDesc(ruleList, service, subServiceDesc, quantity, productTypeDesc, licenseClass) {
    def shortDesc
    ruleList.each { rule ->
        switch (rule.attribute1) {

            case "Plain Text":

                if (shortDesc) {
                    shortDesc += rule.attribute6
                } else {
                    shortDesc = rule.attribute6
                }
                break

            case "Conditional":

                if (rule.attribute3.equals("Service")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, service)
                }
                if (rule.attribute3.equals("LicenseClass")) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, rule.attribute6, licenseClass)
                }

                break
            case "Replace":
                if ((rule.attribute2.equals("Quantity")) && (rule.attribute3.equals("Service"))) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, quantity, service)
                }
                if ((rule.attribute2.equals("SubServiceDesc")) && (rule.attribute3.equals("Service"))) {
                    shortDesc = conditionalShortDesc(shortDesc, rule.attribute4, rule.attribute5, subServiceDesc, service)
                }
                if (rule.attribute2.equals("ProductTypeDesc")) {
                    if (shortDesc) {
                        shortDesc += productTypeDesc
                    } else {
                        shortDesc = productTypeDesc
                    }
                }

                break

        }
    }
    return shortDesc
}

