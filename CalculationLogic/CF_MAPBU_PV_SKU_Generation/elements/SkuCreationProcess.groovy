if (api.isSyntaxCheck()) return

def bu = out.BU

def sku
def shortDesc
def longDesc
def subServiceDesc
def productTypeDesc
def subscriptionYears

def ServiceMap
def SubServiceMap
def baseSKU
def segment


def shortDescRuleList = api.local.ShortDesc
def longDescRuleList = api.local.LongDesc

ServiceMap = out.BUServiceMap
def ServiceList = (ServiceMap) ? ServiceMap.keySet() : []
def licenseMap = out.BULicenseMap
def licenseList = (licenseMap) ? licenseMap.keySet() : []
def paymenttermMap = out.BUPaymentTermMap
def paymenttermList = (paymenttermMap) ? paymenttermMap.keySet() : []
def productTypeMap = out.BUProductTypeMap
def productTypeList = (productTypeMap) ? productTypeMap.keySet() : []
def quantityMap = out.BUQuantityMap
def quantityList = (quantityMap) ? quantityMap.keySet() : []
SubServiceMap = out.BUSubServiceMap
def SubServiceList = (SubServiceMap) ? SubServiceMap.keySet() : []
def licenseTypeMap = out.BULicenseType
def licenseTypeList = (licenseTypeMap) ? licenseTypeMap.keySet() : []


ServiceList.each { service ->
    quantityList.each { quantity ->
        SubServiceList.each { subService ->
            paymenttermList.each { paymentTerm ->
                productTypeList.each { productType ->
                    licenseTypeList.each { licenseType ->

                        licenseList.each { license ->


                            baseSKU = ServiceMap[service].attribute2
                            if (subService) {
                                subServiceDesc = SubServiceMap[subService].attribute1
                            }
                            if (productType) {
                                productTypeDesc = productTypeMap[productType].attribute1
                            }
                            subscriptionYears = paymenttermMap[paymentTerm].attribute1
                            segment = licenseMap[license].attribute1


                            sku = Library.createMAPBUSKU(bu, service, quantity, subService, subscriptionYears, licenseType, license, productType)
                            shortDesc = Library.createShortDesc(shortDescRuleList, service, subServiceDesc, quantity, productTypeDesc, subscriptionYears, license, licenseType)
                            longDesc = Library.createLongDesc(longDescRuleList, service, subServiceDesc, quantity, productTypeDesc, license)


                            if (sku) Library.addOrUpdateMAPBUPXTable(sku, service, quantity, subService, paymentTerm, license, productType, shortDesc, longDesc, baseSKU, subscriptionYears, segment, subServiceDesc)


                        }
                    }
                }
            }
        }
    }
}




