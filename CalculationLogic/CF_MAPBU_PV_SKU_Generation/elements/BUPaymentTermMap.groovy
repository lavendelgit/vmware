def BU = out.BU
def paymenttermMap


def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BUPaymentTerm").id),
        Filter.equal("key1", "PV"),
        Filter.equal("attribute2", "Yes")
]

def buPaymentTermList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2"], *filter)

paymenttermMap = buPaymentTermList.collectEntries {
    [(it.key2): it]
}


return paymenttermMap