def BU = out.BU
def quantityMap


def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BUQuantity").id),
        Filter.equal("key1", "PV"),
        Filter.equal("attribute1", "Yes")
]

def buQuantityList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1"], *filter)

quantityMap = buQuantityList.collectEntries {
    [(it.key2): it]
}
return quantityMap