def BU = out.BU
def licenseTypeMap


def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("BULicenseType").id),
        Filter.equal("key1", "PV"),
        Filter.equal("attribute2", "Yes")
]

def buLicenseList = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2"], *filter)

licenseTypeMap = buLicenseList.collectEntries {
    [(it.key2): it]
}


return licenseTypeMap