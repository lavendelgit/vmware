def filter = [
        Filter.equal("lookupTable.id", api.findLookupTable("MAPBULongDesc").id),
        Filter.equal("key1", "PV"),
        Filter.equal("attribute7", "Yes")
]

api.local.LongDesc = api.find("MLTV2", 0, api.getMaxFindResultsLimit(), "key2",
        ["key1", "key2", "attribute1", "attribute2", "attribute3", "attribute4", "attribute5", "attribute6", "attribute7"], *filter)


return