if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def threeYearDiscount = api.getElement("threeYearDiscount")
def oneYearCommercialSnS = api.getElement("OneYearCommercialBasicProductionSnSPrice")
if (ext != null) {
    if (ext == "3G-SSS-C" || ext == "3P-SSS-C" ||
            ext == "3G-SSS-F" || ext == "3P-SSS-F" ||
            ext == "3P-SSS-A") {
        if (threeYearDiscount != null && oneYearCommercialSnS != null) {
            threeYearDiscount = 1 - threeYearDiscount
            return 3 * threeYearDiscount * oneYearCommercialSnS
        }
    }
}