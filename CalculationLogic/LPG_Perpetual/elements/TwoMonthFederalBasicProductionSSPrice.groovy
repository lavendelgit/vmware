if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def twoMonthCommercialSS = api.getElement("TwoMonthCommercialBasicProductionSSPrice")
if (ext != null) {
    if (ext == "2M-GSUB-F" || ext == "2M-GSUP-F" || ext == "2M-PSUB-F" || ext == "2M-PSUP-F") {
        if (twoMonthCommercialSS != null) {
            return twoMonthCommercialSS
        }
    }
}