def LC = api.getElement("LCFactor")

def currencyFilter = Filter.equal("name", LC)
def fxRate = api.findLookupTableValues("LocalCurrencyExchangeRates", currencyFilter).attribute5[0]

//api.trace(fxRate)

if (fxRate != null) {
    return new BigDecimal(fxRate)
} else {
    return null
}