if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def oneYearCommercialSnS = api.getElement("OneYearCommercialBasicProductionSnSPrice")
if (ext != null) {
    if (ext == "2M-GSSS-C" || ext == "2M-PSSS-C" ||
            ext == "2M-GSSS-F" || ext == "2M-PSSS-F" ||
            ext == "2M-PSSS-A") {
        if (oneYearCommercialSnS != null) {
            return oneYearCommercialSnS / 6
        }
    }
}