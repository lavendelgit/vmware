if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def threeYearDiscount = api.getElement("threeYearDiscount")
def oneYearAcademicSnS = api.getElement("OneYearAcademicBasicSnSPrice")
if (ext != null) {
    if (ext == "3G-SSS-A") {
        if (threeYearDiscount != null && oneYearAcademicSnS != null) {
            threeYearDiscount = 1 - threeYearDiscount
            return 3 * threeYearDiscount * oneYearAcademicSnS
        }
    }
}