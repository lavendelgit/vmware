if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def academicPrice = api.getElement("AcademicLicense")
def oneYearAcademicSnS = api.getElement("OneYearAcademicBasicSnSPercent")
if (ext != null) {
    if (ext == "G-SSS-A" || ext == "GSUB-A" || ext == "GSUP-A" ||
            ext == "3G-SSS-A" || ext == "3GSUB-A" || ext == "3GSUP-A" ||
            ext == "2M-GSSS-A" || ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
        if (academicPrice != null && oneYearAcademicSnS != null) {
            def result = academicPrice * oneYearAcademicSnS
            result = Library.round(result, 0)
            return result
        }
    }
}