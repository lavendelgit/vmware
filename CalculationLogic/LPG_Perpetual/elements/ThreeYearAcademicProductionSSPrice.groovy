if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def price = api.getElement("ThreeYearCommercialBasicProductionSupportSubscriptionPrice")
if (ext == "3PSUB-A" || ext == "3PSUP-A") {
    return price
}