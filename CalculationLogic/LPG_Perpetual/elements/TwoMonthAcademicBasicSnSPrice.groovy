if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def oneYearAcademicSnS = api.getElement("OneYearAcademicBasicSnSPrice")
if (ext != null) {
    if (ext == "2M-GSSS-A") {
        if (oneYearAcademicSnS != null) {
            return oneYearAcademicSnS / 6
        }
    }
}