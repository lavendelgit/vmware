if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def price = api.getElement("ThreeYearCommercialBasicProductionSnSPrice")
if (ext == "3P-SSS-A") {
    return price
}