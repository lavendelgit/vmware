if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def threeYearDiscount = api.getElement("threeYearDiscount")
def oneYearAcademicSS = api.getElement("OneYearAcademicBasicSupportSubscriptionPrice")
if (ext != null) {
    if (ext == "3GSUB-A" || ext == "3GSUP-A") {
        if (threeYearDiscount != null && oneYearAcademicSS != null) {
            threeYearDiscount = 1 - threeYearDiscount
            return 3 * threeYearDiscount * oneYearAcademicSS
        }
    }
}