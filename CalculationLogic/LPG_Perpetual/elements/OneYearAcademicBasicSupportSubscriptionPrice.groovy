if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def oneYearAcademicSnSPercent = api.getElement("OneYearAcademicBasicSnSPercent")
def oneYearAcademicSnSPrice = api.getElement("OneYearAcademicBasicSnSPrice")
def oneYearAcademicSSPercent = api.getElement("OneYearAcademicBasicSupportSubscriptionPercent")
if (ext != null) {
    if (ext == "GSUB-A" || ext == "GSUP-A" ||
            ext == "3GSUB-A" || ext == "3GSUP-A" ||
            ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
        if (oneYearAcademicSnSPrice != null && oneYearAcademicSSPercent != null && oneYearAcademicSSPercent != null) {
            //def perc = oneYearAcademicSSPercent / oneYearAcademicSnSPercent
            def result = (oneYearAcademicSnSPrice * oneYearAcademicSSPercent) / oneYearAcademicSnSPercent
            result = result?.toString()
            return Library.truncate(result)
        }
    }
}