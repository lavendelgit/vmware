if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def oneYearAcademicSS = api.getElement("OneYearAcademicBasicSupportSubscriptionPrice")
if (ext != null) {
    if (ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
        if (oneYearAcademicSS != null) {
            return oneYearAcademicSS / 6
        }
    }
}