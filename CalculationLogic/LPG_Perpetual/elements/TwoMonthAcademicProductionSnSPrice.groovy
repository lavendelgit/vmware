if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def price = api.getElement("TwoMonthCommercialBasicProductionSnSPrice")
if (ext == "2M-PSSS-A") {
    return price
}