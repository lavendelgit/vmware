def listPrice = api.getElement("ListPrice")
def rate = api.getElement("ConversionFactorGBP")
def LC = api.getElement("LCFactor")

if (!LC || !rate || !listPrice) return ""

def LCprice = listPrice * rate
def threshHold = 50
def roundToThresholdMet = 1
def roundToThresholdNotMet = 0.1
def substract = 0.05

if (api.getElement("ProductType") == "Perpetual- SnS") {
    return LCprice.setScale(2, BigDecimal.ROUND_HALF_UP)
} else {
    if (LC == "LC-77") {
        return libs.vmwareUtil.RoundingLogicDesktopV2.applyRoundingLogic(LCprice, threshHold, substract, roundToThresholdMet, roundToThresholdNotMet)?.toBigDecimal()
    } else {
        return libs.vmwareUtil.RoundingLogicV2.applyRoundingLogic(LCprice)?.toBigDecimal()
    }
}  