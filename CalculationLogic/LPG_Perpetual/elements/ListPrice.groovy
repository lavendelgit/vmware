if (api.syntaxCheck) {
    return null
}
import java.math.RoundingMode

def ext = api.getElement("Extension")
def discountedCommercialLicense = api.getElement("DiscountedCommercialLicense")
def oneYearCommercialBPSnSPrice = api.getElement("OneYearCommercialBasicProductionSnSPrice")
def oneYearCommercialBPSSPrice = api.getElement("OneYearCommercialBasicProductionSupportSubscriptionPrice")
def acadLicense = api.getElement("AcademicLicense")
def fedLicense = api.getElement("FederalLicense")
def threeYearCommercialBPSnSPrice = api.getElement("ThreeYearCommercialBasicProductionSnSPrice")
def threeYearCommercialBPSSPrice = api.getElement("ThreeYearCommercialBasicProductionSupportSubscriptionPrice")
def twoMonthBPSnSPrice = api.getElement("TwoMonthCommercialBasicProductionSnSPrice")
def twoMonthBPSSPrice = api.getElement("TwoMonthCommercialBasicProductionSSPrice")
def oneYearFederalBPSnS = api.getElement("OneYearFederalBasicProductionSnSPrice")
def oneYearFederalBPSS = api.getElement("OneYearFederalBasicProductionSupportSubscriptionPrice")
def threeYearFederalBPSnS = api.getElement("ThreeYearFederalBasicProductionSnSPrice")
def threeYearFederalBPSS = api.getElement("ThreeYearFederalBasicProductionSupportSubscriptionPrice")
def twoMonthFederalBPSnS = api.getElement("TwoMonthFederalBasicProductionSnSPrice")
def twoMonthFederalBPSS = api.getElement("TwoMonthFederalBasicProductionSSPrice")
def discountedFederalLicense = api.getElement("DiscountedFederalLicense")
def result
def oneYearAcadBasicSnS = api.getElement("OneYearAcademicBasicSnSPrice")
def oneYearAcadBasicSS = api.getElement("OneYearAcademicBasicSupportSubscriptionPrice")
def threeYearAcadBasicSnS = api.getElement("ThreeYearAcademicBasicSnSPrice")
def threeYearAcadBasicSS = api.getElement("ThreeYearAcademicBasicSupportSubscriptionPrice")
def twoMonthAcadBasicSnS = api.getElement("TwoMonthAcademicBasicSnSPrice")
def twoMonthAcadBasicSS = api.getElement("TwoMonthAcademicBasicSupportSubscriptionPrice")
def oneYearAcadProductionSnS = api.getElement("OneYearAcademicProductionSnSPrice")
def oneYearAcadProductionSS = api.getElement("OneYearAcademicProductionSSPrice")
def threeYearAcadProductionSnS = api.getElement("ThreeYearAcademicProductionSnSPrice")
def threeYearAcadProductionSS = api.getElement("ThreeYearAcademicProductionSSPrice")
def twoMonthAcadProductionSnS = api.getElement("TwoMonthAcademicProductionSnSPrice")
def twoMonthAcadProductionSS = api.getElement("TwoMonthAcademicProductionSSPrice")
def fiveYearCommercial = api.getElement("FiveYearCommercialProductionSnSPrice")
def fiveYearFederal = api.getElement("FiveYearFederalProductionSnSPrice")
def fiveYearAcademic = api.getElement("FiveYearAcademicProductionSnSPrice")
def isMultiYearSKU = out.isMultiYearSKU

if (ext != null) {
    if (ext == "C" || ext == "C-L1" ||
            ext == "C-L2" || ext == "C-L3" || ext == "C-L4" || ext == "C-T1" ||
            ext == "C-T2" || ext == "C-T3" || ext == "C-L7" || ext == "C-L8" ||
            ext == "C-L9" || ext == "C-L10") {
        if (discountedCommercialLicense != null) {
            result = discountedCommercialLicense
        }
    }
    if (ext == "F-L1" || ext == "F-L2" || ext == "F-L3" || ext == "F-L4") {
        if (discountedFederalLicense != null) {
            result = discountedFederalLicense
        }
    }
    if (ext == "G-SSS-C" || ext == "P-SSS-C") {
        if (oneYearCommercialBPSnSPrice != null) {
            result = oneYearCommercialBPSnSPrice
        }
    }
    if (ext == "GSUB-C" || ext == "GSUP-C" || ext == "PSUB-C" || ext == "PSUP-C") {
        if (oneYearCommercialBPSSPrice != null) {
            result = oneYearCommercialBPSSPrice
        }
    }
    if (ext == "A") {
        if (acadLicense != null) {
            result = acadLicense
        }
    }
    if (ext == "F") {
        if (fedLicense != null) {
            result = fedLicense
        }
    }
    if (ext == "3G-SSS-C" || ext == "3P-SSS-C") {
        if (threeYearCommercialBPSnSPrice != null) {
            result = threeYearCommercialBPSnSPrice
        }
    }
    if (ext == "3GSUB-C" || ext == "3GSUP-C" || ext == "3PSUB-C" || ext == "3PSUP-C") {
        if (threeYearCommercialBPSSPrice != null) {
            result = threeYearCommercialBPSSPrice
        }
    }
    if (ext == "2M-GSSS-C" || ext == "2M-PSSS-C") {
        if (twoMonthBPSnSPrice != null) {
            result = twoMonthBPSnSPrice
        }
    }
    if (ext == "2M-GSUB-C" || ext == "2M-GSUP-C" || ext == "2M-PSUB-C" || ext == "2M-PSUP-C") {
        if (twoMonthBPSSPrice != null) {
            result = twoMonthBPSSPrice
        }
    }


    if (ext == "G-SSS-F" || ext == "P-SSS-F") {
        if (oneYearFederalBPSnS != null) {
            result = oneYearFederalBPSnS
        }
    }
    if (ext == "GSUB-F" || ext == "GSUP-F" || ext == "PSUB-F" || ext == "PSUP-F") {
        if (oneYearFederalBPSS != null) {
            result = oneYearFederalBPSS
        }
    }
    if (ext == "3G-SSS-F" || ext == "3P-SSS-F") {
        if (threeYearFederalBPSnS != null) {
            result = threeYearFederalBPSnS
        }
    }
    if (ext == "3GSUB-F" || ext == "3GSUP-F" || ext == "3PSUB-F" || ext == "3PSUP-F") {
        if (threeYearFederalBPSS != null) {
            result = threeYearFederalBPSS
        }
    }
    if (ext == "2M-GSSS-F" || ext == "2M-PSSS-F") {
        if (twoMonthFederalBPSnS != null) {
            result = twoMonthFederalBPSnS
        }
    }
    if (ext == "2M-GSUB-F" || ext == "2M-GSUP-F" || ext == "2M-PSUB-F" || ext == "2M-PSUP-F") {
        if (twoMonthFederalBPSS != null) {
            result = twoMonthFederalBPSS
        }
    }
}

if (ext == "G-SSS-A") {
    if (oneYearAcadBasicSnS != null) {
        result = oneYearAcadBasicSnS
    }
}
if (ext == "GSUB-A" || ext == "GSUP-A") {
    if (oneYearAcadBasicSS != null) {
        result = oneYearAcadBasicSS
    }
}
if (ext == "3G-SSS-A") {
    if (threeYearAcadBasicSnS != null) {
        result = threeYearAcadBasicSnS
    }
}
if (ext == "3GSUB-A" || ext == "3GSUP-A") {
    if (threeYearAcadBasicSS != null) {
        result = threeYearAcadBasicSS
    }
}
if (ext == "2M-GSSS-A") {
    if (twoMonthAcadBasicSnS != null) {
        result = twoMonthAcadBasicSnS
    }
}
if (ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
    if (twoMonthAcadBasicSS != null) {
        result = twoMonthAcadBasicSS
    }
}
if (ext == "G-SSS-A") {
    result = oneYearAcadBasicSnS
}
if (ext == "GSUB-A" || ext == "GSUP-A") {
    result = oneYearAcadBasicSS
}
if (ext == "P-SSS-A") {
    result = oneYearAcadProductionSnS
}
if (ext == "PSUB-A" || ext == "PSUP-A") {
    result = oneYearAcadProductionSS
}
if (ext == "3G-SSS-A") {
    result = threeYearAcadBasicSnS
}
if (ext == "3GSUB-A" || ext == "3GSUP-A") {
    result = threeYearAcadBasicSS
}
if (ext == "3P-SSS-A") {
    result = threeYearAcadProductionSnS
}
if (ext == "3PSUB-A" || ext == "3PSUP-A") {
    result = threeYearAcadProductionSS
}
if (ext == "2M-GSSS-A") {
    result = twoMonthAcadBasicSnS
}
if (ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
    result = twoMonthAcadBasicSS
}
if (ext == "2M-PSSS-A") {
    result = twoMonthAcadProductionSnS
}
if (ext == "2M-PSUB-A" || ext == "2M-PSUP-A") {
    result = twoMonthAcadProductionSS
}
if (ext == "5P-SSS-C") {
    result = fiveYearCommercial
}
if (ext == "5P-SSS-A") {
    result = fiveYearAcademic
}
if (ext == "5P-SSS-F") {
    result = fiveYearFederal
}

api.trace(result)


if (result != null && (ext == "G-SSS-C" || ext == "P-SSS-C" || ext == "A" || ext == "G-SSS-A")) {
    result = Library.round(result, 0)
} else if (isMultiYearSKU && result != null) {
    result = Library.round(result, 2)
} else {
    if (result != null) {
        result = result?.toString()
        return Library.truncate(result)
    }
}
return result