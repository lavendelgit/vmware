if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def oneYearCommercialSnS = api.getElement("OneYearCommercialBasicProductionSnSPrice")
if (ext != null) {
    if (ext == "G-SSS-F" || ext == "P-SSS-F") {
        if (oneYearCommercialSnS != null) {
            return oneYearCommercialSnS
        }
    }
}