if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def federalLPrice = api.getElement("FederalLicense")
def licenseAdjustment = api.getElement("FederalLicenseDiscount")
if (ext != null) {
    if (ext == "F-L1" || ext == "F-L2" || ext == "F-L3" || ext == "F-L4") {
        if (federalLPrice != null && licenseAdjustment != null) {
            licenseAdjustment = 1 - licenseAdjustment
            return federalLPrice * licenseAdjustment
        }
    }
}