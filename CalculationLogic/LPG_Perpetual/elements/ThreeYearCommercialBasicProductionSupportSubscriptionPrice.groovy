if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def threeYearDiscount = api.getElement("threeYearDiscount")
def oneYearCommercialSS = api.getElement("OneYearCommercialBasicProductionSupportSubscriptionPrice")
if (ext != null) {
    if (ext == "3GSUB-C" || ext == "3GSUP-C" || ext == "3PSUB-C" || ext == "3PSUP-C" ||
            ext == "3GSUB-F" || ext == "3GSUP-F" || ext == "3PSUB-F" || ext == "3PSUP-F" ||
            ext == "3PSUB-A" || ext == "3PSUP-A") {
        if (threeYearDiscount != null && oneYearCommercialSS != null) {
            threeYearDiscount = 1 - threeYearDiscount
            return 3 * threeYearDiscount * oneYearCommercialSS
        }
    }
}