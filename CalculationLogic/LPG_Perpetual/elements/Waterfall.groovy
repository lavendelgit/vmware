if (api.isSyntaxCheck()) return
def extension = api.getElement("Extension")?.toString()
def commercialL = api.getElement("CommercialLicense")?.toString()
def partNo = api.getElement("PartNumber")?.toString()
def title = "Waterfall: " + partNo + " in USD"
def commercialLP = api.getElement("CommercialLicensePrice")
def oneYearCommercialBPSnSP = api.getElement("OneYearCommercialBasicProductionSnSPrice")
def oneYearCommercialBPSSP = api.getElement("OneYearCommercialBasicProductionSupportSubscriptionPrice")
def listP = api.getElement("ListPrice")
def federal = api.getElement("FederalLicense")
def discountedFederal = api.getElement("DiscountedFederalLicense")

if (listP != null && (extension == "C-L1" || extension == "C-L2" || extension == "C-L3" ||
        extension == "C-L4" || extension == "C-L5" || extension == "C-L6" ||
        extension == "C-L7" || extension == "C-L8" || extension == "C-L9" ||
        extension == "C-L10" || extension == "C-T1" || extension == "C-T2" ||
        extension == "C-T3")) {
    def discount = 0
    api.trace("listP", null, listP)
    api.trace("commercialLP", null, commercialLP)
    if (listP != null && commercialLP != null) {
        discount = listP - commercialLP
        api.trace("discount", null, discount)
    }
    def chartDef1 = [
            title  : [
                    text: title
            ],
            tooltip: [
                    pointFormat: '$ <b>{point.y:,.2f}</b>'
            ],
            series : [
                    data: [
                            [
                                    name: commercialL,
                                    y   : commercialLP
                            ], [
                                    name: "Commercial License Discount",
                                    y   : discount
                            ], [
                                    name   : partNo,
                                    "isSum": true
                            ]
                    ]
            ]
    ]
    return api.buildFlexChart("hc_wf_base", chartDef1)
}


def commercialSnSDiscount = 0
if (commercialLP != null && oneYearCommercialBPSnSP != null) {
    commercialSnSDiscount = oneYearCommercialBPSnSP - commercialLP
}
def commercialSSDiscount = 0
if (oneYearCommercialBPSSP != null && oneYearCommercialBPSnSP != null) {
    commercialSSDiscount = oneYearCommercialBPSSP - oneYearCommercialBPSnSP
}
def commercialBPSnSDLable
def commercialBPSSDLable
def commercialBPSnSLable
if (listP != null && (extension == "G-SSS-C" || extension == "GSUB-C" || extension == "GSUP-C" ||
        extension == "P-SSS-C" || extension == "PSUB-C" || extension == "PSUP-C" ||
        extension == "P-SSS-A" || extension == "PSUB-A" || extension == "PSUP-A")) {

    if (extension == "G-SSS-C" || extension == "P-SSS-C" || extension == "P-SSS-A") {
        if (extension == "G-SSS-C") {
            commercialBPSnSDLable = "1 Year Commercial Basic SnS Discount"
        } else if (extension == "P-SSS-C") {
            commercialBPSnSDLable = "1 Year Commercial Production SnS Discount"
        } else if (extension == "P-SSS-A") {
            commercialBPSnSDLable = "1 Year Academic Production SnS Discount"
        }
        def chartDef2 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: commercialBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef2)
    }
    if (extension == "GSUB-C" || extension == "GSUP-C" ||
            extension == "PSUB-C" || extension == "PSUP-C" ||
            extension == "PSUB-A" || extension == "PSUP-A") {
        if (extension == "GSUB-C") {
            commercialBPSnSDLable = "1 Year Commercial Basic SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Basic Subscription Discount"
            commercialBPSnSLable = "1 Year Commercial Basic SnS SKU"
        }
        if (extension == "GSUP-C") {
            commercialBPSnSDLable = "1 Year Commercial Basic SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Basic Support Discount"
            commercialBPSnSLable = "1 Year Commercial Basic SnS SKU"
        }
        if (extension == "PSUB-C") {
            commercialBPSnSDLable = "1 Year Commercial Production SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Production Subscription Discount"
            commercialBPSnSLable = "1 Year Commercial Production SnS SKU"
        }
        if (extension == "PSUP-C") {
            commercialBPSnSDLable = "1 Year Commercial Production SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Production Support Discount"
            commercialBPSnSLable = "1 Year Commercial Production SnS SKU"
        }
        if (extension == "PSUB-A") {
            commercialBPSnSDLable = "1 Year Academic Production SnS Discount"
            commercialBPSSDLable = "1 Year Academic Production Subscription Discount"
            commercialBPSnSLable = "1 Year Academic Production SnS SKU"
        }
        if (extension == "PSUP-A") {
            commercialBPSnSDLable = "1 Year Academic Production SnS Discount"
            commercialBPSSDLable = "1 Year Academic Production Support Discount"
            commercialBPSnSLable = "1 Year Academic Production SnS SKU"
        }
        def chartDef3 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: commercialBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : commercialBPSnSLable,
                                        "isSum": true
                                ], [
                                        name: commercialBPSSDLable,
                                        y   : commercialSSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef3)
    }
}
if (listP != null && (extension == "3G-SSS-C" || extension == "3GSUB-C" || extension == "3GSUP-C" ||
        extension == "3P-SSS-C" || extension == "3PSUB-C" || extension == "3PSUP-C" ||
        extension == "3P-SSS-A" || extension == "3PSUB-A" || extension == "3PSUP-A")) {
    def commercialBPSnSSKULable
    def commercialBPSSSKULable
    def threeYearCommercialBPSnSDLable
    def threeYearCommercialSnSDiscount = 0
    def threeYearCommercialSSDiscount = 0
    if (extension == "3G-SSS-C" || extension == "3P-SSS-C" || extension == "3P-SSS-A") {
        if (listP != null && oneYearCommercialBPSnSP != null) {
            threeYearCommercialSnSDiscount = listP - oneYearCommercialBPSnSP
        }
        if (extension == "3G-SSS-C") {
            commercialBPSnSDLable = "1 Year Commercial Basic SnS Discount"
            commercialBPSnSSKULable = "1 Year Commercial Basic SnS SKU"
            threeYearCommercialBPSnSDLable = "3 Year Commercial Basic SnS Adjustment"
        } else if (extension == "3P-SSS-C") {
            commercialBPSnSDLable = "1 Year Commercial Production SnS Discount"
            commercialBPSnSSKULable = "1 Year Commercial Production SnS SKU"
            threeYearCommercialBPSnSDLable = "3 Year Commercial Production SnS Adjustment"
        } else if (extension == "3P-SSS-A") {
            commercialBPSnSDLable = "1 Year Academic Production SnS Discount"
            commercialBPSnSSKULable = "1 Year Academic Production SnS SKU"
            threeYearCommercialBPSnSDLable = "3 Year Academic Production SnS Adjustment"
        }
        def chartDef4 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: commercialBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : commercialBPSnSSKULable,
                                        "isSum": true
                                ], [
                                        name: threeYearCommercialBPSnSDLable,
                                        y   : threeYearCommercialSnSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef4)
    }
    if (extension == "3GSUB-C" || extension == "3GSUP-C" ||
            extension == "3PSUB-C" || extension == "3PSUP-C" ||
            extension == "3PSUB-A" || extension == "3PSUP-A") {
        if (listP != null && oneYearCommercialBPSSP != null) {
            threeYearCommercialSSDiscount = listP - oneYearCommercialBPSSP
        }
        if (extension == "3GSUB-C") {
            commercialBPSnSDLable = "1 Year Commercial Basic SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Basic Subscription Discount"
            commercialBPSnSLable = "1 Year Commercial Basic SnS SKU"
            commercialBPSSSKULable = "1 Year Commercial Basic Subscription SKU"
            threeYearCommercialBPSnSDLable = "3 Year Commercial Basic Subscription Adjustment"
        }
        if (extension == "3GSUP-C") {
            commercialBPSnSDLable = "1 Year Commercial Basic SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Basic Support Discount"
            commercialBPSnSLable = "1 Year Commercial Basic SnS SKU"
            commercialBPSSSKULable = "1 Year Commercial Basic Support SKU"
            threeYearCommercialBPSnSDLable = "3 Year Commercial Basic Support Adjustment"
        }
        if (extension == "3PSUB-C") {
            commercialBPSnSDLable = "1 Year Commercial Production SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Production Subscription Discount"
            commercialBPSnSLable = "1 Year Commercial Production SnS SKU"
            commercialBPSSSKULable = "1 Year Commercial Poduction Subscription SKU"
            threeYearCommercialBPSnSDLable = "3 Year Commercial Production Subscription Adjustment"
        }
        if (extension == "3PSUP-C") {
            commercialBPSnSDLable = "1 Year Commercial Production SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Production Support Discount"
            commercialBPSnSLable = "1 Year Commercial Production SnS SKU"
            commercialBPSSSKULable = "1 Year Commercial Production Support SKU"
            threeYearCommercialBPSnSDLable = "3 Year Commercial Production Support Adjustment"
        }
        if (extension == "3PSUB-A") {
            commercialBPSnSDLable = "1 Year Academic Production SnS Discount"
            commercialBPSSDLable = "1 Year Academic Production Subscription Discount"
            commercialBPSnSLable = "1 Year Academic Production SnS SKU"
            commercialBPSSSKULable = "1 Year Academic Poduction Subscription SKU"
            threeYearCommercialBPSnSDLable = "3 Year Academic Production Subscription Adjustment"
        }
        if (extension == "3PSUP-A") {
            commercialBPSnSDLable = "1 Year Academicl Production SnS Discount"
            commercialBPSSDLable = "1 Year Academic Production Support Discount"
            commercialBPSnSLable = "1 Year Academic Production SnS SKU"
            commercialBPSSSKULable = "1 Year Academic Production Support SKU"
            threeYearCommercialBPSnSDLable = "3 Year Academic Production Support Adjustment"
        }
        def chartDef5 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: commercialBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : commercialBPSnSLable,
                                        "isSum": true
                                ], [
                                        name: commercialBPSSDLable,
                                        y   : commercialSSDiscount
                                ], [
                                        name   : commercialBPSSSKULable,
                                        "isSum": true
                                ], [
                                        name: threeYearCommercialBPSnSDLable,
                                        y   : threeYearCommercialSSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef5)
    }
}
if (listP != null && (extension == "2M-GSSS-C" || extension == "2M-GSUB-C" || extension == "2M-GSUP-C" ||
        extension == "2M-PSSS-C" || extension == "2M-PSUB-C" || extension == "2M-PSUP-C")) {
    def commercialBPSnSSKULable
    def commercialBPSSSKULable
    def twoMonthCommercialBPSnSDLable
    def twoMonthCommercialSnSDiscount = 0
    def twoMonthCommercialSSDiscount = 0
    if (extension == "2M-GSSS-C" || extension == "2M-PSSS-C") {
        if (listP != null && oneYearCommercialBPSnSP != null) {
            twoMonthCommercialSnSDiscount = listP - oneYearCommercialBPSnSP
        }
        if (extension == "2M-GSSS-C") {
            commercialBPSnSDLable = "1 Year Commercial Basic SnS Discount"
            commercialBPSnSSKULable = "1 Year Commercial Basic SnS SKU"
            twoMonthCommercialBPSnSDLable = "2 Month Commercial Basic SnS Adjustment"
        } else if (extension == "2M-PSSS-C") {
            commercialBPSnSDLable = "1 Year Commercial Production SnS Discount"
            commercialBPSnSSKULable = "1 Year Commercial Production SnS SKU"
            twoMonthCommercialBPSnSDLable = "2 Month Commercial Production SnS Adjustment"
        }
        def chartDef6 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: commercialBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : commercialBPSnSSKULable,
                                        "isSum": true
                                ], [
                                        name: twoMonthCommercialBPSnSDLable,
                                        y   : twoMonthCommercialSnSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef6)
    }
    if (extension == "2M-GSUB-C" || extension == "2M-GSUP-C" ||
            extension == "2M-PSUB-C" || extension == "2M-PSUP-C") {
        if (listP != null && oneYearCommercialBPSSP != null) {
            twoMonthCommercialSSDiscount = listP - oneYearCommercialBPSSP
        }
        if (extension == "2M-GSUB-C") {
            commercialBPSnSDLable = "1 Year Commercial Basic SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Basic Subscription Discount"
            commercialBPSnSLable = "1 Year Commercial Basic SnS SKU"
            commercialBPSSSKULable = "1 Year Commercial Basic Subscription SKU"
            twoMonthCommercialBPSnSDLable = "2 Month Commercial Basic Subscription Adjustment"
        }
        if (extension == "2M-GSUP-C") {
            commercialBPSnSDLable = "1 Year Commercial Basic SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Basic Support Discount"
            commercialBPSnSLable = "1 Year Commercial Basic SnS SKU"
            commercialBPSSSKULable = "1 Year Commercial Basic Support SKU"
            twoMonthCommercialBPSnSDLable = "2 Month Commercial Basic Support Adjustment"
        }
        if (extension == "2M-PSUB-C") {
            commercialBPSnSDLable = "1 Year Commercial Production SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Production Subscription Discount"
            commercialBPSnSLable = "1 Year Commercial Production SnS SKU"
            commercialBPSSSKULable = "1 Year Commercial Poduction Subscription SKU"
            twoMonthCommercialBPSnSDLable = "2 Month Commercial Production Subscription Adjustment"
        }
        if (extension == "2M-PSUP-C") {
            commercialBPSnSDLable = "1 Year Commercial Production SnS Discount"
            commercialBPSSDLable = "1 Year Commercial Production Support Discount"
            commercialBPSnSLable = "1 Year Commercial Production SnS SKU"
            commercialBPSSSKULable = "1 Year Commercial Production Support SKU"
            twoMonthCommercialBPSnSDLable = "2 Month Commercial Production Support Adjustment"
        }
        def chartDef7 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: commercialBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : commercialBPSnSLable,
                                        "isSum": true
                                ], [
                                        name: commercialBPSSDLable,
                                        y   : commercialSSDiscount
                                ], [
                                        name   : commercialBPSSSKULable,
                                        "isSum": true
                                ], [
                                        name: twoMonthCommercialBPSnSDLable,
                                        y   : twoMonthCommercialSSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef7)
    }
}


def federalBPSnSDLable
def federalBPSSDLable
def federalBPSnSLable
if (listP != null && extension == "F") {
    def chartDef8 = [
            title  : [
                    text: title
            ],
            tooltip: [
                    pointFormat: '$ <b>{point.y:,.2f}</b>'
            ],
            series : [
                    data: [
                            [
                                    name: commercialL,
                                    y   : commercialLP
                            ], [
                                    name: "Federal License Adjustment",
                                    y   : 0
                            ], [
                                    name   : "Federal License",
                                    "isSum": true
                            ]
                    ]
            ]
    ]
    return api.buildFlexChart("hc_wf_base", chartDef8)
}
if (listP != null && (extension == "G-SSS-F" || extension == "GSUB-F" || extension == "GSUP-F" ||
        extension == "P-SSS-F" || extension == "PSUB-F" || extension == "PSUP-F")) {

    if (extension == "G-SSS-F" || extension == "P-SSS-F") {
        if (extension == "G-SSS-F") {
            federalBPSnSDLable = "1 Year Federal Basic SnS Discount"
        } else if (extension == "P-SSS-F") {
            federalBPSnSDLable = "1 Year Federal Production SnS Discount"
        }
        def chartDef9 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: "Federal License Adjustment",
                                        y   : 0
                                ], [
                                        name   : "Federal License",
                                        "isSum": true
                                ], [
                                        name: federalBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef9)
    }
    if (extension == "GSUB-F" || extension == "GSUP-F" ||
            extension == "PSUB-F" || extension == "PSUP-F") {
        if (extension == "GSUB-F") {
            federalBPSnSDLable = "1 Year Federal Basic SnS Discount"
            federalBPSSDLable = "1 Year Federal Basic Subscription Discount"
            federalBPSnSLable = "1 Year Federal Basic SnS SKU"
        }
        if (extension == "GSUP-F") {
            federalBPSnSDLable = "1 Year Federal Basic SnS Discount"
            federalBPSSDLable = "1 Year Federal Basic Support Discount"
            federalBPSnSLable = "1 Year Federal Basic SnS SKU"
        }
        if (extension == "PSUB-F") {
            federalBPSnSDLable = "1 Year Federal Production SnS Discount"
            federalBPSSDLable = "1 Year Federal Production Subscription Discount"
            federalBPSnSLable = "1 Year Federal Production SnS SKU"
        }
        if (extension == "PSUP-F") {
            federalBPSnSDLable = "1 Year Federal Production SnS Discount"
            federalBPSSDLable = "1 Year Federal Production Support Discount"
            federalBPSnSLable = "1 Year Federal Production SnS SKU"
        }
        def chartDef10 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: "Federal License Adjustment",
                                        y   : 0
                                ], [
                                        name   : "Federal License",
                                        "isSum": true
                                ], [
                                        name: federalBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : federalBPSnSLable,
                                        "isSum": true
                                ], [
                                        name: federalBPSSDLable,
                                        y   : commercialSSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef10)
    }
}
if (listP != null && (extension == "3G-SSS-F" || extension == "3GSUB-F" || extension == "3GSUP-F" ||
        extension == "3P-SSS-F" || extension == "3PSUB-F" || extension == "3PSUP-F")) {
    def federalBPSnSSKULable
    def federalBPSSSKULable
    def threeYearFederalBPSnSDLable
    def threeYearCommercialSnSDiscount = 0
    def threeYearCommercialSSDiscount = 0
    if (extension == "3G-SSS-F" || extension == "3P-SSS-F") {
        if (listP != null && oneYearCommercialBPSnSP != null) {
            threeYearCommercialSnSDiscount = listP - oneYearCommercialBPSnSP
        }
        if (extension == "3G-SSS-F") {
            federalBPSnSDLable = "1 Year Federal Basic SnS Discount"
            federalBPSnSSKULable = "1 Year Federal Basic SnS SKU"
            threeYearFederalBPSnSDLable = "3 Year Federal Basic SnS Adjustment"
        } else if (extension == "3P-SSS-F") {
            federalBPSnSDLable = "1 Year Federal Production SnS Discount"
            federalBPSnSSKULable = "1 Year Federal Production SnS SKU"
            threeYearFederalBPSnSDLable = "3 Year Federal Production SnS Adjustment"
        }
        def chartDef11 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: "Federal License Adjustment",
                                        y   : 0
                                ], [
                                        name   : "Federal License",
                                        "isSum": true
                                ], [
                                        name: federalBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : federalBPSnSSKULable,
                                        "isSum": true
                                ], [
                                        name: threeYearFederalBPSnSDLable,
                                        y   : threeYearCommercialSnSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef11)
    }
    if (extension == "3GSUB-F" || extension == "3GSUP-F" ||
            extension == "3PSUB-F" || extension == "3PSUP-F") {
        if (listP != null && oneYearCommercialBPSSP != null) {
            threeYearCommercialSSDiscount = listP - oneYearCommercialBPSSP
        }
        if (extension == "3GSUB-F") {
            federalBPSnSDLable = "1 Year Federal Basic SnS Discount"
            federalBPSSDLable = "1 Year Federal Basic Subscription Discount"
            federalBPSnSLable = "1 Year Federal Basic SnS SKU"
            federalBPSSSKULable = "1 Year Federal Basic Subscription SKU"
            threeYearFederalBPSnSDLable = "3 Year Federal Basic Subscription Adjustment"
        }
        if (extension == "3GSUP-F") {
            federalBPSnSDLable = "1 Year Federal Basic SnS Discount"
            federalBPSSDLable = "1 Year Federal Basic Support Discount"
            federalBPSnSLable = "1 Year Federal Basic SnS SKU"
            federalBPSSSKULable = "1 Year Federal Basic Support SKU"
            threeYearFederalBPSnSDLable = "3 Year Federal Basic Support Adjustment"
        }
        if (extension == "3PSUB-F") {
            federalBPSnSDLable = "1 Year Federal Production SnS Discount"
            federalBPSSDLable = "1 Year Federal Production Subscription Discount"
            federalBPSnSLable = "1 Year Federal Production SnS SKU"
            federalBPSSSKULable = "1 Year Federal Poduction Subscription SKU"
            threeYearFederalBPSnSDLable = "3 Year Federal Production Subscription Adjustment"
        }
        if (extension == "3PSUP-F") {
            federalBPSnSDLable = "1 Year Federal Production SnS Discount"
            federalBPSSDLable = "1 Year Federal Production Support Discount"
            federalBPSnSLable = "1 Year Federal Production SnS SKU"
            federalBPSSSKULable = "1 Year Federal Production Support SKU"
            threeYearFederalBPSnSDLable = "3 Year Federal Production Support Adjustment"
        }
        def chartDef12 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: "Federal License Adjustment",
                                        y   : 0
                                ], [
                                        name   : "Federal License",
                                        "isSum": true
                                ], [
                                        name: federalBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : federalBPSnSLable,
                                        "isSum": true
                                ], [
                                        name: federalBPSSDLable,
                                        y   : commercialSSDiscount
                                ], [
                                        name   : federalBPSSSKULable,
                                        "isSum": true
                                ], [
                                        name: threeYearFederalBPSnSDLable,
                                        y   : threeYearCommercialSSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef12)
    }
}


if (listP != null && (extension == "2M-GSSS-F" || extension == "2M-GSUB-F" || extension == "2M-GSUP-F" ||
        extension == "2M-PSSS-F" || extension == "2M-PSUB-F" || extension == "2M-PSUP-F" ||
        extension == "2M-PSSS-A" || extension == "2M-PSUB-A" || extension == "2M-PSUP-A")) {
    def federalBPSnSSKULable
    def federalBPSSSKULable
    def twoMonthFederalBPSnSDLable
    def twoMonthCommercialSnSDiscount = 0
    def twoMonthCommercialSSDiscount = 0
    if (extension == "2M-GSSS-F" || extension == "2M-PSSS-F" || extension == "2M-PSSS-A") {
        if (listP != null && oneYearCommercialBPSnSP != null) {
            twoMonthCommercialSnSDiscount = listP - oneYearCommercialBPSnSP
        }
        if (extension == "2M-GSSS-F") {
            federalBPSnSDLable = "1 Year Federal Basic SnS Discount"
            federalBPSnSSKULable = "1 Year Federal Basic SnS SKU"
            twoMonthfederalBPSnSDLable = "2 Month Federal Basic SnS Adjustment"
        } else if (extension == "2M-PSSS-F") {
            federalBPSnSDLable = "1 Year Federal Production SnS Discount"
            federalBPSnSSKULable = "1 Year Federal Production SnS SKU"
            twoMonthfederalBPSnSDLable = "2 Month Federal Production SnS Adjustment"
        } else if (extension == "2M-PSSS-A") {
            federalBPSnSDLable = "1 Year Academic Production SnS Discount"
            federalBPSnSSKULable = "1 Year Academic Production SnS SKU"
            twoMonthfederalBPSnSDLable = "2 Month Academic Production SnS Adjustment"
        }
        def chartDef13 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: federalBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : federalBPSnSSKULable,
                                        "isSum": true
                                ], [
                                        name: twoMonthfederalBPSnSDLable,
                                        y   : twoMonthCommercialSnSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef13)
    }
    if (extension == "2M-GSUB-F" || extension == "2M-GSUP-F" ||
            extension == "2M-PSUB-F" || extension == "2M-PSUP-F" ||
            extension == "2M-PSUB-A" || extension == "2M-PSUP-A") {
        if (listP != null && oneYearCommercialBPSSP != null) {
            twoMonthCommercialSSDiscount = listP - oneYearCommercialBPSSP
        }
        if (extension == "2M-GSUB-F") {
            federalBPSnSDLable = "1 Year Federal Basic SnS Discount"
            federalBPSSDLable = "1 Year Federal Basic Subscription Discount"
            federalBPSnSLable = "1 Year Federal Basic SnS SKU"
            federalBPSSSKULable = "1 Year Federal Basic Subscription SKU"
            twoMonthFederalBPSnSDLable = "2 Month Federal Basic Subscription Adjustment"
        }
        if (extension == "2M-GSUP-F") {
            federalBPSnSDLable = "1 Year Federal Basic SnS Discount"
            federalBPSSDLable = "1 Year Federal Basic Support Discount"
            federalBPSnSLable = "1 Year Federal Basic SnS SKU"
            federalBPSSSKULable = "1 Year Federal Basic Support SKU"
            twoMonthFederalBPSnSDLable = "2 Month Federal Basic Support Adjustment"
        }
        if (extension == "2M-PSUB-F") {
            federalBPSnSDLable = "1 Year Federal Production SnS Discount"
            federalBPSSDLable = "1 Year Federal Production Subscription Discount"
            federalBPSnSLable = "1 Year Federal Production SnS SKU"
            federalBPSSSKULable = "1 Year Federal Poduction Subscription SKU"
            twoMonthFederalBPSnSDLable = "2 Month Federal Production Subscription Adjustment"
        }
        if (extension == "2M-PSUP-F") {
            federalBPSnSDLable = "1 Year Federal Production SnS Discount"
            federalBPSSDLable = "1 Year Federal Production Support Discount"
            federalBPSnSLable = "1 Year Federal Production SnS SKU"
            federalBPSSSKULable = "1 Year Federal Production Support SKU"
            twoMonthFederalBPSnSDLable = "2 Month Federal Production Support Adjustment"
        }
        if (extension == "2M-PSUB-A") {
            federalBPSnSDLable = "1 Year Academic Production SnS Discount"
            federalBPSSDLable = "1 Year Academic AcademicProduction Subscription Discount"
            federalBPSnSLable = "1 Year Academic Production SnS SKU"
            federalBPSSSKULable = "1 Year Academic Poduction Subscription SKU"
            twoMonthFederalBPSnSDLable = "2 Month Academic Production Subscription Adjustment"
        }
        if (extension == "2M-PSUP-A") {
            federalBPSnSDLable = "1 Year Academic Production SnS Discount"
            federalBPSSDLable = "1 Year Academic Production Support Discount"
            federalBPSnSLable = "1 Year Academic Production SnS SKU"
            federalBPSSSKULable = "1 Year Academic Production Support SKU"
            twoMonthFederalBPSnSDLable = "2 Month Academic Production Support Adjustment"
        }
        def chartDef14 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: federalBPSnSDLable,
                                        y   : commercialSnSDiscount
                                ], [
                                        name   : federalBPSnSLable,
                                        "isSum": true
                                ], [
                                        name: federalBPSSDLable,
                                        y   : commercialSSDiscount
                                ], [
                                        name   : federalBPSSSKULable,
                                        "isSum": true
                                ], [
                                        name: twoMonthFederalBPSnSDLable,
                                        y   : twoMonthCommercialSSDiscount
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef14)
    }
}

if (listP != null && (extension == "F-L1" || extension == "F-L2" || extension == "F-L3" || extension == "F-L4")) {
    def adjustment = 0
    def discount = 0
    if (federal != null && commercialLP != null) {
        adjustment = federal - commercialLP
    }
    if (federal != null && discountedFederal != null) {
        discount = discountedFederal - federal
    }
    def chartDef15 = [
            title  : [
                    text: title
            ],
            tooltip: [
                    pointFormat: '$ <b>{point.y:,.2f}</b>'
            ],
            series : [
                    data: [
                            [
                                    name: commercialL,
                                    y   : commercialLP
                            ], [
                                    name: "Federal License Adjustment",
                                    y   : adjustment
                            ], [
                                    name   : "Federal License",
                                    "isSum": true
                            ], [
                                    name: "Federal License Discount",
                                    y   : discount
                            ], [
                                    name   : partNo,
                                    "isSum": true
                            ]
                    ]
            ]
    ]
    return api.buildFlexChart("hc_wf_base", chartDef15)
}
if (listP != null && extension == "F") {
    def adjustment = 0
    if (federal != null && commercialLP != null) {
        adjustment = federal - commercialLP
    }
    def chartDef16 = [
            title  : [
                    text: title
            ],
            tooltip: [
                    pointFormat: '$ <b>{point.y:,.2f}</b>'
            ],
            series : [
                    data: [
                            [
                                    name: commercialL,
                                    y   : commercialLP
                            ], [
                                    name: "Federal License Adjustment",
                                    y   : adjustment
                            ], [
                                    name   : partNo,
                                    "isSum": true
                            ]
                    ]
            ]
    ]
    return api.buildFlexChart("hc_wf_base", chartDef16)
}

def acad = api.getElement("AcademicLicense")
def discount = 0
if (acad != null && commercialLP != null) {
    discount = acad - commercialLP
}

if (listP != null && extension == "A") {
    def chartDef17 = [
            title  : [
                    text: title
            ],
            tooltip: [
                    pointFormat: '$ <b>{point.y:,.2f}</b>'
            ],
            series : [
                    data: [
                            [
                                    name: commercialL,
                                    y   : commercialLP
                            ], [
                                    name: "Academic License Discount",
                                    y   : discount
                            ], [
                                    name   : partNo,
                                    "isSum": true
                            ]
                    ]
            ]
    ]
    return api.buildFlexChart("hc_wf_base", chartDef17)
}

def oneYearAcadBasicSnS = api.getElement("OneYearAcademicBasicSnSPrice")
def discountB = 0
if (acad != null && oneYearAcadBasicSnS != null) {
    discountB = oneYearAcadBasicSnS - acad
}
def oneYearAcadBasicSS = api.getElement("OneYearAcademicBasicSupportSubscriptionPrice")

if (listP != null && extension == "G-SSS-A") {
    if (extension == "G-SSS-A") {
        def chartDef18 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: "Academic License Discount",
                                        y   : discount
                                ], [
                                        name   : "Academic License",
                                        "isSum": true
                                ], [
                                        name: "1 Year Academic Basic SnS Discount",
                                        y   : discountB
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef18)
    }
}
if (listP != null && (extension == "GSUB-A" || extension == "GSUP-A")) {
    def discountSS = 0
    if (oneYearAcadBasicSS != 0 && oneYearAcadBasicSnS != 0) {
        discountSS = oneYearAcadBasicSS - oneYearAcadBasicSnS
        def acadBSSLabel = ""
        if (extension == "GSUB-A") {
            acadBSSLabel = "1 Year Academic Basic Subscription Discount"
        }
        if (extension == "GSUP-A") {
            acadBSSLabel = "1 Year Academic Basic Support Discount"
        }

        def chartDef19 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: "Academic License Discount",
                                        y   : discount
                                ], [
                                        name   : "Academic License",
                                        "isSum": true
                                ], [
                                        name: "1 Year Academic Basic SnS Discount",
                                        y   : discountB
                                ], [
                                        name   : "1 Year Academic Basic SnS SKU",
                                        "isSum": true
                                ], [
                                        name: acadBSSLabel,
                                        y   : discountSS
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef19)
    }
}


if (listP != null && extension == "3G-SSS-A") {
    if (extension == "3G-SSS-A") {
        def discount3Year = 0
        if (listP != null && oneYearAcadBasicSnS != null) {
            discount3Year = listP - oneYearAcadBasicSnS
        }
        def chartDef20 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: "Academic License Discount",
                                        y   : discount
                                ], [
                                        name   : "Academic License",
                                        "isSum": true
                                ], [
                                        name: "1 Year Academic Basic SnS Discount",
                                        y   : discountB
                                ], [
                                        name   : "1 Year Academic Basic SnS SKU",
                                        "isSum": true
                                ], [
                                        name: "3 Year Academic Basic SnS Adjustment",
                                        y   : discount3Year
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef20)
    }
}
if (listP != null && (extension == "3GSUB-A" || extension == "3GSUP-A")) {
    def discountSS = 0.0
    if (oneYearAcadBasicSS != null && oneYearAcadBasicSnS != null) {
        discountSS = oneYearAcadBasicSS - oneYearAcadBasicSnS
    }
    def discountSSThree = 0.0
    def test
    if (oneYearAcadBasicSS != null) {
        listP = listP?.toBigDecimal()
        oneYearAcadBasicSS?.toBigDecimal()
        test = listP - oneYearAcadBasicSS
        discountSSThree = listP - oneYearAcadBasicSS
    }
    def acadBSSLabelOne = ""
    def acadBSSLabelThree = ""
    def acadBSSSKU = ""
    if (extension == "3GSUB-A") {
        acadBSSLabelOne = "1 Year Academic Basic Subscription Discount"
        acadBSSLabelThree = "3 Year Academic Basic Subscription Adjustment"
        acadBSSSKU = "1 Year Academic Basic Subscription SKU"
    }
    if (extension == "3GSUP-A") {
        acadBSSLabelOne = "1 Year Academic Basic Support Discount"
        acadBSSLabelThree = "3 Year Academic Basic Support Adjustment"
        acadBSSSKU = "1 Year Academic Basic Subpport SKU"
    }

    def chartDef21 = [
            title  : [
                    text: title
            ],
            tooltip: [
                    pointFormat: '$ <b>{point.y:,.2f}</b>'
            ],
            series : [
                    data: [
                            [
                                    name: commercialL,
                                    y   : commercialLP
                            ], [
                                    name: "Academic License Discount",
                                    y   : discount
                            ], [
                                    name   : "Academic License",
                                    "isSum": true
                            ], [
                                    name: "1 Year Academic Basic SnS Discount",
                                    y   : discountB
                            ], [
                                    name   : "1 Year Academic Basic SnS SKU",
                                    "isSum": true
                            ], [
                                    name: acadBSSLabelOne,
                                    y   : discountSS
                            ], [
                                    name   : acadBSSSKU,
                                    "isSum": true
                            ], [
                                    name: acadBSSLabelThree,
                                    y   : discountSSThree
                            ], [
                                    name   : partNo,
                                    "isSum": true
                            ]
                    ]
            ]
    ]
    return api.buildFlexChart("hc_wf_base", chartDef21)
}


def twoMonthAcadBasicSnS = api.getElement("TwoMonthAcademicBasicSnSPrice")
if (listP != null && extension == "2M-GSSS-A") {
    if (extension == "2M-GSSS-A") {
        def discount2Month = 0
        if (listP != null && twoMonthAcadBasicSnS != null) {
            discount2Month = listP - oneYearAcadBasicSnS
        }
        def chartDef22 = [
                title  : [
                        text: title
                ],
                tooltip: [
                        pointFormat: '$ <b>{point.y:,.2f}</b>'
                ],
                series : [
                        data: [
                                [
                                        name: commercialL,
                                        y   : commercialLP
                                ], [
                                        name: "Academic License Discount",
                                        y   : discount
                                ], [
                                        name   : "Academic License",
                                        "isSum": true
                                ], [
                                        name: "1 Year Academic Basic SnS Discount",
                                        y   : discountB
                                ], [
                                        name   : "1 Year Academic Basic SnS SKU",
                                        "isSum": true
                                ], [
                                        name: "2 Month Academic Basic SnS Adjustment",
                                        y   : discount2Month
                                ], [
                                        name   : partNo,
                                        "isSum": true
                                ]
                        ]
                ]
        ]
        return api.buildFlexChart("hc_wf_base", chartDef22)
    }
}
if (listP != null && (extension == "2M-GSUB-A" || extension == "2M-GSUP-A")) {
    def discountSS = 0.0
    if (oneYearAcadBasicSS != null && oneYearAcadBasicSnS != null) {
        discountSS = oneYearAcadBasicSS - oneYearAcadBasicSnS
    }
    if (oneYearAcadBasicSS != null) {
        listP = listP?.toBigDecimal()
        discountSSThree = listP - oneYearAcadBasicSS
    }
    def acadBSSLabelOne = ""
    def acadBSSLabelTwo = ""
    def acadBSSSKU = ""
    if (extension == "2M-GSUB-A") {
        acadBSSLabelOne = "1 Year Academic Basic Subscription Discount"
        acadBSSLabelTwo = "2 Month Academic Basic Subscription Adjustment"
        acadBSSSKU = "1 Year Academic Basic Subscription SKU"
    }
    if (extension == "2M-GSUP-A") {
        acadBSSLabelOne = "1 Year Academic Basic Support Discount"
        acadBSSLabelTwo = "2 Month Academic Basic Support Adjustment"
        acadBSSSKU = "1 Year Academic Basic Subpport SKU"
    }

    def chartDef23 = [
            title  : [
                    text: title
            ],
            tooltip: [
                    pointFormat: '$ <b>{point.y:,.2f}</b>'
            ],
            series : [
                    data: [
                            [
                                    name: commercialL,
                                    y   : commercialLP
                            ], [
                                    name: "Academic License Discount",
                                    y   : discount
                            ], [
                                    name   : "Academic License",
                                    "isSum": true
                            ], [
                                    name: "1 Year Academic Basic SnS Discount",
                                    y   : discountB
                            ], [
                                    name   : "1 Year Academic Basic SnS SKU",
                                    "isSum": true
                            ], [
                                    name: acadBSSLabelOne,
                                    y   : discountSS
                            ], [
                                    name   : acadBSSSKU,
                                    "isSum": true
                            ], [
                                    name: acadBSSLabelTwo,
                                    y   : discountSSTwo
                            ], [
                                    name   : partNo,
                                    "isSum": true
                            ]
                    ]
            ]
    ]
    return api.buildFlexChart("hc_wf_base", chartDef23)
}


if (listP != null && (extension == "5P-SSS-C" || extension == "5P-SSS-A" || extension == "5P-SSS-F")) {

    def discount5Year = 0
    if (listP != null && oneYearCommercialBPSnSP != null) {
        discount5Year = listP - oneYearCommercialBPSnSP
    }
    def chartDef24 = [
            title  : [
                    text: title
            ],
            tooltip: [
                    pointFormat: '$ <b>{point.y:,.2f}</b>'
            ],
            series : [
                    data: [
                            [
                                    name: commercialL,
                                    y   : commercialLP
                            ], [
                                    name: "1 Year Commercial Production SnS Discount",
                                    y   : commercialSnSDiscount
                            ], [
                                    name   : "1 Year Commercial Production SnS SKU",
                                    "isSum": true
                            ], [
                                    name: "5 Year Commercial Production SnS Adjustment",
                                    y   : discount5Year
                            ], [
                                    name   : partNo,
                                    "isSum": true
                            ]
                    ]
            ]
    ]
    return api.buildFlexChart("hc_wf_base", chartDef24)
}