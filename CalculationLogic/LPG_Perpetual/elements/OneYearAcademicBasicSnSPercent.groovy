if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
if (ext == "G-SSS-A" || ext == "GSUB-A" || ext == "GSUP-A" ||
        ext == "3G-SSS-A" || ext == "3GSUB-A" || ext == "3GSUP-A" ||
        ext == "2M-GSSS-A" || ext == "2M-GSUB-A" || ext == "2M-GSUP-A") {
    ext = "G-SSS-C"
}
def licenseTab
if (ext != null) {
    licenseTab = api.find("MLTV", api.filter("lookupTable.uniqueName", "SnSTypeAdjustment"),
            api.filter("name", ext))
}
if (licenseTab != null) {
    return licenseTab[0]?.attribute1
}