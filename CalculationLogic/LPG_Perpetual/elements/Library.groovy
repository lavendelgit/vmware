api.retainGlobal = true

def round(Number num, int decimalPlaces) {
    if (num == null) {
        return null
    }

    def factor = (int) Math.pow(10, decimalPlaces)

    return Math.round(num * factor) / factor
}

def truncate(String result) {
    def stringLength = result?.length()
    def index = result?.indexOf(".")
    //api.trace("test",null,stringLength)
    //api.trace("test",null,index)
    if (index < 0) {
        index = stringLength
        result = result?.substring(0, index)
    }
    index += 3
    def diff = stringLength - index
    if (diff < 0) {
        index = stringLength
    }
    api.trace("test", null, index)
    result = result?.substring(0, index)
    result = result?.toBigDecimal()
}