def listPrice = api.getElement("ListPrice")
def rate = api.getElement("ConversionFactorJPY")
def LC = api.getElement("LCFactor")

if (!LC || !rate || !listPrice) return ""

def LCprice = listPrice * rate
def threshHold = 5000
def roundToThresholdMet = 100
def roundToThresholdNotMet = 10
def substract = 10

if (api.getElement("ProductType") == "Perpetual- SnS") {
    return LCprice.setScale(0, BigDecimal.ROUND_HALF_UP)
} else {
    if (LC == "LC-77") {
        return libs.vmwareUtil.RoundingLogicDesktopV2.applyRoundingLogic(LCprice, threshHold, substract, roundToThresholdMet, roundToThresholdNotMet)?.toBigDecimal()
    } else {
        return libs.vmwareUtil.RoundingLogicV2.applyRoundingLogic(LCprice)?.toBigDecimal()
    }
}  