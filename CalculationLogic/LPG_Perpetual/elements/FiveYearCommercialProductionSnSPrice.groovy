if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def fiveYearDiscount = api.getElement("fiveYearDiscount")
def oneYearCommercialSnS = api.getElement("OneYearCommercialBasicProductionSnSPrice")
if (ext != null) {
    if (ext == "5P-SSS-C") {
        if (fiveYearDiscount != null && oneYearCommercialSnS != null) {
            fiveYearDiscount = 1 - fiveYearDiscount
            return 5 * fiveYearDiscount * oneYearCommercialSnS
        }
    }
}