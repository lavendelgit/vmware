if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def threeYearCommercialSS = api.getElement("ThreeYearCommercialBasicProductionSupportSubscriptionPrice")
if (ext != null) {
    if (ext == "3GSUB-F" || ext == "3GSUP-F" || ext == "3PSUB-F" || ext == "3PSUP-F") {
        if (threeYearCommercialSS != null) {
            return threeYearCommercialSS
        }
    }
}