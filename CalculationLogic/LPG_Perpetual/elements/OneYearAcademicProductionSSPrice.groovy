if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def price = api.getElement("OneYearCommercialBasicProductionSupportSubscriptionPrice")
if (ext == "PSUB-A" || ext == "PSUP-A") {
    return price
}