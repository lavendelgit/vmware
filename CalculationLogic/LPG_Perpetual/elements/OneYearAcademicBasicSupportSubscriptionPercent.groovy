if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
if (ext == "3GSUB-A" || ext == "2M-GSUB-A" || ext == "GSUB-A") {
    ext = "GSUB-C"
}
if (ext == "3GSUP-A" || ext == "2M-GSUP-A" || ext == "GSUP-A") {
    ext = "GSUP-C"
}
def snsTab
if (ext != null) {
    snsTab = api.find("MLTV", api.filter("lookupTable.uniqueName", "SupportMaintenance"),
            api.filter("name", ext))
}
if (snsTab != null) {
    return snsTab[0]?.attribute1
}