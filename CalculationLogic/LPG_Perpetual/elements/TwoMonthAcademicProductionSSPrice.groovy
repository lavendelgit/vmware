if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def price = api.getElement("TwoMonthCommercialBasicProductionSSPrice")
if (ext == "2M-PSUB-A" || ext == "2M-PSUP-A") {
    return price
}