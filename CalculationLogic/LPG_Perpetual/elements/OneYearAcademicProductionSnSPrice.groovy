if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def price = api.getElement("OneYearCommercialBasicProductionSnSPrice")
if (ext == "P-SSS-A") {
    return price
}