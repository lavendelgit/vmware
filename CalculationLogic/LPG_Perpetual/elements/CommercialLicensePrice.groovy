if (api.syntaxCheck) {
    return null
}
def commercialL = api.getElement("CommercialLicense")
def commercialLTab
if (commercialL) {
    commercialLTab = api.find("PX", Filter.equal("name", "ListPrice"), Filter.equal("sku", commercialL))
}
if (commercialLTab != null) {
    def result = commercialLTab[0]?.attribute1
    return result
}

