if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
if (ext == "GSUB-C" || ext == "GSUP-C" ||
        ext == "3G-SSS-C" || ext == "3GSUB-C" || ext == "3GSUP-C" ||
        ext == "2M-GSSS-C" || ext == "2M-GSUB-C" || ext == "2M-GSUP-C" ||
        ext == "GSUB-F" || ext == "GSUP-F" || ext == "G-SSS-F" ||
        ext == "3G-SSS-F" || ext == "3GSUB-F" || ext == "3GSUP-F" ||
        ext == "2M-GSSS-F" || ext == "2M-GSUB-F" || ext == "2M-GSUP-F") {
    ext = "G-SSS-C"
}
if (ext == "PSUB-C" || ext == "PSUP-C" ||
        ext == "3P-SSS-C" || ext == "3PSUB-C" || ext == "3PSUP-C" ||
        ext == "2M-PSSS-C" || ext == "2M-PSUB-C" || ext == "2M-PSUP-C" ||
        ext == "PSUB-F" || ext == "PSUP-F" || ext == "P-SSS-F" ||
        ext == "3P-SSS-F" || ext == "3PSUB-F" || ext == "3PSUP-F" ||
        ext == "2M-PSSS-F" || ext == "2M-PSUB-F" || ext == "2M-PSUP-F" ||
        ext == "PSUB-A" || ext == "PSUP-A" || ext == "P-SSS-A" ||
        ext == "3P-SSS-A" || ext == "3PSUB-A" || ext == "3PSUP-A" ||
        ext == "2M-PSSS-A" || ext == "2M-PSUB-A" || ext == "2M-PSUP-A" ||
        ext == "5P-SSS-C" || ext == "5P-SSS-A" || ext == "5P-SSS-F") {
    ext = "P-SSS-C"
}
def licenseTab
if (ext != null) {
    licenseTab = api.find("MLTV", api.filter("lookupTable.uniqueName", "SnSTypeAdjustment"),
            api.filter("name", ext))
}
if (licenseTab != null) {
    return licenseTab[0]?.attribute1
}