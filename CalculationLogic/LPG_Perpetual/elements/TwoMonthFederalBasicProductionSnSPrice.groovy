if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def twoMonthCommercialSnS = api.getElement("TwoMonthCommercialBasicProductionSnSPrice")
if (ext != null) {
    if (ext == "2M-GSSS-F" || ext == "2M-PSSS-F") {
        if (twoMonthCommercialSnS != null) {
            return twoMonthCommercialSnS
        }
    }
}