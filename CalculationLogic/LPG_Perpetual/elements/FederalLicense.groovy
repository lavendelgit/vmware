if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def commercialLPrice = api.getElement("CommercialLicensePrice")
def fedPercent = api.getElement("FederalLicensePercent")

if (ext != null) {

    if (ext == "F" || ext == "F-L1" || ext == "F-L2" || ext == "F-L3" || ext == "F-L4") {
        if (commercialLPrice != null && fedPercent != null) {
            fedPercent = 1 - fedPercent
            def result = commercialLPrice * fedPercent
            result = result?.toString()
            return Library.truncate(result)
        }
    }
}