if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def oneYearCommercialSS = api.getElement("OneYearCommercialBasicProductionSupportSubscriptionPrice")
if (ext != null) {
    if (ext == "GSUB-F" || ext == "GSUP-F" || ext == "PSUB-F" || ext == "PSUP-F") {
        if (oneYearCommercialSS != null) {
            return oneYearCommercialSS
        }
    }
}