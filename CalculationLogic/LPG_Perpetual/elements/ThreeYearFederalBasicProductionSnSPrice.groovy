if (api.syntaxCheck) {
    return null
}
def ext = api.getElement("Extension")
def threeYearCommercialSnS = api.getElement("ThreeYearCommercialBasicProductionSnSPrice")
if (ext != null) {
    if (ext == "3G-SSS-F" || ext == "3P-SSS-F") {
        if (threeYearCommercialSnS != null) {
            return threeYearCommercialSnS
        }
    }
}