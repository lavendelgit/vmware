api.local.skuInfo.each {
    def nyansaRecord = [:]
    nyansaRecord = [
            sku         : it.key,
            label       : it.value.ShortDesc,
            "attribute8": it.value.ItemType
    ]
    if (nyansaRecord) api.addOrUpdate("P", nyansaRecord)
}
