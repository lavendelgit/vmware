api.local.skuInfo = getSKUInfo()
return

Map getSKUInfo() {
    Map skuInfos = [:]
    def start = 0
    def extensionName = "NyansaProducts"
    def filter = [
            Filter.equal("name", extensionName)
    ]
    def skuStream = api.stream("PX20", "sku", *filter)
    skuInfos = skuStream?.collectEntries {
        skuInfo ->
            [(skuInfo.sku): [
                    "ShortDesc": skuInfo.attribute7,
                    "ItemType" : "Nyansa"
            ]]
    }
    skuStream?.close()
    return skuInfos
}
