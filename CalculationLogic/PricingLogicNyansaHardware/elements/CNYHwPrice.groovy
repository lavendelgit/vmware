BigDecimal hwPrice = out.HwPrice * libs.vmwareUtil.CacheManager.getCNYLCFactor(out.LCFactor)
return hwPrice.setScale(2, BigDecimal.ROUND_HALF_UP)
//@Todo