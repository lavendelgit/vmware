BigDecimal hwPrice = 0.0
if (out.Offer == "Hardware Capex") {
    hwPrice = out.BasePrice * out.ReplacementServiceFactor * out.SegmentAdjustment
    hwPrice = hwPrice.setScale(2, BigDecimal.ROUND_HALF_UP)
} else if (out.Offer == "Extended Replacement Service") {
    hwPrice = out.BasePrice * out.ReplacementServiceFactor * out.SegmentAdjustment
    hwPrice = out.YearFactor ? (hwPrice * out.YearFactor) : hwPrice
    hwPrice = out.LongevityFactor ? (hwPrice * out.LongevityFactor) : hwPrice
    hwPrice = hwPrice.setScale(2, BigDecimal.ROUND_HALF_UP)

}
return hwPrice

//@Todo Rounding to nearest 1