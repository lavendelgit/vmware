BigDecimal hwPrice = out.HwPrice * libs.vmwareUtil.CacheManager.getGBPLCFactor(out.LCFactor)
return hwPrice.setScale(2, BigDecimal.ROUND_HALF_UP)
//@Todo