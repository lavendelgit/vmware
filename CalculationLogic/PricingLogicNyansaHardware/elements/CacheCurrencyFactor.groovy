if (!out.LCFactor) return
if (!api.global.LCFactorCache)
    api.global.LCFactorCache = libs.vmwareUtil.CacheManager.cacheLCFactor(out.LCFactor)
return