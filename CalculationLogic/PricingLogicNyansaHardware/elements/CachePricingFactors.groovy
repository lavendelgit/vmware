if (!api.global.batch) {
    api.global.batch = [:]
}


if (!api.global.replacementFactorCache) {
    api.global.replacementFactorCache = libs.vmwareUtil.CacheManager.cacheHwReplacementServiceFactor(["All", Constants.STR_PRODUCT], ["All", Constants.STR_HW_OFFER, Constants.STR_EXT_REPLACEMNET_OFFER], [Constants.STR_BASE])
}

if (!api.global.basePriceCache) {
    api.global.basePriceCache = libs.vmwareUtil.CacheManager.cacheBasePrice(["All", Constants.STR_PRODUCT], ["All", Constants.STR_HW_OFFER, Constants.STR_EXT_REPLACEMNET_OFFER])
}

if (!api.global.segmentAdjustmentCache) {
    api.global.segmentAdjustmentCache = libs.vmwareUtil.CacheManager.cacheSegmentAdjustments(["All", Constants.STR_PRODUCT], ["All", Constants.STR_HW_OFFER, Constants.STR_EXT_REPLACEMNET_OFFER], ["All", Constants.STR_BASE])
}

if (!api.global.longevityFactorCache) {
    api.global.longevityFactorCache = libs.vmwareUtil.CacheManager.cacheLongevityFactors(["All", Constants.STR_PRODUCT], ["All", Constants.STR_EXT_REPLACEMNET_OFFER], ["All", Constants.STR_BASE])
}
api.local.pid = api.product("sku")
if (!api.global.batch[api.local.pid]) {
    api.global.batch.clear()
    def batch = api.getBatchInfo()?.collect { it[0] } ?: [api.local.pid]
    filter = [
            Filter.equal("name", "NyansaProducts"),
            Filter.in("sku", batch)
    ]
    def nyansaHwProductsList = api.find("PX20", *filter)
    nyansaHardwareMap = nyansaHwProductsList.collectEntries { [(it.sku): it] }
    batch.each {
        api.global.batch[it] = [
                "nyansaHwPXObj": (nyansaHardwareMap[it] ?: null)

        ]
    }
}
return