if (!api.global.basePrice) {
    def filters = [
            Filter.equal("key1", "Nyansa"),
            Filter.equal("key2", "Software"),
            //  Filter.equal("key3","Voyance")
    ]
    api.global.basePrice = api.findLookupTableValues("BasePrice", *filters)?.collectEntries {
        [(it.key3): it.attribute1 as BigDecimal]
    }
}
return