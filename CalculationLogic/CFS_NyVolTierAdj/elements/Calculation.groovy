api.local.factor = out.CurrentItem.attribute1 ?: 0.0 as BigDecimal
api.local.amount = out.CurrentItem.attribute2 ?: 0.0 as BigDecimal
def baseModel = out.CurrentItem.key3
if (out.OverrideType == "Amount") {
    api.local.factor = (api.local.amount / api.global.basePrice[out.CurrentItem.key3])?.setScale(6, BigDecimal.ROUND_HALF_UP)
} else if (out.OverrideType == "Factor") {
    api.local.amount = (api.local.factor * api.global.basePrice[out.CurrentItem.key3])?.setScale(2, BigDecimal.ROUND_HALF_UP)
}