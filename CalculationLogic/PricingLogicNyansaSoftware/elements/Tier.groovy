def tierRecord = api.global.volumeTierCache.find { it.value.TierName == out.VolumeTierName }
return tierRecord ? tierRecord.value.Factor : 0.0 as BigDecimal