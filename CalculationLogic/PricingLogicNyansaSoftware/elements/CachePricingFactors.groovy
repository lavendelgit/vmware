if (!api.global.batch) {
    api.global.batch = [:]
}
if (!api.global.basePriceCache) {
    api.global.basePriceCache = libs.vmwareUtil.CacheManager.cacheBasePrice(["All", Constants.STR_PRODUCT], ["All", Constants.STR_SW_OFFER])
}

if (!api.global.volumeTierCache) {
    api.global.volumeTierCache = libs.vmwareUtil.CacheManager.cacheVolumeTier()
}

if (!api.global.supportFactorCache) {
    api.global.supportFactorCache = libs.vmwareUtil.CacheManager.cacheSupportAdjustments(["All", Constants.STR_PRODUCT], ["All", Constants.STR_SW_OFFER], ["All", Constants.STR_BASE])
}

if (!api.global.serviceLevelFactorCache) {
    api.global.serviceLevelFactorCache = libs.vmwareUtil.CacheManager.cacheServiceLevelFactors(["All", Constants.STR_PRODUCT], ["All", Constants.STR_SW_OFFER], ["All", Constants.STR_BASE])
}


if (!api.global.segmentAdjustmentCache) {
    api.global.segmentAdjustmentCache = libs.vmwareUtil.CacheManager.cacheSegmentAdjustments(["All", Constants.STR_PRODUCT], ["All", Constants.STR_SW_OFFER], ["All", Constants.STR_BASE])
    api.trace("api.global.segmentAdjustmentCache", api.global.segmentAdjustmentCache)
}

if (!api.global.termsCache) {
    api.global.termsCache = libs.vmwareUtil.CacheManager.cacheTermsFactor(["All", Constants.STR_PRODUCT], ["All", Constants.STR_SW_OFFER], ["All", Constants.STR_BASE])
}

if (!api.global.paymentLabelCache) {
    api.global.paymentLabelCache = libs.vmwareUtil.CacheManager.cachePaymentFrequncyLabel()
}
api.local.pid = api.product("sku")
if (!api.global.batch[api.local.pid]) {
    api.global.batch.clear()
    def batch = api.getBatchInfo()?.collect { it[0] } ?: [api.local.pid]
    filter = [
            Filter.equal("name", "NyansaProducts"),
            Filter.in("sku", batch)
    ]
    def nyansaProductsList = api.find("PX20", *filter)
    nyansaMap = nyansaProductsList.collectEntries { [(it.sku): it] }
    batch.each {
        api.global.batch[it] = [
                "nyansaSwPXObj": (nyansaMap[it] ?: null)

        ]
    }
}
return