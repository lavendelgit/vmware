BigDecimal swPrice = 0.0
swPrice = out.BasePrice * out.Tier * out.SegmentAdjustment * out.SupportFactor * out.ServiceFactor * out.TermFactor * out.LengthFactor
swPrice = out.NormalizationFactor ? (swPrice / out.NormalizationFactor) : 0.0
swPrice = swPrice.setScale(2, BigDecimal.ROUND_HALF_UP)
return swPrice
