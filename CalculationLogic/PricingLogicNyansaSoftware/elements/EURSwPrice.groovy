BigDecimal swPrice = out.SwPrice * libs.vmwareUtil.CacheManager.getEURLCFactor(out.LCFactor)
return swPrice.setScale(2, BigDecimal.ROUND_HALF_UP)
