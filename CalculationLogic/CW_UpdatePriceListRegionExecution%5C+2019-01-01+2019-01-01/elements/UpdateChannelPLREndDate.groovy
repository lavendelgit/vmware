import net.pricefx.common.api.InputType

api.local.errorMessages = []
if (!api.local.selectedProducts && api.local.selectedProducts?.size() == 0) {
    api.local.errorMessages << "Please select Products!"
}

List filter = [
        Filter.equal("name", "PriceListRegion"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()

List masterLaunchDateDiffSKUs = []
Map channelEndDatesinConfig = out.ChannelLaunchRegionConfig?.collectEntries { [(it.name): it.attribute5] }
List masterLaunchRegions = out.ChannelLaunchRegionConfig?.collect { it.name }
launchRegions = selectedRecords?.findAll { masterLaunchRegions.contains(it.attribute2) }
for (region in launchRegions) {
    if (region.attribute10 != channelEndDatesinConfig[region.attribute2]) {
        masterLaunchDateDiffSKUs << region
    }
}

List record = []
for (regionRecord in masterLaunchDateDiffSKUs) {
    selectedDate = out.ChannelLaunchRegionConfig?.find { it.name == regionRecord.attribute2 }?.attribute5
    record << populatePayload(regionRecord, selectedDate)

}

if (record?.size() > 0) {
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(record), true)
}

Map populatePayload(Map rowData, def selectedDate) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"    : rowData?.typedId,
                    "attribute10": selectedDate
            ],
            "oldValues"    : [
                    "version": rowData?.version,
                    "typedId": rowData?.typedId,
            ]
    ]
}
