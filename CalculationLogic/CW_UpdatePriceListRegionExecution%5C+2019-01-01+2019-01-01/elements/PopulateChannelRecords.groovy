import net.pricefx.common.api.InputType

String message

List filter = [
        Filter.equal("name", "ValidCombination"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()

Map channelLaunchDatesinConfig = out.ChannelLaunchRegionConfig?.collectEntries { [(it.name): it.attribute4] }
List channelRegions = out.ChannelLaunchRegionConfig?.collect { it.name }
List channelLaunchDateDiffSKUs = []

launchRegions = selectedRecords?.findAll { channelRegions.contains(it.attribute13) }
for (region in launchRegions) {
    if (region.attribute8 != channelLaunchDatesinConfig[region.attribute13]) {
        channelLaunchDateDiffSKUs << region
    }
}

List record = []
for (regionRecord in channelLaunchDateDiffSKUs) {
    selectedDate = out.ChannelLaunchRegionConfig?.find { it.name == regionRecord.attribute13 }?.attribute4
    record << populatePayload(regionRecord, selectedDate)
}
if (record?.size() > 0) {
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(record), true)
}

return


Map populatePayload(Map rowData, def selectedDate) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"   : rowData?.typedId,
                    "attribute8": selectedDate
            ],
            "oldValues"    : [
                    "version": rowData?.version,
                    "typedId": rowData?.typedId,
            ]
    ]
}
