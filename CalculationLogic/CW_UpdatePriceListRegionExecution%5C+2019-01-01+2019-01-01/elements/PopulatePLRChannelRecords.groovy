import net.pricefx.common.api.InputType

api.local.errorMessages = []
if (!api.local.selectedProducts && api.local.selectedProducts?.size() == 0) {
    api.local.errorMessages << "Please select Products!"
}
List filter = [
        Filter.equal("name", "PriceListRegion"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()

List channelLaunchDateDiffSKUs = []

Map channelLaunchDatesinConfig = out.ChannelLaunchRegionConfig?.collectEntries { [(it.name): it.attribute4] }
List channelLaunchRegions = out.ChannelLaunchRegionConfig?.collect { it.name }
launchRegions = selectedRecords?.findAll { channelLaunchRegions.contains(it.attribute2) }

for (region in launchRegions) {
    if (region.attribute9 != channelLaunchDatesinConfig[region.attribute2]) {
        channelLaunchDateDiffSKUs << region
    }
}

List record = []
for (regionRecord in channelLaunchDateDiffSKUs) {
    selectedDate = out.ChannelLaunchRegionConfig?.find { it.name == regionRecord.attribute2 }?.attribute4
    record << populatePayload(regionRecord, selectedDate)
}

if (record?.size() > 0) {
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(record), true)
}


Map populatePayload(Map rowData, def selectedDate) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"   : rowData?.typedId,
                    "attribute9": selectedDate
            ],
            "oldValues"    : [
                    "version": rowData?.version,
                    "typedId": rowData?.typedId,
            ]
    ]
}

