import net.pricefx.common.api.InputType

String message

List filter = [
        Filter.equal("name", "ValidCombination"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()

List pxRecords = []
List masterLaunchRegionConfig = out.MasterLaunchRegionConfig
List masterRegions = masterLaunchRegionConfig?.collect { it.name }
List channelRegions = out.ChannelLaunchRegionConfig?.collect { it.name }
for (masterRegion in masterRegions) {
    regionInfo = masterLaunchRegionConfig?.find { it.name == masterRegion }
    selectedRegionRecords = selectedRecords?.findAll { it.attribute13 == masterRegion }
    if (!selectedRegionRecords || selectedRegionRecords?.size() == 0) {
        selectedSKUs = selectedRecords?.collect { it.sku }?.unique()
        for (sku in selectedSKUs) {
            skuInfo = selectedRecords?.find { it.sku == sku }
            pxRecords << [
                    "ValidCombination",
                    skuInfo.sku,
                    skuInfo.attribute11,
                    skuInfo.attribute12,
                    skuInfo.attribute1,
                    skuInfo.attribute2,
                    regionInfo.attribute9,
                    regionInfo.attribute9,
                    regionInfo.attribute6,
                    regionInfo.attribute7,
                    masterRegion
            ]
        }
    }
}


if (pxRecords) {
    payload = [
            "data": [
                    "data"   : pxRecords,
                    "header" : [
                            "name",
                            "sku",
                            "attribute11",
                            "attribute12",
                            "attribute1",
                            "attribute2",
                            "attribute3",
                            "attribute5",
                            "attribute7",
                            "attribute9",
                            "attribute13"
                    ],
                    "options": [
                            "detectJoinFields"    : false,
                            "maxJoinFieldsLengths": []
                    ]
            ]]
    //response = api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
    return
}


