import net.pricefx.common.api.InputType

String message


List filter = [
        Filter.equal("name", "PriceListRegion"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()
List pxRecords = []
List channelLaunchRegionConfig = out.ChannelLaunchRegionConfig
Map channelLaunchDatesinConfig = out.ChannelLaunchRegionConfig?.collectEntries { [(it.name): it.attribute4] }

List channelRegions = out.ChannelLaunchRegionConfig?.collect { it.name }
for (channelRegion in channelRegions) {
    regionInfo = channelLaunchRegionConfig?.find { it.name == channelRegion }
    selectedRegionRecords = selectedRecords?.findAll { it.attribute3 == channelRegion }
    if (!selectedRegionRecords || selectedRegionRecords?.size() == 0) {
        selectedSKUs = selectedRecords?.collect { it.sku }?.unique()
        for (sku in selectedSKUs) {
            skuInfo = selectedRecords?.find { it.sku == sku }
            pxRecords << [
                    "PriceListRegion",
                    skuInfo.sku,
                    skuInfo.attribute4,
                    skuInfo.attribute5,
                    skuInfo.attribute6,
                    channelRegion,
                    channelRegion,
                    channelRegion?.split(' ')?.getAt(1)?.replaceAll("[0-9]", ""),
                    channelLaunchDatesinConfig[channelRegion]
            ]
        }
    }
}


if (pxRecords) {
    payload = [
            "data": [
                    "data"   : pxRecords,
                    "header" : [
                            "name",
                            "sku",
                            "attribute4",
                            "attribute5",
                            "attribute6",
                            "attribute2",
                            "attribute3",
                            "attribute1",
                            "attribute9",
                    ],
                    "options": [
                            "detectJoinFields"    : false,
                            "maxJoinFieldsLengths": []
                    ]
            ]]
    response = api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
    return
}
