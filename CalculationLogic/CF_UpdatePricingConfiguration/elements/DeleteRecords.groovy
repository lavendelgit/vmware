if (api.local.pxRecords) {
    Map deletePL = [
            "data": [
                    "filterCriteria": [
                            "operator"    : "and",
                            "_constructor": "AdvancedCriteria",
                            "criteria"    : [
                                    [
                                            "fieldName": "name",
                                            "operator" : "equals",
                                            "value"    : Constants.PRODUCT_RULES_TABLE_NAME
                                    ],
                                    [
                                            "fieldName": "sku",
                                            "operator" : "inSet",
                                            "value"    : api.local.skus
                                    ],
                                    [
                                            "fieldName": "attribute4",
                                            "operator" : "inSet",
                                            "value"    : out.PricingConfigCache?.values()?.collect { it.Currency } as List
                                    ],
                                    [
                                            "fieldName": "attribute5",
                                            "operator" : "inSet",
                                            "value"    : out.PricingConfigCache?.values()?.collect { it.Region } as List
                                    ]
                            ]
                    ]
            ]
    ]
    api.boundCall("boundcall", "delete/PX/batch", api.jsonEncode(deletePL), false)

    return
}