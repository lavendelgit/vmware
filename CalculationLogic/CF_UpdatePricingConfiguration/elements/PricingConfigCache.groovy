def skuStream = api.stream("PX3", "sku", null)
Map skuInfos = skuStream?.collectEntries {
    config ->
        [
                (config.sku + "_" + config.attribute1 + "_" + config.attribute2): [
                        SKU     : config.sku,
                        Currency: config.attribute1,
                        Region  : config.attribute2
                ]
        ]
}
skuStream?.close()
return skuInfos
