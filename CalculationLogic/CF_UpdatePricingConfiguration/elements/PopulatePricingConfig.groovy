Map pricingConfigCache = out.PricingConfigCache
api.local.pxRecords = []
api.local.skus = []

def rulesGenLib = libs.stdProductLib.RulesGenHelper
for (skuDetail in api.local.skuDetails) {
    Map pricingConfigs = pricingConfigCache?.findAll { it.value.SKU == skuDetail.sku }
    for (pricingConfig in pricingConfigs) {
        pXParams = [
                "sku"                 : skuDetail.sku,
                "Product"             : skuDetail.attribute1,
                "Offer"               : skuDetail.attribute2,
                "DC"                  : skuDetail.attribute9,
                "Base"                : skuDetail.attribute3,
                "Metric"              : skuDetail.attribute8,
                "Term"                : skuDetail.attribute12,
                "Payment"             : skuDetail.attribute14,
                "Tier"                : skuDetail.attribute18,
                "Segment"             : skuDetail.attribute10,
                "Version"             : skuDetail.attribute19,
                "Promotion"           : skuDetail.attribute21,
                "OS"                  : skuDetail.attribute7,
                "Quantity"            : skuDetail.attribute11,
                "PurchasingProgram"   : skuDetail.attribute17,
                "TermUoM"             : skuDetail.attribute13,
                "SupportType"         : skuDetail.attribute15,
                "SupportTier"         : skuDetail.attribute16,
                "Currency"            : pricingConfig.value.Currency,
                "Region"              : pricingConfig.value.Region,
                "SubRegion"           : skuDetail.attribute6,
                "Hosting"             : skuDetail.attribute22,
                "ProgrammaticDiscount": skuDetail.attribute23,
                "Rule"                : skuDetail.attribute20,
                "SAPBaseSKU"          : skuDetail.attribute24,
                "StandardProductSKU"  : skuDetail.attribute25,
        ]
        api.local.skus << skuDetail.sku
        api.local.pxRecords << rulesGenLib.populateSKURecord(Constants.PRODUCT_RULES_TABLE_NAME, pXParams)
    }
}
return