if (api.isSyntaxCheck())
    return

def prodStartRow = 0
def maxResults = api.getMaxFindResultsLimit()

//AddOn
def addOnSKUList = []
def addOnSKUTable = api.findLookupTable("VeloAddonSKUs")
while (addonProdList = api.find("MLTV", prodStartRow, maxResults, null,
        Filter.equal("lookupTable.id", addOnSKUTable.id),
        //Filter.notEqual("attribute11","New")
)
) {
    prodStartRow += addonProdList?.size()
    addonProdList?.each {
        addOnSKUList.add(it.name)
        api.trace(it.name, null, null)
    }
}

//Hardware
prodStartRow = 0
def hardwareSKUList = []
def hardwareSKUTable = api.findLookupTable("VeloHardwareSKUs")
while (hwProdList = api.find("MLTV", prodStartRow, maxResults, null,
        Filter.equal("lookupTable.id", hardwareSKUTable.id),
        //Filter.notEqual("attribute9","New")
)
) {
    prodStartRow += hwProdList?.size()
    hwProdList?.each {
        hardwareSKUList.add(it.name)
        api.trace(it.name, null, null)
    }
}

//Software
prodStartRow = 0
def softwareSKUList = []
def softwareSKUTable = api.findLookupTable("VeloSoftwareSKUs")
while (swProdList = api.find("MLTV", prodStartRow, maxResults, null,
        Filter.equal("lookupTable.id", softwareSKUTable.id),
        //Filter.notEqual("attribute11","New")
)
) {
    prodStartRow += swProdList?.size()
    swProdList?.each {
        softwareSKUList.add(it.name)
        api.trace(it.name, null, null)
    }
}

api.trace("Addon SKU", null, addOnSKUList)
api.trace("HW SKU", null, hardwareSKUList.toString())
api.trace("SW SKU", null, softwareSKUList.toString())
