/*if (api.isSyntaxCheck())  
	return

def hwSKU = []
def swSKU = []
def addOnSKU = []

def prodStartRow = 0
def maxResults = api.getMaxFindResultsLimit()

while(prodTab = api.find("P",prodStartRow,Filter.in("attribute8", "Velo Add-On", "Software", "Hardware Capex", "Rental", "Extended Replacement Service"),                          
                        )
     ){
  prodStartRow += prodTab?.size()
  if(prodTab){
    prodTab.each{      
      def sku = it.sku
      if(it.attribute8 == "Software")
      	swSKU.add(sku)
      else if(it.attribute8 == "Velo Add-On")
        addOnSKU.add(sku)
      else
        hwSKU.add(sku)
    }
  }
}

//AddOn
def addOnSKUList = []
prodStartRow = 0
def addOnSKUTable = api.findLookupTable("VeloAddonSKUs")
while(addonProdList = api.find("MLTV",prodStartRow, maxResults, null, Filter.equal("lookupTable.id",addOnSKUTable.id)    )
     ){
  prodStartRow += addonProdList?.size()
  addonProdList?.each{
    addOnSKUList.add(it.name)
  }
}

//Hardware
prodStartRow = 0
def hardwareSKUList = []
def hardwareSKUTable = api.findLookupTable("VeloHardwareSKUs")
while( hwProdList = api.find("MLTV",prodStartRow, maxResults, null, Filter.equal("lookupTable.id",hardwareSKUTable.id)    )                  
     ){
 prodStartRow += hwProdList?.size()
  hwProdList?.each{
    hardwareSKUList.add(it.name)
  }
}

//Software
prodStartRow = 0
def softwareSKUList = []
def softwareSKUTable = api.findLookupTable("VeloSoftwareSKUs")
while(swProdList = api.find("MLTV",prodStartRow, maxResults, null, Filter.equal("lookupTable.id",softwareSKUTable.id)    )
     ){
  prodStartRow += swProdList?.size()
  swProdList?.each{
    softwareSKUList.add(it.name)
  }
}

def addonCount1	= addOnSKUList.size()
def hwCount1	= hardwareSKUList.size()
def swCount1 	= softwareSKUList.size()

api.trace("Historical addonSKU Count :",  null, addOnSKU.size().toString())
api.trace("Historical hardwareSKU Count", null, hwSKU.size().toString())
api.trace("Historical softwareSKU Count", null, swSKU.size().toString())

api.trace("addonSKU Count Creaetd from Logic", null, addonCount1)
api.trace("hardwareSKU Count Creaetd from Logic", null, hwCount1)
api.trace("softwareSKU Count Creaetd from Logic", null, swCount1)

// Updating Flag in Addon SKU Table
addOnSKUList = addOnSKUList.minus(addOnSKU)
addOnSKUList.each{
    def record = [ 
      "lookupTableId":addOnSKUTable.id,        
      "lookupTableName":addOnSKUTable.uniqueName,       
      "name"	  : it, 
      "attribute11": "New"
    ]        
    api.addOrUpdate("MLTV", record)  
}

// Updating Flag in Hardware SKU Table
hardwareSKUList = hardwareSKUList.minus(hwSKU)
hardwareSKUList.each{
    def record = [ 
      "lookupTableId":hardwareSKUTable.id,        
      "lookupTableName":hardwareSKUTable.uniqueName,       
      "name"	  : it, 
      "attribute9": "New"
    ]        
    api.addOrUpdate("MLTV", record)    
}

// Updating Flag in Software SKU Table
softwareSKUList = softwareSKUList.minus(swSKU)
softwareSKUList.each{
    def record = [ 
      "lookupTableId":softwareSKUTable.id,        
      "lookupTableName":softwareSKUTable.uniqueName,       
      "name"	  : it, 
      "attribute11": "New"
    ]        
    api.addOrUpdate("MLTV", record)  
}

def addonCount2	= addOnSKUList.size()
def hwCount2	= hardwareSKUList.size()
def swCount2 	= softwareSKUList.size()

api.trace("New addonSKU Product Count", null, addonCount2)
api.trace("New hardwareSKU Product Count", null, hwCount2)
api.trace("New softwareSKU Product Count", null, swCount2)

api.trace("addOn Matches", null, addonCount1-addonCount2)
api.trace("hw Matches", null, hwCount1-hwCount2)
api.trace("sw Matches", null, swCount1-swCount2)*/