import com.googlecode.genericdao.search.Filter

api.local.approvedPriceGrid = []
int start = 0
List filters = [
        Filter.in(Constants.APPROVAL_STATUS_FIELD, Constants.APPROVED, Constants.AUTO_APPROVED),
        Filter.in(Constants.PRICEGRID_ID_FIELD, out.CacheLPGID),
        Filter.greaterOrEqual(Constants.APPROVAL_DATE_COLUMN, out.CFLastUpdateDateTime)
]
int maxResults = api.getMaxFindResultsLimit()
while (items = api.find("PGI", start, maxResults, null, [Constants.SKU_FIELD], *filters)) {
    start += items.size()
    api.local.approvedPriceGrid.addAll(items?.sku)
}
return

