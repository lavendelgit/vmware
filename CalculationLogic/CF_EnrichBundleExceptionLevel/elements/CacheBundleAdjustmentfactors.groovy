((Constants.FACTOR_ATTRIBUTE_MAPPING)?.keySet()).each { factor ->
    api.local[Constants.FACTOR_ATTRIBUTE_MAPPING[factor]] = Lib.cacheAndPopulateTermUoMAdjustments(api.local.bundleAdjustmentsCache, api.local.bundleAdjMetaData, factor)
}
return
