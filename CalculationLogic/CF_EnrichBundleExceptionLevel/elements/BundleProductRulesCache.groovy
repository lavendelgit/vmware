api.local.priceListRegionCache = [:]
Map currencyDetails = [:]
List fields = [Constants.BUNDLE_PRODUCT_RULES_STANDARD_PRODUCT_SKU_FIELD, Constants.BUNDLE_PRODUCT_RULES_CURRENCY_FIELD, Constants.BUNDLE_PRODUCT_RULES_PRICELISTREGION_FIELD]
List filter = [
        Filter.equal(Constants.NAME_FIELD, Constants.BUNDLE_PRODUCT_RULES_PX_TABLE),
        Filter.in(Constants.BUNDLE_PRODUCT_RULES_STANDARD_PRODUCT_SKU_FIELD, api.local.bundleProducts?.collect { (it.PartNumber) ?: (it.sku) })
]
productPriceListRegionStream = api.stream("PX30", Constants.BUNDLE_PRODUCT_RULES_STANDARD_PRODUCT_SKU_FIELD, fields, *filter)
productsPriceListRegionCache = productPriceListRegionStream?.collect { row -> row }
productPriceListRegionStream?.close()
for (standardProduct in productsPriceListRegionCache) {
    currencyDetails = [
            (Constants.CURRENCY)                : standardProduct.attribute4,
            (Constants.BUNDLE_PRICE_LIST_REGION): standardProduct.attribute5
    ]
    if (api.local.priceListRegionCache[standardProduct.attribute24]) {
        api.local.priceListRegionCache[standardProduct.attribute24] << currencyDetails
    } else {
        api.local.priceListRegionCache[standardProduct.attribute24] = [currencyDetails]
    }
}
return

