for (sku in api.local.bundleProducts) {
    stdSkuDetails = [
            (Constants.PRODUCT_GROUP): sku.ProductGroup,
            (Constants.PRODUCT)      : sku.Product,
            (Constants.OFFER)        : sku.Offer,
            (Constants.BASE)         : sku.Base
    ]
    List exceptionDetails = []
    List priceListRegions = api.local.priceListRegionCache[(sku?.PartNumber) ?: (sku?.sku)]
    Constants.LC_ATTRIBUTES.each { attribute ->
        Map exceptionAdjustmentCache = api.local[attribute + Constants.ADJUSTMENT_CACHE]?.findAll {
            it.value[Constants.PRODUCT_GROUP_FIELD_VALUE] == sku.ProductGroup
        }
        Map defaultAdjustmentCache = api.local[attribute + Constants.ADJUSTMENT_CACHE]?.findAll {
            it.value[Constants.PRODUCT_GROUP_FIELD_VALUE] == (Constants.DEFAULT_VALUE)
        }
        for (priceListRegion in priceListRegions) {
            sku[(Constants.CURRENCY)] = priceListRegion?.Currency
            sku[(Constants.BUNDLE_PRICE_LIST_REGION)] = priceListRegion?.PriceListRegion
            exceptionDetails << Lib.getExceptionsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, stdSkuDetails, sku, attribute)
        }
    }
    Map stdRecord = [:]
    uniqueExceptionDetails = exceptionDetails?.unique()?.sort { it?.Priority }
    attributeExceptionLevel = uniqueExceptionDetails?.findAll { it?.Priority == (Constants.ATTRIBUTE_EXCEPTION_LEVEL) }
    if (attributeExceptionLevel) {
        exceptionList = attributeExceptionLevel[Constants.LCEXCEPTION_LEVEL] as List
        exceptionListSf = attributeExceptionLevel[Constants.LCEXCEPTION_LEVEL_SF] as List
        exceptionListValue = attributeExceptionLevel[Constants.LCEXCEPTION_VALUES] as List
        String exception = attributeExceptionLevel[Constants.LCEXCEPTION_LEVEL]
        String exceptionSf = attributeExceptionLevel[Constants.LCEXCEPTION_LEVEL_SF]
        String exceptionValue = attributeExceptionLevel[Constants.LCEXCEPTION_VALUES]

        if (exception.size() > (Constants.CHARACTER_LENGTH)) {
            if (exceptionSf?.size() > (Constants.CHARACTER_LENGTH)) {
                exceptionLevel = Constants.warning
            } else {
                exceptionLevel = exceptionListSf.join(",")
            }
        } else {
            exceptionLevel = exceptionList?.join(",")
        }
        String exceptionValues

        if (exceptionValue.size() > (Constants.CHARACTER_LENGTH)) {
            exceptionLevel = Constants.warning
        } else {
            exceptionValues = exceptionListValue?.join(",")
        }
        factorsList = []
        if (exceptionSf?.size() <= (Constants.CHARACTER_LENGTH)) {
            factorsList = attributeExceptionLevel[Constants.LCFACTORS] as List
        }
        stdRecord = Lib.populateLCExceptionRecords(sku.PartNumber ?: sku.sku, Constants.EXCEPTION, exceptionLevel, exceptionValues, factorsList?.join(","))
    } else if (uniqueExceptionDetails?.findAll { it.Priority == (Constants.STANDARD_EXCEPTION_LEVEL) }) {
        stdRecord = Lib.populateExceptionRecords(sku.PartNumber ?: sku.sku, Constants.STANDARD, " ", " ", " ")
    }
    if (stdRecord) {
        api.addOrUpdate("PX50", stdRecord)
    }
}
return
