import groovy.transform.Field

@Field final String VERSION_FIELD = "version"
@Field final String TYPE_ID_FIELD = "typedId"
@Field final String CREATEDATE_FIELD = "createDate"
@Field final String CREATEDBY_FIELD = "createdBy"
@Field final String LASTUPDATEDATE_FIELD = "lastUpdateDate"
@Field final String LASTUPDATEBY_FIELD = "lastUpdateBy"
@Field final String OVERRIDE_FIELD = "attribute36"
@Field final String FLAG_FIELD = "attribute35"
@Field final String VALID_FROM_FIELD = "attribute43"
@Field final String VALID_TO_FIELD = "attribute44"
@Field final String SEGMENT_TYPE = "Segment Type"
@Field final String TYPE = "Type"
@Field final String SIZE = "Size"
@Field final String PREFIX_BASE = "B"
@Field final String PREFIX_OFFER = "O"
@Field final String ID_FIELD = "id"
@Field final String SKU_FIELD = "sku"
@Field final String NAME_FIELD = "name"
@Field final String TERM_BUNDLE_ADJ_FIELD = "attribute7"
@Field final String TERM_UOM_BUNDLE_ADJ_FILED = "attribute33"
@Field final String PRODUCTGROUP_BUNDLE_ADJ_FILED = "attribute45"
@Field final String VALID_FROM_DATE = "attribute41"
@Field final String EXCEPTION_FIELD = "attribute34"
@Field final String EXCEPTION_LEVEL_FIELD = "attribute35"
@Field final String EXCEPTION_VALUES_FIELD = "attribute40"
@Field final String FACTORS_FIELD = "attribute36"
@Field final String LCEXCEPTION_FIELD = "attribute41"
@Field final String LCEXCEPTION_LEVEL_FIELD = "attribute42"
@Field final String LCEXCEPTION_VALUES_FIELD = "attribute43"
@Field final String LCFACTORS_FIELD = "attribute44"
@Field final String PRODUCT_GROUP_FIELD = "attribute45"
@Field final String SKU_TYPE_FIELD = "attribute24"
@Field final String BUNDLE_PRODUCT_RULES_STANDARD_PRODUCT_SKU_FIELD = "attribute24"
@Field final String BUNDLE_PRODUCT_RULES_CURRENCY_FIELD = "attribute4"
@Field final String BUNDLE_PRODUCT_RULES_PRICELISTREGION_FIELD = "attribute5"
@Field final String BUNDLE = "Bundle"
@Field final String NULL_VALUE = " "
@Field final String PRIORITY = "Priority"
@Field final String STANDARD_PRODUCT_PX_TABLE = "StandardProducts"
@Field final String BUNDLE_ADJUSTMENT_PX_TABLE = "BundleAdjustment"
@Field final String BUNDLE_PRODUCT_RULES_PX_TABLE = "BundleProductRules"
@Field final String STATUS_FIELD = "status"
@Field final String INCREMENTAL = "incremental"
@Field final String INC_LOAD_DATE = "incLoadDate"
@Field final String READY = "READY"
@Field final String APPROVAL_STATUS_FIELD = "approvalState"
@Field final String APPROVAL_DATE_COLUMN = "approvalDate"
@Field final String APPROVED = "APPROVED"
@Field final String AUTO_APPROVED = "AUTO_APPROVED"
@Field final String CF_STATUS = "draft"
@Field final String CF_STATUS_VALUE = "false"
@Field final String CF_UNIQUE_NAME = "uniqueName"
@Field final String BUNDLE_PRODUCT_EXCEPTION_UPDATE_LABEL = "BundleProductExceptionUpdate"
@Field final String PRICEGRID_ID_FIELD = "priceGridId"
@Field final String LPG_ID_FIELD = "LPGId"
@Field final String CURRENCY_FIELD = "attribute24"
@Field final String REGION_FIELD = "attribute13"
@Field final String PROMOTION_FIELD = "attribute32"
@Field final String UPLIFT_FIELD = "attribute34"
@Field final String VOLUME_TIER_FIELD = "attribute28"
@Field final String DATA_CENTER_FIELD = "attribute20"
@Field final String PURCHASING_PROGRAM_FIELD = "attribute19"
@Field final String SEGMENT_FIELD = "attribute18"
@Field final String SUPPORT_TYPE_FIELD = "attribute17"
@Field final String SUPPORT_TIER_FIELD = "attribute16"
@Field final String TERM_FIELD = "attribute15"
@Field final String PAYMENT_FIELD = "attribute22"
@Field final String METRIC_FIELD = "attribute14"
@Field final String HOSTING_FIELD = "attribute40"
@Field final String OS_FIELD = "attribute38"
@Field final String PROGRAM_OPTION_FIELD = "attribute42"
@Field final String ADJUSTMENT_CACHE = "BundleAdjustmentCache"
@Field final String PRODUCT_GROUP = "ProductGroup"
@Field final String PRODUCT_GROUP_FIELD_VALUE = "Product Group"
@Field final String DEFAULT_VALUE = "*"
@Field final String PRODUCT = "Product"
@Field final String OFFER = "Offer"
@Field final String BASE = "Base"
@Field final String CURRENCY = "Currency"
@Field final String BUNDLE_PRICE_LIST_REGION = "PriceListRegion"
@Field final String PRICE_LIST_REGION = "Price List Region"
@Field final String PRICE_LIST_REGION_FACTOR = "PB Uplift"
@Field final String PRICE_LIST_FACTOR = "uplift"
@Field final String CURRENCY_ADJ_FIELD = "currency"
@Field final String PROMOTION = "Promotion"
@Field final String UPGRADE = "Upgrade"
@Field final String VOLUME_TIER = "Volume Tier Size"
@Field final String DATA_CENTER = "Data Center"
@Field final String PURCHASING_PROGRAM = "Purchasing Program"
@Field final String SEGMENT = "Segment Type"
@Field final String SUPPORT_TYPE = "Support Type"
@Field final String SUPPORT_TIER = "Support Tier"
@Field final String TERM = "Term"
@Field final String RETENTION_PERIOD = "Retention Period"
@Field final String PAYMENT = "Payment Type"
@Field final String METRIC = "Metric"
@Field final String HOSTING = "Hosting"
@Field final String OS = "OS"
@Field final String PROGRAM_OPTION = "Program Option"
@Field final String PS_TERM = "PS Term"
@Field final String PS_OPTION = "PS Option"
@Field final String TERM_FIELD_UOM = "Term UoM"
@Field final String RETENTION_PERIOD_UOM = "Retention Period UOM"
@Field final String FX_RATE = "Fx Rate"
@Field final String PB_UPLIFT = "PB Uplift"
@Field final String STANDARD = "Standard"
@Field final String EXCEPTION = "Exception"
@Field final String EXCEPTION_LEVEL = "Exception Level"
@Field final String EXCEPTION_LEVEL_SF = "Exception Level Sf"
@Field final String EXCEPTION_VALUES = "Exception Values"
@Field final String LC_EXCEPTION = "LC Exception"
@Field final String LCEXCEPTION_LEVEL = "LC Exception Level"
@Field final String LCEXCEPTION_LEVEL_SF = "LC Exception Level Sf"
@Field final String LCEXCEPTION_VALUES = "LC Exception Values"
@Field final String FACTORS = "Factors"
@Field final String LCFACTORS = "LC Exception Factors"
@Field final String PRE_PREFIX = "Extra"
@Field final String CURRENCY_FACTOR = "Fx Rate"
@Field final String warning = "Attribute length exceeded 70 characters"
@Field final String PRICELISTREGION_CURRENCY = "Price List Region_Currency"
@Field final String TERM_TERMUOM = "Term_Term UoM"
@Field final String RETENTIONPERIOD_RETENTIONUOM = "Retention Period_Retention Period UOM"
@Field final String PL_CR = "PL_CR"
@Field final String TERMSF_UOM = "TERM_TERMUOM"
@Field final String RETP_RETPUOM = "RETP_RETPUOM"
@Field final int CHARACTER_LENGTH = 70
@Field final int BASE_EXCEPTION_LEVEL = 2
@Field final int OFFER_EXCEPTION_LEVEL = 3
@Field final int STANDARD_EXCEPTION_LEVEL = 4
@Field final int OTHER_EXCEPTION_LEVEL = 5
@Field final int ATTRIBUTE_EXCEPTION_LEVEL = 1
@Field final List PRODUCT_HIERARCHIES = ["Product Group", "Product", "Offer", "Base"]
@Field final List FACTOR_ATTRIBUTES = [CURRENCY_FIELD, REGION_FIELD, PROMOTION_FIELD, UPLIFT_FIELD, VOLUME_TIER_FIELD, DATA_CENTER_FIELD, PURCHASING_PROGRAM_FIELD, SEGMENT_FIELD, SUPPORT_TYPE_FIELD, SUPPORT_TIER_FIELD, TERM_FIELD, PAYMENT_FIELD, METRIC_FIELD, HOSTING_FIELD, OS_FIELD, PROGRAM_OPTION_FIELD]
@Field final List SKU_ATTRIBUTES = ["metric", "promotion", "volumeTier", "dataCenter", "purchasingProgram", "segment", "supportType", "supportTier", "term", "retentionPeriod", "payment", "hosting", "OS", "programOption", "psTerm", "psOption"]
@Field final List LC_ATTRIBUTES = ["currency", "uplift"]
@Field final Map STD_PRODUCT_ATTRIBUTES = [
        "currency"         : "Currency",
        "region"           : "PriceListRegion",
        "metric"           : "Metric",
        "promotion"        : "Promotion",
        "upgrade"          : "Upgrade",
        "volumeTier"       : "VolumeTier",
        "dataCenter"       : "DataCenter",
        "purchasingProgram": "PurchasingProgram",
        "segment"          : "Segment",
        "supportType"      : "SupportType",
        "supportTier"      : "SupportTier",
        "term"             : "Term",
        "payment"          : "PaymentType",
        "hosting"          : "Hosting",
        "OS"               : "OS",
        "programOption"    : "ProgramOption"]
@Field final Map ATTRIBUTE_SHORT_FORMS = [
        (CURRENCY)            : "CR",
        (PRICE_LIST_REGION)   : "PL",
        (PROMOTION)           : "PROMO",
        (UPGRADE)             : "UPG",
        (VOLUME_TIER)         : "VT",
        (DATA_CENTER)         : "DC",
        (PURCHASING_PROGRAM)  : "PP",
        (SEGMENT)             : "SEG",
        (SUPPORT_TYPE)        : "PROD",
        (SUPPORT_TIER)        : "SUPT",
        (TERM)                : "TERM",
        (TERM_FIELD_UOM)      : "TERMUOM",
        (RETENTION_PERIOD)    : "RETP",
        (RETENTION_PERIOD_UOM): "RETPUOM",
        (PAYMENT)             : "PAY",
        (METRIC)              : "MTRC",
        (HOSTING)             : "HOS",
        (OS)                  : "OS",
        (PROGRAM_OPTION)      : "PO",
        (PS_TERM)             : "PST",
        (PS_OPTION)           : "PSO",
        (FX_RATE)             : "FxR",
        (PB_UPLIFT)           : "upl"]
@Field final Map FACTOR_ATTRIBUTE_MAPPING = [
        (CURRENCY_FIELD)          : "currencyBundleAdjustmentCache",
        (REGION_FIELD)            : "regionBundleAdjustmentCache",
        (UPLIFT_FIELD)            : "upliftBundleAdjustmentCache",
        (PROMOTION_FIELD)         : "promotionBundleAdjustmentCache",
        (VOLUME_TIER_FIELD)       : "volumeTierBundleAdjustmentCache",
        (DATA_CENTER_FIELD)       : "dataCenterBundleAdjustmentCache",
        (PURCHASING_PROGRAM_FIELD): "purchasingProgramBundleAdjustmentCache",
        (SEGMENT_FIELD)           : "segmentBundleAdjustmentCache",
        (SUPPORT_TYPE_FIELD)      : "supportTypeBundleAdjustmentCache",
        (SUPPORT_TIER_FIELD)      : "supportTierBundleAdjustmentCache",
        (TERM_FIELD)              : "termBundleAdjustmentCache",
        (PAYMENT_FIELD)           : "paymentBundleAdjustmentCache",
        (METRIC_FIELD)            : "metricBundleAdjustmentCache",
        (HOSTING_FIELD)           : "hostingBundleAdjustmentCache",
        (OS_FIELD)                : "OSBundleAdjustmentCache",
        (PROGRAM_OPTION_FIELD)    : "programOptionBundleAdjustmentCache"]


