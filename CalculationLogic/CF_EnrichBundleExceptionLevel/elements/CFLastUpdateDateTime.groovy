List filters = [Filter.equal(Constants.CF_STATUS, Constants.CF_STATUS_VALUE),
                Filter.equal(Constants.CF_UNIQUE_NAME, Constants.BUNDLE_PRODUCT_EXCEPTION_UPDATE_LABEL)]
String lastUpdatedDate = api.find("CF", 0, "-lastUpdateDate", *filters)?.collect { it.lastUpdateDate }.getAt(0)

return lastUpdatedDate

