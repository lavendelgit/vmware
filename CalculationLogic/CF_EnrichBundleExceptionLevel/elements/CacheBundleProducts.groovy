int start = 0
api.local.bundleProducts = []
List filters = [
        Filter.equal(Constants.NAME_FIELD, Constants.STANDARD_PRODUCT_PX_TABLE),
        Filter.in(Constants.SKU_FIELD, api.local.approvedPriceGrid),
        Filter.equal(Constants.SKU_TYPE_FIELD, Constants.BUNDLE)
]
int maxResults = api.getMaxFindResultsLimit()
while (items = api.namedEntities(api.find("PX50", start, maxResults, Constants.SKU_FIELD, *filters))) {
    start += items.size()
    api.local.bundleProducts.addAll(items)
}
return
