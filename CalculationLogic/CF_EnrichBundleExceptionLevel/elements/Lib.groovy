Map cacheAndPopulateTermUoMAdjustments(List adjustmentCache, Map metaDataCache, String factorAttribute) {

    Map factorAdjustmentCache = cacheAdjustments(adjustmentCache, metaDataCache, factorAttribute)
    factorAdjustmentCache = populateTermUoMAdjustments(factorAdjustmentCache)
    return factorAdjustmentCache
}

Map populateTermUoMAdjustments(Map factorAdjustmentCache) {
    Map populatedTermCache = [:]
    Map tempAdjustmentCache = [:]
    if (factorAdjustmentCache?.find { it.value.Term != null && it.value["Term UoM"] != null }) {
        factorAdjustmentCache.each { key, value -> tempAdjustmentCache[key] = value.clone() }
        populatedTermCache = populateTermAltUoMAdjustments(tempAdjustmentCache)
        if (populatedTermCache?.size() > 0 && factorAdjustmentCache?.size() > 0) {
            factorAdjustmentCache = factorAdjustmentCache + populatedTermCache
        }
    }
    return factorAdjustmentCache
}

Map populateTermAltUoMAdjustments(Map adjustmentCache) {
    String term
    String termUoM
    Map populatedTermCache = [:]
    Map destTerm = [:]
    adjustmentCache?.findAll { it.value.Term != null && it.value["Term UoM"] != null }?.each {
        term = it.value.Term
        termUoM = it.value["Term UoM"]
        destTerm = api.local.termUoMConversionCache[term + "_" + termUoM]
        if (destTerm) {
            it.value.Term = destTerm.DestTerm
            it.value["Term UoM"] = destTerm.DestUoM
            key = frameTermKeys(it.value)
            populatedTermCache[key] = it.value
        }
    }
    return populatedTermCache
}

Map cacheAdjustments(List adjustmentCache, Map metaData, String factorAttribute) {
    Map factorAdjustmentCache = [:]
    List adjustments = adjustmentCache?.findAll { it[factorAttribute] != null }
    List keys
    int termIndex
    int productIndex
    int productGroupIndex
    excludeList = [Constants.VERSION_FIELD, Constants.TYPE_ID_FIELD, Constants.ID_FIELD, Constants.NAME_FIELD, Constants.SKU_FIELD, Constants.CREATEDATE_FIELD, Constants.CREATEDBY_FIELD, Constants.LASTUPDATEDATE_FIELD, Constants.LASTUPDATEBY_FIELD, factorAttribute, Constants.OVERRIDE_FIELD, Constants.FLAG_FIELD, Constants.VALID_FROM_FIELD, Constants.VALID_TO_FIELD]
    adjustments?.each {
        keys = it.keySet().collect()
        keysList = keys?.minus(excludeList)
        termIndex = keysList?.indexOf(Constants.TERM_BUNDLE_ADJ_FIELD)
        if (termIndex != -1) {
            keysList = keysList?.minus([Constants.TERM_UOM_BUNDLE_ADJ_FILED])
            keysList = keysList.plus(termIndex + 1, Constants.TERM_UOM_BUNDLE_ADJ_FILED)
        }
        if (productGroupIndex != -1) {
            keysList = keysList?.minus([Constants.PRODUCTGROUP_BUNDLE_ADJ_FILED])
            keysList = keysList.plus(productIndex, Constants.PRODUCTGROUP_BUNDLE_ADJ_FILED)
        }
        keyNames = []
        keysList?.each {
            key ->
                keyNames << metaData[key]
        }
        keyValueList = []
        Map record = [:]
        keysList?.each {
            key ->
                if (it[key]) {
                    keyValueList << it[key]
                    record[metaData[key]] = it[key]
                }
        }
        key = libs.vmwareUtil.util.frameKey(keyValueList)
        record.Adjustment = it.attribute36 ?: it[factorAttribute]
        record.Keys = keyNames
        factorAdjustmentCache[key] = record
    }
    factorAdjustmentCache = factorAdjustmentCache?.sort { -it.value?.Keys?.size() }
    return factorAdjustmentCache
}

String frameTermKeys(Map termAdj) {
    String keyString
    List keys = termAdj?.Keys
    keys?.each {
        key ->
            keyString = keyString ? keyString + "_" + termAdj[key] : termAdj[key]
    }
    return keyString
}

Map getExceptionsForAttribute(Map exceptionAdjustmentCache, Map defaultAdjustmentCache, Map stdSKUDetail, Map sku, String attribute) {
    Map keyLists = getAdjustmentsFromCache(exceptionAdjustmentCache, sku)
    Map defaultkeyLists = getAdjustmentsFromCache(defaultAdjustmentCache, sku)
    Map skuAdjustment
    keyLists?.StdKeys?.unique()
    for (keyList in keyLists?.StdKeys) {
        skuAdjustment = getAdjustments(exceptionAdjustmentCache, stdSKUDetail.ProductGroup, stdSKUDetail.Product, stdSKUDetail.Offer, stdSKUDetail.Base, keyList.values().drop(4))
        if (skuAdjustment) {
            break
        }
    }

    if (!skuAdjustment) {
        defaultkeyLists?.StdKeys?.unique()
        for (keyList in defaultkeyLists?.StdKeys) {
            skuAdjustment = getAdjustments(defaultAdjustmentCache, stdSKUDetail.ProductGroup, stdSKUDetail.Product, stdSKUDetail.Offer, stdSKUDetail.Base, keyList.values().drop(4))
            if (skuAdjustment) {
                break
            }
        }
    }

    if (skuAdjustment) {
        if (Constants.SKU_ATTRIBUTES?.contains(attribute)) {
            exception = getExceptionDetails(skuAdjustment, sku)
        } else {
            exception = getLCExceptionDetails(skuAdjustment, sku, attribute)
        }
    } else {
        if (Constants.SKU_ATTRIBUTES?.contains(attribute)) {
            exception = [
                    (Constants.EXCEPTION)       : (Constants.NULL_VALUE),
                    (Constants.EXCEPTION_LEVEL) : (Constants.NULL_VALUE),
                    (Constants.EXCEPTION_VALUES): (Constants.NULL_VALUE),
                    (Constants.FACTORS)         : (Constants.NULL_VALUE),
                    (Constants.PRIORITY)        : (Constants.OTHER_EXCEPTION_LEVEL)
            ]
        } else {
            exception = [
                    (Constants.LC_EXCEPTION)      : (Constants.STANDARD),
                    (Constants.LCEXCEPTION_LEVEL) : (Constants.NULL_VALUE),
                    (Constants.LCEXCEPTION_VALUES): (Constants.NULL_VALUE),
                    (Constants.LCFACTORS)         : (Constants.NULL_VALUE),
                    (Constants.PRIORITY)          : (Constants.OTHER_EXCEPTION_LEVEL)
            ]
        }
    }
    return exception
}

Map getAdjustmentsFromCache(Map adjustmentsCache, Map sku) {
    Map keyLists = [:]
    List skuKeyLists = []
    Map keys = [:]
    adjustmentsCache?.each {
        adj ->
            keys = [:]
            adj.value.Keys?.each {
                key ->
                    if (key == Constants.SEGMENT_TYPE) {
                        key = key?.minus(Constants.TYPE).replaceAll("\\s", "")
                    } else {
                        key = key?.minus(Constants.SIZE).replaceAll("\\s", "")
                    }
                    if (sku[key])
                        keys[key] = sku[key]
            }
            skuKeyLists << keys
    }
    skuKeyLists = skuKeyLists?.minus(Constants.PRODUCT_HIERARCHIES)
    keyLists.StdKeys = skuKeyLists
    return keyLists

}

Map getAdjustments(Map adjustmentCache, String productGroup, String product, String offer, String base, List adjs) {
    convertedAdjusment = standardKeyConversion(adjustmentCache)
    sortedCombinations = getSortedCombinations(productGroup, product, offer, base, adjs)
    Map adjustment
    String key
    for (combination in sortedCombinations) {
        key = libs.vmwareUtil.util.frameKey(combination.value.getAt(0))
        adjustment = convertedAdjusment?.getAt(key?.toLowerCase())
        if (adjustment) {
            return adjustment
        }
    }
}

Map standardKeyConversion(Map keyAdjusments) {
    Map convertedAdjusments = [:]
    keyAdjusments?.each { key, value ->
        convertedAdjusments[key.toLowerCase()] = value
    }
    return convertedAdjusments
}

Map getSortedCombinations(String productGroup, String product, String offer, String base, List adjs) {
    List combinations = []
    combinations << [productGroup, "*"]
    combinations << [product, "*"]
    combinations << [offer, "*"]
    combinations << [base, "*"]
    for (adj in adjs) {
        combinations << [adj, "*"]
    }
    List combinationList = combinations?.combinations()
    return combinationList?.groupBy({ libs.vmwareUtil.CacheManager.getWeightage(it, "*") })?.sort { -it.key }
}

Map getExceptionDetails(Map adjustmentDetails, Map stdproddetails) {
    if ((adjustmentDetails.Base == stdproddetails.Base)) {
        attributeDetails = adjustmentDetails.Keys.minus(Constants.PRODUCT_HIERARCHIES) as List
        if (attributeDetails?.size() > 0) {
            return getExceptionLevel(adjustmentDetails, attributeDetails, (Constants.PREFIX_BASE), (Constants.PRE_PREFIX), "")
        }

    }
    if ((adjustmentDetails.Offer == stdproddetails.Offer) && (adjustmentDetails.Base == "*")) {
        attributeDetails = adjustmentDetails.Keys.minus(Constants.PRODUCT_HIERARCHIES) as List
        if (attributeDetails?.size() > 0) {
            return getExceptionLevel(adjustmentDetails, attributeDetails, (Constants.PREFIX_OFFER), (Constants.PRE_PREFIX), "")
        }

    }
    return [
            (Constants.EXCEPTION)       : (Constants.STANDARD),
            (Constants.EXCEPTION_LEVEL) : (Constants.NULL_VALUE),
            (Constants.EXCEPTION_VALUES): (Constants.NULL_VALUE),
            (Constants.FACTORS)         : (Constants.NULL_VALUE),
            (Constants.PRIORITY)        : (Constants.STANDARD_EXCEPTION_LEVEL)
    ]
}


Map getLCExceptionDetails(Map adjustmentDetails, Map stdproddetails, String attribute) {
    if ((adjustmentDetails.Base == stdproddetails.Base)) {
        attributeDetails = adjustmentDetails.Keys.minus(Constants.PRODUCT_HIERARCHIES) as List
        if (attributeDetails?.size() > 0) {
            return getExceptionLevel(adjustmentDetails, attributeDetails, (Constants.PREFIX_BASE), "", attribute)
        }
    }

    if ((adjustmentDetails.Offer == stdproddetails.Offer) && (adjustmentDetails.Base == "*")) {
        attributeDetails = adjustmentDetails.Keys.minus(Constants.PRODUCT_HIERARCHIES) as List
        if (attributeDetails?.size() > 0) {
            return getExceptionLevel(adjustmentDetails, attributeDetails, (Constants.PREFIX_OFFER), "", attribute)
        }
    }

    if ((adjustmentDetails.Product == stdproddetails.Product) && (adjustmentDetails.Offer == "*") && (adjustmentDetails.Base == "*")) {
        attributeDetails = adjustmentDetails.Keys.minus(Constants.PRODUCT_HIERARCHIES) as List
        if (attributeDetails?.size() > 0) {
            return getExceptionLevel(adjustmentDetails, attributeDetails, "P", "", attribute)
        }
    }
    return [
            (Constants.LC_EXCEPTION)      : (Constants.STANDARD),
            (Constants.LCEXCEPTION_LEVEL) : (Constants.NULL_VALUE),
            (Constants.LCEXCEPTION_VALUES): (Constants.NULL_VALUE),
            (Constants.LCFACTORS)         : (Constants.NULL_VALUE),
            (Constants.PRIORITY)          : (Constants.STANDARD_EXCEPTION_LEVEL)
    ]
}

Map getExceptionLevel(Map adjustmentDetails, List attributeDetails, String prefix, String excPrefix, String adjAttribute) {
    List exceptionlevelList = []
    List exceptionlevelSfList = []
    List exceptionvalueList = []
    if (excPrefix == (Constants.PRE_PREFIX)) {
        attributeDetails?.each { attribute ->
            exceptionlevelList << attribute
            exceptionvalueList << adjustmentDetails[attribute]
            exceptionlevelSfList << Constants.ATTRIBUTE_SHORT_FORMS[attribute]
        }
    } else {
        attributeDetails?.each { attribute ->
            String retainAttribute = attribute
            if ((adjAttribute == (Constants.CURRENCY_ADJ_FIELD)) && ((attribute == (Constants.CURRENCY)) || (attribute == (Constants.PRICE_LIST_REGION)))) {
                attribute = (Constants.CURRENCY_FACTOR)
                exceptionlevelList << attribute

            }
            if ((adjAttribute == (Constants.PRICE_LIST_FACTOR)) && ((attribute == (Constants.CURRENCY)) || (attribute == (Constants.PRICE_LIST_REGION)))) {
                attribute = (Constants.PRICE_LIST_REGION_FACTOR)
                exceptionlevelList << attribute
            }
            exceptionlevelList.unique()
            exceptionvalueList << adjustmentDetails[retainAttribute]
            exceptionlevelSfList << Constants.ATTRIBUTE_SHORT_FORMS[attribute]
        }

    }

    exceptionlevelList.add(0, prefix)
    exceptionlevelSfList.add(0, prefix)

    if (excPrefix == (Constants.PRE_PREFIX)) {
        return exceptionDetails = [
                (Constants.EXCEPTION)         : (Constants.EXCEPTION),
                (Constants.EXCEPTION_LEVEL)   : exceptionlevelList?.join("_"),
                (Constants.EXCEPTION_LEVEL_SF): exceptionlevelSfList?.join("_"),
                (Constants.EXCEPTION_VALUES)  : exceptionvalueList?.join("_"),
                (Constants.FACTORS)           : adjustmentDetails.Adjustment,
                (Constants.PRIORITY)          : (Constants.ATTRIBUTE_EXCEPTION_LEVEL)
        ]
    } else {
        exceptionDetails =
                [
                        (Constants.LC_EXCEPTION)        : (Constants.EXCEPTION),
                        (Constants.LCEXCEPTION_LEVEL)   : exceptionlevelList.join("_"),
                        (Constants.LCEXCEPTION_LEVEL_SF): exceptionlevelSfList.join("_"),
                        (Constants.LCEXCEPTION_VALUES)  : exceptionvalueList.join("_"),
                        (Constants.LCFACTORS)           : adjustmentDetails.Adjustment,
                        (Constants.PRIORITY)            : (Constants.ATTRIBUTE_EXCEPTION_LEVEL)
                ]
    }
}

Map populateExceptionRecords(String sku, String exceptionLevelField, String exceptionLevel, String exceptionValue, String factor) {
    return [
            (Constants.NAME_FIELD)            : Constants.STANDARD_PRODUCT_PX_TABLE,
            (Constants.SKU_FIELD)             : sku,
            (Constants.EXCEPTION_FIELD)       : exceptionLevelField,
            (Constants.EXCEPTION_LEVEL_FIELD) : exceptionLevel,
            (Constants.EXCEPTION_VALUES_FIELD): exceptionValue,
            (Constants.FACTORS_FIELD)         : factor
    ]

}

Map populateLCExceptionRecords(String sku, String exceptionLevelField, String exceptionLevel, String exceptionValue, String factor) {
    return [
            (Constants.NAME_FIELD)              : Constants.STANDARD_PRODUCT_PX_TABLE,
            (Constants.SKU_FIELD)               : sku,
            (Constants.LCEXCEPTION_FIELD)       : exceptionLevelField,
            (Constants.LCEXCEPTION_LEVEL_FIELD) : exceptionLevel,
            (Constants.LCEXCEPTION_VALUES_FIELD): exceptionValue,
            (Constants.LCFACTORS_FIELD)         : factor
    ]

}
