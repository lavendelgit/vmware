for (sku in api.local.bundleProducts) {
    stdSkuDetails = [
            (Constants.PRODUCT_GROUP): sku.ProductGroup,
            (Constants.PRODUCT)      : sku.Product,
            (Constants.OFFER)        : sku.Offer,
            (Constants.BASE)         : sku.Base
    ]
    List exceptionDetails = []
    Constants.SKU_ATTRIBUTES.each { attribute ->
        if (sku[Constants.STD_PRODUCT_ATTRIBUTES[attribute]]) {
            Map exceptionAdjustmentCache = api.local[attribute + Constants.ADJUSTMENT_CACHE]?.findAll {
                it.value[Constants.PRODUCT_GROUP_FIELD_VALUE] == sku.ProductGroup
            }
            Map defaultAdjustmentCache = api.local[attribute + Constants.ADJUSTMENT_CACHE]?.findAll {
                it.value[Constants.PRODUCT_GROUP_FIELD_VALUE] == (Constants.DEFAULT_VALUE)
            }
            exceptionDetails << Lib.getExceptionsForAttribute(exceptionAdjustmentCache, defaultAdjustmentCache, stdSkuDetails, sku, attribute)
        }
    }
    Map stdRecord = [:]
    uniqueExceptionDetails = exceptionDetails?.unique()?.sort { it?.Priority }
    attributeExceptionLevel = uniqueExceptionDetails?.findAll { it?.Priority == (Constants.ATTRIBUTE_EXCEPTION_LEVEL) }
    if (attributeExceptionLevel) {
        exceptionList = attributeExceptionLevel[Constants.EXCEPTION_LEVEL] as List
        exceptionListSf = attributeExceptionLevel[Constants.EXCEPTION_LEVEL_SF] as List
        exceptionListValue = attributeExceptionLevel[Constants.EXCEPTION_VALUES] as List

        String exception = attributeExceptionLevel[Constants.EXCEPTION_LEVEL]
        String exceptionSf = attributeExceptionLevel[Constants.EXCEPTION_LEVEL_SF]
        String exceptionValue = attributeExceptionLevel[Constants.EXCEPTION_VALUES]

        if (exception.size() > (Constants.CHARACTER_LENGTH)) {
            if (exceptionSf?.size() > (Constants.CHARACTER_LENGTH)) {
                exceptionLevel = Constants.warning
            } else {
                exceptionLevel = exceptionListSf.join(",")
            }
        } else {
            exceptionLevel = exceptionList?.join(",")
        }

        if (exceptionValue.size() > (Constants.CHARACTER_LENGTH)) {
            exceptionLevel = Constants.warning
        } else {
            exceptionValues = exceptionListValue?.join(",")
        }

        factorsList = []
        if (exceptionSf?.size() <= (Constants.CHARACTER_LENGTH)) {
            factorsList = attributeExceptionLevel[Constants.FACTORS] as List
        }
        stdRecord = Lib.populateExceptionRecords(sku.PartNumber ?: sku.sku, Constants.EXCEPTION, exceptionLevel, exceptionValues, factorsList?.join(","))
    } else if (uniqueExceptionDetails?.findAll { it.Priority == (Constants.STANDARD_EXCEPTION_LEVEL) }) {
        stdRecord = Lib.populateExceptionRecords(sku.PartNumber ?: sku.sku, Constants.STANDARD, " ", " ", " ")
    }
    if (stdRecord) {
        api.addOrUpdate("PX50", stdRecord)
    }
}
return
