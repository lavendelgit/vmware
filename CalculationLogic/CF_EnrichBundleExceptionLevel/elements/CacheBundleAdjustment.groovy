List products = api.local.bundleProducts?.Product?.unique()
List filters = [
        Filter.equal(Constants.NAME_FIELD, Constants.BUNDLE_ADJUSTMENT_PX_TABLE),
        Filter.or(
                Filter.in(Constants.PRODUCT, products),
                Filter.in(Constants.PRODUCT, "*")
        )
]
bundleAdjustmentStream = api.stream("PX50", "sku,attribute43", *filters)
api.local.bundleAdjustmentsCache = bundleAdjustmentStream?.collect { row -> row }
bundleAdjustmentStream?.close()
return