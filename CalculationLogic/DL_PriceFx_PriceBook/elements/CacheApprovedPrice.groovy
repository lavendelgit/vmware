import com.googlecode.genericdao.search.Filter

api.local.approvedPrice = []
List filter = []
int start = 0
if (api.currentItem(Constants.INCREMENTAL)) {
    filter = [
            Filter.in(Constants.APPROVAL_STATUS_FIELD, Constants.APPROVED, Constants.AUTO_APPROVED),
            Filter.in(Constants.PRICEGRID_ID_FIELD, out.LPGConfiguration),
            Filter.greaterOrEqual(Constants.APPROVAL_DATE_COLUMN, api.currentItem(Constants.INC_LOAD_DATE))
    ]
} else {
    filter = [
            Filter.in(Constants.APPROVAL_STATUS_FIELD, Constants.APPROVED, Constants.AUTO_APPROVED),
            Filter.in(Constants.PRICEGRID_ID_FIELD, out.LPGConfiguration)
    ]
}
int maxResults = api.getMaxFindResultsLimit()
while (items = api.namedEntities(api.find("PGI", start, maxResults, null, *filter))) {
    start += items.size()
    api.local.approvedPrice.addAll(items)
}
return

