String getApprovalReasons(String typedId) {
    pgId = typedId.toString().replaceAll("\\.PGI", "")
    reasons = api.findWorkflowInfo("PGI", pgId)?.activeStep?.comment ?: ""
    return reasons
}