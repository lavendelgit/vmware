import groovy.transform.Field

@Field final String PP_TABLE_NAME_SAP_PRICE_LIST_REGION = "SAPPriceListRegionName"
@Field final String PRODUCT_INPUT_NAME = "Product"