import net.pricefx.common.api.InputType

api.local.errorMessages = []
if (!api.local.selectedProducts && api.local.selectedProducts?.size() == 0) {
    api.local.errorMessages << "Please select Products!"
}
List filter = [
        Filter.equal("name", "ValidCombination"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()


List masterLaunchDateDiffSKUs = []
List masterEndDateDiffSKUs = []
Map masterLaunchDatesinConfig = out.MasterLaunchRegionConfig?.collectEntries { [(it.name): it.attribute6] }
Map masterEndDatesinConfig = out.MasterLaunchRegionConfig?.collectEntries { [(it.name): it.attribute7] }
List masterLaunchRegions = out.MasterLaunchRegionConfig?.collect { it.name }

launchRegions = selectedRecords?.findAll { masterLaunchRegions.contains(it.attribute13) }
for (region in launchRegions) {
    if (region.attribute7 != masterLaunchDatesinConfig[region.attribute13]) {
        masterLaunchDateDiffSKUs << region
    }
}


List record = []
for (regionRecord in masterLaunchDateDiffSKUs) {
    selectedDate = out.MasterLaunchRegionConfig?.find { it.name == regionRecord.attribute13 }?.attribute6
    record << populatePayload(regionRecord, selectedDate)
}

if (record?.size() > 0) {
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(record), true)
    recordStream = api.stream("PX20", null, *filter)
    selectedRecords = recordStream?.collect { it }
    recordStream?.close()

}


launchRegions = selectedRecords?.findAll { masterLaunchRegions.contains(it.attribute13) }
for (region in launchRegions) {
    if (region.attribute9 != masterEndDatesinConfig[region.attribute13]) {
        masterEndDateDiffSKUs << region
    }
}

endDateRecord = []
for (regionRecord in masterEndDateDiffSKUs) {
    selectedDate = out.MasterLaunchRegionConfig?.find { it.name == regionRecord.attribute13 }?.attribute7
    endDateRecord << populateMasterEndDatePayload(regionRecord, selectedDate)
}
if (endDateRecord?.size() > 0) {
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(endDateRecord), true)
}

Map populatePayload(Map rowData, def selectedDate) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"   : rowData?.typedId,
                    "attribute7": selectedDate
            ],
            "oldValues"    : [
                    "version": rowData?.version,
                    "typedId": rowData?.typedId,
            ]
    ]
}

Map populateMasterEndDatePayload(Map rowData, def selectedDate) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"   : rowData?.typedId,
                    "attribute9": selectedDate
            ],
            "oldValues"    : [
                    "version": rowData?.version,
                    "typedId": rowData?.typedId,
            ]
    ]
}

