List filter = [
        Filter.or(
                Filter.equal("attribute8", "Yes"),
                Filter.equal("attribute3", "Yes")
        )
]
return api.findLookupTableValues("PriceListRegion", *filter)