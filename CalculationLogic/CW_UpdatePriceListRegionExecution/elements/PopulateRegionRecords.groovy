import net.pricefx.common.api.InputType

String message

List filter = [
        Filter.equal("name", "ValidCombination"),
        Filter.in("sku", api.local.selectedProducts),
]

recordStream = api.stream("PX20", null, *filter)
selectedRecords = recordStream?.collect { it }
recordStream?.close()


List masterRegions = out.MasterLaunchRegionConfig?.collect { it.name }
List channelRegions = out.ChannelLaunchRegionConfig?.collect { it.name }
selectedRegionRecords = selectedRecords?.findAll { masterRegions.contains(it.attribute13) }
List record = []
for (region in selectedRegionRecords) {
    if (masterRegions.contains(region.attribute13) && !region.attribute3) {
        record << populatePayload(region, out.MasterLaunchRegionConfig?.find { it.name == region.attribute13 })
    }
}

if (record?.size() > 0) {
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(record), true)
    recordStream = api.stream("PX20", null, *filter)
    selectedRecords = recordStream?.collect { it }
    recordStream?.close()
}

selectedRegionRecords = selectedRecords?.findAll { channelRegions.contains(it.attribute13) }
record = []
for (region in selectedRegionRecords) {
    if (channelRegions.contains(region.attribute13) && !region.attribute4) {
        record << populateChannelRegionPayload(region, out.ChannelLaunchRegionConfig.find { it.name == region.attribute13 })
    }
}
if (record?.size() > 0) {
    api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(record), true)
}

if (api.local.errorMessages) {
    message = "<div style='color:red;'>${api.local.errorMessages.join(' ')}</div>"
}

def formSection = api.createConfiguratorEntry()
formSection.setMessage(message)

def button = formSection
        .createParameter(InputType.BUTTON, "OpenProducts")
        .setLabel(">> Open Product Table and verify your change")
        .addParameterConfigEntry("targetPage", "productExtensionsPage")

return formSection


Map populatePayload(Map rowData, Map regionInfo) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"   : rowData?.typedId,
                    "attribute3": regionInfo.attribute9,
                    "attribute5": regionInfo.attribute9,
                    "attribute7": regionInfo.attribute6,
                    "attribute9": regionInfo.attribute7,
            ],
            "oldValues"    : [
                    "version": rowData?.version,
                    "typedId": rowData?.typedId,
            ]
    ]
}

Map populateChannelRegionPayload(Map rowData, Map regionInfo) {

    return [
            "operationType": "update",
            "data"         : [
                    "typedId"    : rowData?.typedId,
                    "attribute4" : regionInfo.attribute9,
                    "attribute6" : regionInfo.attribute9,
                    "attribute8" : regionInfo.attribute4,
                    "attribute10": regionInfo.attribute5,
            ],
            "oldValues"    : [
                    "version": rowData?.version,
                    "typedId": rowData?.typedId,
            ]
    ]
}