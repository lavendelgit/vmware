import groovy.transform.Field

@Field final String PRODUCT_INPUT_NAME = "Product"
@Field final String CONFIRM_CONFIG_INPUT = "PriceListConfiguration"
@Field final String PRODUCT_INPUT_LABEL = "Select Product(s)"
@Field final String CONFIRM_CONFIG_LABEL = "Select/Update Price List Region Configuraions"