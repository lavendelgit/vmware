List filters = [Filter.equal(Constants.NAME, Constants.PX_STANDARDPRODUCTS),
                Filter.equal(Constants.PX_STANDARDPRODUCTS_PRODUCTGROUP, api.local.productGroup),
                Filter.equal(Constants.PX_STANDARDPRODUCTS_PRODUCT, api.local.product)]
if (api.local.offer) {
    filters << Filter.equal(Constants.PX_STANDARDPRODUCTS_OFFER, api.local.offer)
}
if (api.local.base) {
    filters << Filter.equal(Constants.PX_STANDARDPRODUCTS_BASE, api.local.base)
}
List fields = [Constants.SKU, Constants.PX_STANDARDPRODUCTS_PRODUCTGROUP, Constants.PX_STANDARDPRODUCTS_PRODUCT, Constants.PX_STANDARDPRODUCTS_OFFER, Constants.PX_STANDARDPRODUCTS_BASE]
Object stdProductsStream = api.stream("PX50", null, fields, *filters)
api.local.stdProductsValues = stdProductsStream?.collect { row -> row }
stdProductsStream?.close()
return