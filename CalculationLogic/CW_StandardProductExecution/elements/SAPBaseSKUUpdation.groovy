Map updateRecord = [:]
List errorMessages = out.PopulateErrorMessages
if (!errorMessages) {
    for (sapBaseSku in api.local.sapBaseSkuValues) {
        updateRecord = [
                (Constants.NAME)                      : Constants.PX_SABBASESKU,
                (Constants.SKU)                       : sapBaseSku.sku,
                (Constants.PX_SABBASESKU_PRODUCTGROUP): api.local.newProductGroup ?: (sapBaseSku?.attribute9),
                (Constants.PX_SABBASESKU_PRODUCT)     : api.local.newProduct ?: (sapBaseSku?.attribute6),
                (Constants.PX_SABBASESKU_OFFER)       : (api.local.newOffer) ?: (sapBaseSku?.attribute7),
                (Constants.PX_SABBASESKU_BASE)        : (api.local.newBase) ?: (sapBaseSku?.attribute8)
        ]
        api.addOrUpdate("PX20", updateRecord)
    }
}