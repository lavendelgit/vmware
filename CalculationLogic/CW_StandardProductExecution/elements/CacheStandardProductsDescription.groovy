api.local.standardProductsDescriptions = []
List filters = [
        Filter.equal(Constants.PP_STANDARDPRODUCTSDESCRIPTION_PRODUCTGROUP, api.local.productGroup),
        Filter.equal(Constants.PP_STANDARDPRODUCTSDESCRIPTION_PRODUCT, api.local.product)
]
if (api.local.offer) {
    filters << Filter.equal(Constants.PP_STANDARDPRODUCTSDESCRIPTION_OFFER, api.local.offer)
}
if (api.local.base) {
    filters << Filter.equal(Constants.PP_STANDARDPRODUCTSDESCRIPTION_BASE, api.local.base)
}
List fields = [Constants.NAME, Constants.PP_STANDARDPRODUCTSDESCRIPTION_PRODUCTGROUP, Constants.PP_STANDARDPRODUCTSDESCRIPTION_PRODUCT, Constants.PP_STANDARDPRODUCTSDESCRIPTION_OFFER, Constants.PP_STANDARDPRODUCTSDESCRIPTION_BASE]
api.local.standardProductsDescriptions = api.findLookupTableValues(Constants.PP_STANDARDPRODUCTSDESCRIPTION, fields, null, *filters)
return