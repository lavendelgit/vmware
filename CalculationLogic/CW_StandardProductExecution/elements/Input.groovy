api.local.productGroup = api.input(Constants.PRODUCTGROUP)
api.local.newProductGroup = api.input(Constants.NEWPRODUCTGROUP)
api.local.product = api.input(Constants.PRODUCT)
api.local.newProduct = api.input(Constants.NEWPRODUCT)
api.local.offer = api.input(Constants.OFFER)
api.local.newOffer = api.input(Constants.NEWOFFER)
api.local.base = api.input(Constants.BASE)
api.local.newBase = api.input(Constants.NEWBASE)
return