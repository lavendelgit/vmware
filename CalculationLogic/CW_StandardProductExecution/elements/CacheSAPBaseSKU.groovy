List filters = [
        Filter.equal(Constants.NAME, Constants.PX_SABBASESKU),
        Filter.equal(Constants.PX_SABBASESKU_PRODUCTGROUP, api.local.productGroup),
        Filter.equal(Constants.PX_SABBASESKU_PRODUCT, api.local.product)
]
if (api.local.offer) {
    filters << Filter.equal(Constants.PX_SABBASESKU_OFFER, api.local.offer)
}
if (api.local.base) {
    filters << Filter.equal(Constants.PX_SABBASESKU_BASE, api.local.base)
}
List fields = [Constants.SKU, Constants.PX_SABBASESKU_PRODUCTGROUP, Constants.PX_SABBASESKU_PRODUCT, Constants.PX_SABBASESKU_OFFER, Constants.PX_SABBASESKU_BASE]
Object sapBaseSkuStream = api.stream("PX20", null, fields, *filters)
api.local.sapBaseSkuValues = sapBaseSkuStream?.collect { row -> row }
sapBaseSkuStream?.close()
return