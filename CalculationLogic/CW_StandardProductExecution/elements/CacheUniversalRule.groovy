List filters = [
        Filter.equal(Constants.NAME, Constants.PX_UNIVERSALRULE),
        Filter.equal(Constants.PX_UNIVERSALRULE_PRODUCTGROUP, api.local.productGroup),
        Filter.equal(Constants.PX_UNIVERSALRULE_PRODUCT, api.local.product)
]
if (api.local.offer) {
    filters << Filter.equal(Constants.PX_UNIVERSALRULE_OFFER, api.local.offer)
}
if (api.local.base) {
    filters << Filter.equal(Constants.PX_UNIVERSALRULE_BASE, api.local.base)
}
Object universalRuleStream = api.stream("PX50", null, *filters)
api.local.universalRuleValues = universalRuleStream?.collect { row -> row }
universalRuleStream?.close()
return