api.local.basePriceData = []
stdBasePriceRecords = api.local.stdBasePrice?.collect()
for (basePrice in stdBasePriceRecords) {
    api.local.basePriceData << [
            (Constants.PX_STANDARDBASEPRICE),
            basePrice?.attribute1,
            api.targetDate(),
            (Constants.VALIDITY),
            basePrice?.sku,
            basePrice?.attribute2,
            (api.local.newProduct) ?: basePrice?.attribute3,
            (api.local.newOffer) ?: basePrice?.attribute4,
            (api.local.newBase) ?: basePrice?.attribute5,
            basePrice?.attribute6,
            basePrice?.attribute7,
            basePrice?.attribute8,
            basePrice?.attribute9,
            basePrice?.attribute10,
            basePrice?.attribute11,
            basePrice?.attribute12,
            basePrice?.attribute13,
            basePrice?.attribute14,
            basePrice?.attribute15,
            basePrice?.attribute16,
            basePrice?.attribute17,
            basePrice?.attribute18,
            basePrice?.attribute19,
            basePrice?.attribute20,
            basePrice?.attribute21,
            basePrice?.attribute22,
            basePrice?.attribute23,
            basePrice?.attribute24,
            basePrice?.attribute25,
            basePrice?.attribute26,
            basePrice?.attribute27,
            basePrice?.attribute28,
            basePrice?.attribute31,
            basePrice?.attribute32,
            basePrice?.attribute33,
            basePrice?.attribute34,
            basePrice?.attribute35,
            (api.local.newProductGroup) ?: basePrice?.attribute36

    ]
}
return