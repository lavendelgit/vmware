Map updateRecord = [:]
List errorMessages = out.PopulateErrorMessages
if (!errorMessages) {
    for (universalRule in api.local.universalRuleValues) {
        updateRecord = [
                (Constants.NAME)                               : Constants.PX_UNIVERSALRULE,
                (Constants.SKU)                                : universalRule.sku,
                (Constants.PX_UNIVERSALRULE_OS)                : universalRule.attribute4,
                (Constants.PX_UNIVERSALRULE_METRIC)            : universalRule.attribute5,
                (Constants.PX_UNIVERSALRULE_DATACENTER)        : universalRule.attribute6,
                (Constants.PX_UNIVERSALRULE_SEGMENT)           : universalRule.attribute7,
                (Constants.PX_UNIVERSALRULE_QUANTITY)          : universalRule.attribute8,
                (Constants.PX_UNIVERSALRULE_TERM)              : universalRule.attribute9,
                (Constants.PX_UNIVERSALRULE_TERMUOM)           : universalRule.attribute10,
                (Constants.PX_UNIVERSALRULE_PAYMENT_TYPE)      : universalRule.attribute11,
                (Constants.PX_UNIVERSALRULE_SUPPORT_TYPE)      : universalRule.attribute12,
                (Constants.PX_UNIVERSALRULE_SUPPORT_TIER)      : universalRule.attribute13,
                (Constants.PX_UNIVERSALRULE_PURCHASING_PROGRAM): universalRule.attribute14,
                (Constants.X_UNIVERSALRULE_TIER)               : universalRule.attribute15,
                (Constants.PX_UNIVERSALRULE_VERSION)           : universalRule.attribute16,
                (Constants.PX_UNIVERSALRULE_CURRENCY)          : universalRule.attribute18,
                (Constants.PX_UNIVERSALRULE_PRICELISTREGION)   : universalRule.attribute19,
                (Constants.PX_UNIVERSALRULE_SUBREGION)         : universalRule.attribute20,
                (Constants.PX_UNIVERSALRULE_PRODUCTGROUP)      : api.local.newProductGroup ?: (universalRule?.attribute28),
                (Constants.PX_UNIVERSALRULE_PRODUCT)           : api.local.newProduct ?: (universalRule?.attribute1),
                (Constants.PX_UNIVERSALRULE_OFFER)             : api.local.newOffer ?: (universalRule?.attribute2),
                (Constants.PX_UNIVERSALRULE_BASE)              : api.local.newBase ?: (universalRule?.attribute3)
        ]
        api.addOrUpdate("PX50", updateRecord)
    }
}