List filters = [
        Filter.equal(Constants.NAME, Constants.PX_PRICELISTREGION),
        Filter.equal(Constants.PX_PRICELISTREGION_PRODUCTGROUP, api.local.productGroup),
        Filter.equal(Constants.PX_PRICELISTREGION_PRODUCT, api.local.product)
]
if (api.local.offer) {
    filters << Filter.equal(Constants.PX_PRICELISTREGION_OFFER, api.local.offer)
}
if (api.local.base) {
    filters << Filter.equal(Constants.PX_PRICELISTREGION_BASE, api.local.base)
}
List fields = [Constants.SKU, Constants.PX_PRICELISTREGION_PRODUCTGROUP, Constants.PX_PRICELISTREGION_PRODUCT, Constants.PX_PRICELISTREGION_OFFER, Constants.PX_PRICELISTREGION_BASE, Constants.PX_PRICELISTREGION_CURRENCY, Constants.PX_PRICELISTREGION_CHANNELLAUNCHREGION]
Object priceListRegionStream = api.stream("PX20", null, fields, *filters)
api.local.priceListRegionsValues = priceListRegionStream?.collect { row -> row }
priceListRegionStream?.close()
return