List errorMessages = out.PopulateErrorMessages
if (!errorMessages) {
    api.local.stdAdjs?.each { data ->
        payload = [
                [
                        "operationType": "update",
                        "data"         : [
                                (Constants.TYPEDID)                          : data?.typedId,
                                (Constants.PX_STANDARD_ADJUSTMENT_VALID_FROM): data?.attribute41,
                                (Constants.PX_STANDARD_ADJUSTMENT_VALID_TO)  : api.targetDate(),
                        ],
                        "oldValues"    : [
                                (Constants.VERSION): data?.version,
                                (Constants.TYPEDID): data?.typedId,
                        ]
                ]
        ]
        api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(payload), true)
        return
    }
}