List filters = [
        Filter.equal(Constants.NAME, Constants.PX_STANDARDBASEPRICE),
        Filter.equal(Constants.PX_STANDARDBASEPRICE_PRODUCTGROUP, api.local.productGroup),
        Filter.equal(Constants.PX_STANDARDBASEPRICE_PRODUCT, api.local.product)
]
if (api.local.offer) {
    filters << Filter.equal(Constants.PX_STANDARDBASEPRICE_OFFER, api.local.offer)
}
if (api.local.base) {
    filters << Filter.equal(Constants.PX_STANDARDBASEPRICE_BASE, api.local.base)
}
def stdBasePriceStream = api.stream("PX50", null, *filters)
api.local.stdBasePrice = stdBasePriceStream?.collect { it }
stdBasePriceStream?.close()
return