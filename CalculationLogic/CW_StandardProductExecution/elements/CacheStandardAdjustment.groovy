List filters = [
        Filter.equal(Constants.NAME, Constants.PX_STANDARD_ADJUSTMENT_TABLE),
        Filter.equal(Constants.PX_STANDARD_ADJUSTMENT_PRODUCT_GROUP, api.local.productGroup),
        Filter.equal(Constants.PX_STANDARD_ADJUSTMENT_PRODUCT, api.local.product)
]
if (api.local.offer) {
    filters << Filter.equal(Constants.PX_STANDARD_ADJUSTMENT_OFFER, api.local.offer)
}
if (api.local.base) {
    filters << Filter.equal(Constants.PX_STANDARD_ADJUSTMENT_BASE, api.local.base)
}
def stdAdjStream = api.stream("PX50", null, *filters)
api.local.stdAdjs = stdAdjStream?.collect { it }
stdAdjStream?.close()
return