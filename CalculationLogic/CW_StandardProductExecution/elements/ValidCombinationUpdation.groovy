Map updateRecord = [:]
List errorMessages = out.PopulateErrorMessages
if (!errorMessages) {
    for (validCombination in api.local.validCombinationValues) {
        updateRecord = [
                (Constants.NAME)                            : Constants.PX_VALIDCOMBINATION,
                (Constants.SKU)                             : validCombination.sku,
                (Constants.PX_VALIDCOMBINATION_PRODUCTGROUP): api.local.newProductGroup ?: (validCombination?.attribute18),
                (Constants.PX_VALIDCOMBINATION_PRODUCT)     : api.local.newProduct ?: (validCombination?.attribute14),
                (Constants.PX_VALIDCOMBINATION_OFFER)       : api.local.newOffer ?: (validCombination?.attribute15),
                (Constants.PX_VALIDCOMBINATION_BASE)        : api.local.newBase ?: (validCombination?.attribute16)
        ]
        api.addOrUpdate("PX20", updateRecord)
    }
}