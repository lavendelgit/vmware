def formSection = api.createConfiguratorEntry()
if (!out.PopulateErrorMessages) {
    formSection.setMessage("<br /><br /><div style='font-size:16px;'>Updation Completed!</div>")
} else {
    formSection.setMessage("<br /><br /><div style='font-size:16px;'>Updation Unsuccessful!</div>")
}
return formSection