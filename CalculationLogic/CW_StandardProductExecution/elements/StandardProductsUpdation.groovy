Map updateRecord = [:]
List errorMessages = out.PopulateErrorMessages
if (!errorMessages) {
    for (standardProduct in api.local.stdProductsValues) {
        updateRecord = [
                (Constants.NAME)                            : Constants.PX_STANDARDPRODUCTS,
                (Constants.SKU)                             : standardProduct.sku,
                (Constants.PX_STANDARDPRODUCTS_PRODUCTGROUP): (api.local.newProductGroup) ?: (standardProduct?.attribute39),
                (Constants.PX_STANDARDPRODUCTS_PRODUCT)     : (api.local.newProduct) ?: (standardProduct?.attribute1),
                (Constants.PX_STANDARDPRODUCTS_OFFER)       : (api.local.newOffer) ?: (standardProduct?.attribute2),
                (Constants.PX_STANDARDPRODUCTS_BASE)        : (api.local.newBase) ?: (standardProduct?.attribute4)
        ]
        api.addOrUpdate("PX50", updateRecord)
    }
}
return