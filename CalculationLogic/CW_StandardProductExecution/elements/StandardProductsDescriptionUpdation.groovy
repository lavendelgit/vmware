Map updateRecord = [:]
def standardProductDescriptionTableName = api.findLookupTable(Constants.PP_STANDARDPRODUCTSDESCRIPTION)
List errorMessages = out.PopulateErrorMessages
if (!errorMessages) {
    for (standardProductsDescription in api.local.standardProductsDescriptions) {
        updateRecord = [
                (Constants.LOOKUPTABLENAME)                            : Constants.PP_STANDARDPRODUCTSDESCRIPTION,
                (Constants.LOOKUPTABLEID)                              : standardProductDescriptionTableName.id,
                (Constants.NAME)                                       : standardProductsDescription.name,
                (Constants.PP_STANDARDPRODUCTSDESCRIPTION_PRODUCTGROUP): api.local.newProductGroup ?: (standardProductsDescription?.attributeExtension___ProductGroup),
                (Constants.PP_STANDARDPRODUCTSDESCRIPTION_PRODUCT)     : api.local.newProduct ?: (standardProductsDescription?.attributeExtension___Product),
                (Constants.PP_STANDARDPRODUCTSDESCRIPTION_OFFER)       : (api.local.newOffer) ?: (standardProductsDescription?.attributeExtension___Offer),
                (Constants.PP_STANDARDPRODUCTSDESCRIPTION_BASE)        : (api.local.newBase) ?: (standardProductsDescription?.attributeExtension___Base)
        ]
        api.addOrUpdate("JLTV", updateRecord)
    }
}