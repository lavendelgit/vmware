Map updateRecord = [:]
List errorMessages = out.PopulateErrorMessages
if (!errorMessages) {
    for (priceListRegion in api.local.priceListRegionsValues) {
        updateRecord = [
                (Constants.NAME)                                  : Constants.PX_PRICELISTREGION,
                (Constants.SKU)                                   : priceListRegion.sku,
                (Constants.PX_PRICELISTREGION_CURRENCY)           : priceListRegion.attribute1,
                (Constants.PX_PRICELISTREGION_CHANNELLAUNCHREGION): priceListRegion.attribute2,
                (Constants.PX_PRICELISTREGION_PRODUCTGROUP)       : api.local.newProductGroup ?: (priceListRegion?.attribute11),
                (Constants.PX_PRICELISTREGION_PRODUCT)            : api.local.newProduct ?: (priceListRegion?.attribute4),
                (Constants.PX_PRICELISTREGION_OFFER)              : (api.local.newOffer) ?: (priceListRegion?.attribute5),
                (Constants.PX_PRICELISTREGION_BASE)               : (api.local.newBase) ?: (priceListRegion?.attribute6)
        ]
        api.addOrUpdate("PX20", updateRecord)
    }
}