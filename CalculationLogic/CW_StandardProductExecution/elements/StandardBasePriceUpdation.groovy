List errorMessages = out.PopulateErrorMessages
if (!errorMessages) {
    (api.local.stdBasePrice)?.each { data ->
        payload = [
                [
                        "operationType": "update",
                        "data"         : [
                                (Constants.TYPEDID)                       : data?.typedId,
                                (Constants.PX_STANDARDBASEPRICE_VALIDFROM): data?.attribute29,
                                (Constants.PX_STANDARDBASEPRICE_VALIDTO)  : api.targetDate()
                        ],
                        "oldValues"    : [
                                (Constants.VERSION): data?.version,
                                (Constants.TYPEDID): data?.typedId,
                        ]
                ]
        ]
        api.boundCall("boundcall", "update/PX/batch", api.jsonEncode(payload), true)
        return
    }
}