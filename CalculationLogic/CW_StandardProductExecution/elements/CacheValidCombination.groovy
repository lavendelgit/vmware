List filters = [
        Filter.equal(Constants.NAME, Constants.PX_VALIDCOMBINATION),
        Filter.equal(Constants.PX_VALIDCOMBINATION_PRODUCTGROUP, api.local.productGroup),
        Filter.equal(Constants.PX_VALIDCOMBINATION_PRODUCT, api.local.product)
]
if (api.local.offer) {
    filters << Filter.equal(Constants.PX_VALIDCOMBINATION_OFFER, api.local.offer)
}
if (api.local.base) {
    filters << Filter.equal(Constants.PX_VALIDCOMBINATION_BASE, api.local.base)
}
List fields = [Constants.SKU, Constants.PX_VALIDCOMBINATION_PRODUCTGROUP, Constants.PX_VALIDCOMBINATION_PRODUCT, Constants.PX_VALIDCOMBINATION_OFFER, Constants.PX_VALIDCOMBINATION_BASE]
Object validCombinationStream = api.stream("PX20", null, fields, *filters)
api.local.validCombinationValues = validCombinationStream?.collect { row -> row }
validCombinationStream?.close()
return