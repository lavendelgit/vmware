List errorMessages = []
if (api.local.base) {
    if (!api.local.offer) {
        errorMessages << "Please select the corresponding offer value!"
    }
}
if (!api.local.newProductGroup) {
    if (!api.local.newProduct) {
        if (!api.local.newOffer) {
            if (!api.local.newBase) {
                errorMessages << "Please enter new value"
            }
        }
    }
}
return errorMessages

