def message = ""
List errorMessages = out.PopulateErrorMessages
if (errorMessages) {
    message += """
<html>         
<body>
<h4><b><div style='color:red;'>${errorMessages.join(' ')}</div></b></h4>
</body>
</html>
   
 """
} else {
    message += """
<html>
<head></head>
<body>
<h4><b>Existing value has been changed to New value:</b></h4>
<table style='border: 1px solid black;border-collapse: collapse;width:100%'>
  <tr>
    <th style='border: 1px solid black;text-align:left'>Attribute</th>
    <th style='border: 1px solid black;text-align:left'>Existing value</th>
    <th style='border: 1px solid black;text-align:left'>New value</th>
  </tr>
<tr> 
   <td style='border: 1px solid black'>${Constants.PRODUCTGROUP}</td>
   <td style='border: 1px solid black'>${api.local.productGroup ?: Constants.PLACE_HOLDER}</td>
   <td style='border: 1px solid black'>${api.local.newProductGroup ?: api.local.productGroup ?: Constants.PLACE_HOLDER}</td>
    </tr>
<tr> 
    <td style='border: 1px solid black'>${Constants.PRODUCT}</td>
    <td style='border: 1px solid black'>${api.local.product ?: Constants.PLACE_HOLDER}</td>
    <td style='border: 1px solid black'>${api.local.newProduct ?: api.local.product ?: Constants.PLACE_HOLDER}</td>
</tr>
<tr> 
   <td style='border: 1px solid black'>${Constants.OFFER}</td>
    <td style='border: 1px solid black'>${api.local.offer ?: Constants.PLACE_HOLDER}</td>
    <td style='border: 1px solid black'>${api.local.newOffer ?: api.local.offer ?: Constants.PLACE_HOLDER}</td>
</tr>
<tr> 
     <td style='border: 1px solid black'>${Constants.BASE}</td>
     <td style='border: 1px solid black'>${api.local.base ?: Constants.PLACE_HOLDER}</td>
     <td style='border: 1px solid black'>${api.local.newBase ?: api.local.base ?: Constants.PLACE_HOLDER}</td>
</tr>
</table>
</body>
</html>
"""
    message += """
<br></br>
<html>
<head></head>
<body>
<table style='border: 1px solid black;border-collapse: collapse;width:100%'>
  <tr>
    <th style='border: 1px solid black;text-align:left'>TableName</th>
    <th style='border: 1px solid black;text-align:left'>No.of.Modified Record</th>
    </tr>
<tr>
    <td style='border: 1px solid black'>${Constants.PX_STANDARD_PRODUCTS}</td>
    <td style='border: 1px solid black'>${api.local.stdProductsValues?.size()}</td>
    </tr>
<tr>
    <td style='border: 1px solid black'>${Constants.PX_STANDARDADJUSTMENT_TABLE}</td>
    <td style='border: 1px solid black'>${(api.local.stdAdjs)?.size()}</td>
    </tr>
<tr>
    <td style='border: 1px solid black'>${Constants.PX_STANDARD_BASE_PRICE}</td>
    <td style='border: 1px solid black'>${(api.local.stdBasePrice)?.size()}</td>
    </tr>
<tr>
    <td style='border: 1px solid black'>${Constants.PX_PRICE_LIST_REGION}</td>
    <td style='border: 1px solid black'>${api.local.priceListRegionsValues?.size()}</td>
    </tr>
<tr>
    <td style='border: 1px solid black'>${Constants.PX_SAB_BASE_SKU}</td>
    <td style='border: 1px solid black'>${api.local.sapBaseSkuValues?.size()}</td>
    </tr>
<tr>
    <td style='border: 1px solid black'>${Constants.PX_UNIVERSAL_RULE}</td>
    <td style='border: 1px solid black'>${api.local.universalRuleValues?.size()}</td>
    </tr>
<tr>
    <td style='border: 1px solid black'>${Constants.PX_VALID_COMBINATION}</td>
    <td style='border: 1px solid black'>${api.local.validCombinationValues?.size()}</td>
    </tr>
<tr>
    <td style='border: 1px solid black'>${Constants.PP_STANDARD_PRODUCTS_DESCRIPTION}</td>
    <td style='border: 1px solid black'>${api.local.standardProductsDescriptions?.size()}</td>
    </tr>

</table>
</body>
</html>
   
 """
}
def formSection = api.createConfiguratorEntry()
formSection.setMessage(message)
return formSection