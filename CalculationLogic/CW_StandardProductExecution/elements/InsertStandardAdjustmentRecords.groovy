List errorMessages = out.PopulateErrorMessages
if (!errorMessages) {
    payload =
            [
                    "data": [
                            "data"   : api.local.data,
                            "header" : (Constants.PX_STANDARD_ADJUSTMENT_HEADER),
                            "options": [
                                    "detectJoinFields"    : false,
                                    "maxJoinFieldsLengths": []
                            ]
                    ]
            ]
    api.boundCall("boundcall", "loaddata/PX", api.jsonEncode(payload), false)
    return
}
