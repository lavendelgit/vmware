BigDecimal basePriceUSD = api.getElement("ResultPrice")
return Library.round(basePriceUSD * libs.vmwareUtil.util.getExchangeRate("JPY", "i3"), 6)