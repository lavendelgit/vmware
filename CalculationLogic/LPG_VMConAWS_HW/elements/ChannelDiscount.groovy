def channelDiscounts = api.findLookupTableValues("Channeldiscounts", Filter.equal("name", "VMC-AWS"))[0]

return [
        "disti"          : channelDiscounts["attribute1"],
        "distiResDefault": channelDiscounts["attribute2"],
        "distiResEmc"    : channelDiscounts["attribute3"],
]