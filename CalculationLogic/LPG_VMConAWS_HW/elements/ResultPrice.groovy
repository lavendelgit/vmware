BigDecimal usdPrice = Library.round(api.getElement("GlobalBasePrice"), 6)
if (usdPrice == null) {
    api.addWarning("Global Base Price not found.")
    return 0
}

return usdPrice