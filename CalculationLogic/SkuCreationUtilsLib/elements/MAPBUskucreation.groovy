/**
 * This function contructs MAPBU SKU
 * @param BU
 * @param Service
 * @param Datacenter
 * @param Quantity
 * @param SubService
 * @param PaymentTerm
 * @param PaymentType
 * @param Tier
 * @param License
 * @param Version
 * @param ProductType
 * @return
 */
def createMAPBUSKU(BU, Service, Datacenter, Quantity, SubService, PaymentTerm, PaymentType, Tier, License, Version, ProductType) {
    def sku = BU + Service + "-" + Datacenter + Quantity + SubService + "-" +
            PaymentTerm + PaymentType + Tier + "-" + License + Version + ProductType
    return sku
}

/**
 * Function to create short description
 * @param License
 * @param Service
 * @param Quantity
 * @param SubService
 * @param PaymentType
 * @param PaymentTerm
 * @param ServiceMap
 * @param SubServiceMap
 * @param PaymentTypeMap
 * @return
 */
def createShortDesc(License, Service, Quantity, SubService, PaymentType, PaymentTerm, ServiceMap, SubServiceMap, PaymentTypeMap) {
    def shortDesc
    switch (License) {
        case "A":
            shortDesc = "Academic" + " "
            break
        case "F":
            shortDesc = "U.S.Federal" + " "
            break
        default:
            break
    }

    if (Service && (License == "C")) shortDesc = ServiceMap[Service].attribute1 + " "
    else if (Service && (License != "C")) shortDesc += ServiceMap[Service].attribute1 + " "

    if (Quantity == 1) shortDesc += "per" + " "
    else if (Quantity > 1) shortDesc += "per" + " " + Quantity + " "

    if (SubService) shortDesc += SubServiceMap[SubService] + " " + "Commitment Plan" + " - "

    if (PaymentType) shortDesc += PaymentTypeMap[PaymentType] + " "

    if (PaymentTerm) shortDesc += PaymentTerm + " " + "months"

    return shortDesc
}

/**
 * Function to create long description
 * @param License
 * @param Service
 * @param Quantity
 * @param SubService
 * @param PaymentType
 * @param PaymentTerm
 * @return
 */
def createLongDesc(License, Service, Quantity, SubService, PaymentType, PaymentTerm) {
    def longDesc
    return longDesc
}

/**
 * Function to update MAPBUProducts PX table
 * @param sku
 * @param Service
 * @param Datacenter
 * @param Quantity
 * @param SubService
 * @param PaymentTerm
 * @param PaymentType
 * @param Tier
 * @param License
 * @param Version
 * @param ProductType
 * @param ShortDesc
 * @param LongDesc
 * @return
 */
def addOrUpdateMAPBUPXTable(sku, Service, Datacenter, Quantity, SubService, PaymentTerm, PaymentType, Tier, License, Version, ProductType, ShortDesc, LongDesc, baseSKU, termYears, segment, metric) {

    def mapbuRecord = [
            name       : "MAPBUProducts",
            sku        : sku,
            attribute11: ShortDesc,
            attribute12: LongDesc,
            attribute1 : Service,
            attribute2 : Datacenter,
            attribute3 : Quantity,
            attribute4 : SubService,
            attribute5 : PaymentTerm,
            attribute6 : PaymentType,
            attribute7 : Tier,
            attribute8 : License,
            attribute9 : Version,
            attribute10: ProductType,
            attribute13: termYears,
            attribute14: segment,
            attribute15: metric,
            attribute16: baseSKU

    ]
    if (mapbuRecord) api.addOrUpdate("PX20", mapbuRecord)
}