if (api.isSyntaxCheck())
    return

def longDesc, longDescDisclaimer
def table = api.findLookupTable("VeloSoftwareSKUsLongDescription")
def sku3, network, edition, software, gateway, support, service, paymentTerm, paymentType, licenceClass, renewal

Set distinctSoftwareProducts = out.CreateSoftwareSKU
distinctSoftwareProducts.each {

    network = it
    def sku = "NB-VC"
    api.global.editionList.each {
        edition = it
        api.global.softwareList.each {
            software = it
            api.global.gatewayList.each {
                gateway = it
                api.global.supportOptionList.each {
                    support = it
                    api.global.serviceLevelList.each {
                        service = it
                        api.global.billingTermList.each {
                            paymentTerm = it.key1.toInteger()
                            paymentType = it.key2

                            def pType
                            if (paymentType == "Prepaid") pType = "P"
                            else if (paymentType == "Monthly") pType = "M"
                            else if (paymentType == "Annual") pType = "A"

                            api.global.renewalList.each {
                                renewal = it
                                api.global.licenseClassList.each {
                                    licenceClass = it

                                    def sku1, sku2

                                    if (software && software.trim().length() > 0)
                                        sku1 = sku + network + "-" + edition + "-" + software + "-"
                                    else
                                        sku1 = sku + network + "-" + edition + "-"

                                    if (gateway && gateway.trim().length() > 0)
                                        sku2 = sku1 + gateway + "-"
                                    else
                                        sku2 = sku1

                                    if (renewal.trim().length() > 0)
                                        sku3 = sku2 + support + service + "-" + paymentTerm * 12 + pType + "-" + renewal + "-" + licenceClass
                                    else
                                        sku3 = sku2 + support + service + "-" + paymentTerm * 12 + pType + "-" + licenceClass

                                    longDesc = ""
                                    longDescDisclaimer = ""

                                    if (licenceClass == "C") {
                                        longDesc += "VMware SD-WAN by VeloCloud "

                                    } else if (licenceClass == "F") {
                                        longDesc += "U.S. Federal VMware SD-WAN by VeloCloud "
                                    }

                                    switch (network) {

                                        case "001G":
                                            longDesc += "1 Gbps "
                                            break
                                        case "002G":
                                            longDesc += "2 Gbps "
                                            break
                                        case "005G":
                                            longDesc += "5 Gbps "
                                            break

                                        case "010G":
                                            longDesc += "10 Gbps "
                                            break
                                        case "030G":
                                            longDesc += "30 Gbps "
                                            break
                                        case "050G":
                                            longDesc += "50 Gbps "
                                            break

                                        case "100M":
                                            longDesc += "100 Mbps "
                                            break
                                        case "200M":
                                            longDesc += "200 Mbps "
                                            break
                                        case "350M":
                                            longDesc += "350 Mbps "
                                            break
                                        case "500M":
                                            longDesc += "500 Mbps "
                                            break
                                        case "750M":
                                            longDesc += "750 Mbps "
                                            break

                                        case "050M":
                                            longDesc += "050 Mbps "
                                            break
                                        case "030M":
                                            longDesc += "030 Mbps "
                                            break
                                        case "010M":
                                            longDesc += "010 Mbps "
                                            break

                                        default:
                                            break
                                    }

                                    if (edition == "STD") {
                                        longDesc += "Standard Service Subscription "

                                    } else if (edition == "PRE") {
                                        longDesc += "Premium Service Subscription "
                                    } else if (edition == "ENT") {
                                        longDesc += "Enterprise Service Subscription "
                                    }


                                    switch (paymentTerm) {

                                        case "1":
                                            longDesc += "for 1 year, "
                                            break
                                        case "2":
                                            longDesc += "for 2 year, "
                                            break
                                        case "3":
                                            longDesc += "for 3 year, "
                                            break
                                        case "4":
                                            longDesc += "for 4 year, "
                                            break
                                        case "5":
                                            longDesc += "for 5 year, "
                                            break

                                        default:
                                            break
                                    }

                                    if (renewal == "R") {
                                        longDesc += "Renewal, "
                                    }

                                    switch (paymentType) {

                                        case "Prepaid":
                                            longDesc += "Prepaid, "
                                            break
                                        case "Monthly":
                                            longDesc += "Monthly, "
                                            break
                                        case "Annual":
                                            longDesc += "Annual, "
                                            break

                                        default:
                                            break
                                    }


                                    if (software == "SO" && gateway == "SG")
                                        longDesc += "Software SD-WAN Orchestrator with Software SD-WAN Gateway by VeloCloud, "
                                    else if (software == "HO" && gateway == "SG")
                                        longDesc += "Hosted SD-WAN Orchestrator with Software SD-WAN Gateway by VeloCloud, "
                                    else if (software == "SO" && gateway == "HG")
                                        longDesc += "Software SD-WAN Orchestrator with Hosted SD-WAN Gateway by VeloCloud, "
                                    else if (software == "HO" && gateway == "HG")
                                        longDesc += "Hosted SD-WAN Orchestrator with Hosted SD-WAN Gateway by VeloCloud, "

                                    else if (software == "SO" && (gateway.trim().length() == 0 || gateway == null))
                                        longDesc += "Software SD-WAN Orchestrator by VeloCloud, "
                                    else if (software == "HO" && (gateway.trim().length() == 0 || gateway == null))
                                        longDesc += "Hosted SD-WAN Orchestrator by VeloCloud, "
                                    else if (gateway == "SG" && (software.trim().length() == 0 || software == null))
                                        longDesc += "Software SD-WAN Gateway by VeloCloud, "
                                    else if (gateway == "HG" && (software.trim().length() == 0 || software == null))
                                        longDesc += "Hosted SD-WAN Gateway by VeloCloud, "


                                    switch (service) {

                                        case "S1":
                                            longDesc += "VeloCloud Basic "
                                            break

                                        case "S2":
                                            longDesc += "VeloCloud Production "
                                            break

                                        case "S3":
                                            longDesc += "VeloCloud Premier "
                                            break

                                        default:
                                            break
                                    }


                                    if (support == "L14") {
                                        longDesc += "Support Total (L1-4),  "
                                    } else if (support == "L34") {
                                        longDesc += "Support Backline (L3-4), "
                                    }

                                    switch (service) {

                                        case "S1":
                                            longDesc += "12 Hours/Day, per published Business Hours, Mon. thru Fri "
                                            break

                                        case "S2":
                                            longDesc += "24 Hour - 7 days a week "
                                            break

                                        case "S3":
                                            longDesc += "24 Hour - 7 days a week "
                                            break

                                        default:
                                            break
                                    }

                                    if (gateway == "HG") {
                                        longDesc += "Hosted VMware SD-WAN Gateways by VeloCloud in North America and EMEA. Hosted VMWare SD-WAN Gateways by VeloCloud for APAC or LATAM require separate purchase of an Add-on: hosted VMware SD-WAN Gateway by VeloCloud for APAC or Add-on: hosted VMware SD-WAN Gateway by VeloCloud for LATAM."
                                    }

                                    if (licenceClass == "F") {
                                        longDesc += "Restricted SKU: This SKU is for on premise deployment and will require approval from the VMware VeloCloud product team. Please contact your VMware VeloCloud account team representative for details. In addition, GSA Schedule sales of this SKU are prohibited."
                                    }

                                    if (paymentTerm != 2 &&
                                            paymentTerm != 4 &&
                                            licenceClass != "A" &&
                                            !sku3.contains("null") &&
                                            software.trim().length() > 0
                                    ) {
                                        def record = [
                                                "lookupTableId"                       : table?.id,
                                                "lookupTableName"                     : table.uniqueName,
                                                "name"                                : sku3,
                                                "attributeExtension___LongDescription": longDesc
                                        ]
                                        api.addOrUpdate("JLTV", record)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}