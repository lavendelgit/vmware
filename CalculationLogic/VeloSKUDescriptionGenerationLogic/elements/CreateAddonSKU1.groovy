if (api.isSyntaxCheck())
    return

def addOnSKU
def sku = "NB-VC-AD"
def maxResults = api.getMaxFindResultsLimit()
def table = api.findLookupTable("VeloAddonSKUs")
def wanTypeOption, purchaseOption, paymentTerm, paymentType, licenseClass

api.global.wanTypeOptionsList.each {
    wanTypeOption = it
    api.global.purchaseOptionsList.each {
        purchaseOption = it
        api.global.billingTermList.each {
            paymentTerm = it.key1.toInteger()
            paymentType = it.key2

            def pType
            if (paymentType == "Prepaid") pType = "P"
            else if (paymentType == "Monthly") pType = "M"
            else if (paymentType == "Annual") pType = "A"
            api.global.licenseClassList.each {
                licenseClass = it

                addOnSKU = sku + "-" + wanTypeOption + "-" + purchaseOption + "-" + paymentTerm * 12 + pType + "-" + licenseClass

                def record = [
                        "lookupTableId"  : table.id,
                        "lookupTableName": table.uniqueName,
                        "name"           : addOnSKU,
                        "attribute1"     : "Description",
                        "attribute2"     : "AD",
                        "attribute3"     : wanTypeOption,
                        "attribute4"     : purchaseOption,
                        "attribute5"     : "",
                        "attribute6"     : paymentTerm,
                        "attribute7"     : paymentType,
                        "attribute8"     : "Type 1",
                        "attribute9"     : "",
                        "attribute10"    : licenseClass,
                ]
                api.trace("sku", null, addOnSKU)
                if ((paymentTerm == 1 || paymentTerm == 3 || paymentTerm == 5) && (licenseClass == "C" || licenseClass == "F"))
                    api.addOrUpdate("MLTV", record)
            }
        }
    }
}