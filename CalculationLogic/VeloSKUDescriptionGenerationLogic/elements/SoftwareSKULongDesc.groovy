if (api.isSyntaxCheck())
    return

def startRow = 0
def sku, gateway, licenseClass, longDescDisclaimer1, longDescDisclaimer2
def maxResults = api.getMaxFindResultsLimit()
def table = api.findLookupTable("VeloSoftwareSKUs")


while (records = api.find("MLTV", startRow, maxResults, null, Filter.equal("lookupTable.id", table.id),
        Filter.equal("attribute10", "F")
)
) {
    startRow += records.size()

    records.each {
        longDescDisclaimer1 = ""
        longDescDisclaimer2 = ""
        sku = it.name
        gateway = it.attribute5
        licenseClass = it.attribute10

        if (gateway == "HG")
            longDescDisclaimer1 = "Hosted VMware SD-WAN Gateways by VeloCloud in North America and EMEA. Hosted VMWare SD-WAN Gateways by VeloCloud for APAC or LATAM require separate purchase of an Add-on: hosted VMware SD-WAN Gateway by VeloCloud for APAC or Add-on: hosted VMware SD-WAN Gateway by VeloCloud for LATAM."

        if (licenseClass == "F")
            longDescDisclaimer2 = "Restricted SKU: This SKU is for on premise deployment and will require approval from the VMware VeloCloud product team. Please contact your VMware VeloCloud account team representative for details. In addition, GSA Schedule sales of this SKU are prohibited."

        api.logWarn("longDescDisclaimer1*********", longDescDisclaimer1)
        api.logWarn("longDescDisclaimer2*********", longDescDisclaimer2)

        if (longDescDisclaimer1.trim().length() > 0)
            longDescDisclaimer1 = longDescDisclaimer1?.substring(0, 255)
        if (longDescDisclaimer2.trim().length() > 0)
            longDescDisclaimer2 = longDescDisclaimer2?.substring(0, 255)

        def record = [
                "lookupTableId"  : table?.id,
                "lookupTableName": table.uniqueName,
                "name"           : sku,
                "attribute13"    : longDescDisclaimer1,
                "attribute14"    : longDescDisclaimer2
        ]
        api.addOrUpdate("MLTV", record)
    }
}
api.trace("startRow", null, startRow)