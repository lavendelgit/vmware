if (api.isSyntaxCheck())
    return

def hwCapexLongDesc, hwCapexShortDesc
def hwRentalLongDesc, hwRentalShortDesc
def hwExtReplSvcLongDesc, hwExtReplSvcShortDesc

def maxResults = api.getMaxFindResultsLimit()
def table = api.findLookupTable("VeloHardwareSKUs")
def hwModel, paymentTerm, paymentType, replacementService
def licenseClass, locationClass, softwareClass

Set distinctReplSet = []
def replacementServiceTable = api.findLookupTable("ReplacementFactor")
def repFList = api.find("MLTV2", 0, maxResults, null, Filter.equal("lookupTable.id", replacementServiceTable.id)
)
if (repFList)
    repFList.each {
        distinctReplSet.add(it.key1)
    }


api.global.basePriceList.each {
    hwModel = it
    def capexSKU = "NB-VC" + "-" + hwModel
    def extReplSKU = "NB-VC" + hwModel
    def rentalSKU = "NB-VC" + hwModel

    api.global.locationOptionsList.each {
        def capexSKU1
        def extReplSKU1
        def rentalSKU1
        if (hwModel == "510LTE") {
            locationClass = it
            if (locationClass.trim().length() > 0) {
                capexSKU1 = capexSKU + "-" + locationClass
                extReplSKU1 = extReplSKU + "-" + locationClass
                rentalSKU1 = rentalSKU + "-" + locationClass
            }
        } else {
            locationClass = ""
            capexSKU1 = capexSKU
            extReplSKU1 = extReplSKU
            rentalSKU1 = rentalSKU
        }

        if (api.global.billingTermList) {
            api.global.billingTermList.each {
                paymentTerm = it.key1.toInteger()
                paymentType = it.key2

                distinctReplSet.each {
                    def pType
                    replacementService = it
                    if (paymentType == "Prepaid") pType = "P"
                    else if (paymentType == "Monthly") pType = "M"
                    else if (paymentType == "Annual") pType = "A"

                    api.global.renewalList.each {
                        def renewal = it
                        api.global.softwareList.each {
                            softwareClass = it

                            def capexSKU2
                            if (softwareClass.trim().length() > 0)
                                capexSKU2 = capexSKU1 + "-" + softwareClass + "-" + paymentTerm * 12 + "-" + pType + "-" + replacementService
                            else
                                capexSKU2 = capexSKU1 + "-" + paymentTerm * 12 + "-" + pType + "-" + replacementService

                            def extReplSKU2 = extReplSKU1 + "-" + "EXT" + "-" + replacementService + "-" + paymentTerm * 12 + pType

                            def rentalSKU2
                            if (renewal.trim().length() > 0)
                                rentalSKU2 = rentalSKU1 + "-" + paymentTerm * 12 + pType + "-" + renewal + "-" + replacementService
                            else
                                rentalSKU2 = rentalSKU1 + "-" + paymentTerm * 12 + pType + "-" + replacementService

                            api.global.licenseClassList.each {
                                licenseClass = it

                                def capexSKU3 = capexSKU2 + "-" + licenseClass
                                def extReplSKU3 = extReplSKU2 + "-" + licenseClass
                                def rentalSKU3 = rentalSKU2 + "-" + licenseClass


                                // Descriptions for CAPEX SKU's
                                switch (replacementService) {

                                    case "NBD":
                                        hwCapexLongDesc = "Edge Appliance with Next Business Day ship (NBD) Replacement Service."
                                        break

                                    case "SBD":
                                        hwCapexLongDesc = "Edge Appliance with Same Business Day ship (SBD) Replacement Service."
                                        break

                                    case "RTR":
                                        hwCapexLongDesc = "VeloCloud Return (RTR) Replacement Service is included in first year's hardware purchase."
                                        break

                                    case "4H7":
                                        hwCapexLongDesc = "Edge Appliance with 4 Hour Delivery 24x7 Replacement Service."
                                        break

                                    case "4H5":
                                        hwCapexLongDesc = "Edge Appliance with 4 Hour Delivery 9x5 Replacement Service."
                                        break
                                }

                                hwCapexShortDesc = "VMware SD-WAN Edge " + hwModel + " Appliance,"
                                switch (softwareClass) {
                                    case "SO":
                                        hwCapexShortDesc = hwCapexShortDesc + "Deployment: Software VCO "
                                        break

                                    case "HO":
                                        hwCapexShortDesc = hwCapexShortDesc + "Deployment: Hosted VCO "
                                        break
                                }

                                hwCapexShortDesc = hwCapexShortDesc + " for " + paymentTerm + " year - Capex - Per Edge - One Time Charge."
                                hwCapexShortDesc = hwCapexShortDesc + "This SKU to be used for Edge hardware purchase in conjunction with Standard, Enterprise or Premium Edition software purchase."


                                // Descriptions for Extended Replacement Service SKU's
                                def replacementServiceDesc = replacementService
                                if (replacementService == "4H7") {
                                    replacementServiceDesc = "4Hour RMA 24x7"
                                }
                                if (replacementService == "4H5") {
                                    replacementServiceDesc = "4Hour RMA 9x5"
                                }

                                if (locationClass.trim().length() > 0) {

                                    hwExtReplSvcShortDesc = paymentTerm + " yr Extended Replacement Service (" + replacementServiceDesc + ") for VMware SD-WAN Edge " + hwModel + "-" + locationClass + " - Per Edge - " + paymentTerm * 12 + " month " + paymentType + "."
                                    if (locationClass == "NAEU")
                                        hwExtReplSvcLongDesc = "VMware SD-WAN Edge " + hwModel + " North America and EMEA " + paymentTerm + "Yr" + " " + paymentType + " "
                                    else if (locationClass == "AP")
                                        hwExtReplSvcLongDesc = "VMware SD-WAN Edge " + hwModel + " Asia Pacific " + paymentTerm + "Yr" + " " + paymentType + " "
                                } else {
                                    hwExtReplSvcShortDesc = paymentTerm + " yr Extended Replacement Service (" + replacementServiceDesc + ") for VMware SD-WAN Edge " + hwModel + " - Per Edge - " + paymentTerm * 12 + " month " + paymentType + "."
                                    hwExtReplSvcLongDesc = "VMware SD-WAN Edge " + hwModel + " " + paymentTerm + "Yr" + " " + paymentType + " "
                                }

                                switch (replacementService) {

                                    case "NBD":
                                        hwExtReplSvcLongDesc = hwExtReplSvcLongDesc + "Extended Replacement Service; VeloCloud Next Business Day."
                                        break

                                    case "SBD":
                                        hwExtReplSvcLongDesc = hwExtReplSvcLongDesc + "Extended Replacement Service; VeloCloud Same Business Day."
                                        break

                                    case "RTR":
                                        hwExtReplSvcLongDesc = hwExtReplSvcLongDesc + "Extended Replacement Service; VeloCloud Return."
                                        break

                                    case "4H7":
                                        hwExtReplSvcLongDesc = hwExtReplSvcLongDesc + "Extended Replacement Service; VeloCloud 4Hour RMA 24x7."
                                        break

                                    case "4H5":
                                        hwExtReplSvcLongDesc = hwExtReplSvcLongDesc + "Extended Replacement Service; VeloCloud 4Hour RMA 9x5"
                                        break
                                }


                                // Hardware Rental SKU Description Generation
                                if (renewal.trim().length() > 0)
                                    hwRentalShortDesc = "VMware SD-WAN Edge " + hwModel + " appliance rental renewal - per Edge, Commitment Plan - " + paymentTerm * 12 + " month, " + paymentType
                                else
                                    hwRentalShortDesc = "VMware SD-WAN Edge " + hwModel + " appliance rental - per Edge, Commitment Plan - " + paymentTerm * 12 + " month, " + paymentType

                                hwRentalLongDesc = "VMware SD-WAN Edge " + hwModel + " appliance rental for " + paymentTerm + " year, " + paymentType + ", Replacement Service "
                                switch (replacementService) {

                                    case "NBD":
                                        hwRentalLongDesc = hwRentalLongDesc + "Next Business Day ship (NBD) included for the duration of the rental period."
                                        break

                                    case "SBD":
                                        hwRentalLongDesc = hwRentalLongDesc + " Same Business Day ship (SBD) included for the duration of the rental period."
                                        break

                                    case "RTR":
                                        hwRentalLongDesc = hwRentalLongDesc + "VeloCloud Return (RTR) included for the duration of the rental period."
                                        break

                                    case "4H7":
                                        hwRentalLongDesc = hwRentalLongDesc + "(4Hour RMA 24x7) included for the duration of the rental period."
                                        break

                                    case "4H5":
                                        hwRentalLongDesc = hwRentalLongDesc + "(4Hour RMA 9x5) included for the duration of the rental period."
                                        break
                                }


                                if (paymentTerm != 2 && paymentTerm != 4 &&
                                        paymentType == "Prepaid" && licenseClass != "A" &&
                                        !capexSKU3.contains("null") &&
                                        softwareClass?.trim()?.length() > 0) {

                                    def record1 = [
                                            "lookupTableId"  : table.id,
                                            "lookupTableName": table.uniqueName,
                                            "name"           : capexSKU3,
                                            "attribute1"     : hwCapexShortDesc,
                                            "attribute2"     : hwModel,
                                            "attribute3"     : paymentTerm,
                                            "attribute4"     : paymentType,
                                            "attribute5"     : replacementService,
                                            "attribute6"     : licenseClass,
                                            "attribute7"     : locationClass,
                                            "attribute8"     : softwareClass,
                                            "attribute10"    : "Hardware Capex",
                                            "attribute11"    : hwCapexLongDesc
                                            //"attribute12": renewal
                                    ]
                                    api.addOrUpdate("MLTV", record1)
                                    api.trace("hwCapexShortDesc", null, hwCapexShortDesc)
                                    api.trace("hwCapexLongDesc", null, hwCapexLongDesc)
                                }


                                if (licenseClass != "A" &&
                                        !extReplSKU3.contains("null") &&
                                        paymentTerm != 3 && paymentTerm != 5) {

                                    def record2 = [
                                            "lookupTableId"  : table.id,
                                            "lookupTableName": table.uniqueName,
                                            "name"           : extReplSKU3,
                                            "attribute1"     : hwExtReplSvcShortDesc,
                                            "attribute2"     : hwModel,
                                            "attribute3"     : paymentTerm,
                                            "attribute4"     : paymentType,
                                            "attribute5"     : replacementService,
                                            "attribute6"     : licenseClass,
                                            "attribute7"     : locationClass,
                                            "attribute8"     : "",
                                            "attribute10"    : "Extended Replacement Service",
                                            "attribute11"    : hwExtReplSvcLongDesc
                                            //"attribute12": renewal
                                    ]
                                    api.addOrUpdate("MLTV", record2)
                                    api.trace("hwExtReplSvcShortDesc", null, hwExtReplSvcShortDesc)
                                    api.trace("hwExtReplSvcLongDesc", null, hwExtReplSvcLongDesc)
                                }

                                if (paymentTerm != 2 && paymentTerm != 4 &&
                                        !rentalSKU3.contains("null") &&
                                        licenseClass != "A") {

                                    record3 = [
                                            "lookupTableId"  : table.id,
                                            "lookupTableName": table.uniqueName,

                                            "name"           : rentalSKU3,
                                            "attribute1"     : hwRentalShortDesc,
                                            "attribute2"     : hwModel,
                                            "attribute3"     : paymentTerm,
                                            "attribute4"     : paymentType,
                                            "attribute5"     : replacementService,

                                            "attribute6"     : licenseClass,
                                            "attribute7"     : locationClass,
                                            "attribute8"     : "",
                                            "attribute10"    : "Rental",
                                            "attribute11"    : hwRentalLongDesc,
                                            "attribute12"    : renewal
                                    ]
                                    api.addOrUpdate("MLTV", record3)
                                    api.trace("hwExtReplSvcShortDesc", null, hwRentalShortDesc)
                                    api.trace("hwExtReplSvcLongDesc", null, hwRentalLongDesc)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}