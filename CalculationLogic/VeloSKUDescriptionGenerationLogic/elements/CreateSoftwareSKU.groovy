if (api.isSyntaxCheck())
    return

def shortDesc, longDesc, longDescDisclaimer
def table = api.findLookupTable("VeloSoftwareSKUs")
def sku3, network, edition, software, gateway, support, service, paymentTerm, paymentType, licenceClass, renewal, descLength

def distinctSoftwareProducts = []
def maxResults = api.getMaxFindResultsLimit()
def ppTable = api.findLookupTable("SoftwareAdjustments")
def recList = api.find("MLTV2", 0, maxResults, null, Filter.equal("lookupTable.id", ppTable?.id))

if (recList)
    recList.each {
        distinctSoftwareProducts.add(it.key1)
    }

distinctSoftwareProducts.each {

    network = it
    def sku = "NB-VC"
    api.global.editionList.each {
        edition = it
        api.global.softwareList.each {
            software = it
            api.global.gatewayList.each {
                gateway = it
                api.global.supportOptionList.each {
                    support = it
                    api.global.serviceLevelList.each {
                        service = it
                        api.global.billingTermList.each {
                            paymentTerm = it.key1.toInteger()
                            paymentType = it.key2

                            def pType
                            if (paymentType == "Prepaid") pType = "P"
                            else if (paymentType == "Monthly") pType = "M"
                            else if (paymentType == "Annual") pType = "A"

                            api.global.renewalList.each {
                                renewal = it
                                api.global.licenseClassList.each {
                                    licenceClass = it

                                    def sku1, sku2

                                    if (software && software.trim().length() > 0)
                                        sku1 = sku + network + "-" + edition + "-" + software + "-"
                                    else
                                        sku1 = sku + network + "-" + edition + "-"

                                    if (gateway && gateway.trim().length() > 0)
                                        sku2 = sku1 + gateway + "-"
                                    else
                                        sku2 = sku1

                                    if (renewal.trim().length() > 0)
                                        sku3 = sku2 + support + service + "-" + paymentTerm * 12 + pType + "-" + renewal + "-" + licenceClass
                                    else
                                        sku3 = sku2 + support + service + "-" + paymentTerm * 12 + pType + "-" + licenceClass

                                    shortDesc = ""
                                    longDesc = ""
                                    longDescDisclaimer = ""

                                    if (licenceClass == "C") {
                                        shortDesc += "VMware SD-WAN by VeloCloud "
                                        longDesc += "VMware SD-WAN by VeloCloud "

                                    } else if (licenceClass == "F") {
                                        shortDesc += "U.S. Federal VMware SD-WAN by VeloCloud "
                                        longDesc += "U.S. Federal VMware SD-WAN by VeloCloud "
                                    }

                                    switch (network) {

                                        case "001G":
                                            longDesc += "1 Gbps "
                                            break
                                        case "002G":
                                            longDesc += "2 Gbps "
                                            break
                                        case "005G":
                                            longDesc += "5 Gbps "
                                            break

                                        case "010G":
                                            longDesc += "10 Gbps "
                                            break
                                        case "030G":
                                            longDesc += "30 Gbps "
                                            break
                                        case "050G":
                                            longDesc += "50 Gbps "
                                            break

                                        case "100M":
                                            longDesc += "100 Mbps "
                                            break
                                        case "200M":
                                            longDesc += "200 Mbps "
                                            break
                                        case "350M":
                                            longDesc += "350 Mbps "
                                            break
                                        case "500M":
                                            longDesc += "500 Mbps "
                                            break
                                        case "750M":
                                            longDesc += "750 Mbps "
                                            break

                                        case "050M":
                                            longDesc += "050 Mbps "
                                            break
                                        case "030M":
                                            longDesc += "030 Mbps "
                                            break
                                        case "010M":
                                            longDesc += "010 Mbps "
                                            break

                                        default:
                                            break
                                    }

                                    if (edition == "STD") {
                                        shortDesc += "Standard Edition "
                                        longDesc += "Standard Service Subscription "

                                    } else if (edition == "PRE") {
                                        shortDesc += "Premium Edition "
                                        longDesc += "Premium Service Subscription "
                                    } else if (edition == "ENT") {
                                        shortDesc += "Enterprise Edition "
                                        longDesc += "Enterprise Service Subscription "
                                    }


                                    if (software == "SO" && gateway == "SG")
                                        shortDesc += "(Software Orchestrator/Software Gateway) "
                                    else if (software == "HO" && gateway == "SG")
                                        shortDesc += "(Hosted Orchestrator/Software Gateway) "
                                    else if (software == "SO" && gateway == "HG")
                                        shortDesc += "(Software Orchestrator/Hosted Gateway) "
                                    else if (software == "HO" && gateway == "HG")
                                        shortDesc += "(Hosted Orchestrator/Hosted Gateway) "

                                    else if (software == "SO" && (gateway.trim().length() == 0 || gateway == null))
                                        shortDesc += "(Software Orchestrator) "
                                    else if (software == "HO" && (gateway.trim().length() == 0 || gateway == null))
                                        shortDesc += "(Hardware Orchestrator) "
                                    else if (gateway == "SG" && (software.trim().length() == 0 || software == null))
                                        shortDesc += "(Software Gateway) "
                                    else if (gateway == "HG" && (software.trim().length() == 0 || software == null))
                                        shortDesc += "(Hardware Gateway) "


                                    switch (network) {

                                        case "001G":
                                            shortDesc += "- 1 Gbps - Per Edge, "
                                            break
                                        case "002G":
                                            shortDesc += "- 2 Gbps - Per Edge, "
                                            break
                                        case "005G":
                                            shortDesc += "- 5 Gbps - Per Edge, "
                                            break


                                        case "010G":
                                            shortDesc += "- 10 Gbps - Per Edge, "
                                            break
                                        case "030G":
                                            shortDesc += "- 30 Gbps - Per Edge, "
                                            break
                                        case "050G":
                                            shortDesc += "- 50 Gbps - Per Edge, "
                                            break


                                        case "100M":
                                            shortDesc += "- 100 Mbps - Per Edge, "
                                            break
                                        case "200M":
                                            shortDesc += "- 200 Mbps - Per Edge, "
                                            break
                                        case "350M":
                                            shortDesc = shortDesc + "- 350 Mbps - Per Edge, "
                                            break
                                        case "500M":
                                            shortDesc += "- 500 Mbps - Per Edge, "
                                            break
                                        case "750M":
                                            shortDesc += "- 750 Mbps - Per Edge, "
                                            break

                                        case "050M":
                                            shortDesc += "- 050 Mbps - Per Edge, "
                                            break
                                        case "030M":
                                            shortDesc += "- 030 Mbps - Per Edge, "
                                            break
                                        case "010M":
                                            shortDesc += "- 010 Mbps - Per Edge, "
                                            break

                                        default:
                                            break
                                    }

                                    switch (paymentTerm) {

                                        case "1":
                                            shortDesc += "Commitment Plan - 12 month "
                                            longDesc += "for 1 year, "
                                            break
                                        case "2":
                                            shortDesc += "Commitment Plan - 24 month "
                                            longDesc += "for 2 year, "
                                            break
                                        case "3":
                                            shortDesc += "Commitment Plan - 36 month "
                                            longDesc += "for 3 year, "
                                            break
                                        case "4":
                                            shortDesc += "Commitment Plan - 48 month "
                                            longDesc += "for 4 year, "
                                            break
                                        case "5":
                                            shortDesc += "Commitment Plan - 60 month "
                                            longDesc += "for 5 year, "
                                            break

                                        default:
                                            break
                                    }

                                    if (renewal == "R") {
                                        longDesc += "Renewal, "
                                    }

                                    switch (paymentType) {

                                        case "Prepaid":
                                            shortDesc += "Prepaid "
                                            longDesc += "Prepaid, "
                                            break
                                        case "Monthly":
                                            shortDesc += "Monthly "
                                            longDesc += "Monthly, "
                                            break
                                        case "Annual":
                                            shortDesc += "Annual "
                                            longDesc += "Annual, "
                                            break

                                        default:
                                            break
                                    }


                                    if (software == "SO" && gateway == "SG")
                                        longDesc += "Software SD-WAN Orchestrator with Software SD-WAN Gateway by VeloCloud, "
                                    else if (software == "HO" && gateway == "SG")
                                        longDesc += "Hosted SD-WAN Orchestrator with Software SD-WAN Gateway by VeloCloud, "
                                    else if (software == "SO" && gateway == "HG")
                                        longDesc += "Software SD-WAN Orchestrator with Hosted SD-WAN Gateway by VeloCloud, "
                                    else if (software == "HO" && gateway == "HG")
                                        longDesc += "Hosted SD-WAN Orchestrator with Hosted SD-WAN Gateway by VeloCloud, "

                                    else if (software == "SO" && (gateway.trim().length() == 0 || gateway == null))
                                        longDesc += "Software SD-WAN Orchestrator by VeloCloud, "
                                    else if (software == "HO" && (gateway.trim().length() == 0 || gateway == null))
                                        longDesc += "Hosted SD-WAN Orchestrator by VeloCloud, "
                                    else if (gateway == "SG" && (software.trim().length() == 0 || software == null))
                                        longDesc += "Software SD-WAN Gateway by VeloCloud, "
                                    else if (gateway == "HG" && (software.trim().length() == 0 || software == null))
                                        longDesc += "Hosted SD-WAN Gateway by VeloCloud, "


                                    switch (service) {

                                        case "S1":
                                            longDesc += "VeloCloud Basic "
                                            break

                                        case "S2":
                                            longDesc += "VeloCloud Production "
                                            break

                                        case "S3":
                                            longDesc += "VeloCloud Premier "
                                            break

                                        default:
                                            break
                                    }


                                    if (support == "L14") {
                                        longDesc += "Support Total (L1-4),  "
                                    } else if (support == "L34") {
                                        longDesc += "Support Backline (L3-4), "
                                    }

                                    switch (service) {

                                        case "S1":
                                            longDesc += "12 Hours/Day, per published Business Hours, Mon. thru Fri "
                                            break

                                        case "S2":
                                            longDesc += "24 Hour - 7 days a week "
                                            break

                                        case "S3":
                                            longDesc += "24 Hour - 7 days a week "
                                            break

                                        default:
                                            break
                                    }

                                    if (renewal == "R")
                                        shortDesc += "- Renewal. "

                                    if (gateway == "HG") {
                                        longDesc += "Hosted VMware SD-WAN Gateways by VeloCloud in North America and EMEA. Hosted VMWare SD-WAN Gateways by VeloCloud for APAC or LATAM require separate purchase of an Add-on: hosted VMware SD-WAN Gateway by VeloCloud for APAC or Add-on: hosted VMware SD-WAN Gateway by VeloCloud for LATAM."
                                    }

                                    if (licenceClass == "F") {
                                        shortDesc += "Restricted SKU. Please see SKU long description. "
                                        longDesc += "Restricted SKU: This SKU is for on premise deployment and will require approval from the VMware VeloCloud product team. Please contact your VMware VeloCloud account team representative for details. In addition, GSA Schedule sales of this SKU are prohibited."
                                    }

                                    if (paymentTerm != 2 &&
                                            paymentTerm != 4 &&
                                            licenceClass != "A" &&
                                            !sku3.contains("null") &&
                                            software.trim().length() > 0
                                    ) {

                                        def record = [
                                                "lookupTableId"  : table?.id,
                                                "lookupTableName": table.uniqueName,
                                                "name"           : sku3,
                                                "attribute1"     : shortDesc,
                                                "attribute2"     : network,
                                                "attribute3"     : edition,

                                                "attribute4"     : software,
                                                "attribute5"     : gateway,
                                                "attribute6"     : support,

                                                "attribute7"     : service,
                                                "attribute8"     : paymentTerm,
                                                "attribute9"     : paymentType,
                                                "attribute10"    : licenceClass,
                                                "attribute15"    : renewal
                                        ]

                                        descLength = longDesc.length()

                                        if (descLength < 1019 && descLength > 764) {
                                            def descRecord = [
                                                    "attribute12": longDesc.substring(0, 255),
                                                    "attribute13": longDesc.substring(255, 510),
                                                    "attribute14": longDesc.substring(510, 764),
                                                    "attribute16": longDesc.substring(764, descLength)

                                            ]
                                            record << descRecord
                                        } else {

                                            if (descLength < 765 && descLength > 510) {
                                                def descRecord = [
                                                        "attribute12": longDesc.substring(0, 255),
                                                        "attribute13": longDesc.substring(255, 510),
                                                        "attribute14": longDesc.substring(510, descLength)
                                                ]
                                                record << descRecord
                                            } else {
                                                if (descLength < 511 && descLength > 255) {
                                                    def descRecord = [
                                                            "attribute12": longDesc.substring(0, 255),
                                                            "attribute13": longDesc.substring(255, descLength)
                                                    ]
                                                    record << descRecord
                                                } else {
                                                    if (descLength < 256) {
                                                        def descRecord = [
                                                                "attribute12": longDesc
                                                        ]
                                                        record << descRecord
                                                    }
                                                }
                                            }
                                        }
                                        api.addOrUpdate("MLTV", record)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
return distinctSoftwareProducts