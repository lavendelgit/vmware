def warningStyle = out.ConstantConfiguration
ResultMatrix resultMatrix = api.newMatrix((Constants.SKU), (Constants.SKU_DESCRIPTION), (Constants.SKU_TYPE), (Constants.BASE_PRICE_SKU), (Constants.PRODUCT), (Constants.OFFER), (Constants.BASE), (Constants.SEGMENT), (Constants.METRIC), (Constants.TERM), (Constants.TERM_UOM), (Constants.PAYMENT_TYPE), (Constants.PRODUCT_TYPE), (Constants.SUPPORT_TIER), (Constants.DATACENTER), (Constants.OS), (Constants.HOSTING), (Constants.PURCHASING_PROGRAM), (Constants.VOLUME_TIER), (Constants.PROGRAM_OPTION), (Constants.PS_TERM), (Constants.PS_OPTION), (Constants.VERSION), (Constants.PROMOTION), (Constants.RETENTION_PERIOD), (Constants.RETENTION_PERIOD_UOM), (Constants.EXCEPTION), (Constants.EXCEPTION_LEVEL), (Constants.FACTORS))
if (out.DependentSkuOption == (Constants.YES_OPTION)) {
    if (api.local.dependentSkuDetails) {
        for (data in api.local.dependentSkuDetails) {
            resultMatrix?.addRow(data)
        }
    } else {
        resultMatrix = api.newMatrix("")
        warningCell = resultMatrix?.styledCell(Constants.DEPENDENT_SKU_WARNING, warningStyle.TEXT_COLOR, warningStyle.BG_COLOR, warningStyle.TEXT_WEIGHT, warningStyle.TEXT_ALLIGN)
        resultMatrix?.addRow(warningCell)
    }
    return resultMatrix
} else {
    resultMatrix = api.newMatrix("")
    warningCell = resultMatrix?.styledCell(warningStyle.WARNING, warningStyle.TEXT_COLOR, warningStyle.BG_COLOR, warningStyle.TEXT_WEIGHT, warningStyle.TEXT_ALLIGN)
    resultMatrix?.addRow(warningCell)

    warningCell = resultMatrix?.styledCell(Constants.ALL_INPUTS_WARNING_MESSAGE, warningStyle.TEXT_COLOR, warningStyle.BG_COLOR, warningStyle.TEXT_WEIGHT, warningStyle.TEXT_ALLIGN)
    resultMatrix?.addRow(warningCell)

    return resultMatrix
}