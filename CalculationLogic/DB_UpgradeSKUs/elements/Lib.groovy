Map getStandardProductAttributes(List filters) {
    List standardProductdetails = []
    Map standardProducts = [:]
    recordStream = api.stream("PX50", null, *filters)
    standardProductdetails << recordStream?.collect { it }
    recordStream?.close()
    standardProducts = standardProductdetails?.flatten()?.collectEntries { [(it?.sku): it] }
    return standardProducts
}

Map getDescriptions(List skus) {
    return api.findLookupTableValues(Constants.STANDARD_PRODUCTS_DESCRIPTION_PP_TABLE, Filter.in(Constants.NAME, skus))?.collectEntries {
        [(it.name): [
                (Constants.SHORT_DESCRIPTION): it?.attributeExtension___ShortDescription,
                (Constants.LONG_DESCRIPTION) : it?.attributeExtension___LongDescription
        ]
        ]
    }
}