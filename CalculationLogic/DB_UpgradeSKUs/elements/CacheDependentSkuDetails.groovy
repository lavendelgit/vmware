if (out.DependentSkuOption == (Constants.NO_OPTION)) {
    return null
}
List filters = []
Map dependentDetails = [:]
api.local.dependentSkuDetails = []
List skus = api.local.upgradeSkus?.flatten()
filters << Filter.equal(Constants.NAME, Constants.STANDARD_PRODUCTS_PX_TABLE)
filters << Filter.in(Constants.STANDARD_PRODUCTS_BASE_PRICE_SKU_COLUMN, skus)
Map standardProducts = Lib.getStandardProductAttributes(filters)
List dependentSkus = standardProducts?.keySet() as List
Map descriptions = Lib.getDescriptions(dependentSkus)
for (standardProduct in standardProducts) {
    dependentDetails = [
            (Constants.SKU)                 : standardProduct?.key ?: (Constants.PLACE_HOLDER),
            (Constants.SKU_DESCRIPTION)     : (descriptions[standardProduct?.key]?.ShortDescription) ?: (Constants.PLACE_HOLDER),
            (Constants.SKU_TYPE)            : (standardProduct?.value?.attribute24) ?: (Constants.PLACE_HOLDER),
            (Constants.BASE_PRICE_SKU)      : (standardProduct?.value?.attribute14) ?: (Constants.PLACE_HOLDER),
            (Constants.PRODUCT)             : (standardProduct?.value?.attribute1) ?: (Constants.PLACE_HOLDER),
            (Constants.OFFER)               : (standardProduct?.value?.attribute2) ?: (Constants.PLACE_HOLDER),
            (Constants.BASE)                : (standardProduct?.value?.attribute4) ?: (Constants.PLACE_HOLDER),
            (Constants.SEGMENT)             : (standardProduct?.value?.attribute9) ?: (Constants.PLACE_HOLDER),
            (Constants.METRIC)              : (standardProduct?.value?.attribute5) ?: (Constants.PLACE_HOLDER),
            (Constants.TERM)                : (standardProduct?.value?.attribute6) ?: (Constants.PLACE_HOLDER),
            (Constants.TERM_UOM)            : (standardProduct?.value?.attribute21) ?: (Constants.PLACE_HOLDER),
            (Constants.PAYMENT_TYPE)        : (standardProduct?.value?.attribute7) ?: (Constants.PLACE_HOLDER),
            (Constants.PRODUCT_TYPE)        : (standardProduct?.value?.attribute19) ?: (Constants.PLACE_HOLDER),
            (Constants.SUPPORT_TIER)        : (standardProduct?.value?.attribute18) ?: (Constants.PLACE_HOLDER),
            (Constants.DATACENTER)          : (standardProduct?.value?.attribute3) ?: (Constants.PLACE_HOLDER),
            (Constants.OS)                  : (standardProduct?.value?.attribute22) ?: (Constants.PLACE_HOLDER),
            (Constants.HOSTING)             : (standardProduct?.value?.attribute23) ?: (Constants.PLACE_HOLDER),
            (Constants.PURCHASING_PROGRAM)  : (standardProduct?.value?.attribute20) ?: (Constants.PLACE_HOLDER),
            (Constants.VOLUME_TIER)         : (standardProduct?.value?.attribute8) ?: (Constants.PLACE_HOLDER),
            (Constants.PROGRAM_OPTION)      : (standardProduct?.value?.attribute25) ?: (Constants.PLACE_HOLDER),
            (Constants.PS_TERM)             : (standardProduct?.value?.attribute12) ?: (Constants.PLACE_HOLDER),
            (Constants.PS_OPTION)           : (standardProduct?.value?.attribute13) ?: (Constants.PLACE_HOLDER),
            (Constants.VERSION)             : (standardProduct?.value?.attribute10) ?: (Constants.PLACE_HOLDER),
            (Constants.PROMOTION)           : (standardProduct?.value?.attribute11) ?: (Constants.PLACE_HOLDER),
            (Constants.RETENTION_PERIOD)    : (standardProduct?.value?.attribute31) ?: (Constants.PLACE_HOLDER),
            (Constants.RETENTION_PERIOD_UOM): (standardProduct?.value?.attribute32) ?: (Constants.PLACE_HOLDER),
            (Constants.EXCEPTION)           : (standardProduct?.value?.attribute34) ?: (Constants.PLACE_HOLDER),
            (Constants.EXCEPTION_LEVEL)     : (standardProduct?.value?.attribute35) ?: (Constants.PLACE_HOLDER),
            (Constants.FACTORS)             : (standardProduct?.value?.attribute36) ?: (Constants.PLACE_HOLDER)
    ]
    api.local.dependentSkuDetails << dependentDetails
}
return


