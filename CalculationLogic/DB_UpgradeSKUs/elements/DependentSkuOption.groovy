def dependentSku = api.option(Constants.DEPENDENT_SKU_INCLUDE_OPTION, [(Constants.YES_OPTION), (Constants.NO_OPTION)])
parameter = api.getParameter(Constants.DEPENDENT_SKU_INCLUDE_OPTION)
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
}
return dependentSku