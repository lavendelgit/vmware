List skus = api.local.upgradeSkus?.flatten()
Map baseDetails = [:]
api.local.baseDetails = []
Map baseSkus = api.local.baseToUpgrade?.findAll { it.key2 in skus }?.unique()?.flatten()?.collectEntries { [(it.key2): (it.key1)] }
Map destinationSkus = api.local.upgradeToDestination?.findAll { it.key1 in skus }?.unique()?.flatten()?.collectEntries { [(it.key1): (it.key2)] }
Map upgradeDescriptions = Lib.getDescriptions(skus)
Map baseDescriptions = Lib.getDescriptions(baseSkus?.values() as List)
Map destinationDescriptions = Lib.getDescriptions(destinationSkus?.values() as List)
for (sku in skus) {
    baseSku = baseSkus[sku]
    destinationSku = destinationSkus[sku]
    baseDetails = [(sku): [
            (Constants.BASESKU)                   : baseSku ?: (Constants.PLACE_HOLDER),
            (Constants.BASESKU_DESCRIPTION)       : (baseDescriptions[baseSku]?.ShortDescription) ?: (Constants.PLACE_HOLDER),
            (Constants.UPGRADESKU)                : sku ?: (Constants.PLACE_HOLDER),
            (Constants.UPGRADESKU_DESCRIPTION)    : (upgradeDescriptions[sku]?.ShortDescription) ?: (Constants.PLACE_HOLDER),
            (Constants.DESTINATIONSKU)            : destinationSku ?: (Constants.PLACE_HOLDER),
            (Constants.DESTINATIONSKU_DESCRIPTION): (destinationDescriptions[destinationSku]?.ShortDescription) ?: (Constants.PLACE_HOLDER)
    ]
    ]
    api.local.baseDetails << baseDetails
}
return
