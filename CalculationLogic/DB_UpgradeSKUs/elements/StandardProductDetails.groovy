List filters = []
Map data = [:]
api.local.data = []
List baseData = api.local.baseDetails
List skus = api.local.upgradeSkus?.flatten()
filters << Filter.equal(Constants.NAME, Constants.STANDARD_PRODUCTS_PX_TABLE)
filters << Filter.in(Constants.STANDARD_PRODUCTS_SKU_COLUMN, skus)
Map standardProducts = Lib.getStandardProductAttributes(filters)
for (standardProduct in standardProducts) {
    data = [
            (Constants.BASE_SKU)                   : baseData[standardProduct?.key]?.BaseSku?.getAt(0),//sku?.sku
            (Constants.BASE_SKU_DESCRIPTION)       : baseData[standardProduct?.key]?.BaseSkuDescription?.getAt(0),
            (Constants.DESTINATION_SKU)            : baseData[standardProduct?.key]?.DestinationSku?.getAt(0),
            (Constants.DESTINATION_SKU_DESCRIPTION): baseData[standardProduct?.key]?.DestinationDescription?.getAt(0),
            (Constants.UPGRADE_SKU)                : standardProduct?.key,
            (Constants.UPGRADE_SKU_DESCRIPTION)    : baseData[standardProduct?.key]?.UpgradeDescription?.getAt(0),
            (Constants.SKU_TYPE)                   : (standardProduct?.value?.attribute24) ?: (Constants.PLACE_HOLDER),//sp[sku]->sku?.attr24
            (Constants.BASE_PRICE_SKU)             : (standardProduct?.value?.attribute14) ?: (Constants.PLACE_HOLDER),
            (Constants.PRODUCT)                    : (standardProduct?.value?.attribute1) ?: (Constants.PLACE_HOLDER),
            (Constants.OFFER)                      : (standardProduct?.value?.attribute2) ?: (Constants.PLACE_HOLDER),
            (Constants.BASE)                       : (standardProduct?.value?.attribute4) ?: (Constants.PLACE_HOLDER),
            (Constants.SEGMENT)                    : (standardProduct?.value?.attribute9) ?: (Constants.PLACE_HOLDER),
            (Constants.METRIC)                     : (standardProduct?.value?.attribute5) ?: (Constants.PLACE_HOLDER),
            (Constants.TERM)                       : (standardProduct?.value?.attribute6) ?: (Constants.PLACE_HOLDER),
            (Constants.TERM_UOM)                   : (standardProduct?.value?.attribute21) ?: (Constants.PLACE_HOLDER),
            (Constants.PAYMENT_TYPE)               : (standardProduct?.value?.attribute7) ?: (Constants.PLACE_HOLDER),
            (Constants.PRODUCT_TYPE)               : (standardProduct?.value?.attribute19) ?: (Constants.PLACE_HOLDER),
            (Constants.SUPPORT_TIER)               : (standardProduct?.value?.attribute18) ?: (Constants.PLACE_HOLDER),
            (Constants.DATACENTER)                 : (standardProduct?.value?.attribute3) ?: (Constants.PLACE_HOLDER),
            (Constants.OS)                         : (standardProduct?.value?.attribute22) ?: (Constants.PLACE_HOLDER),
            (Constants.HOSTING)                    : (standardProduct?.value?.attribute23) ?: (Constants.PLACE_HOLDER),
            (Constants.PURCHASING_PROGRAM)         : (standardProduct?.value?.attribute20) ?: (Constants.PLACE_HOLDER),
            (Constants.VOLUME_TIER)                : (standardProduct?.value?.attribute8) ?: (Constants.PLACE_HOLDER),
            (Constants.PROGRAM_OPTION)             : (standardProduct?.value?.attribute25) ?: (Constants.PLACE_HOLDER),
            (Constants.PS_TERM)                    : (standardProduct?.value?.attribute12) ?: (Constants.PLACE_HOLDER),
            (Constants.PS_OPTION)                  : (standardProduct?.value?.attribute13) ?: (Constants.PLACE_HOLDER),
            (Constants.VERSION)                    : (standardProduct?.value?.attribute10) ?: (Constants.PLACE_HOLDER),
            (Constants.PROMOTION)                  : (standardProduct?.value?.attribute11) ?: (Constants.PLACE_HOLDER),
            (Constants.RETENTION_PERIOD)           : (standardProduct?.value?.attribute31) ?: (Constants.PLACE_HOLDER),
            (Constants.RETENTION_PERIOD_UOM)       : (standardProduct?.value?.attribute32) ?: (Constants.PLACE_HOLDER),
            (Constants.EXCEPTION)                  : (standardProduct?.value?.attribute34) ?: (Constants.PLACE_HOLDER),
            (Constants.EXCEPTION_LEVEL)            : (standardProduct?.value?.attribute35) ?: (Constants.PLACE_HOLDER),
            (Constants.FACTORS)                    : (standardProduct?.value?.attribute36) ?: (Constants.PLACE_HOLDER)
    ]
    api.local.data << data
}
return

