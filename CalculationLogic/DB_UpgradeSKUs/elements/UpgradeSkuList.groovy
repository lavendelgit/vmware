api.local.upgradeSkus = []
def baseSKU = out.BaseSKU
def upgradeSKU = out.UpgradeSKU
def destinationSKU = out.DestinationSKU
api.local.upgradeSkus << api.local.baseToUpgrade?.findAll { (it.key1) in baseSKU }?.key2?.unique()
api.local.upgradeSkus << api.local.baseToUpgrade?.findAll { (it.key2) in upgradeSKU }?.key2?.unique()
api.local.upgradeSkus << api.local.upgradeToDestination?.findAll { (it.key2) in destinationSKU }?.key1?.unique()
api.local.upgradeSkus?.unique()?.flatten()
return