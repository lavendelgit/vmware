if (api.local.groupedPriceBookData) {
    api.local.skuPrices = [:]
    List data
    for (skuData in api.local.groupedPriceBookData) {
        value = skuData?.value
        if (!api.local.skuPrices[value?.PartNumber?.getAt(0)]) {
            api.local.skuPrices[value?.PartNumber?.getAt(0)] = [
                    (out.ConstantConfiguration.COLUMN_PLATFORM_GROUP)  : value?.PlatformGroup?.getAt(0),
                    (out.ConstantConfiguration.COLUMN_PRODUCT_PLATFORM): value?.ProductPlatform?.getAt(0),
                    (out.ConstantConfiguration.COLUMN_PRODUCT_FAMILY)  : value?.ProductFamily?.getAt(0),
                    (out.ConstantConfiguration.COLUMN_PRODUCT)         : value?.Product?.getAt(0),
            ]
            data = []
            for (priceData in value) {
                data << [
                        (out.ConstantConfiguration.COLUMN_START_DATE): priceData?.StartDate,
                        (out.ConstantConfiguration.COLUMN_END_DATE)  : priceData?.EndDate,
                        (Constants.PRICE)                            : priceData[out.Currency]
                ]
                api.local.skuPrices[value?.PartNumber?.getAt(0)]?.PriceData = data
            }
        }
    }
}
return