startYear = api.dateUserEntry(out.ConstantConfiguration.INPUT_START_YEAR)
parameter = api.getParameter(out.ConstantConfiguration.INPUT_START_YEAR)
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
}
return startYear