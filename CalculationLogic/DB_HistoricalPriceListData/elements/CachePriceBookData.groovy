if (out.DashBoardConfiguration && out.StartYear && out.EndYear && out.Currency && (out.StartYear < out.EndYear)) {
    api.local.queriedPriceBook = []
    List filter = []
    out.ConstantConfiguration.FILTER_COLUMN_OPTION?.each { columnName ->
        if (api.userEntry(columnName))
            filter << Filter.in(columnName, api.userEntry(columnName))
    }
    filter << Filter.greaterOrEqual(out.ConstantConfiguration.COLUMN_END_DATE, out.StartYear)
    filter << Filter.lessOrEqual(out.ConstantConfiguration.COLUMN_START_DATE, out.EndYear)
    def ctx = api.getDatamartContext()
    def dm = ctx.getDataSource(Constants.DATA_SOURCE)
    def query = ctx?.newQuery(dm)
            ?.select(out.ConstantConfiguration.COLUMN_PART_NUMBER, out.ConstantConfiguration.COLUMN_PART_NUMBER)
            ?.select(out.ConstantConfiguration.COLUMN_PLATFORM_GROUP, out.ConstantConfiguration.COLUMN_PLATFORM_GROUP)
            ?.select(out.ConstantConfiguration.COLUMN_PRODUCT_PLATFORM, out.ConstantConfiguration.COLUMN_PRODUCT_PLATFORM)
            ?.select(out.ConstantConfiguration.COLUMN_PRODUCT_FAMILY, out.ConstantConfiguration.COLUMN_PRODUCT_FAMILY)
            ?.select(out.ConstantConfiguration.PRODUCT, out.ConstantConfiguration.PRODUCT)
            ?.select(out.Currency, out.Currency)
            ?.select(out.ConstantConfiguration.COLUMN_START_DATE, out.ConstantConfiguration.COLUMN_START_DATE)
            ?.select(out.ConstantConfiguration.COLUMN_END_DATE, out.ConstantConfiguration.COLUMN_END_DATE)
            ?.where(*filter)
            ?.orderBy(out.ConstantConfiguration.COLUMN_PART_NUMBER)
    streamResult = ctx?.streamQuery(query)
    while (streamResult?.next()) {
        api.local.queriedPriceBook << streamResult.get()
    }
    streamResult?.close()
}
return
