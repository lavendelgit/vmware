import java.text.DateFormat
import java.text.SimpleDateFormat


api.local.plData = []
BigDecimal prevRecordPrice = 0.0
if (api.local.skuPrices) {
    Map record = [:]
    DateFormat df2 = new SimpleDateFormat("MMM-yyyy", Locale.US)
    Date userEndDate = new SimpleDateFormat("yyyy-MM-dd").parse(out.EndYear)
    Date userStartDate = new SimpleDateFormat("yyyy-MM-dd").parse(out.StartYear)
    userEndYear = userEndDate.format("yyyy") as Integer
    List months = []
    for (sku in api.local.skuPrices) {
        prevRecordPrice = 0.0
        record = [(out.ConstantConfiguration.PART_NUMBER)     : sku?.key,
                  (out.ConstantConfiguration.PLATFORM_GROUP)  : sku?.value?.PlatformGroup,
                  (out.ConstantConfiguration.PRODUCT_PLATFORM): sku?.value?.ProductPlatform,
                  (out.ConstantConfiguration.PRODUCT_FAMILY)  : sku?.value?.ProductFamily,
                  (out.ConstantConfiguration.PRODUCT)         : sku?.value?.Product,
                  (out.ConstantConfiguration.CURRENCY)        : out.Currency]
        costs = sku.value?.PriceData?.sort { it.StartDate }
        for (cost in costs) {
            startDate = cost?.StartDate < userStartDate ? userStartDate : cost.StartDate
            endDate = cost?.EndDate > userEndDate ? userEndDate : cost.EndDate
            months = libs.vmwareUtil.PopulateMonths.getListMonths(startDate, endDate, Locale.US, df2, userEndYear)
            if (out.DashBoardConfiguration == out.ConstantConfiguration.MONTHLY_PRICE_PROJECTION) {
                for (month in months) {
                    record[month] = cost?.Price
                }
            } else if (out.DashBoardConfiguration == out.ConstantConfiguration.PRICE_EFFECTIVE_MONTH_PROJECTION) {
                for (month in months) {
                    roundCost = cost?.Price?.setScale(2, BigDecimal.ROUND_HALF_UP)
                    if (prevRecordPrice != roundCost && roundCost) {
                        prevRecordPrice = roundCost
                        api.local.plData << Lib.generateRecords(sku, month, roundCost, out.Currency)
                    }
                }
            } else if (out.DashBoardConfiguration == out.ConstantConfiguration.ALL_PRICE_EFFECTIVE_PROJECTION) {
                for (month in months) {
                    api.local.plData << Lib.generateRecords(sku, month, cost.Price, out.Currency)
                }
            }
        }
        if (out.DashBoardConfiguration == out.ConstantConfiguration.MONTHLY_PRICE_PROJECTION) {
            api.local.plData << record
        }
    }
}
return