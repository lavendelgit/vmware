Map generateRecords(Object skuDetails, String month, BigDecimal price, String currency) {
    return [
            (out.ConstantConfiguration.PART_NUMBER)     : skuDetails?.key,
            (out.ConstantConfiguration.PLATFORM_GROUP)  : skuDetails?.value?.PlatformGroup,
            (out.ConstantConfiguration.PRODUCT_PLATFORM): skuDetails?.value?.ProductPlatform,
            (out.ConstantConfiguration.PRODUCT_FAMILY)  : skuDetails?.value?.ProductFamily,
            (out.ConstantConfiguration.PRODUCT)         : skuDetails?.value?.Product,
            (out.ConstantConfiguration.CURRENCY)        : currency,
            (out.ConstantConfiguration.PRICE_MONTH)     : month,
            (out.ConstantConfiguration.LIST_PRICE)      : price,
    ]

}