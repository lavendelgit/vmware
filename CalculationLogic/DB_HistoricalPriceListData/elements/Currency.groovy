currency = api.option(out.ConstantConfiguration.CURRENCY, Constants.CURRENCY_OPTIONS)
parameter = api.getParameter(out.ConstantConfiguration.CURRENCY)
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
}
return currency