if (out.DashBoardConfiguration && out.StartYear && out.EndYear && out.Currency && (out.StartYear < out.EndYear)) {
    api.local.plData?.each { data ->
        api.local.resultMatrix?.addRow(data)
    }
    return api.local.resultMatrix
} else {
    api.local.resultMatrix = api.newMatrix("")
    warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.WARNING, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    api.local.resultMatrix?.addRow(warningCell)
    if (out.StartYear > out.EndYear) {
        warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.YEAR_WARNING_MESSAGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
        api.local.resultMatrix?.addRow(warningCell)
    } else {
        warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.ALL_INPUTS_WARNING_MESSAGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
        api.local.resultMatrix?.addRow(warningCell)
    }
    return api.local.resultMatrix
}
