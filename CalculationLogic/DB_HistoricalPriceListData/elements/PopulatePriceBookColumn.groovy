import java.text.DateFormat
import java.text.SimpleDateFormat

if (api.local.skuPrices) {
    api.local.resultMatrix = api.newMatrix(out.ConstantConfiguration.PLATFORM_GROUP, out.ConstantConfiguration.PRODUCT_PLATFORM, out.ConstantConfiguration.PRODUCT_FAMILY, out.ConstantConfiguration.PRODUCT, out.ConstantConfiguration.PART_NUMBER, out.ConstantConfiguration.CURRENCY)
    api.local.resultMatrix?.setEnableClientFilter(true)
    if (out.DashBoardConfiguration == out.ConstantConfiguration.MONTHLY_PRICE_PROJECTION) {
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(out.EndYear)
        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(out.StartYear)
        DateFormat df2 = new SimpleDateFormat("MMM-yyyy", Locale.US)
        endYear = endDate?.format("yyyy") as Integer
        api.local.resultMatrix?.addColumns(libs.vmwareUtil.PopulateMonths.getListMonths(startDate, endDate, Locale.US, df2, endYear))
    } else if (out.DashBoardConfiguration == out.ConstantConfiguration.PRICE_EFFECTIVE_MONTH_PROJECTION || out.DashBoardConfiguration == out.ConstantConfiguration.ALL_PRICE_EFFECTIVE_PROJECTION) {
        api.local.resultMatrix?.addColumns([out.ConstantConfiguration.PRICE_MONTH, out.ConstantConfiguration.LIST_PRICE])
    }
}
return