import groovy.transform.Field

@Field final List CURRENCY_OPTIONS = ["USD", "EUR", "GBP", "AUD", "CNY", "JPY", "EMEAUSD", "EMEAUSD2", "GUSD"]
@Field final String DATA_SOURCE = "HistoricalPriceData"
@Field final String PRICE = "Price"
