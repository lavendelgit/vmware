dbConfiguration = api.option(out.ConstantConfiguration.DB_CONFIGURATION, out.ConstantConfiguration.CONFIGURATION_OPTIONS)
parameter = api.getParameter(out.ConstantConfiguration.DB_CONFIGURATION)
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
}
return dbConfiguration