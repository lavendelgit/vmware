if (!api.global.baseDCFinalPrice) {
    api.global.baseDCFinalPrice = [:]
    BigDecimal hwSupport = api.global.factorConstants.getAt("Hardware Support") as BigDecimal
    Map lhs1YrWaterfall = api.global.lhsWaterfallPrices?.getAt(out.Service + "_1 Year")
    Map lhs3YrWaterfall = api.global.lhsWaterfallPrices?.getAt(out.Service + "_3 Year")
    Map lhsODWaterfall = api.global.lhsWaterfallPrices?.getAt(out.Service + "_On Demand")
    BigDecimal swPrice = lhs1YrWaterfall?.PfxSwPrice
    BigDecimal swUpliftAdjustment = (lhs1YrWaterfall.SwAdjustmentOverride ?: lhs1YrWaterfall?.SwAdjustment)
    api.global.baseDCFinalPrice["1 Year"] = Library.calculateBasePrice(api.global.factorConstants?.getAt("Base DC"), out.Service, out.PerHour, swPrice, swUpliftAdjustment, "1 Year", api.global.awsHwPrices, hwSupport)
    swPrice = lhs3YrWaterfall?.PfxSwPrice
    swUpliftAdjustment = (lhs3YrWaterfall.SwAdjustmentOverride ?: lhs3YrWaterfall?.SwAdjustment)
    api.global.baseDCFinalPrice["3 Year"] = Library.calculateBasePrice(api.global.factorConstants?.getAt("Base DC"), out.Service, out.PerHour, swPrice, swUpliftAdjustment, "3 Year", api.global.awsHwPrices, hwSupport)
    swPrice = lhsODWaterfall?.PfxSwPrice
    swUpliftAdjustment = (lhsODWaterfall.SwAdjustmentOverride ?: lhsODWaterfall?.SwAdjustment)
    api.global.baseDCFinalPrice["On Demand"] = Library.calculateBasePrice(api.global.factorConstants?.getAt("Base DC"), out.Service, out.PerHour, swPrice, swUpliftAdjustment, "On Demand", api.global.awsHwPrices, hwSupport)
}
return api.global.baseDCFinalPrice[out.Term]?.setScale(2, BigDecimal.ROUND_HALF_UP)