BigDecimal hwSupportPrice = out.AWSHwPrice ? out.AWSHwPrice * (1 + (api.global.factorConstants.getAt("Hardware Support") as BigDecimal)) : 0
if (out.Term == "1 Year") {
    hwSupportPrice = hwSupportPrice * out.PerHour * 12

} else if (out.Term == "3 Year") {
    hwSupportPrice = hwSupportPrice * out.PerHour * 36
}
return hwSupportPrice//?.setScale(6,BigDecimal.ROUND_UP)