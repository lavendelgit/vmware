api.trace("api.global.termsCache", api.global.termsCache)
api.trace("out.Offer", out.Offer)
api.trace("out.PaymentType", out.PaymentType)
api.trace("out.TermYears", out.TermYears)
return libs.vmwareUtil.CacheManager.getTermFactor(api.global.termsCache, out.Offer, out.PaymentType, out.TermYears) as BigDecimal