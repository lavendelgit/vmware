Map lhsWaterfall = api.global.lhsWaterfallPrices?.getAt(out.Service + "_On Demand")
BigDecimal swPrice = lhsWaterfall?.PfxSwPrice
BigDecimal hwSupport = api.global.factorConstants.getAt("Hardware Support") as BigDecimal
BigDecimal swUpliftAdjustment = (lhsWaterfall.SwAdjustmentOverride ?: lhsWaterfall?.SwAdjustment)
return Library.calculateBasePrice(out.DataCenter, out.Service, out.PerHour, swPrice, swUpliftAdjustment, "On Demand", api.global.awsHwPrices, hwSupport)?.setScale(8, BigDecimal.ROUND_HALF_UP)
