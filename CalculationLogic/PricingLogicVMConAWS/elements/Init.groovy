if (!api.global.batch) {
    api.global.batch = [:]
}

if (!api.global.termAttributes) {
    api.global.termAttributes = [:]
}
if (!api.global.serviceAttributes) {
    api.global.serviceAttributes = [:]
}
if (!api.global.factorConstants) {
    api.global.factorConstants = [:]
}
if (!api.global.lhsWaterfallPrices) {
    api.global.lhsWaterfallPrices = [:]
}
if (!api.global.awsHwPrices) {
    api.global.awsHwPrices = [:]
}
if (!api.global.termsCache) {
    api.global.termsCache = libs.vmwareUtil.CacheManager.cacheTermsFactor(["All", Constants.STR_PRODUCT], ["All", Constants.STR_OFFER], ["All", Constants.STR_BASE])
}

api.local.pid = api.product("sku")
if (!api.global.batch[api.local.pid]) {

    api.global.batch.clear()
    def batch = api.getBatchInfo()?.collect { it[0] } ?: [api.local.pid]

    filter = [
            Filter.equal("name", "VMConAWS"),
            Filter.in("sku", batch)
    ]
    def VMCProductsList = api.find("PX20", *filter)

    def VMCPXCache = [:]
    VMCPXCache = VMCProductsList.collectEntries { [(it.sku): it] }
    batch.each {
        api.global.batch[it] = [
                "VMCPX": (VMCPXCache[it] ?: null)
        ]
    }
}

if (!api.global.fxRates) {
    api.global.fxRates = api.findLookupTableValues("VMCFxRates")?.sort { rate -> rate.key3 }?.collectEntries {
        fxRate ->
            [
                    (fxRate.key1 + "_" + fxRate.key2 + "_" + fxRate.key5): [
                            "SAPFactor": fxRate.attribute1,
                            "Adj"      : fxRate.attribute2
                    ]
            ]
    }
}

if (!api.global.termAttributes) {
    api.global.termAttributes = api.findLookupTableValues("VMCTerm")?.collectEntries {
        termAttribute ->
            [(termAttribute.key1 + "_" + termAttribute.key2): [
                    "OnDemandMultiple"     : termAttribute.attribute1,
                    "ManagedServicePremium": termAttribute.attribute2,
                    "ChannelPartnerUplift" : termAttribute.attribute3
            ]
            ]
    }
}

if (!api.global.serviceAttributes) {
    api.global.serviceAttributes = api.findLookupTableValues("VMCServiceAttributes")?.collectEntries {
        serviceAttribute ->
            [
                    (serviceAttribute.name): [
                            "Hosts"   : serviceAttribute.attribute1 as Integer,
                            // "ChannelPartnerUplift" : serviceAttribute.attribute2 as BigDecimal,
                            "TCOYears": serviceAttribute.attribute3 as Integer,
                            "TCOUoM"  : serviceAttribute.attribute4,
                            "Sns"     : serviceAttribute.attribute2 as BigDecimal
                    ]
            ]
    }
}

if (!api.global.factorConstants) {
    api.global.factorConstants = api.findLookupTableValues("VMCFactorConstants")?.collectEntries {
        [(it.name): it.attribute1]
    }
}

if (!api.global.lhsWaterfallPrices) {
    api.global.lhsWaterfallPrices = api.findLookupTableValues("VMCLHSWaterfallPrices")?.collectEntries {
        lhsWaterfallPrice ->
            [(lhsWaterfallPrice.key1 + "_" + lhsWaterfallPrice.key2): [
                    "Term"                    : lhsWaterfallPrice.key2,
                    "BUPrice"                 : lhsWaterfallPrice.attribute1,
                    "ActualBundlePrice"       : lhsWaterfallPrice.attribute2,
                    "ActualBundleAdjustment"  : lhsWaterfallPrice.attribute3,
                    "BundlePriceOverride"     : lhsWaterfallPrice.attribute4,
                    "BundleAdjustmentOverride": lhsWaterfallPrice.attribute5,
                    "PfxSwPrice"              : lhsWaterfallPrice.attribute6,
                    "PfxHwPrice"              : lhsWaterfallPrice.attribute7,
                    "PfxBasePrice"            : lhsWaterfallPrice.attribute8,
                    "SwAdjustment"            : lhsWaterfallPrice.attribute9,
                    "SwAdjustmentOverride"    : lhsWaterfallPrice.attribute10
            ]
            ]
    }
}

if (!api.global.awsHwPrices) {
    api.global.awsHwPrices = api.findLookupTableValues("VMCAwsHwPrices")?.collectEntries {
        awsHwPrice ->
            [(awsHwPrice.key1 + ":" + awsHwPrice.key2): [
                    "OneHROD"          : awsHwPrice.attribute3 as BigDecimal,
                    "OneYearHrlyRate"  : awsHwPrice.attribute4 as BigDecimal,
                    "ThreeYearHrlyRate": awsHwPrice.attribute5 as BigDecimal,
                    "OneHRODUplift"    : awsHwPrice.attribute6 as BigDecimal,
                    "OneYearUplift"    : awsHwPrice.attribute7 as BigDecimal,
                    "ThreeYearUplift"  : awsHwPrice.attribute8 as BigDecimal
            ]
            ]
    }
}