def termYears = out.TermYears as BigDecimal
BigDecimal factor = out.DCFactorUplift
if (termYears == 3) {
    factor = out.TermDiscount
}
if (out.PaymentType == "Monthly") {
    return (((out.BasePrice * factor) / 12) * out.BillingFreqTermsFactor) as BigDecimal
} else if (out.PaymentType == "Prepaid") {
    BigDecimal basePrice = out.BasePrice * factor * out.BillingFreqTermsFactor
    basePrice = termYears == 3 ? (basePrice * termYears) : basePrice
    return basePrice
} else if (out.PaymentType == "Hourly") {
    return (out.BasePrice * out.DCFactorUplift * out.BillingFreqTermsFactor)
}