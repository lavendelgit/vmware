BigDecimal serviceUplift = api.global.awsHwPrices?.getAt(out.DataCenter + ":" + out.Service)?.OneHRODUplift
if (out.Term == "1 Year") {
    serviceUplift = api.global.awsHwPrices?.getAt(out.DataCenter + ":" + out.Service)?.OneYearUplift
} else if (out.Term == "3 Year") {
    serviceUplift = api.global.awsHwPrices?.getAt(out.DataCenter + ":" + out.Service)?.ThreeYearUplift
}
return serviceUplift ? serviceUplift / 100 : 0 as BigDecimal

