BigDecimal termYears = out.TermYears as BigDecimal
if (termYears == 3) {
    return (1 - ((out.BasePrice * 3) - out.FinalBasePriceBeforeSAPUplift) / (out.BasePrice * 3))?.setScale(6, BigDecimal.ROUND_HALF_UP)
}
return null
