if (out.DataCenterCode == "L1") {
    return
}
def decimalPlace = (out.PaymentType == "Hourly") ? 6 : 0 as Integer
BigDecimal fxRate = (api.global.fxRates?.getAt(out.FromCurrency + "_" + "JPY" + "_" + out.Service)?.SAPFactor * (api.global.fxRates?.getAt(out.FromCurrency + "_" + "JPY" + "_" + out.Service)?.Adj))//?.setScale(4,BigDecimal.ROUND_UP)

return (out.MonthlyBasePrice * fxRate)?.setScale(decimalPlace, BigDecimal.ROUND_HALF_DOWN) as BigDecimal