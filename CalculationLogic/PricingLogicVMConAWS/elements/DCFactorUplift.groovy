if (out.DataCenterCode == "L1") {
    return 1
}
if (out.TermYears == "3" || out.TermYears == "1") {
    return (1 + (out.DCOneYrPrice - out.BaseDCOneYrPrice) / out.BaseDCOneYrPrice)?.setScale(6, BigDecimal.ROUND_HALF_UP) as BigDecimal
} else if (out.TermYears == "0" && out.Term == "On Demand") {
    return (1 + (out.FinalBasePriceBeforeSAPUplift - out.BaseDCOnDemandPrice) / out.BaseDCOnDemandPrice)?.setScale(6, BigDecimal.ROUND_HALF_UP) as BigDecimal
}
