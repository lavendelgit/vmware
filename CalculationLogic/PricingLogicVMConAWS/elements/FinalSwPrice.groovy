BigDecimal swPrice = out.SoftwarePrice * (1 + out.SwUpliftAdjustment) * (1 + out.ServiceUplift)//?.setScale(6,BigDecimal.ROUND_HALF_UP)
if (out.Term == "1 Year") {
    swPrice = swPrice * 730 * 12

} else if (out.Term == "3 Year") {
    swPrice = swPrice * out.PerHour * 36
}
return swPrice//?.setScale(6,BigDecimal.ROUND_HALF_UP)