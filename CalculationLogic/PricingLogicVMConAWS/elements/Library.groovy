def calculateBasePrice(String dataCenter, String service, BigDecimal perHour, BigDecimal swBaseDCPrice, BigDecimal swUpliftAdj, String term, Map serviceUpliftCache, BigDecimal hwSupport) {

    BigDecimal serviceUplift = serviceUpliftCache?.getAt(dataCenter + ":" + service)?.OneHRODUplift
    if (term == "1 Year") {
        serviceUplift = serviceUpliftCache?.getAt(dataCenter + ":" + service)?.OneYearUplift
    } else if (term == "3 Year") {
        serviceUplift = serviceUpliftCache?.getAt(dataCenter + ":" + service)?.ThreeYearUplift
    }
    serviceUplift = serviceUplift ? serviceUplift / 100 : 0 as BigDecimal
    BigDecimal awsHwPrice = serviceUpliftCache?.getAt(dataCenter + ":" + service)?.OneHROD
    if (term == "1 Year") {
        awsHwPrice = serviceUpliftCache?.getAt(dataCenter + ":" + service)?.OneYearHrlyRate
    } else if (term == "3 Year") {
        awsHwPrice = serviceUpliftCache?.getAt(dataCenter + ":" + service)?.ThreeYearHrlyRate
    }
    BigDecimal finalBaseDCHwPrice = awsHwPrice ? (awsHwPrice * (1 + hwSupport)) : 0 as BigDecimal
    BigDecimal finalBaseDCSwPrice = (swBaseDCPrice * (1 + swUpliftAdj) * (1 + serviceUplift))?.setScale(6, BigDecimal.ROUND_HALF_UP)

    if (term == "1 Year") {
        finalBaseDCHwPrice = (finalBaseDCHwPrice * perHour * 12) as BigDecimal
        finalBaseDCSwPrice = (finalBaseDCSwPrice * perHour * 12) as BigDecimal
    } else if (term == "3 Year") {
        finalBaseDCHwPrice = (finalBaseDCHwPrice * perHour * 36) as BigDecimal
        finalBaseDCSwPrice = (finalBaseDCSwPrice * perHour * 36) as BigDecimal
    }
    if (term != "On Demand") {
        return (finalBaseDCHwPrice + finalBaseDCSwPrice)?.setScale(2, BigDecimal.ROUND_HALF_UP)
    } else {
        return finalBaseDCHwPrice + finalBaseDCSwPrice
    }

}