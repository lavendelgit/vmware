BigDecimal awsHwPrice = api.global.awsHwPrices?.getAt(out.DataCenter + ":" + out.Service)?.OneHROD
if (out.Term == "1 Year") {
    awsHwPrice = api.global.awsHwPrices?.getAt(out.DataCenter + ":" + out.Service)?.OneYearHrlyRate
} else if (out.Term == "3 Year") {
    awsHwPrice = api.global.awsHwPrices?.getAt(out.DataCenter + ":" + out.Service)?.ThreeYearHrlyRate
}
return awsHwPrice?.setScale(6, BigDecimal.ROUND_HALF_UP)

