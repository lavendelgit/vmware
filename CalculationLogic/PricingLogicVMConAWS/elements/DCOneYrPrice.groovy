api.trace("api.global.awsHwPrices", api.global.awsHwPrices)
Map lhsWaterfall = api.global.lhsWaterfallPrices?.getAt(out.Service + "_1 Year")
BigDecimal swPrice = lhsWaterfall?.PfxSwPrice
BigDecimal hwSupport = api.global.factorConstants.getAt("Hardware Support") as BigDecimal
BigDecimal swUpliftAdjustment = (lhsWaterfall.SwAdjustmentOverride ?: lhsWaterfall?.SwAdjustment)
BigDecimal dcPrice = Library.calculateBasePrice(out.DataCenter, out.Service, out.PerHour, swPrice, swUpliftAdjustment, "1 Year", api.global.awsHwPrices, hwSupport)?.setScale(2, BigDecimal.ROUND_HALF_UP)

BigDecimal sapUplift = (1 + (dcPrice - out.BaseDCOneYrPrice) / out.BaseDCOneYrPrice)?.setScale(6, BigDecimal.ROUND_HALF_UP) as BigDecimal
api.trace("sapUplift", sapUplift)
return out.BaseDCOneYrPrice * sapUplift
