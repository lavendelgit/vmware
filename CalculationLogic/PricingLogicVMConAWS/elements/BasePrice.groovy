BigDecimal termYears = out.TermYears as BigDecimal
BigDecimal basePrice = 0
if (out.DataCenterCode == "L1") {
    if (termYears == 1 || termYears == 3) {
        return out.DCOneYrPrice
    } else if (termYears == 0) {
        return out.DCOnDemandPrice
    }
    return
}
if (termYears == 1) {
    basePrice = out.BaseDCFinalPrice
} else if (termYears == 3) {
    BigDecimal uplift = (1 + ((out.DCOneYrPrice - out.BaseDCOneYrPrice) / out.BaseDCOneYrPrice))?.setScale(6, BigDecimal.ROUND_HALF_UP)
    basePrice = out.BaseDCOneYrPrice * uplift
} else {
    basePrice = out.BaseDCOnDemandPrice
}
return basePrice//?.setScale(out.RoundingDecimal,BigDecimal.ROUND_DOWN)
