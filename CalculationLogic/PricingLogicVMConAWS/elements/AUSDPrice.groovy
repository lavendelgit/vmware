if (out.DataCenterCode == "L1") {
    return
}
BigDecimal fxRate = api.global.fxRates?.getAt(out.FromCurrency + "_" + "A-USD" + "_" + out.Service)?.SAPFactor * (api.global.fxRates?.getAt(out.FromCurrency + "_" + "A-USD" + "_" + out.Service)?.Adj)?.setScale(6, BigDecimal.ROUND_UP)
return (out.MonthlyBasePrice * fxRate)?.setScale(out.RoundingDecimal, BigDecimal.ROUND_HALF_DOWN) as BigDecimal