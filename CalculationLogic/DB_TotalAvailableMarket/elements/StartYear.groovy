startYear = api.dateUserEntry(Constants.INPUT_START_YEAR)
parameter = api.getParameter(Constants.INPUT_START_YEAR)
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
}
return startYear