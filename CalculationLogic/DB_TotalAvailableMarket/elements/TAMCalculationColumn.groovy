if (api.local.queriedPriceingDiscount) {
    api.local.resultMatrix = api.newMatrix( Constants.PRODUCT_CLASS, Constants.PND_SUBCLASS_GROUP, Constants.PLATFORM_GROUP, Constants.PRODUCT_GROUP, Constants.PRODUCT_DIVISION, Constants.CUSTOMER_PURCHASE_SKU,Constants.PRODUCT_SKU,Constants.PHANTOM_SKU,Constants.ORDER_DATE,Constants.CURRENCY_CODE,Constants.GROSS_BOOKING_LC)
    api.local.resultMatrix?.setEnableClientFilter(true)
}
return