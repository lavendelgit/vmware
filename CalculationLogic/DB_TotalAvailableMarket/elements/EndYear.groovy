endYear = api.dateUserEntry(Constants.INPUT_END_YEAR)
parameter = api.getParameter(Constants.INPUT_END_YEAR)
if (parameter != null && parameter.getValue() == null) {
    parameter.setRequired(true)
}
return endYear