BigDecimal totalGrossBookingsCC = 0.00
BigDecimal percentageIncrease
BigDecimal estimatedTAM
if (out.StartYear && out.EndYear && (out.StartYear < out.EndYear)) {
    totalGrossBookingsCC = 0.00

    api.local.queriedPriceingDiscount?.each { data ->
        data[Constants.GROSS_BOOKING_LC] = data?.GrossBookingsCC?.setScale(2, BigDecimal.ROUND_HALF_UP)
        api.local.resultMatrix?.addRow(data)
        totalGrossBookingsCC += data?.GrossBookingsCC ? data?.GrossBookingsCC?.setScale(2, BigDecimal.ROUND_HALF_UP) : 0.00
    }
    api.local.totalGrossBookingsCC = totalGrossBookingsCC
    return api.local.resultMatrix
} else {
    api.local.resultMatrix = api.newMatrix("")
    warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.WARNING, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    api.local.resultMatrix?.addRow(warningCell)
    if (out.StartYear > out.EndYear) {
        warningCell = api.local.resultMatrix?.styledCell(out.ConstantConfiguration.YEAR_WARNING_MESSAGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
        api.local.resultMatrix?.addRow(warningCell)
    } else {
        warningCell = api.local.resultMatrix?.styledCell(Constants.ALL_INPUTS_WARNING_MESSAGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
        api.local.resultMatrix?.addRow(warningCell)
    }
    return api.local.resultMatrix
}