if (out.StartYear && out.EndYear && (out.StartYear < out.EndYear)) {
    api.local.queriedPriceingDiscount = []
    List filter = []
    Constants.FILTER_COLUMN_OPTION?.each { columnName ->
        if (api.userEntry(columnName))
            filter << Filter.in(columnName, api.userEntry(columnName))
    }

    filter << Filter.greaterOrEqual(Constants.COLUMN_ORDER_DATE, out.StartYear)
    filter << Filter.lessOrEqual(Constants.COLUMN_ORDER_DATE, out.EndYear)

    def ctx = api.getDatamartContext()
    def dm = ctx.getDataSource(Constants.DATA_SOURCE)
    def query = ctx?.newQuery(dm)
            ?.select(Constants.COLUMN_PRODUCT_CLASS, Constants.COLUMN_PRODUCT_CLASS)
            ?.select(Constants.COLUMN_PND_SUBCLASS_GROUP, Constants.COLUMN_PND_SUBCLASS_GROUP)
            ?.select(Constants.COLUMN_PLATFORM_GROUP, Constants.COLUMN_PLATFORM_GROUP)
            ?.select(Constants.COLUMN_PRODUCT_GROUP, Constants.COLUMN_PRODUCT_GROUP)
            ?.select(Constants.COLUMN_PRODUCT_DIVISION, Constants.COLUMN_PRODUCT_DIVISION)
            ?.select(Constants.COLUMN_CUSTOMER_PURCHASE_SKU, Constants.COLUMN_CUSTOMER_PURCHASE_SKU)
            ?.select(Constants.COLUMN_PRODUCT_SKU, Constants.COLUMN_PRODUCT_SKU)
            ?.select(Constants.COLUMN_PHANTOM_SKU, Constants.COLUMN_PHANTOM_SKU)
            ?.select(Constants.COLUMN_ORDER_DATE, Constants.COLUMN_ORDER_DATE)
            ?.select(Constants.COLUMN_GROSS_BOOKING_CC, Constants.COLUMN_GROSS_BOOKING_CC)
            ?.select(Constants.COLUMN_CURRENCY_CODE, Constants.COLUMN_CURRENCY_CODE)
            ?.where(*filter)
            ?.orderBy(Constants.COLUMN_ORDER_DATE)
    streamResult = ctx?.streamQuery(query)
    while (streamResult?.next()) {
        api.local.queriedPriceingDiscount << streamResult.get()
    }
    streamResult?.close()
}
return
