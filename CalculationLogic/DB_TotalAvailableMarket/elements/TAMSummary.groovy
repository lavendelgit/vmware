import net.pricefx.server.dto.calculation.ResultMatrix

if (out.StartYear && out.EndYear && (out.StartYear < out.EndYear)) {
    ResultMatrix resultMatrix = api.newMatrix(Constants.COLUMN_FIELDS, Constants.COLUMN_VALUES)
    percentageChange = api.input(Constants.PERCENTAGE_CHANGE)
    estimatedTAM = (api.local.totalGrossBookingsCC + (api.local.totalGrossBookingsCC * (percentageChange / 100))).setScale(2, BigDecimal.ROUND_HALF_UP)
    currencyCode = (api.input(Constants.CURRENCY_CODE) ?: Constants.PLACE_HOLDER) as String
    platformGroup = (api.input(Constants.PLATFORM_GROUP) ?: Constants.PLACE_HOLDER) as String
    productGroup = (api.input(Constants.PRODUCT_GROUP) ?: Constants.PLACE_HOLDER) as String
    productSKU = (api.input(Constants.PRODUCT_SKU) ?: Constants.PLACE_HOLDER) as String
    customerPurchasedSKU = (api.input(Constants.CUSTOMER_PURCHASE_SKU) ?: Constants.PLACE_HOLDER) as String
    startDate = (api.userEntry(Constants.INPUT_START_YEAR))
    endDate = (api.userEntry(Constants.INPUT_END_YEAR))


    styleTotal = resultMatrix?.styledCell(Constants.COLUMN_TOTAL, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleGrossBookingCC = resultMatrix?.styledCell(api.local.totalGrossBookingsCC, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleEstimateTAM = resultMatrix?.styledCell(Constants.COLUMN_ESTIMATED_TAM, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleEstimatedTAMValue = resultMatrix?.styledCell(estimatedTAM, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    stylePercentageChange = resultMatrix?.styledCell(Constants.PERCENTAGE_CHANGE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    stylePercentageChangeValue = resultMatrix?.styledCell(percentageChange, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleCurrencyCode = resultMatrix?.styledCell(Constants.CURRENCY_CODE, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleCurrencyCodeValue = resultMatrix?.styledCell(currencyCode, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    stylePlatformGroup = resultMatrix?.styledCell(Constants.PLATFORM_GROUP, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    stylePlatformGroupValue = resultMatrix?.styledCell(platformGroup, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleProductGroup = resultMatrix?.styledCell(Constants.PRODUCT_GROUP, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleProductGroupValue = resultMatrix?.styledCell(productGroup, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleProductSKU = resultMatrix?.styledCell(Constants.PRODUCT_SKU, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleProductSKUValue = resultMatrix?.styledCell(productSKU, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleCustomerPurchasedSKU = resultMatrix?.styledCell(Constants.CUSTOMER_PURCHASE_SKU, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleCustomerPurchasedSKUValue = resultMatrix?.styledCell(customerPurchasedSKU, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleStartDate = resultMatrix?.styledCell(Constants.INPUT_START_YEAR, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleStartDateValue = resultMatrix?.styledCell(startDate, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleEndDate = resultMatrix?.styledCell(Constants.INPUT_END_YEAR, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)
    styleEndDateValue = resultMatrix?.styledCell(endDate, out.ConstantConfiguration.TEXT_COLOR, out.ConstantConfiguration.BG_COLOR, out.ConstantConfiguration.TEXT_WEIGHT, out.ConstantConfiguration.TEXT_ALLIGN)

    resultMatrix?.addRow(styleTotal, styleGrossBookingCC)
    resultMatrix?.addRow(styleEstimateTAM, styleEstimatedTAMValue)
    resultMatrix?.addRow(stylePercentageChange, stylePercentageChangeValue)
    resultMatrix?.addRow(styleCurrencyCode, styleCurrencyCodeValue)
    resultMatrix?.addRow(stylePlatformGroup, stylePlatformGroupValue)
    resultMatrix?.addRow(styleProductGroup, styleProductGroupValue)
    resultMatrix?.addRow(styleProductSKU, styleProductSKUValue)
    resultMatrix?.addRow(styleCustomerPurchasedSKU, styleCustomerPurchasedSKUValue)
    resultMatrix?.addRow(styleStartDate, styleStartDateValue)
    resultMatrix?.addRow(styleEndDate, styleEndDateValue)
    return resultMatrix
}