if (api.isSyntaxCheck()) return
def AWSUplift = api.getElement("AWSAdjustmentUplift") ?: 0.0
AWSUplift += 1
def hw = api.getElement("HW")
if (AWSUplift != null && hw != null) {
    /*if(api.global.hw!=null)
    api.global.HWIncludingUplift = api.global.hw * AWSUplift*/
    return AWSUplift * hw
}