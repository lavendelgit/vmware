if (api.syntaxCheck) {
    return null
}
def itemType = api.getElement("ItemType")
if (itemType == "1 Year" || itemType == "3 Year") {
    return 0
}
def tab = api.findLookupTable("Adjustments")
def dataCenter = api.getElement("DataCenter")
def tabList
if (dataCenter != null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("key1", "On Demand Uplift"),
            Filter.equal("key2", dataCenter))
}
/*if(tabList==null){
  tabList = api.find("MLTV2",Filter.equal("lookupTable.id", tab?.id),
                     Filter.equal("key1","On Demand Uplift"))
}*/
if (tabList != null) {
    def onDemand = tabList[0]?.attribute1
    return onDemand
}