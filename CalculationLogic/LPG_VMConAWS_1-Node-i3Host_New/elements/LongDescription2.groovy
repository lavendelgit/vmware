def extn = api.getElement("VMConAWSExtn")
if (extn != null && extn["attribute9"][0] != null) {
    api.logInfo("long description", extn["attribute9"][0])
    def longDescriptionLength = extn["attribute9"][0].length()
    if (longDescriptionLength > 140) longDescriptionLength = 140
    if (extn["attribute9"][0].length() > 70)
        return extn["attribute9"][0].substring(70, longDescriptionLength)
    else return ""
}