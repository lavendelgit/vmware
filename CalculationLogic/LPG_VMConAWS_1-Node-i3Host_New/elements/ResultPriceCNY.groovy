def utilLib = libs.vmwareUtil.util
def itemType = api.getElement("ItemType")?.toString()
def resultPrice
def resultPriceUSD = api.getElement("ResultPrice")
def term = api.getElement("HostType")
def exchangeRate = utilLib.getExchangeRate("CNY", term)
if (exchangeRate != null && resultPriceUSD != null) {
    resultPrice = resultPriceUSD * exchangeRate
    if (itemType != null) {
        if (itemType == "On Demand") {
            resultPrice = Library.round(resultPrice, 6)
        } else {
            resultPrice = Library.round(resultPrice, 2)
        }
        /*if(term == "r5" && itemType=="3 Year"){
          resultPrice = Library.round(resultPrice,0)
        }*/
        return resultPrice
    }
} else
    return null