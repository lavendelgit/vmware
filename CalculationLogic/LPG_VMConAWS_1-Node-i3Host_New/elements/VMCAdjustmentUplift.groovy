if (api.isSyntaxCheck()) return
def upliftType = api.getElement("SWUpliftType")
if (upliftType == null) {
    upliftType == "LC Factor"
}
if (upliftType != null) {
    upliftType = upliftType?.toString()
}
def tab = api.findLookupTable("VMCAdjustmentUplift")
def dataCenter = api.getElement("DataCenter")?.toString()
def tabList

def tab2 = api.findLookupTable("AWSHWPrice")
def hostType = api.getElement("HostType")?.toString()
def itemType = api.getElement("ItemType")
def govCloudList
def usWestOregonList
api.trace("upliftType", null, upliftType)
if (dataCenter == "AWS GovCloud US" && upliftType == "AWS HW") {
    def AWSUpliftList = api.find("MLTV2", Filter.equal("lookupTable.id", tab2?.id),
            Filter.equal("key1", "AWS GovCloud US"),
            Filter.equal("key2", hostType))
    if (AWSUpliftList != null) {
        if (itemType == "On Demand") {
            return AWSUpliftList[0]?.attribute5
        } else if (itemType == "1 Year") {
            return AWSUpliftList[0]?.attribute6
        } else {
            return AWSUpliftList[0]?.attribute7
        }
    }
} else if ((dataCenter == "AWS GovCloud US" && upliftType == "LC Factor") || upliftType == "AWS HW") {
    govCloudList = api.find("MLTV2", Filter.equal("lookupTable.id", tab2?.id),
            Filter.equal("key1", dataCenter),
            Filter.equal("key2", hostType))
    usWestOregonList = api.find("MLTV2", Filter.equal("lookupTable.id", tab2?.id),
            Filter.equal("key1", "US West Oregon"),
            Filter.equal("key2", hostType))
    if (govCloudList != null && usWestOregonList != null) {
        def govCloudVal
        def usWestOregonVal
        if (itemType == "On Demand") {
            govCloudVal = govCloudList[0]?.attribute2?.toDouble()
            usWestOregonVal = usWestOregonList[0]?.attribute2?.toDouble()
            if (usWestOregonVal != 0 && usWestOregonVal != null && govCloudVal != null) {
                def val = (govCloudVal - usWestOregonVal) / usWestOregonVal
                return val?.toBigDecimal()
            }
        } else if (itemType == "1 Year") {
            govCloudVal = govCloudList[0]?.attribute3?.toDouble()
            usWestOregonVal = usWestOregonList[0]?.attribute3?.toDouble()
            if (usWestOregonVal != 0 && usWestOregonVal != null && govCloudVal != null) {
                def val = (govCloudVal - usWestOregonVal) / usWestOregonVal
                return val?.toBigDecimal()
            }
        } else {
            govCloudVal = govCloudList[0]?.attribute4?.toDouble()
            usWestOregonVal = usWestOregonList[0]?.attribute4?.toDouble()
            if (usWestOregonVal != 0 && usWestOregonVal != null && govCloudVal != null) {
                def val = (govCloudVal - usWestOregonVal) / usWestOregonVal
                return val?.toBigDecimal()
            }
        }
    }
} else {
    if (dataCenter != null) {
        tabList = api.find("MLTV", Filter.equal("lookupTable.id", tab?.id),
                Filter.equal("name", dataCenter))
    }
    if (tabList != null) {
        def LCFactor = tabList[0]?.attribute3?.toDouble()
        def latestFX = tabList[0]?.attribute4?.toDouble()
        api.trace("val1", null, LCFactor)
        api.trace("val2", null, latestFX)
        if (LCFactor != 0 && latestFX != 0 && LCFactor != null && latestFX != null) {
            def SaaS
            SaaS = (LCFactor / latestFX) - 1
            return SaaS?.toBigDecimal()
        }
    }
}