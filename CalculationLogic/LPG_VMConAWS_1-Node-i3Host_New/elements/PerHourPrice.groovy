/*if (api.syntaxCheck){
  return null
}*/
def threeYearTCO = api.getElement("3YearTCO") ?: 0
threeYearTCO = threeYearTCO?.toDouble()
//api.trace("threeYearTCO",null,threeYearTCO)
def val = threeYearTCO / 36 / 730
//api.trace("val",null,val)
return val?.toBigDecimal()