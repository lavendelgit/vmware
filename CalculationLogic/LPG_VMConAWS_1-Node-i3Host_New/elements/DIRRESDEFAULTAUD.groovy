/*def utilLib = libs.vmwareUtil.util
def dirRESDefaultDollar = api.getElement("DIRRESDEFAULTDollar")
def term = api.getElement("HostType")
def exchangeRate = utilLib.getExchangeRate("AUD", term)
if (exchangeRate != null)
	return  dirRESDefaultDollar * exchangeRate
else
  	return null
*/
def resultPrice = api.getElement("ResultPriceAUD")
def DIRRESDEFAULT = api.getElement("DIRRESDEFAULT")
def itemType = api.getElement("ItemType")

if (itemType != null && resultPrice != null && DIRRESDEFAULT != null) {
    def result = resultPrice * (1 - DIRRESDEFAULT)
    if (itemType == "On Demand") {
        return Library.round(result, 6)
    } else {
        return Library.round(result, 2)
    }
    //return resultPrice * (1-DIRRESDEFAULT)
}