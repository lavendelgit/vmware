def itemType = api.product("attribute14")
if (itemType != null) {
    if (itemType == 1) {
        return "On Demand"
    } else if (itemType == 12) {
        return "1 Year"
    } else if (itemType == 36) {
        return "3 Year"
    }
}
//else return api.product("attribute8") - ItemType is empty for all VMC on AWS