def extn = api.getElement("VMConAWSExtn")
if (extn != null && extn["attribute9"][0] != null) {
    api.logInfo("long description", extn["attribute9"][0])
    if (extn["attribute9"][0].length() > 70)
        return extn["attribute9"][0].substring(0, 70)
    else return extn["attribute9"][0]
}