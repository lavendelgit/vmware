if (api.syntaxCheck) {
    return null
}
def aws = api.getElement("AWSi3Host")
def hwSupportPercent = api.getElement("HardwareSupportPercent")
if (aws != null && hwSupportPercent != null) {
    /*if(api.global.AWSHW3Year!=null)
    api.global.HWSupport3Year = api.global.AWSHW3Year*hwSupportPercent
    api.trace("HWSupport3Year",null,api.global.HWSupport3Year)*/
    return aws * hwSupportPercent
}
