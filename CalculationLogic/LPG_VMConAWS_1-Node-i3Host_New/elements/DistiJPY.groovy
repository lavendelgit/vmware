/*def utilLib = libs.vmwareUtil.util
def distiUSD = api.getElement("DistiDollar")
def term = api.getElement("HostType")
def exchangeRate = utilLib.getExchangeRate("JPY", term)
if (exchangeRate != null)
	return  distiUSD * exchangeRate
else
  	return null
*/
def resultPrice = api.getElement("ResultPriceJPY")
def disti = api.getElement("Disti")
def itemType = api.getElement("ItemType")

if (itemType != null && resultPrice != null && disti != null) {
    def result = resultPrice * (1 - disti)
    if (itemType == "On Demand") {
        return Library.round(result, 6)
    } else {
        return Library.round(result, 0)
    }
    //return resultPrice * (1-disti)
}