if (api.syntaxCheck) {
    return null
}
def itemType = api.getElement("ItemType")
if (itemType == "On Demand") {
    return "N/A"
}
def VMConAWS = api.getElement("UpliftedVMCOnAWSTerm") ?: 0
if (VMConAWS != null) {
    return VMConAWS * 730 * 12
}