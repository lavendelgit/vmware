if (api.isSyntaxCheck()) return
def itemType = api.getElement("ItemType")?.toString()
def hostType = api.getElement("HostType")?.toString()
def upliftType = api.getElement("SWUpliftType")
if (upliftType != null) {
    upliftType = upliftType?.toString()
}
def dataCenterBase = "US West Oregon"
def dataCenter = api.getElement("DataCenter")?.toString()
def resultPriceBeforeFactor = api.getElement("ResultPriceBeforeFactor")
if (itemType == "3 Year") {
    resultPriceBeforeFactor = resultPriceBeforeFactor / 3
}
def valOD1
def base
def factorOD1
if (itemType != null && hostType != null && (itemType == "On Demand" || itemType == "1 Year")) {
    base = libs.vmwareUtil.USWestOregonBase.getUSWestOregonBase(hostType, itemType, dataCenterBase, upliftType)
    if (base != null && base != 0 && resultPriceBeforeFactor != null) {
        api.trace("base", null, base)
        api.trace("resultPriceBeforeFactor", null, resultPriceBeforeFactor)
        factorOD1 = resultPriceBeforeFactor / base
        valOD1 = Library.round(factorOD1, 6)
        if (itemType == "On Demand" || itemType == "1 Year") {
            return valOD1
        }
    }
}
def oneYearPrice
if (itemType == "3 Year" && dataCenter != null) {
    def oneYearPriceBeforeFactorBase = libs.vmwareUtil.OneYearPrice.OneYearPrice(hostType, "1 Year", dataCenterBase, upliftType)
    def oneYearPriceBeforeFactor = libs.vmwareUtil.OneYearPrice.OneYearPrice(hostType, "1 Year", dataCenter, upliftType)
    api.trace("oneYearPriceBeforeFactorBase", null, oneYearPriceBeforeFactorBase)
    api.trace("oneYearPriceBeforeFactor", null, oneYearPriceBeforeFactor)
    def factor
    if (oneYearPriceBeforeFactor != null && oneYearPriceBeforeFactorBase != null && oneYearPriceBeforeFactorBase != 0)
        factor = oneYearPriceBeforeFactor / oneYearPriceBeforeFactorBase
    factor = Library.round(factor, 6)
    api.trace("factor", null, factor)
    api.trace("oneYearPriceBeforeFactorBase", null, oneYearPriceBeforeFactorBase)
    if (factor != null && oneYearPriceBeforeFactorBase != null) {
        oneYearPrice = factor * oneYearPriceBeforeFactorBase
        oneYearPrice = Library.round(oneYearPrice, 2)
        api.trace("oneYearPrice", null, oneYearPrice)
        def threeYearPrice = oneYearPrice * 3
        if (resultPriceBeforeFactor) {
            resultPriceBeforeFactor = resultPriceBeforeFactor * 3
            api.trace("resultPriceBeforeFactor", null, resultPriceBeforeFactor)
            def val = 1 - ((threeYearPrice - resultPriceBeforeFactor) / threeYearPrice)
            val = Library.round(val, 6)
            if (threeYearPrice != null && val != null) {
                api.global.resultPrice = threeYearPrice * val
            }
            return val
        }
    }
}