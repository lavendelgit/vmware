if (api.isSyntaxCheck()) return
def itemType = api.getElement("ItemType")?.toString()
def VMConAWS = api.getElement("UpliftedVMCOnAWSTerm")
def VMConAWS1Year = api.getElement("VMCOnAWS1YearTerm")
def VMConAWS3Year = api.getElement("VMCOnAWS3YearTerm")
def resultPrice
if (itemType != null) {
    if (itemType == "On Demand") {
        resultPrice = Library.round(VMConAWS, 6)
    }
    if (itemType == "1 Year") {
        resultPrice = Library.round(VMConAWS1Year, 2)
    }
    if (itemType == "3 Year") {
        resultPrice = Library.round(VMConAWS3Year, 2)
    }
    return resultPrice
}
