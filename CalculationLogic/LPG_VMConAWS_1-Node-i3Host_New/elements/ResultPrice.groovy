if (api.isSyntaxCheck()) return
def factor = api.getElement("Factor")
def itemType = api.getElement("ItemType")?.toString()
def hostType = api.getElement("HostType")?.toString()
def upliftType = api.getElement("SWUpliftType")
if (upliftType != null) {
    upliftType = upliftType?.toString()
}
def dataCenter = "US West Oregon"
if (itemType != null && hostType != null && (itemType == "On Demand" || itemType == "1 Year")) {
    def base = libs.vmwareUtil.USWestOregonBase.getUSWestOregonBase(hostType, itemType, dataCenter, upliftType)
    if (factor != null && base != null) {
        def result = factor * base
        api.trace("factor:" + factor + "	base:" + base + "	result:", result)
        if (result != null) {
            if (itemType == "On Demand") {
                result = result * (1 - api.getElement("Discount"))
                //api.trace("Result Price:", result)
                return Library.round(result, 6)
            } else {
                return Library.round(result, 2)
            }
        }
    }
}
if (itemType == "3 Year") {
    def val = api.global.resultPrice
    return Library.round(val, 2)
}