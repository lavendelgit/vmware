if (api.syntaxCheck) {
    return null
}
def channelPartnerUplift = api.getElement("ChannelPartnerUplift") ?: 0
def aws = api.getElement("AWSi3Host") ?: 0
def hardwareSupport = api.getElement("HardwareSupport") ?: 0
if (channelPartnerUplift != null && aws != null && hardwareSupport != null) {
    /*if(api.global.HWSupport3Year!=null && api.global.AWSHW3Year!=null){
      api.trace("AWSHW3Year",null,api.global.AWSHW3Year)
      api.trace("HWSupport3Year",null,api.global.HWSupport3Year)
      api.global.VMConAWS3Year = channelPartnerUplift + api.global.AWSHW3Year?.toBigDecimal() + api.global.HWSupport3Year?.toBigDecimal()}
    api.trace("VMConAWS3Year",null,api.global.VMConAWS3Year)*/
    return channelPartnerUplift + aws + hardwareSupport
}