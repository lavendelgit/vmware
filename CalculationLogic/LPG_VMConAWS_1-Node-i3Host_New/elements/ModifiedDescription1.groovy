def extn = api.getElement("VMConAWSExtn")
if (extn != null && extn["attribute8"][0] != null) {
    api.logInfo("mdified description", extn["attribute8"][0])
    if (extn["attribute8"][0].length() > 70)
        return extn["attribute8"][0].substring(0, 70)
    else return extn["attribute8"][0]
}