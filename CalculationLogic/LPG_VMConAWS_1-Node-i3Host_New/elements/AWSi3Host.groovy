if (api.syntaxCheck) {
    return null
}
def tab = api.findLookupTable("AWSHWPrice")
def dataCenter = api.getElement("DataCenter")?.toString()
def hostType = api.getElement("HostType")?.toString()
def itemType = api.getElement("ItemType")
def tabList
if (dataCenter != null && hostType != null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("key1", dataCenter),
            Filter.equal("key2", hostType))
}
if (tabList != null) {
    api.trace("aws", null, tabList)
    if (itemType == "On Demand") {
        return tabList[0]?.attribute2
    }
    if (itemType == "1 Year") {
        return tabList[0]?.attribute3
    }
    if (itemType == "3 Year") {
        //api.global.AWSHW3Year = tabList[0]?.attribute3
        return tabList[0]?.attribute4
    }
}