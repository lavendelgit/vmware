def productDescription = api.product("label")

if (productDescription != null) {
    api.logInfo("productDescription", productDescription)
    if (productDescription.length() > 70)
        return api.attributedResult(productDescription.substring(0, 70)).withSuffix(productDescription.substring(70))
    else return productDescription
}