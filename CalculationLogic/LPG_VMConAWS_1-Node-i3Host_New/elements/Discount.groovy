def itemType = api.getElement("ItemType")?.toString()
def hostType = api.getElement("HostType")?.toString()
def upliftType = api.getElement("SWUpliftType")
if (upliftType != null) {
    upliftType = upliftType?.toString()
}
def dataCenterBase = "US West Oregon"
def base = libs.vmwareUtil.USWestOregonBase.getUSWestOregonBase(hostType, itemType, dataCenterBase, upliftType)
def discount
def usWestOregonBase = api.getElement("USWestOregonBasePrice")
if (itemType != null && hostType != null && base != null && usWestOregonBase != null)
    discount = 1 - (usWestOregonBase / base)
return discount