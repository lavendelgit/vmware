if (api.syntaxCheck) {
    return null
}
def host

def tab = api.findLookupTable("HostPerCore")
def hostType = api.getElement("HostType")
def tabList
if (hostType != null) {
    tabList = api.find("MLTV", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("name", hostType))
    if (tabList != null) {
        host = tabList[0]?.attribute1
    }
}

def perCore = api.getElement("PerCorePrice")
if (perCore != null && host) {
    return perCore * host
}