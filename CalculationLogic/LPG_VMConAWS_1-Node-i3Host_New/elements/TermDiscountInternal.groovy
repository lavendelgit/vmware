if (api.isSyntaxCheck()) return
def termDiscount = api.getElement("TermDiscount")
if (termDiscount != null) {
    termDiscount = termDiscount / 100
}