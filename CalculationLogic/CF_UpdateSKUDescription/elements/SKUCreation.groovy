api.local.descriptionCache = []
String shortDesc = ""
Map longDesc = [:]
String warning
String longDescription
Integer shortDescLen = out.SKUConfigCache?.ShortDescLen as Integer
shortDescLen = shortDescLen ?: Constants.SHORT_DESC_DEFAULT_LEN
Integer longDescLen = out.SKUConfigCache?.LongDescLen as Integer
longDescLen = longDescLen ?: Constants.LONG_DESC_DEFAULT_LEN
for (standardProduct in api.local.validStandardProductsCache) {
    rule = standardProduct.value
    pXParams = [
            "TermUoM"             : rule.Term ? out.TermUomCache[rule.Term] : Constants.NULL_ATTRIBUTE_VALUE_PLACEHOLDER,
            "RetentionPeriodUOM"  : rule["Retention Period"] ? out.RetentionPeriodUomCache[rule["Retention Period"]] : Constants.NULL_ATTRIBUTE_VALUE_PLACEHOLDER,
            "ProductKey"          : rule?.Product,
            "OfferKey"            : rule?.Offer,
            "DataCenterKey"       : rule["Data Center"],
            "BaseKey"             : rule?.Base,
            "MetricKey"           : rule?.Metric,
            "TermKey"             : rule?.Term,
            "PaymentTypeKey"      : rule["Payment Type"],
            "VolumeTierKey"       : rule["Volume Tier"],
            "SegmentKey"          : rule?.Segment,
            "VersionKey"          : rule?.Version,
            "PromotionKey"        : rule?.Promotion,
            "PSTermKey"           : rule["PS Term"],
            "PSOptionKey"         : rule["PS Option"],
            "QuantityKey"         : rule?.Quantity,
            "SupportTierKey"      : rule["Support Tier"],
            "SupportTypeKey"      : rule["Support Type"],
            "PurchasingProgramKey": rule["Purchasing Program"],
            "OSKey"               : rule?.OS,
            "HostingKey"          : rule?.Hosting,
            "ProgramOptionKey"    : rule["Program Option"],
            "RetentionPeriodKey"  : rule["Retention Period"],
    ]
    sku = rule.StandardProductSKU
    shortDesc = libs.stdProductLib.SkuGenHelper.createDesc(out.ShortDescConfig, pXParams)
    longDescription = libs.stdProductLib.SkuGenHelper.createDesc(out.LongDescConfig, pXParams)
    shortDescTrimmed = shortDesc
    longDescTrimmed = longDescription
    if (shortDesc.length() > shortDescLen) {
        shortDescTrimmed = shortDesc?.substring(0, shortDescLen)
        warning = Constants.SHORT_DESC_TRUNCATED_MSG
    }
    if (longDescription.length() > longDescLen) {
        longDescTrimmed = longDescription?.substring(0, longDescLen)
        warning = warning?.length() > 0 ? Constants.SHORT_LONG_DESC_TRUNCATED_MSG : Constants.LONG_DESC_TRUNCATED_MSG
    }
    api.local.descriptionCache << [out.StandardDescriptionPPCache, sku, api.jsonEncode([Product: pXParams.ProductKey, Offer: pXParams.OfferKey, Base: pXParams.BaseKey, ShortDescription: shortDescTrimmed, LongDescription: longDescTrimmed, ActualShortDescription: shortDesc, ActualLongDescription: longDescription, Warning: warning])]
}
return