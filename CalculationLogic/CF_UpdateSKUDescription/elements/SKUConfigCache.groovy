List filter = [
        Filter.in(Constants.KEY_NAME, [Constants.SHORT_DESCRIPTION_LENGTH, Constants.LONG_DESCRIPTION_LENGTH])
]
return api.findLookupTableValues(Constants.SKUCONFIG_TABLE_NAME, *filter)?.collectEntries { [(it.name): it.attribute1] }