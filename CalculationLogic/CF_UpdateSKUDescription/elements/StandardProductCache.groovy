List filter = [
        Filter.equal("name", "StandardProducts"),
        Filter.in("attribute1", out.ProductCache?.keySet()),
        Filter.in("attribute2", out.OfferCache ? out.OfferCache.keySet() : null),
        Filter.in("attribute4", out.BaseCache ? out.BaseCache.keySet() : null),
        Filter.in("attribute22", out.OSCache ? out.OSCache.keySet() : null),
        Filter.in("attribute5", out.MetricCache ? out.MetricCache.keySet() : null),
        Filter.in("attribute3", out.DataCenterCache ? out.DataCenterCache.keySet() : null),
        Filter.in("attribute12", out.PSTermCache ? out.PSTermCache.keySet() : null),
        Filter.in("attribute13", out.PSOptionCache ? out.PSOptionCache.keySet() : null),
        Filter.in("attribute9", out.SegmentCache ? out.SegmentCache.keySet() : null),
        Filter.in("attribute16", out.QuantityCache ? out.QuantityCache.keySet() : null),
        Filter.in("attribute6", out.TermCache ? out.TermCache.keySet() : null),
        Filter.in("attribute7", out.PaymentTypeCache ? out.PaymentTypeCache.keySet() : null),
        Filter.in("attribute19", out.SupportTypeCache ? out.SupportTypeCache.keySet() : null),
        Filter.in("attribute18", out.SupportTierCache ? out.SupportTierCache.keySet() : null),
        Filter.in("attribute20", out.PurchasingProgramCache ? out.PurchasingProgramCache.keySet() : null),
        Filter.in("attribute8", out.VolumeTierCache ? out.VolumeTierCache.keySet() : null),
        Filter.in("attribute10", out.VersionCache ? out.VersionCache.keySet() : null),
        Filter.in("attribute11", out.PromoCache ? out.PromoCache.keySet() : null),
        Filter.in("attribute23", out.HostingCache ? out.HostingCache.keySet() : null),
        Filter.in("attribute25", out.ProgramOptionCache ? out.ProgramOptionCache.keySet() : null),
        Filter.in("attribute31", out.RetentionPeriodCache ? out.RetentionPeriodCache.keySet() : null),

]

recordStream = api.stream("PX50", null, *filter)
api.local.standardProductCache = recordStream?.collect { row -> row }
recordStream?.close()
api.local.validStandardProductsCache = libs.stdProductLib.SkuGenHelper.cacheValidRules(api.local.standardProductCache, out.StandardProductMetaData, out.RuleKeyMapping, true, out.AttributeDelimiterCache)
return
