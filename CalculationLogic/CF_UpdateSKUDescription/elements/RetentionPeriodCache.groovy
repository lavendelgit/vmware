Map validTermCache = libs.stdProductLib.RulesGenHelper.cacheCommonAttributes(out.Configuration.RETENTION_PERIOD_TABLE, false)
return validTermCache?.collectEntries { [(it.key): it.value.attribute1] }