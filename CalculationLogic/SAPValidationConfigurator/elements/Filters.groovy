def ce = api.createConfiguratorEntry()
def lpgName = ce.createParameter(InputType.STRINGUSERENTRY, "LPGName")
api.trace("lpg name:", lpgName.getValue())
lpgName = lpgName.getValue()
//lpgName = "VMConAWSi3r5_4_Hema"
if (lpgName != null) {
    def Nodei3LpgList = api.find("PG", 0, 1, null, Filter.equal("label", lpgName))
    api.trace("Nodei3LpgList:", Nodei3LpgList)
    def lpgMeta
    if (Nodei3LpgList != null && Nodei3LpgList[0] != null)
        lpgMeta = api.find("PGIM", Filter.equal("priceGridId", Nodei3LpgList[0].id))
    api.local.mappingData = [:]
    if (lpgMeta != null)
        lpgMeta.each {
            //api.trace("lpgMeta:", lpgMeta)
            api.local.mappingData << [((it.elementName ? it.elementName : it.fieldName)): [fieldName  : it.fieldName,
                                                                                           label      : it.label,
                                                                                           formatType : it.formatType,
                                                                                           elementName: it.elementName]]
        }
    def productFamilyList
    def productFamilyAttributeName = getAttributeName("Description")
    api.trace("productFamilyAttributeName", productFamilyAttributeName)
    if (Nodei3LpgList != null && Nodei3LpgList[0] != null) {
        api.trace("Nodei3LpgList[0].id:", Nodei3LpgList[0].id)
        productFamilyList = api.find("PGI", 0, 200, productFamilyAttributeName, [productFamilyAttributeName], true,
                Filter.equal("priceGridId", Nodei3LpgList[0].id))//.productFamilyAttributeName//.attribute45
        productFamilyList = productFamilyList?.collect { it?.get(productFamilyAttributeName) ?: "" }
    }
    api.trace("ProductFamilyList:", productFamilyList)
    def productFamily = ce.createParameter(InputType.OPTIONS, "ProductFamily")
    productFamily?.setValueOptions(productFamilyList)

    def prodIdList
    def productIdAttributeName = getAttributeName("SAPmaterialnumber")
    api.trace("productIdAttributeName", productIdAttributeName)
    if (Nodei3LpgList != null && Nodei3LpgList[0] != null) {
        prodIdList = api.find("PGI", 0, 200, productIdAttributeName, [productIdAttributeName], true,
                Filter.equal("priceGridId", Nodei3LpgList[0].id))//.productIdAttributeName//.attribute66
        prodIdList = prodIdList?.collect { it?.get(productIdAttributeName) ?: "" }
    }
    api.trace("prodIdList:", prodIdList)
    def productId = ce.createParameter(InputType.OPTIONS, "ProductId")
    productId?.setValueOptions(prodIdList)

    def usageItemList
    def usageItemAttributeName = getAttributeName("UsageItem")
    api.trace("usageItemAttributeName", usageItemAttributeName)
    if (Nodei3LpgList != null && Nodei3LpgList[0] != null) {
        usageItemList = api.find("PGI", 0, 200, usageItemAttributeName, [usageItemAttributeName], true,
                Filter.equal("priceGridId", Nodei3LpgList[0].id))//.usageItemAttributeName//.attribute69
        usageItemList = usageItemList?.collect { it?.get(usageItemAttributeName) ?: "" }
    }

    api.trace("usageItemList:", usageItemList)
    def usageItem = ce.createParameter(InputType.OPTIONS, "UsageItem")
    usageItem?.setValueOptions(usageItemList)

    def dataCenterList
    def dataCenterAttributeName = getAttributeName("Datacentercode")
    api.trace("dataCenterAttributeName", dataCenterAttributeName)
    if (Nodei3LpgList != null && Nodei3LpgList[0] != null) {
        dataCenterList = api.find("PGI", 0, 200, dataCenterAttributeName, [dataCenterAttributeName], true,
                Filter.equal("priceGridId", Nodei3LpgList[0].id))//.dataCenterAttributeName//.attribute62
        dataCenterList = dataCenterList?.collect { it?.get(dataCenterAttributeName) ?: "" }
    }
    api.trace("dataCenterList:", dataCenterList)
    def dataCenter = ce.createParameter(InputType.OPTIONS, "DataCenter")
    dataCenter?.setValueOptions(dataCenterList)

    def billingTermList
    def billingTermAttributeName = getAttributeName("BillingTerm")
    api.trace("billingTermAttributeName", billingTermAttributeName)
    if (Nodei3LpgList != null && Nodei3LpgList[0] != null) {
        billingTermList = api.find("PGI", 0, 200, billingTermAttributeName, [billingTermAttributeName], true,
                Filter.equal("priceGridId", Nodei3LpgList[0].id))//.billingTermAttributeName//.attribute63
        billingTermList = billingTermList?.collect { it?.get(billingTermAttributeName) ?: "" }
    }
    api.trace("billingTermList:", billingTermList)
    def billingTerm = ce.createParameter(InputType.OPTIONS, "Billing Term")
    billingTerm?.setValueOptions(billingTermList)

    def billingFrequencyList
    def billingFrequencyAttributeName = getAttributeName("BillingTermFrequency")
    api.trace("billingFrequencyAttributeName", billingFrequencyAttributeName)
    if (Nodei3LpgList != null && Nodei3LpgList[0] != null) {
        billingFrequencyList = api.find("PGI", 0, 200, billingFrequencyAttributeName, [billingFrequencyAttributeName], true,
                Filter.equal("priceGridId", Nodei3LpgList[0].id))//.billingFrequencyAttributeName//.attribute96
        billingFrequencyList = billingFrequencyList?.collect { it?.get(billingFrequencyAttributeName) ?: "" }
    }
    api.trace("billingFrequencyList:", billingFrequencyList)
    def billingFrequency = ce.createParameter(InputType.OPTIONS, "BillingFrequency")
    billingFrequency?.setValueOptions(billingFrequencyList)

}

def getAttributeName(elementName) {
    def retValue = (api.local.mappingData.get(elementName) ? api.local.mappingData.get(elementName).fieldName : elementName)
    return retValue
}

return ce