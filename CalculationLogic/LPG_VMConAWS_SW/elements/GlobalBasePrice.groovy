List listPrice = api.productExtension("ListPrice")
if (listPrice) {
    return Library.round(listPrice[0]["attribute1"], 6)
}

api.addWarning("Global Base Price not found")

return 0