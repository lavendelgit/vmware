String dataCenter = api.getElement("DataCenterFrom")
if (dataCenter == "US West Oregon") {
    return 0
}

BigDecimal i3Adjustment = calculateAdjustment("i3", dataCenter)
BigDecimal r5Adjustment = calculateAdjustment("r5", dataCenter)

api.trace("i3Adjustment", i3Adjustment)
api.trace("r5Adjustment", r5Adjustment)
api.trace("dataCenter", dataCenter)

BigDecimal saasAdjustment = i3Adjustment.max(r5Adjustment)

return saasAdjustment

def calculateAdjustment(String hostType, String dataCenter) {
    String termAttribute = "attribute2"
    BigDecimal usWestOregonPricePoint = api.vLookup("AWSHWPrice", termAttribute, "US West Oregon", hostType)
    BigDecimal pricePoint = api.vLookup("AWSHWPrice", termAttribute, dataCenter, hostType)

    api.trace("usWestOregonPricePoint", usWestOregonPricePoint)
    api.trace("pricePoint", pricePoint)
    if (pricePoint) {
        return ((pricePoint - usWestOregonPricePoint) / usWestOregonPricePoint).setScale(15)
    }
    return 0
}