BigDecimal result = api.getElement("JPY") * (1 - api.getElement("ChannelDiscount")["distiResEmc"])

if (api.getElement("Is1YrDraas") || api.getElement("Is1YrDraas")) {
    return Library.round(result, 0)
}

return result