BigDecimal resultPrice = api.getElement("GlobalBasePrice") * new BigDecimal(api.getElement("Factor"))
api.trace("Result Price before rounding", resultPrice)

if (api.getElement("Is3YrDraas")) {
    resultPrice = Library.round(resultPrice, 2)
    resultPrice = Library.round(3 * resultPrice, 2) * api.getElement("3YrDiscount")

}

if (api.getElement("Is1YrDraas") || api.getElement("Is3YrDraas")) {
    return Library.round(resultPrice, 2)
}

return Library.round(resultPrice, 6)