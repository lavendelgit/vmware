if (api.isSyntaxCheck()) {
    return null
}

List westOregonFilters = [
        Filter.equal("attribute4", api.getElement("HostType")),
        Filter.equal("attribute3", "US West Oregon")
]
String billingTerm = api.getElement("BillingTerm")
if (!billingTerm) {
    westOregonFilters.add(Filter.isEmpty("attribute22"))
} else {
    westOregonFilters.add(Filter.equal("attribute22", billingTerm))
}


def usWestOregon = api.find("P", *westOregonFilters)
if (!usWestOregon) {
    api.addWarning("US West Oregon Not Found")
    return
}

String usWestOregonSku = usWestOregon[0]["sku"]

List westOregonPriceFilters = [
        Filter.equal("name", "ListPrice"),
        Filter.equal("sku", usWestOregonSku)
]

BigDecimal usWestOregonSkuPrice = new BigDecimal(api.find("PX6", 0, "sku", *westOregonPriceFilters)["attribute1"][0]).setScale(6, BigDecimal.ROUND_HALF_UP)
BigDecimal basePriceInclUplift = api.getElement("BasePriceInclUplift")

api.trace("usWestOregonSkuPrice", usWestOregonSkuPrice)
api.trace("basePriceInclUplift", basePriceInclUplift)

return Library.round(1 + (basePriceInclUplift - usWestOregonSkuPrice) / usWestOregonSkuPrice, 6)
