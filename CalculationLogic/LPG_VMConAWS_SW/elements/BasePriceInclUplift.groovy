BigDecimal uplift = api.getElement("AdjustmentUplift")
BigDecimal basePrice = api.getElement("GlobalBasePrice")

if (uplift == null) {
    return 0
}

return (uplift + 1) * basePrice