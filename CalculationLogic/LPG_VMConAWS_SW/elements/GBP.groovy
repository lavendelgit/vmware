BigDecimal basePriceUSD = api.getElement("ResultPrice")

return Library.round(basePriceUSD * libs.vmwareUtil.util.getExchangeRate("GBP", "i3"), api.getElement("Rounding"))