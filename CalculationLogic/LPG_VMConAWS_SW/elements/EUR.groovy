BigDecimal basePriceUSD = api.getElement("ResultPrice")
return Library.round(basePriceUSD * libs.vmwareUtil.util.getExchangeRate("EUR", "i3"), api.getElement("Rounding"))