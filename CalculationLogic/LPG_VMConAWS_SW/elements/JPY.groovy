BigDecimal basePriceUSD = api.getElement("ResultPrice")
BigDecimal result = Library.round(basePriceUSD * libs.vmwareUtil.util.getExchangeRate("JPY", "i3"), 6)

if (api.getElement("Is1YrDraas") || api.getElement("Is3YrDraas")) {
    return Library.round(result, 0)
}

return result