def hostType = api.getElement("HostType")
def itemType = api.getElement("ItemType")
def dataCenter = "US West Oregon"
def upliftType = api.getElement("SWUpliftType")
if (upliftType != null) {
    upliftType = upliftType[0]
}
def basePrice = libs.vmwareUtil.USWestOregonBase.getUSWestOregonBase(hostType, itemType, dataCenter, upliftType)

return basePrice