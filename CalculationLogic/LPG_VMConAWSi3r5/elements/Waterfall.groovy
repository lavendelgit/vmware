if (api.isSyntaxCheck()) return
def componentA = api.getElement("ComponentsActualPrice") ?: 0
def componentD = api.getElement("ComponentsDiscountedPrice") ?: 0
def discount = componentD - componentA//
def threeYearTCO = api.getElement("3YearTCO") ?: 0
def VMCTCODiscount = threeYearTCO - componentD
def perHour = api.getElement("PerHourPrice") ?: 0
def perHourThreeYearDiscount = perHour - threeYearTCO
def perHourHost = api.getElement("PerHourHost") ?: 0
def perHHPerHDiscount = perHourHost - perHour
def channelPartnerUplift = api.getElement("ChannelPartnerUplift") ?: 0
def CPUPerHHDiscount = channelPartnerUplift - perHourHost
def aws = api.getElement("AWSi3Host") ?: 0
def hardware = api.getElement("HardwareSupport") ?: 0
def upliftedVMConAWS = api.getElement("UpliftedVMCOnAWS") ?: 0
def total = channelPartnerUplift + aws + hardware
def uplift = upliftedVMConAWS - total
def hostType = api.getElement("HostType")?.toString()
def dataCenter = api.getElement("DataCenter")?.toString()
def itemType = api.getElement("ItemType")?.toString()
def title = hostType + " " + dataCenter + " " + itemType
def oneYearVMC = api.getElement("VMCOnAWS1Year")
def diff1Year
if (itemType == "1 Year") {
    diff1Year = oneYearVMC - upliftedVMConAWS
}
def threeYearVMC = api.getElement("VMCOnAWS3Year")
def diff3Year
if (itemType == "3 Year") {
    diff3Year = threeYearVMC - upliftedVMConAWS
}
def vmcCalculated = api.getElement("UpliftedVMCOnAWSTerm")
def vmcCalculated1Year = api.getElement("VMCOnAWS1YearTerm")
def vmcCalculated3Year = api.getElement("VMCOnAWS3YearTerm")
def actualCalculatedDif = 0
if (itemType != null) {
    if (itemType == "On Demand") {
        actualCalculatedDif = vmcCalculated - upliftedVMConAWS
    }
    if (itemType == "1 Year") {
        actualCalculatedDif = vmcCalculated1Year - oneYearVMC
    }
    if (itemType == "3 Year") {
        actualCalculatedDif = vmcCalculated3Year - threeYearVMC
    }
}


def chartDef1 = [
        title  : [
                text: title
        ],
        tooltip: [
                pointFormat: '$ <b>{point.y:,.2f}</b>'
        ],
        series : [
                data: [[
                               name: "Components Actual",
                               y   : componentA
                       ], [
                               name: "Discount",
                               y   : discount
                       ], [
                               name   : "VMC",
                               "isSum": true
                       ], [
                               name: "SnS",
                               y   : VMCTCODiscount
                       ], [
                               name   : "3 Year TCO",
                               "isSum": true
                       ], [
                               name: "Term Adjustment",
                               y   : perHourThreeYearDiscount
                       ], [
                               name   : "Per Hour",
                               "isSum": true
                       ], [
                               name: "Metric Adjustment",
                               y   : perHHPerHDiscount
                       ], [
                               name   : "Per Hour Host",
                               "isSum": true
                       ], [
                               name: "Standard Uplifts",
                               y   : CPUPerHHDiscount
                       ], [
                               name   : "Subscription OnDemand Per Hour Per Host",
                               "isSum": true
                       ], [
                               name: "AWS HW",
                               y   : aws
                       ], [
                               name: "Hardware Support",
                               y   : hardware
                       ], [
                               name   : "VMC on AWS",
                               "isSum": true
                       ], [
                               name: "SW HW Uplift",
                               y   : uplift
                       ], [
                               name   : "Calculated VMC on AWS",
                               "isSum": true
                       ], [
                               name: "Calculated Actual Adjustment",
                               y   : actualCalculatedDif
                       ], [
                               name   : "Actual VMC on AWS",
                               "isSum": true
                       ]
                ]
        ]
]
def chartDef2 = [
        title  : [
                text: title
        ],
        tooltip: [
                pointFormat: '$ <b>{point.y:,.2f}</b>'
        ],
        series : [
                data: [[
                               name: "Components Actual",
                               y   : componentA
                       ], [
                               name: "Discount",
                               y   : discount
                       ], [
                               name   : "VMC",
                               "isSum": true
                       ], [
                               name: "SnS",
                               y   : VMCTCODiscount
                       ], [
                               name   : "3 Year TCO",
                               "isSum": true
                       ], [
                               name: "Term Adjustment",
                               y   : perHourThreeYearDiscount
                       ], [
                               name   : "Per Hour",
                               "isSum": true
                       ], [
                               name: "Metric Adjustment",
                               y   : perHHPerHDiscount
                       ], [
                               name   : "Per Hour Host",
                               "isSum": true
                       ], [
                               name: "Standard Uplifts",
                               y   : CPUPerHHDiscount
                       ], [
                               name   : "Subscription OnDemand Per Hour Per Host",
                               "isSum": true
                       ], [
                               name: "AWS HW",
                               y   : aws
                       ], [
                               name: "Hardware Support",
                               y   : hardware
                       ], [
                               name   : "VMC on AWS",
                               "isSum": true
                       ], [
                               name: "SW HW Uplift",
                               y   : uplift
                       ], [
                               name   : "Uplifted VMC on AWS",
                               "isSum": true
                       ], [
                               name: "1 Year Host Adjustment",
                               y   : diff1Year
                       ], [
                               name   : "Calculated VMC on AWS 1 Year",
                               "isSum": true
                       ], [
                               name: "Calculated Actual Adjustment",
                               y   : actualCalculatedDif
                       ], [
                               name   : "Actual VMC on AWS",
                               "isSum": true
                       ]
                ]
        ]
]
def chartDef3 = [
        title  : [
                text: title
        ],
        tooltip: [
                pointFormat: '$ <b>{point.y:,.2f}</b>'
        ],
        series : [
                data: [[
                               name: "Components Actual",
                               y   : componentA
                       ], [
                               name: "Discount",
                               y   : discount
                       ], [
                               name   : "VMC",
                               "isSum": true
                       ], [
                               name: "SnS",
                               y   : VMCTCODiscount
                       ], [
                               name   : "3 Year TCO",
                               "isSum": true
                       ], [
                               name: "Term Adjustment",
                               y   : perHourThreeYearDiscount
                       ], [
                               name   : "Per Hour",
                               "isSum": true
                       ], [
                               name: "Metric Adjustment",
                               y   : perHHPerHDiscount
                       ], [
                               name   : "Per Hour Host",
                               "isSum": true
                       ], [
                               name: "Standard Uplifts",
                               y   : CPUPerHHDiscount
                       ], [
                               name   : "Subscription OnDemand Per Hour Per Host",
                               "isSum": true
                       ], [
                               name: "AWS HW",
                               y   : aws
                       ], [
                               name: "Hardware Support",
                               y   : hardware
                       ], [
                               name   : "VMC on AWS",
                               "isSum": true
                       ], [
                               name: "SW HW Uplift",
                               y   : uplift
                       ], [
                               name   : "Uplifted VMC on AWS",
                               "isSum": true
                       ], [
                               name: "3 Year Host Adjustment",
                               y   : diff3Year
                       ], [
                               name   : "Calculated VMC on AWS 3 Year",
                               "isSum": true
                       ], [
                               name: "Calculated Actual Adjustment",
                               y   : actualCalculatedDif
                       ], [
                               name   : "Actual VMC on AWS",
                               "isSum": true
                       ]
                ]
        ]
]
if (itemType != null) {
    if (itemType == "On Demand") {
        return api.buildFlexChart("hc_wf_base", chartDef1)
    }
    if (itemType == "1 Year") {
        return api.buildFlexChart("hc_wf_base", chartDef2)
    }
    if (itemType == "3 Year") {
        return api.buildFlexChart("hc_wf_base", chartDef3)
    }
}
return api.buildFlexChart("hc_wf_base", chartDef)