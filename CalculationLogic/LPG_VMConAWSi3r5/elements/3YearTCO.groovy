if (api.syntaxCheck) {
    return null
}
def sns = api.getElement("SnS") ?: 0
sns = sns * 3
def dPrice = api.getElement("ComponentsDiscountedPrice") ?: 0
return sns + dPrice