if (api.isSyntaxCheck()) return

/*def AWSOnDemand = api.getElement("UpliftedVMCOnAWSTerm")
def AWS1Year = api.getElement("VMCOnAWS1YearTerm")
def AWS3Year = api.getElement("VMCOnAWS3YearTerm")*/
def resultPrice = api.getElement("ResultPrice")
def DIRRESDEFAULT = api.getElement("DIRRESDEFAULT")
def itemType = api.getElement("ItemType")

if (itemType != null && resultPrice != null && DIRRESDEFAULT != null) {
    def result = resultPrice * (1 - DIRRESDEFAULT)
    if (itemType == "On Demand") {
        return Library.round(result, 6)
    } else {
        return Library.round(result, 2)
    }
    //return resultPrice * (1-DIRRESDEFAULT)
}
/*if(itemType=="On Demand"){
  if(AWSOnDemand!=null && DIRRESDEFAULT!=null){
    return AWSOnDemand * (1-DIRRESDEFAULT)
  }
}
if(itemType=="1 Year"){
  if(AWS1Year!=null && DIRRESDEFAULT!=null){
    return AWS1Year * (1-DIRRESDEFAULT)
  }
}
if(itemType=="3 Year"){
  if(AWS3Year!=null && DIRRESDEFAULT!=null){
    return AWS3Year * (1-DIRRESDEFAULT)
  }
}*/