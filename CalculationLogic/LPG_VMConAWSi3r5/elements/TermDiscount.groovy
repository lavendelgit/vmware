//VLookup("BUSWAdj","Discount Percent",ItemType,HostType,"US West Oregon")
if (api.isSyntaxCheck()) return
def itemType = api.getElement("ItemType")
def hostType = api.getElement("HostType")
if (itemType != null && hostType != null) {
    def tab = api.findLookupTable("TermDiscounts")
    def tabList
    if (itemType != null && hostType != null) {
        tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
                Filter.equal("key1", itemType),
                Filter.equal("key2", hostType))
        if (tabList != null) {
            return tabList[0]?.attribute1
        }
    }
}