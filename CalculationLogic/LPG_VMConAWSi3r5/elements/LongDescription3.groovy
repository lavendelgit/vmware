def extn = api.getElement("VMConAWSExtn")
if (extn != null && extn["attribute9"][0] != null) {
    api.logInfo("long description", extn["attribute9"][0])
    if (extn["attribute9"][0].length() > 140 && extn["attribute9"][0].length() < 210)
        return extn["attribute9"][0].substring(140)
    else return ""
}