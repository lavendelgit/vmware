def productDescription = api.product("label")

if (productDescription != null) {
    api.logInfo("productDescription", productDescription)
    if (productDescription.length() > 140 && productDescription.length() < 210)
        return productDescription.substring(140, 210)
    else return ""
}