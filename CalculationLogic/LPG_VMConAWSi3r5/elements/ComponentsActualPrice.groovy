if (api.syntaxCheck) {
    return null
}
def dataCenter = api.getElement("DataCenter")
def tab = api.findLookupTable("PerpetualSKUs")
def tabList
def actualPrice = 0
if (dataCenter != null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("key2", dataCenter))
}
/*if(tabList==null){
  tabList = api.find("MLTV2",Filter.equal("lookupTable.id", tab?.id))  
}*/
if (tabList) {
    tabList.each {
        def price
        price = it.attribute2 ?: 0
        api.trace("test", null, price)
        actualPrice += price
    }

}
return actualPrice