def extn = api.getElement("VMConAWSExtn")
if (extn != null && extn["attribute8"][0] != null) {
    api.logInfo("mdified description", extn["attribute8"][0])
    def modifiedDescriptionLength = extn["attribute8"][0].length()
    if (modifiedDescriptionLength > 140) modifiedDescriptionLength = 140
    if (extn["attribute8"][0].length() > 70)
        return extn["attribute8"][0].substring(70, modifiedDescriptionLength)
    else return ""
}