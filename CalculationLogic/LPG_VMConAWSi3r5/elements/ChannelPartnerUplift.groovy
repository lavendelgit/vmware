if (api.syntaxCheck) {
    return null
}
def onDemand = api.getElement("OnDemandUplift")
def channelPercent = api.getElement("ChannelPartnerUpliftPercent")
if (channelPercent != null && onDemand != null) {
    channelPercent += 1
    return channelPercent * onDemand
}