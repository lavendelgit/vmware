def extn = api.getElement("VMConAWSExtn")
if (extn != null && extn["attribute8"][0] != null) {
    api.logInfo("mdified description", extn["attribute8"][0])
    if (extn["attribute8"][0].length() > 140 && extn["attribute8"][0].length() < 210)
        return extn["attribute8"][0].substring(140)
    else return ""
}