if (api.syntaxCheck) {
    return null
}

def originalListPrice = api.productExtension("OriginalListPrice")[0]
if (originalListPrice != null) {
    Map currencyToOrginalPrice = [
            "USD": originalListPrice["attribute1"],
            "EUR": originalListPrice["attribute2"],
            "GBP": originalListPrice["attribute3"],
            "AUD": originalListPrice["attribute4"],
            "CNY": originalListPrice["attribute5"],
            "JPY": originalListPrice["attribute6"]
    ]

    def resultMatrix = api.newMatrix("Currency", "Calculated Price", "Original Price", "Difference", "Percentage Difference")
    currencyToOrginalPrice.each {
        resultMatrix.addRow(calculateCurrencyRow(it.key, it.value))
    }

    return resultMatrix
}

Map calculateCurrencyRow(String currency, BigDecimal originalPrice) {
    BigDecimal calculatedPrice = currency == "USD" ? api.getElement("ResultPrice") : api.getElement("ResultPrice" + currency)
    if (calculatedPrice == null) calculatedPrice = 0
    if (originalPrice == null) originalPrice = 0
    BigDecimal difference = calculatedPrice - originalPrice
    def row = [:]
    row.put("Currency", currency)
    row.put("Calculated Price", calculatedPrice)
    row.put("Original Price", originalPrice)
    row.put("Difference", difference)
    if (calculatedPrice != 0)
        row.put("Percentage Difference", difference / calculatedPrice * 100 + "%")

    return row
}