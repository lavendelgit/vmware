def productDescription = api.product("label")

if (productDescription != null) {
    api.logInfo("productDescription", productDescription)
    def prodDescriptionLength = productDescription.length()
    if (productDescription.length() > 140) prodDescriptionLength = 140
    if (productDescription.length() > 70)
        return productDescription.substring(70, prodDescriptionLength)
    else return ""
}