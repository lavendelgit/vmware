if (api.syntaxCheck) {
    return null
}
def total = api.getElement("VMCOnAWS") ?: 0
def sw = api.getElement("SW")
if (total && sw) {
    def hw = total - sw
    /*if(api.global.VMConAWS3Year!=null)
    api.global.hw = api.global.VMConAWS3Year - sw
    api.trace("hw3Year",null,api.global.hw)*/
    return hw
}