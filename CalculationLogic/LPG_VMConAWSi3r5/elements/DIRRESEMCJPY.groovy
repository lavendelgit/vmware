/*def utilLib = libs.vmwareUtil.util
def dirRESEMCDollar = api.getElement("DIRRESEMCDollar")
def term = api.getElement("HostType")
def exchangeRate = utilLib.getExchangeRate("JPY", term)
if (exchangeRate != null)
	return  dirRESEMCDollar * exchangeRate
else
  	return null
*/
def resultPrice = api.getElement("ResultPriceJPY")
def DIRRESEMC = api.getElement("DIRRESEMC")
def itemType = api.getElement("ItemType")

if (itemType != null && resultPrice != null && DIRRESEMC != null) {
    def result = resultPrice * (1 - DIRRESEMC)
    if (itemType == "On Demand") {
        return Library.round(result, 6)
    } else {
        return Library.round(result, 0)
    }
    //return resultPrice * (1-DIRRESEMC)
}