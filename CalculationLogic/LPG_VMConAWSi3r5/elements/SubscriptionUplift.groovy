if (api.syntaxCheck) {
    return null
}
def perHourHost = api.getElement("PerHourHost")
def subscriptionPercent = api.getElement("SubscriptionUpliftPercent") ?: 0
if (subscriptionPercent != null && perHourHost != null) {
    subscriptionPercent += 1
    return perHourHost * subscriptionPercent
}