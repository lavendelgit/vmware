def extn = api.getElement("VMConAWSExtn")
if (extn != null && extn["attribute9"][0] != null) {
    api.logInfo("long description", extn["attribute9"][0])
    if (extn["attribute9"][0].length() > 70)
    //suffix text is not included in excel download
        return api.attributedResult(extn["attribute9"][0].substring(0, 70)).withSuffix(extn["attribute9"][0].substring(70))
    else return extn["attribute9"][0]
}