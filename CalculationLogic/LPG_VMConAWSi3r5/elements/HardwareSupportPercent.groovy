if (api.syntaxCheck) {
    return null
}
def tab = api.findLookupTable("Adjustments")
def dataCenter = api.getElement("DataCenter")
def tabList
if (dataCenter != null) {
    tabList = api.find("MLTV2", Filter.equal("lookupTable.id", tab?.id),
            Filter.equal("key1", "Hardware Support"),
            Filter.equal("key2", dataCenter))
}
if (tabList != null) {
    def hwSupport = tabList[0]?.attribute1 ?: 0
    return hwSupport
}