def val1 = api.getElement("SWBasePriceIncludingUplift")
def val2 = api.getElement("TermDiscountInternal")
if (val1 != null && val2 != null) {
    val2 += 1
    return val1 * val2
}