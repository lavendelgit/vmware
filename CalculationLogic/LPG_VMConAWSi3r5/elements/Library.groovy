api.retainGlobal = true

def round(Number num, int decimalPlaces) {
    if (num == null) {
        return null
    }

    def factor = (int) Math.pow(10, decimalPlaces)

    return Math.round(num * factor) / factor
}