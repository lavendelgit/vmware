if (api.syntaxCheck) {
    return null
}
def itemType = api.getElement("ItemType")
if (itemType == "On Demand" || itemType == "1 Year") {
    return "N/A"
}
def VMConAWS1Year = api.getElement("VMCOnAWS1Year")
if (VMConAWS1Year != null) {
    return VMConAWS1Year * 3
}