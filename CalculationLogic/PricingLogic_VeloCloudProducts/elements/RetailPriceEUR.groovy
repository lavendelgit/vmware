def listPrice = api.getElement("FinalRetailPrice")
def rate = api.getElement("ConversionFactorCNY")
def LC = api.getElement("LCFactor")

if (!LC || !rate || !listPrice) return ""

def LCprice = listPrice * rate
def threshHold = 500
def roundToThresholdMet = 10
def roundToThresholdNotMet = 1
def substract = 1

if (api.getElement("ProductType") == "Perpetual- SnS") {
    return LCprice.setScale(2, BigDecimal.ROUND_HALF_UP)
} else {
    if (LC == "LC-77") {
        return libs.vmwareUtil.RoundingLogicDesktopV2.applyRoundingLogic(LCprice, threshHold, substract, roundToThresholdMet, roundToThresholdNotMet)?.toBigDecimal()
    } else {
        return libs.vmwareUtil.RoundingLogicV2.applyRoundingLogic(LCprice)?.toBigDecimal()
    }
}  