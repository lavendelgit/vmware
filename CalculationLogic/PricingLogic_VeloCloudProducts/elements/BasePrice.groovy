def basePrice
def productType = api.getElement("ProductType")

if (productType == "Software") {

    def swDetails = api.getElement("FetchSWDetails")
    def supportLevel = api.getElement("SupportLevel")
    def priceDetails = api.getElement("LoadSWBasePrices")
    def priceDetailsL14L34 = api.getElement("LoadSWBasePricesL14L34")
    def editionBasePrice = api.getElement("EditionBasePrice")
    def editionFactor = api.getElement("EditionFactor")

    api.trace("priceDetails", null, priceDetails.toString())

    if (supportLevel == "L14") {
        basePrice = priceDetailsL14L34?.attribute1 + editionBasePrice

    }
    if (supportLevel == "L34") {
        if (priceDetailsL14L34?.attribute1 != null && priceDetailsL14L34?.attribute2 != null && editionBasePrice != null)
            basePrice = (priceDetailsL14L34?.attribute1 + editionBasePrice) - priceDetailsL14L34?.attribute2
    }

    if (swDetails?.attribute4.contains("SG")) {
        if (basePrice && priceDetails?.attribute4 && editionFactor) {
            basePrice = basePrice + (priceDetails?.attribute4 * editionFactor)
            api.trace("basePrice", basePrice)
        }
    }
    if (swDetails?.attribute4.contains("HG")) {
        if (basePrice && priceDetails?.attribute3 && editionFactor) {
            basePrice = basePrice + (priceDetails?.attribute3 * editionFactor)
            api.trace("basePrice", basePrice)
        }
    }
} else if (productType == "Extended Replacement Service") {
    return api.global.basePriceListMap[api.getElement("HWModel")]
} else if (productType == "Hardware Capex") {
    return api.global.basePriceListMap[api.getElement("HWModel")]
} else if (productType == "Rental") {
    return api.global.basePriceListMap[api.getElement("HWModel")]
}
return basePrice