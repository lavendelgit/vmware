def productType = api.getElement("ProductType")

if (productType == "Software") {
    def swDetails = api.getElement("FetchSWDetails")
    return swDetails.attribute7
} else if (productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental") {

    def hwDetails = api.getElement("FetchHWDetails")
    def extensionString = hwDetails?.attribute3

    if (extensionString?.contains("12P"))
        return "1"
    else if (extensionString?.contains("24P"))
        return "2"
    else if (extensionString?.contains("36P"))
        return "3"
    else if (extensionString?.contains("48P"))
        return "4"
    else if (extensionString?.contains("60P"))
        return "5"
    else if (extensionString?.contains("12A"))
        return "1"
    else if (extensionString?.contains("24A"))
        return "2"
    else if (extensionString?.contains("36A"))
        return "3"
    else if (extensionString?.contains("48A"))
        return "4"
    else if (extensionString?.contains("60A"))
        return "5"
    else if (extensionString?.contains("12M"))
        return "1"
    else if (extensionString?.contains("24M"))
        return "2"
    else if (extensionString?.contains("36M"))
        return "3"
    else if (extensionString?.contains("48M"))
        return "4"
    else if (extensionString?.contains("60M"))
        return "5"

}
