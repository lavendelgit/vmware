def productType = api.getElement("ProductType")

if (productType == "Software" ||
        productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental"
)
    return 1
else
    return 0
	