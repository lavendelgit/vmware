import java.math.RoundingMode

def suggestedRetailPrice

def years = api.getElement("Years")?.toBigDecimal()
def basePrice = api.getElement("BasePrice")?.toBigDecimal()
def billingTermFactor = api.getElement("BillingTermFactor")?.toBigDecimal()
def serviceLevelFactor = api.getElement("ServiceLevelFactor")?.toBigDecimal()
def yearsFactor = api.getElement("YearsFactor")?.toBigDecimal()
def productType = api.getElement("ProductType")
if (productType == "Software") {

    if (basePrice && years && billingTermFactor)
        suggestedRetailPrice = basePrice * years * billingTermFactor

    if (suggestedRetailPrice && serviceLevelFactor && yearsFactor != 0 && yearsFactor != null)
        suggestedRetailPrice = suggestedRetailPrice * serviceLevelFactor / yearsFactor
}

return suggestedRetailPrice?.toBigDecimal()
