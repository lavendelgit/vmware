def price
def productType = api.getElement("ProductType")
if (productType == "Software") {
    def swDetails = api.getElement("FetchSWDetails")
    price = swDetails?.attribute10
} else if (productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental") {
    def hwDetails = api.getElement("FetchHWDetails")
    price = hwDetails?.attribute9
}

return price