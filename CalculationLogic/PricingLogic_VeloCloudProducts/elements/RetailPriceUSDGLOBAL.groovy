def listPrice = api.getElement("FinalRetailPrice")
def rate = api.getElement("ConversionFactorUSDGLOBAL")
def LC = api.getElement("LCFactor")

if (!LC || !rate || !listPrice) return ""

def LCprice = listPrice * rate
return LCprice.setScale(2, BigDecimal.ROUND_HALF_UP)