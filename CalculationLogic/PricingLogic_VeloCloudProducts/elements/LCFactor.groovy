/*
if (api.getElement("ProductType") == "Software") return "LC-91"
if (api.getElement("ProductType").startsWith("Hardware")) return "LC-92"
return api.product("attribute9")* old code */

def lcf
def sku = api.product("PartNumber")
def prods = api.find("P", Filter.equal("sku", sku))
def lcFactor = prods.attribute9[0]
if (lcFactor) lcf = lcFactor

if (out.ProductType == "Software") return lcf
if (out.ProductType.startsWith("Hardware")) return lcf
return lcf
