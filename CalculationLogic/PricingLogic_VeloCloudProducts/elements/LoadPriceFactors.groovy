if (api.global.basePriceListMap == null)
    api.global.basePriceListMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("HardwareBasePrice")

//if(api.global.replacementFactorMap == null)
//api.global.replacementFactorMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("ReplacementFactor")

//if(api.global.billingTermsFactorMap == null)
//api.global.billingTermsFactorMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("BillingTermsFactor")

if (api.global.longevityFactorMap == null)
    api.global.longevityFactorMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("LongevityFactor")

if (api.global.serviceLevelFactorMap == null)
    api.global.serviceLevelFactorMap = libs.vmwareUtil.LoadPPData.getLTVAsMap("ServiceLevelFactor")


api.trace("api.global.basePriceListMap", null, api.global.basePriceListMap.toString())
//api.trace("api.global.replacementFactorMap", null, api.global.replacementFactorMap.toString())
//api.trace("api.global.billingTermsFactorMap", null, api.global.billingTermsFactorMap.toString())
api.trace("api.global.longevityFactorMap", null, api.global.longevityFactorMap.toString())
api.trace("api.global.serviceLevelFactorMap", null, api.global.serviceLevelFactorMap.toString())