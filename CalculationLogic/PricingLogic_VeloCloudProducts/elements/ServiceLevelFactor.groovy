def productType = api.getElement("ProductType")

if (productType == "Software")
    return api.global.serviceLevelFactorMap[api.getElement("ServiceLevel")].toBigDecimal()
else if (productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental") {

}