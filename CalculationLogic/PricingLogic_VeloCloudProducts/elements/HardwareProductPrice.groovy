import java.math.RoundingMode

def suggestedRetailPrice
def productType = api.getElement("ProductType")
def years = api.getElement("Years")?.toBigDecimal()
def basePrice = api.getElement("BasePrice")?.toBigDecimal()
def tco = api.getElement("RentalTCO")?.toBigDecimal()
def longevityFactor = api.getElement("LongevityFactor")?.toBigDecimal()
def replacementFactor = api.getElement("ReplacementFactor")?.toBigDecimal()
def billingTermFactor = api.getElement("BillingTermFactor")?.toBigDecimal()
def yearsFactor = api.getElement("YearsFactor")?.toBigDecimal()

if (productType == "Extended Replacement Service") {

    if (basePrice && replacementFactor)
        suggestedRetailPrice = (basePrice * replacementFactor)

    if (suggestedRetailPrice && years && longevityFactor)
        suggestedRetailPrice = suggestedRetailPrice * years * longevityFactor

} else if (productType == "Hardware Capex") {

    if (basePrice && replacementFactor)
        suggestedRetailPrice = basePrice * replacementFactor

} else if (productType == "Rental") {

    if (basePrice && tco)
        suggestedRetailPrice = basePrice * tco

    if (suggestedRetailPrice && years && billingTermFactor)
        suggestedRetailPrice = suggestedRetailPrice * years * billingTermFactor

    if (suggestedRetailPrice && replacementFactor && yearsFactor && yearsFactor != 0)
        suggestedRetailPrice = suggestedRetailPrice * replacementFactor / yearsFactor
}

return suggestedRetailPrice?.toBigDecimal()