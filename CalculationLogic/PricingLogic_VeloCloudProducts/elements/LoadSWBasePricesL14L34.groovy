def bandwidth = api.getElement("Bandwidth")
def edition = api.getElement("Edition")

if (!bandwidth && edition) return
api.trace("Bandwidth & Edition", edition, bandwidth)

def ppTable = api.findLookupTable("L14L34Prices")
def recList = api.find("MLTV2", 0, 1, null,
        Filter.equal("key1", bandwidth),
        Filter.equal("key2", edition),
        Filter.equal("lookupTable.id", ppTable.id))

api.trace("Record List", null, recList[0].toString())

if (recList)
    return recList[0]




