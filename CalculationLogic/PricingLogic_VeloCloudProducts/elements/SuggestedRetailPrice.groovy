import java.math.RoundingMode

def srp
def productType = api.getElement("ProductType")
def swProductPrice = api.getElement("SoftwareProductPrice")

if (productType == "Software" && swProductPrice != null) {
    srp = swProductPrice
} else if (productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental") {
    srp = api.getElement("HardwareProductPrice")
}

return srp?.toBigDecimal()?.setScale(0, RoundingMode.UP)