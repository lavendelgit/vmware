def paymentType
def productType = api.getElement("ProductType")

if (productType == "Software") {
    def swDetails = api.getElement("FetchSWDetails")
    paymentType = swDetails?.attribute8
} else if (productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental") {
    def hwDetails = api.getElement("FetchHWDetails")
    paymentType = hwDetails?.attribute8
}

api.trace("paymentType", null, paymentType)
return paymentType