def productType = api.getElement("ProductType")

if (productType == "Software")
    return
else if (productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental") {
    return api.global.longevityFactorMap[api.getElement("Years")]
}