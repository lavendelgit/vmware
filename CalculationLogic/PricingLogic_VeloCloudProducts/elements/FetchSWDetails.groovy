def sku = api.product("sku")
def maxResults = api.getMaxFindResultsLimit()
def pxList = api.find("PX", 0, maxResults, null,
        Filter.equal("sku", sku),
        Filter.equal("name", "VeloCloudSoftwareProducts")
)
if (pxList) {
    api.trace("Details", null, pxList[0])
    return pxList[0]
}