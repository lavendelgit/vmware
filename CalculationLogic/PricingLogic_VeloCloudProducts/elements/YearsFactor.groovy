if (api.isSyntaxCheck()) {
    api.abortCalculation()
}

def paymentType = api.getElement("PaymentType")
def years = api.getElement("Years")
if (years) years = years.toInteger()

api.trace(paymentType.toString().toLowerCase())

if (years && paymentType) {
    if (paymentType.toString().toLowerCase() == "prepaid") return 1
    if (paymentType.toString().toLowerCase() == "annual") return years
    if (paymentType.toString().toLowerCase() == "monthly") return years * 12
}