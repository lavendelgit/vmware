def productType = api.getElement("ProductType")

if (productType == "Software")
    return
else if (productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental") {

    def hwDetails = api.getElement("FetchHWDetails")
    return hwDetails?.attribute7

}