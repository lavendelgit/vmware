def edition = api.getElement("Edition")

if (!edition) return
api.trace("Edition", null, edition)

def ppTable = api.findLookupTable("EditionOptionsNew")
def recList = api.find("MLTV", 0, 1, null, Filter.equal("name", edition), Filter.equal("lookupTable.id", ppTable.id))

api.trace("Record List", null, recList[0].toString())

if (recList)
    return recList[0].attribute1