def srp = api.getElement("SuggestedRetailPrice")

return srp?.toFloat()?.round()?.toBigDecimal()