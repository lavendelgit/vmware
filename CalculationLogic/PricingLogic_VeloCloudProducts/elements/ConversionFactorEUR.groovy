def LC = api.getElement("LCFactor")
if (LC == null) return 0
def currencyFilter = Filter.equal("name", LC)
def fxRate = api.findLookupTableValues("LocalCurrencyExchangeRates", currencyFilter).attribute4[0]

//api.trace(fxRate)

if (fxRate != null) {
    return new BigDecimal(fxRate)
} else {
    return null
}