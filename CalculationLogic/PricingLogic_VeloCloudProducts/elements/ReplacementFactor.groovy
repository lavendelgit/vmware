def maxResults = api.getMaxFindResultsLimit()
def productType = api.getElement("ProductType")
def replacementType = api.getElement("ReplacementType")

def ppTable = api.findLookupTable("ReplacementFactor")
while (recList = api.find("MLTV2", 0, maxResults, null,
        Filter.equal("lookupTable.id", ppTable.id),
        Filter.equal("key1", replacementType),
        Filter.equal("key2", productType)
)
) {
    if (recList)
        return recList[0]?.attribute1
}