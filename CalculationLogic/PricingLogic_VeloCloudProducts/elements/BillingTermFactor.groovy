def paymentType
def years = api.getElement("Years")
def productType = api.getElement("ProductType")

if (productType == "Software") {
    def swDetails = api.getElement("FetchSWDetails")
    paymentType = swDetails.attribute8
} else if (productType == "Extended Replacement Service" ||
        productType == "Hardware Capex" ||
        productType == "Rental") {
    def hwDetails = api.getElement("FetchHWDetails")
    paymentType = hwDetails?.attribute8
}


def ppTable = api.findLookupTable("BillingTermsFactor")
while (recList = api.find("MLTV2", 0, 1, null,
        Filter.equal("lookupTable.id", ppTable.id),
        Filter.equal("key1", years),
        Filter.equal("key2", paymentType)
)
) {
    if (recList)
        return recList[0]?.attribute1
}