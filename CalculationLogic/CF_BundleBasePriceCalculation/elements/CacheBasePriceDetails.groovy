List filter = [
        Filter.equal(Constants.NAME, Constants.BASE_PRICE_TABLE_NAME),
        Filter.equal(Constants.SKU_TYPE_COLUMN_NAME, Constants.STANDALONE_SKU_TYPE), //Standalone / null
        Filter.in(Constants.BASEPRICE_SKU_COLUMN_NAME, api.local.baseSKUCache?.values())
]
recordStream = api.stream("PX50", null, [Constants.BASEPRICE_SKU_COLUMN_NAME, Constants.BASEPRICE_COLUMN_NAME, Constants.QUANTITY_BASEPRICE_COLUMN_NAME, Constants.VALID_FROM_COLUMN_NAME], *filter)
api.local.basePriceDetails = recordStream?.collect { componentSku ->
    [
            sku      : componentSku.attribute1,
            quantity : componentSku.attribute12 as BigDecimal,
            basePrice: componentSku.attribute6 as BigDecimal,
            ValidFrom: componentSku.attribute29
    ]
}
recordStream?.close()
return