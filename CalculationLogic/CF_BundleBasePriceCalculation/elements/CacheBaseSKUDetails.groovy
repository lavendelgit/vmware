List filter = [
        Filter.equal(Constants.NAME, Constants.BASE_PRICE_TABLE_NAME),
        Filter.equal(Constants.SKU_TYPE_COLUMN_NAME, Constants.STANDALONE_SKU_TYPE),
        Filter.in(Constants.BASEPRICE_SKU_COLUMN_NAME, api.local.baseSKUCache?.values())
]
recordStream = api.stream("PX50", null, *filter)
api.local.baseSkuDetails = recordStream?.collect { it }
recordStream?.close()
return