List filter = [
        Filter.equal(Constants.NAME, Constants.BOM_PX_TABLE),
        Filter.in(Constants.SKU, api.local.bundleSKUDetails?.keySet() as List)]

recordStream = api.stream("PX10", null, [Constants.SKU, Constants.QUANTITY_COLUMN_NAME, Constants.RAW_MATERIAL_COLUMN_NAME, Constants.SUB_COMPONENT_SKU_COLUMN_NAME, Constants.DISCOUNT_COLUMN_NAME], *filter)
api.local.bomEntries = recordStream?.collect {
    [
            sku            : it.sku,
            quantity       : it.attribute3 as BigDecimal,
            rawMaterial    : it.attribute7,
            subComponentSku: it.attribute1,
            discount       : it.attribute4 as BigDecimal
    ]
}
recordStream?.close()
return