List filter = [
        Filter.equal(Constants.NAME, Constants.PRODUCT_TABLE_NAME),
        Filter.in(Constants.SKU, api.local.bomEntries?.collect { it.subComponentSku })
]
recordStream = api.stream("PX50", null, [Constants.SKU, Constants.BASE_PRICE_SKU_COLUMN_NAME], *filter)
api.local.baseSKUCache = recordStream?.collectEntries { [(it.sku): it.attribute14] } //@todo need to be replaced
recordStream?.close()
return