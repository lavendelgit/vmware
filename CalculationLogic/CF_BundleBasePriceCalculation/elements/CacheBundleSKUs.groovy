List filter = [
        Filter.equal(Constants.NAME, Constants.BASE_PRICE_TABLE_NAME),
        Filter.equal(Constants.SKU_TYPE_COLUMN_NAME, Constants.BUNDLE_SKU_TYPE),
        Filter.equal(Constants.INCLUSION_FLAG_COLUMN_NAME, libs.stdProductLib.ConstConfiguration.BUNDLE_CALCULATION_FLAG)
]
recordStream = api.stream("PX50", null, *filter)
api.local.bundleSKUDetails = recordStream?.collectEntries {
    [
            (it.attribute1): it
    ]
}

recordStream?.close()
return
