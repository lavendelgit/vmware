api.local.roundingRulesCache = [:]
standardAdjMetaData = out.StandardAdjustmentMetaData?.collectEntries { [(it.value): it.key] }
api.local.roundingRulesCache = libs.vmwareUtil.PricingHelper.cacheAdjustments(api.local.stdAdjustmentsCache, standardAdjMetaData, Constants.PRECISION_COLUMN_NAME, Constants.ROUNDING_TYPE_COLUMN_NAME, true)
Map adjustmentCache = [:]
if (api.local.roundingRulesCache?.find { it.value.Term != null && it.value[Constants.TERM_UOM] != null }) {
    api.local.roundingRulesCache.each { key, value -> adjustmentCache[key] = value.clone() }
    populatedTermCache = libs.vmwareUtil.PricingHelper.populateTermAltUoMAdjustments(adjustmentCache, out.TermUoMConversionCache)
    if (populatedTermCache?.size() > 0 && api.local.roundingRulesCache?.size() > 0) {
        api.local.roundingRulesCache = api.local.roundingRulesCache + populatedTermCache
    }
}
return