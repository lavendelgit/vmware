return api.find("PXAM", Filter.equal(Constants.NAME, Constants.PRODUCT_TABLE_NAME))?.collectEntries {
    [(it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()): it.fieldName]
}
