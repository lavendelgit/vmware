api.local.bundleSKUBasePriceCache = [:]
BigDecimal basePrice = 0.0
BigDecimal discountedPrice = 0.0
List components = []
Map roundingRule = [:]
for (bundleSKU in api.local.bundleSKUDetails) {
    basePrice = 0.0
    discountedPrice = 0.0
    components = api.local.bomEntries.findAll { it.sku == bundleSKU.key } //get subcomponents for this Bundle SKU
    for (component in components) {
        baseSKU = api.local.baseSKUCache[component.subComponentSku] //get subComponentSku Base Price * qty
        baseSKUDetail = api.local.basePriceDetails?.find { it.sku == baseSKU } //component.subComponentSku }
        if (baseSKUDetail) {
            baseSKUPricePerQty = baseSKUDetail?.basePrice as BigDecimal
            if (baseSKUDetail?.quantity != 0) {
                baseSKUPricePerQty = (baseSKUDetail?.basePrice / baseSKUDetail?.quantity) as BigDecimal
            }
            basePrice += baseSKUPricePerQty * component.quantity as BigDecimal
            discountedPrice += component.discount ? baseSKUPricePerQty * component.discount * component.quantity : baseSKUPricePerQty * component.quantity
        }
    }

    roundingRule = getRoundingRule(bundleSKU?.value)
    api.local.bundleSKUBasePriceCache[bundleSKU.key] = [
            basePrice         : basePrice,
            validFrom         : bundleSKU.value.attribute29 ? bundleSKU.value.attribute29 : new Date().format(Constants.DATE_YYYYMMDD_FORMAT),
            validTo           : bundleSKU.value.attribute30 ? bundleSKU.value.attribute30 : "9999-12-31",
            discountedPrice   : libs.vmwareUtil.PricingHelper.roundedPrice(discountedPrice, roundingRule?.RoundingType, roundingRule?.Precision),
            bundleDiscountPct : 1 - (discountedPrice / basePrice),
            baseDescription   : bundleSKU.value.attribute22,
            skuType           : bundleSKU.value.attribute2,
            product           : bundleSKU.value.attribute3,
            offer             : bundleSKU.value.attribute4,
            base              : bundleSKU.value.attribute5,
            os                : bundleSKU.value.attribute7,
            currency          : bundleSKU.value.attribute8,
            priceListRegion   : bundleSKU.value.attribute9,
            dataCentre        : bundleSKU.value.attribute10,
            metric            : bundleSKU.value.attribute11,
            quantity          : bundleSKU.value.attribute12,
            segment           : bundleSKU.value.attribute13,
            supportTier       : bundleSKU.value.attribute14,
            supportType       : bundleSKU.value.attribute15,
            term              : bundleSKU.value.attribute16,
            termUOM           : bundleSKU.value.attribute21,
            paymentType       : bundleSKU.value.attribute17,
            volumeTier        : bundleSKU.value.attribute18,
            purchasingProgram : bundleSKU.value.attribute19,
            promotion         : bundleSKU.value.attribute20,
            version           : bundleSKU.value.attribute28,
            hosting           : bundleSKU.value.attribute25,
            programOption     : bundleSKU.value.attribute26,
            retentionPeriod   : bundleSKU.value.attribute31,
            retentionPeriodUOM: bundleSKU.value.attribute32,
            psTerm            : bundleSKU.value.attribute34,
            psOption          : bundleSKU.value.attribute35

    ]
}
return

Map getRoundingRule(Map bundleSKU) {
    def keyLists = libs.vmwareUtil.PricingHelper.getAdjustmentsFromCache(api.local.roundingRulesCache, bundleSKU)?.values()?.getAt(0)
    Map roundingRule = [:]
    def roundingKeysList = []
    for (keyList in keyLists) {
        roundingKeysList = keyList?.values()?.drop(3)
        roundingRule = libs.vmwareUtil.PricingHelper.getPrecision(api.local.roundingRulesCache, bundleSKU?.attribute1, bundleSKU?.attribute3, bundleSKU?.attribute4, bundleSKU?.attribute5, roundingKeysList)
        if (roundingRule?.Precision || roundingRule?.Precision == 0) {
            break
        }
    }

    roundingRule?.Precision = roundingRule?.Precision == 0 || roundingRule?.Precision ? roundingRule?.Precision : 2
    roundingRule?.RoundingType = roundingRule?.RoundingType ?: Constants.ROUND
    return roundingRule
}
