Map metaData = api.find("PXAM", Filter.equal(Constants.NAME, Constants.STANDARD_ADJUSTMENT_TABLE))?.collectEntries {
    [(it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()): it.fieldName]
}
metaData[Constants.SKU] = Constants.SKU
return metaData
