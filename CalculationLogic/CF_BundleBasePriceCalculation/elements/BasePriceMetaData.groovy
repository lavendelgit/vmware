return api.find("PXAM", Filter.equal(Constants.NAME, Constants.BASE_PRICE_TABLE_NAME))?.collectEntries {
    [(it.labelTranslations?.replaceAll("[\n\r{\\\"\\\":\\\"}]", "")?.trim()): it.fieldName]
}