List filter = [
        Filter.equal(Constants.NAME, Constants.STANDARD_ADJUSTMENT_TABLE),
        Filter.or(Filter.equal(Constants.PRODUCT_COLUMN_NAME, "*"), Filter.in(Constants.PRODUCT_COLUMN_NAME, api.local.bundleSKUDetails?.attribute3?.unique())),
        Filter.or(Filter.equal(Constants.OFFER_COLUMN_NAME, "*"), Filter.in(Constants.OFFER_COLUMN_NAME, api.local.bundleSKUDetails?.attribute4?.unique())),
        Filter.or(Filter.equal(Constants.BASE_COLUMN_NAME, "*"), Filter.in(Constants.BASE_COLUMN_NAME, api.local.bundleSKUDetails?.attribute5?.unique())),
        Filter.or(Filter.equal(Constants.SKU, "*"), Filter.in(Constants.SKU, api.local.bundleSKUDetails?.keySet())),
        Filter.isNotNull(Constants.ROUNDING_TYPE_COLUMN_NAME)
]
stdAdjustmentStream = api.stream("PX50", Constants.VALIDFROM_COLUMN_NAME, *filter)
api.local.stdAdjustmentsCache = stdAdjustmentStream?.collect { row -> row }
stdAdjustmentStream?.close()
return
