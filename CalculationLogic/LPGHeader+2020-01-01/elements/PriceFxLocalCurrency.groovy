Map priceFxCurrencyPercent = [
        (Constants.NAMUSD)   : Lib.calculatePercent((api.local.inputLists?.(Constants.NAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_NAMUSD)?.sum()),
        (Constants.APACUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.APACUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_APACUSD)?.sum()),
        (Constants.CHINAUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.CHINAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_CHINAUSD)?.sum()),
        (Constants.LATAMUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.LATAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_LATAMUSD)?.sum()),
        (Constants.EMEAUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.EMEAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_EMEAUSD)?.sum()),
        (Constants.EMEAUSD2) : Lib.calculatePercent((api.local.inputLists?.(Constants.EMEAUSD2))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_EMEAUSD2)?.sum()),
        (Constants.GLOBALUSD): Lib.calculatePercent((api.local.inputLists?.(Constants.GLOBALUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_GLOBALUSD)?.sum()),
        (Constants.EMEAEUR)  : Lib.calculatePercent((api.local.inputLists?.(Constants.EMEAEUR))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_EMEAEUR)?.sum()),
        (Constants.EMEAGBP)  : Lib.calculatePercent((api.local.inputLists?.(Constants.EMEAGBP))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_EMEAGBP)?.sum()),
        (Constants.APACCNY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.APACCNY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_APACCNY)?.sum()),
        (Constants.APACAUD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.APACAUD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_APACAUD)?.sum()),
        (Constants.APACJPY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.APACJPY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_APACJPY)?.sum())
]
return priceFxCurrencyPercent
