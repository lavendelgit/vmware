Map chartDef = [
        chart        : [type: 'column'],
        title        : [
                align: 'left',
                text : 'List Price % VS Product-Offer'
        ],
        accessibility: [
                announceNewData: [
                        enabled: true
                ]
        ],
        xAxis        : [
                type: 'category'
        ],
        yAxis        : [
                title: [
                        text: Constants.LIST_PRICES
                ],
        ],
        legend       : [
                enabled: false
        ],
        credits      : [
                enabled: false
        ],
        plotOptions  : [
                series: [
                        borderWidth: 0,
                        dataLabels : [
                                enabled: true,
                                format : '{point.y:.2f}%'
                        ]
                ]
        ],

        tooltip      : [
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat : '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> of total<br/>'
        ],

        series       : [
                [
                        name        : Constants.LIST_PRICES,
                        colorByPoint: true,
                        data        : [
                                [
                                        name     : Constants.PRICEFX_LIST_PRICE,
                                        y        : api.local.pricefxLocalCurrencyPercentage,
                                        drilldown: Constants.PRICEFX_LIST_PRICE
                                ],
                                [
                                        name     : Constants.SAP_LIST_PRICE,
                                        y        : api.local.sapLocalCurrencyPercentage,
                                        drilldown: Constants.SAP_LIST_PRICE
                                ],
                                [
                                        name     : Constants.DISTI_LIST_PRICE,
                                        y        : api.local.distiListPricePercentage,
                                        drilldown: Constants.DISTI_LIST_PRICE
                                ],
                                [
                                        name     : Constants.SAP_DISTI_LIST_PRICE,
                                        y        : api.local.sapDistiListPricePercentage,
                                        drilldown: Constants.SAP_DISTI_LIST_PRICE
                                ],
                                [
                                        name     : Constants.RESELLER_LIST_PRICE,
                                        y        : api.local.resellerListPricePercentage,
                                        drilldown: Constants.RESELLER_LIST_PRICE
                                ],
                                [
                                        name     : Constants.SAP_RESELLER_LIST_PRICE,
                                        y        : api.local.sapResellerListPricePercentage,
                                        drilldown: Constants.SAP_RESELLER_LIST_PRICE
                                ],
                        ]
                ]
        ],
        drilldown    : [
                breadcrumbs: [
                        position: [
                                align: 'right'
                        ]
                ],
                series     : [
                        [
                                name: Constants.PRICEFX_LIST_PRICE,
                                id  : Constants.PRICEFX_LIST_PRICE,
                                data: [
                                        [Constants.NAMUSD,
                                         out.PriceFxLocalCurrency?.NAMUSD
                                        ],
                                        [Constants.APACUSD,
                                         out.PriceFxLocalCurrency?.APACUSD
                                        ],
                                        [Constants.CHINAUSD,
                                         out.PriceFxLocalCurrency?.CHINAUSD
                                        ],
                                        [Constants.LATAMUSD,
                                         out.PriceFxLocalCurrency?.LATAMUSD
                                        ],
                                        [Constants.EMEAUSD,
                                         out.PriceFxLocalCurrency?.EMEAUSD
                                        ],
                                        [Constants.EMEAUSD2,
                                         out.PriceFxLocalCurrency?.EMEAUSD2
                                        ],
                                        [Constants.GLOBALUSD,
                                         out.PriceFxLocalCurrency?.GlobalUSD
                                        ],
                                        [Constants.EMEAEUR,
                                         out.PriceFxLocalCurrency?.EMEAEUR
                                        ],
                                        [Constants.EMEAGBP,
                                         out.PriceFxLocalCurrency?.EMEAGBP
                                        ],
                                        [Constants.APACCNY,
                                         out.PriceFxLocalCurrency?.APACCNY
                                        ],
                                        [Constants.APACAUD,
                                         out.PriceFxLocalCurrency?.APACAUD
                                        ],
                                        [Constants.APACJPY,
                                         out.PriceFxLocalCurrency?.APACJPY
                                        ],
                                ]
                        ],
                        [
                                name: Constants.SAP_LIST_PRICE,
                                id  : Constants.SAP_LIST_PRICE,
                                data: [
                                        [Constants.SAP_NAMUSD,
                                         out.SAPLocalCurrency?.SAPNAMUSD
                                        ],
                                        [Constants.SAP_APACUSD,
                                         out.SAPLocalCurrency?.SAPAPACUSD
                                        ],
                                        [Constants.SAP_CHINAUSD,
                                         out.SAPLocalCurrency?.SAPCHINAUSD
                                        ],
                                        [Constants.SAP_LATAMUSD,
                                         out.SAPLocalCurrency?.SAPLATAMUSD
                                        ],
                                        [Constants.SAP_EMEAUSD,
                                         out.SAPLocalCurrency?.SAPEMEAUSD
                                        ],
                                        [Constants.SAP_EMEAUSD2,
                                         out.SAPLocalCurrency?.SAPEMEAUSD2
                                        ],
                                        [Constants.SAP_GLOBALUSD,
                                         out.SAPLocalCurrency?.SAPGlobalUSD
                                        ],
                                        [Constants.SAP_EMEAEUR,
                                         out.SAPLocalCurrency?.SAPEMEAEUR
                                        ],
                                        [Constants.SAP_EMEAGBP,
                                         out.SAPLocalCurrency?.SAPEMEAGBP
                                        ],
                                        [Constants.SAP_APACCNY,
                                         out.SAPLocalCurrency?.SAPAPACCNY
                                        ],
                                        [Constants.SAP_APACAUD,
                                         out.SAPLocalCurrency?.SAPAPACAUD
                                        ],
                                        [Constants.SAP_APACJPY,
                                         out.SAPLocalCurrency?.SAPAPACJPY
                                        ],
                                ]
                        ],
                        [
                                name: Constants.DISTI_LIST_PRICE,
                                id  : Constants.DISTI_LIST_PRICE,
                                data: [
                                        [Constants.DISTI_NAMUSD,
                                         out.DistiListPrice?.DISTINAMUSD
                                        ],
                                        [Constants.DISTI_APACUSD,
                                         out.DistiListPrice?.DISTIAPACUSD
                                        ],
                                        [Constants.DISTI_CHINAUSD,
                                         out.DistiListPrice?.DISTICHINAUSD
                                        ],
                                        [Constants.DISTI_LATAMUSD,
                                         out.DistiListPrice?.DISTILATAMUSD
                                        ],
                                        [Constants.DISTI_EMEAUSD,
                                         out.DistiListPrice?.DISTIEMEAUSD
                                        ],
                                        [Constants.DISTI_EMEAUSD2,
                                         out.DistiListPrice?.DISTIEMEAUSD2
                                        ],
                                        [Constants.DISTI_GLOBALUSD,
                                         out.DistiListPrice?.DISTIGlobalUSD
                                        ],
                                        [Constants.DISTI_EMEAEUR,
                                         out.DistiListPrice?.DISTIEMEAEUR
                                        ],
                                        [Constants.DISTI_EMEAGBP,
                                         out.DistiListPrice?.DISTIEMEAGBP
                                        ],
                                        [Constants.DISTI_APACCNY,
                                         out.DistiListPrice?.DISTIAPACCNY
                                        ],
                                        [Constants.DISTI_APACAUD,
                                         out.DistiListPrice?.DISTIAPACAUD
                                        ],
                                        [Constants.DISTI_APACJPY,
                                         out.DistiListPrice?.DISTIAPACJPY
                                        ],
                                ]
                        ],
                        [
                                name: Constants.SAP_DISTI_LIST_PRICE,
                                id  : Constants.SAP_DISTI_LIST_PRICE,
                                data: [
                                        [Constants.SAP_DISTI_NAMUSD,
                                         out.SAPDistiListPrice?.SAPDISTINAMUSD
                                        ],
                                        [Constants.SAP_DISTI_APACUSD,
                                         out.SAPDistiListPrice?.SAPDISTIAPACUSD
                                        ],
                                        [Constants.SAP_DISTI_CHINAUSD,
                                         out.SAPDistiListPrice?.SAPDISTICHINAUSD
                                        ],
                                        [Constants.SAP_DISTI_LATAMUSD,
                                         out.SAPDistiListPrice?.SAPDISTILATAMUSD
                                        ],
                                        [Constants.SAP_DISTI_EMEAUSD,
                                         out.SAPDistiListPrice?.SAPDISTIEMEAUSD
                                        ],
                                        [Constants.SAP_DISTI_EMEAUSD2,
                                         out.SAPDistiListPrice?.SAPDISTIEMEAUSD2
                                        ],
                                        [Constants.SAP_DISTI_GLOBALUSD,
                                         out.SAPDistiListPrice?.SAPDISTIGlobalUSD
                                        ],
                                        [Constants.SAP_DISTI_EMEAEUR,
                                         out.SAPDistiListPrice?.SAPDISTIEMEAEUR
                                        ],
                                        [Constants.SAP_DISTI_EMEAGBP,
                                         out.SAPDistiListPrice?.SAPDISTIEMEAGBP
                                        ],
                                        [Constants.SAP_DISTI_APACCNY,
                                         out.SAPDistiListPrice?.SAPDISTIAPACCNY
                                        ],
                                        [Constants.SAP_DISTI_APACAUD,
                                         out.SAPDistiListPrice?.SAPDISTIAPACAUD
                                        ],
                                        [Constants.SAP_DISTI_APACJPY,
                                         out.SAPDistiListPrice?.SAPDISTIAPACJPY
                                        ],
                                ]
                        ],
                        [
                                name: Constants.RESELLER_LIST_PRICE,
                                id  : Constants.RESELLER_LIST_PRICE,
                                data: [
                                        [Constants.RESELLER_NAMUSD,
                                         out.ResellerListPrice?.RESELLERNAMUSD
                                        ],
                                        [Constants.RESELLER_APACUSD,
                                         out.ResellerListPrice?.RESELLERAPACUSD
                                        ],
                                        [Constants.RESELLER_CHINAUSD,
                                         out.ResellerListPrice?.RESELLERCHINAUSD
                                        ],
                                        [Constants.RESELLER_LATAMUSD,
                                         out.ResellerListPrice?.RESELLERLATAMUSD
                                        ],
                                        [Constants.RESELLER_EMEAUSD,
                                         out.ResellerListPrice?.RESELLEREMEAUSD
                                        ],
                                        [Constants.RESELLER_EMEAUSD2,
                                         out.ResellerListPrice?.RESELLEREMEAUSD2
                                        ],
                                        [Constants.RESELLER_GLOBALUSD,
                                         out.ResellerListPrice?.RESELLERGlobalUSD
                                        ],
                                        [Constants.RESELLER_EMEAEUR,
                                         out.ResellerListPrice?.RESELLEREMEAEUR
                                        ],
                                        [Constants.RESELLER_EMEAGBP,
                                         out.ResellerListPrice?.RESELLEREMEAGBP
                                        ],
                                        [Constants.RESELLER_APACCNY,
                                         out.ResellerListPrice?.RESELLERAPACCNY
                                        ],
                                        [Constants.RESELLER_APACAUD,
                                         out.ResellerListPrice?.RESELLERAPACAUD
                                        ],
                                        [Constants.RESELLER_APACJPY,
                                         out.ResellerListPrice?.RESELLERAPACJPY
                                        ],
                                ]
                        ],
                        [
                                name: Constants.SAP_RESELLER_LIST_PRICE,
                                id  : Constants.SAP_RESELLER_LIST_PRICE,
                                data: [
                                        [Constants.SAP_RESELLER_NAMUSD,
                                         out.SAPResellerListPrice?.SAPRESELLERNAMUSD
                                        ],
                                        [Constants.SAP_RESELLER_APACUSD,
                                         out.SAPResellerListPrice?.SAPRESELLERAPACUSD
                                        ],
                                        [Constants.SAP_RESELLER_CHINAUSD,
                                         out.SAPResellerListPrice?.SAPRESELLERCHINAUSD
                                        ],
                                        [Constants.SAP_RESELLER_LATAMUSD,
                                         out.SAPResellerListPrice?.SAPRESELLERLATAMUSD
                                        ],
                                        [Constants.SAP_RESELLER_EMEAUSD,
                                         out.SAPResellerListPrice?.SAPRESELLEREMEAUSD
                                        ],
                                        [Constants.SAP_RESELLER_EMEAUSD2,
                                         out.SAPResellerListPrice?.SAPRESELLEREMEAUSD2
                                        ],
                                        [Constants.SAP_RESELLER_GLOBALUSD,
                                         out.SAPResellerListPrice?.SAPRESELLERGlobalUSD
                                        ],
                                        [Constants.SAP_RESELLER_EMEAEUR,
                                         out.SAPResellerListPrice?.SAPRESELLEREMEAEUR
                                        ],
                                        [Constants.SAP_RESELLER_EMEAGBP,
                                         out.SAPResellerListPrice?.SAPRESELLEREMEAGBP
                                        ],
                                        [Constants.SAP_RESELLER_APACCNY,
                                         out.SAPResellerListPrice?.SAPRESELLERAPACCNY
                                        ],
                                        [Constants.SAP_RESELLER_APACAUD,
                                         out.SAPResellerListPrice?.SAPRESELLERAPACAUD
                                        ],
                                        [Constants.SAP_RESELLER_APACJPY,
                                         out.SAPResellerListPrice?.SAPRESELLERAPACJPY
                                        ],
                                ]
                        ],
                ]
        ]
]
def chart = api.setPricegridCalculationChart(chartDef, out.Id)
return chart