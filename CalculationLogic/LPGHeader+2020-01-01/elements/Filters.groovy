Map inputs = Lib.getInputs()
List product = inputs?.product
List offer = inputs?.offer
List base = inputs?.base
List filters = [Filter.equal(Constants.PRICE_GRID_ID, out.Id)]
if (product) {
    filters << Filter.in(Constants.COLUMN_PRODUCT, product)
}
if (offer) {
    filters << Filter.in(Constants.COLUMN_OFFER, offer)
}
if (base) {
    filters << Filter.in(Constants.COLUMN_BASE, base)
}
return filters