def headerConfig = api.inlineConfigurator(Constants.HEADER_INPUTS, Constants.HEADER_CONFIGURATOR_LOGIC)
String id = api.currentItem(Constants.ID)?.toString()
def conf = api.getParameter(Constants.HEADER_INPUTS)
if (conf != null && conf?.getValue() == null) {
    conf?.setValue([(Constants.LPGID): id])
}
return headerConfig