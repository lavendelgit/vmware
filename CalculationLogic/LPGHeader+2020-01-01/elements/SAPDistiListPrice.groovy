Map sapDistiListPrice = [
        (Constants.SAP_DISTI_NAMUSD)   : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_NAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_NAMUSD)?.sum()),
        (Constants.SAP_DISTI_APACUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_APACUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_APACUSD)?.sum()),
        (Constants.SAP_DISTI_CHINAUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_CHINAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_CHINAUSD)?.sum()),
        (Constants.SAP_DISTI_LATAMUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_LATAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_LATAMUSD)?.sum()),
        (Constants.SAP_DISTI_EMEAUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_EMEAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_EMEAUSD)?.sum()),
        (Constants.SAP_DISTI_EMEAUSD2) : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_EMEAUSD2))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_EMEAUSD2)?.sum()),
        (Constants.SAP_DISTI_GLOBALUSD): Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_GLOBALUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_GLOBALUSD)?.sum()),
        (Constants.SAP_DISTI_EMEAEUR)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_EMEAEUR))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_EMEAEUR)?.sum()),
        (Constants.SAP_DISTI_EMEAGBP)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_EMEAGBP))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_EMEAGBP)?.sum()),
        (Constants.SAP_DISTI_APACCNY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_APACCNY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_APACCNY)?.sum()),
        (Constants.SAP_DISTI_APACAUD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_APACAUD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_APACAUD)?.sum()),
        (Constants.SAP_DISTI_APACJPY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_DISTI_APACJPY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_DISTI_APACJPY)?.sum())
]
return sapDistiListPrice
