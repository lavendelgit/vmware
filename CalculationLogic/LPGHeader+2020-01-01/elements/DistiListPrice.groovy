Map distiPricePercent = [
        (Constants.DISTI_NAMUSD)   : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_NAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_NAMUSD)?.sum()),
        (Constants.DISTI_APACUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_APACUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_APACUSD)?.sum()),
        (Constants.DISTI_CHINAUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_CHINAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_CHINAUSD)?.sum()),
        (Constants.DISTI_LATAMUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_LATAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_LATAMUSD)?.sum()),
        (Constants.DISTI_EMEAUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_EMEAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_EMEAUSD)?.sum()),
        (Constants.DISTI_EMEAUSD2) : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_EMEAUSD2))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_EMEAUSD2)?.sum()),
        (Constants.DISTI_GLOBALUSD): Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_GLOBALUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_GLOBALUSD)?.sum()),
        (Constants.DISTI_EMEAEUR)  : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_EMEAEUR))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_EMEAEUR)?.sum()),
        (Constants.DISTI_EMEAGBP)  : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_EMEAGBP))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_EMEAGBP)?.sum()),
        (Constants.DISTI_APACCNY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_APACCNY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_APACCNY)?.sum()),
        (Constants.DISTI_APACAUD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_APACAUD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_APACAUD)?.sum()),
        (Constants.DISTI_APACJPY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.DISTI_APACJPY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_DISTI_APACJPY)?.sum())
]
return distiPricePercent
