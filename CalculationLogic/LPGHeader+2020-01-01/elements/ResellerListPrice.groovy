Map resellerPricePercent = [
        (Constants.RESELLER_NAMUSD)   : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_NAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_NAMUSD)?.sum()),
        (Constants.RESELLER_APACUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_APACUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_APACUSD)?.sum()),
        (Constants.RESELLER_CHINAUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_CHINAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_CHINAUSD)?.sum()),
        (Constants.RESELLER_LATAMUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_LATAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_LATAMUSD)?.sum()),
        (Constants.RESELLER_EMEAUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_EMEAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_EMEAUSD)?.sum()),
        (Constants.RESELLER_EMEAUSD2) : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_EMEAUSD2))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_EMEAUSD2)?.sum()),
        (Constants.RESELLER_GLOBALUSD): Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_GLOBALUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_GLOBALUSD)?.sum()),
        (Constants.RESELLER_EMEAEUR)  : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_EMEAEUR))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_EMEAEUR)?.sum()),
        (Constants.RESELLER_EMEAGBP)  : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_EMEAGBP))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_EMEAGBP)?.sum()),
        (Constants.RESELLER_APACCNY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_APACCNY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_APACCNY)?.sum()),
        (Constants.RESELLER_APACAUD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_APACAUD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_APACAUD)?.sum()),
        (Constants.RESELLER_APACJPY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.RESELLER_APACJPY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_RESELLER_APACJPY)?.sum())
]
return resellerPricePercent
