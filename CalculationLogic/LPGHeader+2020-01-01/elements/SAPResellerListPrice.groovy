Map sapResellerPricePercent = [
        (Constants.SAP_RESELLER_NAMUSD)   : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_NAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_NAMUSD)?.sum()),
        (Constants.SAP_RESELLER_APACUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_APACUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_APACUSD)?.sum()),
        (Constants.SAP_RESELLER_CHINAUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_CHINAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_CHINAUSD)?.sum()),
        (Constants.SAP_RESELLER_LATAMUSD) : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_LATAMUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_LATAMUSD)?.sum()),
        (Constants.SAP_RESELLER_EMEAUSD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_EMEAUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_EMEAUSD)?.sum()),
        (Constants.SAP_RESELLER_EMEAUSD2) : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_EMEAUSD2))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_EMEAUSD2)?.sum()),
        (Constants.SAP_RESELLER_GLOBALUSD): Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_GLOBALUSD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_GLOBALUSD)?.sum()),
        (Constants.SAP_RESELLER_EMEAEUR)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_EMEAEUR))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_EMEAEUR)?.sum()),
        (Constants.SAP_RESELLER_EMEAGBP)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_EMEAGBP))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_EMEAGBP)?.sum()),
        (Constants.SAP_RESELLER_APACCNY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_APACCNY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_APACCNY)?.sum()),
        (Constants.SAP_RESELLER_APACAUD)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_APACAUD))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_APACAUD)?.sum()),
        (Constants.SAP_RESELLER_APACJPY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_RESELLER_APACJPY))?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_RESELLER_APACJPY)?.sum())
]
return sapResellerPricePercent
