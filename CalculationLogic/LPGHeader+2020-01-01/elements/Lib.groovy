Map getInputs() {
    def headerInputs = api.jsonDecode(out.CurrentItem?.configuration)?.headerInputs?.value
    return [
            product: headerInputs?.(Constants.PRODUCT),
            offer  : headerInputs?.(Constants.OFFER),
            base   : headerInputs?.(Constants.BASE)
    ]
}

BigDecimal calculatePercent(BigDecimal priceChangeTotal, BigDecimal previousPriceTotal) {
    if (priceChangeTotal == null) {
        priceChangeTotal = 0
    }
    if (previousPriceTotal == null) {
        previousPriceTotal = 0
    }
    if (((priceChangeTotal) && (previousPriceTotal)) == 0) {
        return 0.0
    } else if (previousPriceTotal == 0) {
        return 100.0
    }
    else {
        return (priceChangeTotal / previousPriceTotal) * 100
    }
}