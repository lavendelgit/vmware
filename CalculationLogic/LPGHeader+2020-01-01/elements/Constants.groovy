import groovy.transform.Field

@Field final String PRICE_GRID_ID = "priceGridId"
@Field final String ID = "id"
@Field final String LPGID = "LpgId"
@Field final String PRODUCT = "Product"
@Field final String OFFER = "Offer"
@Field final String BASE = "Base"
@Field final String COLUMN_PRODUCT = "attribute3"
@Field final String COLUMN_OFFER = "attribute4"
@Field final String COLUMN_BASE = "attribute5"
@Field final String HEADER_INPUTS = "ConfigInputs"
@Field final String HEADER_CONFIGURATOR_LOGIC = "HeaderConfigurator"
@Field final String Δ_NAMUSD_LIST_PRICE = "Δ NAMUSD List Price"
@Field final String Δ_APACUSD_LIST_PRICE = "Δ APACUSD List Price"
@Field final String Δ_CHINAUSD_LIST_PRICE = "Δ CHINAUSD List Price"
@Field final String Δ_LATAMUSD_LIST_PRICE = "Δ LATAMUSD List Price"
@Field final String Δ_EMEAUSD_LIST_PRICE = "Δ EMEAUSD List Price"
@Field final String Δ_EMEAUSD2_LIST_PRICE = "Δ EMEAUSD2 List Price"
@Field final String Δ_GLOBALUSD_LIST_PRICE = "Δ GlobalUSD List Price"
@Field final String Δ_EMEAEUR_LIST_PRICE = "Δ EMEAEUR List Price"
@Field final String Δ_EMEAGBP_LIST_PRICE = "Δ EMEAGBP List Price"
@Field final String Δ_APACCNY_LIST_PRICE = "Δ APACCNY List Price"
@Field final String Δ_APACAUD_LIST_PRICE = "Δ APACAUD List Price"
@Field final String Δ_APACJPY_LIST_PRICE = "Δ APACJPY List Price"
@Field final String Δ_NAMUSD_SAP_LIST_PRICE = "Δ NAMUSD SAP List Price"
@Field final String Δ_APACUSD_SAP_LIST_PRICE = "Δ APACUSD SAP List Price"
@Field final String Δ_CHINAUSD_SAP_LIST_PRICE = "Δ CHINAUSD SAP List Price"
@Field final String Δ_LATAMUSD_SAP_LIST_PRICE = "Δ LATAMUSD SAP List Price"
@Field final String Δ_EMEAUSD_SAP_LIST_PRICE = "Δ EMEAUSD SAP List Price"
@Field final String Δ_EMEAUSD2_SAP_LIST_PRICE = "Δ EMEAUSD2 SAP List Price"
@Field final String Δ_GLOBALUSD_SAP_LIST_PRICE = "Δ GlobalUSD SAP List Price"
@Field final String Δ_EMEAEUR_SAP_LIST_PRICE = "Δ EMEAEUR SAP List Price"
@Field final String Δ_EMEAGBP_SAP_LIST_PRICE = "Δ EMEAGBP SAP List Price"
@Field final String Δ_APACCNY_SAP_LIST_PRICE = "Δ APACCNY SAP List Price"
@Field final String Δ_APACAUD_SAP_LIST_PRICE = "Δ APACAUD SAP List Price"
@Field final String Δ_APACJPY_SAP_LIST_PRICE = "Δ APACJPY SAP List Price"
@Field final String Δ_NAMUSD_DISTI_LIST_PRICE = "Δ NAMUSD Disti List Price"
@Field final String Δ_APACUSD_DISTI_LIST_PRICE = "Δ APACUSD Disti List Price"
@Field final String Δ_CHINAUSD_DISTI_LIST_PRICE = "Δ CHINAUSD Disti List Price"
@Field final String Δ_LATAMUSD_DISTI_LIST_PRICE = "Δ LATAMUSD Disti List Price"
@Field final String Δ_EMEAUSD_DISTI_LIST_PRICE = "Δ EMEAUSD Disti List Price"
@Field final String Δ_EMEAUSD2_DISTI_LIST_PRICE = "Δ EMEAUSD2 Disti List Price"
@Field final String Δ_GLOBALUSD_DISTI_LIST_PRICE = "Δ GlobalUSD Disti List Price"
@Field final String Δ_EMEAEUR_DISTI_LIST_PRICE = "Δ EMEAEUR Disti List Price"
@Field final String Δ_EMEAGBP_DISTI_LIST_PRICE = "Δ EMEAGBP Disti List Price"
@Field final String Δ_APACCNY_DISTI_LIST_PRICE = "Δ APACCNY Disti List Price"
@Field final String Δ_APACAUD_DISTI_LIST_PRICE = "Δ APACAUD Disti List Price"
@Field final String Δ_APACJPY_DISTI_LIST_PRICE = "Δ APACJPY Disti List Price"
@Field final String Δ_NAMUSD_SAP_DISTI_LIST_PRICE = "Δ NAMUSD SAP Disti List Price"
@Field final String Δ_APACUSD_SAP_DISTI_LIST_PRICE = "Δ APACUSD SAP Disti List Price"
@Field final String Δ_CHINAUSD_SAP_DISTI_LIST_PRICE = "Δ CHINAUSD SAP Disti List Price"
@Field final String Δ_LATAMUSD_SAP_DISTI_LIST_PRICE = "Δ LATAMUSD SAP Disti List Price"
@Field final String Δ_EMEAUSD_SAP_DISTI_LIST_PRICE = "Δ EMEAUSD SAP Disti List Price"
@Field final String Δ_EMEAUSD2_SAP_DISTI_LIST_PRICE = "Δ EMEAUSD2 SAP Disti List Price"
@Field final String Δ_GLOBALUSD_SAP_DISTI_LIST_PRICE = "Δ GlobalUSD SAP Disti List Price"
@Field final String Δ_EMEAEUR_SAP_DISTI_LIST_PRICE = "Δ EMEAEUR SAP Disti List Price"
@Field final String Δ_EMEAGBP_SAP_DISTI_LIST_PRICE = "Δ EMEAGBP SAP Disti List Price"
@Field final String Δ_APACCNY_SAP_DISTI_LIST_PRICE = "Δ APACCNY SAP Disti List Price"
@Field final String Δ_APACAUD_SAP_DISTI_LIST_PRICE = "Δ APACAUD SAP Disti List Price"
@Field final String Δ_APACJPY_SAP_DISTI_LIST_PRICE = "Δ APACJPY SAP Disti List Price"
@Field final String Δ_NAMUSD_RESELLER_LIST_PRICE = "Δ NAMUSD Reseller List Price"
@Field final String Δ_APACUSD_RESELLER_LIST_PRICE = "Δ APACUSD Reseller List Price"
@Field final String Δ_CHINAUSD_RESELLER_LIST_PRICE = "Δ CHINAUSD Reseller List Price"
@Field final String Δ_LATAMUSD_RESELLER_LIST_PRICE = "Δ LATAMUSD Reseller List Price"
@Field final String Δ_EMEAUSD_RESELLER_LIST_PRICE = "Δ EMEAUSD Reseller List Price"
@Field final String Δ_EMEAUSD2_RESELLER_LIST_PRICE = "Δ EMEAUSD2 Reseller List Price"
@Field final String Δ_GLOBALUSD_RESELLER_LIST_PRICE = "Δ GlobalUSD Reseller List Price"
@Field final String Δ_EMEAEUR_RESELLER_LIST_PRICE = "Δ EMEAEUR Reseller List Price"
@Field final String Δ_EMEAGBP_RESELLER_LIST_PRICE = "Δ EMEAGBP Reseller List Price"
@Field final String Δ_APACCNY_RESELLER_LIST_PRICE = "Δ APACCNY Reseller List Price"
@Field final String Δ_APACAUD_RESELLER_LIST_PRICE = "Δ APACAUD Reseller List Price"
@Field final String Δ_APACJPY_RESELLER_LIST_PRICE = "Δ APACJPY Reseller List Price"
@Field final String Δ_NAMUSD_SAP_RESELLER_LIST_PRICE = "Δ NAMUSD SAP Reseller List Price"
@Field final String Δ_APACUSD_SAP_RESELLER_LIST_PRICE = "Δ APACUSD SAP Reseller List Price"
@Field final String Δ_CHINAUSD_SAP_RESELLER_LIST_PRICE = "Δ CHINAUSD SAP Reseller List Price"
@Field final String Δ_LATAMUSD_SAP_RESELLER_LIST_PRICE = "Δ LATAMUSD SAP Reseller List Price"
@Field final String Δ_EMEAUSD_SAP_RESELLER_LIST_PRICE = "Δ EMEAUSD SAP Reseller List Price"
@Field final String Δ_EMEAUSD2_SAP_RESELLER_LIST_PRICE = "Δ EMEAUSD2 SAP Reseller List Price"
@Field final String Δ_GLOBALUSD_SAP_RESELLER_LIST_PRICE = "Δ GlobalUSD SAP Reseller List Price"
@Field final String Δ_EMEAEUR_SAP_RESELLER_LIST_PRICE = "Δ EMEAEUR SAP Reseller List Price"
@Field final String Δ_EMEAGBP_SAP_RESELLER_LIST_PRICE = "Δ EMEAGBP SAP Reseller List Price"
@Field final String Δ_APACCNY_SAP_RESELLER_LIST_PRICE = "Δ APACCNY SAP Reseller List Price"
@Field final String Δ_APACAUD_SAP_RESELLER_LIST_PRICE = "Δ APACAUD SAP Reseller List Price"
@Field final String Δ_APACJPY_SAP_RESELLER_LIST_PRICE = "Δ APACJPY SAP Reseller List Price"
@Field final String PREVIOUS_NAMUSD_LIST_PRICE = "Previous NAMUSD List Price"
@Field final String PREVIOUS_APACUSD_LIST_PRICE = "Previous APACUSD List Price"
@Field final String PREVIOUS_CHINAUSD_LIST_PRICE = "Previous CHINAUSD List Price"
@Field final String PREVIOUS_LATAMUSD_LIST_PRICE = "Previous LATAMUSD List Price"
@Field final String PREVIOUS_EMEAUSD_LIST_PRICE = "Previous EMEAUSD List Price"
@Field final String PREVIOUS_EMEAUSD2_LIST_PRICE = "Previous EMEAUSD2 List Price"
@Field final String PREVIOUS_GLOBALUSD_LIST_PRICE = "Previous GlobalUSD List Price"
@Field final String PREVIOUS_EMEAEUR_LIST_PRICE = "Previous EMEAEUR List Price"
@Field final String PREVIOUS_EMEAGBP_LIST_PRICE = "Previous EMEAGBP List Price"
@Field final String PREVIOUS_APACCNY_LIST_PRICE = "Previous APACCNY List Price"
@Field final String PREVIOUS_APACAUD_LIST_PRICE = "Previous APACAUD List Price"
@Field final String PREVIOUS_APACJPY_LIST_PRICE = "Previous APACJPY List Price"
@Field final String PREVIOUS_NAMUSD_SAP_LIST_PRICE = "Previous NAMUSD SAP List Price"
@Field final String PREVIOUS_APACUSD_SAP_LIST_PRICE = "Previous APACUSD SAP List Price"
@Field final String PREVIOUS_CHINAUSD_SAP_LIST_PRICE = "Previous CHINAUSD SAP List Price"
@Field final String PREVIOUS_LATAMUSD_SAP_LIST_PRICE = "Previous LATAMUSD SAP List Price"
@Field final String PREVIOUS_EMEAUSD_SAP_LIST_PRICE = "Previous EMEAUSD SAP List Price"
@Field final String PREVIOUS_EMEAUSD2_SAP_LIST_PRICE = "Previous EMEAUSD2 SAP List Price"
@Field final String PREVIOUS_GLOBALUSD_SAP_LIST_PRICE = "Previous GlobalUSD SAP List Price"
@Field final String PREVIOUS_EMEAEUR_SAP_LIST_PRICE = "Previous EMEAEUR SAP List Price"
@Field final String PREVIOUS_EMEAGBP_SAP_LIST_PRICE = "Previous EMEAGBP SAP List Price"
@Field final String PREVIOUS_APACCNY_SAP_LIST_PRICE = "Previous APACCNY SAP List Price"
@Field final String PREVIOUS_APACAUD_SAP_LIST_PRICE = "Previous APACAUD SAP List Price"
@Field final String PREVIOUS_APACJPY_SAP_LIST_PRICE = "Previous APACJPY SAP List Price"
@Field final String PREVIOUS_NAMUSD_DISTI_LIST_PRICE = "Previous NAMUSD Disti List Price"
@Field final String PREVIOUS_APACUSD_DISTI_LIST_PRICE = "Previous APACUSD Disti List Price"
@Field final String PREVIOUS_CHINAUSD_DISTI_LIST_PRICE = "Previous CHINAUSD Disti List Price"
@Field final String PREVIOUS_LATAMUSD_DISTI_LIST_PRICE = "Previous LATAMUSD Disti List Price"
@Field final String PREVIOUS_EMEAUSD_DISTI_LIST_PRICE = "Previous EMEAUSD Disti List Price"
@Field final String PREVIOUS_EMEAUSD2_DISTI_LIST_PRICE = "Previous EMEAUSD2 Disti List Price"
@Field final String PREVIOUS_GLOBALUSD_DISTI_LIST_PRICE = "Previous GlobalUSD Disti List Price"
@Field final String PREVIOUS_EMEAEUR_DISTI_LIST_PRICE = "Previous EMEAEUR Disti List Price"
@Field final String PREVIOUS_EMEAGBP_DISTI_LIST_PRICE = "Previous EMEAGBP Disti List Price"
@Field final String PREVIOUS_APACCNY_DISTI_LIST_PRICE = "Previous APACCNY Disti List Price"
@Field final String PREVIOUS_APACAUD_DISTI_LIST_PRICE = "Previous APACAUD Disti List Price"
@Field final String PREVIOUS_APACJPY_DISTI_LIST_PRICE = "Previous APACJPY Disti List Price"
@Field final String PREVIOUS_NAMUSD_SAP_DISTI_LIST_PRICE = "Previous NAMUSD SAP Disti List Price"
@Field final String PREVIOUS_APACUSD_SAP_DISTI_LIST_PRICE = "Previous APACUSD SAP Disti List Price"
@Field final String PREVIOUS_CHINAUSD_SAP_DISTI_LIST_PRICE = "Previous CHINAUSD SAP Disti List Price"
@Field final String PREVIOUS_LATAMUSD_SAP_DISTI_LIST_PRICE = "Previous LATAMUSD SAP Disti List Price"
@Field final String PREVIOUS_EMEAUSD_SAP_DISTI_LIST_PRICE = "Previous EMEAUSD SAP Disti List Price"
@Field final String PREVIOUS_EMEAUSD2_SAP_DISTI_LIST_PRICE = "Previous EMEAUSD2 SAP Disti List Price"
@Field final String PREVIOUS_GLOBALUSD_SAP_DISTI_LIST_PRICE = "Previous GlobalUSD SAP Disti List Price"
@Field final String PREVIOUS_EMEAEUR_SAP_DISTI_LIST_PRICE = "Previous EMEAEUR SAP Disti List Price"
@Field final String PREVIOUS_EMEAGBP_SAP_DISTI_LIST_PRICE = "Previous EMEAGBP SAP Disti List Price"
@Field final String PREVIOUS_APACCNY_SAP_DISTI_LIST_PRICE = "Previous APACCNY SAP Disti List Price"
@Field final String PREVIOUS_APACAUD_SAP_DISTI_LIST_PRICE = "Previous APACAUD SAP Disti List Price"
@Field final String PREVIOUS_APACJPY_SAP_DISTI_LIST_PRICE = "Previous APACJPY SAP Disti List Price"
@Field final String PREVIOUS_NAMUSD_RESELLER_LIST_PRICE = "Previous NAMUSD Reseller List Price"
@Field final String PREVIOUS_APACUSD_RESELLER_LIST_PRICE = "Previous APACUSD Reseller List Price"
@Field final String PREVIOUS_CHINAUSD_RESELLER_LIST_PRICE = "Previous CHINAUSD Reseller List Price"
@Field final String PREVIOUS_LATAMUSD_RESELLER_LIST_PRICE = "Previous LATAMUSD Reseller List Price"
@Field final String PREVIOUS_EMEAUSD_RESELLER_LIST_PRICE = "Previous EMEAUSD Reseller List Price"
@Field final String PREVIOUS_EMEAUSD2_RESELLER_LIST_PRICE = "Previous EMEAUSD2 Reseller List Price"
@Field final String PREVIOUS_GLOBALUSD_RESELLER_LIST_PRICE = "Previous GlobalUSD Reseller List Price"
@Field final String PREVIOUS_EMEAEUR_RESELLER_LIST_PRICE = "Previous EMEAEUR Reseller List Price"
@Field final String PREVIOUS_EMEAGBP_RESELLER_LIST_PRICE = "Previous EMEAGBP Reseller List Price"
@Field final String PREVIOUS_APACCNY_RESELLER_LIST_PRICE = "Previous APACCNY Reseller List Price"
@Field final String PREVIOUS_APACAUD_RESELLER_LIST_PRICE = "Previous APACAUD Reseller List Price"
@Field final String PREVIOUS_APACJPY_RESELLER_LIST_PRICE = "Previous APACJPY Reseller List Price"
@Field final String PREVIOUS_NAMUSD_SAP_RESELLER_LIST_PRICE = "Previous NAMUSD SAP Reseller List Price"
@Field final String PREVIOUS_APACUSD_SAP_RESELLER_LIST_PRICE = "Previous APACUSD SAP Reseller List Price"
@Field final String PREVIOUS_CHINAUSD_SAP_RESELLER_LIST_PRICE = "Previous CHINAUSD SAP Reseller List Price"
@Field final String PREVIOUS_LATAMUSD_SAP_RESELLER_LIST_PRICE = "Previous LATAMUSD SAP Reseller List Price"
@Field final String PREVIOUS_EMEAUSD_SAP_RESELLER_LIST_PRICE = "Previous EMEAUSD SAP Reseller List Price"
@Field final String PREVIOUS_EMEAUSD2_SAP_RESELLER_LIST_PRICE = "Previous EMEAUSD2 SAP Reseller List Price"
@Field final String PREVIOUS_GLOBALUSD_SAP_RESELLER_LIST_PRICE = "Previous GlobalUSD SAP Reseller List Price"
@Field final String PREVIOUS_EMEAEUR_SAP_RESELLER_LIST_PRICE = "Previous EMEAEUR SAP Reseller List Price"
@Field final String PREVIOUS_EMEAGBP_SAP_RESELLER_LIST_PRICE = "Previous EMEAGBP SAP Reseller List Price"
@Field final String PREVIOUS_APACCNY_SAP_RESELLER_LIST_PRICE = "Previous APACCNY SAP Reseller List Price"
@Field final String PREVIOUS_APACAUD_SAP_RESELLER_LIST_PRICE = "Previous APACAUD SAP Reseller List Price"
@Field final String PREVIOUS_APACJPY_SAP_RESELLER_LIST_PRICE = "Previous APACJPY SAP Reseller List Price"
@Field final String NAMUSD = "NAMUSD"
@Field final String APACUSD = "APACUSD"
@Field final String CHINAUSD = "CHINAUSD"
@Field final String LATAMUSD = "LATAMUSD"
@Field final String EMEAUSD = "EMEAUSD"
@Field final String EMEAUSD2 = "EMEAUSD2"
@Field final String GLOBALUSD = "GlobalUSD"
@Field final String EMEAEUR = "EMEAEUR"
@Field final String EMEAGBP = "EMEAGBP"
@Field final String APACCNY = "APACCNY"
@Field final String APACAUD = "APACAUD"
@Field final String APACJPY = "APACJPY"
@Field final String SAP_NAMUSD = "SAPNAMUSD"
@Field final String SAP_APACUSD = "SAPAPACUSD"
@Field final String SAP_CHINAUSD = "SAPCHINAUSD"
@Field final String SAP_LATAMUSD = "SAPLATAMUSD"
@Field final String SAP_EMEAUSD = "SAPEMEAUSD"
@Field final String SAP_EMEAUSD2 = "SAPEMEAUSD2"
@Field final String SAP_GLOBALUSD = "SAPGlobalUSD"
@Field final String SAP_EMEAEUR = "SAPEMEAEUR"
@Field final String SAP_EMEAGBP = "SAPEMEAGBP"
@Field final String SAP_APACCNY = "SAPAPACCNY"
@Field final String SAP_APACAUD = "SAPAPACAUD"
@Field final String SAP_APACJPY = "SAPAPACJPY"
@Field final String DISTI_NAMUSD = "DISTINAMUSD"
@Field final String DISTI_APACUSD = "DISTIAPACUSD"
@Field final String DISTI_CHINAUSD = "DISTICHINAUSD"
@Field final String DISTI_LATAMUSD = "DISTILATAMUSD"
@Field final String DISTI_EMEAUSD = "DISTIEMEAUSD"
@Field final String DISTI_EMEAUSD2 = "DISTIEMEAUSD2"
@Field final String DISTI_GLOBALUSD = "DISTIGlobalUSD"
@Field final String DISTI_EMEAEUR = "DISTIEMEAEUR"
@Field final String DISTI_EMEAGBP = "DISTIEMEAGBP"
@Field final String DISTI_APACCNY = "DISTIAPACCNY"
@Field final String DISTI_APACAUD = "DISTIAPACAUD"
@Field final String DISTI_APACJPY = "DISTIAPACJPY"
@Field final String SAP_DISTI_NAMUSD = "SAPDISTINAMUSD"
@Field final String SAP_DISTI_APACUSD = "SAPDISTIAPACUSD"
@Field final String SAP_DISTI_CHINAUSD = "SAPDISTICHINAUSD"
@Field final String SAP_DISTI_LATAMUSD = "SAPDISTILATAMUSD"
@Field final String SAP_DISTI_EMEAUSD = "SAPDISTIEMEAUSD"
@Field final String SAP_DISTI_EMEAUSD2 = "SAPDISTIEMEAUSD2"
@Field final String SAP_DISTI_GLOBALUSD = "SAPDISTIGlobalUSD"
@Field final String SAP_DISTI_EMEAEUR = "SAPDISTIEMEAEUR"
@Field final String SAP_DISTI_EMEAGBP = "SAPDISTIEMEAGBP"
@Field final String SAP_DISTI_APACCNY = "SAPDISTIAPACCNY"
@Field final String SAP_DISTI_APACAUD = "SAPDISTIAPACAUD"
@Field final String SAP_DISTI_APACJPY = "SAPDISTIAPACJPY"
@Field final String RESELLER_NAMUSD = "RESELLERNAMUSD"
@Field final String RESELLER_APACUSD = "RESELLERAPACUSD"
@Field final String RESELLER_CHINAUSD = "RESELLERCHINAUSD"
@Field final String RESELLER_LATAMUSD = "RESELLERLATAMUSD"
@Field final String RESELLER_EMEAUSD = "RESELLEREMEAUSD"
@Field final String RESELLER_EMEAUSD2 = "RESELLEREMEAUSD2"
@Field final String RESELLER_GLOBALUSD = "RESELLERGlobalUSD"
@Field final String RESELLER_EMEAEUR = "RESELLEREMEAEUR"
@Field final String RESELLER_EMEAGBP = "RESELLEREMEAGBP"
@Field final String RESELLER_APACCNY = "RESELLERAPACCNY"
@Field final String RESELLER_APACAUD = "RESELLERAPACAUD"
@Field final String RESELLER_APACJPY = "RESELLERAPACJPY"
@Field final String SAP_RESELLER_NAMUSD = "SAPRESELLERNAMUSD"
@Field final String SAP_RESELLER_APACUSD = "SAPRESELLERAPACUSD"
@Field final String SAP_RESELLER_CHINAUSD = "SAPRESELLERCHINAUSD"
@Field final String SAP_RESELLER_LATAMUSD = "SAPRESELLERLATAMUSD"
@Field final String SAP_RESELLER_EMEAUSD = "SAPRESELLEREMEAUSD"
@Field final String SAP_RESELLER_EMEAUSD2 = "SAPRESELLEREMEAUSD2"
@Field final String SAP_RESELLER_GLOBALUSD = "SAPRESELLERGlobalUSD"
@Field final String SAP_RESELLER_EMEAEUR = "SAPRESELLEREMEAEUR"
@Field final String SAP_RESELLER_EMEAGBP = "SAPRESELLEREMEAGBP"
@Field final String SAP_RESELLER_APACCNY = "SAPRESELLERAPACCNY"
@Field final String SAP_RESELLER_APACAUD = "SAPRESELLERAPACAUD"
@Field final String SAP_RESELLER_APACJPY = "SAPRESELLERAPACJPY"
@Field final String PREVIOUS_NAMUSD = "Previous NAMUSD"
@Field final String PREVIOUS_APACUSD = "Previous APACUSD"
@Field final String PREVIOUS_CHINAUSD = "Previous CHINAUSD"
@Field final String PREVIOUS_LATAMUSD = "Previous LATAMUSD"
@Field final String PREVIOUS_EMEAUSD = "Previous EMEAUSD"
@Field final String PREVIOUS_EMEAUSD2 = "Previous EMEAUSD2"
@Field final String PREVIOUS_GLOBALUSD = "Previous GlobalUSD"
@Field final String PREVIOUS_EMEAEUR = "Previous EMEAEUR"
@Field final String PREVIOUS_EMEAGBP = "Previous EMEAGBP"
@Field final String PREVIOUS_APACCNY = "Previous APACCNY"
@Field final String PREVIOUS_APACAUD = "Previous APACAUD"
@Field final String PREVIOUS_APACJPY = "Previous APACJPY"
@Field final String PREVIOUS_SAP_NAMUSD = "Previous SAPNAMUSD"
@Field final String PREVIOUS_SAP_APACUSD = "Previous SAPAPACUSD"
@Field final String PREVIOUS_SAP_CHINAUSD = "Previous SAPCHINAUSD"
@Field final String PREVIOUS_SAP_LATAMUSD = "Previous SAPLATAMUSD"
@Field final String PREVIOUS_SAP_EMEAUSD = "Previous SAPEMEAUSD"
@Field final String PREVIOUS_SAP_EMEAUSD2 = "Previous SAPEMEAUSD2"
@Field final String PREVIOUS_SAP_GLOBALUSD = "Previous SAPGlobalUSD"
@Field final String PREVIOUS_SAP_EMEAEUR = "Previous SAPEMEAEUR"
@Field final String PREVIOUS_SAP_EMEAGBP = "Previous SAPEMEAGBP"
@Field final String PREVIOUS_SAP_APACCNY = "Previous SAPAPACCNY"
@Field final String PREVIOUS_SAP_APACAUD = "Previous SAPAPACAUD"
@Field final String PREVIOUS_SAP_APACJPY = "Previous SAPAPACJPY"
@Field final String PREVIOUS_DISTI_NAMUSD = "Previous DISTINAMUSD"
@Field final String PREVIOUS_DISTI_APACUSD = "Previous DISTIAPACUSD"
@Field final String PREVIOUS_DISTI_CHINAUSD = "Previous DISTICHINAUSD"
@Field final String PREVIOUS_DISTI_LATAMUSD = "Previous DISTILATAMUSD"
@Field final String PREVIOUS_DISTI_EMEAUSD = "Previous DISTIEMEAUSD"
@Field final String PREVIOUS_DISTI_EMEAUSD2 = "Previous DISTIEMEAUSD2"
@Field final String PREVIOUS_DISTI_GLOBALUSD = "Previous DISTIGlobalUSD"
@Field final String PREVIOUS_DISTI_EMEAEUR = "Previous DISTIEMEAEUR"
@Field final String PREVIOUS_DISTI_EMEAGBP = "Previous DISTIEMEAGBP"
@Field final String PREVIOUS_DISTI_APACCNY = "Previous DISTIAPACCNY"
@Field final String PREVIOUS_DISTI_APACAUD = "Previous DISTIAPACAUD"
@Field final String PREVIOUS_DISTI_APACJPY = "Previous DISTIAPACJPY"
@Field final String PREVIOUS_SAP_DISTI_NAMUSD = "Previous SAPDISTINAMUSD"
@Field final String PREVIOUS_SAP_DISTI_APACUSD = "Previous SAPDISTIAPACUSD"
@Field final String PREVIOUS_SAP_DISTI_CHINAUSD = "Previous SAPDISTICHINAUSD"
@Field final String PREVIOUS_SAP_DISTI_LATAMUSD = "Previous SAPDISTILATAMUSD"
@Field final String PREVIOUS_SAP_DISTI_EMEAUSD = "Previous SAPDISTIEMEAUSD"
@Field final String PREVIOUS_SAP_DISTI_EMEAUSD2 = "Previous SAPDISTIEMEAUSD2"
@Field final String PREVIOUS_SAP_DISTI_GLOBALUSD = "Previous SAPDISTIGlobalUSD"
@Field final String PREVIOUS_SAP_DISTI_EMEAEUR = "Previous SAPDISTIEMEAEUR"
@Field final String PREVIOUS_SAP_DISTI_EMEAGBP = "Previous SAPDISTIEMEAGBP"
@Field final String PREVIOUS_SAP_DISTI_APACCNY = "Previous SAPDISTIAPACCNY"
@Field final String PREVIOUS_SAP_DISTI_APACAUD = "Previous SAPDISTIAPACAUD"
@Field final String PREVIOUS_SAP_DISTI_APACJPY = "Previous SAPDISTIAPACJPY"
@Field final String PREVIOUS_RESELLER_NAMUSD = "Previous RESELLERNAMUSD"
@Field final String PREVIOUS_RESELLER_APACUSD = "Previous RESELLERAPACUSD"
@Field final String PREVIOUS_RESELLER_CHINAUSD = "Previous RESELLERCHINAUSD"
@Field final String PREVIOUS_RESELLER_LATAMUSD = "Previous RESELLERLATAMUSD"
@Field final String PREVIOUS_RESELLER_EMEAUSD = "Previous RESELLEREMEAUSD"
@Field final String PREVIOUS_RESELLER_EMEAUSD2 = "Previous RESELLEREMEAUSD2"
@Field final String PREVIOUS_RESELLER_GLOBALUSD = "Previous RESELLERGlobalUSD"
@Field final String PREVIOUS_RESELLER_EMEAEUR = "Previous RESELLEREMEAEUR"
@Field final String PREVIOUS_RESELLER_EMEAGBP = "Previous RESELLEREMEAGBP"
@Field final String PREVIOUS_RESELLER_APACCNY = "Previous RESELLERAPACCNY"
@Field final String PREVIOUS_RESELLER_APACAUD = "Previous RESELLERAPACAUD"
@Field final String PREVIOUS_RESELLER_APACJPY = "Previous RESELLERAPACJPY"
@Field final String PREVIOUS_SAP_RESELLER_NAMUSD = "Previous SAPRESELLERNAMUSD"
@Field final String PREVIOUS_SAP_RESELLER_APACUSD = "Previous SAPRESELLERAPACUSD"
@Field final String PREVIOUS_SAP_RESELLER_CHINAUSD = "Previous SAPRESELLERCHINAUSD"
@Field final String PREVIOUS_SAP_RESELLER_LATAMUSD = "Previous SAPRESELLERLATAMUSD"
@Field final String PREVIOUS_SAP_RESELLER_EMEAUSD = "Previous SAPRESELLEREMEAUSD"
@Field final String PREVIOUS_SAP_RESELLER_EMEAUSD2 = "Previous SAPRESELLEREMEAUSD2"
@Field final String PREVIOUS_SAP_RESELLER_GLOBALUSD = "Previous SAPRESELLERGlobalUSD"
@Field final String PREVIOUS_SAP_RESELLER_EMEAEUR = "Previous SAPRESELLEREMEAEUR"
@Field final String PREVIOUS_SAP_RESELLER_EMEAGBP = "Previous SAPRESELLEREMEAGBP"
@Field final String PREVIOUS_SAP_RESELLER_APACCNY = "Previous SAPRESELLERAPACCNY"
@Field final String PREVIOUS_SAP_RESELLER_APACAUD = "Previous SAPRESELLERAPACAUD"
@Field final String PREVIOUS_SAP_RESELLER_APACJPY = "Previous SAPRESELLERAPACJPY"
@Field final String PRICEFX_LIST_PRICE = "Pricefx List Price"
@Field final String SAP_LIST_PRICE = "SAP List Price"
@Field final String DISTI_LIST_PRICE = "Disti List Price"
@Field final String SAP_DISTI_LIST_PRICE = "SAP Disti List Price"
@Field final String RESELLER_LIST_PRICE = "Reseller List Price"
@Field final String SAP_RESELLER_LIST_PRICE = "SAP reseller List Price"
@Field final String LIST_PRICES = "List Prices"