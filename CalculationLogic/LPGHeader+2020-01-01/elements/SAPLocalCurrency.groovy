Map sapCurrencyPercent = [
        (Constants.SAP_NAMUSD)   : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_NAMUSD)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_NAMUSD)?.sum()),
        (Constants.SAP_APACUSD)  : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_APACUSD)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_APACUSD)?.sum()),
        (Constants.SAP_CHINAUSD) : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_CHINAUSD)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_CHINAUSD)?.sum()),
        (Constants.SAP_LATAMUSD) : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_LATAMUSD)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_LATAMUSD)?.sum()),
        (Constants.SAP_EMEAUSD)  : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_EMEAUSD)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_EMEAUSD)?.sum()),
        (Constants.SAP_EMEAUSD2) : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_EMEAUSD2)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_EMEAUSD2)?.sum()),
        (Constants.SAP_GLOBALUSD): Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_GLOBALUSD)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_GLOBALUSD)?.sum()),
        (Constants.SAP_EMEAEUR)  : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_EMEAEUR)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_EMEAEUR)?.sum()),
        (Constants.SAP_EMEAGBP)  : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_EMEAGBP)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_EMEAGBP)?.sum()),
        (Constants.SAP_APACCNY)  : Lib.calculatePercent((api.local.inputLists?.(Constants.SAP_APACCNY))?.sum(), (api.local.inputLists?.(Constants.PREVIOUS_SAP_APACCNY))?.sum()),
        (Constants.SAP_APACAUD)  : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_APACAUD)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_APACAUD)?.sum()),
        (Constants.SAP_APACJPY)  : Lib.calculatePercent(api.local.inputLists?.(Constants.SAP_APACJPY)?.sum(), api.local.inputLists?.(Constants.PREVIOUS_SAP_APACJPY)?.sum()),
]
return sapCurrencyPercent
