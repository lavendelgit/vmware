if (api.isDebugMode()) {
    return api.find("PG", Filter.equal(Constants.ID, out.Id))?.find()
}
return api.currentItem()