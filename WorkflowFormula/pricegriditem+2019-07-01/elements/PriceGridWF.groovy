List<String> workFlowSteps = api.findLookupTableValues("WorkflowConfiguration")
for (step in workFlowSteps) {
    if (step.attribute4 == "Group") {
        def users = api.find("U", 0, api.getMaxFindResultsLimit(), null)
        def groupApproversLoginNames = users.findAll { it.allGroups.uniqueName[0] == step.attribute2 }.collect { it.loginName }
        workflow.addApprovalStep(step.attribute1)
                .withApprovers(*groupApproversLoginNames)
    } else {
        approver = step.attribute2
        workflow.addApprovalStep(step.attribute1)
                .withApprovers(approver)
    }
}
